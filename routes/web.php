<?php

use App\Submenu;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/','IndexController@index');

//login admin routes
Route::match(['get','post'],'/admin','AdminController@login');
Route::match(['get','post'],'/admin/password/reset','Auth\ForgotPasswordController@sendResetLinkEmail');
//Route::match(['get','post'],'/admin/password/reset','AdminController@restPass');

Route::get('404',['as'=>'404','uses'=>'ErrorHandlerController@errorCode404']);
Route::get('405',['as'=>'405','uses'=>'ErrorHandlerController@errorCode405']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

    Route::match(['get','post'],'/death-notice-login','UserController@userlogin');
    Route::get('/user/getdp','UserController@getDP');
    Route::get('/user/getrta','UserController@getRTA');
    Route::get('/user/getadmin','UserController@getAdmin');

 //Route::group(['middleware' => ['login']], function(){

    Route::get('/user/dashboard','UserController@userdashboard');
    Route::get('/user/settings','UserController@usersettings');
    Route::get('/user/password','UserController@changePassword');
    Route::get('/user/check-pwd','UserController@userchkPassword');
    Route::match(['get','post'],'/user/update-pwd','UserController@userupdatePassword');

    
    Route::match(['get','post'],'/death-notice/add-notice','DeathNoticeController@addNotice');
    Route::match(['get','post'],'/death-notice/edit-notice/{id}','DeathNoticeController@editNotice');
    Route::match(['get','post'],'/death-notice/delete-notice/{id}','DeathNoticeController@deleteNotice');
    Route::get('/death-notice/view-all-notice','DeathNoticeController@viewAllNotice');
    Route::get('/death-notice/view-notice','DeathNoticeController@viewNotice');
    Route::get('/death-notice/view-all-logs','DeathNoticeController@viewAllLogs');
    Route::get('/death-notice/view-logs/{id}','DeathNoticeController@viewLogs');
    Route::get('/death-notice/view-all-active-notice','DeathNoticeController@viewAllActiveNotice');
    Route::get('/death-notice/view-all-pending-notice','DeathNoticeController@viewAllPendingNotice');
    Route::get('/death-notice/verify-insert-notice/{id}','DeathNoticeController@verifyInsertNotice');
    Route::get('/death-notice/reject-insert-notice/{id}','DeathNoticeController@rejectInsertNotice');
    Route::get('/death-notice/verify-delete-notice/{id}','DeathNoticeController@verifyDeleteNotice');
    Route::get('/death-notice/reject-delete-notice/{id}','DeathNoticeController@rejectDeleteNotice');
    Route::get('/death-notice/verify-edit-notice/{id}','DeathNoticeController@verifyEditNotice');
    Route::get('/death-notice/reject-edit-notice/{id}','DeathNoticeController@rejectEditNotice');

 // });

//protect admin from unauthorized access from url
   Route::group(['middleware' => ['auth']], function(){

    Route::get('/admin/dashboard','AdminController@dashboard');
    Route::get('/admin/settings','AdminController@settings');
    Route::get('/admin/check-pwd','AdminController@chkPassword');
    Route::match(['get','post'],'/admin/update-pwd','AdminController@updatePassword');


    //Route::get('/admin/add-page','PageController@getMenu');
    Route::get('/admin/add-page/getsubmenu/{id}','PageController@getSubmenu');
    Route::get('/admin/add-user/getdp','UserController@getDP');
    Route::get('/admin/add-user/getrta','UserController@getRTA');
    Route::get('/admin/add-user/getadmin','UserController@getAdmin');

//Route::get('/admin/edit-page/getsubmenu/{id}','PageController@getSubmenu');
//Route::get('dropdownlist/getstates/{id}','DataController@getStates');
     //  Route::post('select-ajax', ['as'=>'select-ajax','uses'=>'PageController@selectAjax']);

    Route::match(['get','post'],'/admin/add-daily-update','DailyUpdateController@addDailyUpdate');
    Route::match(['get','post'],'/admin/edit-daily-update/{id}','DailyUpdateController@editDailyUpdate');
    Route::get('/admin/view-daily-update','DailyUpdateController@viewDailyUpdate');

     //Menu routes(Admin)
     Route::match(['get','post'],'/admin/add-menu','MenuController@addMenu');
     Route::match(['get','post'],'/admin/edit-menu/{id}','MenuController@editMenu');
     Route::match(['get','post'],'/admin/delete-menu/{id}','MenuController@deleteMenu');
     Route::get('/admin/view-menu','MenuController@viewMenu');
     Route::get('/admin/activate-menu/{id}','MenuController@activateMenu');
     Route::get('/admin/deactivate-menu/{id}','MenuController@deactivateMenu');

       // User mgmt routes(Admin)
     Route::match(['get','post'],'/admin/add-user','AdminController@addUser');
     Route::match(['get','post'],'/admin/edit-user/{id}','AdminController@editUser');
     Route::match(['get','post'],'/admin/delete-user/{id}','AdminController@deleteUser');
     Route::get('/admin/view-user','AdminController@viewUser');
     Route::get('/admin/activate-user/{id}','AdminController@activateUser');
     Route::get('/admin/deactivate-user/{id}','AdminController@deactivateUser');

      //SubMenu routes(Admin)
      Route::match(['get','post'],'/admin/add-submenu','SubmenuController@addSubmenu');
      Route::match(['get','post'],'/admin/edit-submenu/{id}','SubmenuController@editSubmenu');
      Route::match(['get','post'],'/admin/delete-submenu/{id}','SubmenuController@deleteSubmenu');
      Route::get('/admin/view-all-submenu','SubmenuController@viewAllSubmenu');
      Route::get('/admin/view-submenu/{id}','SubmenuController@viewSubmenu');
      Route::get('/admin/activate-submenu/{id}','SubmenuController@activateSubmenu');
      Route::get('/admin/deactivate-submenu/{id}','SubmenuController@deactivateSubmenu');

 
    //News Routes (Admin)
    Route::match(['get','post'],'/admin/add-news-notice','NewsandNoticeController@addNewsandNotice');
    Route::match(['get','post'],'/admin/edit-news-notice/{id}','NewsandNoticeController@editNewsandNotice');
    Route::match(['get','post'],'/admin/delete-news-notice/{id}','NewsandNoticeController@deleteNewsandNotice');
    Route::get('/admin/view-news-notice','NewsandNoticeController@viewNewsandNotice');
    Route::get('/admin/publish-news-notice/{id}','NewsandNoticeController@publishNewsandNotice');
    Route::get('/admin/unpublish-news-notice/{id}','NewsandNoticeController@unpublishNewsandNotice');
    Route::get('/admin/news-notice/exportexcel','NewsandNoticeController@exportExcel');
    Route::get('/admin/news-notice/exportpdf','NewsandNoticeController@exportPDF');
    Route::post('/admin/news-notice/importexcel', 'NewsandNoticeController@importExcel');


    //Press Release Routes (Admin)
    Route::match(['get','post'],'/admin/add-press-release','PressReleaseController@addPressRelease');
    Route::match(['get','post'],'/admin/edit-press-release/{id}','PressReleaseController@editPressRelease');
    Route::match(['get','post'],'/admin/delete-press-release/{id}','PressReleaseController@deletePressRelease');
    Route::get('/admin/view-press-release','PressReleaseController@viewPressRelease');
    Route::get('/admin/publish-press-release/{id}','PressReleaseController@publishPressRelease');
    Route::get('/admin/unpublish-press-release/{id}','PressReleaseController@unpublishPressRelease');
    Route::get('/admin/press-release/exportexcel','PressReleaseController@exportExcel');
    Route::get('/admin/press-release/exportpdf','PressReleaseController@exportPDF');
    Route::post('/admin/press-release/importexcel', 'PressReleaseController@importExcel');

    //Clearing member routes (Admin)
     Route::match(['get','post'],'/admin/add-cm','ClearingMemberController@addClearingMember');
     Route::match(['get','post'],'/admin/edit-cm/{id}','ClearingMemberController@editClearingMember');
     Route::match(['get','post'],'/admin/delete-cm/{id}','ClearingMemberController@deleteClearingMember');
     Route::get('/admin/view-cm','ClearingMemberController@viewClearingMember');
     Route::get('/admin/activate-cm/{id}','ClearingMemberController@activateCM');
     Route::get('/admin/deactivate-cm/{id}','ClearingMemberController@deactivateCM');
     Route::get('/admin/cm/exportexcel','ClearingMemberController@exportExcel');
     Route::get('/admin/cm/exportpdf','ClearingMemberController@exportPDF');
     Route::post('/admin/cm/importexcel', 'ClearingMemberController@importExcel');

      //CASBA Bank (Admin)
    Route::match(['get','post'],'/admin/add-casba-bank','CasbaBankController@addBank');
    Route::match(['get','post'],'/admin/edit-casba-bank/{id}','CasbaBankController@editBank');
    Route::match(['get','post'],'/admin/delete-casba-bank/{id}','CasbaBankController@deleteBank');
    Route::get('/admin/view-casba-bank','CasbaBankController@viewBank');
    Route::get('/admin/activate-casba-bank/{id}','CasbaBankController@activateBank');
    Route::get('/admin/deactivate-casba-bank/{id}','CasbaBankController@deactivateBank');
    Route::get('/admin/casba-bank/exportexcel','CasbaBankController@exportExcel');
    Route::get('/admin/casba-bank/exportpdf','CasbaBankController@exportPDF');
    Route::post('/admin/casba-bank/importexcel', 'CasbaBankController@importExcel');

     //Downloads routes (Admin)
     Route::match(['get','post'],'/admin/add-downloads','DownloadsController@addDownloads');
     Route::match(['get','post'],'/admin/edit-downloads/{id}','DownloadsController@editDownloads');
     Route::match(['get','post'],'/admin/delete-downloads/{id}','DownloadsController@deleteDownloads');
     Route::get('/admin/view-downloads','DownloadsController@viewDownloads');
     Route::get('/admin/activate-downloads/{id}','DownloadsController@activateDownloads');
     Route::get('/admin/deactivate-downloads/{id}','DownloadsController@deactivateDownloads');
     Route::get('/admin/downloads/exportexcel','DownloadsController@exportExcel');
     Route::get('/admin/downloads/exportpdf','DownloadsController@exportPDF');
     Route::post('/admin/downloads/importexcel', 'DownloadsController@importExcel');

      //Pages Routes (Admin)
      Route::match(['get','post'],'/admin/add-page','PageController@addPage');
      Route::match(['get','post'],'/admin/edit-page/{id}','PageController@editPage');
      Route::match(['get','post'],'/admin/delete-page/{id}','PageController@deletePage');
      Route::get('/admin/view-page','PageController@viewPages');
      Route::get('/admin/activate-page/{id}','PageController@activatePage');
      Route::get('/admin/deactivate-page/{id}','PageController@deactivatePage');
      Route::get('/admin/page/exportexcel','PageController@exportExcel');
      Route::get('/admin/page/exportpdf','PageController@exportPDF');
      Route::post('/admin/page/importexcel', 'PageController@importExcel');

      //RTA Routes (Admin)
      Route::match(['get','post'],'/admin/add-rta','RTAController@addRTA');
      Route::match(['get','post'],'/admin/edit-rta/{id}','RTAController@editRTA');
      Route::match(['get','post'],'/admin/delete-rta/{id}','RTAController@deleteRTA');
      Route::get('/admin/view-rta','RTAController@viewRTA');
      Route::get('/admin/activate-rta/{id}','RTAController@activateRTA');
      Route::get('/admin/deactivate-rta/{id}','RTAController@deactivateRTA');
      Route::get('/admin/rta/exportexcel','RTAController@exportExcel');
      Route::get('/admin/rta/exportpdf','RTAController@exportPDF');
      Route::post('/admin/rta/importexcel', 'RTAController@importExcel');
 
    //Issuer (Admin)
    Route::match(['get','post'],'/admin/add-issuer','IssuerController@addIssuer');
    Route::match(['get','post'],'/admin/edit-issuer/{id}','IssuerController@editIssuer');
    Route::match(['get','post'],'/admin/delete-issuer/{id}','IssuerController@deleteIssuer');
    Route::get('/admin/view-all-issuer','IssuerController@viewAllIssuer');
    Route::get('/admin/view-issuer/{id}','IssuerController@viewIssuer');
    Route::get('/admin/activate-issuer/{id}','IssuerController@activateIssuer');
    Route::get('/admin/deactivate-issuer/{id}','IssuerController@deactivateIssuer');
    Route::get('/admin/issuer/exportexcel','IssuerController@exportExcel');
    Route::get('/admin/issuer/exportpdf','IssuerController@exportPDF');
    Route::post('/admin/issuer/importexcel', 'IssuerController@importExcel');

    //ISIN (Admin)
    Route::match(['get','post'],'/admin/add-isin','ISINController@addISIN');
    Route::match(['get','post'],'/admin/edit-isin/{id}','ISINController@editISIN');
    Route::match(['get','post'],'/admin/delete-isin/{id}','ISINController@deleteISIN');
    Route::get('/admin/view-isin','ISINController@viewISIN');
    Route::get('/admin/activate-isin/{id}','ISINController@activateISIN');
    Route::get('/admin/deactivate-isin/{id}','ISINController@deactivateISIN');
    Route::get('/admin/isin/exportexcel','ISINController@exportExcel');
    Route::get('/admin/isin/exportpdf','ISINController@exportPDF');
    Route::post('/admin/isin/importexcel', 'ISINController@importExcel');

    //DP Details Routes (Admin)
    Route::match(['get','post'],'/admin/add-dp','DPController@addDP');
    Route::match(['get','post'],'/admin/edit-dp/{id}','DPController@editDP');
    Route::match(['get','post'],'/admin/delete-dp/{id}','DPController@deleteDP');
    Route::get('/admin/view-dp','DPController@viewDP');
    Route::get('/admin/activate-dp/{id}','DPController@activateDP');
    Route::get('/admin/deactivate-dp/{id}','DPController@deactivateDP');
    Route::get('/admin/dp/exportexcel','DPController@exportExcel');
    Route::get('/admin/dp/exportpdf','DPController@exportPDF');
    Route::post('/admin/dp/importexcel', 'DPController@importExcel');


    //cdsc tariff Routes (Admin)
    Route::match(['get','post'],'/admin/add-cdsc-tariff','CDSCTariffController@addTariff');
    Route::match(['get','post'],'/admin/edit-cdsc-tariff/{id}','CDSCTariffController@editTariff');
    Route::match(['get','post'],'/admin/delete-cdsc-tariff/{id}','CDSCTariffController@deleteTariff');
    Route::get('/admin/view-cdsc-tariff','CDSCTariffController@viewTariff');
    Route::get('/admin/activate-cdsc-tariff/{id}','CDSCTariffController@activateTariff');
    Route::get('/admin/deactivate-cdsc-tariff/{id}','CDSCTariffController@deactivateTariff');
    Route::get('/admin/cdsc-tariff/exportexcel','CDSCTariffController@exportExcel');
    Route::get('/admin/cdsc-tariff/exportpdf','CDSCTariffController@exportPDF');
    Route::post('/admin/cdsc-tariff/importexcel', 'CDSCTariffController@importExcel');

    
    //  Settlement routes(Admin)
    Route::match(['get','post'],'/admin/add-settlement','SettlementController@addSettlement');
    Route::match(['get','post'],'/admin/edit-settlement/{id}','SettlementController@editSettlement');
    Route::match(['get','post'],'/admin/delete-settlement/{id}','SettlementController@deleteSettlement');
    Route::get('/admin/view-settlement','SettlementController@viewSettlement');
    Route::get('/admin/activate-settlement/{id}','SettlementController@activateSettlement');
    Route::get('/admin/deactivate-settlement/{id}','SettlementController@deactivateSettlement');
    Route::get('/admin/settlement/exportexcel','SettlementController@exportExcel');
    Route::get('/admin/settlement/exportpdf','SettlementController@exportPDF');
    Route::get('/admin/settlement/importexcel', 'SettlementController@importExcel');

    // Book Closure Routes (Admin)
    Route::match(['get','post'],'/admin/add-book-closure/{id}','BookClosureController@addBook');
    Route::match(['get','post'],'/admin/edit-book-closure/{id}','BookClosureController@editBookClosure');
    Route::match(['get','post'],'/admin/delete-book-closure/{id}','BookClosureController@deleteBookClosure');
    Route::get('/admin/view-book-closure/{id}','BookClosureController@viewBookClosure');
    Route::get('/admin/view-all-book-closure','BookClosureController@viewBook');

    // Slider  Images routes(Admin)
     Route::match(['get','post'],'/admin/add-image','SliderImageController@addImage');
     Route::match(['get','post'],'/admin/edit-image/{id}','SliderImageController@editSliderImage');
     Route::match(['get','post'],'/admin/delete-image/{id}','SliderImageController@deleteImage');
     Route::get('/admin/view-image','SliderImageController@viewImage');

       //   Death Notice User mgmt routes(Admin)
     Route::match(['get','post'],'/admin/add-death-notice-user','UserController@addDeathNoticeUser');
     Route::match(['get','post'],'/admin/edit-death-notice-user/{id}','UserController@editDeathNoticeUser');
     Route::match(['get','post'],'/admin/delete-death-notice-user/{id}','UserController@deleteDeathNoticeUser');
     Route::get('/admin/view-death-notice-user','UserController@viewDeathNoticeUser');
     Route::get('/admin/activate-death-notice-user/{id}','UserController@activateDeathNoticeUser');
     Route::get('/admin/deactivate-death-notice-user/{id}','UserController@deactivateDeathNoticeUser');
     Route::get('/admin/view-password/{id}','UserController@viewPassword');
     Route::get('/admin/reset-password/{id}','UserController@resetPassword');
});

//logout routes
Route::get('/logout','AdminController@logout');
Route::get('/user/logout','UserController@userlogout');

//    frontend  routes

Route::get('Home.index', 'Frontend\HomeController@index')->name('Home.index');
//Route::get('home', 'Frontend\HomeController@index')->name('home');
Route::get('contact-us','Frontend\ContactController@index');
Route::get('introduction', 'Frontend\AboutusController@index')->name('home.about-us.introduction');

Route::get('bod', 'Frontend\BoardOfDirectorController@index')->name('Home.About-Us.board-of-director');
Route::get('managementteam', 'Frontend\ManagementTeamController@index')->name('Home.About-Us.management-team');
Route::get('globalpartner', 'Frontend\GlobalPartnerController@index')->name('Home.About-Us.global-partner');
Route::get('faq', 'Frontend\FqaController@index')->name('Home.Investor.faq');
Route::get('Home.Investor.isinscript', 'Frontend\ISINController@index')->name('Home.Investor.isinscript');
Route::get('Home.Investor.book-closure', 'Frontend\BookClosureInfoController@index')->name('Home.Investor.book-closure');
Route::get('settlementid', 'Frontend\SettlementIDController@index')->name('Home.Investor.settlementid');
Route::get('Home.Investor.registered-company', 'Frontend\RegisteredCompanyController@index')->name('Home.Investor.registered-company');
Route::get('do&dont', 'Frontend\DosController@index')->name('Home.Investor.dos-dont');
Route::get('Home.Depository-Participants.introduction', 'Frontend\DPIntroductionController@index')->name('Home.Depository-Participants.introduction');
Route::get('Home.Depository-Participants.cdsc-tariff', 'Frontend\CDSCTariffController@index')->name('Home.Depository-Participants.cdsc-tariff');
Route::get('Home.Depository-Participants.services', 'Frontend\ServicesController@index')->name('Home.Depository-Participants.services');
Route::get('Home.Depository-Participants.admission-procedure', 'Frontend\DPAdmissionProcedureController@index')->name('Home.Depository-Participants.admission-procedure');
Route::get('Home.Depository-Participants.hardwaresoftware', 'Frontend\DPHardwareController@index')->name('Home.Depository-Participants.hardwaresoftware');
Route::get('Home.Depository-Participants.operating', 'Frontend\DPOController@index')->name('Home.Depository-Participants.operating');
Route::get('Home.Depository-Participants.numberofdp', 'Frontend\NumberOfDPController@index')->name('Home.Depository-Participants.numberofdp');
Route::get('Home.RTA.introduction','Frontend\RtaIntroductionController@index')->name('Home.RTA.introduction');
Route::get('Home.RTA.rtalist', 'Frontend\RtaListController@index')->name('Home.RTA.rtalist');
Route::get('Home.RTA.admission-procedure', 'Frontend\RtaAdmissionProcedureController@index')->name('Home.RTA.admission-procedure');
Route::get('Home.RTA.operating-instruction', 'Frontend\RtaOperatingInstructionController@index')->name('Home.RTA.operating-instruction');
Route::get('Home.RTA.documentationforcooperativeauction', 'Frontend\DocumentationForCooperateActionController@index')->name('Home.RTA.documentationforcooperativeauction');
Route::get('Home.Clearing-Member.introduction', 'Frontend\CMIntroductionController@index')->name('Home.Clearing-Member.introduction');
Route::get('Home.Clearing-Member.clearingmember', 'Frontend\ListOfClearingMemberController@index')->name('Home.Clearing-Member.clearingmember');
Route::get('Home.Clearing-Member.settlement-procedure', 'Frontend\SettlementProcedureController@index')->name('Home.Clearing-Member.settlement-procedure');
Route::get('Home.Issuer.introduction', 'Frontend\IssuerController@index')->name('Home.Issuer.introduction');
Route::get('Home.Issuer.benefits', 'Frontend\BenifitsController@index')->name('Home.Issuer.benefits');
Route::get('Home.Issuer.admission-procedure', 'Frontend\IssuerAdmissionProcedureController@index')->name('Home.Issuer.admission-procedure');

Route::get('Home.Publication.bylaws', 'Frontend\ByLawsController@index')->name('Home.Publication.bylaws');
Route::get('Home.Publication.circulars', 'Frontend\CircularController@index')->name('Home.Publication.circulars');
Route::get('Home.download', 'Frontend\DownloadsController@index')->name('Home.download');
Route::get('/contact-us', 'Frontend\ContactController@index')->name('Home.contact-us');
Route::get('Home.registerbankincasba', 'Frontend\RegisterbankincasbaController@index')->name('Home.registerbankincasba');
Route::get('Home.registeredDPsinMEROSHARE', 'Frontend\RegisteredbankinmeroshareController@index')->name('Home.registeredDPsinMEROSHARE');
Route::get('Home/Notices', 'Frontend\NoticesController@index')->name('Home/Notices');
Route::get('Home/news', 'Frontend\NewsController@index')->name('Home/news');
Route::get('Home/pressrelease', 'Frontend\PressReleaseController@index')->name('Home/pressrelease');
Route::get('Home.Investor.death_notice', 'Frontend\DeathNoticeController@index')->name('Home.Investor.death_notice');
//CMS Route
Route::get('/{slug}','Frontend\PageController@show');

//Publication and Demat
Route::get('Home.demat', 'Frontend\DownloadsController@index')->name('Home.demat');
 Route::get('Home.publication', 'Frontend\DownloadsController@showPublication')->name('Home.publication');
 Route::get('/company/{id}','Frontend\IssuerController@company');