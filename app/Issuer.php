<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\RTA;

class Issuer extends Model
{
    //
    use SoftDeletes;
    protected $table = 'issuer';
    protected $fillable = ['rta_id','name','address','phone','email','company_code','reg_no','pan_no','issued_capital','listed_capital','issuer_status'];
    protected $dates = ['deleted_at'];
    
    public function rta(){
        return $this->belongsTo('App\RTA','rta_id','rta_id');
    }
}
