<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeathNoticeLog extends Model
{
    //
    protected $table = 'death_notice_log';
    protected $fillable = ['user_id','name','father_name','grandfather_name','address','bo_account','file_name','file','published_date','ending_date','verified_by','status','isVerified','request'];
}
