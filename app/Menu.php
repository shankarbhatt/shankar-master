<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    //
    use SoftDeletes;
    protected $table = 'menu';
    protected $fillable = ['name','order'];
    protected $dates = ['deleted_at'];

    public function submenus()
    {
        return $this->hasMany('App\Submenu','menu_id','menu_id');
    }
    public function submenu(){
        return $this->hasOne('App\Submenu','menu_id','menu_id');        
    }
    public function parent(){
        return $this->hasOne('App\Submenu','menu_id','menu_id');        
    }
}
