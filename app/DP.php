<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DP extends Model
{
    //
    use SoftDeletes;
    protected $table = 'dp';
    protected $filable = ['dp_id','type','name','address','phone','email','status','meroshare'];
    protected $dates = ['deleted_at'];
}
