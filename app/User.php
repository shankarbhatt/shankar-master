<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    //
      use SoftDeletes;
    protected $table = 'users';
    protected $fillable = ['user_id','type','email','password','actual_password','status'];
  //  protected $hidden = ['password', 'remember_token'];
    protected $dates = ['deleted_at'];
}
