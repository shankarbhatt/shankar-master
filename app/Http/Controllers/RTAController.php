<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RTA;
use Excel;

class RTAController extends Controller
{
    //
    public function addRTA(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $rta = new RTA();

            $this->validate($request, [
                'rta_id' => 'required|digits:3|unique:rta',
                'name' => 'required|regex:/^[a-zA-Z .]+$/u',
                'address' => 'required',
                'phone' => 'required|regex:/^[0-9 ,]+$/u',
                'email' => 'required|email'
              ],
              [
                'rta_id.digits' => 'RTA ID must be exactly 3 numbers',
                'name.regex' => 'Name must be only text',
                'address.required'=> 'Address is required',
                'phone.regex' => 'Phone must be only numbers',
            ]);

            $rta->rta_id = $data['rta_id'];
            $rta->name = $data['name'];
            $rta->dp_type = 4;
            $rta->address = $data['address'];
            $rta->phone = $data['phone'];
            $rta->email = $data['email'];
            $rta->save();
            return redirect('/admin/view-rta')->with('flash_message_success','RTA Added Successfully..');
        }
        return view('admin.rta.add_rta');
    }

    public function viewRTA(){
        $rta = RTA::orderBy('name','ASC')->get();
        return view('admin.rta.view_rta')->with(compact('rta'));
    }

    public function editRTA(Request $request,$id=null){
        if($request->isMethod('post')){
            $data = $request->all();

              $this->validate($request, [
                'name' => 'required|regex:/^[a-zA-Z .]+$/u',
                'address' => 'required',
                'phone' => 'required|regex:/^[0-9 ,]+$/u',
                'email' => 'required|email'
              ],
              [
                'name.regex' => 'Name must be only text',
                'address.required'=> 'Address is required',
                'phone.regex' => 'Phone must be only numbers',
            ]);

            RTA::where(['id'=>$id])->update(['name'=>$data['name'],'address'=>$data['address'],'phone'=>$data['phone'],'email'=>$data['email']]);
            return redirect('/admin/view-rta')->with('flash_message_success','RTA Updated Successfully..');
        }
            $rtaDetails = RTA::where(['id'=>$id])->first();
            return view('admin.rta.edit_rta')->with(compact('rtaDetails'));
    }

    public function deleteRTA($id=null){
        if(!empty($id)){
            $status = 2;
            RTA::where(['id'=>$id])->update(['status'=>$status]);
            RTA::where(['id'=>$id])->delete();
            return redirect()->back()->with('flash_message_success','RTA Deleted Successfully..');
        }
    }

    public function activateRTA(Request $request,$id=null){
       
        if(empty($data['status'])){
            $status = 1;
           // $date = date('Y/m/d');
           RTA::where(['id'=>$id])->update(['status'=>$status]);
        } 
        return redirect('/admin/view-rta')->with('flash_message_success','RTA Activated Successfully...');
}

public function deactivateRTA(Request $request,$id=null){
       
    if(empty($data['status'])){
        $status = 0;
       // $date = date('Y/m/d');
        RTA::where(['id'=>$id])->update(['status'=>$status]);
    } 
    return redirect('/admin/view-rta')->with('flash_message_success','RTA Deactivated Successfully...');
}
    public function exportExcel(){
        $rta = RTA::get()->toArray();
        Excel::create('rta_details',function($excel) use ($rta) {
            $excel->sheet('rta_details',function($sheet) use ($rta) {
                $sheet->fromArray($rta);
            });
        })->download('xls');
    }

}
