<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Admin;
use Illuminate\Support\Facades\Hash; 

class AdminController extends Controller
{
    public function login(Request $request){

    	if($request->isMethod('post')){
    		$data = $request->input();
    	
    		if(Auth::attempt(['email' => $data['email'], 'password' => $data['password']])){
				return redirect('/admin/dashboard');
    		} else {
				return redirect('/admin')->with('flash_message_error','Incorrect Email or Password..');
    		}
    	}
        return view('admin.admin_login');
    }

    public function dashboard(){
        return view('admin.dashboard');
	}

	public function forgotPass(){
        return view('admin.forgot_password');
	}

	public function resetPass(){
        return view('admin.reset_password');
	}

	public function settings(){
		return view('admin.settings');
	}

	public function chkPassword(Request $request){
		$data = $request->all();
		$current_password = $data['current_pwd'];
		$check_password = Admin::first();
		if(Hash::check($current_password,$check_password->password)){
			echo "true"; die;
		} else {
			echo "false"; die;
		}

	}

	public function updatePassword(Request $request){
		if($request->isMethod('post')){
			$data = $request->all();
		   
			$check_password = Admin::where(['email' => Auth::user()->email])->first();
			$current_password = $data['current_pwd'];
			if(Hash::check($current_password,$check_password->password)){
				$password = bcrypt($data['new_pwd']);
				$id = Auth::user()->id;
				Admin::where('id',$id)->update(['password' => $password]);
				return redirect('/admin/settings')->with('flash_message_success','Password Updated Successfully!');
			} else {
				return redirect('/admin/settings')->with('flash_message_error','Incorrect Current Password!');
			}
		}
	}
	
	public function logout(){
		Session::flush();
		return redirect('/admin')->with('flash_message_success','Logged out Successfully..');
	}
}
