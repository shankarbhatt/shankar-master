<?php

namespace App\Http\Controllers;
use App\ClearingMember;
use Excel;

use Illuminate\Http\Request;

class ClearingMemberController extends Controller
{
    public function addClearingMember(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();

                $this->validate($request, [
                'name' => 'required|regex:/^[a-zA-Z .]+$/u',
                'cm_no' => 'required|numeric|unique:clearing_member',
                'address' => 'required',
                'pool_account' => 'required|digits:16',
              ],
              [
                'name.regex' => 'Name must be only text',
                'cm_no.numberic' => 'CM No must be only numbers',
                'address.required'=> 'Address is required',
                'pool_account.digits' => 'Pool Account must be exactly 16 numbers',
            ]);

            $clearingMember = new ClearingMember();
            $clearingMember->name = $data['name'];
            $clearingMember->cm_no = $data['cm_no'];
            $clearingMember->address = $data['address'];
            $clearingMember->pool_account = $data['pool_account'];
            $poolAccountDP = substr($data['pool_account'], 3, 5);
            $clearingMember->pool_account_dp = $poolAccountDP;
            $clearingMember->save();
            return redirect('/admin/view-cm')->with('flash_message_success','Clearing Member Added Successfully..');
        }
         return view('admin.clearing_member.add_clearing_member');
    }

    public function viewClearingMember(){
        $clearingMember = ClearingMember::orderBy('name','ASC')->get();
      //  $clearingMember = json_decode(json_encode($clearingMember));
        return view('admin.clearing_member.view_clearing_member')->with(compact('clearingMember'));
    }

    public function editClearingMember(Request $request,$cm_id=null){
        if($request->isMethod('post')){
            $data = $request->all();

               $this->validate($request, [
                'name' => 'required|regex:/^[a-zA-Z .]+$/u',
                'cm_no' => 'required|numeric',
                'address' => 'required',
                'pool_account' => 'required|digits:16',
              ],
              [
                'name.regex' => 'Name must be only text',
                'cm_no.numberic' => 'CM No must be only numbers',
                'address.required'=> 'Address is required',
                'pool_account.digits' => 'Pool Account must be exactly 16 numbers',
            ]);

            $poolAccountDP = substr($data['pool_account'], 3, 5);
            ClearingMember::where(['cm_id'=>$cm_id])->update(['name'=>$data['name'],'cm_no'=>$data['cm_no'],'address'=>$data['address'],'pool_account'=>$data['pool_account'],'pool_account_dp'=>$poolAccountDP]);
            return redirect('/admin/view-cm')->with('flash_message_success','Clearing Member Updated Successfully..');
        }
        $clearingMemberDetails = ClearingMember::where(['cm_id'=>$cm_id])->first();
        return view('admin.clearing_member.edit_clearing_member')->with(compact('clearingMemberDetails'));
    }

    public function deleteClearingMember($cm_id=null){
        if(!empty($cm_id)){
            $status = 2;
            ClearingMember::where(['cm_id'=>$cm_id])->update(['status'=>$status]);
            ClearingMember::where(['cm_id'=>$cm_id])->delete();
            return redirect()->back()->with('flash_message_success','Clearing Member Deleted Successfully..');
        }
    }

    public function activateCM(Request $request,$cm_id=null){
       
        if(empty($data['status'])){
            $status = 1;
           // $date = date('Y/m/d');
           ClearingMember::where(['cm_id'=>$cm_id])->update(['status'=>$status]);
        } 
        return redirect('/admin/view-cm')->with('flash_message_success','Clearing Member Activated Successfully...');
   }

   public function deactivateCM(Request $request,$cm_id=null){
       
    if(empty($data['status'])){
        $status = 0;
       // $date = date('Y/m/d');
       ClearingMember::where(['cm_id'=>$cm_id])->update(['status'=>$status]);
    } 
    return redirect('/admin/view-cm')->with('flash_message_success','Clearing Member Deactivated Successfully...');
}

public function exportExcel(){
    $cm = ClearingMember::get()->toArray();
    Excel::create('clearing_member_details',function($excel) use ($cm) {
        $excel->sheet('clearing_member_details',function($sheet) use ($cm) {
            $sheet->fromArray($cm);
        });
    })->download('xls');
}

}
