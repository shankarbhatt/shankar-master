<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Submenu;
use App\Menu;
use DB;
use Illuminate\Support\Facades\Input;


class SubmenuController extends Controller
{
    
    public function addSubmenu(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();

            $this->validate($request, [
                'menu_id' => 'required',
                'submenu_name' => 'required|regex:/^[a-zA-Z &]+$/u',
                'order' => 'required|numeric|not_in:0'
              ],
              [
                'menu_id.required' => 'Menu Name is required',
                'submenu_name.regex' => 'Submenu Name must be only text',
                'order.numeric' => 'Order must be only numbers',
                'order.not_in' => 'Order cannot be 0. It must start from 1.'
            ]);

            $order = Input::get('order');
            $menu = Input::get('menu_id');

            $db_order = DB::table('submenu')->where('order', $order)->where('menu_id', $menu)->pluck('order')->first();
            $db_menu = DB::table('submenu')->where('order', $order)->where('menu_id', $menu)->pluck('menu_id')->first();
  
            $submenu = new Submenu();
            $submenu->menu_id = $data['menu_id'];
            $submenu->name = $data['submenu_name'];
               if($menu == $db_menu && $order == $db_order) {
                return redirect()->back()->with('flash_message_error','Order Aleady Exists..');
            } else {
                  $submenu->order = $data['order'];
            }

            $submenu->save();
            return redirect('/admin/view-all-submenu')->with('flash_message_success','Submenu Added Successfully..');
        } 
        return view('admin.submenu.add_submenu');
     }
 
     public function viewAllSubmenu(){
        $data = Submenu::with('menu')->orderBy('name','ASC')->get();
       // print_r($data);die;
        return view('admin.submenu.view_all_submenu')->with(compact('data'));
    }

    public function viewSubmenu($menu_id){
        if(!empty($menu_id)) {
            $menu = Menu::where(['menu_id'=>$menu_id])->first();
            $data = Submenu::where(['menu_id'=>$menu_id])->get();
            return view('admin.submenu.view_submenu')->with(compact('menu','data'));
        }
    }
 
     public function editSubmenu(Request $request,$id=null){
         if($request->isMethod('post')){
             $data = $request->all();

             $this->validate($request, [
                'menu_id' => 'required',
                'submenu_name' => 'required|regex:/^[a-zA-Z &]+$/u',
                'order' => 'required|numeric|not_in:0'
              ],
              [
                'menu_id.required' => 'Menu Name is required',
                'submenu_name.regex' => 'Submenu Name must be only text',
                'order.numeric' => 'Order must be only numbers',
                'order.not_in' => 'Order cannot be 0. It must start from 1.'
            ]);

             $order = Input::get('order');
             $menu = Input::get('menu_id');

             Submenu::where(['submenu_id'=>$id])->update(['menu_id'=>$data['menu_id'],'name' => $data['submenu_name'],'order' => $data['order']]);
             return redirect('/admin/view-all-submenu')->with('flash_message_success','Submenu Updated Successfully..');
         }
         $submenu = Submenu::where(['submenu_id'=>$id])->first();
         return view('admin.submenu.edit_submenu')->with(compact('submenu'));
     }
 
     public function deleteSubmenu($id=null){
         if(!empty($id)){
            $status = 2;
            Submenu::where(['submenu_id'=>$id])->update(['status'=>$status]);
            Submenu::where(['submenu_id'=>$id])->delete();
             return redirect()->back()->with('flash_message_success','Submenu Deleted Successfully..');
         }
     }

     public function activateSubmenu(Request $request,$id=null){
       
        if(empty($data['status'])){
            $status = 1;
           // $date = date('Y/m/d');
           Submenu::where(['submenu_id'=>$id])->update(['status'=>$status]);
        } 
        return redirect('/admin/view-all-submenu')->with('flash_message_success','Submenu Activated Successfully...');
    }

    public function deactivateSubmenu(Request $request,$id=null){
       
        if(empty($data['status'])){
            $status = 0;
           // $date = date('Y/m/d');
           Submenu::where(['submenu_id'=>$id])->update(['status'=>$status]);
        } 
        return redirect('/admin/view-all-submenu')->with('flash_message_success','Submenu Deactivated Successfully...');
    }

}
