<?php

namespace App\Http\Controllers;
use App\Page;
use Excel;
use App\Submenu;
use App\Menu;
use DB;

use Illuminate\Http\Request;

class PageController extends Controller
{
    
    public function addPage(Request $request){
       if($request->isMethod('post')){
           $data = $request->all();

              $this->validate($request, [
                'menu_id' => 'required',
                'title'=> 'required',
                'description' => 'required'
              ],
              [
                'menu_id.required' => 'Menu Name is required',
                'title.required' => 'Title is required',
                'description.required' => 'Description is required'
            ]);

           $page = new Page();
           $page->menu_id = $data['menu_id'];
           $page->submenu_id = $data['submenu_id'];
           $page->title = $data['title'];
           $page->slug = str_slug($data['slug']);
           $description  = strip_tags(htmlspecialchars_decode($data['description']));
           $page->description = $description;
           $page->save();
          
         // echo $title; ;die;
           return redirect('/admin/view-page')->with('flash_message_success','Page Added Successfully..');
       } 
         $menu = DB::table('menu')->where('deleted_at',null)->where('status',1)->pluck("name","menu_id");
         return view('admin.page.add_page')->with(compact('menu'));
  
    }

    public function getSubmenu($id) 
     {        
        $submenu = DB::table("submenu")->where("menu_id",$id)->pluck("name","submenu_id");
        return json_encode($submenu);
      }

    public function viewPages(){
        $pages = Page::orderBy('title','ASC')->get();
        return view('admin.page.view_page')->with(compact('pages'));
    }

    public function editPage(Request $request,$page_id=null){
        if($request->isMethod('post')){
            $data = $request->all();

            $this->validate($request, [
                
                'title'=> 'required',
                'description' => 'required'
              ],
              [
               
                'title.required' => 'Title is required',
                'description.required' => 'Description is required'
            ]);
            
            $description  = strip_tags(htmlspecialchars_decode($data['description']));
            Page::where(['page_id'=>$page_id])->update(['title'=>$data['title'],'slug'=>str_slug($data['slug']),'description'=>$description]);
            return redirect('/admin/view-page')->with('flash_message_success','Page Updated Successfully..');
        }
        $pageDetails = Page::where(['page_id'=>$page_id])->first();
        return view('admin.page.edit_page')->with(compact('pageDetails'));
    }

    public function deletePage($page_id=null){
        if(!empty($page_id)){
            $status = 2;
            Page::where(['page_id'=>$page_id])->update(['status'=>$status]);
            Page::where(['page_id'=>$page_id])->delete();
            return redirect()->back()->with('flash_message_success','Page Deleted Successfully..');
        }
    }

    public function activatePage($page_id=null){
       
        if(empty($data['status'])){
            $status = 1;
           // $date = date('Y/m/d');
           Page::where(['page_id'=>$page_id])->update(['status'=>$status]);
        } 
        return redirect('/admin/view-page')->with('flash_message_success','Page Activated Successfully...');
}

public function deactivatePage($page_id=null){
       
    if(empty($data['status'])){
        $status = 0;
       // $date = date('Y/m/d');
       Page::where(['page_id'=>$page_id])->update(['status'=>$status]);
    } 
    return redirect('/admin/view-page')->with('flash_message_success','Page Deactivated Successfully...');
}

    public function exportExcel(){
        $page = Page::get()->toArray();
        Excel::create('page_details',function($excel) use ($page) {
            $excel->sheet('page_details',function($sheet) use ($page) {
                $sheet->fromArray($page);
            });
        })->download('xls');
    }
}
