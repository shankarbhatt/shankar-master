<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\PressRelease;
use Excel;

class PressReleaseController extends Controller
{
    public function addPressRelease(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $pressRelease = new PressRelease();

            $this->validate($request, [
                'title' => 'required',
                'file_name' => 'required',
                'file' => 'required|mimes:doc,pdf,docx',
                'published_date' => 'required|date_format:Y-m-d'
            ],
            [
               'title.required'=> 'Title is required',
               'file_name.required'=> 'File Name is required',
               'file.mimes'=> 'File must be in pdf, doc or docx format.',
               'published_date.date_format' => 'Trade Date must be in Y-m-d (2018-09-23) format'
            ]);

            if($request->has('file')){
                $file = Input::file('file');
                if($file->isValid()){
                    $file = $request->file('file');
                    $destination_path = public_path().'/press_release_files';
            
                    $date = date('Y_m_d_H_i_s_');
                    $extension = $file->getClientOriginalExtension();
                    $files = $file->getClientOriginalName();
                    $fileName = $date . $files;
                  //  echo $fileName;die;
                    $file->move($destination_path,$fileName);
                    $pressRelease->file = $fileName;
                }
             }

            $pressRelease->title = $data['title'];
            $description  = strip_tags(htmlspecialchars_decode($data['description']));
            $pressRelease->description = $description;
             $pressRelease->file_name = $data['file_name'];
            $pressRelease->published_date = $data['published_date'];
            $pressRelease->save();
            return redirect('/admin/view-press-release')->with('flash_message_success','Press Release Added Successfully...');
            //echo "<pre>"; print_r($data); die;
        } 
        return view('admin.press_release.add_press_release');
    }

    public function viewPressRelease(){
        $pressRelease =  PressRelease::orderBy('published_date','DESC')->get();
        return view('admin.press_release.view_press_release')->with(compact('pressRelease'));
    }

    public function editPressRelease(Request $request, $press_release_id = null){
        if($request->isMethod('post')){
           $data = $request->all();

          $this->validate($request, [
                'title' => 'required',
                'file_name' => 'required',
                'file' => 'mimes:doc,pdf,docx',
                'published_date' => 'required|date_format:Y-m-d'
            ],
            [
               'title.required'=> 'Title is required',
               'file_name.required'=> 'File Name is required',
               'file.mimes'=> 'File must be in pdf, doc or docx format.',
               'published_date.date_format' => 'Trade Date must be in Y-m-d (2018-09-23) format'
            ]);

        if($request->has('file')){
            $file = Input::file('file');
            if($file->isValid()){
                 $file = $request->file('file');
                 $destination_path = public_path().'/press_release_files';
        
                $date = date('Y_m_d_H_i_s_');
                $extension = $file->getClientOriginalExtension();
                $files = $file->getClientOriginalName();
                $fileName =$date . $files;
                $file->move($destination_path,$fileName);
            }
         } else {
            $fileName = $data['current_file'];
         }

           $description  = strip_tags(htmlspecialchars_decode($data['description']));
           PressRelease::where(['press_release_id'=>$press_release_id])->update(['title' => $data['title'], 'description' => $description,'file_name'=>$data['file_name'],'file'=>$fileName,'published_date'=>$data['published_date']]);
           return redirect('/admin/view-press-release')->with('flash_message_success','Press Release Updated Successfully..');
        }
        $pressReleaseDetails = PressRelease::where(['press_release_id'=>$press_release_id])->first();
        return view('admin.press_release.edit_press_release')->with(compact('pressReleaseDetails'));
    }

    public function deletePressRelease($press_release_id = null){
        if(!empty($press_release_id)){
            $status = 2;
            PressRelease::where(['press_release_id'=>$press_release_id])->update(['status'=>$status]);
            PressRelease::where(['press_release_id'=>$press_release_id])->delete();
            return redirect()->back()->with('flash_message_success','Press Release Deleted Successfully..');
        }
    }

    public function publishPressRelease(Request $request,$press_release_id=null){
       
        if(empty($data['status'])){
            $status = 1;
         //   $date = date('Y/m/d');
            PressRelease::where(['press_release_id'=>$press_release_id])->update(['status'=>$status,'published_date'=>$date]);
        } 
        return redirect('/admin/view-press-release')->with('flash_message_success','Press Release Published Successfully...');
    }

    public function unpublishPressRelease(Request $request, $press_release_id = null){
        if(empty($data['status'])){
            $status = 0;
            PressRelease::where(['press_release_id'=>$press_release_id])->update(['status'=>$status]);
        } 
        return redirect('/admin/view-press-release')->with('flash_message_success','Press Release Unpublished Successfully...');

    }

    public function exportExcel(){
        $pressRelease = PressRelease::get()->toArray();
        Excel::create('press_release_details',function($excel) use ($pressRelease) {
            $excel->sheet('press_release_details',function($sheet) use ($pressRelease) {
                $sheet->fromArray($pressRelease);
            });
        })->download('xls');
    }
}
