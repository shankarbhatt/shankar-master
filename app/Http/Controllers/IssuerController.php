<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Issuer;
use App\RTA;
use Excel;

class IssuerController extends Controller
{
    //
    public function addIssuer(Request $request) {
        if($request->isMethod('post')){
            $data = $request->all();
            $issuer =  new Issuer();
            $rta = new RTA();
        
         $this->validate($request, [
                'name' => 'required|regex:/^[a-zA-Z .( )]+$/u',
                'address' => 'required',
                'phone' => 'required|regex:/^[0-9 ,]+$/u',
                'email' => 'required|email',
                'company_code' => 'required|unique:issuer',
                'reg_no' => 'required|unique:issuer',
                'pan_no' => 'required',
               // 'pan_no' => 'required|digits|min:9|max:10|unique:issuer',
                'issued_capital' => 'required|numeric',
                'listed_capital' => 'required|numeric',
                'rta_id' => 'required'
              ],
              [
                'name.regex' => 'Name must be only text',
                'address.required'=> 'Address is required',
                'phone.regex' => 'Phone must be only numbers',
                'company_code.required' => 'Company Code is required',
                'reg_no.required' => 'Registered No is required',
                'pan_no.required' => 'PAN No is required',
               // 'pan_no.min' => 'Pan No must be at least 9 digits',
               // 'pan_no.max' => 'Pan No shouldnot be greater than 10 digits',
                'issued_capital.digits' => 'Issued Capital must be only numbers',
                'listed_capital.digits' => 'Listed Capital must be only numbers',
                'rta_id.required' => 'RTA Name is required',
            ]);
            
            $issuer->rta_id = $data['rta_id'];
            $issuer->name = $data['name'];
            $issuer->address = $data['address'];
            $issuer->phone = $data['phone'];
            $issuer->email = $data['email'];
            $issuer->company_code = $data['company_code'];
            $issuer->reg_no = $data['reg_no'];
            $issuer->pan_no = $data['pan_no'];
            $issuer->issued_capital = $data['issued_capital'];
            $issuer->listed_capital = $data['listed_capital'];
          
            $issuer->save();
            return redirect('/admin/view-all-issuer')->with('flash_message_success','Issuer Added Sucessfully..');
        }
        return view('admin.issuer.add_issuer');
    }

    public function viewAllIssuer(){
        $data = Issuer::with('rta')->orderBy('name','ASC')->get();
       // print_r($data);die;
        return view('admin.issuer.view_all_issuer')->with(compact('data'));
    }

    public function viewIssuer($rta_id){
        if(!empty($rta_id)) {
            $rta = RTA::where(['rta_id'=>$rta_id])->first();
            $data = Issuer::where(['rta_id'=>$rta_id])->get();
            return view('admin.issuer.view_issuer')->with(compact('data','rta'));
        }
    }

    public function editIssuer(Request $request,$issuer_id){
        if($request->isMethod('post')){
            $data = $request->all();

             $this->validate($request, [
                'name' => 'required|regex:/^[a-zA-Z .( )]+$/u',
                'address' => 'required',
                'phone' => 'required|regex:/^[0-9 ,]+$/u',
                'email' => 'required|email',
                'company_code' => 'required',
                'reg_no' => 'required',
                'pan_no' => 'required',
                //'pan_no' => 'required|digits|min:9|max:10',
                'issued_capital' => 'required|numeric',
                'listed_capital' => 'required|numeric',
                'rta_id' => 'required'
              ],
              [
                'name.regex' => 'Name must be only text',
                'address.required'=> 'Address is required',
                'phone.regex' => 'Phone must be only numbers',
                'company_code.required' => 'Company Code is required',
                'reg_no.required' => 'Registered No is required',
                'pan_no.required' => 'PAN No is required',
               // 'pan_no.min' => 'Pan No must be at least 9 digits',
               // 'pan_no.max' => 'Pan No shouldnot be greater than 10 digits',
                'issued_capital.digits' => 'Issued Capital must be only numbers',
                'listed_capital.digits' => 'Listed Capital must be only numbers',
                'rta_id.required' => 'RTA Name is required',
            ]);
        
            Issuer::where(['issuer_id'=>$issuer_id])->update(['rta_id'=>$data['rta_id'],'name' => $data['name'],'address'=>$data['address'],'phone'=>$data['phone'],'email'=>$data['email'],'company_code'=>$data['company_code'],'reg_no'=>$data['reg_no'],'pan_no'=>$data['pan_no'],'issued_capital'=>$data['issued_capital'],'listed_capital'=>$data['listed_capital']]);
           return redirect('/admin/view-all-issuer')->with('flash_message_success','Issuer Updated Successfully..');
        }
        $issuerData = Issuer::where(['issuer_id'=>$issuer_id])->first();
        return view('admin.issuer.edit_issuer')->with(compact('issuerData'));
    }

    public function deleteIssuer($issuer_id){
        if(!empty($issuer_id)){
            $status = 2;
            Issuer::where(['issuer_id'=>$issuer_id])->update(['status'=>$status]);
            Issuer::where(['issuer_id'=>$issuer_id])->delete();
            return redirect()->back()->with('flash_message_success','Issuer Deleted Successfully..');
        }
    }

    public function activateIssuer(Request $request,$id=null){
       
        if(empty($data['status'])){
            $status = 1;
           // $date = date('Y/m/d');
           Issuer::where(['issuer_id'=>$id])->update(['status'=>$status]);
        } 
        return redirect('/admin/view-all-issuer')->with('flash_message_success','Issuer Activated Successfully...');
   }

   public function deactivateIssuer(Request $request,$id=null){
       
    if(empty($data['status'])){
        $status = 0;
       // $date = date('Y/m/d');
       Issuer::where(['issuer_id'=>$id])->update(['status'=>$status]);
    } 
    return redirect('/admin/view-all-issuer')->with('flash_message_success','Issuer Deactivated Successfully...');
  }

  public function exportExcel(){
    $issuer = Issuer::get()->toArray();
    Excel::create('issuer_details',function($excel) use ($issuer) {
        $excel->sheet('issuer_details',function($sheet) use ($issuer) {
            $sheet->fromArray($issuer);
        });
    })->download('xls');
}
}
