<?php

namespace App\Http\Controllers;
use App\BookClosure;
use App\RTARegCompany;

use Illuminate\Http\Request;

class BookClosureController extends Controller
{
    //
    public function addUser(){
        echo "Under Construction!!!! Work In Progress!!!";
    }
      public function viewUser(){
        echo "Under Construction!!!! Work In Progress!!!";
    }

     public function addBook(){
        echo "Under Construction!!!! Work In Progress!!!";
    }
      public function viewBook(){
        echo "Under Construction!!!! Work In Progress!!!";
    }

    public function addBookClosure(Request $request,$company_id){
        $company =  RTARegCompany::Where(['company_id'=>$company_id])->first();
  
        if($request->isMethod('post')){
            $data = $request->all();
            $book = new BookClosure();
            $book ->company_id  = $company_id;
            $book ->last_trade_date  = $data['last_trade_date'];
            $book ->book_closure_start_date = $data['book_closure_start_date'];
            $book ->book_closure_end_date = $data['book_closure_end_date'];
            $book ->book_closure_purpose  = $data['book_closure_purpose'];
            $book ->book_closure_fiscal_year  = $data['book_closure_fiscal_year'];
          //  echo $book; die;
            $book->save();
            return redirect('/admin/view-all-book-closure')->with('flash_message_success','Book Closure Added Successfully..');
        }
         return view('admin.investor.book_closure.add_book_closure')->with(compact('company'));
    }

    public function viewBookClosure($company_id){
        if(!empty($company_id)) {
           $company = RTARegCompany::where(['company_id'=>$company_id])->first();
           $book = BookClosure::where(['company_id'=>$company_id])->get();
           return view('admin.investor.book_closure.view_book_closure')->with(compact('company','book'));
        }
    }
    
    public function viewAllBookClosure(){
      //  $com = RTARegCompany::get();
        $book = BookClosure::get();  
       return view('admin.investor.book_closure.view_all_book_closure')->with(compact('book'));
    }

    public function editBookClosure(Request $request,$id=null){
        if($request->isMethod('post')){
            $data = $request->all();
            BooKClosure::where(['id'=>$id])->update(['last_trade_date'=>$data['last_trade_date'],'book_closure_start_date'=>$data['book_closure_start_date'],'book_closure_end_date'=>$data['book_closure_end_date'],'book_closure_purpose'=>$data['book_closure_purpose'],'book_closure_fiscal_year'=>$data['book_closure_fiscal_year']]);
            return redirect('/admin/view-book-closure')->with('flash_message_success',' Updated Successfully..');
        }
        $bookClosureDetails = BooKClosure::where(['id'=>$id])->first();
        return view('admin.investor.book_closure.edit_book_closure')->with(compact('bookClosureDetails'));
    }

    public function deleteBookClosure($id=null){
        if(!empty($id)){
            BooKClosure::where(['id'=>$id])->delete();
            return redirect()->back()->with('flash_message_success','Book Closure Deleted Successfully..');
        }
    }
}
