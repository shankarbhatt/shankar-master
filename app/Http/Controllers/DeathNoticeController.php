<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DeathNotice;
use App\DeathNoticeLog;
use App\User;
use Session;
use Excel;
use DB;
use Response;
use Validator;
use File;
use Illuminate\Support\Facades\Input;

class DeathNoticeController extends Controller
{
    //
    public function addNotice(Request $request){
       if(Session::has('idSession')){
             if($request->isMethod('post')){

                $data = $request->all();

                $this->validate($request, [
                'death_name' => 'required|regex:/^[a-zA-Z ]+$/u',
                'father_name' => 'required|regex:/^[a-zA-Z ]+$/u',
                'grandfather_name' => 'required|regex:/^[a-zA-Z ]+$/u',
                'address' => 'required',
               // 'bo_account' => 'required',
               // 'file_name' => 'required',
                'file' => 'required|mimes:pdf|max:2048',
              //  'description' => 'required',
              //  'published_date' => 'required|date_format:Y-m-d'
            ],
            [
               'death_name.required'=> 'Death Person Name is required',
               'death_name.regex'=> 'Death Person Name must be only text',
               'father_name.required'=> 'Father Name is required',
               'father_name.regex'=> 'Father Name must be only text',
               'grandfather_name.required'=> 'Grand Father Name is required',
               'grandfather_name.regex'=> 'Grand Father Name must be only text',
               'address.required'=> 'Address is required',
              // 'bo_account.required'=> 'Benificial Owner Account is required',
              // 'file_name.required'=> 'File Name is required',
               'file.max'=> 'File must be less than 2MB',
               'file.required'=> 'File is required',
               'file.mimes'=> 'File must be in pdf format.',
              // 'description.required' => 'Description is required',
             //  'published_date.date_format' => 'Published Date must be in Y-m-d (2018-09-23) format'
            ]);
   
            $deathNotice = new DeathNotice();
            $log = new DeathNoticeLog();

             if($request->has('file')){
                $file = Input::file('file');
                if($file->isValid()){
                    $file = $request->file('file');
                    $destination_path = public_path().'/death_notice_files';
                    date_default_timezone_set('Asia/kathmandu');
                    $date = date('Y-m-d_H-i-s');
                    $extension = $file->getClientOriginalExtension();
                    $fileExtension = strtolower($extension);
                    $userID = Session::get('idSession');
                    $files = 'death_notice_'.$userID.'-'.$date;
                    $file_name = pathinfo($files, PATHINFO_FILENAME);
                    $fileWithExtension = $file_name.'.'.$fileExtension;
                  //  $fileName = $fileWithExtension;
                    $finalFileName = str_replace(' ', '_', $fileWithExtension);
                   // echo $finalFileName; die;
                    $file->move($destination_path,$finalFileName);
                  //  $deathNotice->file = $finalFileName;
                    $log->file = $finalFileName;
                }
             }

            date_default_timezone_set('Asia/kathmandu');
            $date = date('Y-m-d H:i:s');
            
            $user_id = Session::get('idSession');
            $user_type = Session::get('loginSession');
            
              if($user_type == 3){
                if($data['bo_account'] == null)
                 return redirect()->back()->with('flash_message_error','BO Account is required');
            }

            $noticeId = DB::table('death_notice')->insertGetId(['user_type' =>$user_type, 'user_id' => $user_id,'name'=>$data['death_name'],'father_name'=>$data['father_name'],'grandfather_name'=>$data['grandfather_name'],'address'=>$data['address'],'bo_account'=>$data['bo_account'],'file_name'=>null,'file'=>$finalFileName,'created_at'=> $date]);

            $log->user_id = $user_id;
            $log->user_type = $user_type;
            $log->name = $data['death_name'];
            $log->father_name = $data['father_name'];
            $log->grandfather_name = $data['grandfather_name'];
            $log->address = $data['address'];
            $log->bo_account = $data['bo_account'];
            $log->file_name = null;
            $log->request = 1;
            $log->notice_id = $noticeId;
            $log->created_at = $date;
       
            $log->save();

            return redirect('/death-notice/view-notice')->with('flash_message_success','Notice Add Approval Request Sent Successfully..');
        } 
        return view('death_notice.notice.add_notice');

        } else {
          return redirect('/death-notice')->with('flash_message_error','Please Login First to access..');
        }
    }

     public function viewNotice(){
        if(Session::has('idSession')){

        $user_id = Session::get('idSession');
        $Notice =  DB::table('death_notice')->orderBy('published_date','DESC')->where('user_id',$user_id)->get();
          return view('death_notice.notice.view_notice')->with(compact('Notice'));
          
        } else {
          return redirect('/death-notice')->with('flash_message_error','Please Login First to access..');
        }
    }

     public function viewAllNotice(){
        if(Session::has('idSession')){

          $notices = DB::table('death_notice')->orderBy('published_date','DESC')->get();
          return view('death_notice.notice.view_all_notice')->with(compact('notices'));
          
        } else {
          return redirect('/death-notice')->with('flash_message_error','Please Login First to access..');
        }
    }

    public function viewAllActiveNotice(){
        if(Session::has('idSession')){
         
          $notices = DB::table('death_notice')->orderBy('published_date','DESC')->where('status',1)->get();
          return view('death_notice.notice.view_all_active_notice')->with(compact('notices'));
          
        } else {
          return redirect('/death-notice')->with('flash_message_error','Please Login First to access..');
        }
    }

     public function viewAllLogs(){
        if(Session::has('idSession')){

          $notices = DB::table('death_notice_log')->orderBy('published_date','DESC')->get();
          return view('death_notice.notice.view_all_logs')->with(compact('notices'));
          
        } else {
          return redirect('/death-notice')->with('flash_message_error','Please Login First to access..');
        }
    }

      public function viewLogs($id){
        if(Session::has('idSession')){

          $user_id = Session::get('idSession');
         
          $notices = DB::table('death_notice_log')->orderBy('published_date','DESC')->where('user_id',$user_id)->where('notice_id',$id)->get();
         
          return view('death_notice.notice.view_logs')->with(compact('notices','id'));
          
        } else {
          return redirect('/death-notice')->with('flash_message_error','Please Login First to access..');
        }
    }

       public function viewAllPendingNotice(){
        if(Session::has('idSession')){
         
          $notices = DB::table('death_notice_log')->orderBy('published_date','DESC')->where('isVerified',0)->get();
          return view('death_notice.notice.view_all_pending_notice')->with(compact('notices'));
          
        } else {
          return redirect('/death-notice')->with('flash_message_error','Please Login First to access..');
        }
    }

    public function editNotice(Request $request,$id=null){

       if(Session::has('idSession')){
             if($request->isMethod('post')){

                $data = $request->all();

               $this->validate($request, [
                'death_name' => 'required|regex:/^[a-zA-Z ]+$/u',
                'father_name' => 'required|regex:/^[a-zA-Z ]+$/u',
                'grandfather_name' => 'required|regex:/^[a-zA-Z ]+$/u',
                'address' => 'required',
               // 'bo_account' => 'required',
               // 'file_name' => 'required',
                'file' => 'mimes:pdf|max:2048',
               // 'description' => 'required',
               // 'published_date' => 'required|date_format:Y-m-d'
            ],
            [
               'death_name.required'=> 'Death Person Name is required',
               'death_name.regex'=> 'Death Person Name must be only text',
               'father_name.required'=> 'Father Name is required',
               'father_name.regex'=> 'Father Name must be only text',
               'grandfather_name.required'=> 'Grand Father Name is required',
               'grandfather_name.regex'=> 'Grand Father Name must be only text',
               'address.required'=> 'Address is required',
             //  'bo_account.required'=> 'Benificial Owner Account is required',
              // 'file_name.required'=> 'File Name is required',
               'file.max'=> 'File must be less than 2MB',
               'file.required'=> 'File is required',
               'file.mimes'=> 'File must be in pdf format.',
              // 'description.required' => 'Description is required',
             //  'published_date.date_format' => 'Published Date must be in Y-m-d (2018-09-23) format'
            ]);

            $log = new DeathNoticeLog();
            
             $user_type = Session::get('loginSession');

            if($user_type == 3){
              if($data['bo_account'] == null)
                 return redirect()->back()->with('flash_message_error','BO Account is required');
            }

            if($request->has('file')){
                $file = Input::file('file');
                if($file->isValid()){
                    $file = $request->file('file');
                    $destination_path = public_path().'/death_notice_files';
                    date_default_timezone_set('Asia/kathmandu');
                    $date = date('Y-m-d_H-i-s');
                    $extension = $file->getClientOriginalExtension();
                    $fileExtension = strtolower($extension);
                    // $files = $file->getClientOriginalName();
                    $userID = Session::get('idSession');
                    $files = 'death_notice_'.$userID.'-'.$id.'_'.$date;
                    $file_name = pathinfo($files, PATHINFO_FILENAME);
                    $fileWithExtension = $file_name.'.'.$fileExtension;
                  //  $fileName = $fileWithExtension;
                    $finalFileName = str_replace(' ', '_', $fileWithExtension);
                    $file->move($destination_path,$finalFileName);
                }

             } else {
                $finalFileName = $data['current_file'];
             }
         
            $user_id = Session::get('idSession');
            $user_type = Session::get('loginSession');

            $log->user_id = $user_id;
            $log->user_type = $user_type;
            $log->name = $data['death_name'];
            $log->father_name = $data['father_name'];
            $log->grandfather_name = $data['grandfather_name'];
            $log->address = $data['address'];
            $log->bo_account = $data['bo_account'];
            $log->file_name = null;
            $log->file = $finalFileName;
            $log->request = 2;
            $log->status = 0;
            $log->isVerified = 0;
            $log->notice_id = $id;
            date_default_timezone_set('Asia/kathmandu');
            $date = date('Y-m-d H:i:s');
            $log->updated_at = $date;
            $log->save();
            return redirect('/death-notice/view-notice')->with('flash_message_success','Notice Update Request Sent Successfully..');
        } 
        
         $noticeDetails = DB::table('death_notice')->where(['id'=>$id])->first();
         return view('death_notice.notice.edit_notice')->with(compact('noticeDetails'));

        } else {
          return redirect('/death-notice')->with('flash_message_error','Please Login First to access..');
        }
    }


    public function deleteNotice($id=null){
       if(Session::has('idSession')){
          if(!empty($id)){

            $deathData = DB::table('death_notice')->where('id',$id)->first();

            $log = new DeathNoticeLog();
          
          /* $user_id = Session::get('idSession');
            $user_type = Session::get('loginSession');*/

            $log->user_id = $deathData->user_id;
            $log->user_type = $deathData->user_type;
            $log->name = $deathData->name;
            $log->father_name = $deathData->father_name;
            $log->grandfather_name =  $deathData->grandfather_name;
            $log->address = $deathData->address;
            $log->bo_account =  $deathData->bo_account;
            $log->file_name = null;
            $log->file = $deathData->file;
         //   $log->description = $deathData->description;
            $log->published_date = $deathData->published_date;
            $log->ending_date = $deathData->ending_date;
            $log->request = 3;
            $log->status = 0;
            $log->isVerified = 0;
            $log->notice_id = $id;
            date_default_timezone_set('Asia/kathmandu');
            $date = date('Y-m-d H:i:s');
            $log->updated_at = $date;
            
            $log->save();
            return redirect('/death-notice/view-notice')->with('flash_message_success','Notice Delete Request Sent Successfully..');
         }
          
        } else {
          return redirect('/death-notice')->with('flash_message_error','Please Login First to access..');
        }
        
    }

      public function verifyInsertNotice(Request $request,$id=null){
   
           if(Session::has('idSession')){
            $userID = Session::get('loggedSession');
           
            $published_date = date('Y-m-d');
            $death_notice_time = DB::table('migrations')->first();
            $notice_duration = $death_notice_time->notice_duration;
            $ending_date = Date('Y-m-d', strtotime("+".$notice_duration." days"));

            date_default_timezone_set('Asia/kathmandu');
            $date = date('Y-m-d H:i:s');

            DB::table('death_notice')->where(['id'=>$id])->update(['published_date'=>$published_date,'ending_date'=>$ending_date,'status'=>1,'verified_by'=>$userID,'updated_at'=>$date]);

            DB::table('death_notice_log')->where(['notice_id'=>$id])->update(['published_date'=>$published_date,'ending_date'=>$ending_date,'status'=>1,'isVerified'=>1,'verified_by'=>$userID,'updated_at'=>$date]);
            return redirect('/death-notice/view-all-pending-notice')->with('flash_message_success','Notice Verified & Activated Successfully...');

          } else {
          return redirect('/death-notice')->with('flash_message_error','Please Login First to access..');
        }
   }

      public function rejectInsertNotice(Request $request,$id=null){

        if(Session::has('idSession')){
            $userID = Session::get('loggedSession');

             date_default_timezone_set('Asia/kathmandu');
             $date = date('Y-m-d H:i:s');

           DB::table('death_notice')->where(['id'=>$id])->update(['status'=> 2,'verified_by'=>$userID,'updated_at'=>$date]);
           DB::table('death_notice_log')->where(['notice_id'=>$id])->update(['status'=> 2,'isVerified'=>1,'verified_by'=>$userID,'updated_at'=>$date]);
           return redirect('/death-notice/view-all-pending-notice')->with('flash_message_success','Notice Rejected Successfully...');

        } else {
          return redirect('/death-notice')->with('flash_message_error','Please Login First to access..');
        }
        
   }

     public function verifyDeleteNotice(Request $request,$id=null){
    
        if(Session::has('idSession')){
          $userID = Session::get('loggedSession');

          date_default_timezone_set('Asia/kathmandu');
          $date = date('Y-m-d H:i:s');

           DB::table('death_notice')->where(['id'=>$id])->update(['status'=>3,'verified_by'=>$userID,'updated_at'=>$date]);
           DB::table('death_notice_log')->where(['notice_id'=>$id,'request'=>3])->update(['status'=> 3,'isVerified'=>1,'verified_by'=>$userID,'updated_at'=>$date]);
           return redirect('/death-notice/view-all-pending-notice')->with('flash_message_success','Notice Verified & Deleted Successfully...');

        } else {
          return redirect('/death-notice')->with('flash_message_error','Please Login First to access..');
        }
        
   }

      public function rejectDeleteNotice(Request $request,$id=null){
         $userID = Session::get('loggedSession');

          date_default_timezone_set('Asia/kathmandu');
          $date = date('Y-m-d H:i:s');

        if(Session::has('idSession')){
           DB::table('death_notice_log')->where(['notice_id'=>$id])->update(['status'=> 2,'isVerified'=>1,'verified_by'=>$userID,'updated_at'=>$date]);
          return redirect('/death-notice/view-all-pending-notice')->with('flash_message_success','Notice Rejected Successfully...');

        } else {
          return redirect('/death-notice')->with('flash_message_error','Please Login First to access..');
        }
       
   }

     public function verifyEditNotice(Request $request,$id=null){
       
          if(Session::has('idSession')){
            $userID = Session::get('loggedSession');

             date_default_timezone_set('Asia/kathmandu');
             $date = date('Y-m-d H:i:s');

            $noticeLog = DB::table('death_notice_log')->where('notice_id',$id)->where('isVerified',0)->where('request',2)->first();
          
             DB::table('death_notice')->where(['id'=>$id])->update(['name' => $noticeLog->name,'father_name'=>$noticeLog->father_name,'grandfather_name'=>$noticeLog->grandfather_name,'address'=>$noticeLog->address,'bo_account'=>$noticeLog->bo_account,'published_date'=>$noticeLog->published_date,'ending_date'=>$noticeLog->ending_date,'file_name'=>null,'file'=> $noticeLog->file,'status'=> 1,'verified_by'=>$userID,'updated_at'=>$date]);

           DB::table('death_notice_log')->where(['notice_id'=>$id])->update(['status'=> 1,'isVerified'=>1,'verified_by'=>$userID,'updated_at'=>$date]);
           return redirect('/death-notice/view-all-pending-notice')->with('flash_message_success','Notice Verified & Updated Successfully...');

        } else {
          return redirect('/death-notice')->with('flash_message_error','Please Login First to access..');
        }
   }

      public function rejectEditNotice(Request $request,$id=null){
         $userID = Session::get('loggedSession');

          date_default_timezone_set('Asia/kathmandu');
          $date = date('Y-m-d H:i:s');

        if(Session::has('idSession')){
           DB::table('death_notice_log')->where(['notice_id'=>$id])->update(['status'=> 2,'isVerified'=>1,'verified_by'=>$userID,'updated_at'=>$date]);
          return redirect('/death-notice/view-all-pending-notice')->with('flash_message_success','Notice Rejected Successfully...');

        } else {
          return redirect('/death-notice')->with('flash_message_error','Please Login First to access..');
        } 
   }
}
