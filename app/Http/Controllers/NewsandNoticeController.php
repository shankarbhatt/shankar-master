<?php

namespace App\Http\Controllers;

use App\NewsandNotice;
use Excel;
use PDF;
use App\Exports\DataExport;

use Response;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class NewsandNoticeController extends Controller
{
    //
     public function addNewsandNotice(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();

            $this->validate($request, [
                'title' => 'required',
                'type' => 'required',
                'file_name' => 'required',
                'description' => 'required_if:type,news',
                'published_date' => 'required|date_format:Y-m-d'
            ],
            [
               'title.required'=> 'Title is required',
               'type.required'=> 'Publication Type is required',
               'file_name.required'=> 'File Name is required',
               'filename.type' => 'File is required',
               'description.type' => 'Description is required',
               'published_date.date_format' => 'Trade Date must be in Y-m-d (2018-09-23) format'
            ]);

            $news = new NewsandNotice();

           if($request->has('file')){
                $file = Input::file('file');
                if($file->isValid()){
                    $file = $request->file('file');
                    $destination_path = public_path().'/news_notice_files';
            
                    $date = date('Y_m_d_H_i_s_');
                    $extension = $file->getClientOriginalExtension();
                    $files = $file->getClientOriginalName();
                    $fileName =$date . $files;
                  //  echo $fileName;die;
                    $file->move($destination_path,$fileName);
                    $news->file = $fileName;
                }
             }

            $news->title = $data['title'];
            $news->type = $data['type'];
            $news->published_date = $data['published_date'];
            $description  = strip_tags(htmlspecialchars_decode($data['description']));
            $news->description = $description;
            $news->file_name = $data['file_name'];
            $news->save();
            return redirect('/admin/view-news-notice')->with('flash_message_success','News & Notice Added Successfully...');
           
        }

        return view('admin.news&notice.add_news_notice');
    }

    public function viewNewsandNotice(){
        $news =  NewsandNotice::orderBy('published_date','DESC')->get();
        return view('admin.news&notice.view_news_notice')->with(compact('news'));
    }

    public function editNewsandNotice(Request $request, $id = null){
        if($request->isMethod('post')){
           $data = $request->all();
           
           $this->validate($request, [
            'title' => 'required',
            'type' => 'required',
            'file_name' => 'required',
            'file' => 'mimes:doc,pdf,docx',
           // 'description' => 'required_if:type,news',
            'published_date' => 'required|date_format:Y-m-d'
        ],
        [
           'title.required'=> 'Title is required',
           'type.required'=> 'Publication Type is required',
           'file_name.required'=> 'File Name is required',
           'file.mimes'=> 'File must be in pdf, doc or docx format.',
          // 'file.type' => 'File is required',
          // 'description.type' => 'Description is required',
           'published_date.date_format' => 'Trade Date must be in Y-m-d (2018-09-23) format'
        ]);

        if($request->has('file')){
            $file = Input::file('file');
            if($file->isValid()){
                 $file = $request->file('file');
                 $destination_path = public_path().'/news_files';
        
                $date = date('Y_m_d_H_i_s_');
                $extension = $file->getClientOriginalExtension();
                $files = $file->getClientOriginalName();
                $fileName = $date . $files;
                $file->move($destination_path,$fileName);
            }
         } else {
            $fileName = $data['current_file'];
         }

           $description  = strip_tags(htmlspecialchars_decode($data['description']));
           NewsandNotice::where(['id'=>$id])->update(['title' => $data['title'], 'description' => $description,
            'file_name' => $data['file_name'],'file'=>$fileName,'published_date'=>$data['published_date']]);
           return redirect('/admin/view-news-notice')->with('flash_message_success','News & Notice Updated Successfully..');
        }
        $newsDetails = NewsandNotice::where(['id'=>$id])->first();
        return view('admin.news&notice.edit_news_notice')->with(compact('newsDetails'));
    }

    public function deleteNewsandNotice($id = null){
        if(!empty($id)){
            $status = 2;
            NewsandNotice::where(['id'=>$id])->update(['status'=>$status]);
            NewsandNotice::where(['id'=>$id])->delete();
            return redirect()->back()->with('flash_message_success','News & Notice Deleted Successfully..');
        }
    }

    public function publishNewsandNotice(Request $request,$id=null){
       
            if(empty($data['status'])){
                $status = 1;
               // $date = date('Y/m/d');
                NewsandNotice::where(['id'=>$id])->update(['status'=>$status]);
            } 
            return redirect('/admin/view-news-notice')->with('flash_message_success','News & Notice Published Successfully...');
    }

    public function unpublishNewsandNotice(Request $request, $id = null){
        if(empty($data['status'])){
            $status = 0;
            NewsandNotice::where(['id'=>$id])->update(['status'=>$status]);
        } 
        return redirect('/admin/view-news-notice')->with('flash_message_success','News & Notice Unpublished Successfully...');

    }

    public function exportExcel(){
        $news_notice = NewsandNotice::get()->toArray();
        Excel::create('news_notice_details',function($excel) use ($news_notice) {
            $excel->sheet('news_notice_details',function($sheet) use ($news_notice) {
                $sheet->fromArray($news_notice);
            });
        })->download('xls');
    }

    public function exportPDF()
    {
      
      /* $news = News::get()->toArray();
       Excel::create('news', function($excel) use ($news) {
        $excel->sheet('allnews', function($sheet) use ($news)
        {
            $sheet->fromArray($news);
        });
       })->download('pdf');*/
    }
}
