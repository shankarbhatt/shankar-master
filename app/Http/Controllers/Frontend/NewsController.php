<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\NewsandNotice;

class NewsController extends Controller
{
    public function index()
    {
        $allNotice = NewsandNotice::orderBy('published_date','DESC')->where('status',1)->where('type','notice')->get();
        $allNewsNotices = NewsandNotice::orderBy('published_date','DESC')->where('status',1)->get();
    	return view('Home.news')->with(compact('allNotice','allNewsNotices'));
    }
 public function show($id)
    {
      $allNews=NewsandNotice::where('id',$id)->first();
    	
    
    	return view('Home.show')->with(compact('allNews'));
    }
}
