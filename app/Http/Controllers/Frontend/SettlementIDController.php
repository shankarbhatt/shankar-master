<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Collection;
use DB;
use App\Settlement;
class SettlementIDController extends Controller
{
   
    public function index(){
        $settlement=Settlement::orderBy('trade_date','DESC')->where('deleted_at',null)->where('status',1)->paginate(10);
        return view('Home.Investor.settlementid')->with(compact('settlement'));
    }
    public function search(){
        $trade_date=$_GET['trade_date'];
        $settlement = DB::table("settlement")->where('trade_date', 'LIKE', '%' . $trade_date . '%')->orderBy('trade_date','DESC')->where('deleted_at',null)->get();
 
    return view('Home.Investor.settlementidsearch',['settlement' => $settlement]);
    
      
    
        }
    }
