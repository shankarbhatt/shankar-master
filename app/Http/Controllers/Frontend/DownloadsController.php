<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Publication;


class DownloadsController extends Controller
{
      public function index(){
       
        return view('Home.demat');
    }
    
     public function showPublication(){
        $publication = Publication::orderBy('created_at','DESC')->where('deleted_at',null)->where('type','publication')->get();
        return view('Home.publication')->with(compact('publication'));
    }
}

