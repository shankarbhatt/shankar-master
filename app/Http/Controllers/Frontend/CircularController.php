<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Downloads;

class Circularcontroller extends Controller
{
    public function index(){
         $allCircular =  Downloads::where('type',2)->orderBy('created_at','ASC')->get();
        return view('Home.Publication.circulars')->with(compact('allCircular'));
    }
}
