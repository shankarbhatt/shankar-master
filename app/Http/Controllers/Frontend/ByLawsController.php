<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Downloads;

class ByLawsController extends Controller
{
    public function index(){
        $allLaws =  Downloads::where('type',1)->orderBy('created_at','ASC')->get();
        return view('Home.Publication.bylaws')->with(compact('allLaws'));
    }
}
