<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Issuer;

class RegisteredCompanyController extends Controller
{
    public function index(){
        $issuer =  Issuer::with('rta')->orderBy('name','ASC')->where('status',1)->get();
        return view('Home.Investor.registered-company')->with(compact('issuer'));
    }
}
