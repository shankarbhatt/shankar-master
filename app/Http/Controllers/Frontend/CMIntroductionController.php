<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

class CMIntroductionController extends Controller
{
    public function index()
    {
    	
    	return view('Home.Clearing-Member.introduction');
    }
}
