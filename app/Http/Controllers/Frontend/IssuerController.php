<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\RTA;
use App\Issuer;

class IssuerController extends Controller
{
  public function index(){
      return view('Home.Issuer.introduction');
  }
  public function company($rta_id){
     
    $data=RTA::where(['rta_id'=>$rta_id])->orderBy('name','ASC')->first();
    $issuer=Issuer::where(['rta_id'=>$rta_id])->orderBy('name','ASC')->get();
   return view('Home.Issuer.company')->with(compact('data','issuer'));
}
}
