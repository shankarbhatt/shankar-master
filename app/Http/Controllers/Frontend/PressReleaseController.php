<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\PressRelease;

class PressReleaseController extends Controller
{
    public function index()
    {
        $allPressRelease = PressRelease::orderBy('published_date','DESC')->where('status',1)->get();
    	return view('Home.pressrelease')->with(compact('allPressRelease'));
    }
    public function show($press_release_id)
    {
        $allPressRelease=PressRelease::where('press_release_id',$press_release_id)->first();
    	
    
    	return view('Home.pressrelease')->with(compact('allPressRelease'));
    }
}

