<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Downloads;

class PublicationController extends Controller
{
    public function index(){
        $allPublication =  Downloads::where('type',3)->orderBy('created_at','ASC')->get();
       return view('Home.publication')->with(compact('$allPublication'));
   }
}

