<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;

class DPIntroductionController extends Controller
{
    public function index()
    {
    	return view('Home.Depository-Participants.introduction');
    }
}
