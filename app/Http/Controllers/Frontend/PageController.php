<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;

class PageController extends Controller
{
    public function show($slug)
    {
        $page = Page::where('slug',$slug)->first();

        $menu = $page->submenu->menu;
        if($slug == 'bod')
        {
            return view('Home.About_Us.board-of-director',['page'=>$page]);
        }
        if($slug == 'managementTeam')
        {
            return view('Home.About_Us.management-team',['page'=>$page]);
        }
        if($slug == 'globalPartner')
        {
            return view('Home.About_Us.global-partner',['page'=>$page]);
        } 
        if($slug == 'frequentlyaskquestion')
        {
            return view('Home.Investor.faq',['page'=>$page]);
        }
        if($slug == 'settlementid')
        {
            return view('Home.Investor.settlementid',['page'=>$page]);
        }
        if($slug == 'settlementidsearch')
        {
            return view(' Home.Investor.settlementidsearch',['page'=>$page]);
        }
       
        if($slug == 'do&dont')
        {
            return view('Home.Investor.dos-dont',['page'=>$page]);
        }
        if($slug == 'listofdp')
         {
             return view('Home.Depository-Participants.numberofdp',['page'=>$page]);
         }
         if($slug == 'searchlistOfDP')
         {  
             return view('Home.Depository-Participants.searchdp',['page'=>$page]);
         }
         if($slug == 'admissionprocedure')
         {  
             return view(' Home.Depository-Participants.admission-procedure',['page'=>$page]);
         }
          if($slug == 'cdsc-tariff')
         {
             return view('Home.Depository-Participants.cdsc-tariff',['page'=>$page]);
         }
         if($slug == 'hardwaresoftwarerequired')
         {
             return view('Home.Depository-Participants.hardwaresoftware',['page'=>$page]);
         }
         if($slug == 'listofrta')
         {
             return view('Home.RTA.rtalist',['page'=>$page]);
         }
         if($slug == 'rtalistsearch')
         {
             return view('Home.RTA.searchrta',['page'=>$page]);
         }
        
         if($slug == 'rtaadmissionprocedure')
         {
             return view('Home.RTA.admission-procedure',['page'=>$page]);
         }
         if($slug == 'documentation')
         {
             return view('Home.RTA.documentationforcooperativeauction',['page'=>$page]);
         }
         if($slug == 'cmintroduction')
         {
             return view('Home.Clearing-Member.introduction',['page'=>$page]);
         }
         if($slug == 'listofclearingmember')
         {
             return view('Home.Clearing-Member.clearingmember',['page'=>$page]);
         }
         if($slug == 'cmsettlementprocedure')
         {
             return view('Home.Clearing-Member.settlement-procedure',['page'=>$page]);
         }
         if($slug == 'issuer')
         {
             return view('Home.Issuer.introduction',['page'=>$page]);
         }
         if($slug == 'isinscript')
         {
             return view('Home.Investor.isinscript',['page'=>$page]);
         }
         if($slug == 'registeredcompany')
         {
             return view('Home.Investor.registered-company',['page'=>$page]);
         }
         if($slug == 'benefits')    
         {
             return view('Home.Issuer.benefits',['page'=>$page]);
         }
         if($slug == 'issueradmissionprocedure')    
         {
             return view('Home.Issuer.admission-procedure',['page'=>$page]);
         }
        if($slug == 'circulars')    
         {
              return view('Home.Publication.circulars',['page'=>$page]);
         }
        if($slug == 'bylaws')    
         {
             return view('Home.Publication.bylaws',['page'=>$page]);
         }
         if($slug == 'publication')    
         {
             return view('Home.publication',['page'=>$page]);
         }
         if($slug == 'demat')    
         {
             return view('Home.demat',['page'=>$page]);
         }
         if($slug == 'events')    
         {
             return view('Home.Investor.investor',['page'=>$page]);
         }
         if($slug == 'events')    
         {
             return view('Home.Investor.investor',['page'=>$page]);
         }
         if($slug == 'company')    
         {
             return view('Home.Issuer.company',['page'=>$page]);
         }
         if($slug == 'registeredbankinmeroshare')    
         {
             return view('Home.Depository-Participants.dpinmeroshare',['page'=>$page]);
         }
         if($slug == 'casbabank')    
         {
             return view('Home.registerbankincasba',['page'=>$page]);
         }
         if($slug == 'deathnotice')    
         {
             return view('Home.Investor.death_notice',['page'=>$page]);
         }
         if($slug == 'deathNoticeSearch')    
         {
             return view('Home.Investor.death_notice_search',['page'=>$page]);
         }
               return view('Home.page.shared_page',['data'=>$page,'menu'=>$menu]);
    }
}
