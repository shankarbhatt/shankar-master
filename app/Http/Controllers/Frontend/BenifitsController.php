<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

class BenifitsController extends Controller
{
    public function index(){
        return view('Home.Issuer.benefits');
    }
}
