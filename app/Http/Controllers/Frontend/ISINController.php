<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\ISIN;
class ISINController extends Controller
{
    public function index(){
        $isin=ISIN::orderBy('description','ASC')->get();
        return view('Home.Investor.isinscript')->with(compact('isin'));
    }
}
