<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu;
use Illuminate\Support\Facades\Input;

class MenuController extends Controller
{
    //
    public function addMenu(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
 
            $this->validate($request, [
                'name' => 'required|regex:/^[a-zA-Z ]+$/u',
                'order' => 'required|numeric|unique:menu|not_in:0'
              ],
              [
                'name.regex' => 'Name must be only text',
                'order.numeric' => 'Order must be only numbers',
                'order.not_in' => 'Order cannot be 0. It must start from 1.'
            ]);

            $order = Input::get('order');

            $menu = new Menu();
            $menu->name = $data['name'];
            $menu->order = $data['order'];
            $menu->save();
            return redirect('/admin/view-menu')->with('flash_message_success','Menu Added Successfully..');
           
        } 

        return view('admin.menu.add_menu');
     }
 
     public function viewMenu(){
         $data = Menu::orderBy('name','ASC')->get();
         return view('admin.menu.view_menu')->with(compact('data'));
     }
 
     public function editMenu(Request $request,$menu_id=null){
         if($request->isMethod('post')){
             $data = $request->all();
 
             $this->validate($request, [
                'name' => 'required|regex:/^[a-zA-Z ]+$/u',
                'order' => 'required|numeric|not_in:0'
              ],
              [
                'name.regex' => 'Name must be only text',
                'order.numeric' => 'Order must be only numbers',
                'order.not_in' => 'Order cannot be 0. It must start from 1.'
            ]);

             $order = Input::get('order');

             Menu::where(['menu_id'=>$menu_id])->update(['name' => $data['name'],'order' => $data['order']]);
             return redirect('/admin/view-menu')->with('flash_message_success','Menu Updated Successfully..');
         }
         $menu = Menu::where(['menu_id'=>$menu_id])->first();
         return view('admin.menu.edit_menu')->with(compact('menu'));
     }
 
     public function deleteMenu($menu_id=null){
         if(!empty($menu_id)){
            $status = 2;
            Menu::where(['menu_id'=>$menu_id])->update(['status'=>$status]);
            Menu::where(['menu_id'=>$menu_id])->delete();
            return redirect()->back()->with('flash_message_success','Menu Deleted Successfully..');
         }
     }

     public function activateMenu(Request $request,$menu_id=null){
       
        if(empty($data['status'])){
            $status = 1;
           // $date = date('Y/m/d');
           Menu::where(['menu_id'=>$menu_id])->update(['status'=>$status]);
        } 
        return redirect('/admin/view-menu')->with('flash_message_success','Menu Activated Successfully...');
    }

     public function deactivateMenu(Request $request,$menu_id=null){
       
        if(empty($data['status'])){
            $status = 0;
           // $date = date('Y/m/d');
           Menu::where(['menu_id'=>$menu_id])->update(['status'=>$status]);
        } 
        return redirect('/admin/view-menu')->with('flash_message_success','Menu Dectivated Successfully...');
    }

}
