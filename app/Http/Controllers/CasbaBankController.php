<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\CasbaBank;

class CasbaBankController extends Controller
{
    //
    public function addBank(Request $request){
     
        if($request->isMethod('post')){
            $data = $request->all();

             $this->validate($request, [
                'bank_id' => 'required|numeric|unique:casba_bank',
                'bank_name' => 'required|regex:/^[a-zA-Z .]+$/u'
            ],
            [
                'bank_id.numeric' => 'Bank ID must be only numbers',
                'bank_name.regex' => 'Bank Name must be only text'
            ]);

            $bank = new CasbaBank();
            $bank->bank_id = $data['bank_id'];
            $bank->bank_name = $data['bank_name'];
            $bank->save();
            return redirect('/admin/view-casba-bank')->with('flash_message_success','Bank Added Successfully..');
        }
         return view('admin.casba_bank.add_casba_bank');
    }

    public function viewBank(){
         $bank = CasbaBank::orderBy('bank_name','ASC')->get();
        return view('admin.casba_bank.view_casba_bank')->with(compact('bank'));
    }

    public function editBank(Request $request,$id=null){
        if($request->isMethod('post')){
            $data = $request->all();

              $this->validate($request, [
                'bank_name' => 'required|regex:/^[a-zA-Z .]+$/u'
            ],
            [
                'bank_name.regex' => 'Bank Name must be only text'
            ]);

            CasbaBank::where(['id'=>$id])->update(['bank_name'=>$data['bank_name']]);
            return redirect('/admin/view-casba-bank')->with('flash_message_success','Bank Updated Successfully..');
        }
        $bank = CasbaBank::where(['id'=>$id])->first();
        return view('admin.casba_bank.edit_casba_bank')->with(compact('bank'));
    }

    public function deleteBank($id=null){
        if(!empty($id)){
            $status = 2;
            CasbaBank::where(['id'=>$id])->update(['status'=>$status]);
            CasbaBank::where(['id'=>$id])->delete();
            return redirect()->back()->with('flash_message_success','Bank Deleted Successfully..');
        }
    }

    public function activateBank(Request $request,$id=null){
       
        if(empty($data['status'])){
            $status = 1;
           // $date = date('Y/m/d');
           CasbaBank::where(['id'=>$id])->update(['status'=>$status]);
        } 
        return redirect('/admin/view-casba-bank')->with('flash_message_success','Bank Activated Successfully...');
}

public function deactivateBank(Request $request,$id=null){
       
    if(empty($data['status'])){
        $status = 0;
       // $date = date('Y/m/d');
       CasbaBank::where(['id'=>$id])->update(['status'=>$status]);
    } 
    return redirect('/admin/view-casba-bank')->with('flash_message_success','Bank Deactivated Successfully...');
}

    public function exportExcel(){
        $bank = CasbaBank::get()->toArray();
        Excel::create('bank_details',function($excel) use ($bank) {
            $excel->sheet('bank_details',function($sheet) use ($bank) {
            $sheet->fromArray($bank);
            });
        })->download('xls');
    }
}
