<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use DB;
use Auth;
use Session; 
use Illuminate\Support\Facades\Hash; 
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Mail;
use App\Mail\CDSCMail;

class UserController extends Controller
{
    //
     public function addDeathNoticeUser(Request $request){

       if($request->isMethod('post')){
           $data = $request->all();

              $this->validate($request, [
                'email'=> 'required|email',
                'user_type'=> 'required',
                'user_id' => 'required',
              ],
              [
                'email.email' => 'Email must be valid',
                'user_type.required' => 'Type is required',
                'user_id.required' => 'Name is required',
            ]);
            
           $chars = "abcdefghjkmnpqrstuvwxyzABCDEFGHIJKMNPQRSTUVWXYZ23456789";
           $randomPassword = substr(str_shuffle($chars), 0, 8 );

           $user = new User();
           
             if($data['user_type'] == 3 || $data['user_type'] == 4){
                 
              $inactiveUser = DB::table('users')->where('user_id',$data['user_id'])->where('status',0)->count();
              $activeUser = DB::table('users')->where('user_id',$data['user_id'])->where('status',1)->count();
              $uniqueUser = DB::table('users')->where('user_id',$data['user_id'])->where('email',$data['email'])->count();
             //echo $inactiveUser;die;
            // echo $activeUser;die;
            
                if($uniqueUser > 0){
                return redirect()->back()->with('flash_message_error','DP/RTA with this Email already exists...');
              }

              if($activeUser > 0 || $inactiveUser > 0){
                 return redirect()->back()->with('flash_message_error','DP/RTA Name already exists...');
              } 
           }
           
           $user->type = $data['user_type'];
           $user->user_id = $data['user_id'];
           $user->email = $data['email'];
           $actual_password = $randomPassword;
           $user->actual_password = $actual_password;
           $hashPass = bcrypt($actual_password);
           $user->password = $hashPass;
           $user->save();
         
           return redirect('/admin/view-death-notice-user')->with('flash_message_success','Death Notice User Added Successfully.');
       } 
     	   $user_type = DB::table('user_type')->pluck("title","id");
         return view('admin.death_notice_user.add_death_notice_user')->with(compact('user_type'));
  
    }

      public function getDP() 
       {        
          $dp = DB::table("dp")->where('status',1)->where('deleted_at',null)->pluck("name","dp_id");
          return json_encode($dp);
        }

      public function getAdmin() 
       {
          $admin = array('name'=>'ADMIN');
          return json_encode($admin);
        }

         public function getRTA() 
         {        
           $rta = DB::table("rta")->where('status',1)->where('deleted_at',null)->pluck("name","rta_id");
           return json_encode($rta);
        }

      public function viewDeathNoticeUser(){
        $users = User::orderBy('type','ASC')->get();
        return view('admin.death_notice_user.view_death_notice_user')->with(compact('users'));
    }
    
     public function viewPassword($actual_password){
        $password = User::where('actual_password',$actual_password)->get();
        return view('admin.death_notice_user.view_death_notice_user')->with(compact('password'));
    }

     public function editDeathNoticeUser(Request $request,$id=null){
        if($request->isMethod('post')){
            $data = $request->all();

           $this->validate($request, [
                'email'=> 'required|email',
              ],
              [
                'email.email' => 'Email must be valid',
            ]);
            
            User::where(['id'=>$id])->update(['email'=>$data['email']]);
            return redirect('/admin/view-death-notice-user')->with('flash_message_success','Death Notice User Updated Successfully..');
        }

        $userDetails = User::where(['id'=>$id])->first();
        return view('admin.death_notice_user.edit_death_notice_user')->with(compact('userDetails'));
    }


     public function deleteDeathNoticeUser($id=null){
         if(!empty($id)){
            $status = 2;
            User::where(['id'=>$id])->update(['status'=>$status]);
            User::where(['id'=>$id])->delete();
             return redirect()->back()->with('flash_message_success','Death Notice User Deleted Successfully..');
         }
     }
     
       public function resetPassword($id){
        $chars = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789";
        $randomPassword = substr(str_shuffle($chars), 0, 8 );

         $actual_password = $randomPassword;
         $hashPass = bcrypt($actual_password);

        User::where(['id'=>$id])->update(['password'=>$hashPass,'actual_password'=>$actual_password]);
        return redirect('/admin/view-death-notice-user')->with('flash_message_success','Password Reset Successfully...');
    }

     public function activateDeathNoticeUser(Request $request,$id=null){
       
        if(empty($data['status'])){
            $status = 1;
           // $date = date('Y/m/d');
           User::where(['id'=>$id])->update(['status'=>$status]);
        } 
        return redirect('/admin/view-death-notice-user')->with('flash_message_success','Death Notice User Activated Successfully...');
    }

      public function deactivateDeathNoticeUser(Request $request,$id=null){
       
        if(empty($data['status'])){
            $status = 0;

           User::where(['id'=>$id])->update(['status'=>$status]);
        } 
        return redirect('/admin/view-death-notice-user')->with('flash_message_success','Death Notice User Deactivated Successfully...');
    }

   public function userLogin(Request $request){

      if($request->isMethod('post')){
        $data = $request->input();

        $this->validate($request, [
            'user_type'=> 'required',
            'user_id' => 'required',
            'uemail'=> 'required|email',
            'upassword' => 'required|min:8',
          ],
          [
            'user_type.required' => 'User Type is required',
            'user_id.required' => 'DP/RTA Name is required',
            'uemail.email' => 'Email must be valid',
            'uemail.required' => 'Email is required',
            'upassword.required' => 'Password is required',
            'upassword.min' => 'Password must be at least 8 characters',
          ]);

          $user_type = $data['user_type'];
          $user_id = $data['user_id'];
          $uemail = $data['uemail'];
          $upassword = $data['upassword'];
          $hashPass = bcrypt($upassword);
       
          $user1 =  User::where('type',$user_type)->where('user_id',$user_id)->where('email',$uemail)->where('status',1)->where('deleted_at',null)->first();
          
          if($user1 == null)
             {
                  return redirect('/death-notice-login')->with('flash_message_error','Invalid Login Credentials. Please Try Again..');
             }

          $user =  User::where('status',1)->where('type',$user_type)->where('user_id',$user_id)->where('email',$uemail)->where('deleted_at',null)->firstOrFail();
     
         // $isPasswordCorrect = Hash::check($upassword, $user->password);
          $isPasswordCorrect = password_verify($upassword, $user->password);
          
           if($user->email != $uemail || !$isPasswordCorrect){
             return redirect('/death-notice-login')->with('flash_message_error','Invalid Login Credentials. Please Try Again..');
          } else {
              Session::put('userSession',$user->email);
              Session::put('loginSession',$user_type);
              Session::put('idSession',$user->user_id);
              Session::put('loggedSession',$user->id);
              Session::put('passwordSession',$user->actual_password);
            
              if($user->actual_password)
                return redirect('/user/password');
              else
                return redirect('/user/dashboard');
          }

      }
       
        return view('death_notice.user_login');
    }

    public function userdashboard(){
        if(Session::has('userSession')){
            return view('death_notice.dashboard');
         /* $pass = Session::get('passwordSession');
          if($pass == null) 
           return view('death_notice.dashboard');
         else
          return redirect('/user/password')->with('flash_message_error','Please Change Password first to add death');*/
        } else {
          return redirect('/death-notice-login')->with('flash_message_error','Please Login First to access..');
        }
  }

    public function userlogout(){
   
      Session::forget('userSession');
      Session::forget('loginSession');
      Session::forget('idSession');
      Session::flush();
   
      return redirect('/death-notice-login');
  }

  public function usersettings(){   
     if(Session::has('userSession')){
         return view('death_notice.user_settings');
          /*$pass = Session::get('passwordSession');
          if($pass == null) 
           return view('death_notice.settings');
         else
          return redirect('/user/password')->with('flash_message_error','Please Change Password first to add death');*/
        } else {
          return redirect('/death-notice-login')->with('flash_message_error','Please Login First to access..');
        }
    
}
 
 
    public function changePassword(){
        if(Session::has('userSession')){
           return view('death_notice.change_password');
        } else {
          return redirect('/death-notice-login')->with('flash_message_error','Please Login First to access..');
        }
  }

  public function userchkPassword(Request $request){
    $data = $request->all();
    $current_password = $data['current_pwd'];
    $check_password = User::first();
    if(Hash::check($current_password,$check_password->password)){
      echo "true"; die;
    } else {
      echo "false"; die;
    }

  }

  public function userupdatePassword(Request $request){
    if(Session::has('userSession')){
           if($request->isMethod('post')){
              $data = $request->all();

              $this->validate($request, [
                'current_pwd' => 'required|min:8',
                'new_pwd' => 'required|min:8|same:confirm_pwd',
                'confirm_pwd' => 'required|min:8'
            ],
            [
                'current_pwd.required' => 'Current Password is required',
                'current_pwd.min' => 'Current Password must be at least 8 characters',
                'new_pwd.required' => 'New Password is required',
                'new_pwd.min' => 'New Password must be at least 8 characters',
                'new_pwd.same' => 'Confirm Password doesnot match',
                'confirm_pwd.required' => 'Confirm Password is required',
                'confirm_pwd.min' => 'Confirm Password must be at least 8 characters'
            ]);

      $id = Session::get('loggedSession');
      $actual_password = Session::get('passwordSession');
      $check_password = User::where(['id' => $id])->first();
   
      $current_password = $data['current_pwd'];

      if(Hash::check($current_password,$check_password->password)){
        $password = bcrypt($data['new_pwd']);
        User::where('id',$id)->update(['password' => $password,'actual_password'=>null]);
         return redirect('/user/dashboard')->with('flash_message_success','Password Updated Successfully!');
      } else {
           if($actual_password)
                return redirect('/user/password')->with('flash_message_error','Incorrect Current Password!');
              else
                return redirect('/user/settings')->with('flash_message_error','Incorrect Current Password!');
      }
    }
  } else {
    return redirect('/death-notice-login')->with('flash_message_error','Please Login First to access..');
    }
  }
}