<?php

namespace App\Http\Controllers;
use App\NewsandNotice;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class NewsandNoticeAPIController extends Controller
{
    protected $request;
    protected $news;
    
    /**
     *
     * @param Request $request
     * @param Product $user
     */
    public function __construct(Request $request,  NewsandNotice $news) {
        $this->request = $request;
        $this->news = $news;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $news = $this->news->all();
        return response()->json(['news_data' => $news,
            'status' => Response::HTTP_OK]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
  /*  public function store(UserRequest $request) {
        $news = $this->request->all();
        $this->news->news_id = $data['news_id'];
        $this->news->news_title = $data['news_title'];
        $this->news->news_description = $data['news_description'];
        $this->news->save();
        
        return response()->json(['status' => Response::HTTP_CREATED]);
    }*/
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   /* public function update($id) {
        $data = $this->request->all();
        
        $news = $this->news->find($id);
        
        $news->news_id = $data['news_id'];
        $news->news_title = $data['news_title'];
        $news->news_description = $data['news_description'];
        $news->save();
        
        return response()->json(['status' => Response::HTTP_OK]);
    }*/
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   /* public function destroy($id) {
        $news = $this->news->find($id);
        $news->delete();
        
        return response()->json(['status' => Response::HTTP_OK]);
    }*/

}
