<?php

namespace App\Http\Controllers;
use App\Downloads;

use Excel;
use Illuminate\Http\Request;
use DB;
use Response;
use Validator;
use Illuminate\Support\Facades\Input;

class DownloadsController extends Controller
{
    public function addDownloads(Request $request){
       
        if($request->isMethod('post')){
            $data = $request->all();
            $downloads = new Downloads();

             $this->validate($request, [
                'title' => 'required',
                'type' => 'required',
                'file_name' => 'required',
                'file' => 'required|mimes:doc,pdf,docx'
            ],
            [
               'title.required'=> 'Title is required',
               'type.required'=> 'Publication Type is required',
               'file_name.required'=> 'File Name is required',
               'file.mimes'=> 'File must be in pdf, doc or docx format.',
            ]);

             if($request->has('file')){
                $file = Input::file('file');
                if($file->isValid()){
                    $file = $request->file('file');
                    $destination_path = public_path().'/downloads_files';
            
                    $date = date('Y_m_d_H_i_s_');
                    $extension = $file->getClientOriginalExtension();
                    $files = $file->getClientOriginalName();
                    $fileName =$date . $files;
                    $file->move($destination_path,$fileName);
                    $downloads->file = $fileName;
                }
             }

            $downloads->title = $data['title'];
            $downloads->type = $data['type'];
            $downloads->file_name = $data['file_name'];
            $downloads->save();
            return redirect('/admin/view-downloads')->with('flash_message_success','Downloads Added Successfully..');
       }
           return view('admin.downloads.add_downloads');
    }

    public function viewDownloads(){
        $downloads = Downloads::orderBy('title','ASC')->get();
        return view('admin.downloads.view_downloads')->with(compact('downloads'));
    }

    public function editDownloads(Request $request,$id=null){
        if($request->isMethod('post')){
            $data = $request->all();

             $this->validate($request, [
                'title' => 'required',
                'type' => 'required',
                'file_name' => 'required',
                'file' => 'mimes:doc,pdf,docx'
            ],
            [
               'title.required'=> 'Title is required',
               'type.required'=> 'Publication Type is required',
               'file_name.required'=> 'File Name is required',
               'file.mimes'=> 'File must be in pdf, doc or docx format.',
            ]);

             if($request->has('file')){
                $file = Input::file('file');
                if($file->isValid()){
                     $file = $request->file('file');
                     $destination_path = public_path().'/downloads_files';
            
                    $date = date('Y_m_d_H_i_s_');
                    $extension = $file->getClientOriginalExtension();
                    $files = $file->getClientOriginalName();
                    $fileName =$date . $files;
                    $file->move($destination_path,$fileName);
                }
             } else {
                $fileName = $data['current_file'];
             }

            Downloads::where(['id'=>$id])->update(['title' => $data['title'],'type'=>$data['type'],
              'file_name'=>$data['file_name'],'file'=> $fileName]);
           return redirect('/admin/view-downloads')->with('flash_message_success','Downloads Updated Successfully..');
        }
        $downloadsDetails = Downloads::where(['id'=>$id])->first();
        return view('admin.downloads.edit_downloads')->with(compact('downloadsDetails'));
    }

    public function deleteDownloads($id=null){
        if(!empty($id)){
             $status = 2;
            Downloads::where(['id'=>$id])->update(['status'=>$status]);
            Downloads::where(['id'=>$id])->delete();
            return redirect()->back()->with('flash_message_success','Downloads Deleted Successfully..');
        }
    }


     public function activateDownloads(Request $request,$id=null){
       
        if(empty($data['status'])){
            $status = 1;
           // $date = date('Y/m/d');
           Downloads::where(['id'=>$id])->update(['status'=>$status]);
        } 
        return redirect('/admin/view-downloads')->with('flash_message_success','Downloads Activated Successfully...');
    }

     public function deactivateDownloads(Request $request,$id=null){
       
        if(empty($data['status'])){
            $status = 0;
           // $date = date('Y/m/d');
           Downloads::where(['id'=>$id])->update(['status'=>$status]);
        } 
        return redirect('/admin/view-downloads')->with('flash_message_success','Downloads Deactivated Successfully...');
    }

    public function exportExcel(){
        $downloads = Downloads::get()->toArray();
        Excel::create('downloads_details',function($excel) use ($downloads) {
            $excel->sheet('downloads_details',function($sheet) use ($downloads) {
                $sheet->fromArray($downloads);
            });
        })->download('xls');
    }
}
