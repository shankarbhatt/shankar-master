<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DailyUpdate;

class DailyUpdateController extends Controller
{
    //
    public function addDailyUpdate(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();

            $this->validate($request, [
                'no_of_bo_account' => 'required|numeric',
                'no_of_demat_shares' => 'required|numeric'
              ],
              [
                'no_of_bo_account.numeric' => 'BO Account must be only numbers',
                'no_of_demat_shares.numeric' => 'Demat Shares must be only numbers'
            ]);
 
            $daily = new DailyUpdate();
            $daily->no_of_bo_account = $data['no_of_bo_account'];
            $daily->no_of_demat_shares = $data['no_of_demat_shares'];
           // $date = date('Y/m/d:h:i:s');
           // $daily->created_at = $date;
            $daily->save();
            return redirect('/admin/view-daily-update')->with('flash_message_success','Record Saved Successfully..');
        } 
        return view('admin.daily_update.add_daily_update');
     }
 
     public function viewDailyUpdate(){
         $data = DailyUpdate::orderBy('id','DESC')->get();
         return view('admin.daily_update.view_daily_update')->with(compact('data'));
     }

     public function editDailyUpdate(Request $request,$id){
        if($request->isMethod('post')){
            $data = $request->all();
        
            $this->validate($request, [
                'no_of_bo_account' => 'required|numeric',
                'no_of_demat_shares' => 'required|numeric'
              ],
              [
                'no_of_bo_account.numeric' => 'BO Account must be only numbers',
                'no_of_demat_shares.numeric' => 'Demat Shares must be only numbers'
            ]);
 
           // $date = date('Y-m-d');
            DailyUpdate::where(['id'=>$id])->update(['no_of_bo_account'=>$data['no_of_bo_account'],'no_of_demat_shares' => $data['no_of_demat_shares']]);
           return redirect('/admin/view-daily-update')->with('flash_message_success','Record Updated Successfully..');
        }
        $daily = DailyUpdate::where(['id'=>$id])->first();
        return view('admin.daily_update.edit_daily_update')->with(compact('daily'));
    }
    
}
