<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SliderImage;
use Image;
use Illuminate\Support\Facades\Input;

class SliderImageController extends Controller
{
    //
    public function addImage(Request $request){
    
        if($request->isMethod('post')){
            $data = $request->all();

            $this->validate($request, [
                'title' => 'required',
                'file' => 'required|image|mimes:jpeg,png,jpg'
             ],
             [
                 'title.required' => 'Title is required',
                 'file.mimes' => 'Image must be jpg, jpeg or png format',
            ]);

            $slider = new SliderImage();

            if($request->has('file')){
                $file = Input::file('file');
                if($file->isValid()){
                    $file = $request->file('file');
                    $destination_path = public_path().'/slider_images';
            
                    $date = date('Y_m_d_H_i_s_');
                    $extension = $file->getClientOriginalExtension();
                    $files = $file->getClientOriginalName();
                    $fileName =$date . $files;
                    $file->move($destination_path,$fileName);
                    $slider->file = $fileName;
                }
             }
            
            $slider->title = $data['title'];
            $slider->description = $data['description'];
            $slider->save();
            return redirect('/admin/view-image')->with('flash_message_success','Image Saved Successfully..');
         
        } 
        return view('admin.slider_image.add_image');
  
}

    public function viewImage(){
        $image = SliderImage::orderBy('title','ASC')->get();
        return view('admin.slider_image.view_image')->with(compact('image'));
    }

    public function editSliderImage(Request $request,$image_id){
        if($request->isMethod('post')) {
            $data = $request->all();

         $this->validate($request, [
                'title' => 'required',
                'file' => 'image|mimes:jpeg,png,jpg'
             ],
             [
                 'title.required' => 'Title is required',
                 'file.mimes' => 'Image must be jpg, jpeg or png format',
            ]);

            if($request->has('file')){
                $file = Input::file('file');
                if($file->isValid()){
                    $file = $request->file('file');
                    $destination_path = public_path().'/slider_images';
                   
                    $date = date('Y_m_d_H_i_s_');
                    $extension = $file->getClientOriginalExtension();
                    $files = $file->getClientOriginalName();
                    $fileName = $date . $files;
                    $file->move($destination_path,$fileName);
                }
            } else {
                $fileName = $data['current_file'];
            }
         
            SliderImage::where(['image_id'=>$image_id])->update(['title'=>$data['title'],'description' => $data['description'],
                'file'=>$fileName]);
            return redirect('/admin/view-image')->with('flash_message_success','Image Updated Successfully...');
        }
        
        $sliderImage = SliderImage::where(['image_id'=>$image_id])->first();
        return view('admin.slider_image.edit_image')->with(compact('sliderImage'));
        
    }

    public function deleteImage($image_id=null){
        if(!empty($image_id)){
            SliderImage::where(['image_id'=>$image_id])->delete();
            return redirect()->back()->with('flash_message_success','Image Deleted Successfully..');
        }
    }
}
