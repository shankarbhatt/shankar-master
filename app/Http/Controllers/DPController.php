<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DP;
use Excel;


class DPController extends Controller
{
    //
    public function addDP(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();

             $this->validate($request, [
                'dp_id' => 'required|digits:5|unique:dp',
                'name' => 'required|regex:/^[a-zA-Z .]+$/u',
                'address' => 'required',
                'phone' => 'required|regex:/^[0-9 ,]+$/u',
                'email' => 'required|email'
              ],
              [
                'dp_id.required' => 'DP ID is required',
                'name.regex' => 'Name must be only text',
                'address.required'=> 'Address is required',
                'phone.regex' => 'Phone must be only numbers',
            ]);

            $dp = new DP();
            
            $dp->dp_id = $data['dp_id'];
            $dp->dp_type = 3;
            $dp->name = $data['name'];
            $dp->address = $data['address'];
            $dp->phone = $data['phone'];
            $dp->email = $data['email'];
            $dp->meroshare = $data['meroshare'];
            $dp->save();
            return redirect('/admin/view-dp')->with('flash_message_success','Depository Participant Added Successfully..');
        }
         return view('admin.dp.add_dp');
    }

    public function viewDP(){
        $dp = DP::orderBy('name','ASC')->get();
       return view('admin.dp.view_dp')->with(compact('dp'));
    }

    public function editDP(Request $request,$id=null){
        if($request->isMethod('post')){
            $data = $request->all();
         $this->validate($request, [
                'name' => 'required|regex:/^[a-zA-Z .]+$/u',
                'address' => 'required',
                'phone' => 'required|regex:/^[0-9 ,]+$/u',
                'email' => 'required|email'
              ],
              [
                'name.regex' => 'Name must be only text',
                'address.required'=> 'Address is required',
                'phone.regex' => 'Phone must be only numbers',
            ]);

            DP::where(['id'=>$id])->update(['name'=>$data['name'],'address'=>$data['address'],'phone'=>$data['phone'],'email'=>$data['email'],'meroshare'=>$data['meroshare']]);
            return redirect('/admin/view-dp')->with('flash_message_success','Depository Participant Updated Successfully..');
        }
        $dpDetails = DP::where(['id'=>$id])->first();
        return view('admin.dp.edit_dp')->with(compact('dpDetails'));
    }

    public function deleteDP($id=null){
        if(!empty($id)){
            $status = 2;
            DP::where(['id'=>$id])->update(['status'=>$status]);
            DP::where(['id'=>$id])->delete();
            return redirect()->back()->with('flash_message_success','Depository Participant Deleted Successfully..');
        }
    }

    public function activateDP(Request $request,$id=null){
       
        if(empty($data['status'])){
            $status = 1;
           // $date = date('Y/m/d');
            DP::where(['id'=>$id])->update(['status'=>$status]);
        } 
        return redirect('/admin/view-dp')->with('flash_message_success','Depository Participant Activated Successfully...');
}

public function deactivateDP(Request $request,$id=null){
       
    if(empty($data['status'])){
        $status = 0;
       // $date = date('Y/m/d');
        DP::where(['id'=>$id])->update(['status'=>$status]);
    } 
    return redirect('/admin/view-dp')->with('flash_message_success','Depository Participant Deactivated Successfully...');
}

public function exportExcel(){
    $dp = DP::get()->toArray();
    Excel::create('dp_details',function($excel) use ($dp) {
        $excel->sheet('dp_details',function($sheet) use ($dp) {
            $sheet->fromArray($dp);
        });
    })->download('xls');
}

}
