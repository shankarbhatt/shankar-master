<?php

namespace App\Http\Controllers;
use App\CDSCTariff;
use Excel;

use Illuminate\Http\Request;

class CDSCTariffController extends Controller
{
    //
    public function addTariff(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $cdsc = new CDSCTariff();

            $this->validate($request, [
                'category'=>'required',
                'title' => 'required|regex:/^[a-zA-Z .]+$/u',
                'amount' => 'required|numeric',
                'cdsc_amount' => 'required|numeric'
            ],
            [
                'category.required' => 'Tariff Category is required',
                'title.regex' => 'Title must be only text',
                'amount.numberic' => 'Amount must be only numbers',
                'cdsc_amount.numberic' => 'CDSC Amount must be only numbers'
            ]);

            $cdsc->category = $data['category'];
            $cdsc->title  = $data['title'];
            $cdsc->amount = $data['amount'];
            $cdsc->cdsc_amount = $data['cdsc_amount'];
            $cdsc->save();
            return redirect('/admin/view-cdsc-tariff')->with('flash_message_success','CDSC Tariff Added Successfully..');
        }
         return view('admin.cdsc_tariff.add_cdsc_tariff');
    }

    public function viewTariff(){
       $cdsc =  CDSCTariff::orderBy('title','DESC')->get();
       return view('admin.cdsc_tariff.view_cdsc_tariff')->with(compact('cdsc'));
    }

    public function editTariff(Request $request,$id=null){
        if($request->isMethod('post')){
            $data = $request->all();
            
             $this->validate($request, [
                'category'=>'required',
                'title' => 'required|regex:/^[a-zA-Z .]+$/u',
                'amount' => 'required|numeric',
                'cdsc_amount' => 'required|numeric'
            ],
            [
                'category.required' => 'Tariff Category is required',
                'title.regex' => 'Title must be only text',
                'amount.numberic' => 'Amount must be only numbers',
                'cdsc_amount.numberic' => 'CDSC Amount must be only numbers'
            ]);

            CDSCTariff::where(['id'=>$id])->update(['category'=>$data['category'],'title'=>$data['title'],'amount'=>$data['amount'],'cdsc_amount'=>$data['cdsc_amount']]);
            return redirect('/admin/view-cdsc-tariff')->with('flash_message_success','CDSC Tariff Updated Successfully..');
        }

        $cdscDetails = CDSCTariff::where(['id'=>$id])->first();
        return view('admin.cdsc_tariff.edit_cdsc_tariff')->with(compact('cdscDetails'));
    }

    public function deleteTariff($id=null){
        if(!empty($id)){
            $status = 2;
            CDSCTariff::where(['id'=>$id])->update(['status'=>$status]);
            CDSCTariff::where(['id'=>$id])->delete();
            return redirect()->back()->with('flash_message_success','CDSC Tariff Deleted Successfully..');
        }
    }

    public function activateTariff($id=null){
       
        if(empty($data['status'])){
            $status = 1;
           // $date = date('Y/m/d');
           CDSCTariff::where(['id'=>$id])->update(['status'=>$status]);
        } 
        return redirect('/admin/view-cdsc-tariff')->with('flash_message_success','CDSC Tariff Activated Successfully...');
   }

   public function deactivateTariff($id=null){
       
    if(empty($data['status'])){
        $status = 0;
       // $date = date('Y/m/d');
       CDSCTariff::where(['id'=>$id])->update(['status'=>$status]);
    } 
    return redirect('/admin/view-cdsc-tariff')->with('flash_message_success','CDSC Tariff Deactivated Successfully...');
}

    public function exportExcel(){
        $cdsc_tariff = CDSCTariff::get()->toArray();
        Excel::create('cdsc_tariff_details',function($excel) use ($cdsc_tariff) {
            $excel->sheet('cdsc_tariff_details',function($sheet) use ($cdsc_tariff) {
                $sheet->fromArray($cdsc_tariff);
            });
        })->download('xls');
    }
}
