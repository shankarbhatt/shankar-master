<?php

namespace App\Http\Controllers;
use App\Settlement;
use Excel;
use DB;

use Illuminate\Http\Request;
use Illuminate\Database\Schema\Blueprint;

class SettlementController extends Controller
{
    //
    public function addSettlement(Request $request){
        
    try{
        if($request->isMethod('post')){
            $data = $request->all();
        
         $this->validate($request, [
                'settlement_id' => 'required|digits:13|unique:settlement',
                'trade_date' => 'required|date_format:Y-m-d|unique:settlement'
            ],
            [
                'settlement_id.digits' => 'Settlement ID must be exactly 13 numbers',
                'trade_date.date_format' => 'Trade Date must be in Y-m-d (2018-09-23) format',
                'trade_date.unique' => 'Trade Date has already been taken'
            ]);

            $settlement = new Settlement();
            $settlement->settlement_id  = $data['settlement_id'];
            $settlement->trade_date = $data['trade_date'];
          // echo $settlement; die;
            $settlement->save();
            return redirect('/admin/view-settlement')->with('flash_message_success','Settlement Added Successfully..');
        }
         return view('admin.settlement.add_settlement');

    } catch(\Illuminate\Database\QueryException $ex){
       dd('Table or Column Not Found'); 
 }
      
}

    public function viewSettlement(){
        try{
        $settlement = Settlement::orderBy('trade_date','DESC')->get();
       return view('admin.settlement.view_settlement')->with(compact('settlement'));

        } catch(\Illuminate\Database\QueryException $ex){
            dd('Table or Column Not Found'); 
      }
    }

    public function editSettlement(Request $request,$id=null){
        try{
        if($request->isMethod('post')){
            $data = $request->all();

              $this->validate($request, [
                'settlement_id' => 'required|digits:13||unique:settlement',
                'trade_date' => 'required|date_format:Y-m-d||unique:settlement'
            ],
            [
                'settlement_id.digits' => 'Settlement ID must be exactly 13 numbers',
                'trade_date.numberic' => 'Trade Date must be in Y-m-d (2018-09-23) format',
                 'trade_date.unique' => 'Trade Date has already been taken'
            ]);

            Settlement::where(['id'=>$id])->update(['settlement_id'=>$data['settlement_id'],'trade_date'=>$data['trade_date']]);
            return redirect('/admin/view-settlement')->with('flash_message_success','Settlement Updated Successfully..');
        }
        $settlementDetails = Settlement::where(['id'=>$id])->first();
        return view('admin.settlement.edit_settlement')->with(compact('settlementDetails'));

    } catch(\Illuminate\Database\QueryException $ex){
        dd('Table or Column Not Found'); 
  }
}


    public function deleteSettlement($id=null){
        try{

        if(!empty($id)){
            $status = 2;
            Settlement::where(['id'=>$id])->update(['status'=>$status]);
            Settlement::where(['id'=>$id])->delete();
            return redirect()->back()->with('flash_message_success','Settlement Deleted Successfully..');

        } 
      } catch(\Illuminate\Database\QueryException $ex){
        dd('Table or Column Not Found'); 
    }
}

    public function activateSettlement(Request $request,$id=null){
        try{
        if(empty($data['status'])){
            $status = 1;
           // $date = date('Y/m/d');
           Settlement::where(['id'=>$id])->update(['status'=>$status]);
        } 
        return redirect('/admin/view-settlement')->with('flash_message_success','Settlement Activated Successfully...');
} catch(\Illuminate\Database\QueryException $ex){
    dd('Table or Column Not Found'); 
}
    }


    public function deactivateSettlement(Request $request,$id=null){
        try{
        if(empty($data['status'])){
            $status = 0;
           // $date = date('Y/m/d');
           Settlement::where(['id'=>$id])->update(['status'=>$status]);
        } 
        return redirect('/admin/view-settlement')->with('flash_message_success','Settlement Deactivated Successfully...');
    } catch(\Illuminate\Database\QueryException $ex){
        dd('Table or Column Not Found'); 
}
    }

    public function exportExcel(){
        try{
        $settlement = Settlement::get()->toArray();
        Excel::create('settlement',function($excel) use ($settlement) {
            $excel->sheet('allSettlement',function($sheet) use ($settlement) {
                $sheet->fromArray($settlement);
            });
        })->download('xls');
    }  catch(\Illuminate\Database\QueryException $ex){
        dd('Table or Column Not Found'); 
}
    }

    public function exportPDF(){
        try{
        $settlement = Settlement::get()->toArray();
        Excel::create('settlement_details',function($excel) use ($settlement) {
            $excel->sheet('settlement_details',function($sheet) use ($settlement) {
                $sheet->fromArray($settlement);
            });
        })->download('xls');
    }  catch(\Illuminate\Database\QueryException $ex){
        dd('Table or Column Not Found'); 
 }
}
}
