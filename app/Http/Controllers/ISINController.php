<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ISIN;
use Excel;

class ISINController extends Controller
{
    //
    public function addISIN(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $isin = new ISIN();

            $this->validate($request, [
                'issuer_id' => 'required',
                'script' => 'required|regex:/^[a-zA-Z]+$/u|unique:isin',
                'type'=>'required',
                'isin_code' => 'required|regex:/^[a-zA-Z0-9]+$/u|unique:isin|size:12'
               // 'remarks' => 
            ],
            [
                'issuer_id.required' => 'Issuer Name is required',
                'script.regex' => 'Script must be only text',
                'type.required' => 'ISIN Type is required',
                'isin_code.size' => 'ISIN Code must be exactly 12 characters'
               // 'remarks' => ''
            ]);

            $isin->issuer_id = $data['issuer_id'];
            $isin->script = $data['script'];
            $isin->isin_code = $data['isin_code'];
            $isin->type = $data['type'];
            $isin->remarks = $data['remarks'];
          
            $isin->save();
            return redirect('/admin/view-isin')->with('flash_message_success','ISIN & Script Added Sucessfully..');
        }
        return view('admin.isin_script.add_isin');
    }

    public function viewISIN(){
        $data = ISIN::with('issuer')->orderBy('isin_code','DESC')->get();
        return view('admin.isin_script.view_isin')->with(compact('data'));
    }

    public function editISIN(Request $request,$id){
        if($request->isMethod('post')){
            $data = $request->all();
        
             $this->validate($request, [
                'issuer_id' => 'required',
                'script' => 'required|regex:/^[a-zA-Z]+$/u',
                'type'=>'required',
                'isin_code' => 'required|regex:/^[a-zA-Z0-9]+$/u|size:12'
               // 'remarks' => 
            ],
            [
                'issuer_id.required' => 'Issuer Name is required',
                'script.regex' => 'Script must be only text',
                'type.required' => 'ISIN Type is required',
                'isin_code.size' => 'ISIN Code must be exactly 12 characters'
               // 'remarks' => ''
            ]);

            ISIN::where(['id'=>$id])->update(['issuer_id'=>$data['issuer_id'],'script' => $data['script'],'isin_code'=>$data['isin_code'],'type'=>$data['type'],'remarks'=>$data['remarks']]);
           return redirect('/admin/view-isin')->with('flash_message_success','ISIN & Script Updated Successfully..');
        }
        $isin = ISIN::where(['id'=>$id])->first();
        return view('admin.isin_script.edit_isin')->with(compact('isin'));
    }
    
    public function deleteISIN($id){
        if(!empty($id)){
            ISIN::where(['id'=>$id])->delete();
            return redirect()->back()->with('flash_message_success','ISIN & Script Deleted Successfully..');
        }
    }

    public function activateISIN(Request $request,$id=null){
       
        if(empty($data['status'])){
            $status = 1;
           // $date = date('Y/m/d');
           ISIN::where(['id'=>$id])->update(['status'=>$status]);
        } 
        return redirect('/admin/view-isin')->with('flash_message_success','ISIN Activated Successfully...');
}

public function deactivateISIN(Request $request,$id=null){
       
    if(empty($data['status'])){
        $status = 0;
       // $date = date('Y/m/d');
       ISIN::where(['id'=>$id])->update(['status'=>$status]);
    } 
    return redirect('/admin/view-isin')->with('flash_message_success','ISIN Deactivated Successfully...');
}

    public function exportExcel(){
        $isin = ISIN::get()->toArray();
        Excel::create('isin_details',function($excel) use ($isin) {
            $excel->sheet('isin_details',function($sheet) use ($isin) {
                $sheet->fromArray($isin);
            });
        })->download('xls');
    }
}
