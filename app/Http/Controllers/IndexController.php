<?php

namespace App\Http\Controllers;

use DB;
use App\NewsandNotice;
use App\PressRelease;
use App\Downloads;
use App\SliderImage;
use App\DailyUpdate;
use App\RTA;
use App\DP;
use App\Issuer;
use App\ISIN;
use App\ClearingMember;
use App\CasbaBank;
use App\Menu;
use App\Submenu;
use App\Page;


use Illuminate\Http\Request;

class IndexController extends Controller
{
	//
	
    public function index(){
    
		$allPhotos = SliderImage::get();

		$allNews = NewsandNotice::orderBy('published_date','DESC')->where('deleted_at',null)->where('status',1)->take(5)->get();

		$allPressRelease = PressRelease::orderBy('published_date','DESC')->where('deleted_at',null)->where('status',1)->take(5)->get();
		$allCircular =  Downloads::orderBy('created_at','DESC')->where('status',1)->where('deleted_at',null)->where('type',2)->take(5)->get();
		$allDailyUpdates = DailyUpdate::orderBy('created_at','DESC')->take(1)->get();
	    $totalDP = DP::where('deleted_at',null)->where('status',1)->get();
		$totalRTA = RTA::where('deleted_at',null)->where('status',1)->get();
		$totalIssuer = Issuer::where('deleted_at',null)->where('status',1)->get();
		$totalCM = ClearingMember::where('deleted_at',null)->where('status',1)->get();
		$totalBank = CasbaBank::where('deleted_at',null)->where('status',1)->get();
		$totalMeroShareDP = DP::where('meroshare','1')->where('deleted_at',null)->where('status',1)->get();
    	return view('Home.index')->with(compact('allNews','allPressRelease','allCircular','allPhotos','allDailyUpdates','totalDP','totalRTA','totalIssuer','totalCM','totalBank','totalMeroShareDP'));
    }
}
