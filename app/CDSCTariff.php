<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CDSCTariff extends Model
{
    //
    use SoftDeletes;
    protected $table = 'cdsc_tariff';
    protected $filable = ['category','title','amount','cdsc_amount','status'];
    protected $dates = ['deleted_at'];
}
