<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CasbaBank extends Model
{
    //
    use SoftDeletes;
    protected $table = 'casba_bank';
    protected $fillable = ['bank_id','bank_name','status'];
    protected $dates = ['deleted_at'];
}
