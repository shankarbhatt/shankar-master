<?php

namespace App;
use App\RTARegCompany;

use Illuminate\Database\Eloquent\Model;

class BookClosure extends Model
{
    //
    protected $table = 'book_closure';
    protected $fillable = ['company_id','last_trade_date','book_closure_start_date','book_closure_end_date','book_closure_purpose','book_closure_final_year'];

    public function rtacompany(){
        return $this->belongsTo(RTARegCompany::class,'company_id');
    }
}
