<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Downloads extends Model
{
    //
    use SoftDeletes;
    protected $table = 'downloads';
    protected $fillable = ['title', 'type','file_name','file','status'];
    protected $dates = ['deleted_at'];
}
