<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewsandNotice extends Model
{
    //
    use SoftDeletes;
    protected $table = 'news_notice';
    protected $fillable = ['title', 'description','file','status','published_date'];
    protected $dates = ['deleted_at'];
}
