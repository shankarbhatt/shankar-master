<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Issuer;
use Illuminate\Database\Eloquent\SoftDeletes;

class RTA extends Model
{
    //
    use SoftDeletes;
    protected $table = 'rta';
    protected $fillable = ['rta_id','name','dp_type','address','phone','email','status'];

    protected $dates = ['deleted_at'];
  /*  public function issuer(){
        return $this->hasMany('App\Issuer','rta_id');
    }*/
}
