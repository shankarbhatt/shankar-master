<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    //
    use SoftDeletes;
    protected $table = 'page';
    protected $fillable = ['menu_id', 'submenu_id', 'title' ,'description'];
    protected $dates = ['deleted_at'];

    public function menu(){
        return $this->belogsTo('App\Menu','menu_id','menu_id');
    }

    public function submenu(){
        return $this->belongsTo('App\Submenu','submenu_id','submenu_id');
    }
}
