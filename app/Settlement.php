<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Settlement extends Model
{
    //
    use SoftDeletes;
    protected $table = 'settlement';
    protected $fillable = ['settlement_id','trade_date','status'];
    protected $dates = ['deleted_at'];
}
