<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Menu;

class Submenu extends Model
{
    //
    use SoftDeletes;
    protected $table = 'submenu';
    protected $fillable = ['menu_id','name','order'];
    protected $dates = ['deleted_at'];

    public function menu(){
        return $this->belongsTo('App\Menu','menu_id','menu_id');
    }
    public function page(){
        return $this->hasOne('App\Page','submenu_id','submenu_id');
    }
}
