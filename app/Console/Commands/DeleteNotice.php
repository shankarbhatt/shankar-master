<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\DeathNotice;
use App\DeathNoticeLog;

class DeleteNotice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Notice:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notice Deleted Successfully';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $current_date = date('Y/m/d');
        $deathnotice =  DeathNotice::where('ending_date', '<', $current_date)->where('deleted_at',null)->get();
        if(!is_null($deathnotice)){
            foreach ($deathnotice as $key) {
                 DeathNotice::where('id',$key->id)->update(['status' => 4]);
                 DeathNoticelog::where('notice_id',$key->id)->update(['status' => 4]);
            }
        }
    }
}
