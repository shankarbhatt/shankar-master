<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClearingMember extends Model
{
    //
    use SoftDeletes;
    protected $table = 'clearing_member';
    protected $fillable = ['name', 'cm_no','address','pool_account','pool_account_dp'];
    protected $dates = ['deleted_at'];
}
