<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SliderImage extends Model
{
    //
    use SoftDeletes;
    protected $table = 'slider_image';
    protected $fillable = ['title','description','file'];
    protected $dates = ['deleted_at'];
}
