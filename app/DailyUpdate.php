<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyUpdate extends Model
{
    //
    protected $table = 'daily_update';
    protected $fillable = ['no_of_bo_account', 'no_of_demat_shares','created_at'];
}
