<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeathNotice extends Model
{
    //
    protected $table = 'death_notice';
    protected $fillable = ['user_id','name','file_name','name','father_name','grandfather_name','address','bo_account','file','published_date','ending_date','status','verified_by'];

}
