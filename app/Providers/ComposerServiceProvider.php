<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Menu;
use \Illuminate\Support\Facades\View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function ($view) {
            $allMenu = Menu::get();
            $view->with('allMenu', $allMenu);
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
