<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Issuer;

class ISIN extends Model
{
    //
    use SoftDeletes;
    protected $table = 'isin';
    protected $fillable = ['script','isin_code','type','remarks','status'];
    protected $dates = ['deleted_at'];

    public function issuer(){
        return $this->belongsTo('App\Issuer','issuer_id','issuer_id');
    }

}
