<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PressRelease extends Model
{
    //
    use SoftDeletes;
    protected $table = 'press_release';
    protected $fillable = ['title', 'description','file','status','published_date'];
    protected $dates = ['deleted_at']; 
}
