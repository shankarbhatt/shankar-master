@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','View Daily Update')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a>  <a href="">Daily Update</a> <a href="" class="current">View All Daily Update</a> </div>
    <h1>View All Daily Updates</h1>
    @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

      @if(Session::has('flash_message_success'))

        <div class="alert alert-success alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_success') !!} </strong>
        </div>

       @endif
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
      <a href="{{ url('/admin/add-daily-update') }}" class="btn btn-success"> <i class="icon-plus icon-white"></i> Add New</a>
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-list"></i></span>
            <h5>All Daily Updates List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>No of BO Account</th>
                  <th>No of Demat Shares</th>
                  <th>Created At</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              @foreach($data as $update)
                <tr class="gradeU">
                  <td>{{ number_format($update->no_of_bo_account) }}</td>
                  <td>{{ number_format($update->no_of_demat_shares) }}</td>
                  <td>{{ $update->created_at }}</td>
                  <td style="width:10%;"><a href="{{ url('/admin/edit-daily-update/'.$update->id ) }}" class=" btn btn-primary btn-mini" title="Edit"><i class="icon icon-edit"></i></a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection