@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Add Daily Update')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">Daily Update</a> <a href="" class="current">Add Daily Update</a> </div>
    <h1>Add Daily Update</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">

        @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif

        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-plus"></i> </span>
            <h5>Add New Daily Update</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ url('/admin/add-daily-update') }}" name="add_daily_update" id="add_daily_update" novalidate="novalidate"> {{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">No. of BO Account</label>
                <div class="controls">
                <input type="text" name="no_of_bo_account" id="no_of_bo_account" placeholder="Enter No. of BO Account">
              </div>
              </div>
              <div class="control-group">
                <label class="control-label">No. of Shares in Demat</label>
                <div class="controls">
                <input type="text" name="no_of_demat_shares" id="no_of_demat_shares" placeholder="Enter No. of Shares in Demat Form">
              </div>
              </div>
              <div class="form-actions">
               <input type="submit" value="Add" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection