@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Edit Issuer')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">Issuer</a> <a href="" class="current">Edit Issuer</a> </div>
    <h1>Edit Issuer</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">

        @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif


        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-edit"></i> </span>
            <h5>Edit Issuer</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ url('/admin/edit-issuer/'.$issuerData->issuer_id) }}" name="add_issuer" id="add_issuer" novalidate="novalidate"> {{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">Name</label>
                <div class="controls">
                <input type="text" name="name" id="name" placeholder="Enter Issuer Name" value="{{ $issuerData->name }}">
                </div>
              </div>
                <div class="control-group">
                <label class="control-label">Address</label>
                <div class="controls">
                <input type="text" name="address" id="address" placeholder="Enter Address" value="{{ $issuerData->address }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Phone No.</label>
                <div class="controls">
                <input type="text" name="phone" id="phone" placeholder="Enter Phone No" value="{{ $issuerData->phone }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Email</label>
                <div class="controls">
                <input type="text" name="email" id="email" placeholder="Enter Email" value="{{ $issuerData->email }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Company Code</label>
                <div class="controls">
                <input type="text" name="company_code" id="company_code" placeholder="Enter Company Code" value="{{ $issuerData->company_code }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Registered No</label>
                <div class="controls">
                <input type="text" name="reg_no" id="reg_no" placeholder="Enter Registered No" value="{{ $issuerData->reg_no }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">PAN No</label>
                <div class="controls">
                <input type="text" name="pan_no" id="pan_no" placeholder="Enter PAN No" value="{{ $issuerData->pan_no }}">
                </div>
              </div>
             <div class="control-group">
                <label class="control-label">Issued Capital</label>
                <div class="controls">
                <input type="text" name="issued_capital" id="issued_capital" placeholder="Enter Issued Capital" value="{{ $issuerData->issued_capital }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Listed Capital</label>
                <div class="controls">
                <input type="text" name="listed_capital" id="listed_capital" placeholder="Enter Listed Capital" value="{{ $issuerData->listed_capital }}">
                </div>
              </div>
              <div class="control-group">
              <label class="control-label">RTA Name</label>
              <div class="controls">
              <?php $rta = DB::table('rta')->where('status',1)->get();?>
                <select name="rta_id" id="rta_id">
                <option selected disabled>Select RTA Name</option>
                  @foreach($rta as $data)
                  <?php if($data->deleted_at==null) { ?>
                    <option value="{{ $data->rta_id }}" @if($issuerData->rta_id=="$data->rta_id") selected @endif >{{ $data->name." "."(".$data->rta_id .")" }}</option>
                  <?php } ?>
                 @endforeach
                </select>
              </div>
            </div>
              <div class="form-actions">
               <input type="submit" value="Update" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
