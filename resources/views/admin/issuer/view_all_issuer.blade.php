@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','View All Issuer')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a>  <a href="">Issuer</a> <a href="" class="current">View All Issuer</a> </div>
    <h1>View All Issuer</h1>

     @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif
   
    @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

      @if(Session::has('flash_message_success'))

        <div class="alert alert-success alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_success') !!} </strong>
        </div>
       @endif
      
  </div>
  <div class="container-fluid">
    <hr>

     <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload File</h4>
        </div>
        <div class="modal-body">
          <form action="{{ url('/admin/issuer/importexcel') }}" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}
          Choose your File : <input type="file" name="file" id="file" class="form-control"> <br> <br>
          <input type="submit" class="btn btn-primary btn-md" style="margin-left: 5px;" value="Import">
        </form>
        </div>
        <div class="modal-footer">
        
        </div>
      </div>
      
    </div>
  </div>
  
    <div class="row-fluid">
      <div class="span12">
      <a href="{{ url('/admin/add-issuer') }}" class="btn btn-success"> <i class="icon-plus icon-white"></i> Add New</a>
      <a href="{{ url('/admin/issuer/exportexcel') }}" class="btn btn-primary" style="float: right;"> <i class="icon-file icon-white"></i> Export to Excel</a>
     <!--  <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#myModal" style="float: right;margin-right: 10px;"><i class="icon-file icon-white"></i> Import Excel File</button> -->
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-list"></i></span>
            <h5>All Issuer List </h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table" style="table-layout: fixed;">
              <thead>
                <tr>
                  <th>RTA Name</th>
                  <th>Issuer Name</th>
                  <th>Address</th>
                  <th>Phone No</th>
                  <th>Email</th>
                  <th>Company Code</th>
                  <th>REG No</th>
                  <th>PAN No</th>
                  <th>Issued Capital</th>
                  <th>Listed Capital</th>
                  <th>Status</th>
                  <th style="max-width:80px; width:80px;word-wrap:break-word;">Action</th>
                </tr>
              </thead>
              <tbody>
              @foreach($data as $issuers)
                <tr class="gradeU" style="table-layout:fixed;">
                  <td style="text-transform: uppercase;word-wrap:break-word;"> <?php if(empty($issuers->rta->name)) echo "No Data"; else echo $issuers->rta->name.' ('.$issuers->rta_id. ')'; ?> </td>
                  <td style="text-transform: uppercase;max-width:100px; width:100px;word-wrap:break-word;">{{ $issuers->name }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $issuers->address }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $issuers->phone }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $issuers->email }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;"><?php if(empty($issuers->company_code)) echo 'No Data'; else echo $issuers->company_code;?></td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $issuers->reg_no }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $issuers->pan_no }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ number_format($issuers->issued_capital) }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ number_format($issuers->listed_capital) }}</td>
                  <td style="max-width:70px; width:70px;word-wrap:break-word;"> <?php if($issuers->status==1) echo 'Active'; else echo 'Inactive';?></td>
                  <td style="max-width:80px; width:80px;word-wrap:break-word;"><a href="{{ url('/admin/edit-issuer/'.$issuers->issuer_id ) }}" class=" btn btn-primary btn-mini" title="Edit"><i class="icon icon-edit"></i></a>
                  </a><a rel="{{ $issuers->issuer_id }}" rel1="delete-issuer" id="delIssuer" <?php /*href="{{ url('/admin/delete-rta/'.$rtaData->rta_id ) }}"*/?> href="javascript:" class="btn btn-danger btn-mini deleteIssuerRecord" title="Delete"><i class="icon icon-trash"></i></a>
                  <?php  if($issuers->status==0) { ?>
                  <a rel="{{ $issuers->issuer_id }}" rel1="activate-issuer" id="actIssuer" class="btn btn-success btn-mini activateIssuerRecord" title="Activate" href="javascript:"><i class="icon icon-thumbs-up"></i></a>
                  <?php } else { ?>
                  <a rel="{{ $issuers->issuer_id }}" rel1="deactivate-issuer" id="deactIssuer" class="btn btn-warning btn-mini deactivateIssuerRecord" title="Deactivate" href="javascript:"><i class="icon icon-thumbs-down"></i></a>
                  <?php } ?>
                  </td>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection