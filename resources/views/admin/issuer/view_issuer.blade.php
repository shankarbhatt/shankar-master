@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','View Issuer')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a>  <a href="">Issuer</a> <a href="" class="current">View Issuer</a> </div>
    <h1>View Issuer</h1>
    
    @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

      @if(Session::has('flash_message_success'))

        <div class="alert alert-success alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_success') !!} </strong>
        </div>
       @endif
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
     
      <h4 style="margin-left:10px;"> RTA Name : {{ $rta->name }} </h4>   
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-list"></i></span>
            <h5>All Issuer List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table" style="table-layout: fixed;">
              <thead>
                <tr>
                  <th>Issuer Name</th>
                  <th>Address</th>
                  <th>Phone No</th>
                  <th>Email</th>
                  <th>Company Code</th>
                  <th>REG No</th>
                  <th>PAN No</th>
                  <th>Issued Capital</th>
                  <th>Listed Capital</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              @foreach($data as $issuers)
                <tr class="gradeU">
                  <td style="text-transform: uppercase;word-wrap:break-word;">{{ $issuers->name }}</td>
                  <td style="max-width:130px; width:130px;word-wrap:break-word;">{{ $issuers->address }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $issuers->phone }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $issuers->email }}</td>
                  <td><?php if(empty($issuers->company_code)) echo 'No Data'; else echo $issuers->company_code;?></td>
                  <td>{{ $issuers->reg_no }}</td>
                  <td>{{ $issuers->pan_no }}</td>
                  <td>{{ number_format($issuers->issued_capital) }}</td>
                  <td>{{ number_format($issuers->listed_capital) }}</td>
                  <td> <?php if($issuers->status==1) echo 'Active'; else echo 'Inactive';?></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection