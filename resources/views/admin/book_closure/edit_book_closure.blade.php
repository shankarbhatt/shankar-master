@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Edit Book Closure')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">Investors</a> <a href="" class="current">Edit Book Closure</a> </div>
    <h1>Edit Book Closure</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-edit"></i> </span>
            <h5>Edit Book Closure</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ url('/admin/edit-book-closure/'.$bookClosureDetails->id) }}" name="add_book_closure" id="add_book_closure " novalidate="novalidate"> {{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">Last Trade Date</label>
                <div class="controls">
                <input type="text" id="last_trade_date" name="last_trade_date" data-date="01-01-2017" data-date-format="yyyy-mm-dd" value="{{ $bookClosureDetails->last_trade_date }}" class="datepicker span11">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Book Closure Start Date</label>
                <div class="controls">
                <input type="text" id="book_closure_start_date" name="book_closure_start_date" data-date="01-01-2017" data-date-format="yyyy-mm-dd" value="{{ $bookClosureDetails->book_closure_start_date }}" class="datepicker span11">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Book Closure End Date</label>
                <div class="controls">
                <input type="text" id="book_closure_end_date" name="book_closure_end_date" data-date="01-01-2017" data-date-format="yyyy-mm-dd" value="{{ $bookClosureDetails->book_closure_end_date }}" class="datepicker span11">
                </div>
              </div>
              <div class="control-group">
              <label class="control-label">Book Closure Purpose</label>
              <div class="controls">
                <select name="book_closure_purpose" id="book_closure_purpose">
                  <option value="right" @if($bookClosureDetails->book_closure_purpose=="right") selected @endif >Right</option>
                  <option value="bonus" @if($bookClosureDetails->book_closure_purpose=="bonus") selected @endif >Bonus</option>
                </select>
              </div>
            </div>
              <div class="control-group">
                <label class="control-label">Book Closure Fiscal Year</label>
                <div class="controls">
                <input type="text" name="book_closure_fiscal_year" id="book_closure_fiscal_year" value="{{ $bookClosureDetails->book_closure_fiscal_year }}">
                </div>
              </div>
              <div class="form-actions">
               <input type="submit" value="Update" class="btn btn-success">
              </div>
            </form>
          </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
<script src="{{ asset('js/backend_js/matrix.form_common.js') }}"></script>
@endsection