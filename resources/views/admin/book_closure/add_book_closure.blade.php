@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Add Book Closure')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">Investor</a> <a href="" class="current">Add Book Closure</a> </div>
    <h1>Add Book Closure</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-plus"></i> </span>
            <h5>Add New Book Closure</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ url('/admin/add-book-closure/'.$company->company_id) }}" name="add_book_closure" id="add_book_closure" novalidate="novalidate"> {{ csrf_field() }}
            <div class="control-group">
                <label class="control-label">Company Name</label>
                <div class="controls">
                <input type="text" style="text-transform: uppercase;" name="company_id" id="company_id" value="{{ $company->company_name }}" disabled>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Last Trade Date</label>
                <div class="controls">
                <input type="text" id="last_trade_date" name="last_trade_date" data-date="01-01-2017" data-date-format="yyyy-mm-dd" value="2018-01-01" class="datepicker span11">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Book Closure Start Date</label>
                <div class="controls">
                <input type="text" id="book_closure_start_date" name="book_closure_start_date" data-date="01-01-2017" data-date-format="yyyy-mm-dd" value="2018-01-01" class="datepicker span11">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Book Closure End Date</label>
                <div class="controls">
                <input type="text" id="book_closure_end_date" name="book_closure_end_date" data-date="01-01-2017" data-date-format="yyyy-mm-dd" value="2018-01-01" class="datepicker span11">
                </div>
              </div>
              <div class="control-group">
              <label class="control-label">Book Closure Purpose</label>
              <div class="controls">
                <select name="book_closure_purpose" id="book_closure_purpose">
                  <option value="right">Right</option>
                  <option value="bonus">Bonus</option>
                </select>
              </div>
            </div>
              <div class="control-group">
                <label class="control-label">Book Closure Fiscal Year</label>
                <div class="controls">
                <input type="text" name="book_closure_fiscal_year" id="book_closure_fiscal_year" placeholder="Enter Book Closure Fiscal Year">
                </div>
              </div>
              <div class="form-actions">
               <input type="submit" value="Add" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
<script src="{{ asset('js/backend_js/matrix.form_common.js') }}"></script>
@endsection