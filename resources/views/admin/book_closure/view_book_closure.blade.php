@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','View Book Closure')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a>  <a href="">Investors</a> <a href="" class="current">View Book Closure</a> </div>
    <h1>View Book Closure</h1>
    @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

      @if(Session::has('flash_message_success'))

        <div class="alert alert-success alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_success') !!} </strong>
        </div>

       @endif
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
      <a href="{{ url('/admin/view-rta-all-company') }}" class="btn btn-info" style="float: right;"> <i class="icon-eye-open icon-white"></i> View All Company</a>
      <a href="{{ url('/admin/view-rta') }}" class="btn btn-inverse" style="float: right;margin-right: 10px;"> <i class="icon-eye-open icon-white"></i> View All RTA</a>
      @foreach($company as $companies)  @endforeach
       <h4 style="margin-left:10px;text-transform: uppercase;"> Company Name : {{ $company->company_name }} </h4>
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-list"></i></span>
            <h5>Book Closure List of {{ $company->company_name }}</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Script</th>
                  <th>ISIN</th>
                  <th>Last Trade Date</th>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th>Book Closure Purpose</th>
                  <th>FY Book Closure</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              @foreach($book as $allBookClosure)
                <tr class="gradeU">
                  <td>{{ $company->company_script}}</td>
                  <td>{{ $company->company_isin}}</td>
                  <td>{{ $allBookClosure->last_trade_date}}</td>
                  <td>{{ $allBookClosure->book_closure_start_date}}</td>
                  <td>{{ $allBookClosure->book_closure_end_date}}</td>
                  <td style="text-transform: uppercase;">{{ $allBookClosure->book_closure_purpose}}</td>
                  <td>{{ $allBookClosure->book_closure_fiscal_year}}</td>
                  <td style="width:13%;"><a href="{{ url('/admin/edit-book-closure/'.$allBookClosure->id ) }}" class=" btn btn-primary btn-mini" title="Edit"><i class="icon icon-edit"></i></a>
                  </a><a rel="{{ $allBookClosure->id }}" rel1="delete-book-closure" id="delBookClosure"  href="javascript:" class="btn btn-danger btn-mini deleteBookClosureRecord" title="Delete"><i class="icon icon-trash"></i></a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection