@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Edit Bank')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">C-ASBA Bank</a> <a href="" class="current">Edit C-ASBA Bank</a> </div>
    <h1>Edit Bank in C-ASBA</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">

         @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif

        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-edit"></i> </span>
            <h5>Edit Bank in C-ASBA</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ url('/admin/edit-casba-bank/'.$bank->id) }}" name="edit_casba_bank" id="edit_casba_bank" novalidate="novalidate"> {{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">Bank ID</label>
                <div class="controls">
                <input type="text" name="bank_id" id="bank_id" value="{{ $bank->bank_id }}" placeholder="">
              </div>
              </div>
              <div class="control-group">
                <label class="control-label">Bank Name</label>
                <div class="controls">
                <input type="text" name="bank_name" id="bank_name" value="{{ $bank->bank_name }}" placeholder="">
              </div>
              </div>
              <div class="form-actions">
               <input type="submit" value="Update" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection