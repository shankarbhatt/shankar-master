@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Add Bank')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">C-ASBA Bank</a> <a href="" class="current">Add C-ASBA Bank</a> </div>
    <h1>Add Bank in C-ASBA</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">

         @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif

        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-plus"></i> </span>
            <h5>Add New Bank in C-ASBA</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ url('/admin/add-casba-bank') }}" name="add_casba_bank" id="add_casba_bank" novalidate="novalidate"> {{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">Bank ID</label>
                <div class="controls">
                <input type="text" name="bank_id" id="bank_id" placeholder="Enter Bank ID">
              </div>
              </div>
              <div class="control-group">
                <label class="control-label">Bank Name</label>
                <div class="controls">
                <input type="text" name="bank_name" id="bank_name" placeholder="Enter Bank Name">
              </div>
              </div>
              <div class="form-actions">
               <input type="submit" value="Add" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection