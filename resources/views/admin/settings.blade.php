@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Settings')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a class="current">Settings</a> </div>
    <h1>Admin Settings</h1>

      @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif

    @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

      @if(Session::has('flash_message_success'))

        <div class="alert alert-success alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_success') !!} </strong>
        </div>

       @endif

  </div>
  <div class="container-fluid"><hr>
   
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-cog"></i> </span>
              <h5>Update Password</h5>
            </div>
            <div class="widget-content nopadding">
              <form class="form-horizontal" method="post" action="{{ url('/admin/update-pwd') }}" name="password_validate" id="password_validate" novalidate="novalidate"> {{ csrf_field() }}
              <div class="control-group">
                  <label class="control-label">Current Password</label>
                  <div class="controls">
                    <input type="password" name="current_pwd" id="current_pwd" placeholder="Current Password" />
                    <span id="chkPwd"></span>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">New Password</label>
                  <div class="controls">
                    <input type="password" name="new_pwd" id="new_pwd" placeholder="New Password"/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Confirm Password</label>
                  <div class="controls">
                    <input type="password" name="confirm_pwd" id="confirm_pwd" placeholder="Confirm Password"/>
                  </div>
                </div>
                <div class="form-actions">
                  <input type="submit" value="Save" class="btn btn-success">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
