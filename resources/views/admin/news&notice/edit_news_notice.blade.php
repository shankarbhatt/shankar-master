@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Edit News & Notice')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">News</a> <a href="" class="current">Edit News</a> </div>
    <h1>Edit News</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        
         @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif

        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-edit"></i> </span>
            <h5>Edit News</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ url('/admin/edit-news-notice/'.$newsDetails->id) }}" enctype="multipart/form-data" name="edit_news" id="edit_news" novalidate="novalidate"> {{ csrf_field() }}
            <div class="control-group">
              <label class="control-label">Type</label>
              <div class="controls">
                <select name="type" id="type">
                <option selected disabled>Select Type</option>
                  <option value="news" @if($newsDetails->type=="news") selected @endif >News</option>
                  <option value="notice"  @if($newsDetails->type=="notice") selected @endif >Notice</option>
                </select>
              </div>
            </div>
              <div class="control-group">
                <label class="control-label">Title</label>
                <div class="controls">
                <input type="text" name="title" id="title" placeholder="Enter News Title" maxlength="120" value="{{ $newsDetails->title }}" >
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Description</label>
                <div class="controls" style="width: 800px;">
                <textarea class="form-control description" name="description" id="description"> {{ $newsDetails->description }} </textarea>
                </div>
              </div>
               <div class="control-group">
                <label class="control-label">File Name</label>
                <div class="controls">
                <input type="text" id="file_name" name="file_name" placeholder="Enter File Name" value="{{ $newsDetails->file_name }}">
                </div>
            </div>
              <div class="control-group">
              <label class="control-label">File</label>
              <div class="controls">
                <input type="file" name="file" id="file" >
                <input type="hidden" name="current_file" value=" {{ $newsDetails->file }} "> <br><span>Old File: {{ $newsDetails->file }} </span>
              </div>
            </div> 
            <div class="control-group">
                <label class="control-label">Published Date</label>
                <div class="controls">
                <input type="text" id="published_date" name="published_date" data-date="01-01-2017" data-date-format="yyyy-mm-dd" value=" {{ $newsDetails->published_date }}" class="datepicker span11">
                </div>
              </div>
              <div class="form-actions">
               <input type="submit" value="Update" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
<script src="{{ asset('js/backend_js/matrix.form_common.js') }}"></script>


 <script>
    tinymce.init({ 
    selector:'textarea.description',
    forced_root_block : false, 
    force_br_newlines : true,
    force_p_newlines : false,
    branding: false,
    verify_html: false,
    
    height : "200",
    width: "750",

  });

</script>

 <!--<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $('#description').ckeditor();
        // $('.textarea').ckeditor(); // if class is prefered.
    </script>
  </script> -->

@endsection