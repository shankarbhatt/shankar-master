@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','View All News & Notices')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a>  <a href="">News & Notice</a> <a href="" class="current">View All News & Notices</a> </div>
    <h1>View All News & Notices</h1>
    
     @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif

    @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

      @if(Session::has('flash_message_success'))

        <div class="alert alert-success alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_success') !!} </strong>
        </div>

       @endif
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
      <a href="{{ url('/admin/add-news-notice') }}" class="btn btn-success"> <i class="icon-plus icon-white"></i> Add New</a>
       <!--<a href="{{ url('/admin/news-notice/exportexcel') }}" class="btn btn-primary" style="float: right;margin-left: 10px;"> <i class="icon-file icon-white"></i> 
         <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#myModal" style="float: right;margin-right: 10px;"><i class="icon-file icon-white"></i> Import Excel File</button>
        Export to EXCEL</a> -->
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-list"></i></span>
            <h5>All News & Notices List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Type</th>
                  <th>Title</th>
                  <th>Description</th>
                  <th>File Name</th>
                  <th>File</th>
                  <th>Published Date</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              @foreach($news as $allnews)
                <tr class="gradeU">
                  <td style="max-width:60px; width:60px;word-wrap:break-word;text-transform: uppercase;">{{ $allnews->type }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $allnews->title }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $allnews->description }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;"> <?php if(empty($allnews->file_name)) echo "Empty"; else echo $allnews->file_name; ?> </td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;"> <?php if(empty($allnews->file)) echo "No File"; else echo $allnews->file; ?> </td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;"> <?php if(empty($allnews->published_date)) echo "No Date"; else echo $allnews->published_date; ?> </td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;"> <?php if($allnews->status==1) echo 'Published'; else echo 'Unpublished';?></td>
                  <td style="max-width:130px; width:130px;word-wrap:break-word;"><a href="{{ url('/admin/edit-news-notice/'.$allnews->id ) }}" class=" btn btn-primary btn-mini" title="Edit"><i class="icon icon-edit"></i></a>
                  </a><a rel="{{ $allnews->id }}" rel1="delete-news-notice" id="delNewsandNotice" <?php /*hredf="{{ url('/admin/delete-news/'.$allnews->news_id ) }}" */?> href="javascript:" class="btn btn-danger btn-mini deleteNewsandNoticeRecord" title="Delete"><i class="icon icon-trash"></i></a>
                  <?php  if($allnews->status==0) { ?>
                  <a rel="{{ $allnews->id }}" rel1="publish-news-notice" id="pubNewsandNotice" class="btn btn-success btn-mini publishNewsandNoticeRecord" title="Publish" href="javascript:"><i class="icon icon-thumbs-up"></i></a>
                  <?php } else { ?>
                  <a rel="{{ $allnews->id }}" rel1="unpublish-news-notice" id="unpubNewsandNotice" class="btn btn-warning btn-mini unpublishNewsandNoticeRecord" title="Unpublish" href="javascript:"><i class="icon icon-thumbs-down"></i></a>
                  <?php } ?>
                  <?php if(!empty($allnews->file)) { ?>
                  <a href="{{ asset('news_notice_files/' . $allnews->file) }}"  target="_blank" class="btn btn-info btn-mini" title="View File"><i class="icon icon-eye-open"></i></a>
                  <a href="{{ asset('news_notice_files/' . $allnews->file) }}" class="btn btn-inverse btn-mini" download="{{ $allnews->file }}" title="Download File"><i class="icon icon-download-alt"></i></a>
                  <?php } ?>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection