@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','View All Depository Participants')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a>  <a href="">Depository Participant</a> <a href="" class="current">View All Depository Participant</a> </div>
    <h1>View All Depository Participants</h1>

     @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif
   
    @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

      @if(Session::has('flash_message_success'))

        <div class="alert alert-success alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_success') !!} </strong>
        </div>

       @endif
  </div>
  <div class="container-fluid">
    <hr>

     <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload File</h4>
        </div>
        <div class="modal-body">
          <form action="{{ url('/admin/dp/importexcel') }}" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}
          Choose your File : <input type="file" name="file" id="file" class="form-control"> <br> <br>
          <input type="submit" class="btn btn-primary btn-md" style="margin-left: 5px;" value="Import">
        </form>
        </div>
        <div class="modal-footer">
        
        </div>
      </div>
      
    </div>
  </div>
  
    <div class="row-fluid">
      <div class="span12">
      <a href="{{ url('/admin/add-dp') }}" class="btn btn-success"> <i class="icon-plus icon-white"></i> Add New</a>
      <a href="{{ url('/admin/dp/exportexcel') }}" class="btn btn-primary" style="float: right;"> <i class="icon-file icon-white"></i> Export to Excel</a>
    <!--   <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#myModal" style="float: right;margin-right: 10px;"><i class="icon-file icon-white"></i> Import Excel File</button> -->
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-list"></i></span>
            <h5>All Depository Participants List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>DP ID</th>
                  <th>Name</th>
                  <th>Address</th>
                  <th>Phone No</th>
                  <th>Email</th>
                  <th>Is Meroshare</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              @foreach($dp as $dpData)
                <tr class="gradeU">
                  <td >{{ $dpData->dp_id }}</td>
                  <td style="width:20%;text-transform: uppercase;">{{ $dpData->name }}</td>
                  <td style="max-width:200px; width:200px;word-wrap:break-word;text-transform: uppercase;">{{ $dpData->address }}</td>
                  <td>{{ $dpData->phone }}</td>
                  <td>{{ $dpData->email }}</td>
                  <td> <?php if($dpData->meroshare==1) echo 'Yes'; else echo 'No';?></td>
                  <td> <?php if($dpData->status==1) echo 'Active'; else echo 'Inactive';?></td>
                  <td style="width:15%;"><a href="{{url('/admin/edit-dp/'.$dpData->id ) }}" class=" btn btn-primary btn-mini" title="Edit"><i class="icon icon-edit"></i></a>
                  </a><a rel="{{ $dpData->id }}" rel1="delete-dp" id="delDP" href="javascript:" class="btn btn-danger btn-mini deleteDPRecord" title="Delete"><i class="icon icon-trash"></i></a>
                  <?php  if($dpData->status==0) { ?>
                  <a rel="{{ $dpData->id }}" rel1="activate-dp" id="actDP" class="btn btn-success btn-mini activateDPRecord" title="Activate" href="javascript:"><i class="icon icon-thumbs-up"></i></a>
                  <?php } else { ?>
                  <a rel="{{ $dpData->id }}" rel1="deactivate-dp" id="deactDP" class="btn btn-warning btn-mini deactivateDPRecord" title="Deactivate" href="javascript:"><i class="icon icon-thumbs-down"></i></a>
                  <?php } ?>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection