@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Edit Depository Participant')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">Depository Participant</a> <a href="" class="current">Edit Depository Participant</a> </div>
    <h1>Edit Depository Participant</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">

        @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif
    
  
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-edit"></i> </span>
            <h5>Edit {{ $dpDetails->dp_name }} Depository Participant</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{ url('/admin/edit-dp/'.$dpDetails->id) }}" name="edit_dp" id="edit_dp" novalidate="novalidate"> {{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">DP ID</label>
                <div class="controls">
                <input type="text" name="dp_id" id="dp_id" value="{{ $dpDetails->dp_id }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">DP Name</label>
                <div class="controls">
                <input type="text" name="name" id="name" value="{{ $dpDetails->name }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Address</label>
                <div class="controls">
                <input type="text" name="address" id="address" value="{{ $dpDetails->address }}">
                </div>
                <div class="control-group">
                <label class="control-label">Phone No</label>
                <div class="controls">
                <input type="text" name="phone" id="phone" value="{{ $dpDetails->phone }}">
                </div>
                <div class="control-group">
                  <label class="control-label">Email</label>
                  <div class="controls">
                  <input type="text" name="email" id="email" value="{{ $dpDetails->email }}">
                </div>
              </div>
             <!-- <div class="control-group">
              <label class="control-label">Is Mero Share</label>
              <div class="controls">
                <label>
                  <input type="checkbox" id="meroshare" name="meroshare" value="{{ $dpDetails->meroshare }}" @if($dpDetails->meroshare=="1") checked @endif/>
                  <input type="hidden" id="current" name="current" value="{{ $dpDetails->meroshare }}" />
                 </label>
              </div>
            </div> --> <div class="control-group">
              <label class="control-label">Is Meroshare</label>
              <div class="controls">
                <label>
                  <input type="radio" name="meroshare" value="1"  @if($dpDetails->meroshare=="1") checked @endif/>
                  Yes </label>
                <label>
                  <input type="radio" name="meroshare" value="0"   @if($dpDetails->meroshare=="0") checked @endif/>
                  No </label>
              </div>
              </div>
              </div>
              </div>
              <div class="form-actions">
               <input type="submit" value="Update" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
