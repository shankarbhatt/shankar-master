@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Add Depository Participant')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">Depository Participant</a> <a href="" class="current">Add Depository Participants</a> </div>
    <h1>Add Depository Participants</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">

         @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif

        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-plus"></i> </span>
            <h5>Add New Depository Participant</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ url('/admin/add-dp') }}" name="add_dp" id="add_dp" novalidate="novalidate"> {{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">DP ID</label>
                <div class="controls">
                <input type="text" name="dp_id" id="dp_id" placeholder="Enter DP ID">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">DP Name</label>
                <div class="controls">
                <input type="text" name="name" id="name" placeholder="Enter DP Name">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Address</label>
                <div class="controls">
                <input type="text" name="address" id="address" placeholder="Enter Address">
                </div>
                </div>
                <div class="control-group">
                <label class="control-label">Phone No</label>
                <div class="controls">
                <input type="text" name="phone" id="phone" placeholder="Enter Phone No">
                </div>
              </div>
                <div class="control-group">
                <label class="control-label">Email</label>
                <div class="controls">
                <input type="text" name="email" id="email" placeholder="Enter Email">
                </div>
              </div>
            <div class="control-group">
              <label class="control-label">Is Meroshare</label>
              <div class="controls">
                  <label>
                  <input type="radio" name="meroshare" value="1" />
                  Yes </label>
                  <label>
                  <input type="radio" name="meroshare" value="0" checked/>
                  No </label>
              </div>
              </div>
              <div class="form-actions">
               <input type="submit" value="Add" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection