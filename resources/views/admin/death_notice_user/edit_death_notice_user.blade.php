@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Edit Death Notice User')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">Death Notice (User)</a> <a href="" class="current">Edit Death Notice (User)</a> </div>
    <h1>Edit Death Notice User</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">

          @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif

        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-edit"></i> </span>
            <h5>Edit Death Notice User</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ url('/admin/edit-death-notice-user/'.$userDetails->id) }}" name="edit_death_notice_user" id="edit_death_notice_user" novalidate="novalidate"> {{ csrf_field() }}
               <div class="control-group">
                <label class="control-label">Type</label>
                 <?php $type = DB::table('users')->leftJoin('user_type', 'users.type', '=', 'user_type.id')->where('type',$userDetails->type)->get(); ?>
                    @foreach($type as $data)
                    @endforeach
                <div class="controls">
                <input type="text" name="user_type" id="user_type" placeholder="Enter User Type" value="{{ strtoupper($data->title) }}" disabled>
                </div>
              </div>
              
               <div class="control-group">
                <label class="control-label">DP/RTA ID</label>
                <div class="controls">
                <input type="text" name="user_id" id="user_id" placeholder="Enter Full Name" value="{{$userDetails->user_id }} " disabled>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Email</label>
                <div class="controls">
                <input type="text" name="email" id="email" placeholder="Enter Email" value="{{ $userDetails->email }}" >
                </div>
              </div>
            <!--  <div class="control-group">
                <label class="control-label">Password</label>
                <div class="controls">
                <input type="password" name="user_password" id="user_password" placeholder="Enter Password">
                </div>
              </div> -->
              <div class="form-actions">
               <input type="submit" value="Update" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection