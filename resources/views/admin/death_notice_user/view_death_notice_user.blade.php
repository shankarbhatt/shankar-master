@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','View Death Notice User')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a>  <a href="">Death Notice (User)</a> <a href="" class="current">View All Death Notice (User)</a> </div>
    <h1>View All Death Notice Users</h1>
    @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

      @if(Session::has('flash_message_success'))

        <div class="alert alert-success alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_success') !!} </strong>
        </div>

       @endif

  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
      <a href="{{ url('/admin/add-death-notice-user') }}" class="btn btn-success"> <i class="icon-plus icon-white"></i> Add New</a>
     
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-list"></i></span>
            <h5>All Death Notice Users List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Type</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              @foreach($users as $User)
             
                <tr class="gradeU">
                  <td>{{ $User->id }}</td>

                  <?php $type = DB::table('users')->leftJoin('user_type', 'users.type', '=', 'user_type.id')->where('type',$User->type)->get(); ?>
                    @foreach($type as $data)
                    @endforeach
                  <td>{{ strtoupper($data->title) }}</td>
                 <?php if($User->type == 3){ 
                      $type = DB::table('users')->leftJoin('dp', 'users.user_id', '=', 'dp.dp_id')->where('user_id',$User->user_id)->get(); } else if($User->type == 4){ 
                      $type = DB::table('users')->leftJoin('rta', 'users.user_id', '=', 'rta.rta_id')->where('user_id',$User->user_id)->get(); }?>
                    @foreach($type as $data)
                    @endforeach
                  <td> <?php if($User->type == 1) echo "ADMIN"; else if(!empty($data->name)) echo $data->name.' ('.$User->user_id.')'; else echo "No Data"; ?> </td>
                  <td>{{ $User->email }}</td>
                  <td> <?php if($User->status==1) echo 'Active'; else echo 'Inactive';?></td>
                  <td style="width:15%;"><a href="{{url('/admin/edit-death-notice-user/'.$User->id ) }}" class=" btn btn-primary btn-mini" title="Edit"><i class="icon icon-edit"></i></a>
                  
                  <a rel="{{ $User->id }}" rel1="delete-death-notice-user" id="delUser" href="javascript:" class="btn btn-danger btn-mini deleteDeathUserRecord" title="Delete"><i class="icon icon-trash"></i></a>
                  
                   <a rel="{{ $User->actual_password }}" rel1="view-password" id="viewPass" href="javascript:" class="btn btn-success btn-mini viewPasswordRecord" title="View Password" ><i class="icon icon-eye-open"></i></a>

                   <a rel="{{ $User->id }}" rel1="reset-password" id="resetPass" href="javascript:" class="btn btn-inverse btn-mini resetPasswordRecord" title="Reset Password"><i class="icon icon-refresh"></i></a>
                  
                  <?php  if($User->status==0) { ?>
                  <a rel="{{ $User->id }}" rel1="activate-death-notice-user" id="actUser" class="btn btn-success btn-mini activateDeathUserRecord" title="Activate" href="javascript:"><i class="icon icon-thumbs-up"></i></a>
                  <?php } else { ?>
                  <a rel="{{ $User->id }}" rel1="deactivate-death-notice-user" id="deactUser" class="btn btn-warning btn-mini deactivateDeathUserRecord" title="Deactivate" href="javascript:"><i class="icon icon-thumbs-down"></i></a>
                  <?php } ?>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection