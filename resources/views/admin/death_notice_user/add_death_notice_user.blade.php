@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Add Death Notice User')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">Death Notice (User)</a> <a href="" class="current">Add Death Notice (User)</a> </div>
    <h1>Add Death Notice User</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">

    @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif

   @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
            <strong>Whoops!</strong> There were some problems with your input.<br/>
               <strong style="margin-left: 20px;"> - {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-plus"></i> </span>
            <h5>Add New Death Notice User</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ url('/admin/add-death-notice-user') }}" name="add_death_notice_user" id="add_death_notice_user" novalidate="novalidate"> {{ csrf_field() }}
            <div class="control-group">
              <label class="control-label">Type</label>
              <div class="controls">
                <select name="user_type" id="user_type">
                    <option selected disabled>Select User Type</option>
                    @foreach($user_type as $key => $value)
                     <option value="{{ $key }}">{{ strtoupper($value) }}</option>
                    @endforeach
                </select>
              </div>
            </div>
             <div class="control-group">
              <label class="control-label">Name</label>
             <div class="controls">
                <select name="user_id" id="user_id">
                   <option selected disabled>Select DP/RTA Name</option>
                </select>
              </div>
            </div>
              <div class="control-group">
                <label class="control-label">Email</label>
                <div class="controls">
                <input type="text" name="email" id="email" placeholder="Enter Email">
                </div>
              </div>
              <div class="form-actions">
               <input type="submit" value="Add" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')


<script type="text/javascript">
    jQuery(document).ready(function ()
    {
            jQuery('select[name="user_type"]').on('change',function(){
               var ID = jQuery(this).val();
    
                if(ID == 1)
               {
                 jQuery.ajax({
                     url : '/user/getadmin/',
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        console.log(data);
                        jQuery('select[name="use_rid"]').empty();
                        jQuery.each(data, function(key,value){
                          $('select[name="user_id"]').append('<option value="1">'+ value + '</option>');
                        });
                     }
                  });
               }
               else
               {
                  $('select[name="user_id"]').empty();
               }

               if(ID == 3)
               {
                  
                  jQuery.ajax({
                     url : '/user/getdp/',
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        console.log(data);
                        jQuery('select[name="user_id"]').empty();
                        jQuery.each(data, function(key,value){
                          $('select[name="user_id"]').append('<option value="'+ key +'">'+ value + " (" + key + ")"+'</option>');
                        });
                     }
                  });
               }
               else
               {
                  $('select[name="user_id"]').empty();
               }

                  if(ID == 4)
               {
                  
                  jQuery.ajax({
                     url : '/user/getrta/',
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        console.log(data);
                        jQuery('select[name="user_id"]').empty();
                        jQuery.each(data, function(key,value){
                           $('select[name="user_id"]').append('<option value="'+ key +'">'+ value + " (" + key + ")"+'</option>');
                        });
                     }
                  });
               }
               else
               {
                  $('select[name="user_id"]').empty();
               }
            });
    });
    </script>
  
@endsection