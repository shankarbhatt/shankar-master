@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Edit CDSC Tariff')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">CDSC Tariff</a> <a href="" class="current">Edit CDSC Tariff</a> </div>
    <h1>Edit CDSC Tariff</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">

         @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif
   
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-plus"></i> </span>
            <h5>Edit CDSC Tariff</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ url('/admin/edit-cdsc-tariff/'.$cdscDetails->id) }}" name="edit_cdsc_tariff" id="edit_cdsc_tariff" novalidate="novalidate"> {{ csrf_field() }}
            <div class="control-group">
              <label class="control-label">Category</label>
              <div class="controls">
              <?php $category = DB::table('cdsc_tariff_category')->get();?>
                <select name="category" id="category">
                <option selected disabled>Select CDSC Tariff Category</option>
                  @foreach($category as $data)
                  <option value="{{ $data->id }}" @if($cdscDetails->category=="$data->id") selected @endif >{{ $data->title }}</option>
                 @endforeach
                </select>
              </div>
            </div>
                <div class="control-group">
                <label class="control-label">Title</label>
                <div class="controls">
                <input type="text" name="title" id="title" value="{{ $cdscDetails->title }}">
                </div>
                <div class="control-group">
                <label class="control-label">Amount</label>
                <div class="controls">
                <input type="text" name="amount" id="amount" value="{{ $cdscDetails->amount }}">
                </div>
                <div class="control-group">
                <label class="control-label">CDSC Amount</label>
                <div class="controls">
                <input type="text" name="cdsc_amount" id="cdsc_amount" value="{{ $cdscDetails->cdsc_amount }}">
                </div>
              </div>
              </div>
              </div>
              <div class="form-actions">
               <input type="submit" value="Update" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection