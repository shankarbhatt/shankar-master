@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Add CDSC Tariff')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">CDSC Tariff</a> <a href="" class="current">Add CDSC Tariff</a> </div>
    <h1>Add CDSC Tariff</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">

         @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif
   
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-plus"></i> </span>
            <h5>Add CDSC Tariff</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ url('/admin/add-cdsc-tariff') }}" name="add_cdsc_tariff" id="add_cdsc_tariff" novalidate="novalidate"> {{ csrf_field() }}
            <div class="control-group">
              <label class="control-label">Category</label>
              <div class="controls">
              <?php $category = DB::table('cdsc_tariff_category')->get();?>
                <select name="category" id="category">
                <option selected disabled>Select CDSC Tariff Category </option>
                  @foreach($category as $data)
                  <option value="{{ $data->id }}">{{ $data->title }}</option>
                 @endforeach
                </select>
              </div>
            </div>
              <div class="control-group">
                <label class="control-label">Title</label>
                <div class="controls">
                <input type="text" name="title" id="title" placeholder="Title Here">
                </div>
                <div class="control-group">
                <label class="control-label">Amount</label>
                <div class="controls">
                <input type="text" name="amount" id="amount" placeholder="Valid Amount Only">
                </div>
                <div class="control-group">
                <label class="control-label">CDSC Amount</label>
                <div class="controls">
                <input type="text" name="cdsc_amount" id="cdsc_amount" placeholder="Enter Valid CDSC Amount">
                </div>
              </div>
              </div>
              </div>
              <div class="form-actions">
               <input type="submit" value="Add" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection