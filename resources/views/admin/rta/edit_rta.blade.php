@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Edit RTA')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">RTA</a> <a href="" class="current">Edit RTA</a> </div>
    <h1>Edit RTA</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">

          @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif

        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-edit"></i> </span>
            <h5>Edit {{ $rtaDetails->rta_name }} RTA</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ url('/admin/edit-rta/'.$rtaDetails->rta_id) }}" name="edit_rta" id="edit_rta" novalidate="novalidate"> {{ csrf_field() }}
              <div class="control-group">
              <div class="control-group">
                <label class="control-label">RTA ID</label>
                <div class="controls">
                <input type="text" name="rta_id" id="rta_id" value="{{ $rtaDetails->rta_id }}">
                </div>
              </div>
                <label class="control-label">Name</label>
                <div class="controls">
                <input type="text" name="name" id="rname" placeholder="Enter RTA Name" value="{{ $rtaDetails->name }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Address</label>
                <div class="controls">
                <input type="text" name="address" id="address" placeholder="Enter Address" value="{{ $rtaDetails->address }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Phone No.</label>
                <div class="controls">
                <input type="text" name="phone" id="phone" placeholder="Enter Phone No" value="{{ $rtaDetails->phone }}">
                </div>
                <div class="control-group">
                <label class="control-label">Email</label>
                <div class="controls">
                <input type="text" name="email" id="email" placeholder="Enter Email" value="{{ $rtaDetails->email }}">
                </div>
              </div>
              <div class="form-actions">
               <input type="submit" value="Update" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>


@endsection
