@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Edit Page')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">Pages</a> <a href="" class="current">Edit Page</a> </div>
    <h1>Edit Page</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">

        @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif
   
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-edit"></i> </span>
          <h5>Edit {{ $pageDetails->page_title }} Page</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="{{ url('/admin/edit-page/'.$pageDetails->page_id) }}" method="post" enctype="multipart/form-data" class="form-horizontal" name="edit_page" id="edit_page" novalidate="novalidate"> {{ csrf_field() }}
          <div class="control-group">
          <?php $menu = DB::table('menu')->where('status',1)->where('deleted_at',null)->where('menu_id',$pageDetails->menu_id)->get(); ?>
                <label class="control-label">Menu Name</label>
                <div class="controls">
                @foreach($menu as $data)
                <input type="text" name="menu_id" id="menu_id" placeholder="Page Title" value="{{ $data->name.' ('.$pageDetails->menu_id.')' }}" disabled>
                @endforeach
                </div>
            <div class="control-group">
            <?php $submenu = DB::table('submenu')->where('status',1)->where('deleted_at',null)->where('submenu_id',$pageDetails->submenu_id)->get(); ?>
                <label class="control-label">Submenu Name</label>
                <div class="controls">
                @foreach($submenu as $data1)
                <input type="text" name="submenu_id" id="submenu_id" placeholder="Page Title"value="{{ $data1->name.' ('.$pageDetails->submenu_id.')' }}" disabled>
                @endforeach
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Slug</label>
                <div class="controls">
                <input type="text" name="slug" id="slug" placeholder="Enter Page Slug" value="{{ $pageDetails->slug }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Title</label>
                <div class="controls">
                <input type="text" name="title" id="title" placeholder="Page Title" value="{{ $pageDetails->title }}">
                </div>
              </div>
            <div class="control-group">
                <label class="control-label">Description</label>
                <div class="controls" style="width: 800px;">
                <textarea class="form-control description" name="description" id="description" placeholder="Enter Page Description"> {{ $pageDetails->description }} </textarea>
                </div>
              </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Update</button>
            </div>
          </form>
        </div>
      </div>
      </div>
     </div>
    </div>
  </div>
</div>

@endsection

@section('js')

 <!--   <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $('#description').ckeditor();
        // $('.textarea').ckeditor(); // if class is prefered.
    </script> -->

<script type="text/javascript">
  tinymce.init({ 
    selector:'textarea.description',
    forced_root_block : false, 
    force_br_newlines : true,
    force_p_newlines : false,
    branding: false,
    verify_html: false,
    
    height : "200",
    width: "750",

  });
</script>

@endsection