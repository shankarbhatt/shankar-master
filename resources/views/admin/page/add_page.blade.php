@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Add Page')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">Pages</a> <a href="" class="current">Add Page</a> </div>
    <h1>Add Page</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">

         @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif
   
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-plus"></i> </span>
          <h5>Add New Page</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="{{ url('/admin/add-page') }}" method="post" enctype="multipart/form-data" class="form-horizontal" name="add_page" id="add_page" novalidate="novalidate"> {{ csrf_field() }}
          <div class="control-group">
              <label class="control-label">Menu Name</label>
              <div class="controls">
                <select name="menu_id" id="menu_id">
                  <option selected disabled>Select Menu Name</option>
                  @foreach($menu as $key => $value)
                  <option value="{{ $key }}">{{ $value }}</option>
                 @endforeach
                </select>
              </div>
            </div>
             <div class="control-group">
              <label class="control-label">Submenu Name</label>
              <div class="controls">
                <select name="submenu_id" id="submenu_id">
                  <option selected disabled>Select Submenu Name</option>
                </select>
              </div>
            </div>
              <div class="control-group">
                <label class="control-label">Title</label>
                <div class="controls">
                <input type="text" name="title" id="title" placeholder="Enter Page Title">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Slug</label>
                <div class="controls">
                <input type="text" name="slug" id="slug" placeholder="Enter Page Slug">
                </div>
              </div>
            <div class="control-group">
                <label class="control-label">Description</label>
                <div class="controls" style="width: 800px;">
                <textarea class="form-control description" name="description" id="description" placeholder="Enter Page Description"> </textarea>
                </div>
              </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Add</button>
            </div>
          </form>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')


 <!--   <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $('#description').ckeditor();
        // $('.textarea').ckeditor(); // if class is prefered.
    </script> -->

<script type="text/javascript">
  tinymce.init({ 
    selector:'textarea.description',
    forced_root_block : false, 
    force_br_newlines : true,
    force_p_newlines : false,
    branding: false,
    verify_html: false,
    
    height : "200",
    width: "750",

  }); 
  </script>

<script type="text/javascript">
    jQuery(document).ready(function ()
    {
            jQuery('select[name="menu_id"]').on('change',function(){
               var menuID = jQuery(this).val();
               if(menuID)
               {
                  jQuery.ajax({
                     url : '/admin/add-page/getsubmenu/' +menuID,
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        console.log(data);
                        jQuery('select[name="submenu_id"]').empty();
                        jQuery.each(data, function(key,value){
                          $('select[name="submenu_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });
                     }
                  });
               }
               else
               {
                  $('select[name="submenu_id"]').empty();
               }
            });
    });
    </script>

@endsection