@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','View All Press Release')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a>  <a href="">Press Release</a> <a href="" class="current">View All Press Release</a> </div>
    <h1>View All Press Release</h1>

     @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif
   
    @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

      @if(Session::has('flash_message_success'))

        <div class="alert alert-success alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_success') !!} </strong>
        </div>

       @endif
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
      <a href="{{ url('/admin/add-press-release') }}" class="btn btn-success"> <i class="icon-plus icon-white"></i> Add New</a>
     <!-- <a href="{{ url('/admin/press-release/exportexcel') }}" class="btn btn-primary" style="float: right;"> <i class="icon-file icon-white"></i> Export to Excel</a>
       <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#myModal" style="float: right;margin-right: 10px;"><i class="icon-file icon-white"></i> Import Excel File</button> -->
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-list"></i></span>
            <h5>All Press Releases List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table" style="table-layout: fixed;">
              <thead>
                <tr>
                
                  <th>Title</th>
                  <th>Description</th>
                  <th>File Name</th>
                  <th>File</th>
                  <th>Published Date</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              @foreach($pressRelease as $allpressRelease)
                <tr class="gradeU">
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $allpressRelease->title }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $allpressRelease->description }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;"> <?php if(empty($allpressRelease->file_name)) echo "Empty"; else echo $allpressRelease->file_name; ?> </td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;"> <?php if(empty($allpressRelease->file)) echo "No File"; else echo $allpressRelease->file; ?> </td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;"> <?php if(empty($allpressRelease->published_date)) echo "No Date"; else echo $allpressRelease->published_date; ?> </td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;"> <?php  if($allpressRelease->status==1) echo 'Published'; else echo 'Unpublished';?></td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;"><a href="{{ url('/admin/edit-press-release/'.$allpressRelease->press_release_id ) }}" class=" btn btn-primary btn-mini" title="Edit"><i class="icon icon-edit"></i></a>
                  </a><a rel="{{ $allpressRelease->press_release_id }}" rel1="delete-press-release" id="delNotice" <?php /*hredf="{{ url('/admin/delete-news/'.$allnews->news_id ) }}" */?> href="javascript:" class="btn btn-danger btn-mini deletePressRecord" title="Delete"><i class="icon icon-trash"></i></a>
                  <?php  if($allpressRelease->status==0) { ?>
                  <a rel="{{ $allpressRelease->press_release_id }}" rel1="publish-press-release" id="pubPress" class="btn btn-success btn-mini publishPressRecord" title="Publish" href="javascript:"><i class="icon icon-thumbs-up"></i></a>
                  <?php } else { ?>
                  <a rel="{{ $allpressRelease->press_release_id }}" rel1="unpublish-press-release" id="unpubPress" class="btn btn-warning btn-mini unpublishPressRecord" title="Unpublish" href="javascript:"><i class="icon icon-thumbs-down"></i></a>
                  <?php } ?>
                  <?php if(!empty($allpressRelease->file)) { ?>
                  <a href="{{ asset('press_release_files/' . $allpressRelease->file) }}"  target="_blank" class="btn btn-info btn-mini" title="View File"><i class="icon icon-eye-open"></i></a>
                  <a href="{{ asset('press_release_files/' . $allpressRelease->file) }}" class="btn btn-inverse btn-mini" download="{{ $allpressRelease->file }}" title="Download File"><i class="icon icon-download-alt"></i></a>
                  <?php } ?>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection