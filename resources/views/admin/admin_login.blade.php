<!DOCTYPE html>
<html lang="en">
    
<head> <link rel="shortcut icon" type="image/ico" href="{{ asset('images/backend_images/favicon.ico') }}"/>
        <title>Login :: CDSC Admin</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="{{ asset('css/backend_css/bootstrap.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('css/backend_css/bootstrap-responsive.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/backend_css/matrix-login.css') }}" />
        <link href="{{ asset('fonts/backend_fonts/css/font-awesome.css') }}" rel="stylesheet" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

   <!--     <link rel="stylesheet" href="{{ asset('css/backend_css/matrix-style.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/backend_css/select2.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/backend_css/matrix-media.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/backend_css/uniform.css') }}" /> -->

    </head>
    <body>
           <div id="footer" style="position:fixed;bottom:0;width:100%;height:100px;"><h5 style="text-align:center; margin-top:70px;">Copyright&copy; <?php echo date("Y");?>. CDS and Clearing Limited. All Rights Reserved. </h5></div>
        <div id="loginbox"> 

            <form id="loginform" name="loginform" class="form-vertical" method="post" action="{{ url('admin') }}"> {{ csrf_field() }}
				 <div class="control-group normal_text"> <h3><img src="{{ asset('images/backend_images/cdsc_logo.png') }}" alt="Logo" /></h3></div>

                   @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif
                
                 @if(Session::has('flash_message_error'))
       
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                            <strong> {!! session('flash_message_error') !!} </strong>
                    </div>

                 @endif

                   @if(Session::has('flash_message_success'))
       
                     <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                            <strong> {!! session('flash_message_success') !!} </strong>
                     </div>

                    @endif
                
                 <p><h3 style="text-align:center;color:white;">Login to CDSC Admin Panel</h3></p>
                 <div class="control-group">
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"> </i></span><input type="email" name="email" placeholder="Email" required="true" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span><input type="password" name="password" placeholder="Password" required="true" />
                        </div>
                    </div>
                </div>
                
                <div class="form-actions">
                <center><b><span class="pull-none"><input type="submit" class="btn btn-info" value="LOGIN" style="width:50%;font-weight: bold;"/>
               <!-- <span class="pull-left"><a href="{{ url('/admin/password/reset') }}" class="flip-link btn btn-danger" id="lost_password">Forgot password?</a></span>
                <span class="pull-right"><input type="submit" class="flip-link btn btn-success" value="LOGIN" /></span> -->
                </div>
            </form>
        </div>
        
        <script src="{{ asset('js/backend_js/jquery.min.js') }}"></script>  
        <script src="{{ asset('js/backend_js/matrix.login.js') }}"></script> 
        <script src="{{ asset('js/backend_js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/backend_js/jquery.uniform.js') }}"></script> 

      <!--  <script src="{{ asset('js/backend_js/select2.min.js') }}"></script> 
        <script src="{{ asset('js/backend_js/matrix.form_common.js') }}"></script> -->
        
    </body>

</html>
