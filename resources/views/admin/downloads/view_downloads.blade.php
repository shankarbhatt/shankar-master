@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','View All Downloads')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a>  <a href="">Downloads</a> <a href="" class="current">View All Publications</a> </div>
    <h1>View All Downloads</h1>

 @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif
   
     @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif
   
    @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

      @if(Session::has('flash_message_success'))

        <div class="alert alert-success alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_success') !!} </strong>
        </div>

       @endif
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
         <a href="{{ url('/admin/add-downloads') }}" class="btn btn-success"> <i class="icon-plus icon-white"></i> Add New</a>
        <!-- <a href="{{ url('/admin/downloads/exportexcel') }}" class="btn btn-primary" style="float: right;"> <i class="icon-file icon-white"></i> Export to Excel</a>
        <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#myModal" style="float: right;margin-right: 10px;"><i class="icon-file icon-white"></i> Import Excel File</button>
        Export to EXCEL</a> -->
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-list"></i></span>
            <h5>All Downloads List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table"  style="table-layout: fixed;">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Type</th>
                  <th>File Name</th>
                  <th>File</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              @foreach($downloads as $download)
                <tr class="gradeU">
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $download->title }}</td>
                  <?php $type = DB::table('downloads')->leftJoin('downloads_type', 'downloads.type', '=', 'downloads_type.id')->where('type',$download->type)->get(); ?>
                  <?php if(!empty($type)) { ?>
                  @foreach($type as $data)
                  @endforeach
                  <td style="max-width:100px; width:100px;word-wrap:break-word;"> {{ $data->title }}  </td>
                 
                  <?php } else { echo "No Data"; }?>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;"> {{ $download->file_name }} </td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;"> {{ $download->file }} </td>
                  <td style="max-width:50px; width:50px;word-wrap:break-word;"> <?php if($download->status==1) echo 'Active'; else echo 'Inactive';?></td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;"><a href="{{ url('/admin/edit-downloads/'.$download->id ) }}" class=" btn btn-primary btn-mini" title="Edit"><i class="icon icon-edit"></i></a>
                  <a rel="{{ $download->id }}" rel1="delete-downloads" id="delDown" <?php /*href="{{ url('/admin/delete-publication/'.$publications->publication_id ) }}" */?> href="javascript:" class="btn btn-danger btn-mini deleteDownloadsRecord" title="Delete"><i class="icon icon-trash"></i> </a>
                  <?php if(!empty($download->file)) { ?>
                   <a href="{{ asset('downloads_files/' . $download->file) }}"  target="_blank" class="btn btn-info btn-mini" title="View File"><i class="icon icon-eye-open"></i></a>
                   <a href="{{ asset('downloads_files/' . $download->file) }}" class="btn btn-inverse btn-mini" download="{{ $download->file }}" title="Download File"><i class="icon icon-download-alt"></i></a>
                  <?php } ?>
                   <?php  if($download->status==0) { ?>
                  <a rel="{{ $download->id }}" rel1="activate-downloads" id="actDown" class="btn btn-success btn-mini activateDownloadsRecord" title="Activate" href="javascript:"><i class="icon icon-thumbs-up"></i></a>
                  <?php } else { ?>
                  <a rel="{{ $download->id }}" rel1="deactivate-downloads" id="deactDown" class="btn btn-warning btn-mini deactivateDownloadsRecord" title="Deactivate" href="javascript:"><i class="icon icon-thumbs-down"></i></a>
                  <?php } ?>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection