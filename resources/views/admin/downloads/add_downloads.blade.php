@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Add Downloads')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">Downloads</a> <a href="" class="current">Add Downloads</a> </div>
    <h1>Add Downloads</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">

         @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif

      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-plus"></i> </span>
          <h5>Add New Downloads</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="{{ url('/admin/add-downloads') }}" method="post" enctype="multipart/form-data" class="form-horizontal" name="add_downloads" id="add_downloads" novalidate="novalidate"> {{ csrf_field() }}
            <div class="control-group">
                <label class="control-label">Title</label>
                <div class="controls">
                <input type="text" id="title" name="title" placeholder="Enter Title">
                </div>
            </div>
            <div class="control-group">
              <label class="control-label">Type</label>
              <div class="controls">
              <?php $type = DB::table('downloads_type')->get();?>
                <select name="type" id="type">
                <option selected disabled>Select Downloads Type</option>
                  @foreach($type as $data)
                  <option value="{{ $data->id }}">{{ $data->title }}</option>
                 @endforeach
                </select>
              </div>
            </div>
             <div class="control-group">
                <label class="control-label">File Name</label>
                <div class="controls">
                <input type="text" id="file_name" name="file_name" placeholder="Enter File Name">
                </div>
            </div>
            <div class="control-group">
              <label class="control-label">File</label>
              <div class="controls">
                <input type="file" name="file" id="file" />
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Add</button>
            </div>
          </form>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>

@endsection