@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Edit Downloads')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">Downloads</a> <a href="" class="current">Edit Downloads</a> </div>
    <h1>Edit Downloads</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        
         @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif


      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-edit"></i> </span>
          <h5>Edit Downloads</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="{{ url('/admin/edit-downloads/'.$downloadsDetails->id) }}" enctype="multipart/form-data" method="post" class="form-horizontal" name="edit_downloads" id="edit_downloads" novalidate="novalidate"> {{ csrf_field() }}
            <div class="control-group">
                <label class="control-label">Title</label>
                <div class="controls">
                <input type="text" id="title" name="title" placeholder="Enter Title" value="{{ $downloadsDetails->title }}">
                </div>
            </div>
            <div class="control-group">
              <label class="control-label">Type</label>
              <div class="controls">
              <?php $type = DB::table('downloads_type')->get();?>
                <select name="type" id="type">
                <option selected disabled>Select Downloads Type</option>
                  @foreach($type as $data)
                  <option value="{{ $data->id }}" @if($downloadsDetails->type=="$data->id") selected @endif >{{ $data->title }}</option>
                 @endforeach
                </select>
              </div>
            </div>
             <div class="control-group">
                <label class="control-label">File Name</label>
                <div class="controls">
                <input type="text" id="file_name" name="file_name" placeholder="Enter File Name" value="{{ $downloadsDetails->file_name }}">
                </div>
            </div>
            <div class="control-group">
              <label class="control-label">File</label>
              <div class="controls">
                <input type="file" name="file" id="file" >
                <input type="hidden" name="current_file" value=" {{ $downloadsDetails->file }} "> <br><span>Old File: {{ $downloadsDetails->file }} </span>
              </div>
            </div> 
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Update</button>
            </div>
          </form>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>

@endsection