@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Edit Submenu')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">Submenu</a> <a href="" class="current">Edit Submenu</a> </div>
    <h1>Edit Submenu</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">

         @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

      @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif

     
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-plus"></i> </span>
            <h5>Edit Submenu</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ url('/admin/edit-submenu/'.$submenu->submenu_id) }}" name="edit_submenu" id="edit_submenu" novalidate="novalidate"> {{ csrf_field() }}
            <div class="control-group">
              <label class="control-label">Menu Name</label>
              <div class="controls">
              <?php $menu = DB::table('menu')->where('status',1)->get(); ?>
                <select name="menu_id" id="menu_id">
                  <option selected disabled>Select Menu Name</option>
                  @foreach($menu as $data)
                  <?php if($data->deleted_at==null) { ?>
                  <option value="{{ $data->menu_id }}" @if($submenu->menu_id=="$data->menu_id") selected @endif >{{ strtoupper($data->name) }}</option>
                  <?php } ?>
                 @endforeach
                </select>
              </div>
            </div>
             <div class="control-group">
                <label class="control-label">Submenu Name</label>
                <div class="controls">
                <input type="text" name="submenu_name" id="submenu_name" placeholder="Enter Submenu Name" value="{{ $submenu->name }}">
              </div>
              </div>
              <div class="control-group">
                <label class="control-label">Order</label>
                <div class="controls">
                <input type="text" name="order" id="order" placeholder="Enter Number of Order" value="{{ $submenu->order }}">
              </div>
              </div>
              <div class="form-actions">
               <input type="submit" value="Update" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection