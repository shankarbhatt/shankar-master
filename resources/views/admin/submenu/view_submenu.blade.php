@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','View Submenu')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a>  <a href="">Submenu</a> <a href="" class="current">View Submenu</a> </div>
    <h1>View Submenu</h1>

    @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

      @if(Session::has('flash_message_success'))

        <div class="alert alert-success alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_success') !!} </strong>
        </div>
       @endif
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
           <h4 style="margin-left:10px;"> Menu Name : {{ $menu->name }}</h4> 
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-list"></i></span>
            <h5>All Submenu List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Submenu Title</th>
                  <th>Order</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              @foreach($data as $submenu)
                <tr class="gradeU">
                  <td>{{ $submenu->submenu_id }}</td>
                  <td>{{ $submenu->name }}</td>
                  <td>{{ $submenu->order }}</td>
                  <td> <?php if($submenu->status==1) echo 'Active'; else echo 'Inactive';?></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection