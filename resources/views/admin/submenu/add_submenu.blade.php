@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Add Submenu')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">Submenu</a> <a href="" class="current">Add Submenu</a> </div>
    <h1>Add Submenu</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">

          @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

      @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif
     
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-plus"></i> </span>
            <h5>Add New Submenu</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ url('/admin/add-submenu') }}" name="add_submenu" id="add_submenu" novalidate="novalidate"> {{ csrf_field() }}
            <div class="control-group">
              <label class="control-label">Menu Name</label>
              <div class="controls">
              <?php $menu = DB::table('menu')->where('status',1)->get(); ?>
                <select name="menu_id" id="menu_id">
                  <option selected disabled>Select Menu Name</option>
                  @foreach($menu as $data)
                  <?php if($data->deleted_at==null) { ?>
                  <option value="{{ $data->menu_id }}">{{ $data->name }}</option>
                  <?php } ?>
                 @endforeach
                </select>
              </div>
            </div>
              <div class="control-group">
                <label class="control-label">Submenu Name</label>
                <div class="controls">
                <input type="text" name="submenu_name" id="submenu_name" placeholder="Enter Submenu Name">
              </div>
              </div>
              <div class="control-group">
                <label class="control-label">Order</label>
                <div class="controls">
                <input type="text" name="order" id="order" placeholder="Enter Submenu Order">
              </div>
              </div>
              <div class="form-actions">
               <input type="submit" value="Add" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection