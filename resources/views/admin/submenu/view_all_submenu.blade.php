@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','View All Submenu')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a>  <a href="">Submenu</a> <a href="" class="current">View All Submenu</a> </div>
    <h1>View All Submenu</h1>

    @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

      @if(Session::has('flash_message_success'))

        <div class="alert alert-success alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_success') !!} </strong>
        </div>
       @endif
       
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
      <a href="{{ url('/admin/add-submenu') }}" class="btn btn-success"> <i class="icon-plus icon-white"></i> Add New</a>
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-list"></i></span>
            <h5>All Submenu List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Submenu ID</th>
                  <th>Menu Name</th>
                  <th>Submenu Title</th>
                  <th>Order</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              @foreach($data as $submenu)
                <tr class="gradeU">
                  <td>{{ $submenu->submenu_id }}</td>
                  <td>{{ $submenu->menu->name }}</td>
                  <td>{{ $submenu->name }}</td>
                  <td>{{ $submenu->order }}</td>
                  <td> <?php if($submenu->status==1) echo 'Active'; else echo 'Inactive';?></td>
                  <td style="width:10%;"><a href="{{ url('/admin/edit-submenu/'.$submenu->submenu_id ) }}" class=" btn btn-primary btn-mini" title="Edit"><i class="icon icon-edit"></i></a>
                  </a><a rel="{{ $submenu->submenu_id }}" rel1="delete-menu" id="delSubmenu" <?php /*href="{{ url('/admin/delete-rta/'.$rtaData->rta_id ) }}"*/?> href="javascript:" class="btn btn-danger btn-mini deleteSubmenuRecord" title="Delete"><i class="icon icon-trash"></i></a>
                  <?php  if($submenu->status==0) { ?>
                  <a rel="{{  $submenu->submenu_id }}" rel1="activate-submenu" id="actSubmenu" class="btn btn-success btn-mini activateSubmenuRecord" title="Activate" href="javascript:"><i class="icon icon-thumbs-up"></i></a>
                  <?php } else { ?>
                  <a rel="{{  $submenu->submenu_id }}" rel1="deactivate-submenu" id="deactSubmenu" class="btn btn-warning btn-mini deactivateSubmenuRecord" title="Deactivate" href="javascript:"><i class="icon icon-thumbs-down"></i></a>
                  <?php } ?>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection