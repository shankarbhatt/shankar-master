@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','View Menu')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a>  <a href="">Menu</a> <a href="" class="current">View Menu</a> </div>
    <h1>View Menus</h1>

    @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

      @if(Session::has('flash_message_success'))

        <div class="alert alert-success alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_success') !!} </strong>
        </div>
       @endif
       
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
      <a href="{{ url('/admin/add-menu') }}" class="btn btn-success"> <i class="icon-plus icon-white"></i> Add New</a>
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-list"></i></span>
            <h5>All Menu List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Order</th>
                  <th>Status</th>
                  <th>Total Submenu</td>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              @foreach($data as $menu)
                <tr class="gradeU">
                  <td>{{ $menu->menu_id }}</td>
                  <td>{{ $menu->name }}</td>
                  <td>{{ $menu->order }}</td>
                  <td> <?php if($menu->status==1) echo 'Active'; else echo 'Inactive';?></td>
                  <?php $submenu= DB::table('submenu')->where(['menu_id'=> $menu->menu_id])->get(); ?>
                  <td><a href="{{ url('/admin/view-submenu/'.$menu->menu_id ) }}" class="btn btn-primary btn-mini" >{{ count($submenu) }}</td>
                  <td style="width:10%;"><a href="{{ url('/admin/edit-menu/'.$menu->menu_id ) }}" class=" btn btn-primary btn-mini" title="Edit"><i class="icon icon-edit"></i></a>
                  </a><a rel="{{ $menu->menu_id }}" rel1="delete-menu" id="delIssuer" <?php /*href="{{ url('/admin/delete-rta/'.$rtaData->rta_id ) }}"*/?> href="javascript:" class="btn btn-danger btn-mini deleteMenuRecord" title="Delete"><i class="icon icon-trash"></i></a>
                  <?php  if($menu->status==0) { ?>
                  <a rel="{{ $menu->menu_id }}" rel1="activate-menu" id="actMenu" class="btn btn-success btn-mini activateMenuRecord" title="Activate" href="javascript:"><i class="icon icon-thumbs-up"></i></a>
                  <?php } else { ?>
                  <a rel="{{ $menu->menu_id }}" rel1="deactivate-menu" id="deactMenu" class="btn btn-warning btn-mini deactivateMenuRecord" title="Deactivate" href="javascript:"><i class="icon icon-thumbs-down"></i></a>
                  <?php } ?>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection