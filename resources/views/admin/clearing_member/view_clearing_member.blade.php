@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','View All Clearing Members')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a>  <a href="">Clearing Member</a> <a href="" class="current">View All Clearing Members</a> </div>
    <h1>View All Clearing Members</h1>

     @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif
   
    @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

      @if(Session::has('flash_message_success'))

        <div class="alert alert-success alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_success') !!} </strong>
        </div>

       @endif
  </div>
  <div class="container-fluid">
    <hr>

     <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload File</h4>
        </div>
        <div class="modal-body">
          <form action="{{ url('/admin/cm/importexcel') }}" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}
          Choose your File : <input type="file" name="file" id="file" class="form-control"> <br> <br>
          <input type="submit" class="btn btn-primary btn-md" style="margin-left: 5px;" value="Import">
        </form>
        </div>
        <div class="modal-footer">
        
        </div>
      </div>
      
    </div>
  </div>
  
    <div class="row-fluid">
      <div class="span12">
      <a href="{{ url('/admin/add-cm') }}" class="btn btn-success"> <i class="icon-plus icon-white"></i> Add New</a>
      <a href="{{ url('/admin/cm/exportexcel') }}" class="btn btn-primary" style="float: right;"> <i class="icon-file icon-white"></i> Export to Excel</a>
      <!-- <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#myModal" style="float: right;margin-right: 10px;"><i class="icon-file icon-white"></i> Import Excel File</button> -->
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-list"></i></span>
            <h5>All Clearing Members List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Clearing Member(CM)</th>
                  <th>CM No.</th>
                  <th>Address</th>
                  <th>Pool Account</th>
                  <th>Pool Account DP</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              @foreach($clearingMember as $clearingMembers)
                <tr class="gradeU">
                  <td style="width:25%;text-transform: uppercase;">{{ $clearingMembers->name }}</td>
                  <td>{{ $clearingMembers->cm_no }}</td>
                  <td>{{ $clearingMembers->address }}</td>
                  <td>{{ $clearingMembers->pool_account }}</td>
                  <?php $dp = DB::table('clearing_member')->leftJoin('dp', 'clearing_member.pool_account_dp', '=', 'dp.dp_id')->where('cm_id',$clearingMembers->cm_id)->get(); ?>
                 @foreach($dp as $data)
                  <td>{{ $data->name }}</td>
                  @endforeach
                  <td> <?php if($clearingMembers->status==1) echo 'Active'; else echo 'Inactive';?></td>
                  <?php $dp = DB::table('clearing_member')->leftJoin('dp', 'clearing_member.pool_account_dp', '=', 'dp.dp_id')->where('cm_id',$clearingMembers->cm_id)->get(); 
                 ?>
               <!-- @foreach($dp as $data)
                      <td>{{ $data->name }}</td> 
                  @endforeach -->
                  <td style="width:13%;"><a href="{{url('/admin/edit-cm/'.$clearingMembers->cm_id ) }}" class=" btn btn-primary btn-mini" title="Edit"><i class="icon icon-edit"></i></a>
                  </a><a rel="{{ $clearingMembers->cm_id }}" rel1="delete-cm" id="delCM" <?php /*href="{{ url('/admin/delete-cm/'.$clearingMembers->cm_id ) }}"*/?> href="javascript:" class="btn btn-danger btn-mini deleteCMRecord" title="Delete"><i class="icon icon-trash"></i></a>
                  <?php  if($clearingMembers->status==0) { ?>
                  <a rel="{{ $clearingMembers->cm_id }}" rel1="activate-cm" id="actCM" class="btn btn-success btn-mini activateCMRecord" title="Activate" href="javascript:"><i class="icon icon-thumbs-up"></i></a>
                  <?php } else { ?>
                  <a rel="{{ $clearingMembers->cm_id }}" rel1="deactivate-cm" id="deactCM" class="btn btn-warning btn-mini deactivateCMRecord" title="Deactivate" href="javascript:"><i class="icon icon-thumbs-down"></i></a>
                  <?php } ?>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection