@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Edit Clearing Member')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">Clearing Member</a> <a href="" class="current">Edit Clearing Member</a> </div>
    <h1>Edit Clearing Member</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">

         @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif
   
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-edit"></i> </span>
            <h5>Edit {{ $clearingMemberDetails->cm_name}} Clearing Member</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ url('/admin/edit-cm/'.$clearingMemberDetails->cm_id) }}" name="edit_cm" id="edit_cm" novalidate="novalidate"> {{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">CM Name</label>
                <div class="controls">
                <input type="text" name="name" id="name" value="{{ $clearingMemberDetails->name }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">CM No.</label>
                <div class="controls">
                <input type="text" name="cm_no" id="cm_no" value="{{ $clearingMemberDetails->cm_no }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Address</label>
                <div class="controls">
                <input type="text" name="address" id="address" value="{{ $clearingMemberDetails->address }}">
                </div>
                <div class="control-group">
                <label class="control-label">Pool Account</label>
                <div class="controls">
                <input type="text" name="pool_account" id="pool_account" value="{{ $clearingMemberDetails->pool_account }}">
                </div>
             <!--   <div class="control-group">
                <label class="control-label">Pool Account DP</label>
                <div class="controls">
                <input type="text" name="pool_account_dp" id="pool_account_dp" value="{{ $clearingMemberDetails->pool_account_dp }}">
                </div>
              </div> -->
              </div>
              </div>
              <div class="form-actions">
               <input type="submit" value="Update" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
<script src="{{ asset('js/backend_js/matrix.form_common.js') }}"></script>
@endsection