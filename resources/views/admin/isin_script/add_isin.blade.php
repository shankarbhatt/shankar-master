@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Add ISIN & Script')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">ISIN & Script</a> <a href="" class="current">Add ISIN & Script</a> </div>
    <h1>Add ISIN & Script</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">

        @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif

        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-plus"></i> </span>
            <h5>Add New ISIN & Script</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ url('/admin/add-isin') }}" name="add_isin" id="add_isin" novalidate="novalidate"> {{ csrf_field() }}
            <div class="control-group">
              <label class="control-label">Issuer Name</label>
              <div class="controls">
              <?php $issuer = DB::table('issuer')->where('status',1)->get();?>
                <select name="issuer_id" id="issuer_id">
                <option selected disabled>Select Issuer Name</option>
                  @foreach($issuer as $data)
                  <?php if($data->deleted_at==null) { ?>
                  <option value="{{ $data->issuer_id }}">{{ $data->name.' ('.$data->issuer_id.')' }}</option>
                  <?php } ?>
                 @endforeach
                </select>
              </div>
            </div>
            <div class="control-group">
                <label class="control-label">Script</label>
                <div class="controls">
                <input type="text" name="script" id="script" placeholder="Enter Issuer Script">
                </div>
                </div>
              <div class="control-group">
                <label class="control-label">ISIN Code</label>
                <div class="controls">
                <input type="text" name="isin_code" id="isin_code" placeholder="Enter ISIN Code">
                </div>
              </div>
              <div class="control-group">
              <label class="control-label">ISIN Type</label>
              <div class="controls">
              <?php $isin_type = DB::table('isin_type')->get();?>
                <select name="type" id="type">
                <option selected disabled>Select your option</option>
                  @foreach($isin_type as $data)
                  <option value="{{ $data->id }}">{{ strtoupper($data->title) }}</option>
                 @endforeach
                </select>
              </div>
            </div>
              <div class="control-group">
                <label class="control-label">Remarks</label>
                <div class="controls">
                <input type="text" name="remarks" id="remarks" placeholder="Enter Remarks">
                </div>
              </div>
              <div class="form-actions">
               <input type="submit" value="Add" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
