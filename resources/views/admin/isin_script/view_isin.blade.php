@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','View ISIN & Script')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a>  <a href="">ISIN & Script</a> <a href="" class="current">View ISIN & Script</a> </div>
    <h1>View ISIN & Script</h1>

     @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif
   
    @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

      @if(Session::has('flash_message_success'))

        <div class="alert alert-success alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_success') !!} </strong>
        </div>
       @endif
       
  </div>
  <div class="container-fluid">
    <hr>

     <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload File</h4>
        </div>
        <div class="modal-body">
          <form action="{{ url('/admin/isin/importexcel') }}" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}
          Choose your File : <input type="file" name="file" id="file" class="form-control"> <br> <br>
          <input type="submit" class="btn btn-primary btn-md" style="margin-left: 5px;" value="Import">
        </form>
        </div>
        <div class="modal-footer">
        
        </div>
      </div>
      
    </div>
  </div>
  
    <div class="row-fluid">
      <div class="span12">
      <a href="{{ url('/admin/add-isin') }}" class="btn btn-success"> <i class="icon-plus icon-white"></i> Add New</a>    
      <a href="{{ url('/admin/isin/exportexcel') }}" class="btn btn-primary" style="float: right;"> <i class="icon-file icon-white"></i> Export to Excel</a>
      <!-- <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#myModal" style="float: right;margin-right: 10px;"><i class="icon-file icon-white"></i> Import Excel File</button> -->
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-list"></i></span>
            <h5>All ISIN & Script List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Issuer Name</th>
                  <th>Script</th>
                  <th>ISIN Code</th>
                  <th>Type</th>
                  <th>Remarks</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              @foreach($data as $isin)
                <tr class="gradeU">
                  <td style="max-width:200px; width:200px;word-wrap:break-word;text-transform: uppercase;"><?php if(empty($isin->issuer->name)) echo "No Data"; else echo $isin->issuer->name.' ('.$isin->issuer_id.')'; ?> </td>
                  <td>{{ $isin->script }}</td>
                  <td>{{ $isin->isin_code }}</td>
                  <?php $type = DB::table('isin')->leftJoin('isin_type', 'isin.type', '=', 'isin_type.id')->where('type',$isin->type)->get(); ?>
                   <?php if(!empty($type)) { ?>
                   @foreach($type as $data) 
                    @endforeach
                   <td style="text-transform: uppercase;">{{ strtoupper($data->title) }} </td> 
                  
                   <?php } else { echo "No Data"; } ?>
                  <td style="max-width:200px; width:200px;word-wrap:break-word;text-transform: uppercase;">{{ $isin->remarks }}</td>
                  <td> <?php if($isin->status==1) echo 'Active'; else echo 'Inactive';?></td>
                  <td ><a href="{{ url('/admin/edit-isin/'.$isin->id ) }}" class=" btn btn-primary btn-mini" title="Edit"><i class="icon icon-edit"></i></a>
                  </a><a rel="{{ $isin->id }}" rel1="delete-isin" id="delISIN" <?php /*href="{{ url('/admin/delete-rta/'.$rtaData->rta_id ) }}"*/?> href="javascript:" class="btn btn-danger btn-mini deleteISINRecord" title="Delete"><i class="icon icon-trash"></i></a>
                  <?php  if($isin->status==0) { ?>
                  <a rel="{{ $isin->id }}" rel1="activate-isin" id="actISIN" class="btn btn-success btn-mini activateISINRecord" title="Activate" href="javascript:"><i class="icon icon-thumbs-up"></i></a>
                  <?php } else { ?>
                  <a rel="{{ $isin->id }}" rel1="deactivate-isin" id="deactISIN" class="btn btn-warning btn-mini deactivateISINRecord" title="Deactivate" href="javascript:"><i class="icon icon-thumbs-down"></i></a>
                  <?php } ?>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection