<!DOCTYPE html>
<html lang="en">
    
<head> <link rel="shortcut icon" type="image/ico" href="{{ asset('images/backend_images/favicon.ico') }}"/>
        <title>Reset Password :: CDSC Nepal</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="{{ asset('css/backend_css/bootstrap.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('css/backend_css/bootstrap-responsive.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/backend_css/matrix-login.css') }}" />
        <link href="{{ asset('fonts/backend_fonts/css/font-awesome.css') }}" rel="stylesheet" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

   <!--     <link rel="stylesheet" href="{{ asset('css/backend_css/matrix-style.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/backend_css/select2.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/backend_css/matrix-media.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/backend_css/uniform.css') }}" /> -->

    </head>
    <body>
        <div id="loginbox"> 

            <form id="resetpass" name="resetpass" class="form-vertical" method="post" action="{{ url('/admin/forgotpass') }}"> {{ csrf_field() }}
                
                 @if(Session::has('flash_message_error'))
       
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                            <strong> {!! session('flash_message_error') !!} </strong>
                    </div>

                 @endif

                   @if(Session::has('flash_message_success'))
       
                     <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                            <strong> {!! session('flash_message_success') !!} </strong>
                     </div>

                    @endif
                
                    <p><h3 style="text-align:center;color:white;">Reset Password</h3></p>
                    <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                        <span class="add-on bg_lo"><i class="icon-lock"></i></span><input type="password" name="new_password" id="new_password" placeholder="New Password" required="true" />
                        </div>
                    </div>
                    <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span><input type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password" required="true" />
                        </div>
                    </div>
                </div>
               
                <div class="form-actions">
                <!--    <span class="pull-left"><a href="{{ url('admin') }}" class="flip-link btn btn-success" id="login" name="login">&laquo; Back to login</a></span> -->
                    <span class="pull-right"><a class="btn btn-danger" id="reset" name="reset"/>Reset</a></span>
                </div>
            </form>
        </div>
        
        <script src="{{ asset('js/backend_js/jquery.min.js') }}"></script>  
        <script src="{{ asset('js/backend_js/matrix.login.js') }}"></script> 
        <script src="{{ asset('js/backend_js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/backend_js/jquery.uniform.js') }}"></script> 

      <!--  <script src="{{ asset('js/backend_js/select2.min.js') }}"></script> 
        <script src="{{ asset('js/backend_js/matrix.form_common.js') }}"></script> -->
        
    </body>

</html>
