@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Add Settlement')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">Settlement</a> <a href="" class="current">Add New Settlement</a> </div>
    <h1>Add Settlement</h1>  
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        
             @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif
    
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-plus"></i> </span>
            <h5>Add New Settlement</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ url('/admin/add-settlement') }}" name="add_settlement" id="add_settlement" novalidate="novalidate"> {{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">Settlement ID</label>
                <div class="controls">
                <input type="text" name="settlement_id" id="settlement_id" placeholder="Enter Settlement ID" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Trade Date</label>
                <div class="controls">
                <input type="text" id="trade_date" name="trade_date" data-date="01-01-2017" data-date-format="yyyy-mm-dd" value="<?php echo date('Y-m-d');?>" class="datepicker span11">
                </div>
              </div>
              <div class="form-actions">
               <input type="submit" value="Add" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
<script src="{{ asset('js/backend_js/matrix.form_common.js') }}"></script>
@endsection