@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Dashboard')

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard')}}" class="tip-bottom"> <i class="icon-dashboard"></i> Dashboard</a><h5 style="float:left;margin-left: 20px;"><?php date_default_timezone_set('Asia/Kathmandu'); echo date("F j, Y, h:i A");?></h3></div>
  </div>
  
<!--End-breadcrumbs-->

<!--Action boxes-->
  <div class="container-fluid">
    <div class="quick-actions_homepage">
      <ul class="quick-actions">
      <?php 
      $rta = DB::table('rta')->where('deleted_at',null)->where('status',1)->get(); 
      $isin = DB::table('isin')->where('deleted_at',null)->where('status',1)->get();
      $cm = DB::table('clearing_member')->where('deleted_at',null)->where('status',1)->get();
      $dp = DB::table('dp')->where('deleted_at',null)->where('status',1)->get();
      $news = DB::table('news_notice')->where('deleted_at',null)->where('status',1)->get();
      $issuer = DB::table('issuer')->where('deleted_at',null)->where('status',1)->get();
      $bank = DB::table('casba_bank')->where('deleted_at',null)->where('status',1)->get();
      $pressRelease = DB::table('press_release')->where('deleted_at',null)->where('status',1)->get();
      $settlement = DB::table('settlement')->where('deleted_at',null)->where('status',1)->get();
      $page = DB::table('page')->where('deleted_at',null)->where('status',1)->get();
      $dpMeroshare = DB::table('dp')->where('deleted_at',null)->where('status',1)->where('meroshare',1)->get();
      ?>

        <li class="bg_lo"> <a href="{{ url('/admin/view-dp') }} "> <i class="icon-desktop"></i> Total DP <br> <h4> <?php if (empty($dp)) echo "0"; else echo count($dp); ?></h4></a> </li>
        <li class="bg_lo"> <a href="{{ url('/admin/view-rta') }} "> <i class="icon-tablet"></i> Total RTA <br> <h4> <?php if (empty($rta)) echo "0"; else echo count($rta); ?> </h4></a> </li>
        <li class="bg_lo"> <a href="{{ url('/admin/view-cm') }} "> <i class="icon-tasks"></i> Total Clearing Members <br> <h4><?php if (empty($cm)) echo "0"; else echo count($cm); ?> </h4></a> </li>
        <li class="bg_lo"> <a href="{{ url('/admin/view-all-issuer') }} "> <i class="icon-user"></i> Total Issuer <br> <h4><?php if (empty($issuer)) echo "0"; else echo count($issuer); ?> </h4></a> </li>
        <li class="bg_lo"> <a href="{{ url('/admin/view-isin') }}"> <i class="icon-barcode"></i> Total ISIN<br> <h4> <?php if (empty($isin)) echo "0"; else echo count($isin); ?> </h4></a> </li>
        <li class="bg_lo"> <a href="{{ url('/admin/view-page') }} "> <i class="icon-copy"></i> Total Pages<br> <h4> <?php if (empty($page)) echo "0"; else echo count($page); ?> </h4></a> </li>
        <li class="bg_lo"> <a href="{{ url('/admin/view-casba-bank') }}"> <i class="icon-building"></i> Total Banks in C-ASBA<br> <h4> <?php if (empty($bank)) echo "0"; else echo count($bank); ?> </h4></a> </li>
        <li class="bg_lo"> <a href="{{ url('/admin/view-news-notice') }} "> <i class="icon-bar-chart"></i> Total News & Notices<br> <h4> <?php if (empty($news)) echo "0"; else echo count($news); ?> </h4></a> </li>
        <li class="bg_lo"> <a href="{{ url('/admin/view-press-release') }} "> <i class="icon-table"></i> Total Press Releases<br> <h4> <?php if (empty($pressRelease)) echo "0"; else echo count($pressRelease); ?> </h4></a> </li>
        <li class="bg_lo"> <a href="{{ url('/admin/view-settlement') }} "> <i class="icon-reorder"></i> Total Settlement ID<br> <h4> <?php if (empty($settlement)) echo "0"; else echo count($settlement); ?> </h4></a> </li>
        <li class="bg_lo"> <a href="#"> <i class="icon-credit-card"></i> Total DP in Meroshare<br> <h4> <?php if (empty($dpMeroshare)) echo "0"; else echo count($dpMeroshare); ?></h4></a> </li>
     </ul>
    </div>
<!--End-Action boxes-->    


  </div>
</div>
<!--end-main-container-part-->

@endsection
