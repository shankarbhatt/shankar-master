@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','View All Images')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a>  <a href="">Slider Image</a> <a href="" class="current">View All Images</a> </div>
    <h1>View All Images</h1>
    @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

      @if(Session::has('flash_message_success'))

        <div class="alert alert-success alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_success') !!} </strong>
        </div>

       @endif
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
         <a href="{{ url('/admin/add-image') }}" class="btn btn-success"> <i class="icon-plus icon-white"></i> Add New</a>
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-list"></i></span>
            <h5>All Images List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Description</th>
                  <th>Image</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              @foreach($image as $images)
                <tr class="gradeU">
                  <td >{{ $images->image_id }}</td>
                  <td > <?php if(empty($images->title)) echo "No Caption"; else echo $images->title; ?></td>
                    <td > <?php if(empty($images->description)) echo "No Description"; else echo $images->description; ?></td>
                  <td style="width:120px;"> <img src="{{url('slider_images/'.$images->file)}}" width="100px" height="50px"> </td>
                  <td style="width:15%;"><a href="{{ url('/admin/edit-image/'.$images->image_id ) }}" class=" btn btn-primary btn-mini" title="Edit"><i class="icon icon-edit"></i></a>
                  <a rel="{{ $images->image_id }}" rel1="delete-image" id="delIMG" <?php /*href="{{ url('/admin/delete-publication/'.$publications->publication_id ) }}" */?> href="javascript:" class="btn btn-danger btn-mini deleteImgRecord" title="Delete"><i class="icon icon-trash"></i> </a>
                   <?php if(!empty($images->file)) { ?>
                   <a href="{{ asset('slider_images/' . $images->file) }}"  target="_blank" class="btn btn-info btn-mini" title="View File"><i class="icon icon-eye-open"></i></a>
                   <a href="{{ asset('slider_images/' . $images->file) }}" class="btn btn-inverse btn-mini" download="{{ $images->file }}" title="Download File"><i class="icon icon-download-alt"></i></a>
                   <?php } ?>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection