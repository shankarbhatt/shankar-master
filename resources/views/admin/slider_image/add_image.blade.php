@extends('layouts.adminLayout.admin_design')
@section('content')
@section('title','Add Image')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">Slider Image</a> <a href="" class="current">Add Image</a> </div>
    <h1>Add Image</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">

      
         @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif

      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-plus"></i> </span>
          <h5>Add New Image</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="{{ url('/admin/add-image') }}" method="post" enctype="multipart/form-data" class="form-horizontal" name="add_image" id="add_image" novalidate="novalidate"> {{ csrf_field() }}
            <div class="control-group">
                <label class="control-label">Title</label>
                <div class="controls">
                <input type="text" id="title" name="title" placeholder="Enter Image Title">
                </div>
            </div>
             <div class="control-group">
                <label class="control-label">Description</label>
                <div class="controls">
                 <textarea rows="2" cols="2" class="span6" id="description" name="description"></textarea>
                </div>
            </div>
            <div class="control-group">
              <label class="control-label">Image</label>
              <div class="controls">
                <input type="file" name="file" id="file" />
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Add</button>
            </div>
          </form>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>

@endsection