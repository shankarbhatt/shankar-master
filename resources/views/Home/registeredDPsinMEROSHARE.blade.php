@include('Home.layouts.header')
@include('Home.layouts.nav')  
<section id="solution" class="p-t-30 p-b-30">
    <div class="container">
        <div id="book-closure" class="row">
            <div class="col-md-12 p-b-50" id="dp">
                <div class="heading_border bg_red"></div>
                <h2>Registered Banks in C-ASBA</h2>
                  
                                        <div class="table-responsive" id="listall">
                        <table class="table table-responsive table-striped table-bordered" id="">
                            <thead>
                               <tr>
                                <th>S.N</th>
                                <th>DP Name</th>
                                <th>DPID</th>
                                <th>Address</th>
                                <th>Telephone</th>
                                </tr>
                            </thead>
                           <tbody>
                                                        <tr>
                                <td>1</td>
                                <td>ABC Securities Pvt. Limited</td>
                                <td>13200</td>
                                <td>Indrachowk, Kathmandu</td>
                                <td>01-4230787</td>
                            </tr>
                                                        <tr>
                                <td>2</td>
                                <td>ACE Capital Limited</td>
                                <td>10200</td>
                                <td>Laldurbar Kathmandu</td>
                                <td>01-4426161</td>
                            </tr>
                                                        <tr>
                                <td>3</td>
                                <td>Agrawal Securities Pvt. Limited</td>
                                <td>12300</td>
                                <td>Putalisadak, Kathmandu</td>
                                <td>01-4424657, 4424406</td>
                            </tr>
                                                        <tr>
                                <td>4</td>
                                <td>Aryatara Investment & Securities Pvt. Limited</td>
                                <td>11900</td>
                                <td>Dillibazar, Kathmandu</td>
                                <td>01-4424406</td>
                            </tr>
                                                        <tr>
                                <td>5</td>
                                <td>Ashutosh Brokerage and Securities Pvt. Ltd.</td>
                                <td>15600</td>
                                <td>Khichapokhari, Kathmandu</td>
                                <td>01-4220276, 01-4227510</td>
                            </tr>
                                                        <tr>
                                <td>6</td>
                                <td>Asian Securities Pvt. Limited</td>
                                <td>14700</td>
                                <td>Bagbazar, Kathmandu</td>
                                <td>01-4240609</td>
                            </tr>
                                                        <tr>
                                <td>7</td>
                                <td>Bank of Kathmandu Limited</td>
                                <td>11100</td>
                                <td>Kamalpokhari, Kathmandu</td>
                                <td>01-4414541</td>
                            </tr>
                                                        <tr>
                                <td>8</td>
                                <td>Bhrikuti Stock Broking Company Pvt. Limited</td>
                                <td>15000</td>
                                <td>Newroad, Kathmandu</td>
                                <td>01-4223466</td>
                            </tr>
                                                        <tr>
                                <td>9</td>
                                <td>Century Commercial Bank Limited</td>
                                <td>16000</td>
                                <td>Putalisadak, Kathmandu</td>
                                <td>01-4445062, 01-4412579</td>
                            </tr>
                                                        <tr>
                                <td>10</td>
                                <td>Citizens Bank International Limited</td>
                                <td>11700</td>
                                <td>Kamaladi, Kathmandu</td>
                                <td>01-4102534</td>
                            </tr>
                                                        <tr>
                                <td>11</td>
                                <td>Civil Capital Market Limited</td>
                                <td>10100</td>
                                <td>CTC Mall 6th Floor, Sundhara, Kathmandu</td>
                                <td>01-4262303, 01-4261251</td>
                            </tr>
                                                        <tr>
                                <td>12</td>
                                <td>Creative Securities Pvt. Limited</td>
                                <td>13300</td>
                                <td>Kamalpokhari, Kathamndu</td>
                                <td>01-4419582</td>
                            </tr>
                                                        <tr>
                                <td>13</td>
                                <td>Crystal Kanchanjungha Securities Pvt. Limited</td>
                                <td>13400</td>
                                <td>New Plaza, Kathmandu</td>
                                <td>01-4011072</td>
                            </tr>
                                                        <tr>
                                <td>14</td>
                                <td>Dakshinkali Investment and Securities Pvt. Limited</td>
                                <td>12000</td>
                                <td>Kamalpokhari, Kathmandu</td>
                                <td>01-4439676</td>
                            </tr>
                                                        <tr>
                                <td>15</td>
                                <td>Deevyaa Securities & Stock House Pvt. Limited</td>
                                <td>14500</td>
                                <td>Putalisadak, Kathmandu</td>
                                <td>01-4421488</td>
                            </tr>
                                                        <tr>
                                <td>16</td>
                                <td>Dipshikha Dhitopatra Karobar Co. Pvt. Limited</td>
                                <td>11300</td>
                                <td>Anamnagar, Kathamndu</td>
                                <td>01-4169067</td>
                            </tr>
                                                        <tr>
                                <td>17</td>
                                <td>Dynamic Money Managers Securities Pvt. Limited</td>
                                <td>14900</td>
                                <td>Putalisadak, Kathmandu</td>
                                <td>01-4414522</td>
                            </tr>
                                                        <tr>
                                <td>18</td>
                                <td>Everest Bank Limited</td>
                                <td>10800</td>
                                <td>Lazimpat, Kathmandu</td>
                                <td>01-4443377</td>
                            </tr>
                                                        <tr>
                                <td>19</td>
                                <td>Global IME Capital Limited</td>
                                <td>11200</td>
                                <td>Jamal, Kathmandu</td>
                                <td>01-4422460</td>
                            </tr>
                                                        <tr>
                                <td>20</td>
                                <td>Guheswori Merchant Banking & Finance Limited</td>
                                <td>16200</td>
                                <td>Pulchowk,Lalitpur</td>
                                <td>01-5550406, 01-5537407</td>
                            </tr>
                                                        <tr>
                                <td>21</td>
                                <td>Imperial Securities Co. Pvt. Limited</td>
                                <td>13100</td>
                                <td>Anamnagar, Kathmandu</td>
                                <td>01-4231004</td>
                            </tr>
                                                        <tr>
                                <td>22</td>
                                <td>Janata Bank Nepal Limited</td>
                                <td>12200</td>
                                <td>New Baneshwor, Kathmandu</td>
                                <td>01-4786300</td>
                            </tr>
                                                        <tr>
                                <td>23</td>
                                <td>Kailash Bikash Bank Limited</td>
                                <td>15400</td>
                                <td>NewPlaza, Kathmandu</td>
                                <td>14443034</td>
                            </tr>
                                                        <tr>
                                <td>24</td>
                                <td>Kohinoor Investment & Securities Pvt. Limited</td>
                                <td>14300</td>
                                <td>Hattisar, Kathmandu</td>
                                <td>01-4442857</td>
                            </tr>
                                                        <tr>
                                <td>25</td>
                                <td>Kriti Capital and Investment Limited</td>
                                <td>14100</td>
                                <td>Bagdurbar, Sundhara, JDA Complex, KATHMANDU</td>
                                <td>01-4266325, 01-4249453, 01-4216343</td>
                            </tr>
                                                        <tr>
                                <td>26</td>
                                <td>Kumari Bank Limited</td>
                                <td>15200</td>
                                <td>Naxal, Kathmandu</td>
                                <td>01-4415173</td>
                            </tr>
                                                        <tr>
                                <td>27</td>
                                <td>Laxmi Capital Limited</td>
                                <td>10700</td>
                                <td>Pulchowk, Lalitpur</td>
                                <td>01-5551363</td>
                            </tr>
                                                        <tr>
                                <td>28</td>
                                <td>Linch Stock Market Limited</td>
                                <td>13800</td>
                                <td>New Baneshwor, Kathmandu</td>
                                <td>01-4469367</td>
                            </tr>
                                                        <tr>
                                <td>29</td>
                                <td>Machhapuchchhre Bank Limited</td>
                                <td>16100</td>
                                <td>Lazimpat, Kathmandu</td>
                                <td>01-4428556</td>
                            </tr>
                                                        <tr>
                                <td>30</td>
                                <td>Market Securities Exchange Company Pvt. Limited</td>
                                <td>13600</td>
                                <td>Khicha Pokhari, Kathmandu</td>
                                <td>01-4248973</td>
                            </tr>
                                                        <tr>
                                <td>31</td>
                                <td>Naasa Securities Company Limited</td>
                                <td>15900</td>
                                <td>Naxal, Kathmandu</td>
                                <td>01-4440384, 01-4440385</td>
                            </tr>
                                                        <tr>
                                <td>32</td>
                                <td>Nabil Investment Banking Limited</td>
                                <td>10400</td>
                                <td>Naxal, Kathmandu</td>
                                <td>01-4411604</td>
                            </tr>
                                                        <tr>
                                <td>33</td>
                                <td>National Merchant Banker Limited</td>
                                <td>16400</td>
                                <td>Gyneshwor- 32, Kathmandu</td>
                                <td>01-4430746</td>
                            </tr>
                                                        <tr>
                                <td>34</td>
                                <td>Nepal Bangladesh Bank Limited</td>
                                <td>15100</td>
                                <td>Kamaladi, Kathmandu</td>
                                <td>01-4233780</td>
                            </tr>
                                                        <tr>
                                <td>35</td>
                                <td>Nepal Bank Limited</td>
                                <td>15700</td>
                                <td>Dharmapath, Kathmandu</td>
                                <td>01-4221185,</td>
                            </tr>
                                                        <tr>
                                <td>36</td>
                                <td>Nepal Credit & Commerce Bank Limited</td>
                                <td>16300</td>
                                <td>Bagbazar, Kathmandu</td>
                                <td>01-4246991</td>
                            </tr>
                                                        <tr>
                                <td>37</td>
                                <td>Nepal DP Limited</td>
                                <td>15500</td>
                                <td>Putalisadak, Kathmandu</td>
                                <td>01-4227086</td>
                            </tr>
                                                        <tr>
                                <td>38</td>
                                <td>Nepal SBI Bank Limited</td>
                                <td>15300</td>
                                <td>Hattisar, Kathmandu</td>
                                <td>01-211991, 01-4435671</td>
                            </tr>
                                                        <tr>
                                <td>39</td>
                                <td>Nepal Stock House Pvt. Limited</td>
                                <td>11500</td>
                                <td>Anamnagar, Kathmandu</td>
                                <td>01-4233596</td>
                            </tr>
                                                        <tr>
                                <td>40</td>
                                <td>NIBL Capital Markets Limited</td>
                                <td>10600</td>
                                <td>Lazimpat, Kathmandu</td>
                                <td>01-4005080</td>
                            </tr>
                                                        <tr>
                                <td>41</td>
                                <td>NIC Asia Bank Limited</td>
                                <td>13700</td>
                                <td>Thapathali, Kathmandu</td>
                                <td>01-5111179</td>
                            </tr>
                                                        <tr>
                                <td>42</td>
                                <td>NMB Capital Limited</td>
                                <td>11000</td>
                                <td>Nagpokhari, Kathmandu</td>
                                <td>01-4437963, 01-4437964</td>
                            </tr>
                                                        <tr>
                                <td>43</td>
                                <td>Online Securities Pvt. Limited</td>
                                <td>11800</td>
                                <td>Putalisadak, Kathmandu</td>
                                <td>01-4168302</td>
                            </tr>
                                                        <tr>
                                <td>44</td>
                                <td>Prabhu Bank Limited</td>
                                <td>13900</td>
                                <td>Babarmahal, Kathmandu</td>
                                <td>01-4232500</td>
                            </tr>
                                                        <tr>
                                <td>45</td>
                                <td>Prabhu Capital Limited</td>
                                <td>12600</td>
                                <td>Durbar Marg, Kathmandu</td>
                                <td>01-4222987</td>
                            </tr>
                                                        <tr>
                                <td>46</td>
                                <td>Premier Securities Co. Limited</td>
                                <td>14800</td>
                                <td>Newplaza, Putalisadak, Kathmandu</td>
                                <td>01-4432832, 4432704, 4432700</td>
                            </tr>
                                                        <tr>
                                <td>47</td>
                                <td>Prime Commercial Bank Limited</td>
                                <td>16900</td>
                                <td>Kamalpokhari, Kathmandu</td>
                                <td>01-4423433</td>
                            </tr>
                                                        <tr>
                                <td>48</td>
                                <td>Primo Securities Pvt. Limited</td>
                                <td>12800</td>
                                <td>Putalisadak, Kathmandu</td>
                                <td>01-4168092</td>
                            </tr>
                                                        <tr>
                                <td>49</td>
                                <td>Provident Merchant Banking Limited</td>
                                <td>16600</td>
                                <td>Kathmandu-35, Balkumari Bridge</td>
                                <td>01-5147097, 01-5147192</td>
                            </tr>
                                                        <tr>
                                <td>50</td>
                                <td>RBB Merchant Banking Limited</td>
                                <td>16500</td>
                                <td>Teku, Kathmandu</td>
                                <td>01-4243265</td>
                            </tr>
                                                        <tr>
                                <td>51</td>
                                <td>Sani Securities Company Limited</td>
                                <td>14400</td>
                                <td>Jamal, Kathmandu</td>
                                <td>01-4166005</td>
                            </tr>
                                                        <tr>
                                <td>52</td>
                                <td>Sanima Bank Limited</td>
                                <td>15800</td>
                                <td>Naxal, Kathmandu</td>
                                <td>01-4428979, 01-4428980</td>
                            </tr>
                                                        <tr>
                                <td>53</td>
                                <td>Secured Securities Limited</td>
                                <td>11600</td>
                                <td>Kamaladi, Kathamndu</td>
                                <td>01-4168640</td>
                            </tr>
                                                        <tr>
                                <td>54</td>
                                <td>Sewa Securities Pvt. Limited</td>
                                <td>12700</td>
                                <td>Tripureshwor, Kathmandu</td>
                                <td>01-4256642</td>
                            </tr>
                                                        <tr>
                                <td>55</td>
                                <td>Shree Krishna Securities Limited</td>
                                <td>12900</td>
                                <td>Dillibazar, Kathmandu</td>
                                <td>01-4441225</td>
                            </tr>
                                                        <tr>
                                <td>56</td>
                                <td>Siddhartha Capital Limited</td>
                                <td>10900</td>
                                <td>Narayanchaur, Naxal, Kathmandu</td>
                                <td>01-4420924. 4420925</td>
                            </tr>
                                                        <tr>
                                <td>57</td>
                                <td>Sipla Securities Pvt. Limited</td>
                                <td>14600</td>
                                <td>Khicha Pokhari, Kathmandu</td>
                                <td>01-4255078</td>
                            </tr>
                                                        <tr>
                                <td>58</td>
                                <td>South Asian Bulls Pvt. Limited</td>
                                <td>13000</td>
                                <td>Kuleshwor, Kathmandu</td>
                                <td>01-4274020</td>
                            </tr>
                                                        <tr>
                                <td>59</td>
                                <td>Sri Hari Securities Pvt. Limited</td>
                                <td>14000</td>
                                <td>Kamaladi, Kathamndu</td>
                                <td>01-4437465</td>
                            </tr>
                                                        <tr>
                                <td>60</td>
                                <td>Sumeru Securities Pvt. Limited</td>
                                <td>14200</td>
                                <td>Hattisar, Kathmandu</td>
                                <td>01-4444740</td>
                            </tr>
                                                        <tr>
                                <td>61</td>
                                <td>Sunrise Capital Limited</td>
                                <td>12400</td>
                                <td>Kamalpokhari, Kathmandu</td>
                                <td>01-4428550, 4428660</td>
                            </tr>
                                                        <tr>
                                <td>62</td>
                                <td>Trishakti Securities Public Limited</td>
                                <td>11400</td>
                                <td>Putalisadak, Kathmandu</td>
                                <td>01-4232132</td>
                            </tr>
                                                        <tr>
                                <td>63</td>
                                <td>United Finance Limited</td>
                                <td>16800</td>
                                <td>Darbarmarg, KATHMANDU</td>
                                <td>01-4241648</td>
                            </tr>
                                                        <tr>
                                <td>64</td>
                                <td>Vibor Capital Limited</td>
                                <td>12500</td>
                                <td>Putalisadak, Kathmandu</td>
                                <td>01-4168195</td>
                            </tr>
                                                        <tr>
                                <td>65</td>
                                <td>Vision Securities Pvt. Limited</td>
                                <td>13500</td>
                                <td>Putalisadak, Kathmandu</td>
                                <td>01-4438621</td>
                            </tr>
                                                        </tbody>
                        </table>
                    </div>
                                </div>
        </div>
    </div>
</section>   @include('Home.layouts.footer')