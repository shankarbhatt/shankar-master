@include('Home.layouts.header')
<title>C-ASBA Registered Banks :: CDSC</title>

<section id="solution" class="p-t-30 p-b-30">
    <div class="container">
        <div id="book-closure" class="row">
            <div class="col-md-12 p-b-50" id="dp">
                <div class="heading_border bg_red"></div>
                <h2>Registered Banks in C-ASBA</h2>
                  
                                        <div class="table-responsive" id="listall">
                        <table class="table table-responsive table-striped table-bordered" id="">
                            <thead>
                               <tr>
                                <th>S.N</th>
                                <th>Bank Name</th>
                                <th>Bank Code</th>
                                </tr>
                            </thead>
                           <tbody>
                           <?php  $allbank =DB::table('casba_bank')->where('status',1)->orderBy('bank_name','ASC')->where('deleted_at',null)->get();?>

                               <?php $i =0;?>
                        @foreach($allbank as $allbanks)
                                                        <tr>
                                                       
                                <td><?php echo ++$i;?></td>
                                <td>{{$allbanks->bank_name}}</td>
                                <td>{{$allbanks->bank_id}}</td>
                            </tr>
                            @endforeach
                                              
                                                        </tbody>
                        </table>
                        
                    </div>
                                </div>
        </div>
    </div>
</section>   
 @include('Home.layouts.footer')