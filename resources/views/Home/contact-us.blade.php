@include('Home.layouts.header')
<title>Contact Us :: CDSC</title>

    <!-- Contact Us Start -->
   
    <!-- Map End -->
    <section id="contact_form" class="p-t-60 p-b-40">
        <div class="container">
            <div class="row">
                <div class="col-md-8 p-b-10">
                                            <div class="heading">
                            <div class="heading_border bg_red"></div>
                            
                            <h2>WE WANT TO <span class="color_red">HEAR FROM YOU</span></h2>
                        </div>
                        <div class="row p-t-20">
                            <form  id="" method="POST" action="#">

                             <div class="col-md-12"><div id="result"></div></div>

                             <div class="col-md-6 col-sm-6">
                                <div class="single-query">
                                    <input type="text" placeholder="Your Name" class="keyword-input" required name="name" id="name">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="single-query">
                                    <input type="email" placeholder="Email Address" class="keyword-input" required name="email" id="email" >
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="single-query">
                                    <input type="text" placeholder="Phone Number" class="keyword-input"  name="phone" id="phone" >
                                </div>
                            </div>
                            
                            <div  class="col-md-12">
                                <div class="single-query">
                                    <textarea placeholder="Message" name="message" id="message" required="required"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" value="Submit"  class="btn-light button-black" id="" name="btn_submit">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-md-4 col-sm-8 colxs-12">


                    <div class="form_left p-b-60" style="margin-top:30px;">
                        <h3 class="text-uppercase">Get in Touch</h3>
                        <div class="footer_1_contact">
                            <p><i class="icon-telephone114"></i>Phone: 977 1 4238008, 4216068,4240150, 4260386, 4260783, 4263301 </p>
                            <br>
                             <p><i class="icon-print"></i>Fax: 977 1 4240357 </p>
                            <br>
                            <p><i class=" icon-icons142"></i>Email: info@cdsc.com.np</p>
                            <br>
                            <p><i class="icon-icons20"></i>10:00 AM - 5:00 PM  (Sunday to Thursday)</p>
                             <p><i class=""></i><i class=""></i>10:00 AM - 1:30 PM (Friday) </p>
                            <br>
                            
                            <p><i class="icon-icons74"></i> Share Markets Commercial Complex Putalisadak, Kathmandu, Nepal</p>
                            </div>
                        </div>

                        <div class="form_left">
                            <h3 class="text-uppercase">Social Media</h3>
                            
                            <div class="social-icons_1">
                                <ul>
                                    <li><a href="https://www.facebook.com/CDSnClearing/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                </ul>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </section>

        <!-- Map Start -->
        <div id="contact_us_page">
            <div class="container">
                <div class="google-maps">
                     
<iframe src="https://www.google.com/maps/d/embed?mid=11IlfK3_esrbKIT_Z-f6vxyoOIMJSUY4U" width="1200" height="400" frameborder="0" style="border:0" allowfullscreen> </iframe> 
 
            
                 </div>   
                <br/>
                <br/>
                <br/>
                </div>
            </div>
        </div>
        <style>
    .google-maps {
        position: relative;
        padding-bottom: 30%; // This is the aspect ratio
        height: 0;
        overflow: hidden;
    }
    .google-maps iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100% !important;
        height: 100% !important;
    }
</style>
        
        <!-- Contact Us End -->

          <!-- Footer Start -->
@include('Home.layouts.footer')
