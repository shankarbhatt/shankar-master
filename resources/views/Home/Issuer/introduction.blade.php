@include('Home.layouts.header')
<title>Introduction :: Issuer </title>
<!-- Project Details Start -->
<section id="solution" class="p-t-30 p-b-30">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="solution_tabs">
                <ul>
                    <li class="active"><a>Investor</a>  <br/>   </li> 
                   <ul>
                                                        <li class="active">
                                    <a href="{{url('issuer')}}">Introduction</a>
                                </li>
                                  <li class="">
                                    <a href={{url('isinscript')}}>ISIN & Scripts</a>
                                </li>
                                 <li class="">
                                    <a href={{url('registeredcompany')}}>Registered Company</a>
                                </li>
                                                        <li class="">
                                    <a href={{url('benefits')}}>Benefits</a>
                                </li>
                                                        <li class="">
                                    <a href={{url('issueradmissionprocedure')}}>Admission Procedure</a>
                                </li>
                                            </ul>
                                            </ul>
                </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div id="global-relation" class="our_team">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading" id="global-relation">
                                <div class="heading_border bg_red"></div>
                                <h2>Introduction</h2>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 project_details_text">
                           <p class="p_14" style="text-align: justify;"><p>Corporates / Companies which issue any kind of security are known as &#39;Issuer&#39; in the depository system. Only those securities, which are admitted into the CDSC system are available for dematerialisation to the holders of such securities or can be allotted in electronic record form by the issuer. Securities include shares, debentures, bond and mutual fund units. The companies listed in Nepal stock exchange ties can be admitted into the CDSC system. CDSC functions as the central accounting and record keeping office in respect of the securities admitted by issuer companies.&nbsp;<br />
<br />
Before the admission of any security into the CDSC system, it is necessary for the issuer to establish an electronic connectivity with CDSC through a registrar and transfer agent (RTA), who has already established connectivity with CDSC. All RTAs have already established electronic connectivity with CDSC.&nbsp;<br />
<br />
The centralised database architecture of CDSC places it in a unique position to provide issuers up-to-the-moment details of holdings of the security.</p>
</p>
                        </div>
                    </div>
                </div>
                <!-- Our Partners end -->
            </div>
        </div>
    </div>
</section>
<br>
<!-- Project Details End -->  
 <!-- Footer Start -->
 @include('Home.layouts.footer')
