@include('Home.layouts.header')
<title>Company List :: Issuer </title>
          
                <section id="solution" class="p-t-60 p-b-40">
    <div class="container">
        <div id="book-closure" class="row">
            <div class="col-md-12 p-b-50" id="dp">
                <div class="heading_border bg_red"></div>
                <?php $rta = DB::table('issuer')->leftJoin('rta', 'issuer.rta_id', '=', 'rta.rta_id')->where('issuer_id',$data->issuer_id)->get(); ?>
                @foreach($rta as $data)
                     
                  @endforeach 
                <h2>Company List of {{  $data->name }}</h2>
                    <!-- <div class="row">
                        <form>
                            <div class="col-md-3 col-sm-4">
                                <div class="single-query">
                                    <input type="text" placeholder="ENTER RTA NAME" class="keyword-input" id="rta_name">
                                </div>
                            </div>

                            
                            <div class="col-md-2">
                                 <button type="button" class="btn-light button-black" id="btn_search_si">Search</button>
                            </div>
                        </form>
                    </div> -->
                                         <div class="table-responsive" id="listall">
                    <table class="table table-responsive table-striped table-bordered" id="dynamic-table" style="width: 100%;table-layout: fixed;">
                            <thead>
                                <tr>
                                    <th style=" width:40px;">S.N.</th>
                                    <th style=" width:200px;">ISSUER NAME</th>
                                    <th>ADDRESS</th>
                                    <th>PHONE NO</th>
                                    <th>EMAIL</th>
                                    <th>ISSUED CAPITAL</th>
                                    <th>LISTED CAPITAL</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                            
                                  <?php $i =0;?>
                        @foreach($issuer as $data)
                        <tr>
                     <td><?php echo ++$i;?></td>
                   
                      <?php $new =  DB::table('issuer')->where('status',1)->where('deleted_at',null)->where('issuer_id',$data->issuer_id)->get(); ?>
                     @foreach($new as $data1)
                     <td style="word-wrap: break-word;">{{  $data1->name }}</td>
                  @endforeach 
                 
                  <td style="word-wrap: break-word;">{{  $data->address }}</td>
                  <td style="word-wrap: break-word;">{{  $data->phone }}</td>
                  <td style="word-wrap: break-word;">{{  $data->email }}</td>
                 <td style="word-wrap: break-word;">{{ number_format($data->issued_capital )}}</td>
                  <td >{{ number_format($data->listed_capital )}}</td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript">
   $(document).ready(function(){
        $("#rta_name").keyup(function(){
            var rta_name = $("#rta_name").val();
            if(rta_name=="") {
                alert('Please Enter Search Keyword');
            } else {
                $.ajax({
                    type: "POST",
                    url: "http://itsutra.com.np/cdscnp/Rta/Searchrta",
                    data: {rta_name:rta_name},
                    cache: false,
                    success: function(data) {
                        $("#listall").empty().html(data);
                    }
                }); 
            }
        });

        $("#btn_search_si").click(function(){
            var rta_name = $("#rta_name").val();
            if(rta_name=="") {
                alert('Please Enter Search Keyword');
            } else {
                $.ajax({
                    type: "POST",
                    url: "http://itsutra.com.np/cdscnp/Site/Searchrta",
                    data: {rta_name:rta_name},
                    cache: false,
                    success: function(data) {
                        $("#listall").empty().html(data);
                    }
                }); 
            }
        });


   });
</script>  
 <!-- Footer Start -->
 @include('Home.layouts.footer')
