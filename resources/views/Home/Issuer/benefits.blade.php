@include('Home.layouts.header')
<title>Benefits :: Issuer </title>
<!-- Project Details Start -->
<section id="solution" class="p-t-30 p-b-30">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="solution_tabs">
                <ul>
                    <li class="active"><a>Investor</a>  <br/>   </li> 
                   <ul>
                                                        <li class="">
                                    <a href="{{url('issuer')}}">Introduction</a>
                                </li>
                                  <li class="">
                                    <a href={{url('isinscript')}}>ISIN & Scripts</a>
                                </li>
                                 <li class="">
                                    <a href={{url('registeredcompany')}}>Registered Company</a>
                                </li>
                                                        <li class="active">
                                    <a href={{url('benefits')}}>Benefits</a>
                                </li>
                                                        <li class="">
                                    <a href={{url('issueradmissionprocedure')}}>Admission Procedure</a>
                                </li>
                                            </ul>
                                            </ul>
                </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div id="global-relation" class="our_team">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading" id="global-relation">
                                <div class="heading_border bg_red"></div>
                                <h2>Benefits</h2>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 project_details_text">
                           <p class="p_14" style="text-align: justify;"><h3 style="text-align: justify;"><span style="font-size: 18px;"><b>Benefits of Dematerialisation</b></span></h3>

<p style="text-align: justify;">Dematerialisation of securities provides numerous benefits to the issuer. Which are:</p>

<ul>
	<li style="text-align: justify;">In the demat form transfer of securities takes place at CDSC and if the entire issue of a security is held in demat form, the issuer can save considerable time and money being incurred on its share department / RTA.</li>
	<li style="text-align: justify;">Dematted securities are not subject to loss, theft, mutilation or misuse by faking or forging certificates, thereby saving companies from lengthy correspondence, litigation and complaint handling. It will therefore eliminate instances of bad delivery.</li>
	<li style="text-align: justify;">All non-cash corporate actions such as rights, bonus, subdivision of holdings, conversion of securities, issuing securities on mergers/amalgamations and in initial public offerings (IPO) can be handled in demat form without any hassles in the shortest possible time and at very low cost.</li>
	<li style="text-align: justify;">With the CDSC&#39;s centralised database, the issuer can get upto-the-moment information on any changes in its holding pattern of a security. Thus, the company effectively monitors the change in holding and is alert to any undue threat.</li>
	<li style="text-align: justify;">Issuers can save substantial time of managing the share department. It reduces the work load of the department and allows the management to use the excess manpower in other productive departments.</li>
	<li style="text-align: justify;">New technology like e-Voting system for corporate improves transparency and corporate governance standards and also helps in reducing the administrative cost associated with postal ballot while facilitating declaration of results immediately after the close of the voting. Issuers can hugely benefit from such advanced technology which is possible to achieve through a central depository company.</li>
</ul>

<h3 style="text-align: justify;"><span style="font-size:18px;"><strong>Safety</strong></span></h3>

<ul>
	<li style="text-align: justify;">The software developed has all the security features conforming to world standards. The software facilitates a robust accounting and transaction management system.</li>
	<li style="text-align: justify;">CDSC&#39;s system is based on a centralised database with on-line connectivity with Depository Participants (DPs) who provide on-line services to investors. All data pertaining to investor holdings are stored at CDSC. CDSC has also established extensive back up systems.</li>
	<li style="text-align: justify;">CDSC ensures the security and integrity of all data by protecting it from any misuse or manipulation by unauthorised users as all communications between CDSC and its users is encrypted.</li>
	<li style="text-align: justify;">The hardware, software and connectivity systems are reviewed continuously to strengthen the systems &amp; procedures and to comply with the stringent international standards.</li>
</ul>
</p>
                        </div>
                    </div>
                </div>
                <!-- Our Partners end -->
            </div>
        </div>
    </div>
</section>
<br>
<!-- Project Details End -->  
 <!-- Footer Start -->
 @include('Home.layouts.footer')
