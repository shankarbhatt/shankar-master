@include('Home.layouts.header')
<title>Admission :: Issuer </title>
<!-- Project Details Start -->
<section id="solution" class="p-t-30 p-b-30">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="solution_tabs">
                <ul>
                    <li class="active"><a>Investor</a>  <br/>   </li> 
                   <ul>
                                                        <li class="">
                                    <a href="{{url('issuer')}}">Introduction</a>
                                </li>
                                  <li class="">
                                    <a href={{url('isinscript')}}>ISIN & Scripts</a>
                                </li>
                                 <li class="">
                                    <a href={{url('registeredcompany')}}>Registered Company</a>
                                </li>
                                                        <li class="">
                                    <a href={{url('benefits')}}>Benefits</a>
                                </li>
                                                        <li class="active">
                                    <a href={{url('issueradmissionprocedure')}}>Admission Procedure</a>
                                </li>
                                            </ul>
                                            </ul>
                </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div id="global-relation" class="our_team">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading" id="global-relation">
                                <div class="heading_border bg_red"></div>
                                <h2>Admission Procedure</h2>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 project_details_text">
                           <p class="p_14" style="text-align: justify;"><p>The corporate body which has listed its securities in SEBON is eligible for admitting their securities for dematerialization. For the admission procedure in CDSC the company has to fulfill all the criteria listed in the byelaw of CDSC.</p>


<p> <a href="{{ url('/aggrement/2CDS and ISSUER Company Agreement.pdf') }}" class="btn btn-inverse btn-mini" download="2CDS and ISSUER Company Agreement.pdf" title="Download File">The format of the agreement between ISSUER and CDSC is as follows: agreement2</a></p></p>

</p>
                        </div>
                    </div>
                </div>
                <!-- Our Partners end -->
            </div>
        </div>
    </div>
</section>
<br>
<!-- Project Details End -->  
 <!-- Footer Start -->
 @include('Home.layouts.footer')
