@include('Home.layouts.header')
<title>Publications :: Downloads</title>
<section id="solution" class="p-t-60 p-b-40">
  <div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 50px;">
                <div class="row" id="introduction">
                  <div class="col-md-12">
                    <div class="heading">
                        <div class="heading_border bg_red"></div>
                         <h2>Publication</h2>
                    </div>
                  </div>
                </div>

               

                <div class="row">
                  <div class="col-md-12">
                                            <div class="table-responsive" id="listall">
                          <table class="table table-striped table-bordered table-responsive" id="dynamic-table" style="width:100%">
                           <thead>
                                <th>S.N</th>
                              <th>TITLE</th>
                              <th>DOWNLOAD</th>
                            </thead>
                            <?php $i=0; ?>
                            <tbody>
                            <?php $allPublication  = DB::table('downloads')->where('status',1)->where('deleted_at',null)->where('type',4)->orderBy('title','ASC')->get();?>
                            @foreach( $allPublication as $data)
                                                                <tr>
                                                               
                                                                
                                    <td><?php echo ++$i; ?></td>
                                        <td>
                                     
                                        <a href="{{ asset('downloads_files/' . $data->file) }}" download="{{ $data->file }}">{{ str_replace('_', ' ', $data->title)}} (Please Click to Download)</a>
                                       
                                        </td>
                                        <td><a href="{{ asset('downloads_files/' . $data->file)  }}" download="{{ $data->file }}"> <i class="fa fa-download" aria-hidden="true"></i></a></td>
                                 
                                    </tr>
                          
                                    @endforeach                       
                            </tbody>
                            </table>
                           
                        </div>
                                        </div>
                </div>
              </div>
          </div>
        </div>
    </div>
  </div>
</section>

<script type="text/javascript">
  $(document).ready(function(){
    $("#btn_search_si").click(function(){
        var script_name = $("#script_name").val();
        var isin = $("#isin").val();
        $.ajax({
            type: "POST",
            url: "http://itsutra.com.np/cdscnp/Site/searchIsinScript",
            data: {script_name:script_name,isin:isin},
            cache: false,
            success: function(data) {
              $('#listall').html(data);
            }
        });
        // $.ajax({
        //     type: "POST",
        //     url: "http://itsutra.com.np/cdscnp/Site/searchIsinScript",
        //     data: {script_name:script_name,isin:isin},
        //     cache: false,
        //     success: function(data) {
        //       alert(data);
        //     }
        // });
    });
  });
</script>
   <!-- Footer Start -->
   @include('Home.layouts.footer')
