@include('Home.layouts.header')
<title>Registered Company :: Investor </title>
<!-- Project Details Start -->
<section id="solution" class="p-t-60 p-b-40">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row " id="introduction">
                    <div class="col-md-12">
                        <div class="heading">
                            <div class="heading_border bg_red"></div>
                            <h2>Registered Company</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="single-query">
                            <!-- <input type="text" placeholder="ENTER SCRIPT NAME" class="keyword-input" id="script_name"> -->
                          
                              
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                     
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 p-b-35">
                        <div class="table-responsive" id="listall">
                            <table class="table table-bordered table-striped table-responsive " id="dynamic-table" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th style="width:50px;">S.N</th>
                                        <th>COMPANY NAME</th>
                                        <th>ADDRESS</th>
                                        <th style="width:150px;">ISSUED CAPITAL</th>
                                        <th style="width:150px;">LISTED CAPITAL</th>
                                        <th>RTA NAME</th>
                                    </tr>
                                </thead>
                                <tbody>
                              <?php     $issuer =  DB::table('issuer')->orderBy('name','ASC')->where('status',1)->get();?>

                                    <?php $i =0;?>
                                    @foreach($issuer as $data)
                            <tr>
                                <td style="width:50px;"><?php echo ++$i;?></td>
                                <td style="word-wrap: break-word; text-transform:uppercase">{{  $data->name }}</td>
                                <td style="word-wrap: break-word;">{{  $data->address.' '. $data->phone.' '. $data->email }}</td>
                                <td style="word-wrap: break-word;width:150px;">{{  number_format($data->issued_capital) }}</td>
                                <td style="word-wrap: break-word;width:150px;">{{ number_format( $data->listed_capital) }}</td>
                                <?php $rta = DB::table('issuer')->leftJoin('rta', 'issuer.rta_id', '=', 'rta.rta_id')->where('issuer_id',$data->issuer_id)->get(); ?>
                 @foreach($rta as $data1)
                 <td style="word-wrap: break-word;">{{ $data1->name }}</td>
                  @endforeach
                  
                            
                            </tr>
                                @endforeach
                                </tbody>
                            </table>
                       
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
         $("#btn_search_si").click(function(){
             var script_name = $("#script_name").val();
             var isin = $("#isin").val();
             var comany_name =$("#comany_name").val();
             
             $.ajax({
                 type: "POST",
                 url: "http://itsutra.com.np/cdscnp/Site/searchRegisteredCompany",
                 data: {script_name:script_name,isin:isin,comany_name:comany_name},
                 cache: false,
                 success: function(data) {
                     $("#listall").empty().html(data);
                 }
             }); 
         });
    });
</script> 
<!-- Footer Start -->
@include('Home.layouts.footer')
