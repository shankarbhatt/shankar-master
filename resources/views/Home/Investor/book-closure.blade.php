 @include('Home.layouts.header')
@include('Home.layouts.nav')
<!-- End Header Navigation -->
                        <section id="solution" class="p-t-60 p-b-40">
    <div class="container">
        <div id="book-closure" class="row">
            <div class="col-md-12 p-b-50" id="dp">
                <div class="heading_border bg_red"></div>
                <h2>Book Closure Info</h2>
                    <div class="row">
                        <form id="myForm">
                            <div class="col-md-3 col-sm-4">
                                <div class="single-query">
                                    <select class="chosen-select" id="company_name">
                                        <option value=" " disabled="disabled" selected="selected">Select Company</option>
                                                                                <option value="Nepal SBI Bank Ltd- Ordinary Share">Nepal SBI Bank Ltd- Ordinary Share</option>
                                                                                <option value="BANK OF KATHMANDU LIMITED - ORDINARY SHARE">BANK OF KATHMANDU LIMITED - ORDINARY SHARE</option>
                                                                                <option value="Siddhartha Insurance Limited- Ordinary Share">Siddhartha Insurance Limited- Ordinary Share</option>
                                                                                <option value="LAXMI BANK LIMITED - ORDINARY SHARE">LAXMI BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="CITIZENS BANK INTERNATIONAL LIMITED - ORDINARY SHARE">CITIZENS BANK INTERNATIONAL LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SHIKHAR INSURANCE COMPANY LIMITED - ORDINARY SHARE">SHIKHAR INSURANCE COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="HIMALAYAN GENERAL INSURANCE COMPANY LIMITED - ORDINARY SHARE">HIMALAYAN GENERAL INSURANCE COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="PRIMELIFE INSURANCE COMPANY LIMITED - ORDINARY SHARE">PRIMELIFE INSURANCE COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="EVEREST FINANCE LIMITED - ORDINARY SHARE">EVEREST FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="ACE DEVELOPMENT BANK LIMITED - ORDINARY SHARE">ACE DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="AGRICULTURAL DEVELOPMENT BANK-ORDINARY SHARE">AGRICULTURAL DEVELOPMENT BANK-ORDINARY SHARE</option>
                                                                                <option value="SANIMA MAI HYDROPOWER LIMITED - ORDINARY SHARE">SANIMA MAI HYDROPOWER LIMITED - ORDINARY SHARE</option>
                                                                                <option value="BIRATLAXMI BIKAS BANK LIMITED -PROMOTER SHARE">BIRATLAXMI BIKAS BANK LIMITED -PROMOTER SHARE</option>
                                                                                <option value="SIDDHARTHA INVESTMENT GROWTH SCHEME-1- MUTUAL FUND">SIDDHARTHA INVESTMENT GROWTH SCHEME-1- MUTUAL FUND</option>
                                                                                <option value="HIMALAYAN BANK LIMITED - PROMOTER SHARE">HIMALAYAN BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="NAGBELI LAGHUBITTA BIKAS BANK LIMITED-ORDINARY SHARE">NAGBELI LAGHUBITTA BIKAS BANK LIMITED-ORDINARY SHARE</option>
                                                                                <option value="DEPROSC LAGHUBITTA BIKAS  BANK LIMITED - ORDINARY SHARE">DEPROSC LAGHUBITTA BIKAS  BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="DEPROSC LAGHUBITTA BIKAS BANK LIMITED - PROMOTER SHARE">DEPROSC LAGHUBITTA BIKAS BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="SUMMIT MICRO FINANCE DEVELOPMENT BANK LIMITED-PROMOTER SHARE">SUMMIT MICRO FINANCE DEVELOPMENT BANK LIMITED-PROMOTER SHARE</option>
                                                                                <option value="ARANIKO DEVELOPMENT BANK LIMITED- PROMOTER SHARE">ARANIKO DEVELOPMENT BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="INTERNATIONAL LEASING AND FINANCE COMPANY LIMITED- PROMOTER SHARE">INTERNATIONAL LEASING AND FINANCE COMPANY LIMITED- PROMOTER SHARE</option>
                                                                                <option value="TOURISM DEVELOPMENT BANK LIMITED - ORDINARY SHARE">TOURISM DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="TOURISM DEVELOPMENT BANK LIMITED- PROMOTER SHARE">TOURISM DEVELOPMENT BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="ARUN VALLEY HYDROPOWER DEVELOPMENT COMPANY LIMITED - ORDINARY SHARE">ARUN VALLEY HYDROPOWER DEVELOPMENT COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="MAHILA SAHAYATRA MICROFINANCE BITTIYA SANSTHA LIMITED - ORDINARY SHARE">MAHILA SAHAYATRA MICROFINANCE BITTIYA SANSTHA LIMITED - ORDINARY SHARE</option>
                                                                                <option value="MAHILA SAHAYATRA MICROFINANCE BITTIYA SANSTHA LIMITED - PROMOTER SHARE">MAHILA SAHAYATRA MICROFINANCE BITTIYA SANSTHA LIMITED - PROMOTER SHARE</option>
                                                                                <option value="MAHAKALI BIKAS BANK LIMITED - PROMOTER SHARE">MAHAKALI BIKAS BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="MANASLU BIKASH BANK LIMITED - ORDINARY SHARE">MANASLU BIKASH BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="CIVIL BANK LIMITED -PROMOTER SHARE">CIVIL BANK LIMITED -PROMOTER SHARE</option>
                                                                                <option value="WESTERN DEVELOPMENT BANK LIMITED- PROMOTER SHARE">WESTERN DEVELOPMENT BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="PRIMELIFE INSURANCE COMPANY LIMITED- PROMOTER SHARE">PRIMELIFE INSURANCE COMPANY LIMITED- PROMOTER SHARE</option>
                                                                                <option value="NMB  MICROFINANCE BITTIYA SANSTHA LIMITED - PROMOTER SHARE">NMB  MICROFINANCE BITTIYA SANSTHA LIMITED - PROMOTER SHARE</option>
                                                                                <option value="MANJUSHREE FINANCE LIMITED - ORDINARY SHARE">MANJUSHREE FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="PRABHU  BANK LIMITED - ORDINARY SHARE">PRABHU  BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="RELIANCE FINANCE LIMITED - ORDINARY SHARE">RELIANCE FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="RELIANCE FINANCE LIMITED - PROMOTER SHARE">RELIANCE FINANCE LIMITED - PROMOTER SHARE</option>
                                                                                <option value="PREMIER INSURANCE COMPANY (NEPAL) LIMITED - ORDINARY SHARE">PREMIER INSURANCE COMPANY (NEPAL) LIMITED - ORDINARY SHARE</option>
                                                                                <option value="BAGMATI DEVELOPMENT BANK LIMITED - ORDINARY SHARE">BAGMATI DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="BAGMATI DEVELOPMENT BANK LIMITED -PROMOTER SHARE">BAGMATI DEVELOPMENT BANK LIMITED -PROMOTER SHARE</option>
                                                                                <option value="PRIME COMMERCIAL BANK LIMITED - ORDINARY SHARE">PRIME COMMERCIAL BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="PRIME COMMERCIAL BANK LIMITED - PROMOTER SHARE">PRIME COMMERCIAL BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="CENTURY COMMERCIAL BANK LIMITED- PROMOTER SHARE">CENTURY COMMERCIAL BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="NABIL BALANCED FUND 1-MUTUAL FUND">NABIL BALANCED FUND 1-MUTUAL FUND</option>
                                                                                <option value="FEWA BIKAS BANK LIMITED - PROMOTER SHARE">FEWA BIKAS BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="GENERAL FINANCE LIMITED- PROMOTER SHARE">GENERAL FINANCE LIMITED- PROMOTER SHARE</option>
                                                                                <option value="NIRDHAN UTTHAN BANK LIMITED - ORDINARY SHARE">NIRDHAN UTTHAN BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NMB SULAV INVESTMENT FUND I - MUTUAL FUND">NMB SULAV INVESTMENT FUND I - MUTUAL FUND</option>
                                                                                <option value="SRI RAM SUGAR MILLS LIMITED - ORDINARY SHARE">SRI RAM SUGAR MILLS LIMITED - ORDINARY SHARE</option>
                                                                                <option value="PACIFIC DEVELOPMENT BANK LIMITED- PROMOTER SHARE">PACIFIC DEVELOPMENT BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="VIBOR SOCIETY DEVELOPMENT BANK LIMITED - ORDINARY SHARE">VIBOR SOCIETY DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="MACHHAPUCHCHHRE BANK LIMITED - PROMOTER SHARE">MACHHAPUCHCHHRE BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="GRAND BANK NEPAL LIMITED - ORDINARY SHARE">GRAND BANK NEPAL LIMITED - ORDINARY SHARE</option>
                                                                                <option value="RURAL MICROFINANCE DEVELOPMENT CENTRE LIMITED - ORDINARY SHARE">RURAL MICROFINANCE DEVELOPMENT CENTRE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="MITERI DEVELOPMENT BANK LIMITED - ORDINARY SHARE">MITERI DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="WESTERN DEVELOPMENT BANK LIMITED - ORDINARY SHARE">WESTERN DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="VIBOR SOCIETY DEVELOPMENT  BANK LIMITED - PROMOTER SHARE">VIBOR SOCIETY DEVELOPMENT  BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="SOALTEE HOTEL LIMITED - ORDINARY SHARE">SOALTEE HOTEL LIMITED - ORDINARY SHARE</option>
                                                                                <option value="INNOVATIVE DEVELOPMENT BANK LIMITED- PROMOTER SHARE">INNOVATIVE DEVELOPMENT BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="PRABHU INSURANCE LIMITED- ORDINARY SHARE">PRABHU INSURANCE LIMITED- ORDINARY SHARE</option>
                                                                                <option value="PROGRESSIVE FINANCE LIMITED - ORDINARY SHARE">PROGRESSIVE FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="PROGRESSIVE FINANCE LIMITED - PROMOTER SHARE">PROGRESSIVE FINANCE LIMITED - PROMOTER SHARE</option>
                                                                                <option value="NMB MICROFINANCE BITTIYA SANSTHA LIMITED -ORDINARY SHARE">NMB MICROFINANCE BITTIYA SANSTHA LIMITED -ORDINARY SHARE</option>
                                                                                <option value="MERO MICROFINANCE BITTIYA SANSTHA LIMITED - ORDINARY SHARE">MERO MICROFINANCE BITTIYA SANSTHA LIMITED - ORDINARY SHARE</option>
                                                                                <option value="MERO MICROFINANCE BITTIYA SANSTHA LIMITED - PROMOTER SHARE">MERO MICROFINANCE BITTIYA SANSTHA LIMITED - PROMOTER SHARE</option>
                                                                                <option value="MISSION DEVELOPMENT BANK LIMITED - ORDINARY SHARE">MISSION DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="LAXMI BANK LIMITED - PROMOTER SHARE">LAXMI BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="SAHAYOGI VIKAS BANK LIMITED - ORDINARY SHARE">SAHAYOGI VIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SAHAYOGI VIKAS BANK LIMITED - PROMOTER SHARE">SAHAYOGI VIKAS BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="MAHALAXMI BIKAS  BANK  LIMITED - ORDINARY SHARE">MAHALAXMI BIKAS  BANK  LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SWABALAMBAN LAGHUBITTA BIKAS BANK LIMITED - PROMOTER SHARE">SWABALAMBAN LAGHUBITTA BIKAS BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="NAMASTE BITTIYA SANSTHA LIMITED - ORDINARY SHARE">NAMASTE BITTIYA SANSTHA LIMITED - ORDINARY SHARE</option>
                                                                                <option value="GANDAKI BIKAS BANK LIMITED - ORDINARY SHARE">GANDAKI BIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SIDDHARTHA FINANCE LIMITED - ORDINARY SHARE">SIDDHARTHA FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SYNERGY FINANCE LIMITED - ORDINARY SHARE">SYNERGY FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SYNERGY FINANCE LIMITED - PROMOTER SHARE">SYNERGY FINANCE LIMITED - PROMOTER SHARE</option>
                                                                                <option value="UNION FINANCE LIMITED - ORDINARY SHARE">UNION FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="UNION  FINANCE LIMITED - PROMOTER SHARE">UNION  FINANCE LIMITED - PROMOTER SHARE</option>
                                                                                <option value="NAYA NEPAL LAGHU BITTA BIKAS BANK LIMITED - ORDINARY SHARE">NAYA NEPAL LAGHU BITTA BIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="RASTRIYA BEEMA COMPANY LIMITED - ORDINARY SHARE">RASTRIYA BEEMA COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="RASTRIYA BEEMA COMPANY LIMITED - PROMOTER SHARE">RASTRIYA BEEMA COMPANY LIMITED - PROMOTER SHARE</option>
                                                                                <option value="SUPREME DEVELOPMENT BANK LIMITED - PROMOTER SHARE">SUPREME DEVELOPMENT BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="MUKTINATH BIKAS BANK LIMITED - ORDINARY SHARE">MUKTINATH BIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="GURKHAS FINANCE LIMITED - PROMOTER SHARE">GURKHAS FINANCE LIMITED - PROMOTER SHARE</option>
                                                                                <option value="NB INSURANCE COMPANY LIMITED - PROMOTER SHARE">NB INSURANCE COMPANY LIMITED - PROMOTER SHARE</option>
                                                                                <option value="NABIL BANK LIMITED - ORDINARY SHARE">NABIL BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NIC ASIA BANK LIMITED - ORDINARY SHARE">NIC ASIA BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="KUMARI BANK LIMITED - ORDINARY SHARE">KUMARI BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="MAHAKALI BIKAS BANK LIMITED - ORDINARY SHARE">MAHAKALI BIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SIDDHARTHA FINANCE LIMITED - PROMOTER SHARE">SIDDHARTHA FINANCE LIMITED - PROMOTER SHARE</option>
                                                                                <option value="NEPAL COMMUNITY DEVELOPMENT BANK LIMITED - ORDINARY SHARE">NEPAL COMMUNITY DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="COUNTRY DEVELOPMENT BANK LIMITED- PROMOTER SHARE">COUNTRY DEVELOPMENT BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="SIDDHARTHA EQUITY ORIENTED SCHEME- MUTUAL FUND">SIDDHARTHA EQUITY ORIENTED SCHEME- MUTUAL FUND</option>
                                                                                <option value="SAJHA BIKAS BANK LIMITED- ORDINARY SHARE">SAJHA BIKAS BANK LIMITED- ORDINARY SHARE</option>
                                                                                <option value="SAJHA BIKAS BANK LIMITED- PROMOTER SHARE">SAJHA BIKAS BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="JANATA BANK NEPAL LIMITED - ORDINARY SHARE">JANATA BANK NEPAL LIMITED - ORDINARY SHARE</option>
                                                                                <option value="MACHHAPUCHCHHRE BANK LIMITED - ORDINARY SHARE">MACHHAPUCHCHHRE BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NABIL BANK LIMITED - PROMOTER SHARE">NABIL BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="ICFC FINANCE LIMITED - PROMOTER SHARE">ICFC FINANCE LIMITED - PROMOTER SHARE</option>
                                                                                <option value="KASTHAMANDAP DEVELOPMENT BANK LIMITED - ORDINARY SHARE">KASTHAMANDAP DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="KASTHAMANDAP DEVELOPMENT BANK LIMITED - PROMOTER SHARE">KASTHAMANDAP DEVELOPMENT BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="BUSINESS UNIVERSAL DEVELOPMENT BANK LIMITED - ORDINARY SHARE">BUSINESS UNIVERSAL DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NEPAL BANGLADESH BANK LIMITED - PROMOTER SHARE">NEPAL BANGLADESH BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="NEPAL CREDIT AND COMMERCE BANK LIMITED - PROMOTER SHARE">NEPAL CREDIT AND COMMERCE BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="EVEREST BANK LIMITED - CONVERTIBLE PREFERENCE SHARE">EVEREST BANK LIMITED - CONVERTIBLE PREFERENCE SHARE</option>
                                                                                <option value="SANA KISAN BIKAS BANK LIMITED - ORDINARY SHARE">SANA KISAN BIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NARAYANI NATIONAL FINANCE LIMITED - ORDINARY SHARE">NARAYANI NATIONAL FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="VIJAYA LAGHUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE">VIJAYA LAGHUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE</option>
                                                                                <option value="VIJAYA LAGHUBITTA BITTIYA SANSTHA LIMITED - PROMOTER SHARE">VIJAYA LAGHUBITTA BITTIYA SANSTHA LIMITED - PROMOTER SHARE</option>
                                                                                <option value="GOODWILL FINANCE LIMITED - PROMOTER SHARE">GOODWILL FINANCE LIMITED - PROMOTER SHARE</option>
                                                                                <option value="NEPAL LIFE INSURANCE COMPANY LIMITED - PROMOTER SHARE">NEPAL LIFE INSURANCE COMPANY LIMITED - PROMOTER SHARE</option>
                                                                                <option value="NIDC DEVELOPMENT BANK LIMITED - ORDINARY SHARE">NIDC DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="ALPINE DEVELOPMENT  BANK LIMITED - ORDINARY SHARE">ALPINE DEVELOPMENT  BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="GURANS LIFE INSURANCE COMPANY LIMITED - ORDINARY SHARE">GURANS LIFE INSURANCE COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="MITERI DEVELOPMENT BANK LIMITED - PROMOTER SHARE">MITERI DEVELOPMENT BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="JYOTI BIKASH BANK LIMITED - ORDINARY SHARE">JYOTI BIKASH BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="MALIKA VIKAS  BANK LIMITED - ORDINARY SHARE">MALIKA VIKAS  BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="PRABHU INSURANCE LIMITED-PROMOTER SHARE">PRABHU INSURANCE LIMITED-PROMOTER SHARE</option>
                                                                                <option value="MOUNT MAKALU DEVELOPMENT BANK LIMITED - ORDINARY SHARE">MOUNT MAKALU DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="MOUNT MAKALU DEVELOPMENT BANK LIMITED - PROMOTER SHARE">MOUNT MAKALU DEVELOPMENT BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="CIVIL BANK LIMITED - ORDINARY SHARE">CIVIL BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NEPAL BANGLADESH BANK LIMITED - ORDINARY SHARE">NEPAL BANGLADESH BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="LUMBINI BANK LIMITED - ORDINARY SHARE">LUMBINI BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NIC ASIA BANK LIMITED - PROMOTER SHARE">NIC ASIA BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="EVEREST BANK LIMITED - ORDINARY SHARE">EVEREST BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="EVEREST BANK LIMITED - PROMOTOR SHARE">EVEREST BANK LIMITED - PROMOTOR SHARE</option>
                                                                                <option value="SWABALAMBAN LAGHUBITTA BIKAS BANK LIMITED - ORDINARY SHARE">SWABALAMBAN LAGHUBITTA BIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="UNITED FINANCE LIMITED - ORDINARY SHARE">UNITED FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="UNITED FINANCE LIMITED - PROMOTER SHARE">UNITED FINANCE LIMITED - PROMOTER SHARE</option>
                                                                                <option value="COUNTRY DEVELOPMENT BANK LIMITED - ORDINARY SHARE">COUNTRY DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SANA KISAN BIKAS BANK LIMITED-PROMOTER SHARE">SANA KISAN BIKAS BANK LIMITED-PROMOTER SHARE</option>
                                                                                <option value="CHILIME HYDROPOWER COMPANY LIMITED - ORDINARY SHARE">CHILIME HYDROPOWER COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SIDDHARTHA BANK LIMITED - ORDINARY SHARE">SIDDHARTHA BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="TINAU DEVELOPMENT BANK LIMITED - ORDINARY SHARE">TINAU DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="TINAU DEVELOPMENT BANK LIMITED - PROMOTER SHARE">TINAU DEVELOPMENT BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="LUMBINI GENERAL INSURANCE COMPANY LIMITED - ORDINARY SHARE">LUMBINI GENERAL INSURANCE COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SHIKHAR INSURANCE COMPANY LIMITED - PROMOTER SHARE">SHIKHAR INSURANCE COMPANY LIMITED - PROMOTER SHARE</option>
                                                                                <option value="EXCEL DEVELOPMENT BANK LIMITED - PROMOTER SHARE">EXCEL DEVELOPMENT BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="DEVA BIKAS BANK LIMITED - ORDINARY SHARE">DEVA BIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="DEVA BIKAS BANK LIMITED  - PROMOTER SHARE">DEVA BIKAS BANK LIMITED  - PROMOTER SHARE</option>
                                                                                <option value="KALIKA MICROCREDIT DEVELOPMENT BANK LIMITED - ORDINARY SHARE">KALIKA MICROCREDIT DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="GUHESWORI MERCHANT BANKING AND FINANCE LIMITED - ORDINARY SHARE">GUHESWORI MERCHANT BANKING AND FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="JANAKI FINANCE LIMITED-PROMOTER SHARE">JANAKI FINANCE LIMITED-PROMOTER SHARE</option>
                                                                                <option value="NMB BANK LIMITED - ORDINARY SHARE">NMB BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NMB BANK LIMITED - PROMOTER SHARE">NMB BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="SANIMA BANK LIMITED - PROMOTER SHARE">SANIMA BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="TARAGAON REGENCY HOTELS LIMITED - ORDINARY SHARE">TARAGAON REGENCY HOTELS LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SUMMIT MICROFINANCE DEVELOPMENT BANK LIMITED - ORDINARY SHARE">SUMMIT MICROFINANCE DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="OM DEVELOPMENT BANK LIMITED - ORDINARY SHARE">OM DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="OM DEVELOPMENT BANK LIMITED - PROMOTER SHARE">OM DEVELOPMENT BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="PROFESSIONAL DIYALO BIKAS BANK LIMITED - ORDINARY SHARE">PROFESSIONAL DIYALO BIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="PRUDENTIAL INSURANCE COMPANY LIMITED - ORDINARY SHARE">PRUDENTIAL INSURANCE COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NLG INSURANCE COMPANY LIMITED- PROMOTER SHARE">NLG INSURANCE COMPANY LIMITED- PROMOTER SHARE</option>
                                                                                <option value="SAGARMATHA FINANCE LIMITED- PROMOTER SHARE">SAGARMATHA FINANCE LIMITED- PROMOTER SHARE</option>
                                                                                <option value="MULTIPURPOSE FINANCE COMPANUY LIMITED - PROMOTER SHARE">MULTIPURPOSE FINANCE COMPANUY LIMITED - PROMOTER SHARE</option>
                                                                                <option value="SUNRISE BANK LIMITED - ORDINARY SHARE">SUNRISE BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="MEGA BANK NEPAL LIMITED - ORDINARY SHARE">MEGA BANK NEPAL LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NEPAL INVESTMENT BANK LIMITED - ORDINARY SHARE">NEPAL INVESTMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="KUMARI BANK LIMITED - PROMOTER SHARE">KUMARI BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="NEPAL DOORSANCHAR COMPANY LIMITED (NTC) - ORDINARY SHARE">NEPAL DOORSANCHAR COMPANY LIMITED (NTC) - ORDINARY SHARE</option>
                                                                                <option value="BIRATLAXMI BIKASH BANK LIMITED - ORDINARY SHARE">BIRATLAXMI BIKASH BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="BHARGAV BIKASH BANK LIMITED - ORDINARY SHARE">BHARGAV BIKASH BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="BHARGAV BIKASH BANK LIMITED - PROMOTER SHARE">BHARGAV BIKASH BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="SUBHECHHA BIKASH BANK LIMITED-PROMOTER SHARE">SUBHECHHA BIKASH BANK LIMITED-PROMOTER SHARE</option>
                                                                                <option value="LUMBINI FINANCE AND LEASING COMPANY LIMITED- PROMOTER SHARE">LUMBINI FINANCE AND LEASING COMPANY LIMITED- PROMOTER SHARE</option>
                                                                                <option value="GLOBAL IME SAMMUNAT SCHEME -1">GLOBAL IME SAMMUNAT SCHEME -1</option>
                                                                                <option value="GAUMUKHEE BIKASH BANK LIMITED - ORDINARY SHARE">GAUMUKHEE BIKASH BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="GRAND BANK NEPAL LIMITED - PROMOTER SHARE">GRAND BANK NEPAL LIMITED - PROMOTER SHARE</option>
                                                                                <option value="UNILEVER NEPAL LIMITED - ORDINARY SHARE">UNILEVER NEPAL LIMITED - ORDINARY SHARE</option>
                                                                                <option value="LAXMI LAGHUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE">LAXMI LAGHUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE</option>
                                                                                <option value="LAXMI LAGHUBITTA BITTIYA SASNTHA LIMITED - PROMOTER SHARE">LAXMI LAGHUBITTA BITTIYA SASNTHA LIMITED - PROMOTER SHARE</option>
                                                                                <option value="TRIVENI BIKAS BANK LIMITED - ORDINARY SHARE">TRIVENI BIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="ALPINE DEVELOPMENT BANK LIMITED-PROMOTER SHARE">ALPINE DEVELOPMENT BANK LIMITED-PROMOTER SHARE</option>
                                                                                <option value="SRIJANA FINANCE LIMITED - ORDINARY SHARE">SRIJANA FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="JANAUTTHAN SAMUDAYIC LAGHUBITTA BIKASH BANK LIMITED -ORDINARY SHARE">JANAUTTHAN SAMUDAYIC LAGHUBITTA BIKASH BANK LIMITED -ORDINARY SHARE</option>
                                                                                <option value="JANAUTTHAN SAMUDAYIC LAGHUBITTA BIKASH BANK LIMITED - PROMOTER SHARE">JANAUTTHAN SAMUDAYIC LAGHUBITTA BIKASH BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="SWAROJGAR LAGHUBITTA BIKAS BANK LIMITED - ORDINARY SHARE">SWAROJGAR LAGHUBITTA BIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="STANDARD CHARTERED BANK NEPAL LIMITED - ORDINARY SHARE">STANDARD CHARTERED BANK NEPAL LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SUNRISE BANK LIMITED - PROMOTER SHARE">SUNRISE BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="BANK OF KATHMANDU LIMITED - PROMOTOR SHARE">BANK OF KATHMANDU LIMITED - PROMOTOR SHARE</option>
                                                                                <option value="ACE DEVELOPMENT BANK LIMITED -  PROMOTER SHARE">ACE DEVELOPMENT BANK LIMITED -  PROMOTER SHARE</option>
                                                                                <option value="GARIMA BIKAS BANK LIMITED - PROMOTER SHARE">GARIMA BIKAS BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="SAGARMATHA INSURANCE COMPANY LIMITED - ORDINARY SHARE">SAGARMATHA INSURANCE COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="PACIFIC DEVELOPMENT BANK LIMITED - ORDINARY SHARE">PACIFIC DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NEPAL LIFE INSURANCE COMPANY LIMITED - ORDINARY SHARE">NEPAL LIFE INSURANCE COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="HIMALAYAN BANK LIMITED - ORDINARY SHARE">HIMALAYAN BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="BUTWAL POWER COMPANY LIMITED - ORDINARY SHARE">BUTWAL POWER COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SHINE RESUNGA DEVELOPMENT BANK LIMITED - ORDINARY SHARE">SHINE RESUNGA DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SAGARMATHA INSURANCE COMPANY LIMITED - PROMOTER SHARE">SAGARMATHA INSURANCE COMPANY LIMITED - PROMOTER SHARE</option>
                                                                                <option value="NEPAL SBI BANK LIMITED- PROMOTER SHARE">NEPAL SBI BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="PREMIER INSURANCE COMPANY (NEPAL) LIMITED- PROMOTER SHARE">PREMIER INSURANCE COMPANY (NEPAL) LIMITED- PROMOTER SHARE</option>
                                                                                <option value="KANCHAN DEVELOPMENT BANK LIMITED- PROMOTER SHARE">KANCHAN DEVELOPMENT BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="EXCEL DEVELOPMENT BANK LIMITED - ORDINARY SHARE">EXCEL DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NEPAL BANK LIMITED - ORDINARY SHARE">NEPAL BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="LUMBINI BANK LIMITED - PROMOTER SHARE">LUMBINI BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="GARIMA BIKAS BANK LIMITED - ORDINARY SHARE">GARIMA BIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="KANKAI BIKAS BANK LIMITED - ORDINARY SHARE">KANKAI BIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="YETI DEVELOPMENT BANK LIMITED - ORDINARY SHARE">YETI DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NEPAL COMMUNITY DEVELOPMENT BANK LIMITED- PROMOTER SHARE">NEPAL COMMUNITY DEVELOPMENT BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="NEPAL INSURANCE COMPANY LIMITED - ORDINARY SHARE">NEPAL INSURANCE COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="MULTIPURPOSE FINANCE COMPANY LIMITED - ORDINARY SHARE">MULTIPURPOSE FINANCE COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NEPAL CREDIT AND COMMERCE BANK LIMITED - ORDINARY SHARE">NEPAL CREDIT AND COMMERCE BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SANIMA BANK LIMITED - ORDINARY SHARE">SANIMA BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="EVEREST INSURANCE COMPANY LIMITED - ORDINARY SHARE">EVEREST INSURANCE COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="JEBIL'S FINANCE LIMITED - ORDINARY SHARE">JEBIL'S FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="COSMOS DEVELOPMENT BANK LIMITED - ORDINARY SHARE">COSMOS DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NEPAL EXPRESS FINANCE LIMITED- PROMOTER SHARE">NEPAL EXPRESS FINANCE LIMITED- PROMOTER SHARE</option>
                                                                                <option value="BISHAL BAZAR COMPANY LIMITED - ORDINARY SHARE">BISHAL BAZAR COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SUPREME DEVELOPMENT BANK LIMITED - ORDINARY SHARE">SUPREME DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="ICFC FINANCE LIMITED - ORDINARY SHARE">ICFC FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="GAUMUKHEE BIKAS BANK LIMITED-PROMOTER SHARE">GAUMUKHEE BIKAS BANK LIMITED-PROMOTER SHARE</option>
                                                                                <option value="BHAKTAPUR FINANCE COMPANY LIMITED - ORDINARY SHARE">BHAKTAPUR FINANCE COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="GURANS LIFE INSURANCE COMPANY LIMITED - PROMOTER SHARE">GURANS LIFE INSURANCE COMPANY LIMITED - PROMOTER SHARE</option>
                                                                                <option value="SIDDHARTHA DEVELOPMENT BANK LIMITED - ORDINARY SHARE">SIDDHARTHA DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SIDDHARTHA DEVELOPMENT BANK LIMITED - PROMOTER SHARE">SIDDHARTHA DEVELOPMENT BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="RURAL MICROFINANCE DEVELOPMENT CENTRE LIMITED - PROMOTER SHARE">RURAL MICROFINANCE DEVELOPMENT CENTRE LIMITED - PROMOTER SHARE</option>
                                                                                <option value="GANDAKI BIKAS BANK LIMITED -PROMOTER SHARE">GANDAKI BIKAS BANK LIMITED -PROMOTER SHARE</option>
                                                                                <option value="WOMI MICROFINANCE BITTIYA SANSTHA LIMITED- ORDINARY SHARE">WOMI MICROFINANCE BITTIYA SANSTHA LIMITED- ORDINARY SHARE</option>
                                                                                <option value="WOMI MICROFINANCE BITTIYA SANSTHA LIMITED- PROMOTER SHARE">WOMI MICROFINANCE BITTIYA SANSTHA LIMITED- PROMOTER SHARE</option>
                                                                                <option value="KANKREBIHAR BIKAS BANK LIMITED - ORDINARY SHARE">KANKREBIHAR BIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="KANKREBIHAR BIKAS BANK LIMITED- PROMOTER SHARE">KANKREBIHAR BIKAS BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="BOTTLERS NEPAL LIMITED - ORDINARY SHARE">BOTTLERS NEPAL LIMITED - ORDINARY SHARE</option>
                                                                                <option value="BOTTLERS NEPAL (TERAI) LIMITED - ORDINARY SHARE">BOTTLERS NEPAL (TERAI) LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NERUDE LAGHUBITA BIKAS BANK LIMITED - ORDINARY SHARE">NERUDE LAGHUBITA BIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="APEX DEVELOPMENT BANK LIMITED - ORDINARY SHARE">APEX DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SEWA BIKAS BANK LIMITED - ORDINARY SHARE">SEWA BIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="UNIQUE FINACNE LIMITED- PROMOTER SHARE">UNIQUE FINACNE LIMITED- PROMOTER SHARE</option>
                                                                                <option value="SHANGRILA DEVELOPMENT BANK LIMITED - ORDINARY SHARE">SHANGRILA DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="KAILASH BIKAS BANK LIMITED - ORDINARY SHARE">KAILASH BIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="KAILASH BIKASH BANK LIMITED - PROMOTER SHARE">KAILASH BIKASH BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="JYOTI BIKASH BANK LIMITED - PROMOTER SHARE">JYOTI BIKASH BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="NEPAL AAWAS FINANCE LIMITED - ORDINARY SHARE">NEPAL AAWAS FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="APEX DEVELOPMENT BANK LIMITED- PROMOTER SHARE">APEX DEVELOPMENT BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="JALAVIDHYUT LAGANI TATHA BIKAS COMPANY LIMITED - ORDINARY SHARE">JALAVIDHYUT LAGANI TATHA BIKAS COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="CITIZENS BANK INTERNATIONAL LIMITED - PROMOTER SHARE">CITIZENS BANK INTERNATIONAL LIMITED - PROMOTER SHARE</option>
                                                                                <option value="CHHIMEK LAGHUBITTA BIKAS BANK LIMITED - ORDINARY SHARE">CHHIMEK LAGHUBITTA BIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="CHHIMEK LAGHUBITTA BIKAS BANK LIMITED- PROMOTER SHARE">CHHIMEK LAGHUBITTA BIKAS BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="SOCIETY DEVELOPMENT BANK LIMITED - PROMOTER SHARE">SOCIETY DEVELOPMENT BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="KISAN MICRO FINANCE BITTIYA SANSTHA- ORDINARY SHARE">KISAN MICRO FINANCE BITTIYA SANSTHA- ORDINARY SHARE</option>
                                                                                <option value="KISAN MICRO FINANCE BITTIYA SANSTHA LIMITED- PROMOTER SAHRE">KISAN MICRO FINANCE BITTIYA SANSTHA LIMITED- PROMOTER SAHRE</option>
                                                                                <option value="SHREE INVESTMENT AND FINANCE COMPANY LIMITED - ORDINARY SHARE">SHREE INVESTMENT AND FINANCE COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="EVEREST FINANCE LIMITED -  PROMOTER SHARE">EVEREST FINANCE LIMITED -  PROMOTER SHARE</option>
                                                                                <option value="TRIVENI BIKAS BANK LIMITED - PROMOTER SHARE">TRIVENI BIKAS BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="SETI FINANCE LIMITED - ORDINARY SHARE">SETI FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="RELIABLE DEVELOPMENT BANK LIMITED - ORDINARY SHARE">RELIABLE DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="RELIABLE DEVELOPMENT BANK LIMITED- PROMOTER SHARE">RELIABLE DEVELOPMENT BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="BHAKTAPUR FINANCE COMPANY LIMITED- PROMOTER SHARE">BHAKTAPUR FINANCE COMPANY LIMITED- PROMOTER SHARE</option>
                                                                                <option value="KANCHAN DEVELOPMENT BANK LIMITED - ORDINARY SHARE">KANCHAN DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="FEWA BIKAS BANK LIMITED - ORDINARY SHARE">FEWA BIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="GENERAL FINANCE LIMITED - ORDINARY SHARE">GENERAL FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NEPAL INVESTMENT BANK LIMITED - PROMOTER SHARE">NEPAL INVESTMENT BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="LALITPUR FINANCE LIMITED - ORDINARY SHARE">LALITPUR FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="LALITPUR FINANCE LIMITED - PROMOTER SHARE">LALITPUR FINANCE LIMITED - PROMOTER SHARE</option>
                                                                                <option value="SEWA BIKAS BANK LIMITED - PROMOTER SHARE">SEWA BIKAS BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="NECO INSURANCE COMPANY LIMITED- PROMOTER SHARE">NECO INSURANCE COMPANY LIMITED- PROMOTER SHARE</option>
                                                                                <option value="MITHILA LAGHUBITTA BIKASH BANK LIMITED - ORDINARY SHARE">MITHILA LAGHUBITTA BIKASH BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="ASIAN LIFE INSURANCE COMPANY LIMITED - ORDINARY SHARE">ASIAN LIFE INSURANCE COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="ASIAN LIFE INSURANCE COMPANY LIMITED - PROMOTER SHARE">ASIAN LIFE INSURANCE COMPANY LIMITED - PROMOTER SHARE</option>
                                                                                <option value="INNOVATIVE DEVELOPMENT BANK LIMITED - ORDINARY SHARE">INNOVATIVE DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SALT TRADING CORPORATION LIMITED - ORDINARY SHARE">SALT TRADING CORPORATION LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SURYA LIFE INSURANCE COMPANY LIMITED - ORDINARY SHARE">SURYA LIFE INSURANCE COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SURYA LIFE INSURANCE COMPANY LIMITED- PROMOTER SHARE">SURYA LIFE INSURANCE COMPANY LIMITED- PROMOTER SHARE</option>
                                                                                <option value="LIFE INSURANCE CORPORATION (NEPAL) LIMITED - ORDINARY SHARE">LIFE INSURANCE CORPORATION (NEPAL) LIMITED - ORDINARY SHARE</option>
                                                                                <option value="LIFE INSURANCE CORPORATION (NEPAL) LIMITED- PROMOTER SHARE">LIFE INSURANCE CORPORATION (NEPAL) LIMITED- PROMOTER SHARE</option>
                                                                                <option value="HAMRO BIKAS  BANK LIMITED - ORDINARY SHARE">HAMRO BIKAS  BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="UNITED INSURANCE COMPANY (NEPAL) LIMITED - ORDINARY SHARE">UNITED INSURANCE COMPANY (NEPAL) LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SIDDHARTHA BANK LIMITED - PROMOTER SHARE">SIDDHARTHA BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="NIDC DEVELOPMENT BANK LIMITED- PROMOTER SHARE">NIDC DEVELOPMENT BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="LUMBINI GENERAL INSURANCE COMPANY LIMITED- PROMOTER SHARE">LUMBINI GENERAL INSURANCE COMPANY LIMITED- PROMOTER SHARE</option>
                                                                                <option value="CENTRAL FINANCE   LIMITED - ORDINARY SHARE">CENTRAL FINANCE   LIMITED - ORDINARY SHARE</option>
                                                                                <option value="CENTRAL FINANCE LIMITED -  PROMOTER SHARE">CENTRAL FINANCE LIMITED -  PROMOTER SHARE</option>
                                                                                <option value="SAHARA BIKASH BANK LIMITED- ORDINARY SHARE">SAHARA BIKASH BANK LIMITED- ORDINARY SHARE</option>
                                                                                <option value="SAHARA BIKASH BANK LIMITED-PROMOTER SHARE">SAHARA BIKASH BANK LIMITED-PROMOTER SHARE</option>
                                                                                <option value="SINDHU BIKASH BANK LIMITED - ORDINARY SHARE">SINDHU BIKASH BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="MITHILA LAGHUBITTA BIKASH BANK LIMITED - PROMOTER SHARE">MITHILA LAGHUBITTA BIKASH BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="RELIABLE MICROFINANCE BITTIYA SANSTHA LIMITED- ORDINARY SHARE">RELIABLE MICROFINANCE BITTIYA SANSTHA LIMITED- ORDINARY SHARE</option>
                                                                                <option value="RELIABLE MICROFINANCE BITTIYA SANSTHA LIMITED- PROMOTER SHARE">RELIABLE MICROFINANCE BITTIYA SANSTHA LIMITED- PROMOTER SHARE</option>
                                                                                <option value="CENTURY COMMERCIAL BANK LIMITED - ORDINARY SHARE">CENTURY COMMERCIAL BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="BUSINESS UNIVERSAL DEVELOPMENT BANK LIMITED-PROMOTER SHARE">BUSINESS UNIVERSAL DEVELOPMENT BANK LIMITED-PROMOTER SHARE</option>
                                                                                <option value="INTERNATIONAL LEASING AND FINANCE COMPANY LIMITED - ORDINARY SHARE">INTERNATIONAL LEASING AND FINANCE COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SUBHECHHA BIKAS BANK LIMITED - ORDINARY SHARE">SUBHECHHA BIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="API POWER COMPANY LIMITED- ORDINARY SHARE">API POWER COMPANY LIMITED- ORDINARY SHARE</option>
                                                                                <option value="SAGARMATHA FINANCE LIMITED - ORDINARY SHARE">SAGARMATHA FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="LAXMI VALUE FUND-1- MUTUAL FUND">LAXMI VALUE FUND-1- MUTUAL FUND</option>
                                                                                <option value="PASCHIMANCHAL FINANCE COMPANY LIMITED - ORDINARY SHARE">PASCHIMANCHAL FINANCE COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="PASHCHIMANCHAL FINANCE COMPANY LIMITED - PROMOTER SHARE">PASHCHIMANCHAL FINANCE COMPANY LIMITED - PROMOTER SHARE</option>
                                                                                <option value="CIVIL LAGHUBITTA BITTIYA SANSTHA LIMITED -ORDINARY SHARE">CIVIL LAGHUBITTA BITTIYA SANSTHA LIMITED -ORDINARY SHARE</option>
                                                                                <option value="MAHALAXMI BIKAS BANK  LIMITED - PROMOTER SHARE">MAHALAXMI BIKAS BANK  LIMITED - PROMOTER SHARE</option>
                                                                                <option value="KALINCHOWK DEVELOPMENT BANK LIMITED - ORDINARY SHARE">KALINCHOWK DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="KALINCHOWK DEVELOPMENT BANK LIMITED - PROMOTER SHARE">KALINCHOWK DEVELOPMENT BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="KABELI BIKAS BANK LIMITED - ORDINARY SHARE">KABELI BIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="YETI DEVELOPMENT BANK LIMITED - PROMOTER SHARE">YETI DEVELOPMENT BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="NAYA NEPAL LAGHU BITTA BIKAS BANK LIMITED- PROMOTER SHARE">NAYA NEPAL LAGHU BITTA BIKAS BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="BARUN HYDROPOWER COMPANY LIMITED- ORDINARY SHARE">BARUN HYDROPOWER COMPANY LIMITED- ORDINARY SHARE</option>
                                                                                <option value="UNIQUE FINANCE LIMITED - ORDINARY SHARE">UNIQUE FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="ORIENTAL HOTEL LIMITED - ORDINARY SHARE">ORIENTAL HOTEL LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NIDC CAPITAL MARKETS LIMITED- PROMOTER SHARE">NIDC CAPITAL MARKETS LIMITED- PROMOTER SHARE</option>
                                                                                <option value="MANJUSHREE FINANCE LIMITED- PROMOTER SHARE">MANJUSHREE FINANCE LIMITED- PROMOTER SHARE</option>
                                                                                <option value="CITIZEN INVESTMENT TRUST -  ORDINARY SHARE">CITIZEN INVESTMENT TRUST -  ORDINARY SHARE</option>
                                                                                <option value="HAMRO BIKAS BANK LIMITED - PROMOTER SHARE">HAMRO BIKAS BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="NLG INSURANCE COMPANY LIMITED - ORDINARY SHARE">NLG INSURANCE COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NAGBELI LAGHUBITTA BIKAS BANK LIMITED-PROMOTER SHARE">NAGBELI LAGHUBITTA BIKAS BANK LIMITED-PROMOTER SHARE</option>
                                                                                <option value="SOCIETY DEVELOPMENT BANK LIMITED- ORDINARY SHARE">SOCIETY DEVELOPMENT BANK LIMITED- ORDINARY SHARE</option>
                                                                                <option value="RAPTIBHERI BIKAS BANK LIMITED- ORDINARY SHARE">RAPTIBHERI BIKAS BANK LIMITED- ORDINARY SHARE</option>
                                                                                <option value="RAPTIBHERI BIKAS BANK LIMITED- PROMOTER SHARE">RAPTIBHERI BIKAS BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="PURNIMA BIKAS BANK LIMITED - ORDINARY SHARE">PURNIMA BIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NARAYANI NATIONAL FINANCE LIMITED- PROMOTER SHARE">NARAYANI NATIONAL FINANCE LIMITED- PROMOTER SHARE</option>
                                                                                <option value="SINDHU BIKASH BANK LIMITED- PROMOTER SHARE">SINDHU BIKASH BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="JHIMRUK BIKAS BANK LIMITED - ORDINARY SHARE">JHIMRUK BIKAS BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="HAMA MERCHANT AND FINANCE LIMITED - ORDINARY SHARE">HAMA MERCHANT AND FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="HAMA MERCHANT AND FINACNE LIMITED- PROMOTER SHARE">HAMA MERCHANT AND FINACNE LIMITED- PROMOTER SHARE</option>
                                                                                <option value="INFRASTRUCTURE DEVELOPMENT BANK LIMITED - ORDINARY SHARE">INFRASTRUCTURE DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="STANDARD CHARTERED BANK NEPAL LIMITED- PROMOTER SHARE">STANDARD CHARTERED BANK NEPAL LIMITED- PROMOTER SHARE</option>
                                                                                <option value="COSMOS DEVELOPMENT BANK LIMITED - PROMOTER SHARE">COSMOS DEVELOPMENT BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="SETI FINANCE LIMITED-PROMOTER SHARE">SETI FINANCE LIMITED-PROMOTER SHARE</option>
                                                                                <option value="SRIJANA FINANCE LIMITED- PROMOTER SHARE">SRIJANA FINANCE LIMITED- PROMOTER SHARE</option>
                                                                                <option value="JHIMRUK BIKAS BANK LIMITED- PROMOTER SHARE">JHIMRUK BIKAS BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="JANAKI FINANCE LIMITED - ORDINARY SHARE">JANAKI FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NAMASTE BITTIYA SANSTHA LIMITED- PROMOTER SHARE">NAMASTE BITTIYA SANSTHA LIMITED- PROMOTER SHARE</option>
                                                                                <option value="NATIONAL HYDROPOWER COMPANY LIMITED - ORDINARY SHARE">NATIONAL HYDROPOWER COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="MIRMIRE MICROFINANCE DEVELOPMENT BANK LIMITED - ORDINARY SHARE">MIRMIRE MICROFINANCE DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="MIRMIRE MICROFINANCE DEVELOPMENT BANK LIMITED - PROMOTER SHARE">MIRMIRE MICROFINANCE DEVELOPMENT BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="SAPTAKOSHI DEVELOPMENT BANK LIMITED- ORDINARY SHARE">SAPTAKOSHI DEVELOPMENT BANK LIMITED- ORDINARY SHARE</option>
                                                                                <option value="SAPTAKOSH DEVELOPMENT BANK LIMITED- PROMOTER SHARE">SAPTAKOSH DEVELOPMENT BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="NECO INSURANCE LIMITED - ORDINARY SHARE">NECO INSURANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="ARANIKO DEVELOPMENT BANK LIMITED - ORDINARY SHARE">ARANIKO DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="INTERNATIONAL DEVELOPMENT BANK LIMITED - ORDINARY SHARE">INTERNATIONAL DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="LUMBINI FINANCE AND LEASING COMPANY LIMITED - ORDINARY SHARE">LUMBINI FINANCE AND LEASING COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="HIMALAYAN GENERAL INSURANCE COMPANY LIMITED-PROMOTER SHARE">HIMALAYAN GENERAL INSURANCE COMPANY LIMITED-PROMOTER SHARE</option>
                                                                                <option value="NATIONAL LIFE INSURANCE COMPANY LIMITED - ORDINARY SHARE">NATIONAL LIFE INSURANCE COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NATIONAL LIFE INSURANCE COMPANY LIMITED - PROMOTER SHARE">NATIONAL LIFE INSURANCE COMPANY LIMITED - PROMOTER SHARE</option>
                                                                                <option value="NGADI GROUP POWER LIMITED - ORDINARY SHARE">NGADI GROUP POWER LIMITED - ORDINARY SHARE</option>
                                                                                <option value="PRABHU BANK LIMITED - PROMOTER SHARE">PRABHU BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="NEPAL EXPRESS FINANCE LIMITED - ORDINARY SHARE">NEPAL EXPRESS FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NIDC CAPITAL MARKETS LIMITED - ORDINARY SHARE">NIDC CAPITAL MARKETS LIMITED - ORDINARY SHARE</option>
                                                                                <option value="PURNIMA BIKAS BANK LIMITED- PROMOTER SHARE">PURNIMA BIKAS BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="MEGA BANK NEPAL LIMITED-PROMOTER SHARE">MEGA BANK NEPAL LIMITED-PROMOTER SHARE</option>
                                                                                <option value="NIBL SAMRIDDHI FUND-1- MUTUAL FUND">NIBL SAMRIDDHI FUND-1- MUTUAL FUND</option>
                                                                                <option value="RIDI HYDROPOWER DEVELOPMENT COMPANY LIMITED- ORDINARY SHARE">RIDI HYDROPOWER DEVELOPMENT COMPANY LIMITED- ORDINARY SHARE</option>
                                                                                <option value="GOODWILL FINANCE LIMITED - ORDINARY SHARE">GOODWILL FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="KABELI BIKAS BANK LIMITED - PROMOTER SHARE">KABELI BIKAS BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="POKHARA FINANCE LIMITED - ORDINARY SHARE">POKHARA FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="POKHARA FINANCE LIMITED- PROMOTER SHARE">POKHARA FINANCE LIMITED- PROMOTER SHARE</option>
                                                                                <option value="UNITED INSURANCE COMPANY (NEPAL) LIMITED- PROMOTER SHARE">UNITED INSURANCE COMPANY (NEPAL) LIMITED- PROMOTER SHARE</option>
                                                                                <option value="JEBIL'S FINANCE LIMITED-PROMOTER SHARE">JEBIL'S FINANCE LIMITED-PROMOTER SHARE</option>
                                                                                <option value="KARNALI DEVELOPMENT BANK LIMITED - ORDINARY SHARE">KARNALI DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="KARNALI DEVLOPMENT BANK LIMITED - PROMOTER SHARE">KARNALI DEVLOPMENT BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="GLOBAL IME BANK LIMITED - ORDINARY SHARE">GLOBAL IME BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="GLOBAL IME BANK LIMITED - PROMOTER SHARE">GLOBAL IME BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="NEPAL AAWAS FINANCE LIMITED-PRROMOTER SHARE">NEPAL AAWAS FINANCE LIMITED-PRROMOTER SHARE</option>
                                                                                <option value="KALIKA MICROCREDIT DEVELOPMENT BANK LIMITED- PROMOTER SHARE">KALIKA MICROCREDIT DEVELOPMENT BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="MANASLU BIKAS BANK LIMITED-PROMOTER SHARE">MANASLU BIKAS BANK LIMITED-PROMOTER SHARE</option>
                                                                                <option value="INTERNATIONAL DEVELOPMENT BANK LIMITED- PROMOTER SHARE">INTERNATIONAL DEVELOPMENT BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="MISSION DEVELOPMENT BANK LIMITED-PROMOTER SHARE">MISSION DEVELOPMENT BANK LIMITED-PROMOTER SHARE</option>
                                                                                <option value="NIRDHAN UTHAN BANK LIMITED- PROMOTER SHARE">NIRDHAN UTHAN BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="PROFESSIONAL DIYALO BIKAS BANK LIMITED- PROMOTER SHARE">PROFESSIONAL DIYALO BIKAS BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="SHANGRILA DEVELOPMENT BANK LIMITED- PROMOTER SHARE">SHANGRILA DEVELOPMENT BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="SWAROJGAR LAGHUBITTA BIKAS BANK LIMITED- PROMOTER SAHRE">SWAROJGAR LAGHUBITTA BIKAS BANK LIMITED- PROMOTER SAHRE</option>
                                                                                <option value="HIMALAYAN DISTILLERY LIMITED - ORDINARY SHARE">HIMALAYAN DISTILLERY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="MUKTINATH BIKAS BANK LIMITED - PROMOTER SHARE">MUKTINATH BIKAS BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="KANKAI BIKAS BANK LIMITED - PROMOTER SHARE">KANKAI BIKAS BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="SIDDHARTHA INSURANCE LIMITED- PROMOTER SHARE">SIDDHARTHA INSURANCE LIMITED- PROMOTER SHARE</option>
                                                                                <option value="FIRST MICROFINANCE DEVELOPMENT BANK LIMITED - ORDINARY SHARE">FIRST MICROFINANCE DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="FIRST MICROFINANCE DEVELOPMENT BANK LIMITED- PROMOTER SHARE">FIRST MICROFINANCE DEVELOPMENT BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="MALIKA VIKAS BANK LIMITED- PROMOTER SHARE">MALIKA VIKAS BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="NEPAL GRAMEEN BIKAS  BANK LIMITED - ORDINARY SHARE">NEPAL GRAMEEN BIKAS  BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NEPAL GRAMEEN BIKAS  BANK LIMITED- PROMOTER SHARE">NEPAL GRAMEEN BIKAS  BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="SHINE RESUNGA DEVELOPMENT BANK LIMITED- PROMOTER SHARE">SHINE RESUNGA DEVELOPMENT BANK LIMITED- PROMOTER SHARE</option>
                                                                                <option value="PRUDENTIAL INSURANCE COMPANY LIMITED-PROMOTER SHARE">PRUDENTIAL INSURANCE COMPANY LIMITED-PROMOTER SHARE</option>
                                                                                <option value="NEPAL INSURANCE COMPANY LIMITED- PROMOTER SHARE">NEPAL INSURANCE COMPANY LIMITED- PROMOTER SHARE</option>
                                                                                <option value="NB INSURANCE COMPANY LIMITED - ORDINARY SHARE">NB INSURANCE COMPANY LIMITED - ORDINARY SHARE</option>
                                                                                <option value="JANATA BANK NEPAL LIMITED- PROMOTER SHARE">JANATA BANK NEPAL LIMITED- PROMOTER SHARE</option>
                                                                                <option value="NERUDE LAGHUBITTA BIKAS BANK LIMITED- PORMOTER SHARE">NERUDE LAGHUBITTA BIKAS BANK LIMITED- PORMOTER SHARE</option>
                                                                                <option value="GURKHAS  FINANCE LIMITED - ORDINARY SHARE">GURKHAS  FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SAMATA MICROFINANCE BITTIYA SANSTHA LIMITED - PROMOTER SHARE">SAMATA MICROFINANCE BITTIYA SANSTHA LIMITED - PROMOTER SHARE</option>
                                                                                <option value="SAMATA MICROFINANCE BITTIYA SANSTHA LIMITED - ORDINARY SHARE">SAMATA MICROFINANCE BITTIYA SANSTHA LIMITED - ORDINARY SHARE</option>
                                                                                <option value="CORPORATE DEVELOPMENT BANK LIMITED - ORDINARY SHARE">CORPORATE DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="CORPORATE DEVELOPMENT BANK LIMITED - PROMOTER SHARE">CORPORATE DEVELOPMENT BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="GUHESWORI MERCHANT BANKING AND FINANCE LIMITED- PROMOTER SHARE">GUHESWORI MERCHANT BANKING AND FINANCE LIMITED- PROMOTER SHARE</option>
                                                                                <option value="SHREE INVESTMENT AND  FINANCE COMPANY LIMITED - PROMOTER SHARE">SHREE INVESTMENT AND  FINANCE COMPANY LIMITED - PROMOTER SHARE</option>
                                                                                <option value="SYNERGY POWER DEVELOPMENT LIMITED - ORDINARY SHARE">SYNERGY POWER DEVELOPMENT LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NATIONAL MICROFINANCE BITTIYA SANSTHA LIMITED - ORDINARY SHARE">NATIONAL MICROFINANCE BITTIYA SANSTHA LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NATIONAL MICROFINACE BITTIYA SANSTHA LIMITED - PROMOTER SHARE">NATIONAL MICROFINACE BITTIYA SANSTHA LIMITED - PROMOTER SHARE</option>
                                                                                <option value="UNITED MODI HYDROPOWER LIMITED - ORDINARY SHARE">UNITED MODI HYDROPOWER LIMITED - ORDINARY SHARE</option>
                                                                                <option value="ARUN FINANCE LIMITED - PROMOTER SHARE">ARUN FINANCE LIMITED - PROMOTER SHARE</option>
                                                                                <option value="ARUN FINANCE LIMITED - ORDINARY SHARE">ARUN FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="KAMANA BIKAS BANK LIMITED - PROMOTER SHARE">KAMANA BIKAS BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="KAMANA BIKASH BANK LIMITED - ORDINARY SHARE">KAMANA BIKASH BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NABIL EQUITY FUND - MUTUAL FUND">NABIL EQUITY FUND - MUTUAL FUND</option>
                                                                                <option value="NIBL PRAGATI FUND - MUTUAL FUND (OLD MISTAKE) CAN NOT USE THIS ISIN">NIBL PRAGATI FUND - MUTUAL FUND (OLD MISTAKE) CAN NOT USE THIS ISIN</option>
                                                                                <option value="ARUN KABELI POWER LIMITED - ORDINARY SHARE">ARUN KABELI POWER LIMITED - ORDINARY SHARE</option>
                                                                                <option value="CHHYANGDI HYDROPOWER LIMITED - ORDINARY SHARE">CHHYANGDI HYDROPOWER LIMITED - ORDINARY SHARE</option>
                                                                                <option value="GREEN DEVELOPMENT BANK LIMITED - ORDINARY SHARE">GREEN DEVELOPMENT BANK LIMITED - ORDINARY SHARE</option>
                                                                                <option value="GREEN DEVELOPMENT BANK LIMITED - PROMOTER SHARE">GREEN DEVELOPMENT BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="SURODAYA LAGHUBITTIA BITTIYA SANSTHA LIMITED - ORDINARY SHARE">SURODAYA LAGHUBITTIA BITTIYA SANSTHA LIMITED - ORDINARY SHARE</option>
                                                                                <option value="NMB HYBRID FUND L- 1 MUTUAL FUND">NMB HYBRID FUND L- 1 MUTUAL FUND</option>
                                                                                <option value="SURYODAYA LAGHUBITTA BITTIYA SANSTHA LIMITED - PROMOTER SHARE">SURYODAYA LAGHUBITTA BITTIYA SANSTHA LIMITED - PROMOTER SHARE</option>
                                                                                <option value="KHANI KHOLA HYDROPOWER COMPANY  LIMITED - ORDINARY SHARE">KHANI KHOLA HYDROPOWER COMPANY  LIMITED - ORDINARY SHARE</option>
                                                                                <option value="DIBYASHWORI HYDROPOWER LIMITED - ORDINARY SHARE">DIBYASHWORI HYDROPOWER LIMITED - ORDINARY SHARE</option>
                                                                                <option value="INFRASTRUCTURE DEVELOPMENT BANK LIMITED - PROMOTER SHARE">INFRASTRUCTURE DEVELOPMENT BANK LIMITED - PROMOTER SHARE</option>
                                                                                <option value="Bank Of Kathmandu Ltd.- Debenture- 2076">Bank Of Kathmandu Ltd.- Debenture- 2076</option>
                                                                                <option value="Bank Of Kathmandu Ltd.- Debenture- 2079">Bank Of Kathmandu Ltd.- Debenture- 2079</option>
                                                                                <option value="NIBL PRAGATI FUND - MUTUAL FUND">NIBL PRAGATI FUND - MUTUAL FUND</option>
                                                                                <option value="SWADESHI LAHGUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE">SWADESHI LAHGUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE</option>
                                                                                <option value="SWADESHI LAGHUBITTA BITTIYA SANSTHA LIMITED - PROMOTER SHARE">SWADESHI LAGHUBITTA BITTIYA SANSTHA LIMITED - PROMOTER SHARE</option>
                                                                                <option value="RSDC LAGHUBITTA BITTIYA SANSTHA LIMITED - PROMOTER SAHRE">RSDC LAGHUBITTA BITTIYA SANSTHA LIMITED - PROMOTER SAHRE</option>
                                                                                <option value="RSDC LAGHUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE">RSDC LAGHUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE</option>
                                                                                <option value="Nepal Investment Bank Ltd. - Debenture - 2078">Nepal Investment Bank Ltd. - Debenture - 2078</option>
                                                                                <option value="Nepal Investment Bank Ltd. - Debenture - 2075">Nepal Investment Bank Ltd. - Debenture - 2075</option>
                                                                                <option value="Nepal Investment Bank Ltd. - Debenture - 2077">Nepal Investment Bank Ltd. - Debenture - 2077</option>
                                                                                <option value="KUBER MERCHANT FINANCE LIMITED - PROMOTER SHARE">KUBER MERCHANT FINANCE LIMITED - PROMOTER SHARE</option>
                                                                                <option value="KUBER MERCHANT FINANCE LIMITED - ORDINARY SHARE">KUBER MERCHANT FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="WORLD MERCHANT BANKING AND FINANCE LIMITED - ORDINARY SHARE">WORLD MERCHANT BANKING AND FINANCE LIMITED - ORDINARY SHARE</option>
                                                                                <option value="WORLD MERCHANT BANKING AND FINANCE LIMITED - PROMOTER SHARE">WORLD MERCHANT BANKING AND FINANCE LIMITED - PROMOTER SHARE</option>
                                                                                <option value="NIC Asia Bank Ltd.- Debenture- 2077">NIC Asia Bank Ltd.- Debenture- 2077</option>
                                                                                <option value="FORWARD COMMUNITY MICROFINANCE BITTIYA SANSTHA LIMITED - ORDINARY SHARE">FORWARD COMMUNITY MICROFINANCE BITTIYA SANSTHA LIMITED - ORDINARY SHARE</option>
                                                                                <option value="FORWARD COMMUNITY MICROFINANCE BITTIYA SANSTHA LIMITED - PROMOTER SHARE">FORWARD COMMUNITY MICROFINANCE BITTIYA SANSTHA LIMITED - PROMOTER SHARE</option>
                                                                                <option value="CIVIL LAGHUBITTA BITTIYA SANSTHA LIMITED - PROMOTER SHARE">CIVIL LAGHUBITTA BITTIYA SANSTHA LIMITED - PROMOTER SHARE</option>
                                                                            </select>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-4">
                                <div class="single-query">
                                   <select class="chosen-select" id="isin">
                                        <option value="">Select Script</option>
                                                                                <option value="SBI">SBI</option>
                                                                            <option value="BOKL">BOKL</option>
                                                                            <option value="SIL">SIL</option>
                                                                            <option value="LBL">LBL</option>
                                                                            <option value="CZBIL">CZBIL</option>
                                                                            <option value="SICL">SICL</option>
                                                                            <option value="HGI">HGI</option>
                                                                            <option value="PLIC">PLIC</option>
                                                                            <option value="EFL">EFL</option>
                                                                            <option value="ACEDBL">ACEDBL</option>
                                                                            <option value="ADBL">ADBL</option>
                                                                            <option value="SHPC">SHPC</option>
                                                                            <option value="BLDBLP">BLDBLP</option>
                                                                            <option value="SIGS1">SIGS1</option>
                                                                            <option value="HBLPO">HBLPO</option>
                                                                            <option value="NBBL">NBBL</option>
                                                                            <option value="DDBL">DDBL</option>
                                                                            <option value="DDBLPO">DDBLPO</option>
                                                                            <option value="SMFDBP">SMFDBP</option>
                                                                            <option value="ARDBLP">ARDBLP</option>
                                                                            <option value="ILFCPO">ILFCPO</option>
                                                                            <option value="TDBL">TDBL</option>
                                                                            <option value="TDBLPO">TDBLPO</option>
                                                                            <option value="AHPC">AHPC</option>
                                                                            <option value="MSMBS">MSMBS</option>
                                                                            <option value="MSMBSP">MSMBSP</option>
                                                                            <option value="MBBLPO">MBBLPO</option>
                                                                            <option value="MSBBL">MSBBL</option>
                                                                            <option value="CBLPO">CBLPO</option>
                                                                            <option value="WDBLPO">WDBLPO</option>
                                                                            <option value="PLICPO">PLICPO</option>
                                                                            <option value="NMBMFP">NMBMFP</option>
                                                                            <option value="MFIL">MFIL</option>
                                                                            <option value="PRVU">PRVU</option>
                                                                            <option value="RLFL">RLFL</option>
                                                                            <option value="RLFLPO">RLFLPO</option>
                                                                            <option value="PIC">PIC</option>
                                                                            <option value="BGDBL">BGDBL</option>
                                                                            <option value="BGDBLP">BGDBLP</option>
                                                                            <option value="PCBL">PCBL</option>
                                                                            <option value="PCBLP">PCBLP</option>
                                                                            <option value="CCBLPO">CCBLPO</option>
                                                                            <option value="NBF1">NBF1</option>
                                                                            <option value="FBBLPO">FBBLPO</option>
                                                                            <option value="GFLPO">GFLPO</option>
                                                                            <option value="NUBL">NUBL</option>
                                                                            <option value="NMBSF1">NMBSF1</option>
                                                                            <option value="SRS">SRS</option>
                                                                            <option value="PADBLP">PADBLP</option>
                                                                            <option value="VSDBL">VSDBL</option>
                                                                            <option value="MBLPO">MBLPO</option>
                                                                            <option value="GRAND">GRAND</option>
                                                                            <option value="RMDC">RMDC</option>
                                                                            <option value="MDB">MDB</option>
                                                                            <option value="WDBL">WDBL</option>
                                                                            <option value="VSDBLP">VSDBLP</option>
                                                                            <option value="SHL">SHL</option>
                                                                            <option value="INDBPO">INDBPO</option>
                                                                            <option value="PRIN">PRIN</option>
                                                                            <option value="PROFL">PROFL</option>
                                                                            <option value="PROFLP">PROFLP</option>
                                                                            <option value="NMBMF">NMBMF</option>
                                                                            <option value="MERO">MERO</option>
                                                                            <option value="MEROPO">MEROPO</option>
                                                                            <option value="MIDBL">MIDBL</option>
                                                                            <option value="LBLPO">LBLPO</option>
                                                                            <option value="SBBLJ">SBBLJ</option>
                                                                            <option value="SBBLJP">SBBLJP</option>
                                                                            <option value="MLBL">MLBL</option>
                                                                            <option value="SWBBLP">SWBBLP</option>
                                                                            <option value="NBSL">NBSL</option>
                                                                            <option value="GDBL">GDBL</option>
                                                                            <option value="SFL">SFL</option>
                                                                            <option value="SYFL">SYFL</option>
                                                                            <option value="SYFLPO">SYFLPO</option>
                                                                            <option value="UFCL">UFCL</option>
                                                                            <option value="UFCLPO">UFCLPO</option>
                                                                            <option value="NNLB">NNLB</option>
                                                                            <option value="RBCL">RBCL</option>
                                                                            <option value="RBCLPO">RBCLPO</option>
                                                                            <option value="SUPRMP">SUPRMP</option>
                                                                            <option value="MNBBL">MNBBL</option>
                                                                            <option value="GUFLPO">GUFLPO</option>
                                                                            <option value="NBILPO">NBILPO</option>
                                                                            <option value="NABIL">NABIL</option>
                                                                            <option value="NICA">NICA</option>
                                                                            <option value="KBL">KBL</option>
                                                                            <option value="MBBL">MBBL</option>
                                                                            <option value="SFLPO">SFLPO</option>
                                                                            <option value="NCDB">NCDB</option>
                                                                            <option value="CNDBLP">CNDBLP</option>
                                                                            <option value="SEOS">SEOS</option>
                                                                            <option value="SAJHA">SAJHA</option>
                                                                            <option value="SAJHAP">SAJHAP</option>
                                                                            <option value="JBNL">JBNL</option>
                                                                            <option value="MBL">MBL</option>
                                                                            <option value="NABILP">NABILP</option>
                                                                            <option value="ICFCPO">ICFCPO</option>
                                                                            <option value="KDBL">KDBL</option>
                                                                            <option value="KDBLPO">KDBLPO</option>
                                                                            <option value="BUDBL">BUDBL</option>
                                                                            <option value="NBBPO">NBBPO</option>
                                                                            <option value="NCCBPO">NCCBPO</option>
                                                                            <option value="EBLCP">EBLCP</option>
                                                                            <option value="SKBBL">SKBBL</option>
                                                                            <option value="NNFC">NNFC</option>
                                                                            <option value="VLBS">VLBS</option>
                                                                            <option value="VLBSPO">VLBSPO</option>
                                                                            <option value="GFCLPO">GFCLPO</option>
                                                                            <option value="NLICP">NLICP</option>
                                                                            <option value="NIDC">NIDC</option>
                                                                            <option value="ALDBL">ALDBL</option>
                                                                            <option value="GLICL">GLICL</option>
                                                                            <option value="MDBPO">MDBPO</option>
                                                                            <option value="JBBL">JBBL</option>
                                                                            <option value="MDBL">MDBL</option>
                                                                            <option value="PRINPO">PRINPO</option>
                                                                            <option value="MMDBL">MMDBL</option>
                                                                            <option value="MMDBLP">MMDBLP</option>
                                                                            <option value="CBL">CBL</option>
                                                                            <option value="NBB">NBB</option>
                                                                            <option value="LUBL">LUBL</option>
                                                                            <option value="NICAP">NICAP</option>
                                                                            <option value="EBL">EBL</option>
                                                                            <option value="EBLPO">EBLPO</option>
                                                                            <option value="SWBBL">SWBBL</option>
                                                                            <option value="UFL">UFL</option>
                                                                            <option value="UFLPO">UFLPO</option>
                                                                            <option value="CNDBL">CNDBL</option>
                                                                            <option value="SKBBLP">SKBBLP</option>
                                                                            <option value="CHCL">CHCL</option>
                                                                            <option value="SBL">SBL</option>
                                                                            <option value="TNBL">TNBL</option>
                                                                            <option value="TNBLPO">TNBLPO</option>
                                                                            <option value="LGIL">LGIL</option>
                                                                            <option value="SICLPO">SICLPO</option>
                                                                            <option value="EDBLPO">EDBLPO</option>
                                                                            <option value="DBBL">DBBL</option>
                                                                            <option value="DBBLPO">DBBLPO</option>
                                                                            <option value="KMCDB">KMCDB</option>
                                                                            <option value="GMFIL">GMFIL</option>
                                                                            <option value="JFLPO">JFLPO</option>
                                                                            <option value="NMB">NMB</option>
                                                                            <option value="NMBPO">NMBPO</option>
                                                                            <option value="SBBLPO">SBBLPO</option>
                                                                            <option value="TRH">TRH</option>
                                                                            <option value="SMFDB">SMFDB</option>
                                                                            <option value="ODBL">ODBL</option>
                                                                            <option value="ODBLPO">ODBLPO</option>
                                                                            <option value="PRDBL">PRDBL</option>
                                                                            <option value="PICL">PICL</option>
                                                                            <option value="NLGPO">NLGPO</option>
                                                                            <option value="SAFLPO">SAFLPO</option>
                                                                            <option value="MPFLPO">MPFLPO</option>
                                                                            <option value="SRBL">SRBL</option>
                                                                            <option value="MEGA">MEGA</option>
                                                                            <option value="NIB">NIB</option>
                                                                            <option value="KBLPO">KBLPO</option>
                                                                            <option value="NTC">NTC</option>
                                                                            <option value="BLDBL">BLDBL</option>
                                                                            <option value="BHBL">BHBL</option>
                                                                            <option value="BHBLPO">BHBLPO</option>
                                                                            <option value="SUBBLP">SUBBLP</option>
                                                                            <option value="LFLCPO">LFLCPO</option>
                                                                            <option value="GIMES1">GIMES1</option>
                                                                            <option value="GABL">GABL</option>
                                                                            <option value="GRANDP">GRANDP</option>
                                                                            <option value="UNL">UNL</option>
                                                                            <option value="LLBS">LLBS</option>
                                                                            <option value="LLBSPO">LLBSPO</option>
                                                                            <option value="TBBL">TBBL</option>
                                                                            <option value="ALDBLP">ALDBLP</option>
                                                                            <option value="SFFIL">SFFIL</option>
                                                                            <option value="JSLBB">JSLBB</option>
                                                                            <option value="JSLBBP">JSLBBP</option>
                                                                            <option value="SLBBL">SLBBL</option>
                                                                            <option value="SCB">SCB</option>
                                                                            <option value="SRBLPO">SRBLPO</option>
                                                                            <option value="BOKLPO">BOKLPO</option>
                                                                            <option value="ACEDPO">ACEDPO</option>
                                                                            <option value="GBBLPO">GBBLPO</option>
                                                                            <option value="SIC">SIC</option>
                                                                            <option value="PADBL">PADBL</option>
                                                                            <option value="NLIC">NLIC</option>
                                                                            <option value="HBL">HBL</option>
                                                                            <option value="BPCL">BPCL</option>
                                                                            <option value="SHINE">SHINE</option>
                                                                            <option value="SICPO">SICPO</option>
                                                                            <option value="SBIPO">SBIPO</option>
                                                                            <option value="PICPO">PICPO</option>
                                                                            <option value="KADBLP">KADBLP</option>
                                                                            <option value="EDBL">EDBL</option>
                                                                            <option value="NBL">NBL</option>
                                                                            <option value="LUBLPO">LUBLPO</option>
                                                                            <option value="GBBL">GBBL</option>
                                                                            <option value="KNBL">KNBL</option>
                                                                            <option value="YETI">YETI</option>
                                                                            <option value="NCDBPO">NCDBPO</option>
                                                                            <option value="NICL">NICL</option>
                                                                            <option value="MPFL">MPFL</option>
                                                                            <option value="NCCB">NCCB</option>
                                                                            <option value="SANIMA">SANIMA</option>
                                                                            <option value="EIC">EIC</option>
                                                                            <option value="JEFL">JEFL</option>
                                                                            <option value="CSDBL">CSDBL</option>
                                                                            <option value="NEFLPO">NEFLPO</option>
                                                                            <option value="BBC">BBC</option>
                                                                            <option value="SUPRME">SUPRME</option>
                                                                            <option value="ICFC">ICFC</option>
                                                                            <option value="GABLPO">GABLPO</option>
                                                                            <option value="BFCL">BFCL</option>
                                                                            <option value="GLICLP">GLICLP</option>
                                                                            <option value="SDBL">SDBL</option>
                                                                            <option value="SDBLPO">SDBLPO</option>
                                                                            <option value="RMDCPO">RMDCPO</option>
                                                                            <option value="GDBLPO">GDBLPO</option>
                                                                            <option value="WOMI">WOMI</option>
                                                                            <option value="WOMIPO">WOMIPO</option>
                                                                            <option value="KKBL">KKBL</option>
                                                                            <option value="KKBLPO">KKBLPO</option>
                                                                            <option value="BNL">BNL</option>
                                                                            <option value="BNT">BNT</option>
                                                                            <option value="NLBBL">NLBBL</option>
                                                                            <option value="APEX">APEX</option>
                                                                            <option value="SEWA">SEWA</option>
                                                                            <option value="UFILPO">UFILPO</option>
                                                                            <option value="SADBL">SADBL</option>
                                                                            <option value="KBBL">KBBL</option>
                                                                            <option value="KBBLPO">KBBLPO</option>
                                                                            <option value="JBBLPO">JBBLPO</option>
                                                                            <option value="NABB">NABB</option>
                                                                            <option value="APEXPO">APEXPO</option>
                                                                            <option value="HIDCL">HIDCL</option>
                                                                            <option value="CZBILP">CZBILP</option>
                                                                            <option value="CBBL">CBBL</option>
                                                                            <option value="CBBLPO">CBBLPO</option>
                                                                            <option value="SODBLP">SODBLP</option>
                                                                            <option value="KMFL">KMFL</option>
                                                                            <option value="KMFLPO">KMFLPO</option>
                                                                            <option value="SIFC">SIFC</option>
                                                                            <option value="EFLPO">EFLPO</option>
                                                                            <option value="TBBLP">TBBLP</option>
                                                                            <option value="SETI">SETI</option>
                                                                            <option value="REDBL">REDBL</option>
                                                                            <option value="REDBLP">REDBLP</option>
                                                                            <option value="BFCLPO">BFCLPO</option>
                                                                            <option value="KADBL">KADBL</option>
                                                                            <option value="FBBL">FBBL</option>
                                                                            <option value="GFL">GFL</option>
                                                                            <option value="NIBPO">NIBPO</option>
                                                                            <option value="LFC">LFC</option>
                                                                            <option value="LFCPO">LFCPO</option>
                                                                            <option value="SEWAPO">SEWAPO</option>
                                                                            <option value="NILPO">NILPO</option>
                                                                            <option value="MLBBL">MLBBL</option>
                                                                            <option value="ALICL">ALICL</option>
                                                                            <option value="ALICLP">ALICLP</option>
                                                                            <option value="INDB">INDB</option>
                                                                            <option value="STC">STC</option>
                                                                            <option value="SLICL">SLICL</option>
                                                                            <option value="SLICLP">SLICLP</option>
                                                                            <option value="LICN">LICN</option>
                                                                            <option value="LICNPO">LICNPO</option>
                                                                            <option value="HAMRO">HAMRO</option>
                                                                            <option value="UIC">UIC</option>
                                                                            <option value="SBLPO">SBLPO</option>
                                                                            <option value="NIDCPO">NIDCPO</option>
                                                                            <option value="LGILPO">LGILPO</option>
                                                                            <option value="CFCL">CFCL</option>
                                                                            <option value="CFCLPO">CFCLPO</option>
                                                                            <option value="SHBL">SHBL</option>
                                                                            <option value="SHBLPO">SHBLPO</option>
                                                                            <option value="SINDU">SINDU</option>
                                                                            <option value="MLBBLP">MLBBLP</option>
                                                                            <option value="RMFL">RMFL</option>
                                                                            <option value="RMFLPO">RMFLPO</option>
                                                                            <option value="CCBL">CCBL</option>
                                                                            <option value="BUDBLP">BUDBLP</option>
                                                                            <option value="ILFC">ILFC</option>
                                                                            <option value="SUBBL">SUBBL</option>
                                                                            <option value="API">API</option>
                                                                            <option value="SAFL">SAFL</option>
                                                                            <option value="LVF1">LVF1</option>
                                                                            <option value="PFC">PFC</option>
                                                                            <option value="PFCPO">PFCPO</option>
                                                                            <option value="CLBSL">CLBSL</option>
                                                                            <option value="MLBLPO">MLBLPO</option>
                                                                            <option value="KCDBL">KCDBL</option>
                                                                            <option value="KCDBLP">KCDBLP</option>
                                                                            <option value="KEBL">KEBL</option>
                                                                            <option value="YETIPO">YETIPO</option>
                                                                            <option value="NNLBPO">NNLBPO</option>
                                                                            <option value="BARUN">BARUN</option>
                                                                            <option value="UFIL">UFIL</option>
                                                                            <option value="OHL">OHL</option>
                                                                            <option value="NCMPO">NCMPO</option>
                                                                            <option value="MFILPO">MFILPO</option>
                                                                            <option value="CIT">CIT</option>
                                                                            <option value="HAMROP">HAMROP</option>
                                                                            <option value="NLG">NLG</option>
                                                                            <option value="NBBLPO">NBBLPO</option>
                                                                            <option value="SODBL">SODBL</option>
                                                                            <option value="RBBBL">RBBBL</option>
                                                                            <option value="RBBBLP">RBBBLP</option>
                                                                            <option value="PURBL">PURBL</option>
                                                                            <option value="NNFCPO">NNFCPO</option>
                                                                            <option value="SINDUP">SINDUP</option>
                                                                            <option value="JHBL">JHBL</option>
                                                                            <option value="HAMA">HAMA</option>
                                                                            <option value="HAMAPO">HAMAPO</option>
                                                                            <option value="IDBL">IDBL</option>
                                                                            <option value="SCBPO">SCBPO</option>
                                                                            <option value="CSDBLP">CSDBLP</option>
                                                                            <option value="SETIPO">SETIPO</option>
                                                                            <option value="SFFILP">SFFILP</option>
                                                                            <option value="JHBLPO">JHBLPO</option>
                                                                            <option value="JFL">JFL</option>
                                                                            <option value="NBSLPO">NBSLPO</option>
                                                                            <option value="NHPC">NHPC</option>
                                                                            <option value="MMFDB">MMFDB</option>
                                                                            <option value="MMFDBP">MMFDBP</option>
                                                                            <option value="SKDBL">SKDBL</option>
                                                                            <option value="SKDBLP">SKDBLP</option>
                                                                            <option value="NIL">NIL</option>
                                                                            <option value="ARDBL">ARDBL</option>
                                                                            <option value="INDBL">INDBL</option>
                                                                            <option value="LFLC">LFLC</option>
                                                                            <option value="HGIPO">HGIPO</option>
                                                                            <option value="NLICL">NLICL</option>
                                                                            <option value="NLICLP">NLICLP</option>
                                                                            <option value="NGPL">NGPL</option>
                                                                            <option value="PRVUPO">PRVUPO</option>
                                                                            <option value="NEFL">NEFL</option>
                                                                            <option value="NCM">NCM</option>
                                                                            <option value="PURBLP">PURBLP</option>
                                                                            <option value="MEGAPO">MEGAPO</option>
                                                                            <option value="NIBSF1">NIBSF1</option>
                                                                            <option value="RHPC">RHPC</option>
                                                                            <option value="GFCL">GFCL</option>
                                                                            <option value="KEBLPO">KEBLPO</option>
                                                                            <option value="PFL">PFL</option>
                                                                            <option value="PFLPO">PFLPO</option>
                                                                            <option value="UICPO">UICPO</option>
                                                                            <option value="JEFLPO">JEFLPO</option>
                                                                            <option value="KRBL">KRBL</option>
                                                                            <option value="KRBLPO">KRBLPO</option>
                                                                            <option value="GBIME">GBIME</option>
                                                                            <option value="GBIMEP">GBIMEP</option>
                                                                            <option value="NABBPO">NABBPO</option>
                                                                            <option value="KMCDBP">KMCDBP</option>
                                                                            <option value="MSBBLP">MSBBLP</option>
                                                                            <option value="INDBLP">INDBLP</option>
                                                                            <option value="MIDBLP">MIDBLP</option>
                                                                            <option value="NUBLPO">NUBLPO</option>
                                                                            <option value="PRDBLP">PRDBLP</option>
                                                                            <option value="SADBLP">SADBLP</option>
                                                                            <option value="SLBBLP">SLBBLP</option>
                                                                            <option value="HDL">HDL</option>
                                                                            <option value="MNBBLP">MNBBLP</option>
                                                                            <option value="KNBLPO">KNBLPO</option>
                                                                            <option value="SILPO">SILPO</option>
                                                                            <option value="FMDBL">FMDBL</option>
                                                                            <option value="FMDBLP">FMDBLP</option>
                                                                            <option value="MDBLPO">MDBLPO</option>
                                                                            <option value="NGBBL">NGBBL</option>
                                                                            <option value="NGBBLP">NGBBLP</option>
                                                                            <option value="SHINEP">SHINEP</option>
                                                                            <option value="PICLPO">PICLPO</option>
                                                                            <option value="NICLPO">NICLPO</option>
                                                                            <option value="NBIL">NBIL</option>
                                                                            <option value="JBNLPO">JBNLPO</option>
                                                                            <option value="NLBBLP">NLBBLP</option>
                                                                            <option value="GUFL">GUFL</option>
                                                                            <option value="SMATAP">SMATAP</option>
                                                                            <option value="SMATA">SMATA</option>
                                                                            <option value="CORBL">CORBL</option>
                                                                            <option value="CORBLP">CORBLP</option>
                                                                            <option value="GMFILP">GMFILP</option>
                                                                            <option value="SIFCPO">SIFCPO</option>
                                                                            <option value="SPDL">SPDL</option>
                                                                            <option value="NMFBS">NMFBS</option>
                                                                            <option value="NMFBSP">NMFBSP</option>
                                                                            <option value="UMHL">UMHL</option>
                                                                            <option value="ARUNPO">ARUNPO</option>
                                                                            <option value="ARUN">ARUN</option>
                                                                            <option value="KMBLPO">KMBLPO</option>
                                                                            <option value="KMBL">KMBL</option>
                                                                            <option value="NEF">NEF</option>
                                                                            <option value="NIBLPF">NIBLPF</option>
                                                                            <option value="AKPL">AKPL</option>
                                                                            <option value="CHL">CHL</option>
                                                                            <option value="GRDBL">GRDBL</option>
                                                                            <option value="GRDBLP">GRDBLP</option>
                                                                            <option value="SLBS">SLBS</option>
                                                                            <option value="NMBHF1">NMBHF1</option>
                                                                            <option value="SLBSP">SLBSP</option>
                                                                            <option value="KKHC">KKHC</option>
                                                                            <option value="DHPL">DHPL</option>
                                                                            <option value="IDBLPO">IDBLPO</option>
                                                                            <option value="BOKD2076">BOKD2076</option>
                                                                            <option value="BOKD2079">BOKD2079</option>
                                                                            <option value="NIBLPF">NIBLPF</option>
                                                                            <option value="SDESI">SDESI</option>
                                                                            <option value="SDESIP">SDESIP</option>
                                                                            <option value="RSDCP">RSDCP</option>
                                                                            <option value="RSDC">RSDC</option>
                                                                            <option value="NIBD2078">NIBD2078</option>
                                                                            <option value="NIBD2075">NIBD2075</option>
                                                                            <option value="NIBD2077">NIBD2077</option>
                                                                            <option value="KMBSLP">KMBSLP</option>
                                                                            <option value="KMBSL">KMBSL</option>
                                                                            <option value="WMBF">WMBF</option>
                                                                            <option value="WMBFPO">WMBFPO</option>
                                                                            <option value="NICAD2077">NICAD2077</option>
                                                                            <option value="FOWAD">FOWAD</option>
                                                                            <option value="FOWADP">FOWADP</option>
                                                                            <option value="CLBSLP">CLBSLP</option>
                                                                        </select>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-4">
                                <div id="datepicker-component" class="input-group date col-sm-12">
                                    <input type="text" class="form-control" placeholder="Start Date" id="date_from"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-4">
                                <div id="datepicker-component-1" class="input-group date col-sm-12">
                                    <input type="text" class="form-control" placeholder="End Date" id="date_to"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <button type="button" class="btn-light button-black" id="btn_search">Filter</button>
                            </div>
                        </form>
                    </div>
                                        <div class="table-responsive" id="listall">
                        <table class="table table-striped table-bordered table-responsive" id="dynamic-table">
                            <thead>
                                <tr>
                                    <th>Company Name</th>
                                    <th>SCRIPT NAME</th>
                                    <th>ISIN</th>
                                    <th>Last Trade Date</th>
                                    <th>Book Closure Start</th>
                                    <th>Book Closure End</th>
                                    <th>Purpose Book Closure</th>
                                    <th>FY Book Closure</th>
                                </tr>
                            </thead>
                            <tbody>
                                                                    <tr>
                                        <td>NAGBELI LAGHUBITTA BIKAS BANK LIMITED-ORDINARY SHARE</td>
                                        <td>NBBL</td>
                                        <td>NPE249A00000</td>
                                        <td>2018-04-11</td>
                                        <td>2018-04-19</td>
                                        <td>2018-04-16</td>
                                        <td>RIGHT</td>
                                        <td>074/075</td>
                                    </tr>
                                                                    <tr>
                                        <td>Siddhartha Insurance Limited- Ordinary Share</td>
                                        <td>SIL</td>
                                        <td>NPE008A00000</td>
                                        <td>2018-04-17</td>
                                        <td>2018-04-08</td>
                                        <td>2018-04-18</td>
                                        <td>RIGHT</td>
                                        <td>074/075</td>
                                    </tr>
                                                                    <tr>
                                        <td>Siddhartha Insurance Limited- Ordinary Share</td>
                                        <td>SIL</td>
                                        <td>NPE008A00000</td>
                                        <td>2018-03-27</td>
                                        <td>2018-03-22</td>
                                        <td>2018-03-22</td>
                                        <td>BONUS</td>
                                        <td>073/074</td>
                                    </tr>
                                                                    <tr>
                                        <td>Siddhartha Insurance Limited- Ordinary Share</td>
                                        <td>SIL</td>
                                        <td>NPE008A00000</td>
                                        <td>2018-03-13</td>
                                        <td>2018-03-15</td>
                                        <td>2018-03-15</td>
                                        <td>RIGHT</td>
                                        <td>072/073</td>
                                    </tr>
                                                            </tbody>
                        </table>
                    </div>
                                </div>
        </div>
    </div>
</section>


<script type="text/javascript">
    $(document).ready(function(){
        $("#btn_search").click(function() {
            
            var company_name = $("#company_name").val();
            var script_name = $("#isin").val();
            var date_from = $("#date_from").val();
            var date_to = $("#date_to").val();
            $.ajax({
                type: "POST",
                url: "http://itsutra.com.np/cdscnp/Site/SearchBookClosureInfo",
                data: {company_name:company_name,script_name:script_name,date_from:date_from,date_to:date_to},
                cache: false,
                success: function(response) {
                    $("#listall").empty().html(response);

                     
                    $("#company_name").val('');
                    $("#isin").val(' ');
                    $("#date_from").val(' ');
                    $("#date_to").val(' ');
                }
            });
        });

        $('#datepicker-component').datepicker();
        $('#datepicker-component-1').datepicker();
    });
</script>   
<!-- Footer Start -->
@include('Home.layouts.footer')
