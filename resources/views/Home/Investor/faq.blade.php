@include('Home.layouts.header')
@include('Home.layouts.nav')
<title>FAQ :: Investor</title>
<!-- Page Banner End -->
<!-- Project Details Start -->
<section id="solution" class="p-t-30 p-b-30">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="solution_tabs">
                    <ul>
                    <li class="active"><a href="#">Investor</a>  <br/>   </li> 
                    <ul>
                                                        <li class="active">
                                                        <a href="{{url('frequentlyaskquestion')}}">FAQ</a>
                                                        </li>
                                                        <li class="">
                                                        <a href="{{url('settlementid')}}">Settlement ID</a>
                                                        </li>
                                                        <li class="">
                                                        <a href="{{url('do&dont')}}">Dos & Don'ts</a>
                                                        </li>
                                                        <li class="">
                                                        <a href="{{url('events')}}">Events</a>
                                                        </li>
                                                        <li class="">
                                                        <a href="{{url('deathnotice')}}">Death Notice</a>
                                                        </li>
                                                        </ul>
                 </ul>
                </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div id="global-relation" class="our_team">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading" id="global-relation">
                                <div class="heading_border bg_red"></div>
                                <h2>FAQ</h2>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 project_details_text">
                           <p class="p_14" style="text-align: justify;"><p><span style="font-size:18px;">What is Depository</span></p>

<p style="text-align: justify;">A depository facilitates the holding and/or transacting securities in book entry form. In other words, a depository takes the ownership guarantee of the shareholders by holding those securities and other market instruments which are listed in the secondary market, distributed or allotted and can be deposited into the electronic form. The investor has to open a demat account to avail the services of depository. Additionally, the depository maintains the record in the account of the investors.</p>

<p style="text-align: justify;">&nbsp;</p>

<p style="text-align: justify;"><span style="font-size:18px;">What is Dematerialization</span></p>

<p>Dematerialization is a process of converting physical securities into electronic form.</p>

<p>&nbsp;</p>

<p><span style="font-size:18px;">What is ISIN</span></p>

<p style="text-align: justify;">An ISIN is a unique 12 digit alpha<img src="" style="float: left;" />numeric code given to the securities such as shares, preference shares, debentures, bonds, etc. when the security is admitted in the depository system. The first two digits of the ISIN code indicate country of registration for the security.</p>

<h2><h2><button><a href="{{ url('/faq/CDSC_FAQ_(Nepali).pdf') }}" class="btn btn-inverse btn-mini" download="CDSC_FAQ_(Nepali).pdf" title="Download File"> MORE FAQ Nepali </a></button> <button><a href="{{ url('/faq/CDSC_FAQ_English.pdf') }}" class="btn btn-inverse btn-mini" download="CDSC_FAQ_English.pdf" title="Download File"> MORE FAQ English </a></button></h2></h2>
</h1>
<p style="text-align: justify;">&nbsp;<p></p></p>
</p>

<p style="text-align: justify;">&nbsp;</p>


</p>
                        </div>
                    </div>
                </div>
                <!-- Our Partners end -->
            </div>
        </div>
    </div>
</section>
<br>
<!-- Project Details End -->  
 <!-- Footer Start -->
 @include('Home.layouts.footer')