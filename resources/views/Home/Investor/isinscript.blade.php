@include('Home.layouts.header')
<title>ISIN & Script :: Investor </title>
<section id="solution" class="p-t-60 p-b-40">
  <div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row" id="introduction">
                  <div class="col-md-12">
                    <div class="heading">
                        <div class="heading_border bg_red"></div>
                         <h2>ISIN & Scripts</h2>
                    </div>
                  </div>
                </div>

               

                <div class="row">
                  <div class="col-md-12">
                                            <div class="table-responsive" id="listall">
                          <table class="table table-striped table-bordered table-responsive" id="dynamic-table" style="width:100%">
                              <thead>
                                <tr>
                                    <th>S.N</th>
                                    <th>Company Name</th>
                                    <th>ISIN</th>
                                    <th>Script</th>
                                </tr>
                              </thead>
                              <tbody>
                                 <?php $i =0;?>
                                 <?php $isin=DB::table('isin')->orderBy('remarks','ASC')->where('status',1)->get();?>

                                  @foreach($isin as $data)
                                  
                                                                  <tr>
                                    <td> <?php echo ++$i;?></td>
                                    
                                    <?php $issuer = DB::table('isin')->leftJoin('issuer', 'isin.issuer_id', '=', 'issuer.issuer_id')->where('id',$data->id)->get(); ?>
                                    @foreach($issuer as $data1)
                                    <?php $isinType = DB::table('isin')->leftJoin('isin_type', 'isin.type', '=', 'isin_type.id')->where('type',$data->type)->get(); ?>
                                    @foreach($isinType as $data2)
                                    <?php $type = str_replace('_', ' ',$data2->title); ?>
                                     @endforeach 
                                    <td style="word-wrap: break-word;">{{strtoupper($data1->name.' - '.$type)}}</td>
                                      
                                     @endforeach   
                                    <td>{{$data->isin_code}}</td>  
                                    <td>{{$data->script}}</td>  
                                  </tr>
                                             @endforeach                
                                                              </tbody>
                            </table>
                           
                        </div>
                                        </div>
                </div>
              </div>
          </div>
        </div>
    </div>
  </div>
</section>

<script type="text/javascript">
  $(document).ready(function(){
    $("#btn_search_si").click(function(){
        var script_name = $("#script_name").val();
        var isin = $("#isin").val();
        $.ajax({
            type: "POST",
            url: "http://itsutra.com.np/cdscnp/Site/searchIsinScript",
            data: {script_name:script_name,isin:isin},
            cache: false,
            success: function(data) {
              $('#listall').html(data);
            }
        });
        // $.ajax({
        //     type: "POST",
        //     url: "http://itsutra.com.np/cdscnp/Site/searchIsinScript",
        //     data: {script_name:script_name,isin:isin},
        //     cache: false,
        //     success: function(data) {
        //       alert(data);
        //     }
        // });
    });
  });
</script>
   <!-- Footer Start -->
   @include('Home.layouts.footer')
