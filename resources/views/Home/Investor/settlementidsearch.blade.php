@include('Home.layouts.header')
<title>Settlement ID :: Investor </title>
<section id="solution" class="p-t-60 p-b-40">
    <div class="container">
        <div id="book-closure" class="row">
            <div class="col-md-12 p-b-50" id="dp">
                <div class="heading_border bg_red"></div>
                <h2>Settlement ID</h2>
                 <div class="row">
                <div class="col-md-12">
<form action="settlementidsearch" method="GET" role="search">
{{ csrf_field()}}
<div class="row">
<div class="col-md-5 col-sm-4">
<div class="single-query">
<input data-provide="datepicker"  id="trade_date" name="trade_date"  data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" required>
<!-- <select class="chosen-select" id="dp_id">
<option value="">Select DPID</option>
<option value="10100">10100</option>
</select> -->
</div>
</div>

<div class="col-md-2">
<button class="btn-light button-black" id="btn_search_si">Search</button>
</div>
<div class="col-md-2"> <button class="btn-light button-black" type="reset" value="Reset the form"><a href="/settlementid" style="color: #fff;">Reset</a></button>
</div>
</div>
</div>
</form>
</div>

                <hr>
                <div class="table-responsive" id="listall">
                    <table class="table table-striped table-bordered table-responsive" id="" style="width: 100%">
                        <thead>
                            <th>S.N</th>
                            <th>Settlement ID</th>
                            <th>Trade Date</th>
                        </thead>
                        <tbody>
                        <tbody>
                        <?php $trade_date=$_GET['trade_date'];?>
                        <?php $settlement = DB::table("settlement")->where('trade_date', 'LIKE', '%' . $trade_date . '%')->orderBy('trade_date','DESC')->where('deleted_at',null)->get();?>

                           <?php $i =0;?>
                             @foreach($settlement as $settlements)
                            <tr>
                                 
                                <td><?php echo ++$i; ?></td>
                                <td>{{$settlements->settlement_id}}</td>
                                <td>{{$settlements->trade_date}}</td>
                            </tr>
                         
                            <tr>
                                   @endforeach
                              
                        </tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        $("#btn_search_si").click(function() {
            var date_from = $("#date_from").val();
            $.ajax({
                type: "POST",
                url: "hSearchSettlementID",
                data: {tradedate:date_from},
                cache: false,
                success: function(response) {
                    $("#listall").empty().html(response);
                    $("#date_from").val('');
                }
            });
        });
    
        $('#datepicker-component').datepicker({
            format: 'yyyy-mm-dd',
        }).on('changeDate', function(e){
            $(this).datepicker('hide');
        });
    });
</script>
<!-- Footer Start -->
@include('Home.layouts.footer')

