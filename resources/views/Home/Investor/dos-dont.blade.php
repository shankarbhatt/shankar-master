@include('Home.layouts.header')
@include('Home.layouts.nav')
<title>Dos & Don'ts :: Invertor</title>
<!-- Page Banner End -->
<!-- Project Details Start -->
<section id="solution" class="p-t-30 p-b-30">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="solution_tabs">
                <ul>
                    <li class="active"><a>Investor</a>  <br/>   </li> 
                    <ul>
                                                        <li class="">
                                                        <a href="{{url('frequentlyaskquestion')}}">FAQ</a>
                                                        </li>
                                                        <li class="">
                                                        <a href="{{url('settlementid')}}">Settlement ID</a>
                                                        </li>
                                                        <li class="active">
                                                        <a href="{{url('do&dont')}}">Dos & Don'ts</a>
                                                        </li>
                                                        <li class="">
                                                        <a href="{{url('events')}}">Events</a>
                                                        </li>
                                                        <li class="">
                                                        <a href="{{url('deathnotice')}}">Death Notice</a>
                                                        </li>
                                                        </ul>
                 </ul>
                </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div id="global-relation" class="our_team">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading" id="global-relation">
                                <div class="heading_border bg_red"></div>
                                <h2>Dos & Don'ts</h2>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 project_details_text">
                           <p class="p_14" style="text-align: justify;"><p><strong><span style="font-size:14px;"><u>DO&#39;S</u></span></strong></p>

<ul>
	<li>Register for CDSC&#39;s internet based facility &#39;Mero Share&#39; to monitor your demat account yourself. Contact your DP for details.</li>
	<li>Accept the DIS book from your DP only if each slip has been pre-printed with a serial number along with your demat account number and keep it in safe custody.</li>
	<li>Always mention the details like ISIN, number of securities accurately. In case of any queries, please contact your DP or broker.</li>
	<li>Ensure that all demat account holder(s) sign on the DIS.</li>
	<li>Please strike out the any blank space on the slip.</li>
	<li>Cancellations or corrections on the DIS should be initialed or signed by all the account holder(s).</li>
	<li>Submit the DIS ahead of the delivery date for all type of market transactions. DIS can be issued with a future execution date.</li>
	<li>Intimate any change of address or change in bank account details to your DP immediately.</li>
	<li>Before sending securities for demat, record the distinctive numbers of the securities sent.</li>
	<li>The demat account has a nomination facility and it is advisable to appoint a nominee, in case of sole account holders.</li>
	<li>Ensure that, both, your holding and transaction statements are received periodically as instructed to your DP. You are entitled to receive a transaction statement every month if you have any transactions and once a quarter if there has been no transactions in your account.</li>
	<li>Check your demat account statement on receipt. In case you notice any unauthorized debits or credits, contact your DP for clarification. If not resolved, you may contact CDSC&#39;s Investor Relations Officer.</li>
</ul>

<hr />
<p><strong><span style="font-size:14px;">Dont&#39;s</span></strong></p>

<ul>
	<li>Do not leave your instruction slip book with anyone else.</li>
	<li>Do not sign blank DIS as it is equivalent to a bearer cheque.</li>
	<li>Avoid over-writing, cancellations, misspellings, changing of the name and quantity of securities.</li>
	<li>We request you to meticulously follow the above instructions, to avoid any problem in operation of your demat account.</li>
</ul>

<p>&nbsp;</p>
</p>
                        </div>
                    </div>
                </div>
                <!-- Our Partners end -->
            </div>
        </div>
    </div>
</section>
<br>
<!-- Project Details End -->  
 <!-- Footer Start -->
 @include('Home.layouts.footer')
