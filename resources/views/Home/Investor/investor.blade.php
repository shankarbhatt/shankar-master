@include('Home.layouts.header')
<title>World Investor Week: CDSC</title>
<section id="solution" class="p-t-30 p-b-30">

    <div class="container">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row">
                
            </div>
            <div class="col-md-12 col-sm-3 col-xs-3">
           
               <style>
body {
  font-family: Verdana, sans-serif;
  margin: 0;
}

* {
  box-sizing: border-box;
}

.row > .column {
  padding: 0 8px;
}

.row:after {
  content: "";
  display: table;
  clear: both;
}

.column {
  float: left;
  width: 25%;
}

/* The Modal (background) */
.modal {
  display: none;
  position: fixed;
  z-index: 1;
  padding-top: 80px;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: black;
}

/* Modal Content */
.modal-content {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  width: 90%;
  max-width: 1200px;
}

/* The Close Button */
.close {
  color: white;
  position: absolute;
  top: 50px;
  right: 35px;
  font-size: 35px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #999;
  text-decoration: none;
  cursor: pointer;
}

.mySlides {
  display: none;
}

.cursor {
  cursor: pointer;
}

/* Next & previous buttons */
.prev,
.next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -50px;
  color: white;
  background-color:black;
  font-weight: bold;
  font-size: 20px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
  background-color: #ffffff;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #ff0000;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

img {
  margin-bottom: -4px;
  display: block;
 margin-left: auto;
 margin-right: auto;
}

.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}

img.hover-shadow {
  transition: 0.3s;
}

.hover-shadow:hover {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
</style>
<body>

<h2 style="text-align:center">Celebrating World Investor Week 2018</h2>

<div class="row">
  <div class="column">
    <img src="{{ asset('images/investor_week/world_investor_week_pic1.jpg')}}" style="width:100%" onclick="openModal();currentSlide(1)" class="hover-shadow cursor">
  </div>
  <div class="column">
    <img src="{{ asset('images/investor_week/world_investor_week_pic2.jpg')}}" style="width:100%" onclick="openModal();currentSlide(2)" class="hover-shadow cursor">
  </div>
  <div class="column">
    <img src="{{ asset('images/investor_week/world_investor_week_pic3.jpg')}}" style="width:100%" onclick="openModal();currentSlide(3)" class="hover-shadow cursor">
  </div>
  <div class="column">
    <img src="{{ asset('images/investor_week/world_investor_week_pic4.jpg')}}" style="width:100%" onclick="openModal();currentSlide(4)" class="hover-shadow cursor">
  </div>
  </div>
<div class="row" style="margin-bottom: 20px;margin-top: 10px;">
<div class="column">
    <img src="{{ asset('images/investor_week/world_investor_week_pic5.jpg')}}" style="width:100%" onclick="openModal();currentSlide(5)" class="hover-shadow cursor">
  </div>

<div class="column">
    <img src="{{ asset('images/investor_week/world_investor_week_pic6.jpg')}}" style="width:100%" onclick="openModal();currentSlide(6)" class="hover-shadow cursor">
  </div>


<div class="column">
    <img src="{{ asset('images/investor_week/world_investor_week_pic7.jpg')}}" style="width:100%" onclick="openModal();currentSlide(7)" class="hover-shadow cursor">
  </div>

<div class="column">
    <img src="{{ asset('images/investor_week/world_investor_week_pic8.jpg')}}" style="width:100%" onclick="openModal();currentSlide(8)" class="hover-shadow cursor">
  </div>
</div>

<div id="myModal" class="modal">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  
  <div class="modal-content">

    <div class="mySlides">
      <div class="numbertext">1 / 8</div>
      <img src="{{ asset('images/investor_week/world_investor_week_pic1.jpg')}}" style="height:500px;">
    </div>

    <div class="mySlides">
      <div class="numbertext">2 / 8</div>
      <img src="{{ asset('/images/investor_week/world_investor_week_pic2.jpg')}}" style="height:500px;">
    </div>

    <div class="mySlides">
      <div class="numbertext">3 / 8</div>
      <img src="{{ asset('images/investor_week/world_investor_week_pic3.jpg')}}" style="height:500px;">
    </div>
    
    <div class="mySlides">
      <div class="numbertext">4 / 8</div>
      <img src="{{ asset('images/investor_week/world_investor_week_pic4.jpg')}}" style="height:500px;">
    </div>
    
   <div class="mySlides">
      <div class="numbertext">5 / 8</div>
      <img src="{{ asset('images/investor_week/world_investor_week_pic5.jpg')}}" style="height:500px;">
    </div>
    
    <div class="mySlides">
      <div class="numbertext">6 / 8</div>
      <img src="{{ asset('images/investor_week/world_investor_week_pic6.jpg')}}" style="height:500px;">
    </div>
    
    <div class="mySlides">
      <div class="numbertext">7 / 8</div>
      <img src="{{ asset('images/investor_week/world_investor_week_pic7.jpg')}}" style="height:500px;">
    </div>
    
    <div class="mySlides">
      <div class="numbertext">8 / 8</div>
      <img src="{{ asset('images/investor_week/world_investor_week_pic8.jpg')}}" style="height:500px;">
    </div>
   
    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

   </div>
   
</div>


   </div>
</div>

</div>



<script>
function openModal() {
  document.getElementById('myModal').style.display = "block";
}

function closeModal() {
  document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>
                </div>
            </div>
        </div>
    </div>
    </section>
    
<!-- Project Details Start -->
<section id="solution" class="p-t-30 p-b-30">
 
    <div class="container">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row">
           
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div id="global-relation" class="our_team">
                    <div class="row">
                       
                    <div class="col-md-2 col-sm-12 col-xs-12">
            <div class="logo">
                        <a href="{{ url('Home.service.investor') }}"><img src="{{asset('images\investor_week\world_investor_week.png')}}" alt="World Investor Week" /></a>
                               
                    </div>
                </div>
                    
                    <div class="col-md-10 col-sm-12 col-xs-12" style="left: 100px;">
                <div id="global-relation" class="our_team">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading" id="global-relation">
                                <div class="heading_border bg_red"></div>
                                <h2>Activities / Events by CDS and Clearing Limited (CDSC) during World Investor’s Week (01 October 2018 to 07 October 2018)</h2>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 project_details_text">
                           <p class="p_14" style="text-align: justify;">
                          
                                <table style ="width:900px;" class="table table-striped table-bordered table-responsive" id="dynamic-table" >
	<thead> 
		<tr>
			<th style="width:300px;"scope="col">Date: October 2018</th>
			<th scope="col">Activities / Events by CDSC</th>
			
		</tr>
	</thead>
	<tbody>
   
                <tr >
                <td><ul>
                <ol>01 October, 2018</ol> 
                <ol>
    <img src="{{ asset('images/investor_week/world_investor_week_pic1.jpg')}}" style="width:100%" onclick="openModal();currentSlide(1)" class="hover-shadow cursor">
  </ol>
                </ul></td>
                  
                  <td>
                  CDSC staff actively participated in Securities Markets Rally with informative message on specially designed cards. The rally participated by more than one thousand participants, started at 7:50 AM from Bhadrakali Temple (adjacent to Nepal Stock Exchange) and concluded at Bhrikuti Mandap premises with brief addressing from securities market stakeholders that includes Dr. Rewat Bahadur Karki, Chairman of Securities Board of Nepal, Mr. Chandra Singh Saud, Chairman, CDS and Clearing Limited and Chief Executive Officer, Nepal Stock Exchange, Mr. Dev Prakash Gupta, Chief Executive Officer, CDS and Clearing Limited.
                  </td>
                 
</tr>
<tr >
               
                <td><ul>
                <ol>02 October, 2018</ol> 
                <ol>
    <img src="{{ asset('images/investor_week/group.jpg')}}" style="width:100%" class="hover-shadow cursor">
  </ol>
               </ul>
  </td>
                  
                  <td><ul><ol>CDSC collected feedback from various stakeholders on its service offering. 
CDSC also had a detailed discussion with all the stakeholders on its planned launching of its newly designed website and its publications on 03 October 2018. The following publications will be launched:
</ol>
<li>Central Securities Depository and its operational procedure in Nepal.</li>
<li>Post Trade Activities, Clearing and Settlement System in Nepal.</li>
<li>Investors’ Manual of CDSC e-Portal (Mero Share).</li>


</ul>
                  </td>
                  
</tr>
<tr >
<td><ul>
                <ol>03 October, 2018</ol> 
                <ol>
    <img src="{{ asset('images/investor_week/intro_page.png')}}" style="width:100%"  class="hover-shadow cursor">
  </ol></ul>
  </td>
                  
                  <td>
                  CDSC will also be presenting a technical paper Central Depository Services.</br>
CDSC will also be participating in investor awareness programme and address the queries related to its services from various stakeholders.

                  </td>
</tr>

<tr >
<td><ul>
                <ol>04 October, 2018</ol> 
                <ol>
    <img src="{{ asset('images/investor_week/nirdeshan.png')}}" style="width:100%"  class="hover-shadow cursor">
  </ol></ul>
  </td>
                  
                  <td>
                  CDSC will be launching Electronic Instruction Platform “Nirdeshan”. <br/>
                  <br/>

The much awaited, market and investor friendly Electronic Delivery Instruction Slip System (Nirdeshan Purji) will also be launched along with the Nirdeshan Platform.
 
                        </td>               
                  
                
</tr>

<tr >
<td><ul>
<ol>05 October, 2018</ol> 
                <ol>
    <img src="{{ asset('images/investor_week/saud_sir.png')}}" style="width:100%"  class="hover-shadow cursor">
  </ol></ul>
  </td>
                  
                  <td>
                  Mr. Chandra Sing Saud, Chairman of CDS and Clearing Limited will be participating in a panel discussion to be telecasted live on Business Plus Television.                  </td>
                 
</tr>
<tr >
<td><ul>
<ol>06 - 07 October, 2018</ol> 
                <ol>
    <img src="{{ asset('images/investor_week/dmate.png')}}" style="width:100%"  class="hover-shadow cursor">
  </ol></ul>
  </td>
                  
                  <td>
                  Along with its depository participants, CDSC will initiate “One Nepali One DMAT Account” campaign.<br/>
CDSC will also showcase its services by providing information from an information counter (will be placed as a stall in the Provincial Capital Market Expo (PCME). <br/>
We also will be handling queries of investors. We will also be equipped with the leaflets, FAQ, journals and will distribute it which will be beneficial to the visitors.
                 
</tr>
	</tbody>
</table>
</p>
                        </div>
                    </div>
                </div>
                <!-- Our Partners end -->
            </div>
        </div>
    </div>
</section>
<br>

<!-- Project Details End --> 
  <!-- Footer Start -->
  @include('Home.layouts.footer')

