@include('Home.layouts.header')
<title>Death Notice :: Investor </title>

<!-- header end here -->
<section id="solution" class="p-t-60 p-b-40">
    <div class="container">
        <div id="book-closure" class="row">
           
                <h2>Death Notice List</h2>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-10">
                                <form action="deathNoticeSearch" method="GET" role="search">
                                    {{ csrf_field()}}
                                    <div class="col-md-4 ">
                                        <div class="single-query">
                                            <input type="text" placeholder="Enter DP/RTA ID" class="keyword-input" id="user_id" name="user_id">

                                        </div>
                                    </div>

                                    <form action="deathNoticeSearch" method="GET" role="search">
                                        {{ csrf_field()}}
                                        <div class="col-md-4 ">
                                            <div class="single-query">
                                                <input type="text" placeholder="Enter Death Person Name" class="keyword-input" id="name" name="name">

                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <button class="btn-light button-black" id="btn_search_si">Search</button>
                                        </div>
                                    </form>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive" id="listall">
                                <table class="table table-striped table-bordered table-responsive" id="dynamic-table" style="width:100%">
                                    <thead>
                                        <th style="width:50px">S.N</th>
                                        <th style="width:180px">DP/RTA Name</th>
                                        <th style="width:350px">Death Person Name </th>
                                        <th style="width:200px">Address</th>
                                        <th style="width:120px">Published Date</th>
                                        <th style="width:100px">Ending Date</th>
                                        <th style="width:300px">Download</th>
                                    </thead>

                                    <tbody>
                                        <?php $i=0; ?>

                                            <?php    $deathNotice =  DB::table('death_notice')->orderBy('published_date','DESC')->where('status',1)->get();?>
                                                <?php $current_date=date('Y-m-d');?>
                                                    @foreach($deathNotice as $data) @if($data->ending_date>$current_date)
                                                    <tr>
                                                        <td>
                                                            <?php echo ++$i; ?>
                                                        </td>
                                                        <?php if($data->user_type == 3) 
                            $id = DB::table('death_notice')->leftJoin('dp', 'death_notice.user_id', '=', 'dp.dp_id')->where('user_id',
                            $data->user_id)->get(); 
                            else if($data->user_type == 4)  
                           $id = DB::table('death_notice')->leftJoin('rta',
                          'death_notice.user_id', '=', 'rta.rta_id')->where('user_id',$data->user_id)->get(); 
                          else echo "No Data";?>
                                                            @foreach($id as $user) @endforeach
                                                            <td style="text-transform: uppercase;"> {{ $user->name.' ('.$data->user_id.')' }} </td>
                                                            <td>{{$data->name}}
                                                                <li>Father Name: {{$data ->father_name}}</li>
                                                                <li>GrandFather Name: {{$data ->grandfather_name}}</li>
                                                            </td>
                                                            <td>{{$data->address}}</td>
                                                            <td>{{$data->published_date}}</td>
                                                            <td>{{$data->ending_date}}</td>

                                                            <td>
                                                                <a href="{{ asset('death_notice_files/' . $data->file)  }}" download="{{ $data->file }}"> Death Notice  (Please click to download)</i></a>
                                                            </td>

                                                    </tr>
                                                    @endif @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   
</section>

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_search_si").click(function() {
            var script_name = $("#script_name").val();
            var isin = $("#isin").val();
            $.ajax({
                type: "POST",
                url: data: {
                    script_name: script_name,
                    isin: isin
                },
                cache: false,
                success: function(data) {
                    $('#listall').html(data);
                }
            });

        });
    });
</script>
<!-- Footer Start -->
@include('Home.layouts.footer')