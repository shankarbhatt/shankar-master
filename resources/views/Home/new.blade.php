@include('Home.layouts.header')
@include('Home.layouts.nav')  

<!-- Latest News Start -->
<section id="news-section" class="p-t-30 p-b-30">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="row ">
                    <div class="col-md-12">
                       <div class="heading_border bg_red"></div>
                        <h2>Latest News</h2>
                                                <div class="media p-t-20 p-b-20">
                            <div class="media-body">
                                                                <p> <i class="fa fa-calendar"></i> Mar 23, 2018</p>
                                <h4 class="media-heading"><a href="News/details/14.html">Dummy News Five</a></h4>
                            </div>
                        </div>
                                                <div class="media p-t-20 p-b-20">
                            <div class="media-body">
                                                                <p> <i class="fa fa-calendar"></i> Mar 23, 2018</p>
                                <h4 class="media-heading"><a href="News/details/13.html">Dummy News Three</a></h4>
                            </div>
                        </div>
                                                <div class="media p-t-20 p-b-20">
                            <div class="media-body">
                                                                <p> <i class="fa fa-calendar"></i> Mar 23, 2018</p>
                                <h4 class="media-heading"><a href="News/details/12.html">Dummy New Two</a></h4>
                            </div>
                        </div>
                                                <div class="media p-t-20 p-b-20">
                            <div class="media-body">
                                                                <p> <i class="fa fa-calendar"></i> Mar 23, 2018</p>
                                <h4 class="media-heading"><a href="News/details/11.html">Dummy News One</a></h4>
                            </div>
                        </div>
                                                <div class="media p-t-20 p-b-20">
                            <div class="media-body">
                                                                <p> <i class="fa fa-calendar"></i> Mar 23, 2018</p>
                                <h4 class="media-heading"><a href="News/details/10.html">Dummy News</a></h4>
                            </div>
                        </div>
                                            </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="heading">
                    <div class="heading_border bg_red"></div>
                    <h2>News</h2>
                </div>
                
                   <div class="latest_page_box">
                                                                        <h2>Dummy News Five</h2>
                        <p><span><i class="fa fa-calendar"></i> </span> Mar 23, 2018</p>
                        <p>CDS and Clearing Limited, a company established under the company act is a company promoted&nbsp;by Nepal Stock Exchange Limited (NEPSE) in 2010 to provide centralized depository, clearing and settlement services in Nepal. The company is inaugurated on 31st March 2011.</p>

<p><img alt="CDS and Clearing Limited" height="207" src="../../cdscnp.com/images/cdsc_demat.jpg" width="564" /></p>

<p>The main objective of the company is to act as a&#8230;                       
                       <br><br>
                        <a class="btn btn-light button-black" href="News/details/14.html">Read More</a>
                    </div>
                <div class="news_border"></div>
                
                   <div class="latest_page_box">
                                                                        <h2>Dummy News Three</h2>
                        <p><span><i class="fa fa-calendar"></i> </span> Mar 23, 2018</p>
                        <p style="text-align: justify;">CDS and Clearing Limited, a company established under the company act is a company promoted&nbsp;by Nepal Stock Exchange Limited (NEPSE) in 2010 to provide centralized depository, clearing and settlement services in Nepal. The company is inaugurated on 31st March 2011.</p>

<p><img alt="CDS and Clearing Limited" height="207" src="../../cdscnp.com/images/cdsc_demat.jpg" width="564" /></p>

<p style="text-align: justify;">The main objective of the company is&#8230;                       
                       <br><br>
                        <a class="btn btn-light button-black" href="News/details/13.html">Read More</a>
                    </div>
                <div class="news_border"></div>
                
                   <div class="latest_page_box">
                                                                        <h2>Dummy New Two</h2>
                        <p><span><i class="fa fa-calendar"></i> </span> Mar 23, 2018</p>
                        <p>The main objective of the company is to act as a central depository for various instruments (Equity, Bonds, and Warrants etc) especially to handle securities in dematerialized form. This organization is entrusted with the safekeeping, deposit, and with.</p>

<p><a href="webadmin/assets/ckeditor/kcfinder/upload/files/press_release_2074_03_20_multiple_clearing_bank_with_net_settlement.pdf">Download for More </a></p>                       
                       <br><br>
                        <a class="btn btn-light button-black" href="News/details/12.html">Read More</a>
                    </div>
                <div class="news_border"></div>
                
                   <div class="latest_page_box">
                                                                        <h2>Dummy News One</h2>
                        <p><span><i class="fa fa-calendar"></i> </span> Mar 23, 2018</p>
                        <p>The main objective of the company is to act as a central depository for various instruments (Equity, Bonds, and Warrants etc) especially to handle securities in dematerialized form. This organization is entrusted with the safekeeping, deposit, and with</p>                       
                       <br><br>
                        <a class="btn btn-light button-black" href="News/details/11.html">Read More</a>
                    </div>
                <div class="news_border"></div>
                
                   <div class="latest_page_box">
                                               <!--  <div class="news_image">
                            <img src="http://itsutra.com.np/cdscnp/uploads/news/" alt="image">
                        </div> -->
                                                                        <h2>Dummy News</h2>
                        <p><span><i class="fa fa-calendar"></i> </span> Mar 23, 2018</p>
                        <p style="text-align: justify;">&nbsp;</p>

<p style="text-align: justify;"><img alt="" src="webadmin/assets/ckeditor/kcfinder/upload/images/8d2c039bd73f6e79a5741e7f3237c3c0.jpg" style="width: 800px; height: 300px;" /></p>

<p style="text-align: justify;">&nbsp;</p>

<p style="text-align: justify;">The main objective of the company is to act as a central depository for various instruments (Equity, Bonds, and Warrants etc) especially to handle securities in dematerialized form. This organization is entrusted with the safekeeping, deposit, and withdrawal of securities certificates and&#8230;                       
                       <br><br>
                        <a class="btn btn-light button-black" href="News/details/10.html">Read More</a>
                    </div>
                <div class="news_border"></div>
                                <div class="row p-b-80 p-t-80 m-b-60">
                    <div class="col-md-12">
                        <!-- <ul class="pager">
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                          
                        </ul> -->
                                            </div>
                </div>

            </div>
        </div>

    </div>
</section>
<!-- Latest News End --> 
  <!-- Footer Start -->
  @include('Home.layouts.footer')