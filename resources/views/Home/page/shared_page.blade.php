
@include('Home.layouts.header')
@inject('request', 'Illuminate\Http\Request')
<!-- Project Details Start -->
<section id="solution" class="p-t-30 p-b-30">
    <div class="container">
           <div class="row">
             <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="solution_tabs">
                <ul>
                
                          <li class="active"><a>{{ $menu->name }}</a>  <br/>   </li> 
                
                                       <ul class="submenu">
                                      
                                       @if(isset($menu->submenus))
                                             @foreach($menu->submenus as $submenu)
                                               @if(isset($submenu->page->slug))
                                               <title>{{ $submenu->page->title }} :: {{ $menu->name}} </title>
                                               <li class="{{ $request->segment(1) == $submenu->page->slug ? 'active' : '' }}"><a href="{{url($submenu->page->slug)}}">{{ $submenu->name}}</a></li>                                               @else
                                               @endif
                                           @endforeach
                                       @endif
                                       </ul>
                                  
                                
                          
                  </ul>                    
                </div>
             </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div id="global-relation" class="our_team">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading" id="global-relation">
                                <div class="heading_border bg_red"></div>
                                <h2>{{ $data->title }}</h2>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 project_details_text">
                          
                           <p class="p_14" style="text-align: justify;"><p style="text-align: justify;">{{ $data->description}}</p>
                           </p>
                        </div>
                    </div>
                </div>
                <!-- Our Partners end -->
            </div>
        </div>
    </div>
</section>
<br>
<!-- Project Details End -->   <!-- Footer Start -->
   @include('Home.layouts.footer')