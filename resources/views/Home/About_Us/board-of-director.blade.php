@include('Home.layouts.header')
<title>BOD :: About Us</title>

<!-- Project Details Start -->
<section id="solution" class="p-t-30 p-b-30">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="solution_tabs">
            <ul>
            <li class="active"><a>About Us</a>  <br/>   </li> 
                    <ul>
                                                        <li class="">
                                                        <a href="{{url('introduction')}}">Introduction</a>
                                </li>
                                                        <li class="active">
                                                        <a href="{{url('bod')}}">Board Of Directors</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('managementTeam')}}">Management Team</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('globalPartner')}}">Global Partners</a>
                                            
                                </li>
                                            </ul>
                                            </ul>
                </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div id="board-director" class="our_team">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading">
                                <div class="heading_border bg_red"></div>
                                <h2 style="text-align:center;margin-bottom:40px;">Board of Directors</h2>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-md-offset-4">
                            <div class="item">
                                <div class="single-effect">
                                    <figure class="wpf-demo-gallery">
                                                                                <a href="#">
                                                                                            <img src="../../uploads/teams/chandra_sing_saud.jpg" alt="img" style="height:220px;">
                                                                                    </a>
                                                                            </figure>
                                </div>
                                <div class="team_text">
                                    <h3>Mr. Chandra Singh Saud</h3>
                                    <h5>Chairman</h5>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <div class="item">
                                <div class="single-effect">
                                    <figure class="wpf-demo-gallery">
                                                                                <a href="#">
                                                                                            <img src="../../uploads/teams/Niranjan_Phuyal.jpg" alt="img" style="height:220px;">
                                                                                    </a>
                                                                            </figure>
                                </div>
                                <div class="team_text">
                                    <h3>Mr. Niranjan Phuyal</h3>
                                    <h5>Member</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="item">
                                <div class="single-effect">
                                    <figure class="wpf-demo-gallery">
                                                                                <a href="#">
                                                                                            <img src="../../uploads/teams/prabin_pandak_member.jpg" alt="img" style="height:220px;">
                                                                                    </a>
                                                                            </figure>
                                </div>
                                <div class="team_text">
                                    <h3>Mrs. Prabin Pandak</h3>
                                    <h5>Member</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="item">
                                <div class="single-effect">
                                    <figure class="wpf-demo-gallery">
                                                                                <a href="#">
                                                                                            <img src="../../uploads/teams/Murahari_Parajuli.jpg" alt="img" style="height:220px;">
                                                                                    </a>
                                                                            </figure>
                                </div>
                                <div class="team_text">
                                    <h3>Mr. Murahari Parajuli</h3>
                                    <h5>Member</h5>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                    <div class="row" style="left: 50%;transform: translateX(18%);">
                        <div class="col-md-4 col-sm-4">
                            <div class="item">
                                <div class="single-effect">
                                    <figure class="wpf-demo-gallery">
                                                                                <a href="#">
                                                                                            <img src="../../uploads/teams/dev_prakash_gupta.jpg" alt="img" style="height:220px;">
                                                                                    </a>
                                                                            </figure>
                                </div>
                                <div class="team_text">
                                    <h3>Mr. Dev Prakash Gupta</h3>
                                    <h5>Invitee Member</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="item">
                                <div class="single-effect">
                                    <figure class="wpf-demo-gallery">
                                                                                <a href="#">
                                                                                            <img src="../../uploads/teams/Sabina_Pujari.jpg" alt="img" style="height:220px;">
                                                                                    </a>
                                                                            </figure>
                                </div>
                                <div class="team_text">
                                    <h3>Mrs. Sabina Pujari</h3>
                                    <h5>Company Secretary</h5>
                                </div>
                            </div>
                        </div>
                    
                    </div>
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Project Details End -->
   <!-- Footer Start -->
  @include('Home.layouts.footer')