@include('Home.layouts.header')
<title>Global Partners :: About Us </title>


<!-- Project Details Start -->
<section id="solution" class="p-t-30 p-b-30">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="solution_tabs">
                    <ul>
                    <li class="active"><a>About Us</a>  <br/>   </li> 
                    <ul>
                                                        <li class="">
                                                        <a href="{{url('introduction')}}">Introduction</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('bod')}}">Board Of Directors</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('managementTeam')}}">Management Team</a>
                                </li>
                                                        <li class="active">
                                                        <a href="{{url('globalPartner')}}">Global Partners</a>
                                            
                                </li>
                                            </ul>
                                            </ul>
                </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div id="global-relation" class="our_team">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="heading" id="global-relation">
                                    <div class="heading_border bg_red"></div>
                                    <h2 style="margin-bottom:30px;">Global Partners</h2>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                                                    <div class="media">
                                        <div class="media-middle">
                                            <img src="../../images/backend_images/cdsl.jpg">
                                        </div>
                                        <div class="media-body">
                                            <h4><a href="https://www.cdslindia.com/index.html" target="_blank">Centralized Depository Services( India )Limited(Mumbai)</a></h4>
                                            <p class="m-t-15 m-b-20" style="text-align:justify">CDSIL India is the consultant company of CDSC. The depository and settlement services was established in Nepal under the guidance of CDSIL India.</p>
                                        </div>
                                    </div>
                                                                    <div class="media">
                                        <div class="media-middle">
                                            <img src="../../images/backend_images/tata.jpg">
                                        </div>
                                        <div class="media-body">
                                            <h4><a href="https://www.tcs.com/" target="_blank">Tata Consultancy Services (Mumbai)</a></h4>
                                            <p class="m-t-15 m-b-20" style="text-align:justify">The application used for depository and settlement services is developed and maintained by TCS India.</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="media-middle">
                                            <img src="../../images/backend_images/anna.jpg">
                                        </div>
                                        <div class="media-body">
                                            <h4><a href="https://www.anna-web.org/" target="_blank">Association of National Numbering Agencies (ANNA)</a></h4>
                                            <p class="m-t-15 m-b-20" style="text-align:justify">CDSC is a member of ANNA. CDSC provides ISIN (International securities Identification Number) to all the securities listed in depository.</p>
                                        </div>
                                    </div>
                                      <div class="media">
                                        <div class="media-middle">
                                            <img src="../../images/backend_images/asia.png">
                                        </div>
                                        <div class="media-body">
                                            <h4><a href="https://www.acgcsd.org/" target="_blank">Asia Pacific Central Securities Depository Group (Regional)</a></h4>
                                            <p class="m-t-15 m-b-20" style="text-align:justify">CDSC is a member of Asia pacific CSD Group.</p>
                                        </div>
                                    </div>      
                                </div>
                        </div>

                </div>
                <!-- Our Partners end -->
            </div>

        </div>
    </div>
</section>
<!-- Project Details End -->
   <!-- Footer Start -->
  @include('Home.layouts.footer')