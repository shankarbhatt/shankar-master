@include('Home.layouts.header')
<title>Press Release :: CDSC </title>

<section id="positions" class="p-t-60 p-b-40">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                </div>
                <div class="row">
                   
                    <div class="col-md-8" style="width: 1200px;">
                        <div class="heading">
                            <div class="heading_border bg_red"></div>
                            <h2>Press Release</h2>
                        </div>
                                                 <table class="table table-striped table-bordered table-responsive" id="dynamic-table" style="width: 100%">
                            <thead>
                              <th>S.N</th>
                              <th>Title</th>
                              <th>Published Date</th>
                            </thead>
                            <?php $i=0; ?>
                            <tbody>
                             <?php $allPressRelease = DB::table('press_release')->orderBy('published_date','DESC')->where('status',1)->get();?>

                            @foreach($allPressRelease as $PressRelease)
                                                                <tr>
                                                               
                                                                
                                    <td><?php echo ++$i; ?></td>
                                        <td>
                                     
                                      <a href="{{ asset('press_release_files/' . $PressRelease->file) }}" download="{{ $PressRelease->file }}">{{ str_replace('_', ' ',$PressRelease->title)}}</a>
                                          <?php $pubDate = $PressRelease->published_date; if(strtotime($pubDate) > strtotime('-7 day')) {?>
                                        <span class="blink_text" ><img src="../images/backend_images/new.gif"> </span> <?php } else { ?> <span class="blink_text" style="display:none;"><img src="images/backend_images/new.gif"> </span> <?php } ?> 
                                        </td>
                                        <td> <p><span><i class="fa fa-calendar"></i> </span> {{ $PressRelease->published_date }}</p></i></a></td>
                                 
                                    </tr>
                          
                                    @endforeach                       
                            </tbody>
                                                             
                                                    </table>
                                            </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Positions End -->
       <!-- Footer Start -->
       @include('Home.layouts.footer')

