<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Favicon Icon -->
        <link rel="shortcut icon" type="image/x-icon" href="images/img/favicon.ico">

		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:400,600" rel="stylesheet">

		<!-- All css here -->
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/shortcode/shortcodes.css')}}">
        <link rel="stylesheet" href="{{asset('css/animate.css')}}">
        <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
        <link rel="stylesheet" href="{{asset('css/shortcode/style.css')}}">
        <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
        <script src="{{asset('js/vendor/modernizr-2.8.3.min.js')}}"></script>
<!-- Old Css -->
 <!-- Google fonts -->
 <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">

<!-- Required Framework -->
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}" />
<link href="{{asset('banner-slider/css/full-slider.css')}}" rel="stylesheet">

<!-- Required Framework -->
<link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('css/owl.transitions.css')}}" />
<link rel="stylesheet" type="text/css" href="((asset('css/settings.css')}}" />

<!-- Fonts Icons-->
<link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('css/copious-icon.css')}}" />
<link href="{{ asset('fonts/backend_fonts/css/font-awesome.css') }}" rel="stylesheet" />
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>

<!-- Navbar Css -->
<link rel="stylesheet" type="text/css" href="{{asset('css/bootsnav.css')}}" />

<link href="{{asset('datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css" media="screen">

<!-- Custom Css -->
<link rel="stylesheet" type="text/css" href="{{asset('css/chosen.min.css')}}" />

<link rel="stylesheet" type="text/css" href="{{asset('css/datatable.css')}}" />

<link rel="stylesheet" type="text/css" href="{{asset('css/color.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('css/tiles.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}" />
<script src="{{asset('js/jquery.2.2.3.min.js')}}"></script>



    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

		<!-- Header Area Start -->
		<header class="header-area">
		    <!-- <div class="header-top blue-bg fix hidden-xs">
                <div class="container">
                    <div class="row">

                        <div class="col-md-9 col-sm-9">
                            <div class="header-icons">
                                <a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a>
                                <a href="https://twitter.com/login?lang=en"><i class="fa fa-twitter"></i></a>
                                <a href="https://youtube.com"><i class="fa fa-youtube"></i></a>
                                <a href="https://np.linkedin.com/"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                        <button type="submit" class="default-button">Submit Now</button>
                        </div>
                    </div>
                </div>
		    </div> -->
			<div class="header-main bg-green white">
				<div class="container">
					<div class="row">
                        <div class="col-md-12">
                            <div class="logo-wrapper" style="margin-left: -17px;">
                                <div class="logo">
                                    <a href="{{ url('Home.index') }}"><img src="{{asset('images/img/logo/logo1.png')}}" alt="CDS and Clearing Ltd" /></a>
                                </div>
                            </div>
                            <div class="header-main-content">
                                <div class="header-info">
                                    <img src="{{asset('images/img/icons/phone.png')}}" alt="">
                                    <div class="header-info-text">
                                        <h4>01-4238008, 4216068</h4>
                                        <span>We are open 10 am - 5pm</span>
                                    </div>
                                </div>
                                <div class="header-info">
                                    <img src="{{asset('images/img/icons/message.png')}}" alt="">
                                    <div class="header-info-text">
                                        <h4>info@cdsc.com.np</h4>
                                        <span>You can mail us</span>
                                    </div>
                                </div>
                            </div>
                            <div class="quote-btn">
                                <button><a href="https://meroshare.cdsc.com.np/" onClick="javascript: window.open('https://meroshare.cdsc.com.np/');" target="_blank"></a> MERO SHARE</button>
                            </div>
                          
                        </div>
					</div>
				</div>
            </div>
            <!-- Nav div started -->
            <div class="mainmenu-area fixed header-sticky hidden-xs">
			    <div class="container">
			        <div class="row">
						<div class="col-md-12 col-xs-12">
                            <div class="main-menu text-center">
                                <nav>
                                    <ul>
                                    <li class="" id="home"><a href="{{ url('/') }}">HOME</a></li>
                                    @foreach($allMenu as $menu)
                                        <li><a href="#">{{ strtoupper($menu->name) }}</a>
                                            <ul class="submenu">
                                            @foreach($menu->submenus as $submenu)
                                                <li><a href="{{url($submenu->page->slug)}}">{{ $submenu->name}}</a></li>
                                            @endforeach
                                            </ul>
                                        </li>
                                       @endforeach
                                <li><a href="{{url('contact-us')}}">CONTACT</a></li>
                                    
                                    </ul>
                                </nav>
                            </div>
						</div>
			        </div>
			    </div>
			</div>
			<!-- Main Menu Area Start -->
            <!-- Mobile Menu Area start -->
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul>
                                    <li class="" id="home"><a href="{{ url('/') }}">HOME</a></li>

                                        <li><a href="#">ABOUT</a>
                                            <ul class="submenu">
                                                <li>  <a href="{{url('Home.About-Us.introduction')}}">Introduction</a></li>
                                                <li>   <a href="{{url('Home.About-Us.board-of-directory')}}">Board of Directors</a></li>
                                                <li>     <a href="{{url('Home.About-Us.management-team')}}">Management Team</a></li>
                                                <li>    <a href="{{url('Home.About-Us.global-partner')}}">Global Partners</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">INVESTOR</a>
                                            <ul class="submenu megamenu">
                                                <li>
                                                    <ul>
                                                    <li>
                                                    <a href="{{url('Home.Investor.fqa')}}">FAQ</a>


                                        </li>
                                                    <li>
                                                                                        <a href="{{url('Home.Investor.isinscript')}}" >ISIN & SCRIPT</a>


                                        </li>
                                                                                                                        <li>
                                                                                        <a href="{{url('Home.Investor.book-closure')}}">Book Closure Info</a>


                                        </li>
                                                                                                                        <li >
                                                                                        <a href="{{url('Home.Investor.settlementid')}}">Settlement ID</a>


                                        </li>
                                                                                                                        <li>
                                                                                        <a href="{{url('Home.Investor.registered-company')}}" link = "Investor">Registered Company</a>


                                        </li>
                                                                                                                        <li>
                                                                                        <a href="{{url('Home.Investor.dos-dont')}}">Dos & Don'ts</a>


                                        </li>

                                                    </ul>
                                                </li>

                                            </ul>
                                        </li>
                                        <li><a href="{{url('Home.Depository-Participants.introduction')}}">DP</a>
                                            <ul class="submenu">
                                            <li >
                                                                                        <a href="{{url('Home.Depository-Participants.introduction')}}">Introduction</a>


                                        </li>
                                                                                                                        <li>
                                                                                        <a href="{{url('Home.Depository-Participants.numberofdp')}}" link = "Depository-Participants">Number of DP</a>


                                        </li>
                                                                                                                        <li>
                                                                                        <a href="{{url('Home.Depository-Participants.cdsc-tariff')}}">CDSC Tariff</a>


                                        </li>
                                                                                                                        <li>
                                                                                        <a href="{{url('Home.Depository-Participants.services')}}" >Services</a>


                                        </li>
                                                                                                                        <li>
                                                                                        <a href="{{url('Home.Depository-Participants.admission-procedure')}}">Admission Procedure</a>


                                        </li>
                                                                                                                        <li>
                                                                                        <a href="{{url('Home.Depository-Participants.hardwaresoftware')}}">Hardware & Software Requirement</a>


                                        </li>
                                                                                                                        <li>
                                                                                        <a href="{{url('Home.Depository-Participants.operating')}}">Operating Instruction</a>


                                        </li>
                                            </ul>
                                        </li>
                                        <li><a href="{{url('Home.RTA.introduction')}}">RTA</a>
                                            <ul class="submenu">

                                            <li>
                                                                                        <a href="{{url('Home.RTA.introduction')}}">Introduction</a>


                                        </li>
                                                                                                                        <li>
                                                                                        <a href="{{url('Home.RTA.rtalist')}}">RTA LIST</a>


                                        </li>
                                                                                                                        <li>
                                                                                        <a href="{{url('Home.RTA.admission-procedure')}}">Admission Procedure</a>


                                        </li>
                                                                                                                        <li>
                                                                                        <a href="{{url('Home.RTA.operating-instruction')}}">Operating Instruction</a>


                                        </li>
                                                                                                                        <li>
                                                                                        <a href="{{url('Home.RTA.documentationforcooperativeauction')}}">Documentation for Cooperate Action</a>


                                        </li>
                                            </ul>
                                        </li>
                                        <li><a href="{{url('Home.Clearing-Member.introduction')}}">CLEARING MEMBER</a>
                                            <ul class="submenu">
                                            <li >
                                                                                        <a href="{{url('Home.Clearing-Member.introduction')}}">Introduction</a>


                                        </li>
                                                                                                                        <li>
                                                                                        <a href="{{url('Home.Clearing-Member.clearingmember')}}">List of Clearing Member</a>


                                        </li>
                                                                                                                        <li>
                                                                                        <a href="{{url('Home.Clearing-Member.settlement-procedure')}}">Settlement Procedure</a>


                                        </li>
                                            </ul>
                                        </li>
                                        <li><a href="{{url('Home.Issuer.introduction')}}">ISSUER</a>
                                        <ul class="submenu">
                                        <li>
                                                                                        <a href="{{url('Home.Issuer.introduction')}}">Introduction</a>


                                        </li>
                                                                                                                        <li>
                                                                                        <a href="{{url('Home.Issuer.benefits')}}">Benefits</a>


                                        </li>
                                                                                                                        <li>
                                                                                        <a href="{{url('Home.Issuer.admission-procedure')}}" >Admission Procedure</a>


                                        </li>
                                                                            </ul>
                                        </li>
                                        <li><a href="#">PUBLICATION</a>
                                        <ul class="submenu">
                                        <li><a href="{{url('Home.Publication.circulars')}}">Circulars</a></li>
                                        <li><a href="{{url('Home.Publication.bylaws')}}">By Laws</a></li>

                                                                            </ul>
                                        </li>
                                        <li><a href="{{url('Home.download')}}">DOWNLOADS</a></li>
                                <li><a href="{{url('Home.contact-us')}}">CONTACT</a></li>
                                        <li><a href="#">MERO SHARE</a></li>
                                    </ul>
                                </nav>
                            </div>
						</div>
			        </div>
			    </div>
			</div>
            </header>