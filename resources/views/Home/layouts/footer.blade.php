   <!--Start of Footer Widget-area-->
   <div class="footer-widget-area bg-4 overlay-blue pt-110 pb-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-sm-3 col-xs-12">
                        <div class="single-footer-widget">
                            <div class="footer-logo">
                                <a href="/"><img src="{{asset('images/img/logo/logo3.png')}}" alt="FINANCO"></a>
                            </div>
                            <h3 style="margin-left: 20px;">Social Media</h3>
                             <div class="col-md-12" >
                             <a href="https://www.facebook.com/CDSnClearing/"><span class="fa fa-facebook-square" style="font-size:28px;color:white;margin-left: 8px;"></span></a>
                             <!-- <a href="https://www.youtube.com/"<i class="fa fa-youtube-square" style="font-size:28px;color:white;margin-left: 8px;"></i>
                             <a href="https://www.twitter.com"><i class="fa fa-twitter-square" style="font-size:28px;color:white;margin-left: 8px;"></i></a> -->
                            </div>
                            
                            <!-- mailchimp-alerts Start -->
                            <div class="mailchimp-alerts text-centre fix text-small">
                                <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                                <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                                <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                            </div>
                            <!-- mailchimp-alerts end -->
                        </div>
                    </div>
                    <div class="col-lg-3 hidden-md hidden-sm col-xs-12">
                        <div class="single-footer-widget">
                            <h3>External Link</h3>
                            <ul class="footer-list">
                                 <li> <a href="{{url('http://sebon.gov.np/')}}" target="_blank">SEBON</a></li>
                            <li>   <a href="{{url('http://www.nepalstock.com/')}}" target="_blank">NEPSE</a></li>
                             <li>   <a href="{{url('http://mof.gov.np/en/')}}" target="_blank">Ministry of Finance</a></li>
                               <!-- <li>    <a href="{{url('https://www.cdslindia.com/')}}" target="_blank">CDSL India</a></li>
                                <li> <a href="{{url('https://www.tcs.com/')}}" target="_blank">TATA Consultancy</a></li> 
                                <li>   <a href="{{url('https://www.anna-web.org/')}}" target="_blank">ANNA</a></li>
                                 <li>   <a href="{{url('https://www.acgcsd.org/')}}" target="_blank">ACG</a></li> -->
                               
                                <li>   <a href="#" target="_blank"></a></li>
                            </ul>
                        </div>
                    </div>
                      <div class="col-lg-3 hidden-md hidden-sm col-xs-12">
                        <div class="single-footer-widget">
                            <h3>Quick Link</h3>
                            <ul class="footer-list">
                            <li> <a href="{{url('cdscintroduction')}}">About Us</a></li>
                            <li> <a href="{{url('bod')}}">Board of Directors</a></li>
                            <li> <a href="{{url('managementTeam')}}">Management Team</a></li>
                            <li> <a href="{{url('globalPartner')}}">Global Partners</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 hidden-md hidden-sm col-xs-12">
                        <div class="single-footer-widget">
                            <h3>Quick Link</h3>
                             <ul class="footer-list">
                            <li> <a href="{{url('listofdp')}}">List of DP</a></li>
                            <li> <a href="{{url('listofrta')}}">List of RTA</a></li>
                            <li> <a href="{{url('listofclearingmember')}}">List of CM</a></li>
                            <li> <a href="{{url('registeredcompany')}}">Registered Company</a></a></li>
                            </ul>
                        </div>
                    </div>
                    
                 <!--   <div class="col-lg-3  col-sm-4 col-xs-12">
                        <div class="single-footer-widget">
                            <h3>CONTACT US</h3>
                            <div class="footer-contact-info">
                                <img src="{{asset('images/img/icons/f-map.png')}}" alt="">
                                <span class="block">Share Market Commercial Complex Putalisadak, Kathmandu, Nepal<br>
                               </span>
                            </div>
                            <div class="footer-contact-info">
                                <img src="{{asset('images/img/icons/f-phone.png')}}" alt="">
                                <span class="block">Telephone : +977 01-4238008<br>
                                Telephone : +977 01-4216068</span>
                            </div>
                            <div class="footer-contact-info">
                                <img src="{{asset('images/img/icons/f-globe.png')}}" alt="">
                                <span class="block">Email : info@cdsc.com.np<br>
                                Web : www.cdsc.com.np</span>
                            </div>
                          
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
        <!--End of Footer Widget-area-->
        <!-- Start of Footer area -->
       <footer class="footer-area bg-blue ptb-30">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footer-text"  style="text-align:center;">
                            <span class="block">Copyright &copy; <?php echo date('Y'); ?> CDS and Clearing Ltd</a>. All Rights Reserved.</span>
                        </div>
                    </div>
                    
                    </div>
                </div>
        </footer>
        <!-- End of Footer area -->
        
		<!-- All js here -->
        <script src="{{asset('js/vendor/jquery-1.12.4.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/owl.carousel.min.js')}}"></script>
        <script src="{{asset('js/ajax-mail.js')}}"></script>
        <script src="{{asset('js/jquery.ajaxchimp.min.js')}}"></script>
        <script src="{{asset('js/jquery.magnific-popup.js')}}"></script>
        <script src="{{asset('js/counterup.js')}}"></script>	
        <script src="{{asset('js/plugins.js')}}"></script>
        <script src="{{asset('js/canvasjs.min.js')}}"></script>	
        <script src="{{asset('js/canvas-active.js')}}"></script>	
        <script src="{{asset('js/main.js')}}"></script>
    </body>
</html>


            <!-- Back-To-Top -->
            <!-- <div class="container">
                <a href="#" class="back-to-top text-center" style="display: inline;">
                    <i class="fa fa-angle-up"></i>
                </a>
            </div> -->

            <!-- Team Pop Up End -->
            <!-- Team Pop Up End -->

            <!--Required JS -->
            <script src="{{asset('js/jquery.2.2.3.min.js')}}"></script>
            <script src="{{asset('js/bootstrap.min.js')}}"></script>

            
            <!-- SMOOTH SCROLL -->  
            <script type="text/javascript" src="{{asset('js/scroll-desktop.js')}}"></script>
            <script type="text/javascript" src="{{asset('js/scroll-desktop-smooth.js')}}"></script>

            <script src="{{asset('banner-slider/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
            <script src="{{asset('js/owl.carousel.min.js')}}"></script>
            <script src="{{asset('js/bootsnav.js')}}"></script>
            <script src="{{asset('js/wow.min.js')}}"></script>
            <script src="{{asset('js/jquery.parallax-1.1.3.js')}}"></script>
            <script src="{{asset('js/bootstrap-number-input.js')}}"></script>
            <script src="{{asset('js/renge_selectbox-0.2.min.js')}}"></script>
            <script src="{{asset('js/range-Slider.min.js')}}"></script>
            <script src="{{asset('js/jquery.counterup.js')}}"></script>
            <script src="{{asset('js/zelect.js')}}"></script>
            
            <!-- Progress  -->
            <script src="{{asset('js/progressbar.js')}}"></script>
            <script src="{{asset('js/copious_custom.js')}}"></script>
            <script src="{{asset('datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
            <script src="{{asset('chosen.jquery.min.js')}}"></script>


        <script src="{{asset('jquery.dataTables.min.js')}}"></script>
            <script src="{{asset('jquery.dataTables.bootstrap.min.js')}}"></script> 
            <script type="text/javascript">
                /* Smooth-Scroll */

                $('.chosen-select').chosen();
                
    //$("#dynamic-table").DataTable();
    $('#dynamic-table').dataTable( {
        searching: false,
        order: [],
        "lengthMenu": [ [20, 25, 50, -1], [20, 25, 50, "All"] ],
        "sScrollX": "100%"  
    });

    
    $(".scroll").click(function(event) {

        event.preventDefault();
        $('html,body').animate({
            scrollTop: $(this.hash).offset().top
        }, 2000);
    });
    //Check to see if the window is top if not then display button
    var offset = 650;
    var duration = 300;
    $(window).scroll(function() {
        if ($(this).scrollTop() > offset) {
            $('.back-to-top').fadeIn(200);
        } else {
            $('.back-to-top').fadeOut(200);
        }
    });

    $('.back-to-top').on("click", function(event) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 500);
        return false;
    });

    $("table").addClass('table table-bordered');
    // function search_func(value)
    // {
    //     $.ajax({
    //        type: "GET",
    //        url: "http://itsutra.com.np/cdscnp/Home/checkSubscribers",
    //        data: {'search_keyword' : value},
    //        dataType: "text",
    //        type:'POST',
    //        success: function(msg){
    //          if(msg==="true"){
    //             $("#emailexist").text('You have already Subscribed');
    //             $("#subForm").submit(function(e){
    //                 e.preventDefault();
    //             });

    //         }else{
    //             // $("#emailexist").hide();
    //             $("#emailexist").text('');
    //             $("#subForm").unbind('submit');
    //         }
    //     }
    // });
    // }
</script>


</body>

</html>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAOBKD6V47-g_3opmidcmFapb3kSNAR70U&amp;libraries=places"></script>
<script src="assets/js/custom-map.js"></script>
<script type="text/javascript">
	function showUrlInDialog(teamID){
		var tag = $("#team");
		// var teamID = $(this).data("id"); 
		// console.log(id);
		// $(".team_id").on('click', function(){
		// 	var id = $(this).id();
		// 	console.log(id);
		// });
		$.ajax({
			url: 'http://itsutra.com.np/cdscnp/Home/getTeamByID',
			data:{teamID:teamID},
			type:'POST',
			dataType:'json',
			success: function(data) {
				var image = JSON.parse(JSON.stringify(data));
				var filename = image[0].ImageFile;
				var title = image[0].Title;
				var tagline = image[0].TagLine;
				var imagename = "http://itsutra.com.np/cdscnp/uploads/files/" + filename;
				$(".popup_image img").attr('src', imagename);
				$(".pop_text h3").text(title);
				$(".popup_p_text").text(tagline);
				// console.log(imagename);
			}
		});
	}
</script>
<!-- js form nchl -->
<link href="{{asset('css/global.css')}}" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="{{asset('js/jquery-1.6.1.min.js')}}"></script>
	
	<!--[if lte IE 6]><script src="js/ie6/warning.js"></script><script>window.onload=function(){e("js/ie6/")}</script><![endif]-->
	
	<!-- javaScript -->
	<!--[if IE 6]>
		<script type="text/javascript" src="js/DD_belatedPNG.js"></script>
		<script type="text/javascript" src="js/ie_fix.js"></script>
	<![endif]-->
	
	
	<script type="text/javascript" src="{{asset('js/jquery.nivo.slider.pack.js')}}"></script>
	<link rel="stylesheet" type="text/css" media="screen" href="{{asset('css/nivo-slider.css')}}" />
	<script type="text/javascript">
	$(window).load(function() {						 
		$('.slider').nivoSlider({
			effect:'random',
			directionNav:false, //Next & Prev
			controlNav:true, //1,2,3...
			pauseOnHover:true, //Stop animation while hovering
			slices:25,
			animSpeed:1000,
			pauseTime:4000
		});
		
	});
	
	</script>
	
	<script src="{{asset('js/jcarousellite_1.0.1c4.js')}}" type="text/javascript"></script>
	<script type="text/javascript">
	$(function() {
		$(".newsticker-jcarousellite").jCarouselLite({
			vertical: true,
			hoverPause:true,
			visible: 5,
			auto:100,
			speed:3000
		});
	});
    </script>
    <script type="text/javascript">
	$(function() {
		$(".newsticker-jcarousellite1").jCarouselLite({
			vertical: true,
			hoverPause:true,
			visible: 5,
			auto:0,
			speed:00
		
		});
	});
	</script>
 