@include('Home.layouts.header')
@include('Home.layouts.nav')  
                </script><section id="news-section" class="p-t-30 p-b-30">
    <div class="container">
        <div class="row">
           <!--  <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="row ">
                    <div class="col-md-12">
                       <div class="heading_border bg_red"></div>
                        <h2>Latest Notices</h2>
                                                <div class="media p-t-20 p-b-20">
                            <div class="media-body">
                                                                <p> <i class="fa fa-calendar"></i> Mar 11, 2018</p>
                                <h4 class="media-heading"><a href="http://itsutra.com.np/cdscnp/Notices/8">Your Title</a></h4>
                            </div>
                        </div>
                                                <div class="media p-t-20 p-b-20">
                            <div class="media-body">
                                                                <p> <i class="fa fa-calendar"></i> Nov 10, 2017</p>
                                <h4 class="media-heading"><a href="http://itsutra.com.np/cdscnp/Notices/7">What is Lorem Ipsum?</a></h4>
                            </div>
                        </div>
                                                <div class="media p-t-20 p-b-20">
                            <div class="media-body">
                                                                <p> <i class="fa fa-calendar"></i> Nov 10, 2017</p>
                                <h4 class="media-heading"><a href="http://itsutra.com.np/cdscnp/Notices/6">What is Lorem Ipsum?</a></h4>
                            </div>
                        </div>
                                                <div class="media p-t-20 p-b-20">
                            <div class="media-body">
                                                                <p> <i class="fa fa-calendar"></i> Jan 02, 2018</p>
                                <h4 class="media-heading"><a href="http://itsutra.com.np/cdscnp/Notices/5">Dummy Text Generator </a></h4>
                            </div>
                        </div>
                                            </div>
                </div>
            </div> -->

            <div class="col-md-12">
                <div class="heading">
                    <div class="heading_border bg_red"></div>
                    <h2>Notices</h2>
                </div>
                
                   <div class="latest_page_box">
                                                <h4>Your Title</h4>
                        <p><span><i class="fa fa-calendar"></i> </span>Mar 11, 2018</p><br><br>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem&#8230;                       <br><br>
                        <a class="btn btn-light button-black" href="Notices/8.html">Read More</a>
                    </div>
                <div class="news_border"></div>
                
                   <div class="latest_page_box">
                                                <h4>What is Lorem Ipsum?</h4>
                        <p><span><i class="fa fa-calendar"></i> </span>Nov 10, 2017</p><br><br>
                        <p style="text-align: justify;">Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially&#8230;                       <br><br>
                        <a class="btn btn-light button-black" href="Notices/7.html">Read More</a>
                    </div>
                <div class="news_border"></div>
                
                   <div class="latest_page_box">
                                                <h4>What is Lorem Ipsum?</h4>
                        <p><span><i class="fa fa-calendar"></i> </span>Nov 10, 2017</p><br><br>
                        <p style="text-align: justify;">Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially&#8230;                       <br><br>
                        <a class="btn btn-light button-black" href="Notices/6.html">Read More</a>
                    </div>
                <div class="news_border"></div>
                
                   <div class="latest_page_box">
                                                <h4>Dummy Text Generator </h4>
                        <p><span><i class="fa fa-calendar"></i> </span>Jan 02, 2018</p><br><br>
                        <p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,&#8230;                       <br><br>
                        <a class="btn btn-light button-black" href="Notices/5.html">Read More</a>
                    </div>
                <div class="news_border"></div>
                                
            </div>
        </div>

    </div>
</section>   <!-- Footer Start -->
@include('Home.layouts.footer')
