@include('Home.layouts.header')
@include('Home.layouts.nav')
<!-- Positions Start -->
<section id="positions" class="p-t-60 p-b-40">
    <div class="container">
        <div class="heading_border bg_red"></div>
        <h2>Downloads</h2>
        <br>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <h3>Circulars</h3>
                <br>
                <div class="project_details_text">
                                                            <ul class="pro-list">
                       
                        <li class="">
                            <a href="uploads/downloads/notice_20740318_annual_fee.pdf" download="notice_20740318_annual_fee.pdf" title="Download">By Laws Sample 2</a>
                        </li>
                    </ul>
                                        <ul class="pro-list">
                       
                        <li class="">
                            <a href="uploads/downloads/3ISSUER%2c_RTA_and_CDS_agreement.pdf" download="3ISSUER,_RTA_and_CDS_agreement.pdf" title="Download">Sample 3</a>
                        </li>
                    </ul>
                                        <ul class="pro-list">
                       
                        <li class="">
                            <a href="uploads/downloads/3ISSUER%2c_RTA_and_CDS_agreement.pdf" download="3ISSUER,_RTA_and_CDS_agreement.pdf" title="Download">Sample 2</a>
                        </li>
                    </ul>
                                        <ul class="pro-list">
                       
                        <li class="">
                            <a href="uploads/downloads/3ISSUER%2c_RTA_and_CDS_agreement.pdf" download="3ISSUER,_RTA_and_CDS_agreement.pdf" title="Download">Sample 1</a>
                        </li>
                    </ul>
                                    </div>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12">
                <h3>By Laws</h3><br>
                <div class="project_details_text">
                                                            <ul class="pro-list">
                       
                        <li class="">
                            <a href="uploads/downloads/notice_20740318_annual_fee.pdf" download="notice_20740318_annual_fee.pdf" title="Download">By Laws Sample 5</a>
                        </li>
                    </ul>
                                        <ul class="pro-list">
                       
                        <li class="">
                            <a href="uploads/downloads/notice_20740318_annual_fee.pdf" download="notice_20740318_annual_fee.pdf" title="Download">By Laws Sample 4</a>
                        </li>
                    </ul>
                                        <ul class="pro-list">
                       
                        <li class="">
                            <a href="uploads/downloads/notice_20740318_annual_fee.pdf" download="notice_20740318_annual_fee.pdf" title="Download">By Laws Sample 3</a>
                        </li>
                    </ul>
                                        <ul class="pro-list">
                       
                        <li class="">
                            <a href="uploads/downloads/notice_20740318_annual_fee.pdf" download="notice_20740318_annual_fee.pdf" title="Download">Sample 1</a>
                        </li>
                    </ul>
                                    </div>
            </div>
        </div>
        <div class="row">
                        
                    </div>
    </div>
</section>
<!-- Positions End  
 <!-- Footer Start -->

   @include('Home.layouts.footer')
