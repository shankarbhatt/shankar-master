@include('Home.layouts.header')
<title>CDS and Clearing Limited</title>
<section class="rev_slider_wrapper">
<div id="cover-spin"><img align="middle" src="{{ asset('images/backend_images/cdsc_logo.png')}}" alt="CDS and Clearing Ltd" style="margin-top:200px;" /></div>
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
            <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
            <li data-target="#carousel-example-generic" data-slide-to="3" class=""></li>
        </ol>

        <!-- Wrapper for slides -->
      
        @php
    $active = $allPhotos->shift();
    @endphp
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="{{asset('/slider_images/'.$active->file)}}" alt="">
                </div>
                @foreach($allPhotos as $photo)
                <div class="item ">
                    <img src="{{asset('slider_images/'.$photo->file)}}" alt="">
                </div>
                @endforeach
            </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="tp-leftarrow tparrows" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="tp-rightarrow tparrows" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>

<!-- Latested Notices and Circular -->
<!-- Blog Area Start -->

<div class="blog-area pt-120 pb-115"></div>
<div class="container">

    <div class="row">
        <div class-="row">
            <div class=" col-md-4">
                <div class="event">

                    <div class="newTop"></div>
                    <span class="eveTitle title">News &amp; Notices</span>
                    <div class="clear"></div>
                    <div class="newContent">
                        <div class="newsticker-jcarousellite" style="visibility: visible;overflow: hidden;position: relative;z-index: 2;left: 0px;height: 160px; ">
                            <ul>

                              @foreach($allNews as $news)
                                <li>

                                    <div class="thumbnail">
                                         <?php $pubDate = $news->published_date; if(strtotime($pubDate) > strtotime('-7 day')) {?>
                                     <span class="blink_text" ><img src="images/backend_images/new.gif"> </span> <?php } else { ?> <span class="blink_text" style="display:none;"><img src="images/backend_images/new.gif"> </span> <?php } ?> 
                                    <a href="{{ asset('news_notice_files/' .$news->file) }}" download="{{$news->file }}">{{ str_replace('_', ' ', $news->title)}}</a>

                                     </div>

                                </li>
                               @endforeach     

                            </ul>
                           </div>

                </div>

       <div class="clear" style="height:10px;"></div>

</div>
<div class="more" style="font-size:11px;"><a href="{{asset('Home/news')}}">VIEW ALL ...</a></div>
</div>
<div class=" col-md-4">
                <div class="event">

      <div class="newTop"></div>

              <span class="eveTitle title">Press Release</span>
              <div class="clear"></div>
              <div class="newsticker-jcarousellite"  style="visibility: visible;overflow: hidden;position: relative;z-index: 2;left: 0px;height: 160px; ">
              
<ul>

@foreach($allPressRelease as $PressRelease)
             <li>

      <div class="thumbnail">
    <?php $pubDate = $PressRelease->published_date; if(strtotime($pubDate) > strtotime('-7 day')) {?>
    <span class="blink_text" ><img src="images/backend_images/new.gif"> </span> <?php } else { ?> <span style="display:none;"><img src="images/backend_images/new.gif"> </span> <?php } ?> 

       <a href="{{ asset('press_release_files/' .$PressRelease->file) }}" download="{{ $PressRelease->file }}">{{ str_replace('_', ' ', $PressRelease->title)}}</a>



</div>

  </li>
  @endforeach     

</ul>


      </div>

       <div class="clear" style="height:10px;"></div>

</div>
  <div class="more" style="font-size:11px;"><a href="{{asset('Home/pressrelease')}}">VIEW ALL ...</a></div>
</div>

<div class=" col-md-4">
<div class="event" style="height: 235px;">

      <div class="newTop"></div>
                 <span class="eveTitle title"> Circulars</span>
                  
                  <div class="clear"></div>
                        @foreach($allCircular as $law)
                        <div class="downLeft"><a href="{{ asset('downloads_files/' . $law->file) }}" download="{{ $law->file }}">{{ str_replace('_', ' ', $law->title)}}</a></div>

                                  
                                  @endforeach  

   <div class="more" style="font-size:11px; margin-left:-17px;"><a  style="margin-left:15px;margin-top:5px;" href="{{asset('circulars')}}">VIEW ALL ...</a></div>      
                         
                          </div>
                         </div>
                       </div>
                    </div>
                </div>
            </div>
      
<!-- End Of New Section-->
		<!-- Background Area End -->

        <!-- Question Ask Area Start -->

		<!-- Question Ask Area End -->
        <!-- About Area Start -->

        <!-- About Area End -->
        <!-- Information Area Start -->

        <!-- Information Area End -->
        <!-- Services Area Start -->
    <!--   <div class="service-area pt-120 pb-105">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                        <div class="section-title text-center mb-55">
                            <h2>Our Services</h2>

                        </div>
                    </div>
                </div>
                <div class="row">
                <div class='row'>
                    <div class="col-md-4">
                        <div class="single-item">
                            <span class="service-image">
                                <img src="{{asset('images/img/icons/plan.png')}}" alt="">
                            </span>
                            <div class="service-text">
                                <h4>Account Opening</h4>
                                <p>To utilise the services offered by a depository, any person having investment in any security or intending to invest in securities needs to have a demat account with a CDSC-DP.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-item">
                            <span class="service-image">
                                <img src="{{asset('images/img/icons/cash.png')}}" alt="">
                            </span>
                            <div class="service-text">
                                <h4>Dematerialisation</h4>
                                <p>Dematerialisation is a process by which physical certificates (of shares / debentures / other securities) are converted into electronic balances.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-item">
                            <span class="service-image">
                                <img src="{{asset('images/img/icons/brif.png')}}" alt="">
                            </span>
                            <div class="service-text">
                                <h4>Transmission of securities</h4>
                                <p>CDSC offers a facility for transmission of balances held in BO account/s (to other BO account/s).</p>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class='row'>
                    <div class="col-md-4">
                        <div class="single-item">
                            <span class="service-image">
                                <img src="{{asset('images/img/icons/commo.png')}}" alt="">
                            </span>
                            <div class="service-text">
                                <h4>Account Statement</h4>
                                <p>Generally a DP sends to the BO, a statement of his account, if required by BO, if there is any transaction in the account. The balances and transactions can also be viewed by the BOs through CDSC web based facility ‘Mero Share’.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-item">
                            <span class="service-image">
                                <img src="{{asset('images/img/icons/hand.png')}}" alt="">
                            </span>
                            <div class="service-text">
                                <h4>Rematerialization</h4>
                                <p>Rematerialisation is the process by which the electronic balances held in the demat account can be converted back into physical certificates.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="single-item">
                            <span class="service-image">
                                <img src="{{asset('images/img/icons/reti.png')}}" alt="">
                            </span>
                            <div class="service-text">
                                <h4>Pledging</h4>
                                <p>If the BO decides to pledge any securities in his BO account, he can avail of the same by submitting the pledge creation form duly completed, to his DP. </p>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>-->
        <!-- Services Area End -->
        <!-- Advertise Area Start -->

        <!-- Advertise Area End -->
        <!-- Map Area Start -->

        <div class="asked-area pt-120 pb-85 bg-light" style="margin-bottom:20px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="sub_title">
                        <h3 style=" margin-left: 5px;  padding-bottom: -10px;">Depository Participants (DPs) Networks</h3>

                        </div>
                        <div class="google-maps">
                    <iframe src="
https://www.google.com/maps/d/embed?mid=1p0CjuNcGUxDRL1U4zJCjiOpV9UZBV8lO"
width="1200" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
                        </div>

                    </div>
            </div>
        </div>

        <!-- Map Area End -->
        <!-- Projects Area Start -->

        <!-- Projects Area End -->
        <!-- Asked Area Start -->

        <!-- Asked Area End -->
        <!-- Fun Factor Start -->
        <div class="fun-factor-area bg-3  pt-100 fix pb-95" style="background-color: #F5F7F9;margin-bottom: 15px;">
            <div class="container">
                <div class="fun-custom-row">
                      <div class="section-title text-center mb-55">
                            <h2>CDSC Updates</h2>

                        </div>

 <div class="fun-custom-column">
                        <div class="single-fun-factor">
                            <div class="text-icon block">
                                <!-- <img src="{{asset('images/img/icons/teams.png')}}" alt="" class="mr-15"> -->
                                <h2> <a href="{{url('listofdp')}}"><span class=""><?php if (empty($totalDP)) echo "0"; else echo count($totalDP); ?></span></a></h2>
                            </div>
                            <h4> <a href="{{url('listofdp')}}">Licensed Depository Participants</a></h4>
                        </div>
                    </div>

                      <div class="fun-custom-column">
                        <div class="single-fun-factor">
                            <div class="text-icon block">
                               <h2>  <a href="{{url('registeredcompany')}}"><span class=""><?php if (empty($totalIssuer)) echo "0"; else echo count($totalIssuer); ?></span></span></a></h2>
                            </div>
                            <h4>  <a href="{{url('registeredcompany')}}">Registered Company</a></h4>
                        </div>
                    </div>

                    <div class="fun-custom-column">
                        <div class="single-fun-factor">
                            <div class="text-icon block">
                                <!-- <img src="{{asset('images/img/icons/trophy.png')}}" alt="" class="mr-15"> -->
                                <h2><a href="{{url('listofrta')}}"><span class=""><?php if (empty($totalRTA)) echo "0"; else echo count($totalRTA); ?></span></span></a></h2>
                            </div>
                            <h4><a href="{{url('listofrta')}}">RTA List</a></h4>
                        </div>
                    </div>

                   <div class="fun-custom-column">
                        <div class="single-fun-factor">
                            <div class="text-icon block">
                               <h2> <a href="{{url('listofclearingmember')}}"><span class=""><?php if (empty($totalCM)) echo "0"; else echo count($totalCM); ?></span></a></h2>
                            </div>
                            <h4> <a href="{{url('listofclearingmember')}}">Registered Clearing Member</a></h4>
                        </div>
                    </div>

                    <div class="fun-custom-column">
                        <div class="single-fun-factor">
                            <div class="text-icon block">
                                <!-- <img src="{{asset('images/img/icons/face.png')}}" alt="" class="mr-15"> -->
                                <h2><a href="{{url('registeredbankinmeroshare')}}"><span class=""><?php if (empty($totalMeroShareDP)) echo "0"; else echo count($totalMeroShareDP); ?></span></a></h2>
                            </div>
                            <h4><a href="{{url('registeredbankinmeroshare')}}">Registered DPs in MEROSHARE</a></h4>
                        </div>
                    </div>

                    <div class="fun-custom-column">
                        <div class="single-fun-factor">
                            <div class="text-icon block">
                        <h2><a href="{{url('casbabank')}}"><span class=""><?php if (empty($totalBank)) echo "0"; else echo count($totalBank); ?></span></a></h2>
                            </div>
                            <h4><a href="{{url('casbabank')}}">Registered Bank In C-ASBA</a></h4>
                        </div>
                    </div>

                    <div class="fun-custom-column">
                        <div class="single-fun-factor">
                            <div class="text-icon block">
                                <!-- <img src="{{asset('images/img/icons/check.png')}}" alt="" class="mr-15"> -->
                                @foreach($allDailyUpdates as $bo)
                                <h2><span class="">{{ number_format($bo->no_of_bo_account) }}</span></h2>
                                @endforeach
                            </div>
                            <h4>Beneficial Owners' Demat Account</h4>
                        </div>
                    </div>

                     <div class="fun-custom-column">
                        <div class="single-fun-factor">
                            <div class="text-icon block">
                              @foreach($allDailyUpdates as $demat)
                                <h2><span class="">{{ number_format($demat->no_of_demat_shares) }}</span></h2>
                                @endforeach
                            </div>
                            <h4>No. of Shares in Demat Form</h4>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <style>
    .google-maps {
        position: relative;
        padding-bottom: 30%; // This is the aspect ratio
        height: 0;
        overflow: hidden;
    }
    .google-maps iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100% !important;
        height: 100% !important;
    }
</style>
<!-- //for web loader -->
<style type="text/css">
   #cover-spin {
    position:fixed;
    width:100%;
    left:0;right:0;top:0;bottom:0;
    background-color: #732231;
    z-index:9999;
    text-align: center;
}

@-webkit-keyframes spin {
    from {-webkit-transform:rotate(0deg);}
    to {-webkit-transform:rotate(360deg);}
}

@keyframes spin {
    from {transform:rotate(0deg);}
    to {transform:rotate(360deg);}
}

#cover-spin::after {
    content:'';
    display:block;
    position:absolute;
    left:48%;top:40%;
    width:40px;height:40px;
    border-style:solid;
    border-color:white;
    border-top-color:transparent;
    border-width: 4px;
    border-radius:50%;
    -webkit-animation: spin .8s linear infinite;
    animation: spin .8s linear infinite;
}
</style>

        @include('Home.layouts.footer')
        <script>
          
          setTimeout('document.getElementById("cover-spin").style.display="none"', 500);  // 0.5 seconds
    </script>