@include('Home.layouts.header')
<title>News & Notices :: CDSC</title>

<section id="positions" class="p-t-60 p-b-40">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                </div>
                <div class="row">
                   
                    <div class="col-md-8"style="
    width: 1200px;">
                        <div class="heading">
                            <div class="heading_border bg_red"></div>
                            <h2>News and Notices</h2>
                        </div>
                                                 <table class="table table-striped table-bordered table-responsive" id="dynamic-table" style="width: 100%">
                            <thead>
                              <th>S.N</th>
                              <th>Title</th>
                              <th>Published Date</th>
                            </thead>
                            <?php $i=0; ?>
                            <tbody>
                            <?php $allNewsNotices = DB::table('news_notice')->orderBy('published_date','DESC')->where('status',1)->get();?>

                            @foreach($allNewsNotices as $news_notice)
                                                                <tr>
                                                               
                                                                
                                    <td><?php echo ++$i; ?></td>
                                        <td>
                                     
                                        <a href="{{ asset('news_notice_files/' .$news_notice->file) }}" download="{{$news_notice->file }}">{{ str_replace('_', ' ', $news_notice->title)}}</a>
                                        <?php $pubDate = $news_notice->published_date; if(strtotime($pubDate) > strtotime('-7 day')) {?>
                                     <span class="blink_text" ><img src="../images/backend_images/new.gif"> </span> <?php } else { ?> <span class="blink_text" style="display:none;"><img src="images/backend_images/new.gif"> </span> <?php } ?>
                                        </td>
                                        <td> <p><span><i class="fa fa-calendar"></i> </span> {{ $news_notice->published_date }}</p></i></a></td>
                                 
                                    </tr>
                          
                                    @endforeach                       
                            </tbody>
                                                             
                                                    </table>
                                            </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Positions End -->
       <!-- Footer Start -->
       @include('Home.layouts.footer')

