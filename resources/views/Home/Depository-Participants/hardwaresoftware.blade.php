@include('Home.layouts.header') 
<title>Requirements :: DP</title>
<!-- Project Details Start -->
<section id="solution" class="p-t-30 p-b-30">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="solution_tabs">
            <ul>
                    <li class="active"><a>DP</a>  <br/>   </li> 
                    <ul>
                                                        <li class="">
                                                        <a href="{{url('dpintroduction')}}">Introduction</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('listOfDP')}}" link = "Depository-Participants">Number of DP</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('admissionprocedure')}}">Admission Procedure</a>
                                </li>
                                                        <li class="active">
                                                        <a href="{{url('hardwaresoftwarerequired')}}">Hardware & Software Requirement</a>
                                </li>
                                <li>
                                                        <a href="{{url('cdsc-tariff')}}">CDSC Tariff</a>
                                </li>
                                                      
                                            </ul>
                                            </ul>
                </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div id="global-relation" class="our_team">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading" id="global-relation">
                                <div class="heading_border bg_red"></div>
                                <h2>Hardware & Software Requirement</h2>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 project_details_text">
                           <p class="p_14" style="text-align: justify;"><h3 style="text-align: justify;"><u>Hardware Requirement</u></h3>

<p style="text-align: justify;"><u>Server PC - 1</u></p>

<ul>
	<li>Processor - Minimum 2.5 GHz or Higher.</li>
	<li>RAM - Minimum 1 GB or Higher.</li>
	<li>Hard Disk Drive - 100 GB or Higher.</li>
	<li>CD/DVD Drive.</li>
	<li>NIC Card.</li>
	<li>Operating System - Windows Server 2003 - 32 Bit.</li>
	<li>Antivirus Software.</li>
</ul>

<h3><ins>WorkStation PC - Required Nos</ins></h3>

<ul>
	<li>Processor - Minimum 2.5 GHz or Higher.</li>
	<li>RAM - Minimum 1 GB or Higher.</li>
	<li>Hard Disk Drive - 100 GB or Higher.</li>
	<li>NIC Card.</li>
	<li>Operating System - Windows XP Professional - 32 Bit.</li>
</ul>

<h3><ins>Network Hardware</ins></h3>

<ul>
	<li>Configurable Router/Firewall - 1.</li>
	<li>(Features: IPSEc, SSL for VPN).</li>
	<li>Switch - 24 Ports - 1.</li>
	<li>Network Cables - As Required.</li>
</ul>

<h3><ins>Electricity Backup</ins></h3>

<ul>
	<li>Electricity Backup for Load Shedding.</li>
	<li>Front Office Data Backup facilities either on Tape or other Back Media.</li>
</ul>

<h3><ins>Printer &amp; Scanner &amp; Fax - Nos</ins></h3>

<ul>
	<li>As required.</li>
</ul>

<h3><ins>Database Software/System Software/Application Software</ins></h3>

<ul>
	<li>Microsoft SQL Server 2008 Express Edition.</li>
	<li>Microsoft .NET Framework 4.0.</li>
</ul>
</p>
                        </div>
                    </div>
                </div>
                <!-- Our Partners end -->
            </div>
        </div>
    </div>
</section>
<br>
<!-- Project Details End -->  
 <!-- Footer Start -->
 @include('Home.layouts.footer')
