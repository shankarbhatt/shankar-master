@include('Home.layouts.header')
@include('Home.layouts.nav')

<!-- Page Banner End -->
<!-- Project Details Start -->
<section id="solution" class="p-t-30 p-b-30">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="solution_tabs">
                    <ul>
                                                        <li class="">
                                                        <a href="{{url('Home.Depository-Participants.introduction')}}">Introduction</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('Home.Depository-Participants.numberofdp')}}" link = "Depository-Participants">Number of DP</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('Home.Depository-Participants.cdsc-tariff')}}">CDSC Tariff</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('Home.Depository-Participants.services')}}" >Services</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('Home.Depository-Participants.admission-procedure')}}">Admission Procedure</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('Home.Depository-Participants.hardwaresoftware')}}">Hardware & Software Requirement</a>
                                </li>
                                                        <li class="active">
                                                        <a href="{{url('Home.Depository-Participants.operating')}}">Operating Instruction</a>
                                </li>
                                            </ul>
                </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div id="global-relation" class="our_team">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading" id="global-relation">
                                <div class="heading_border bg_red"></div>
                                <h2>Operating Instruction</h2>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 project_details_text">
                           <p class="p_14" style="text-align: justify;"><p style="text-align: justify;">CDS and Clearing Limited, a company established under the company act is a company promoted&nbsp;by Nepal Stock Exchange Limited (NEPSE) in 2010 to provide centralized depository, clearing and settlement services in Nepal. The company is inaugurated on 31st March 2011. The main objective of the company is to act as a central depository for various instruments (Equity, Bonds, and Warrants etc) especially to handle securities in dematerialized form. This organization is entrusted with the safekeeping, deposit, and withdrawal of securities certificates and transfer of ownership/rights of the said instruments. The depository functions will be performed by the company under the securities regulations of Securities Board of Nepal (SEBON).CDSC is a wholly owned subsidiary company of Nepal Stock Exchange Ltd. (NEPSE) which was established on 7th Poush 2067.</p>
</p>
                        </div>
                    </div>
                </div>
                <!-- Our Partners end -->
            </div>
        </div>
    </div>
</section>
<br>
<!-- Project Details End --> 
  <!-- Footer Start -->
  @include('Home.layouts.footer')
