@include('Home.layouts.header')
<title>CDSC Traifff :: DP</title>
<!-- Project Details Start -->
<section id="solution" class="p-t-30 p-b-30">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
             <div class="solution_tabs">
                <ul>
                    <li class="active"><a>DP</a>  <br/>   </li> 
                    <ul>
                                                        <li class="">
                                                        <a href="{{url('dpintroduction')}}">Introduction</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('listofdp')}}" link = "Depository-Participants">List of DP</a>
                                </li>
                                                        <li>
                                                        <a href="{{url('admissionprocedure')}}">Admission Procedure</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('hardwaresoftwarerequired')}}">Hardware & Software Requirement</a>
                                </li>
                                <li class="active">
                                                        <a href="{{url('cdsc-tariff')}}">CDSC Tariff</a>
                                </li>
                                                      
                                            </ul>
                                            </ul>
                </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div id="global-relation" class="our_team">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading" id="global-relation">
                                <div class="heading_border bg_red"></div>
                                <h2>CDSC Tariff</h2>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 project_details_text">
                           <p class="p_14" style="text-align: justify;">
                               <table class="table table-striped table-bordered table-responsive" border="1" cellpadding="1" cellspacing="1">
	<thead>
		<tr>
			<th scope="col">S.N</th>
			<th scope="col">Title</th>
			<th scope="col">Category</th>
			<th scope="col">Amount</th>
			<th scope="col">CDSC Amount</th>
		</tr>
	</thead>
	<tbody>
	    <?php $cdsc = DB::table('cdsc_tariff')->where('status',1)->where('deleted_at',null)->orderBy('title','ASC')->get(); ?>
	    <?php $i =0;?>
    @foreach($cdsc as $cdsc_tariff)
                <tr>
                  <td ><?php echo ++$i;?></td>
                  <td >{{ $cdsc_tariff->title }}</td>
                   <?php $category = DB::table('cdsc_tariff')->leftJoin('cdsc_tariff_category', 'cdsc_tariff.category', '=', 'cdsc_tariff_category.id')->where('category',$cdsc_tariff->category)->get(); ?>
                                    @foreach($category as $data2)
                                    <?php $cat = str_replace('_', ' ',$data2->title); ?>
                                     @endforeach 
                  <td >{{ $cat}}</td>
                  <td>{{ $cdsc_tariff->amount }}</td>
                  <td>{{ $cdsc_tariff->cdsc_amount }}</td>
</tr>
@endforeach
	</tbody>
</table>
</p>
                        </div>
                    </div>
                </div>
                <!-- Our Partners end -->
            </div>
        </div>
    </div>
</section>
<br>
<!-- Project Details End --> 
  <!-- Footer Start -->
  @include('Home.layouts.footer')
