@include('Home.layouts.header')
<title>List Of DP :: DP </title>
<!-- Page Banner End -->
<section id="solution" class="p-t-60 p-b-40">
    <div class="container">
        <div id="book-closure" class="row">
            <div class="col-md-12 p-b-50" id="dp">
                <div class="heading_border bg_red"></div>
                <h2>Licensed Depository Participants In MeroShare</h2>
               <div class="row">
<div class="col-md-12">
<form action="Home.Depository-Participants.searchdp" method="GET" role="search">
{{ csrf_field()}}
<div class="row">
<div class="col-md-5 col-sm-4">
<div class="single-query">

<!-- <select class="chosen-select" id="dp_id">
<option value="">Select DPID</option>
<option value="10100">10100</option>
</select> -->
</div>
</div>

<div class="col-md-2">

</div>
</div>
</div>
</form>
</div>


                                <div class="table-responsive" id="listall">
                             
                    <table class="table table-striped table-bordered table-responsive" id="dynamic-table">
                        <thead>
                            <tr>
                                <th>S.N</th>
                                <th style="width:10%;">DPID</th>
                                <th style="width:30%;">DP NAME</th>
                                <th style="width:20%;">ADDRESS</th>
                                <th style="width:15%;">PHONE NO.</th>
                                <th style="width:30%;">EMAIL ID</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $dp = DB::table('dp',1)->where('deleted_at',null)->where('meroshare',1)->orderBy('name','ASC')->get(); ?>

                            <?php $i =0;?>
                        @foreach($dp as $dps)
                                 <tr>
                                 <td ><?php echo ++$i;?></td>
                  <td >{{ $dps->dp_id }}</td>
                  <td style="width:30%;">{{ $dps->name }}</td>
                  <td style="width:30%;">{{ $dps->address }}</td>
                  <td>{{ $dps->phone }}</td>
                  <td style="width:30%;">{{ $dps->email }}</td>
                                </tr>
                                @endforeach              
                                                    </tbody>
                    </table> 
                    
                    
                </div>
                            </div>
        </div>
    </div>
</section>

<
<!-- Footer Start -->
@include('Home.layouts.footer')
