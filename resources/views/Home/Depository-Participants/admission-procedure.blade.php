@include('Home.layouts.header')
@include('Home.layouts.nav')
<title>Admission :: DP</title>
<!-- Page Banner End -->
<!-- Project Details Start -->
<section id="solution" class="p-t-30 p-b-30">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="solution_tabs">
                <ul>
                    <li class="active"><a>DP</a>  <br/>   </li> 
                    <ul>
                                                        <li class="">
                                                        <a href="{{url('dpintroduction')}}">Introduction</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('listofdp')}}" link = "Depository-Participants">List of DP</a>
                                </li>
                                                        <li class="active">
                                                        <a href="{{url('admissionprocedure')}}">Admission Procedure</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('hardwaresoftwarerequired')}}">Hardware & Software Requirement</a>
                                </li>
                                <li>
                                                        <a href="{{url('cdsc-tariff')}}">CDSC Tariff</a>
                                </li>
                                                      
                                            </ul>
                                            </ul>
                </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div id="global-relation" class="our_team">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading" id="global-relation">
                                <div class="heading_border bg_red"></div>
                                <h2>Admission Procedure</h2>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 project_details_text">
                           <p class="p_14" style="text-align: justify;">

<p style="text-align: justify;">For getting registered as Depository Participant in CDSC, the said company has to get the registration certificate from SEBON. There are certain criteria which the DP has to fulfill which are stated in the CDSC Byelaws. Upon fulfillment of the criteria set up in the byelaw of CDSC, the company is selected as the registered Depository Participant of CDSC.</p>

<p><a href="{{ url('/aggrement/cds and DP Agreement Letter.pdf') }}" class="btn btn-inverse btn-mini" download="cds and DP Agreement Letter.pdf" title="Download File">The agreement between CDSC and DP is as follows: </a></p></p>

</p>
                        </div>
                    </div>
                </div>
                <!-- Our Partners end -->
            </div>
        </div>
    </div>
</section>
<br>
<!-- Project Details End -->  
 <!-- Footer Start -->
   @include('Home.layouts.footer')
