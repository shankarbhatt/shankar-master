@include('Home.layouts.header')
@include('Home.layouts.nav')             
    <section class="rev_slider_wrapper">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                        <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                                       <div class="item active">
                            <img src="uploads/files/8d2c039bd73f6e79a5741e7f3237c3c0.jpg" alt="">
                       </div>
                                           <div class="item ">
                            <img src="uploads/files/f15b4f91279387abd4ac82f14c2b4bda.jpg" alt="">
                       </div>
                                </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="tp-leftarrow tparrows" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="tp-rightarrow tparrows" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>
    <section id="s_services" class="p-t-100 p-b-50">
        <div class="container">
            <div class="row p-t-40 p-b-40">
                <div class="col-md-8">
                    <div class="heading">
                        <div class="heading_border bg_red"></div>
                        <h2>Latest Notices</h2>
                    </div>
                    <div id="services_slider" class="owl-carousel">
                                                <div class="item">
                            <div class="services">         
                                <h3 class="text-uppercase"><a href="Notices/8.html">Your Title</a></h3>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa&#8230;                                <br>
                                <a href="Notices/8.html" style="font-size:15px">[READ MORE]</a>
                            </div>
                        </div>
                                            <div class="item">
                            <div class="services">         
                                <h3 class="text-uppercase"><a href="Notices/7.html">What is Lorem Ipsum?</a></h3>
                                <p style="text-align: justify;">Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the&#8230;                                <br>
                                <a href="Notices/7.html" style="font-size:15px">[READ MORE]</a>
                            </div>
                        </div>
                                            <div class="item">
                            <div class="services">         
                                <h3 class="text-uppercase"><a href="Notices/6.html">What is Lorem Ipsum?</a></h3>
                                <p style="text-align: justify;">Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the&#8230;                                <br>
                                <a href="Notices/6.html" style="font-size:15px">[READ MORE]</a>
                            </div>
                        </div>
                                            <div class="item">
                            <div class="services">         
                                <h3 class="text-uppercase"><a href="Notices/5.html">Dummy Text Generator </a></h3>
                                <p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et&#8230;                                <br>
                                <a href="Notices/5.html" style="font-size:15px">[READ MORE]</a>
                            </div>
                        </div>
                                        </div>
                                        <a href="Notices.html" class="btn-light button-black pill-right">View All </a>
                                    </div>
                <div class="circulars col-md-4 col-sm-4 col-xs-12">
                    <div class="heading">
                        <div class="heading_border bg_red"></div>
                        <h2>Circulars</h2>
                    </div>
                     <div>
                        <div class="over_image"></div>
                        <div class="category_box have_qus m-b-0">
                            <ul class="pro-list">
                                                                    <li> <a href="uploads/downloads/notice_20740318_annual_fee.pdf" download="notice_20740318_annual_fee.pdf" title="Download">By Laws Sample 2</a></li>
                                                                    <li> <a href="uploads/downloads/3ISSUER%2c_RTA_and_CDS_agreement.pdf" download="3ISSUER,_RTA_and_CDS_agreement.pdf" title="Download">Sample 3</a></li>
                                                                    <li> <a href="uploads/downloads/3ISSUER%2c_RTA_and_CDS_agreement.pdf" download="3ISSUER,_RTA_and_CDS_agreement.pdf" title="Download">Sample 2</a></li>
                                
                            </ul>
                            <a href="Publication/Circulars.html" class="btn-light button-black">View All</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="p-b-40">
        <div class="container">
            <div class="heading_border bg_red"></div>
            <h2 class=" text-left">CDSC UPDATES</h2>
            <div class="col1">
                <div class="grid_full">
                    <div class="tile royal_blue">
                        <div class="tile-content" style="top: 0px;">
                            <div class="content one">
                                <h1>
                                    <a href="Home/Investor/Registered-Company.html">
                                        Registered Companies
                                    </a>
                                </h1>
                            </div>
                            <div class="content two ie7">
                                <p>
                                    <a href="Home/Investor/Registered-Company.html">
                                        262                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col2">
                <div class="grid_full">
                    <div class="tile royal_blue">
                        <div class="tile-content" style="top: 0px;">
                            <div class="content one">
                                <h1>
                                    <a href="Clearingmember.html">
                                        Registered Clearing Members
                                    </a>
                                </h1>
                            </div>
                            <div class="content two ie7">
                                <p>
                                    <a href="Clearingmember.html">
                                        1                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="col3">
                <div class="grid_full">
                    <div class="tile royal_blue">
                        <div class="tile-content" style="top: 0px;">
                            <div class="content one">
                                <h1>
                                    <a href="Numberofdp.html">
                                        Licensed Depository Participants
                                    </a>
                                </h1>
                            </div>
                            <div class="content two ie7">
                                <p>
                                    <a href="Numberofdp.html">
                                        61                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="col4">
                <div class="grid_full">
                    <div class="tile royal_blue">
                        <div class="tile-content" style="top: 0px;">
                            <div class="content one">
                                <h1>
                                    <a href="#">
                                        Beneficial Owners' Demat Account
                                    </a>
                                </h1>
                            </div>
                            <div class="content two ie7">
                                <p>
                                    <a href="#">
                                        5000                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="col5">
                <div class="grid_full">
                    <div class="tile royal_blue">
                        <div class="tile-content" style="top: 0px;">
                            <div class="content one">
                                <h1>
                                    <a href="#">
                                        No. of Demat Shares
                                    </a>
                                </h1>
                            </div>
                            <div class="content two ie7">
                                <p>
                                    <a href="#">
                                        2228726650                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col5">
                <div class="grid_full">
                    <div class="tile royal_blue">
                        <div class="tile-content" style="top: 0px;">
                            <div class="content one">
                                <h1>
                                    <a href="Rtalist.html">
                                        RTA<br><span>LIST</span>
                                    </a>
                                </h1>
                            </div>
                            <div class="content two ie7">
                                <p>
                                    <a href="Rtalist.html">
                                        59                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col5">
                <div class="grid_full">
                    <div class="tile royal_blue">
                        <div class="tile-content" style="top: 0px;">
                            <div class="content one">
                                <h1>
                                    <a href="Rtalist.html">
                                        SETTLEMENT ID
                                    </a>
                                </h1>
                            </div>
                            <div class="content two ie7">
                                <p>
                                    <a href="Rtalist.html">
                                        59                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col4">
                <div class="grid_full">
                    <div class="tile royal_blue">
                        <div class="tile-content" style="top: 0px;">
                            <div class="content one">
                                <h1>
                                    <a href="RegisterBankinCasba.html">
                                        Registered Banks in C-ASBA
                                    </a>
                                </h1>
                            </div>
                            <div class="content two ie7">
                                <p>
                                    <a href="RegisterBankinCasba.html">
                                        54                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col5">
                <div class="grid_full">
                    <div class="tile royal_blue">
                        <div class="tile-content" style="top: 0px;">
                            <div class="content one">
                                <h1>
                                    <a href="RegisteredDPsinMEROSHARE.html">
                                        Registered DPs in MEROSHARE
                                    </a>
                                </h1>
                            </div>
                            <div class="content two ie7">
                                <p>
                                    <a href="RegisteredDPsinMEROSHARE.html">
                                        65                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <!-- Latest News & update Start -->
    <section id="latest_news" class="p-b-100 p-t-100 bg_gray">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="heading">
                            <div class="heading_border bg_red"></div>
                            <h2>Latest News</h2>
                        </div>
                        <div id="latest_news-slider" class="owl-carousel">
                                                            <div class="item">
                                    <div class="feature_box">
                                        <a href="News/details/14.html">
                                            <h3>Dummy News Five</h3><br>
                                            <p>CDS and Clearing Limited, a company established under the company act is a company promoted&nbsp;by&#8230;                                        </a>
                                    </div>
                                </div>
                                                                <div class="item">
                                    <div class="feature_box">
                                        <a href="News/details/13.html">
                                            <h3>Dummy News Three</h3><br>
                                            <p style="text-align: justify;">CDS and Clearing Limited, a company established under the company act is a&#8230;                                        </a>
                                    </div>
                                </div>
                                                                <div class="item">
                                    <div class="feature_box">
                                        <a href="News/details/12.html">
                                            <h3>Dummy New Two</h3><br>
                                            <p>The main objective of the company is to act as a central depository for various&#8230;                                        </a>
                                    </div>
                                </div>
                                                                <div class="item">
                                    <div class="feature_box">
                                        <a href="News/details/11.html">
                                            <h3>Dummy News One</h3><br>
                                            <p>The main objective of the company is to act as a central depository for various&#8230;                                        </a>
                                    </div>
                                </div>
                                                                <div class="item">
                                    <div class="feature_box">
                                        <a href="News/details/10.html">
                                            <h3>Dummy News</h3><br>
                                            <p style="text-align: justify;">&nbsp;</p>

<p style="text-align: justify;"> </p>

<p style="text-align: justify;">&nbsp;</p>

<p style="text-align: justify;">The main objective&#8230;                                        </a>
                                    </div>
                                </div>
                                                            </div>
                                                        <a href="News.html" class="btn-light button-black m-t-40 pill-right">View All </a>
                                                    </div>
                  </div>
              </div>
          </div>
    </section>
    <!-- Latest News & update end -->

      <!-- BG + Text _3 Start -->
            <section id="bg_text" class="bg_3" style="background:url('uploads/news/5512dee76b3f0f2c7ff34c4d922057c9.jpg') no-repeat; background-position:50% -224px; background-attachment: fixed;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 style="color:#000">Latest Press Releases</h2>
                    <p class="">Get Press Release of CDS Over Here.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- BG + Text _3 end -->
    <!-- Feature Start -->
    <section id="feature" class="bg_gray feature_slider p-b-40">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div id="latest_news-slider_1" class=" owl-carousel ">
                                                        <div class="item">
                                <div class="feature_box">
                                    <a href="Pressrelease/viewPressrelease/10.html">
                                        <h4>Some of the facts about the NEPSE</h4><br>
                                        <p style="text-align: justify;">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem&#8230;                                        </p>
                                </a>
                            </div>
                        </div>
                                                    <div class="item">
                                <div class="feature_box">
                                    <a href="Pressrelease/viewPressrelease/9.html">
                                        <h4>Press Release</h4><br>
                                        <p style="text-align: justify;">Press Release on 2073-09-24 (Please Click to download)                                        </p>
                                </a>
                            </div>
                        </div>
                                                    <div class="item">
                                <div class="feature_box">
                                    <a href="Pressrelease/viewPressrelease/8.html">
                                        <h4>Press Release on 2074-03-16.</h4><br>
                                        <p style="text-align: justify;">Press Release on 2074-03-16                                        </p>
                                </a>
                            </div>
                        </div>
                                                    <div class="item">
                                <div class="feature_box">
                                    <a href="Pressrelease/viewPressrelease/7.html">
                                        <h4>Press Release on 2074-03-20 Regarding Multiple Clearing Bank along with Net Settlement </h4><br>
                                        <p style="text-align: justify;">Press Release on 2074-03-20 Regarding Multiple Clearing Bank along with Net Settlement                                        </p>
                                </a>
                            </div>
                        </div>
                                            </div>
                </div>
                                <a href="Pressrelease.html" class="btn-light button-black">View All </a>
                                
            </div>
        </div>
    </div>
</section>
<!-- Feature End -->

<!-- Broker -->
  <!--   <section id="brokers" class="bg_gray p-b-40">
        <div class="container">
            <div class="row"> -->
                <!-- <div class="col-md-5 col-sm-5 col-xs-12">
                    <div class="broker_box">
                                                    <img src="http://itsutra.com.np/cdscnp/assets/images/finance/broker.jpg" alt="image">
                                                
                    </div>
                </div> -->
                
                <!-- <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="heading">
                        <div class="heading_border bg_red"></div>
                        <h2>FAQ</h2>
                    </div>

                    <div id="accordion" role="tablist" aria-multiselectable="true">
                                                
                            
                        </div>
                    </div> -->
          <!--   </div>
        </div>
    </section> -->

    <script type="text/javascript">
        $(document).ready(function() {
         
        $('#carouselExampleIndicators').carousel({
          interval: 3000,
          pause: null
        });


       });
   </script>  
    <!-- Footer Start -->
    @include('Home.layouts.footer')
