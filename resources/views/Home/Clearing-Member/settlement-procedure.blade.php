@include('Home.layouts.header')
<title>Settlement :: Clearing Member</title>
<!-- Page Banner End -->
<!-- Project Details Start -->
<section id="solution" class="p-t-30 p-b-30">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="solution_tabs">
            <ul>
                    <li class="active"><a>Clearing Member</a>  <br/>   </li> 
                    <ul>
                                                        <li class="">
                                                        <a href="{{url('cmintroduction')}}">Introduction</a>                                </li>
                                                        <li class="">
                                                        <a href="{{url('listofclearingmember')}}">List of Clearing Member</a>
                                </li>
                                                        <li class="active">
                                                        <a href="{{url('cmsettlementprocedure')}}">Settlement Procedure</a>
                                </li>
                                            </ul>
                                            </ul>
                </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div id="global-relation" class="our_team">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading" id="global-relation">
                                <div class="heading_border bg_red"></div>
                                <h2>Settlement Procedure</h2>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 project_details_text">
                           <p class="p_14" style="text-align: justify;"><p>Settlement runs in a T+3 cycle.&nbsp;The list of things done in the 3 days are as follows:</p>

<ul>
	<li>Trade file load (T day).</li>
	<li>Base price entry for CGT (T day to T+2 day).</li>
	<li>Funds/security payin (T+3 day).</li>
	<li>CGT calculation (T+3 day).</li>
	<li>Shortage/Closeout calculation (T+3 day).</li>
	<li>Funds/security Payout (T+3 day).</li>
	<li>Commission credit (T+3 day).</li>
</ul>
</p>
                        </div>
                    </div>
                </div>
                <!-- Our Partners end -->
            </div>
        </div>
    </div>
</section>
<br>

<!-- Project Details End -->   
@include('Home.layouts.footer')