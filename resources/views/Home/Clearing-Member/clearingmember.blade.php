
@include('Home.layouts.header')
<title>List of CM :: Clearing Member</title>
<!-- header end here -->
<section id="solution" class="p-t-60 p-b-40">
    <div class="container">
        <div id="book-closure" class="row">
            <div class="col-md-12 p-b-50" id="dp">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="solution_tabs">
                <!-- <ul>
                    <li class="active"><a href="#">Clearing Member</a>  <br/>   </li> 
                    <ul>
                                                        <li class="">
                                                        <a href="{{url('cmintroduction')}}">Introduction</a>                                </li>
                                                        <li class="active">
                                                        <a href="{{url('listofclearingmember')}}">List of Clearing Member</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('cmsettlementprocedure')}}">Settlement Procedure</a>
                                </li>
                                            </ul>
                                            </ul> -->
                </div>
            </div>
                <div class="heading_border bg_red"></div>
                <h2>List of Clearing Member</h2>
                
                <div class="table-responsive" id="listall">
                    <table class="table table-responsive table-striped table-bordered" id="dynamic-table" style="width: 100%">
                        <thead>
                           <tr>
                            <th>S.N</th>
                            <th style="width:25%;">CLEARING MEMBER(CM)</th>
                            <th>CM NO.</th>
                            <th>ADDRESS</th>
                            <th>POOL ACCOUNT</th>
                            <th>POOL ACCOUNT DP</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $clearingMember=DB::table('clearing_member')->where('status',1)->where('deleted_at',null)->get(); ?>
                    @foreach($clearingMember as $clearingMembers)
                <tr>
                   
                  <td>{{ $clearingMembers->cm_id }}</td>
                  <td style="width:25%;">{{ $clearingMembers->name }}</td>
                  <td>{{ $clearingMembers->cm_no }}</td>
                  <td>{{ $clearingMembers->address }}</td>
                  <td>{{ $clearingMembers->pool_account }}</td>
                 <?php $dp = DB::table('clearing_member')->leftJoin('dp', 'clearing_member.pool_account_dp', '=', 'dp.dp_id')->where('cm_id',$clearingMembers->cm_id)->get(); ?>
                 @foreach($dp as $data)
                  <td>{{ $data->name }}</td>
                  @endforeach
                  
                        </tr>
                        @endforeach
                                            </tbody>
                </table>
              
            </div>
                    </div>
    </div>
</div>
</section>


<script type="text/javascript">
    $(document).ready(function(){
        $("#btn_search").click(function() {
            var company_name = $("#company_name").val();
            var script_name = $("#script").val();
            var date_from = $("#date_from").val();
            var date_to = $("#date_to").val();
            $.ajax({
                type: "POST",
                url: "http://itsutra.com.np/cdscnp/Site/SearchBookClosureInfo",
                data: {company_name:company_name,script_name:script_name,date_from:date_from,date_to:date_to},
                cache: false,
                success: function(response) {
                    //alert(response);
                    $("#listall").empty().html(response);
                }
            });
        });

        $('#datepicker-component').datepicker();
        $('#datepicker-component-1').datepicker();
    });
</script>
   <!-- Footer Start -->
  @include('Home.layouts.footer')