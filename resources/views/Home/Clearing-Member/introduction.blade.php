@include('Home.layouts.header')
@include('Home.layouts.nav')
<title>Introduction :: Clearing Member </title>
<!-- Project Details Start -->
<section id="solution" class="p-t-30 p-b-30">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="solution_tabs">
                <ul>
                    <li class="active"><a>Clearing Member</a>  <br/>   </li> 
                    <ul>
                                                        <li class="active">
                                                        <a href="{{url('cmintroduction')}}">Introduction</a>                                </li>
                                                        <li class="">
                                                        <a href="{{url('listofclearingmember')}}">List of Clearing Member</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('cmsettlementprocedure')}}">Settlement Procedure</a>
                                </li>
                                            </ul>
                                            </ul>
                </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div id="global-relation" class="our_team">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading" id="global-relation">
                                <div class="heading_border bg_red"></div>
                                <h2>Introduction</h2>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 project_details_text">
                           <p class="p_14" style="text-align: justify;"><p>The companies who wish to admit their shares and securities into the system should obtain electronic connectivity with CDSC to avail the services of a registrar and transfer agents (RTA). CDSC records and safe keeps the securities admitted by these companies. CDSC functions as record keeping office in respect of the securities admitted by these companies.&nbsp;<br />
<br />
CDSC has maintained a centralized database architecture with on-line connectivity with the RTA through VPN network. All the RTA&rsquo;s are connected to CDSC and are providing services to a number of companies across the country.</p>
</p>
                        </div>
                    </div>
                </div>
                <!-- Our Partners end -->
            </div>
        </div>
    </div>
</section>
<br>
<!-- Project Details End -->  
 <!-- Footer Start -->
  @include('Home.layouts.footer')