@include('Home.layouts.header')
<title>List of CM :: Clearing Member</title>

<!-- header end here -->
<section id="solution" class="p-t-60 p-b-40">
    <div class="container">
        <div id="book-closure" class="row">
            <div class="col-md-12 p-b-50" id="dp">
                <div class="heading_border bg_red"></div>
                <h2>Clearing Member List</h2>
                 <div class="row">
<div class="col-md-12">
<form action="Home.Clearing-Member.search" method="GET" role="search">
{{ csrf_field()}}
<div class="row">
<div class="col-md-5 col-sm-4">
<div class="single-query">
<input type="text" placeholder="ENTER CM Name" class="keyword-input" id="name" name="name">
<!-- <select class="chosen-select" id="dp_id">
<option value="">Select DPID</option>
<option value="10100">10100</option>
</select> -->
</div>
</div>

<div class="col-md-2">
<button class="btn-light button-black" id="btn_search_si">Search</button>
</div>
</div>
</div>
</form>
</div>
                
                                <div class="table-responsive" id="listall">
                    <table class="table table-responsive table-striped table-bordered" id="dynamic-table" style="width: 100%">
                        <thead>
                           <tr>
                            <th>S.N</th>
                            <th style="width:25%;">CLEARING MEMBER(CM)</th>
                            <th>CM NO.</th>
                            <th>ADDRESS</th>
                            <th>POOL ACCOUNT</th>
                            <th>POOL ACCOUNT DP</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($clearingMember as $clearingMembers)
                <tr>
                   
                  <td>{{ $clearingMembers->cm_id }}</td>
                  <td style="width:25%;">{{ $clearingMembers->name }}</td>
                  <td>{{ $clearingMembers->cm_no }}</td>
                  <td>{{ $clearingMembers->address }}</td>
                  <td>{{ $clearingMembers->pool_account }}</td>
                 <?php $dp = DB::table('clearing_member')->leftJoin('dp', 'clearing_member.pool_account_dp', '=', 'dp.dp_id')->where('cm_id',$clearingMembers->cm_id)->get(); ?>
                 @foreach($dp as $data)
                  <td>{{ $data->name }}</td>
                  @endforeach
                  
                        </tr>
                        @endforeach
                                            </tbody>
                </table>
                {{ $clearingMember->links() }}
            </div>
                    </div>
    </div>
</div>
</section>


<script type="text/javascript">
    $(document).ready(function(){
        $("#btn_search").click(function() {
            var company_name = $("#company_name").val();
            var script_name = $("#script").val();
            var date_from = $("#date_from").val();
            var date_to = $("#date_to").val();
            $.ajax({
                type: "POST",
                url: "http://itsutra.com.np/cdscnp/Site/SearchBookClosureInfo",
                data: {company_name:company_name,script_name:script_name,date_from:date_from,date_to:date_to},
                cache: false,
                success: function(response) {
                    //alert(response);
                    $("#listall").empty().html(response);
                }
            });
        });

        $('#datepicker-component').datepicker();
        $('#datepicker-component-1').datepicker();
    });
</script>
   <!-- Footer Start -->
  @include('Home.layouts.footer')