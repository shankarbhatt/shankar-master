@include('Home.layouts.header')
<title>RTA List :: RTA </title>
                
                <section id="solution" class="p-t-60 p-b-40">
    <div class="container">
        <div id="book-closure" class="row">
            <div class="col-md-12 p-b-50" id="dp">
                <div class="heading_border bg_red"></div>
                <h2>RTA List</h2>
                    <div class="row">
<div class="col-md-12">
<form action="rtalistsearch" method="GET" role="search">
{{ csrf_field()}}
<div class="row">
<div class="col-md-5 col-sm-4">
<div class="single-query">
<input type="text" placeholder="ENTER RTA Name" class="keyword-input" id="name" name="name">
<!-- <select class="chosen-select" id="dp_id">
<option value="">Select DPID</option>
<option value="10100">10100</option>
</select> -->
</div>
</div>

<div class="col-md-2">
<button class="btn-light button-black" id="btn_search_si">Search</button>
</div>
</div>
</div>
</form>
</div>
                                        <div class="table-responsive" id="listall">
                           <table class="table  table-striped table-bordered" id="dynamic-table" >
                            <thead>
                                <tr>
                                    <th>S.N</th>
                                    <th>RTA NAME</th>
                                    <th>ADDRESS</th>
                                    <th>PHONE NO</th>
                                    <th>EMAIL</th>
                                    <th>Total Company</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $name=$_GET['name'];?>
                            <?php $rta = DB::table("rta")->where('name', 'LIKE', '%' . $name . '%')->orderBy('name','ASC')->where('status',1)->get();?>

                             <?php $i =0;?>
                        @foreach($rta as $rtaDetails)
                        <tr>
                   
                  <td><?php echo ++$i; ?></td>
                  <td style="width:25%;">{{ $rtaDetails->name }}</td>
                  <td>{{ $rtaDetails->address }}</td>
                  <td>{{ $rtaDetails->phone }}</td>
                  <td>{{ $rtaDetails->email }}</td>
                  <td><a href="{{ url('Home.Issuer.company/'.$rtaDetails->rta_id ) }}"><span class="badge"> <?php $issuer= DB::table('issuer')->where(['rta_id'=> $rtaDetails->rta_id])->where('deleted_at',null)->get(); ?>{{ count($issuer) }}</span></a></td>
                        </tr>
                        @endforeach
                    </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript">
   $(document).ready(function(){
        $("#rta_name").keyup(function(){
            var rta_name = $("#rta_name").val();
            if(rta_name=="") {
                alert('Please Enter Search Keyword');
            } else {
                $.ajax({
                    type: "POST",
                    url: "http://itsutra.com.np/cdscnp/Rta/Searchrta",
                    data: {rta_name:rta_name},
                    cache: false,
                    success: function(data) {
                        $("#listall").empty().html(data);
                    }
                }); 
            }
        });

        $("#btn_search_si").click(function(){
            var rta_name = $("#rta_name").val();
            if(rta_name=="") {
                alert('Please Enter Search Keyword');
            } else {
                $.ajax({
                    type: "POST",
                    url: "http://itsutra.com.np/cdscnp/Site/Searchrta",
                    data: {rta_name:rta_name},
                    cache: false,
                    success: function(data) {
                        $("#listall").empty().html(data);
                    }
                }); 
            }
        });


   });
</script>  
 <!-- Footer Start -->
 @include('Home.layouts.footer')
