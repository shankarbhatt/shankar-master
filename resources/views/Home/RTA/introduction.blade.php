@include('Home.layouts.header')
<title>Introduction :: RTA </title>
<!-- Project Details Start -->
<section id="solution" class="p-t-30 p-b-30">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="solution_tabs">
            <ul>
                    <li class="active"><a href="#">RTA</a>  <br/>   </li> 
                    <ul>
                                                        <li class="active">
                                                        <a href="{{url('rtaintroduction')}}">Introduction</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('listofrta')}}">RTA List</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('rtaadmissionprocedure')}}">Admission Procedure</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('documentation')}}">Documentation for Cooperate Action</a>
                                </li>
                                            </ul>
                                            </ul> 
                </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div id="global-relation" class="our_team">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading" id="global-relation">
                                <div class="heading_border bg_red"></div>
                                <h2>Introduction</h2>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 project_details_text">
                           <p class="p_14" style="text-align: justify;"><p>The companies who wish to admit their shares and securities into the system should obtain electronic connectivity with CDSC to avail the services of a registrar and transfer agents (RTA). CDSC records and safe keeps the securities admitted by these companies. CDSC functions as record keeping office in respect of the securities admitted by these companies.&nbsp;<br />
<br />
CDSC has maintained a centralized database architecture with on-line connectivity with the RTA through VPN network. All the RTAs are connected to CDSC and are providing services to a number of companies across the country.</p>
</p>
                        </div>
                    </div>
                </div>
                <!-- Our Partners end -->
            </div>
        </div>
    </div>
</section>
<br>
<!-- Project Details End -->  
 <!-- Footer Start -->
 @include('Home.layouts.footer')
