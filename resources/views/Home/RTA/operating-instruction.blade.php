@include('Home.layouts.header')
@include('Home.layouts.nav')
<!-- Project Details Start -->
<section id="solution" class="p-t-30 p-b-30">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="solution_tabs">
                    <ul>
                                                        <li class="">
                                                        <a href="{{url('Home.RTA.introduction')}}">Introduction</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('Home.RTA.rtalist')}}">RTA LIST</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('Home.RTA.admission-procedure')}}">Admission Procedure</a>
                                </li>
                                                        <li class="active">
                                                        <a href="{{url('Home.RTA.operating-instruction')}}">Operating Instruction</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('Home.RTA.documentationforcooperativeauction')}}">Documentation for Cooperate Action</a>
                                </li>
                                            </ul>
                </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div id="global-relation" class="our_team">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading" id="global-relation">
                                <div class="heading_border bg_red"></div>
                                <h2>Operating Instruction</h2>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 project_details_text">
                           <p class="p_14" style="text-align: justify;"><p>Operating Instruction for RTA.</p>

<p>&nbsp;</p>
</p>
                        </div>
                    </div>
                </div>
                <!-- Our Partners end -->
            </div>
        </div>
    </div>
</section>
<br>
<!-- Project Details End -->  
 <!-- Footer Start -->
 @include('Home.layouts.footer')
