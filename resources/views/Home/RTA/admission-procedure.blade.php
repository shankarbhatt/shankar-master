@include('Home.layouts.header')
<title>Admission Procedure :: RTA</title>
<!-- Project Details Start -->
<section id="solution" class="p-t-30 p-b-30">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="solution_tabs">
                    <ul>
                    <li class="active"><a>RTA</a>  <br/>   </li> 
                    <ul>
                                                        <li class="">
                                                        <a href="{{url('rtaintroduction')}}">Introduction</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('listofrta')}}">RTA List</a>
                                </li>
                                                        <li class="active">
                                                        <a href="{{url('rtaadmissionprocedure')}}">Admission Procedure</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('documentation')}}">Documentation for Cooperate Action</a>
                                </li>
                                            </ul>
                                            </ul> 
                </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div id="global-relation" class="our_team">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading" id="global-relation">
                                <div class="heading_border bg_red"></div>
                                <h2>Admission Procedure</h2>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 project_details_text">
                           <p class="p_14" style="text-align: justify;"><p>The admission procedure for RTA is differentiated in two categories:&nbsp;&nbsp;</p>

<ul>
	<li>Issuer appointing any other RTA for doing the RTA related jobs.</li>
	<li>Issuer working as its own RTA.</li>
</ul>

<p>If the issuer is acting as its own RTA or has appointed any other company for doing the RTA related work then the RTA, Issuer Company and CDSC should do a joint agreement and get registered in CDSC as registered RTA. The issuer when registered in CDSC gets the ISIN</p>

<p style="text-align: justify;">(International Securities Identification Number) for the listed security type. After the ISIN is issued that ISIN is registered to the concerned RTA.</p>

<p> <a href="{{ url('/aggrement/3ISSUER, RTA and CDS agreement.pdf') }}" class="btn btn-inverse btn-mini" download="3ISSUER, RTA and CDS agreement.pdf" title="Download File">The agreement between CDSC and DP is as follows:</a></p></p>

</p>
                        </div>
                    </div>
                </div>
                <!-- Our Partners end -->
            </div>
        </div>
    </div>
</section>
<br>
<!-- Project Details End -->  
 <!-- Footer Start -->
 @include('Home.layouts.footer')
