@include('Home.layouts.header')
<title>Documentation :: RTA</title>
<!-- Project Details Start -->
<section id="solution" class="p-t-30 p-b-30">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
			<div class="solution_tabs">
			<ul>
                    <li class="active"><a>RTA</a>  <br/>   </li> 
                    <ul>
                                                        <li class="">
                                                        <a href="{{url('rtaintroduction')}}">Introduction</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('listofrta')}}">RTA List</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('rtaadmissionprocedure')}}">Admission Procedure</a>
                                </li>
                                                        <li class="active">
                                                        <a href="{{url('documentation')}}">Documentation for Cooperate Action</a>
                                </li>
                                            </ul>
                                            </ul> 
                </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div id="global-relation" class="our_team">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading" id="global-relation">
                                <div class="heading_border bg_red"></div>
                                <h2>Documentation for Cooperate Action</h2>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 project_details_text">
                           <p class="p_14" style="text-align: justify;"><h3><span style="font-family:Arial,Helvetica,sans-serif;"><strong>Types of Corporate Actions and CA Master types to be selected by RTA.</strong></span></h3>

<p style="text-align: justify;">There are various type of corporate action which the RTA has to setup according to the type of corporate benefit issued by the company. According to the record date setup at the time of corporate action the BO&rsquo;s eligible for the corporate benefit are listed in the holding report.</p>

<table class="table table-striped table-bordered table-responsive">
	<thead>
		<tr>
			<th scope="col">S.N</th>
			<th scope="col">Corprotation Actoin Type</th>
			<th scope="col">CA Master Type</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>1</td>
			<td>Bonus</td>
			<td>2</td>
		</tr>
		<tr>
			<td>2</td>
			<td>Rearragement</td>
			<td>1 &amp; 2</td>
		</tr>
		<tr>
			<td>3</td>
			<td>Rights</td>
			<td>4</td>
		</tr>
		<tr>
			<td>4</td>
			<td>Dividend</td>
			<td>5</td>
		</tr>
		<tr>
			<td>5</td>
			<td>Interim Dividend</td>
			<td>5</td>
		</tr>
		<tr>
			<td>6</td>
			<td>Interest Payment</td>
			<td>5</td>
		</tr>
		<tr>
			<td>7</td>
			<td>Call</td>
			<td>4</td>
		</tr>
		<tr>
			<td>8</td>
			<td>Merger</td>
			<td>1</td>
		</tr>
		<tr>
			<td>9.</td>
			<td>Conversation</td>
			<td>1</td>
		</tr>
		<tr>
			<td>10</td>
			<td>Split</td>
			<td>1</td>
		</tr>
		<tr>
			<td>11.</td>
			<td>Redmption</td>
			<td>3</td>
		</tr>
	</tbody>
</table>
</p>
                        </div>
                    </div>
                </div>
                <!-- Our Partners end -->
            </div>
        </div>
    </div>
</section>
<br>
<!-- Project Details End -->  
 <!-- Footer Start -->
 @include('Home.layouts.footer')
