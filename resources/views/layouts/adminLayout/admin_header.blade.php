<!--Header-part-->
<div id="header">
  <h1><a href="{{ url('/admin/dashboard') }}">Dashboard </a></h1>
</div>
<!--close-Header-part--> 

<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li class=""><a title="Visit Website" href="/" style="float: right;" target="_blank"><i class="icon icon-globe"></i> <span class="text"> Visit Website</span></a></li>
    <li class="dropdown" id="profile-messages" ><a title="Login Name" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text"> {{ Auth::user()->name }}</span><b class="caret"></b> </a>
      <ul class="dropdown-menu">
        <li><a title="Settings" href="{{ url('/admin/settings/') }}"><i class="icon icon-cog"></i> Settings</a></li>
        <li class="divider"></li>
        <li><a title="Log Out" href="{{ url('/logout')}}"><i class="icon-off"></i> Log Out</a></li>
      </ul>
    </li>
  </ul>
</div>