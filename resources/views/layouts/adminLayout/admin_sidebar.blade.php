<!--sidebar-menu-->

<div id="sidebar"><a href="" class="visible-phone"></a>
  <ul>
  @if (\Request::is('admin/dashboard'))
    <li class="active"><a href="{{ url('/admin/dashboard') }}"><i class="icon-dashboard"></i> <span>Dashboard</span></a> </li>
    @else 
    <li class=""><a href="{{ url('/admin/dashboard') }}"><i class="icon-dashboard"></i> <span>Dashboard</span></a> </li>
    @endif

    @if (\Request::is('admin/add-menu') || \Request::is('admin/view-menu'))
    <li class="submenu active"> <a href=""><i class="icon icon-th-large"></i> <span>Menu</span> </a>
      <ul>
        <li><a href="{{ url('/admin/add-menu') }}">Add Menu</a></li>
        <li><a href="{{ url('/admin/view-menu') }}">View All Menu</a></li>
      </ul>
    </li>
    @else
    <li class="submenu"> <a href=""><i class="icon icon-th-large"></i> <span>Menu</span> </a>
      <ul>
        <li id="new1"><a href="{{ url('/admin/add-menu') }}">Add Menu</a></li>
        <li id="new2"><a href="{{ url('/admin/view-menu') }}">View All Menu</a></li>
      </ul>
    </li>
    @endif

    @if (\Request::is('admin/add-submenu') || \Request::is('admin/view-all-submenu'))
    <li class="submenu active"> <a href=""><i class="icon icon-align-justify"></i> <span>Submenu</span> </a>
      <ul>
        <li><a href="{{ url('/admin/add-submenu') }}">Add Submenu</a></li>
        <li><a href="{{ url('/admin/view-all-submenu') }}">View All Submenu</a></li>
      </ul>
    </li>
    @else <li class="submenu"> <a href=""><i class="icon icon-align-justify"></i> <span>Submenu</span> </a>
      <ul>
        <li><a href="{{ url('/admin/add-submenu') }}">Add Submenu</a></li>
        <li><a href="{{ url('/admin/view-all-submenu') }}">View All Submenu</a></li>
      </ul>
    </li>
    @endif

    @if (\Request::is('admin/add-page') || \Request::is('admin/view-page'))
    <li class="submenu active" id="page"><a href=""><i class="icon icon-copy"></i> <span>Pages</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-page') }}">Add Page</a></li>
        <li><a href="{{ url('/admin/view-page') }}">View All Pages</a></li>
      </ul>
      </li>
      @else
      <li class="submenu" id="page"><a href=""><i class="icon icon-copy"></i> <span>Pages</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-page') }}">Add Page</a></li>
        <li><a href="{{ url('/admin/view-page') }}">View All Pages</a></li>
      </ul>
      </li>
      @endif

    @if (\Request::is('admin/add-death-notice-user') || \Request::is('admin/view-death-notice-user'))
    <li class="submenu active"><a href=""><i class="icon icon-user"></i> <span>Death Notice (User)</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-death-notice-user') }}">Add Death Notice (User)</a></li>
        <li><a href="{{ url('/admin/view-death-notice-user') }}">View All Death Notice (User)</a></li>
      </ul>
      </li>
      @else
      <li class="submenu"><a href=""><i class="icon icon-user"></i> <span>Death Notice (User)</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-death-notice-user') }}">Add Death Notice (User)</a></li>
        <li><a href="{{ url('/admin/view-death-notice-user') }}">View Death Notice (User)</a></li>
      </ul>
      </li>
      @endif
      
      <!-- @if (\Request::is('admin/add-user') || \Request::is('admin/view-user'))
    <li class="submenu active"><a href=""><i class="icon icon-group"></i> <span>User Management</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-user') }}">Add User</a></li>
        <li><a href="{{ url('/admin/view-user') }}">View All Users</a></li>
      </ul>
      </li>
      @else
      <li class="submenu"><a href=""><i class="icon icon-group"></i> <span>User Management</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-user') }}">Add Users</a></li>
        <li><a href="{{ url('/admin/view-user') }}">View All Users</a></li>
      </ul>
      </li>
    @endif-->

      @if (\Request::is('admin/add-daily-update') || \Request::is('admin/view-daily-update') || \Request::is('admin/edit-daily-update/{id}'))
      <li class="submenu active"><a href=""><i class="icon icon-info-sign "></i> <span>Daily Update</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-daily-update') }}">Add Daily Update</a></li>
        <li><a href="{{ url('/admin/view-daily-update') }}">View All Daily Updates</a></li>
      </ul>
      </li>
      @else
      <li class="submenu"><a href=""><i class="icon icon-info-sign "></i> <span>Daily Update</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-daily-update') }}">Add Daily Update</a></li>
        <li><a href="{{ url('/admin/view-daily-update') }}">View All Daily Updates</a></li>
      </ul>
      </li>
      @endif

      @if (\Request::is('admin/add-casba-bank') || \Request::is('admin/view-casba-bank'))
      <li class="submenu active" id="bank"><a href=""><i class="icon icon-building"></i>C-ASBA Bank<span></span></a>
    <ul>
        <li><a href="{{ url('/admin/add-casba-bank') }}">Add C-ASBA Bank</a></li>
        <li><a href="{{ url('/admin/view-casba-bank') }}">View All C-ASBA Banks</a></li>
      </ul>
      </li>
      @else
      <li class="submenu" id="bank"><a href=""><i class="icon icon-building"></i>C-ASBA Bank<span></span></a>
    <ul>
        <li><a href="{{ url('/admin/add-casba-bank') }}">Add C-ASBA Bank</a></li>
        <li><a href="{{ url('/admin/view-casba-bank') }}">View All C-ASBA Banks</a></li>
      </ul>
      </li>
      @endif

      @if (\Request::is('admin/add-dp') || \Request::is('admin/view-dp'))
    <li class="submenu active"> <a href=""><i class="icon icon-desktop"></i> <span>Depository Participants</span> </a>
      <ul>
        <li><a href="{{ url('/admin/add-dp') }}">Add DP</a></li>
        <li><a href="{{ url('/admin/view-dp') }}">View All DP</a></li>
      </ul>
    </li>
    @else
    <li class="submenu"> <a href=""><i class="icon icon-desktop"></i> <span>Depository Participants</span> </a>
      <ul>
        <li><a href="{{ url('/admin/add-dp') }}">Add DP</a></li>
        <li><a href="{{ url('/admin/view-dp') }}">View All DP</a></li>
      </ul>
    </li>
    @endif

    @if (\Request::is('admin/add-cm') || \Request::is('admin/view-cm'))
    <li class="submenu active"><a href=""><i class="icon icon-tasks"></i> <span>Clearing Member</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-cm') }}">Add Clearing Member</a></li>
        <li><a href="{{ url('/admin/view-cm') }}">View All Clearing Members</a></li>        
      </ul>
      </li>
      @else
      <li class="submenu"><a href=""><i class="icon icon-tasks"></i> <span>Clearing Member</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-cm') }}">Add Clearing Member</a></li>
        <li><a href="{{ url('/admin/view-cm') }}">View All Clearing Members</a></li>        
      </ul>
      </li>
      @endif

      @if (\Request::is('admin/add-rta') || \Request::is('admin/view-rta'))
      <li class="submenu active"><a href=""><i class="icon icon-tablet"></i> <span>RTA</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-rta') }}">Add RTA</a></li>
        <li><a href="{{ url('/admin/view-rta') }}">View All RTA</a></li>
      </ul>
      </li>
      @else
      <li class="submenu"><a href=""><i class="icon icon-tablet"></i> <span>RTA</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-rta') }}">Add RTA</a></li>
        <li><a href="{{ url('/admin/view-rta') }}">View All RTA</a></li>
      </ul>
      </li>
      @endif

      @if (\Request::is('admin/add-issuer') || \Request::is('admin/view-all-issuer'))
       <li class="submenu active"><a href=""><i class="icon icon-bookmark"></i> <span>Issuer</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-issuer') }}">Add Issuer</a></li>
        <li><a href="{{ url('/admin/view-all-issuer') }}">View All Issuer</a></li>
      </ul>
      </li>
      @else
      <li class="submenu"><a href=""><i class="icon icon-bookmark"></i> <span>Issuer</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-issuer') }}">Add Issuer</a></li>
        <li><a href="{{ url('/admin/view-all-issuer') }}">View All Issuer</a></li>
      </ul>
      </li>
      @endif

      @if (\Request::is('admin/add-isin') || \Request::is('admin/view-isin'))
      <li class="submenu active"><a href=""><i class="icon icon-barcode"></i> <span>ISIN & Script</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-isin') }}">Add ISIN & Scipt</a></li>
        <li><a href="{{ url('/admin/view-isin') }}">View All ISIN & Script</a></li>
      </ul>
      </li>
      @else
      <li class="submenu"><a href=""><i class="icon icon-barcode"></i> <span>ISIN & Script</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-isin') }}">Add ISIN & Scipt</a></li>
        <li><a href="{{ url('/admin/view-isin') }}">View All ISIN & Script</a></li>
      </ul>
      </li>
      @endif

      @if (\Request::is('admin/add-cdsc-tariff') || \Request::is('admin/view-cdsc-tariff'))
      <li class="submenu active"> <a href=""><i class="icon icon-hdd"></i> <span>CDSC Tariff</span> </a>
      <ul>
        <li><a href="{{ url('/admin/add-cdsc-tariff') }}">Add CDSC Traiff</a></li>
        <li><a href="{{ url('/admin/view-cdsc-tariff') }}">View All CDSC Traiff</a></li>
      </ul>
    </li>
    @else
    <li class="submenu"> <a href=""><i class="icon icon-hdd"></i> <span>CDSC Tariff</span> </a>
      <ul>
        <li><a href="{{ url('/admin/add-cdsc-tariff') }}">Add CDSC Traiff</a></li>
        <li><a href="{{ url('/admin/view-cdsc-tariff') }}">View All CDSC Traiff</a></li>
      </ul>
    </li>
    @endif  
    
  <!--  @if (\Request::is('admin/add-book-closure') || \Request::is('admin/view-book-closure'))
    <li class="submenu active"> <a href=""><i class="icon icon-book"></i> <span>Book Closure</span> </a>
      <ul>
        <li><a href="{{ url('/admin/add-book-closure') }}">Add Book Closure</a></li>
        <li><a href="{{ url('/admin/view-book-closure') }}">View All Book Closure</a></li>
      </ul>
    </li>
    @else
    <li class="submenu"> <a href=""><i class="icon icon-book"></i> <span>Book Closure</span> </a>
      <ul>
        <li><a href="{{ url('/admin/add-book-closure') }}">Add Book Closure</a></li>
        <li><a href="{{ url('/admin/view-book-closure') }}">View All Book Closure</a></li>
      </ul>
    </li> 
    @endif -->
    
    @if (\Request::is('admin/add-settlement') || \Request::is('admin/view-settlement'))
      <li class="submenu active"> <a href=""><i class="icon icon-reorder"></i> <span>Settlement</span> </a>
      <ul>
        <li><a href="{{ url('/admin/add-settlement') }}">Add Settlement</a></li>
        <li><a href="{{ url('/admin/view-settlement') }}">View All Settlement</a></li>
      </ul>
    </li>
    @else
    <li class="submenu"> <a href=""><i class="icon icon-reorder"></i> <span>Settlement</span> </a>
      <ul>
        <li><a href="{{ url('/admin/add-settlement') }}">Add Settlement</a></li>
        <li><a href="{{ url('/admin/view-settlement') }}">View All Settlement</a></li>
      </ul>
    </li>
    @endif

      @if (\Request::is('admin/add-downloads') || \Request::is('admin/view-downloads'))
    <li class="submenu active"><a href=""><i class="icon icon-suitcase"></i> <span>Downloads</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-downloads') }}">Add Downloads</a></li>
        <li><a href="{{ url('/admin/view-downloads') }}">View All Downloads</a></li>
      </ul>
      </li>
      @else
      <li class="submenu"><a href=""><i class="icon icon-download"></i> <span>Downloads</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-downloads') }}">Add Downloads</a></li>
        <li><a href="{{ url('/admin/view-downloads') }}">View All Downloads</a></li>
      </ul>
      </li>
      @endif

        @if (\Request::is('admin/add-news-notice') || \Request::is('admin/view-news-notice'))
    <li class="submenu active"><a href=""><i class="icon icon-bar-chart"></i> <span>News & Notice</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-news-notice') }}">Add News & Notice</a></li>
        <li><a href="{{ url('/admin/view-news-notice') }}">View All News & Notice</a></li>
      </ul>
      </li>
      @else
      <li class="submenu"><a href=""><i class="icon icon-bar-chart"></i> <span>News & Notice</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-news-notice') }}">Add News & Notice</a></li>
        <li><a href="{{ url('/admin/view-news-notice') }}">View All News & Notice</a></li>
      </ul>
      </li>
      @endif
      
      @if (\Request::is('admin/add-press-release') || \Request::is('admin/view-press-release'))
      <li class="submenu active"><a href=""><i class="icon icon-th"></i> <span>Press Release</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-press-release') }}">Add Press Release</a></li>
        <li><a href="{{ url('/admin/view-press-release') }}">View All Press Releases</a></li>
      </ul>
      </li>
      @else
      <li class="submenu"><a href=""><i class="icon icon-th"></i> <span>Press Release</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-press-release') }}">Add Press Release</a></li>
        <li><a href="{{ url('/admin/view-press-release') }}">View All Press Releases</a></li>
      </ul>
      </li>
      @endif

      @if (\Request::is('admin/add-image') || \Request::is('admin/view-image'))
      <li class="submenu active"><a href=""><i class="icon icon-picture"></i> <span>Slider Images</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-image') }}">Add Image</a></li>
        <li><a href="{{ url('/admin/view-image') }}">View All Images</a></li>
      </ul>
      </li>
      @else
      <li class="submenu"><a href=""><i class="icon icon-picture"></i> <span>Slider Images</span></a>
    <ul>
        <li><a href="{{ url('/admin/add-image') }}">Add Image</a></li>
        <li><a href="{{ url('/admin/view-image') }}">View All Images</a></li>
      </ul>
      </li>
      @endif
  </ul>
</div>
<!--sidebar-menu-->