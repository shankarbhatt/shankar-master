@include('layouts.frontLayout.front_header')
<body class="finance">
    <!-- Header Bottom Start -->
    <section id="header-top" class="bg_blue">
        <div class="bg-white row"><div class="container">
        <div class="col-md-3 col-sm-3 col-xs-12">
            <a href="index"><img src="{{asset('images/frontend_images/logo.png')}}" alt="logo" /></a>
        </div>
        <!-- <div class="col-md-9 col-sm-9 col-xs-12">
            <a style="background-color: #989898;margin-left: 15px;padding: 8px 20px;border-radius: 5px;float: right;" href="https://meroshare.cdsc.com.np/" target="_blank"><img src="assets/images/logo-big.png"></a> -->
            <div class="cont_office">
                <div class="office_menu">
                <a  href="https://meroshare.cdsc.com.np/" class="btn btn-light button-red" target="_blank">Mero Share </a>
                    <a href="http://127.0.0.1:8000/admin" class="btn btn-light button-black" >Login</a>
                </div>
                     </div>
                     <div class="header-top-links">
                        <div class="social-icons text-right">
                            <ul>
                                <li><a href="https://www.facebook.com/CDSCnp"><i class="fa fa-facebook"></i></a></li>

                            </ul>
                        </div>
                    </div>
                </section>
                <!-- Header Bottom End -->
                <nav class="navbar navbar-default navbar-sticky light-bg-nav bootsnav">
                    <div class="container">
                        <!-- Start Header Navigation -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"><i class="icon-reorder"></i></button>
                        </div>
                        <!-- End Header Navigation -->
                        <div class="collapse navbar-collapse  nav_bor_top" id="navbar-menu">
                            <ul class="nav navbar-nav navbar-left" data-in="fadeInDown" data-out="fadeOutUp">
                             <li class="" id="home"><a href="{{ url('Home.index') }}">Home</a></li>
                                                                                             <li class="dropdown" id="h">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">About Us</a>
                                                                        <ul class="dropdown-menu">
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.About-Us.introduction')}}">Introduction</a>
                                            

                                        </li>
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.About-Us.board-of-directory')}}">Board of Directors</a>
                                            

                                        </li>
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.About-Us.management-team')}}">Management Team</a>
                                            

                                        </li>
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.About-Us.global-partner')}}">Global Partners</a>
                                            

                                        </li>
                                                                            </ul>
                                                                    </li>
                                                                                                <li class="dropdown" id="h">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">Investor</a>
                                                                        <ul class="dropdown-menu">
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.Investor.fqa')}}">FAQ</a>
                                            

                                        </li>
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.Investor.isinscript')}}" >ISIN & SCRIPT</a>
                                            

                                        </li>
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.Investor.book-closure')}}">Book Closure Info</a>
                                            

                                        </li>
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.Investor.settlementid')}}">Settlement ID</a>
                                            

                                        </li>
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.Investor.registered-company')}}" link = "Investor">Registered Company</a>
                                            

                                        </li>
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.Investor.dos-dont')}}">Dos & Don'ts</a>
                                            

                                        </li>
                                                                            </ul>
                                                                    </li>
                                                                                                <li class="dropdown" id="h">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">Depository Participants</a>
                                                                        <ul class="dropdown-menu">
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.Depository-Participants.introduction')}}">Introduction</a>
                                            

                                        </li>
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.Depository-Participants.numberofdp')}}" link = "Depository-Participants">Number of DP</a>
                                            

                                        </li>
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.Depository-Participants.cdsc-tariff')}}">CDSC Tariff</a>
                                            

                                        </li>
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.Depository-Participants.services')}}" >Services</a>
                                            

                                        </li>
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.Depository-Participants.admission-procedure')}}">Admission Procedure</a>
                                            

                                        </li>
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.Depository-Participants.hardwaresoftware')}}">Hardware & Software Requirement</a>
                                            

                                        </li>
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.Depository-Participants.operating')}}">Operating Instruction</a>
                                            

                                        </li>
                                                                            </ul>
                                                                    </li>
                                                                                                <li class="dropdown" id="h">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">RTA</a>
                                                                        <ul class="dropdown-menu">
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.RTA.introduction')}}">Introduction</a>
                                            

                                        </li>
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.RTA.rtalist')}}">RTA LIST</a>
                                            

                                        </li>
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.RTA.admission-procedure')}}">Admission Procedure</a>
                                            

                                        </li>
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.RTA.operating-instruction')}}">Operating Instruction</a>
                                            

                                        </li>
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.RTA.documentationforcooperativeauction')}}">Documentation for Cooperate Action</a>
                                            

                                        </li>
                                                                            </ul>
                                                                    </li>
                                                                                                <li class="dropdown" id="h">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">Clearing Member</a>
                                                                        <ul class="dropdown-menu">
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.Clearing-Member.introduction')}}">Introduction</a>
                                            

                                        </li>
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.Clearing-Member.clearingmember')}}">List of Clearing Member</a>
                                            

                                        </li>
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.Clearing-Member.settlement-procedure')}}">Settlement Procedure</a>
                                            

                                        </li>
                                                                            </ul>
                                                                    </li>
                                                                                                <li class="dropdown" id="h">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">Issuer</a>
                                                                        <ul class="dropdown-menu">
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.Issuer.introduction')}}">Introduction</a>
                                            

                                        </li>
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.Issuer.benefits')}}">Benefits</a>
                                            

                                        </li>
                                                                                                                        <li class=" cool-link_3" id="submenu">
                                                                                        <a href="{{url('Home.Issuer.admission-procedure')}}" >Admission Procedure</a>
                                            

                                        </li>
                                                                            </ul>
                                                                    </li>
                                                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Publication</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{url('Home.Publication.circulars')}}">Circulars</a></li>
                                        <li><a href="{{url('Home.Publication.bylaws')}}">By Laws</a></li>
                                    </ul>
                                </li>
                                <li><a href="{{url('Home.download')}}">Downloads</a></li>
                                <li><a href="{{url('Home.contact-us')}}">Contact Us</a></li>
                            </ul>
                        </div>


                    </div>
                </nav>
                <br><br>
                <script>
                    $(function(){
                        $('a').each(function(){
                            if ($(this).prop('href') == window.location.href) {
                                $(this).addClass('active'); $(this).parents('li').addClass('active');
                            } else {
                                $(this).removeClass('active');
                            }
                        });
                    });
                </script>                 