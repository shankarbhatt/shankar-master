 <!-- Footer Start -->
 <footer id="footer_1" class="bg_blue p-t-100">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="footer_box_1">
                    <img src="{{asset('images/frontend_images/logo-inverted.png')}}" alt="image" class="img-responsive">
                    <p class="p-t-30 p-b-30 footer_p" style="text-align: justify;">CDS and Clearing Limited, a company established under the company act is a company promoted by Nepal Stock Exchange Limited (NEPSE) in 2010 to provide centralized depository, clearing and settlement services in Nepal. The company is inaugurated on 31st March 2011.  </p><br>
                        <!-- <div class="">
                            <ul>
                              <div class="brochure">
                                
                                <a href="http://itsutra.com.np/cdscnp/Downloads"><i class="fa fa-file" aria-hidden="true"></i> Downloads</a> 
                                </div>
                            </ul>
                        </div> -->
                    </div>
                </div>

               <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="footer_box_1">
                        <h3>Quick Links</h3>
                        <ul class="footer_link_1">
                            
                                    <li class="col-md-6 col-xs-12">
                                                                                <a href="{{url('Home.About-Us.introduction')}}">About Us</a>
                                        
                                    </li>
                                    
                                    <li class="col-md-6 col-xs-12">
                                                                                <a href="{{url('Home.Investor.fqa')}}">Investor</a>
                                        
                                    </li>
                                    
                                    <li class="col-md-6 col-xs-12">
                                                                                <a href="{{url('Home.Depository-Participants.introduction')}}l">Depository Participants</a>
                                        
                                    </li>
                                    
                                    <li class="col-md-6 col-xs-12">
                                                                                <a href="{{url('Home.RTA.introduction')}}">RTA</a>
                                        
                                    </li>
                                    
                                    <li class="col-md-6 col-xs-12">
                                                                                <a href="{{url('Home.Clearing-Member.introduction')}}">Clearing Member</a>
                                        
                                    </li>
                                    
                                    <li class="col-md-6 col-xs-12">
                                                                                <a href="{{url('Home.Issuer.introduction')}}">Issuer</a>
                                        
                                    </li>
                                    
                                    <li class="col-md-6 col-xs-12">
                                                                                <a href="{{url('Home.About-Us.board-of-directory')}}">Board of Directors</a>
                                        
                                    </li>
                                    
                                    <li class="col-md-6 col-xs-12">
                                                                                <a href="{{url('Home.Depository-Participants.cdsc-tariff')}}">CDSC Tariff</a>
                                        
                                    </li>
                                    
                                    <li class="col-md-6 col-xs-12">
                                                                                <a href="{{url('Home.About-Us.management-team')}}">Management Team</a>
                                        
                                    </li>
                                    
                                    <li class="col-md-6 col-xs-12">
                                                                                <a href="{{url('Home.About-Us.global-partner')}}">Global Partners</a>
                                        
                                    </li>
                                     
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="footer_box_1">
                                <h3>Get in Touch</h3>
                                <div class="footer_1_contact">
                                    <p><i class="fa fa-phone"></i> +977 01-4238008, 4216068, </p>
                                    <p><i class="fa fa-envelope"></i>info@cdsc.com.np</p>
                                    <p><i class="fa fa-map-marker"></i>Share Markets Commercial Complex Putalisadak,Kathmandu, Nepal</p>
                                 
                                    <p><i class="fa fa-clock-o" aria-hidden="true""></i> 10:00 AM - 5:00PM(Sunday to Friday)</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer_botom m-t-40 p-t-20 p-b-20">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <p>Copyrights © 2018 CDSC. All rights reserved.</p>
                            </div>
                            <div class="col-md-6 text-right">
                                <p>Designed & Developed by  <a href="http://shankarbhatt.com.np/" target="_blank"><b>Shankar & </b></a><a href="http://prembasnet.com.np/" target="_blank"><b>Prem </b></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- Footer End -->

            <!-- Back-To-Top -->
            <div class="container">
                <a href="#" class="back-to-top text-center" style="display: inline;">
                    <i class="fa fa-angle-up"></i>
                </a>
            </div>

            <!-- Team Pop Up End -->
            <!-- Team Pop Up End -->

            <!--Required JS -->
            <script src="{{asset('js/frontend_js/jquery.2.2.3.min.js')}}"></script>
            <script src="{{asset('js/frontend_js/bootstrap.min.js')}}"></script>

            
            <!-- SMOOTH SCROLL -->  
            <script type="text/javascript" src="{{asset('js/frontend_js/scroll-desktop.js')}}"></script>
            <script type="text/javascript" src="{{asset('js/frontend_js/scroll-desktop-smooth.js')}}"></script>

            <script src="{{asset('banner-slider/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
            <script src="{{asset('js/frontend_js/owl.carousel.min.js')}}"></script>
            <script src="{{asset('js/frontend_js/bootsnav.js')}}"></script>
            <script src="{{asset('js/frontend_js/wow.min.js')}}"></script>
            <script src="{{asset('js/frontend_js/jquery.parallax-1.1.3.js')}}"></script>
            <script src="{{asset('js/frontend_js/bootstrap-number-input.js')}}"></script>
            <script src="{{asset('js/frontend_js/renge_selectbox-0.2.min.js')}}"></script>
            <script src="{{asset('js/frontend_js/range-Slider.min.js')}}"></script>
            <script src="{{asset('js/frontend_js/jquery.counterup.js')}}"></script>
            <script src="{{asset('js/frontend_js/zelect.js')}}"></script>
            
            <!-- Progress  -->
            <script src="{{asset('js/frontend_js/progressbar.js')}}"></script>
            <script src="{{asset('js/frontend_js/copious_custom.js')}}"></script>
            <script src="{{asset('datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
            <script src="{{asset('js/chosen.jquery.min.js')}}"></script>


        <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
            <script src="{{asset('js/jquery.dataTables.bootstrap.min.js')}}"></script> 
            <script type="text/javascript">
                /* Smooth-Scroll */

                $('.chosen-select').chosen();
                
    //$("#dynamic-table").DataTable();
    $('#dynamic-table').dataTable( {
        searching: false,
        order: [],
        "lengthMenu": [ [20, 25, 50, -1], [20, 25, 50, "All"] ],
        "sScrollX": "100%"  
    });

    
    $(".scroll").click(function(event) {

        event.preventDefault();
        $('html,body').animate({
            scrollTop: $(this.hash).offset().top
        }, 2000);
    });
    //Check to see if the window is top if not then display button
    var offset = 650;
    var duration = 300;
    $(window).scroll(function() {
        if ($(this).scrollTop() > offset) {
            $('.back-to-top').fadeIn(200);
        } else {
            $('.back-to-top').fadeOut(200);
        }
    });

    $('.back-to-top').on("click", function(event) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 500);
        return false;
    });

    $("table").addClass('table table-bordered');
    // function search_func(value)
    // {
    //     $.ajax({
    //        type: "GET",
    //        url: "http://itsutra.com.np/cdscnp/Home/checkSubscribers",
    //        data: {'search_keyword' : value},
    //        dataType: "text",
    //        type:'POST',
    //        success: function(msg){
    //          if(msg==="true"){
    //             $("#emailexist").text('You have already Subscribed');
    //             $("#subForm").submit(function(e){
    //                 e.preventDefault();
    //             });

    //         }else{
    //             // $("#emailexist").hide();
    //             $("#emailexist").text('');
    //             $("#subForm").unbind('submit');
    //         }
    //     }
    // });
    // }
</script>


</body>

</html>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAOBKD6V47-g_3opmidcmFapb3kSNAR70U&amp;libraries=places"></script>
<script src="{{ asset('assets/js/custom-map.js') }}"></script>
<script type="text/javascript">
    function showUrlInDialog(teamID){
        var tag = $("#team");
        // var teamID = $(this).data("id"); 
        // console.log(id);
        // $(".team_id").on('click', function(){
        //  var id = $(this).id();
        //  console.log(id);
        // });
        $.ajax({
            url: 'http://itsutra.com.np/cdscnp/Home/getTeamByID',
            data:{teamID:teamID},
            type:'POST',
            dataType:'json',
            success: function(data) {
                var image = JSON.parse(JSON.stringify(data));
                var filename = image[0].ImageFile;
                var title = image[0].Title;
                var tagline = image[0].TagLine;
                var imagename = "http://itsutra.com.np/cdscnp/uploads/files/" + filename;
                $(".popup_image img").attr('src', imagename);
                $(".pop_text h3").text(title);
                $(".popup_p_text").text(tagline);
                // console.log(imagename);
            }
        });
    }
</script>