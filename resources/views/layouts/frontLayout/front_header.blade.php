<!DOCTYPE HTML>
<html dir="ltr" lang="en">


<!-- Mirrored from itsutra.com.np/cdscnp/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 10 Jun 2018 01:27:20 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <title>CDS Clearing::Home</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="">
    <meta name="description" content="">

    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">

    <!-- Required Framework -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/frontend_css/bootstrap.min.css')}}" />
    <link rel="shortcut icon" href="{{asset('images/frontend_images/logo-icon.png')}}">

    <link href="{{asset('banner-slider/css/full-slider.css')}}" rel="stylesheet">

    <!-- Required Framework -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/frontend_css/owl.carousel.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/frontend_css/owl.transitions.css')}}" />
    <link rel="stylesheet" type="text/css" href="((asset('css/frontend_css/settings.css')}}" />

    <!-- Fonts Icons-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/frontend_css/font-awesome.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/frontend_css/copious-icon.css')}}" />
    <link href="{{ asset('fonts/backend_fonts/css/font-awesome.css') }}" rel="stylesheet" />
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>

    <!-- Navbar Css -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/frontend_css/bootsnav.css')}}" />

    <link href="{{asset('datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css" media="screen">

    <!-- Custom Css -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/frontend_css/chosen.min.css')}}" />

    <link rel="stylesheet" type="text/css" href="{{asset('css/frontend_css/datatable.css')}}" />

    <link rel="stylesheet" type="text/css" href="{{asset('css/frontend_css/color.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/frontend_css/tiles.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/frontend_css/style.css')}}" />
    <script src="{{asset('js/frontend_js/jquery.2.2.3.min.js')}}"></script>
</head>