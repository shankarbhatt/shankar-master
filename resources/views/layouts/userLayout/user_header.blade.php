<!--Header-part-->
<div id="header">
  <h1><a href="{{ url('/admin/dashboard') }}">Dashboard </a></h1>
</div>
<!--close-Header-part--> 

<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
     <?php $userID = Session::get('idSession'); $email = Session::get('userSession'); ?>

    <li class="dropdown" id="profile-messages" ><a title="Login Name" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text"><?php echo $email.' ('.$userID.')'?></span><b class="caret"></b> </a>
      <ul class="dropdown-menu">
        <li><a title="Settings" href="{{ url('/user/settings/') }}"><i class="icon icon-cog"></i> Settings</a></li>
        <li class="divider"></li>
        <li><a title="Log Out" href="{{ url('/user/logout')}}"><i class="icon-off"></i> Log Out</a></li>
      </ul>
    </li>
     
   <!-- <li class="dropdown" id="menu-messages"><a href="#" data-toggle="dropdown" data-target="#menu-messages" class="dropdown-toggle"><i class="icon icon-envelope"></i> <span class="text">Messages</span> <span class="label label-important">5</span> <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a class="sAdd" title="" href="#"><i class="icon-plus"></i> new message</a></li>
        <li class="divider"></li>
        <li><a class="sInbox" title="" href="#"><i class="icon-envelope"></i> inbox</a></li>
        <li class="divider"></li>
        <li><a class="sOutbox" title="" href="#"><i class="icon-arrow-up"></i> outbox</a></li>
        <li class="divider"></li>
        <li><a class="sTrash" title="" href="#"><i class="icon-trash"></i> trash</a></li>
      </ul>
    </li> -->
    
  </ul>
</div>

<!--close-top-Header-menu-->
<!--start-top-serch-->
<!--<div id="search">
  <input type="text" placeholder="Search here..."/>
  <button type="submit" class="tip-bottom" title="Search"><i class="icon-search icon-white"></i></button>
</div>-->
<!--close-top-serch-->