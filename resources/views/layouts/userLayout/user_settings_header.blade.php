<!--Header-part-->
<div id="header">
  <h1><a href="{{ url('/admin/dashboard') }}">Dashboard </a></h1>
</div>
<!--close-Header-part--> 

<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
      <?php $userID = Session::get('idSession'); $email = Session::get('userSession'); ?>
     
    <li class="dropdown" id="profile-messages" ><a title="Login Name" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text"><?php echo $email.' ('.$userID.')'?></span><b class="caret"></b> </a>
      <ul class="dropdown-menu">
        <li><a title="Log Out" href="{{ url('/user/logout')}}"><i class="icon-off"></i> Log Out</a></li>
      </ul>
    </li>

  </ul>
</div>
