<!--sidebar-menu-->

<div id="sidebar"><a href="" class="visible-phone"></a>
  <ul>
    <?php $key = Session::get('loginSession'); ?>
      <?php  if($key == 1) { ?>
        
      @if (\Request::is('user/dashboard'))
    <li class="active"><a href="{{ url('/user/dashboard') }}"><i class="icon-dashboard"></i> <span>Dashboard</span></a> </li>
    @else 
    <li class=""><a href="{{ url('/user/dashboard') }}"><i class="icon-dashboard"></i> <span>Dashboard</span></a> </li>
    @endif

    @if (\Request::is('death-notice/view-all-notice') || \Request::is('death-notice/view-all-pending-notice') || \Request::is('death-notice/view-all-logs') || \Request::is('death-notice/view-all-active-notice'))
    <li class="submenu active"> <a href=""><i class="icon icon-paste"></i> <span>Death Notice</span> </a>
      <ul>
        <li><a href="{{ url('/death-notice/view-all-pending-notice') }}">Pending Death Notice</a></li>
        <li><a href="{{ url('/death-notice/view-all-active-notice') }}">Active Death Notice</a></li>
        <li><a href="{{ url('/death-notice/view-all-notice') }}">Death Notices</a></li>
        <li><a href="{{ url('/death-notice/view-all-logs') }}">Death Notice Logs</a></li>
      </ul>
    </li>
     @else
    <li class="submenu"> <a href=""><i class="icon icon-paste"></i> <span>Death Notice</span> </a>
      <ul>
        <li id="new2"><a href="{{ url('/death-notice/view-all-pending-notice') }}">Pending Death Notice</a></li>
        <li id="new2"><a href="{{ url('/death-notice/view-all-active-notice') }}">Active Death Notice</a></li>
        <li id="new2"><a href="{{ url('/death-notice/view-all-notice') }}">Death Notices</a></li>
        <li id="new2"><a href="{{ url('/death-notice/view-all-logs') }}">Death Notice Logs</a></li>
      </ul>
    </li>
    @endif

 <?php } else {?>

   @if (\Request::is('user/dashboard'))
    <li class="active"><a href="{{ url('/user/dashboard') }}"><i class="icon-dashboard"></i> <span>Dashboard</span></a> </li>
    @else 
    <li class=""><a href="{{ url('/user/dashboard') }}"><i class="icon-dashboard"></i> <span>Dashboard</span></a> </li>
    @endif

    @if (\Request::is('death-notice/add-notice') || \Request::is('death-notice/view-notice'))
    <li class="submenu active"> <a href=""><i class="icon icon-paste"></i> <span>Death Notice</span> </a>
      <ul>
        <li><a href="{{ url('/death-notice/add-notice') }}">Add Death Notice</a></li>
        <li><a href="{{ url('/death-notice/view-notice') }}">View Death Notice</a></li>
      </ul>
    </li>
    @else
    <li class="submenu"> <a href=""><i class="icon icon-paste"></i> <span>Death Notice</span> </a>
      <ul>
        <li id="new1"><a href="{{ url('/death-notice/add-notice') }}">Add Death Notice</a></li>
        <li id="new2"><a href="{{ url('/death-notice/view-notice') }}">View Death Notice</a></li>
      </ul>
    </li>

    @endif

 <?php } ?>

  </ul>
</div>
<!--sidebar-menu-->

