@extends('layouts.userLayout.user_design')
@section('content')
@section('title','Dashboard')

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">  <div id="breadcrumb"> <a href="{{ url('/user/dashboard')}}" class="tip-bottom"> <i class="icon-dashboard"></i> Dashboard</a><h5 style="float:left;margin-left: 20px;"><?php date_default_timezone_set('Asia/Kathmandu'); echo date("F j, Y, h:i A");?></h3></div>
  </div>
  
<!--End-breadcrumbs-->

<!--Action boxes-->
  <div class="container-fluid">
    <div class="quick-actions_homepage">
      <ul class="quick-actions">
     <?php $key = Session::get('loginSession'); ?>
      <?php $pendingNotices = DB::table('death_notice_log')->where('isVerified',0)->get(); ?>
       <?php $activeNotices = DB::table('death_notice')->where('status',1)->get(); ?>
       
       
       <div class="container-fluid">
        @if(Session::has('flash_message_error'))
           
           <div class="alert alert-danger alert-block">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
                   <strong> {!! session('flash_message_error') !!} </strong>
           </div>

        @endif

          @if(Session::has('flash_message_success'))

            <div class="alert alert-success alert-block">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
                   <strong> {!! session('flash_message_success') !!} </strong>
            </div>

           @endif
           
       <?php  if($key == 1) { ?>
        <li class="bg_lo"> <a href="{{ url('/death-notice/view-all-pending-notice') }} "> <i class="icon-thumbs-down"></i>Pending Notices<br> <h4> <?php if (empty($pendingNotices)) echo "0"; else echo count($pendingNotices); ?> </h4></a> </li>

        <li class="bg_lo"> <a href="{{ url('/death-notice/view-all-active-notice') }} "> <i class="icon-thumbs-up"></i> Active Notices<br> <h4> <?php if (empty($activeNotices)) echo "0"; else echo count($activeNotices); ?> </h4></a> </li>
   <?php } else { echo "";  }?>

     </ul>
    </div>
<!--End-Action boxes-->    


  </div>
</div>
<!--end-main-container-part-->

@endsection
