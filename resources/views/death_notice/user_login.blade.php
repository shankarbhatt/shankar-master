<!DOCTYPE html>
<html dir="ltr" lang="en">

<head> <link rel="shortcut icon" type="image/ico" href="{{ asset('images/backend_images/favicon.ico') }}"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
   
    <title>Login :: Death Notice</title>
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('newlogin/assets/libs/select2/dist/css/select2.min.css')}}">
    <link href="{{ asset('newlogin/dist/css/style.min.css')}}" rel="stylesheet">

</head>

<body>
   

    <div id="main-wrapper">
        
            <div class="container-fluid">
             
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <form class="form-horizontal" id="userloginform" name="userloginform" method="post" style="background:#732231;width: 700px;margin-left: 300px;margin-top: 50px;" action="{{ url('death-notice-login') }}"> {{ csrf_field() }}
                                <div class="card-body" style="margin-right: 70px;width:650px;">

                                        @if(count($errors))

                                          <div class="alert alert-danger" id="error" style="width: 650px;margin-left: 7px;">
                                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                   <span aria-hidden="true">&times;</span>
                                               </button>
                                            <strong>Whoops!</strong> There were some problems with your input.
                                              <br/>
                                               <ul>
                                              
                                                @foreach($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                                 @endforeach
                                             </ul>
                                          </div>

                                       @endif

                                     @if(Session::has('flash_message_error'))
       
                                       <div class="alert alert-danger alert-block" id="newError" style="width: 650px;margin-left: 7px;">
                                           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                               <span aria-hidden="true">&times;</span>
                                           </button>
                                               <strong> {!! session('flash_message_error') !!} </strong>
                                       </div>

                                    @endif

                                      @if(Session::has('flash_message_success'))

                                        <div class="alert alert-success alert-block" id="successError">
                                           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                               <span aria-hidden="true">&times;</span>
                                           </button>
                                               <strong> {!! session('flash_message_success') !!} </strong>
                                        </div>

                                       @endif

                                    <h3 class="card-title" style="text-align: center;padding-bottom: 20px;color: #ffffff;margin-left: 100px;margin-top: 30px;"><b>DEATH NOTICE LOGIN</b></h3>
                                    <div class="form-group row">
                                     <label for="user_type" class="col-sm-3 text-right control-label col-form-label" style="color: #ffffff;">User Type</label>
                                         <?php $user_type = DB::table('user_type')->get();?>
                                    <div class="col-md-9">
                                        
                                         
                            <select class="select2 form-control custom-select" name="user_type" id="user_type">
                                <option selected disabled>Select User Type</option>
                                @foreach($user_type as $data)
                                 <option value="{{ $data->id }}">{{ strtoupper($data->title) }}</option>
                                @endforeach
                            </select>
                                            
                                    </div>
                                </div>
                                 <div class="form-group row">
                                  <label for="user_id" class="col-sm-3 text-right control-label col-form-label" style="text-align: center;color: #ffffff;">DP/RTA Name</label>
                                    <div class="col-md-9">
                                        <select class="select2 form-control custom-select" name="user_id" id="user_id" style="margin-top: 5px;">
                                             <option selected disabled>Select DP/RTA Name</option>
                                            
                                        </select>
                                    </div>
                                </div>
                                    <div class="form-group row">
                                        <label for="email" class="col-sm-3 text-right control-label col-form-label" style="text-align: center;color: #ffffff;margin-top: 5px;">Email</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="uemail" name="uemail" placeholder="Email Here" style="margin-top: 5px;">
                                        </div>
                                    </div>
                                  
                                    <div class="form-group row" style="margin-bottom:30px;">
                                        <label for="password" class="col-sm-3 text-right control-label col-form-label" style="text-align: center;color: #ffffff;">Password</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" id="upassword" name="upassword" placeholder="Password Here (at least 8 characters)">
                                        </div>
                                    </div>
                                 
                                 <input type="submit" value="LOGIN" class="btn btn-success" style="width:300px;margin-bottom: 30px;position: relative;left: 35%;font-weight: bold;">
                               </div>
                            </form>
                        </div>
                       <div id="footer" style="position:fixed;bottom:0;width:100%;height:100px;"><h5 style="text-align:center; margin-top:70px;">Copyright&copy; <?php echo date("Y");?>. CDS and Clearing Limited. All Rights Reserved. </h5></div>
                    </div>
                
                </div>
            
        </div>
   
     
    </div>
    
    <script src="{{ asset('js/backend_js/bootstrap.min.js') }}"></script> 
    <script src="{{ asset('js/backend_js/jquery.min.js') }}"></script> 
    <script src="{{ asset('newlogin/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{ asset('newlogin/assets/libs/select2/dist/js/select2.min.js')}}"></script>

    <script type="text/javascript">
     
     $(document).ready(function(){
      $("#user_type").select2();
      $("#user_id").select2();
  });

  
  </script>

   <script type="text/javascript">
         $(".close").click(function () {
            $('#error').hide();
            $('#newError').hide();
            $('#success').hide();
    });


    </script>

      <script type="text/javascript">
    jQuery(document).ready(function ()
    {
            jQuery('select[name="user_type"]').on('change',function(){
               var ID = jQuery(this).val();
               var selectVal = $("#user_type option:selected").val();


               if(ID == 1)
               {
                 jQuery.ajax({
                     url : '/user/getadmin/',
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        console.log(data);
                        jQuery('select[name="user_id"]').empty();
                         $('select[name="user_id"]').append('<option value="0">Select DP/RTA Name</option>');
                        jQuery.each(data, function(key,value){
                           $('select[name="user_id"]').append('<option value="1">'+ value +'</option>');
                        });
                     }
                  });
               }
               else
               {
                  $('select[name="user_id"]').empty();
               }

               if(ID == 3)
               {
  
                  jQuery.ajax({
                     url : '/user/getdp/',
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        console.log(data);
                        jQuery('select[name="user_id"]').empty();
                         $('select[name="user_id"]').append('<option value="0">Select DP/RTA Name</option>');
                        jQuery.each(data, function(key,value){
                           $('select[name="user_id"]').append('<option value="'+ key +'">'+ value + " (" + key + ")"+'</option>');
                        });
                     }
                  });
               }
               else
               {
                  $('select[name="user_id"]').empty();
               }

              if(ID == 4)
               {
                  
                  jQuery.ajax({
                     url : '/user/getrta/',
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        console.log(data);
                        jQuery('select[name="user_id"]').empty();
                         $('select[name="user_id"]').append('<option value="0">Select DP/RTA Name</option>');
                        jQuery.each(data, function(key,value){
                           $('select[name="user_id"]').append('<option value="'+ key +'">'+ value + " (" + key + ")"+'</option>');
                        });
                     }
                  });
               }
               else
               {
                  $('select[name="user_id"]').empty();
               }
            });
    });
    </script>
   
</body>

</html>