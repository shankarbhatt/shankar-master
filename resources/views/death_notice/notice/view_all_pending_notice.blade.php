@extends('layouts.userLayout.user_design')
@section('content')
@section('title','Pending Death Notice')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/user/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a>  <a href="">Death Notice</a> <a href="" class="current">  Pending Death Notice</a> </div>
    <h1>View All Pending Death Notice</h1>
    
    @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

      @if(Session::has('flash_message_success'))

        <div class="alert alert-success alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_success') !!} </strong>
        </div>
       @endif
       
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-list"></i></span>
            <h5>All Pending Death Notices List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table" style="table-layout: fixed;">
              <thead>
                <tr>
                  <th>User Type</th>
                  <th>DP/RTA Name</th>
                  <th style="max-width:80px; width:80px;word-wrap:break-word;">Death Person Name</th>
                  <th style="max-width:50px; width:50px;word-wrap:break-word;">Father's Name</th>
                  <th style="max-width:60px; width:60px;word-wrap:break-word;">Grandfather's Name</th>
                  <th>Address</th>
                  <th>BO Account</th>
                  <th>File</th>
                  <th>Published Date</th>
                  <th>Ending Date</th>
                  <th>Created Date</th>
                  <th>Request</th>
                  <th style="max-width:60px; width:60px;word-wrap:break-word;">Is Verified</th>
                  <th style="max-width:110px; width:110px;word-wrap:break-word;">Action</th>
                </tr>
              </thead>
              <tbody>
              @foreach($notices as $data)
                <?php if($data->status != 0) { } else {?>
                <tr class="gradeU">
                  <td style="text-transform: uppercase;"><?php if($data->user_type == 3) echo "DP"; else if($data->user_type == 4) echo "RTA"; else echo "";?></td>
      
                    <?php if($data->user_type == 3) 
                      $id = DB::table('death_notice')->leftJoin('dp', 'death_notice.user_id', '=', 'dp.dp_id')->where('user_id',
                      $data->user_id)->get(); 
                      else if($data->user_type == 4)  
                        $id = DB::table('death_notice')->leftJoin('rta',
                       'death_notice.user_id', '=', 'rta.rta_id')->where('user_id',$data->user_id)->get(); 
                       else echo "";?>
                  @foreach($id as $user)
                  @endforeach
                  <td style="max-width:100px; width:100px;word-wrap:break-word;text-transform: uppercase;"> {{ $user->name.' ('.$data->user_id.')' }}  </td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->name }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->father_name }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->grandfather_name }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->address }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->bo_account }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->file }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->published_date }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->ending_date }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->created_at }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;"><?php if($data->request == 1) echo 'Add'; if($data->request==2) echo "Edit";  if($data->request == 3) echo 'Delete'; ?></td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;"><?php if($data->isVerified == 0) echo 'Pending'; else echo 'Verified';?></td>
                  <td style="width: 120px;max-width:120px;">
                  <?php if($data->status == 0 && $data->request == 1) { ?>
                  <a rel="{{ $data->notice_id }}" rel1="verify-insert-notice" id="verNotice" class="btn btn-success btn-mini verifyInsertNoticeRecord" title="Verify" href="javascript:"><i class="icon icon-ok"></i></a>
      
                 <a rel="{{ $data->notice_id }}" rel1="reject-insert-notice" id="rejNotice" class="btn btn-danger btn-mini rejectInsertNoticeRecord" title="Reject" href="javascript:"><i class="icon icon-remove"></i></a>
                  <?php } ?>
                  <?php  if($data->status == 0 && $data->request == 3) { ?>
                  <a rel="{{ $data->notice_id }}" rel1="verify-delete-notice" id="verNotice" class="btn btn-success btn-mini verifyDeleteNoticeRecord" title="Verify" href="javascript:"><i class="icon icon-ok"></i></a>

                   <a rel="{{ $data->notice_id }}" rel1="reject-delete-notice" id="rejNotice" class="btn btn-danger btn-mini rejectDeleteNoticeRecord" title="Reject" href="javascript:"><i class="icon icon-remove"></i></a>
                  <?php } ?>
              
                    <?php  if($data->status == 0 && $data->request == 2) { ?>
                  <a rel="{{ $data->notice_id }}" rel1="verify-edit-notice" id="verNotice" class="btn btn-success btn-mini verifyEditNoticeRecord" title="Verify" href="javascript:"><i class="icon icon-ok"></i></a>

                  <a rel="{{ $data->notice_id }}" rel1="reject-edit-notice" id="rejNotice" class="btn btn-danger btn-mini rejectDeleteNoticeRecord" title="Reject" href="javascript:"><i class="icon icon-remove"></i></a>
                  <?php } ?>
              
                  <?php if(!empty($data->file)) { ?>
                   <a href="{{ asset('death_notice_files/' . $data->file) }}"  target="_blank" class="btn btn-info btn-mini" title="View File"><i class="icon icon-eye-open"></i></a>
                   <a href="{{ asset('death_notice_files/' . $data->file) }}" class="btn btn-inverse btn-mini" download="{{ $data->file }}" title="Download File"><i class="icon icon-download-alt"></i></a>
                  <?php } ?>
                  </td>
                </tr>
                  <?php } ?>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
