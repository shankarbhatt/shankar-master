@extends('layouts.userLayout.user_design')
@section('content')
@section('title','Death Notice Logs')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/user/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a>  <a href="">Death Notice</a> <a href="" class="current">Death Notice Logs</a> </div>
    <h1>Death Notice Logs</h1>
    
    @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

      @if(Session::has('flash_message_success'))

        <div class="alert alert-success alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_success') !!} </strong>
        </div>
       @endif
       
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-list"></i></span>
            <h5>All Death Notices Logs List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table" style="table-layout: fixed;">
              <thead>
                <tr>
                  <th>Death Person Name</th>
                  <th>Father's Name</th>
                  <th>Grandfather's Name</th>
                  <th>Address</th>
                  <th>BO Account</th>
                  <th>File</th>
                  <th>Published Date</th>
                  <th>Ending Date</th>
                  <th>Created Date</th>
                  <th>Request</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              @foreach($notices as $data)
               <?php if($data->status == 3) { echo " ";} else { ?>
                <tr class="gradeU">
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->name }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->father_name }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->grandfather_name }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->address }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->bo_account }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;"> <a href="{{ asset('death_notice_files/' . $data->file) }}"  target="_blank" title="View File">{{ $data->file }}</a></td> 
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->published_date }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->ending_date }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->created_at }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;"><?php if($data->request == 1) echo 'Add'; if($data->request==2) echo "Edit";  if($data->request == 3) echo 'Delete';  ?></td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;"><?php if($data->status == 0) echo 'Pending'; if($data->status==1) echo 'Verified'; if($data->status==2) echo "Rejected"; if($data->status == 3) echo 'Deleted'; if($data->status==4) echo "Ended"; ?></td>
                </tr>
                 <?php } ?>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection