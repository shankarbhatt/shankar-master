@extends('layouts.userLayout.user_design')
@section('content')
@section('title','Edit Death Notice')

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="{{ url('/user/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a> <a href="">Death Notice</a> <a href="" class="current">Add Death Notice</a> </div>
    <h1>Edit Death Notice</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">

         @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

         @if(count($errors))

      <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
        <strong>Whoops!</strong> There were some problems with your input.
          <br/>
           <ul>
          
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
             @endforeach
         </ul>
      </div>

   @endif

      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-plus"></i> </span>
          <h5>Edit Death Notice</h5>
        </div>
          <h7 style="color:red;margin-left: 60px;">(*) fields are required</h7>
        <div class="widget-content nopadding">
          <form action="{{ url('/death-notice/edit-notice/'.$noticeDetails->id) }}" method="post" enctype="multipart/form-data" class="form-horizontal" name="edit_death_notice" id="edit_death_notice" novalidate="novalidate"> {{ csrf_field() }}
             <div class="control-group">
                <label class="control-label">Death Person Name  <span style="color:red;">*</span></label>
                <div class="controls">
                <input type="text" id="death_name" name="death_name" placeholder="Enter Name" value=" {{ $noticeDetails->name }} ">
                </div>
            </div>
               <div class="control-group">
                <label class="control-label">Father's Name  <span style="color:red;">*</span></label>
                <div class="controls">
                <input type="text" id="father_name" name="father_name" placeholder="Enter Father's Name" value=" {{ $noticeDetails->father_name }} ">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Grandfather's Name  <span style="color:red;">*</span></label>
                <div class="controls">
                <input type="text" id="grandfather_name" name="grandfather_name" placeholder="Enter Grandfather's Name Name" value=" {{ $noticeDetails->grandfather_name }} ">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Address  <span style="color:red;">*</span></label>
                <div class="controls">
                <input type="text" id="address" name="address" placeholder="Enter Permanent Address" value=" {{ $noticeDetails->address }} ">
                </div>
            </div>
            <div class="control-group">
                <?php $userType = Session::get('loginSession');?>
                <label class="control-label">BO Account <?php if($userType == 3){ ?> <span style="color:red;">*</span> <?php } else { ?> <span style=""></span> <?php } ?>
                </label>
                <div class="controls">
                <input type="text" id="bo_account" name="bo_account" placeholder="Enter Benificial Owner Account" value=" {{ $noticeDetails->bo_account }} ">
                </div>
            </div>
            <!-- <div class="control-group">
                <label class="control-label">File Name  <span style="color:red;">*</span></label>
                <div class="controls">
                <input type="text" id="file_name" name="file_name" placeholder="Enter File Name"  value=" {{ $noticeDetails->file_name }} ">
                </div>
            </div>-->
            <div class="control-group">
              <label class="control-label">File <span style="color:red;">*</span></label> 
              <div class="controls">
                <input type="file" name="file" id="file" />
                <input type="hidden" name="current_file" value="{{ $noticeDetails->file }}"> <span style="color:red;">File format: pdf and Size: less than 2MB</span> <br><span>Old File: {{ $noticeDetails->file }} </span> 
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Update</button>
            </div>
          </form>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
<script src="{{ asset('js/backend_js/matrix.form_common.js') }}"></script>
@endsection