@extends('layouts.userLayout.user_design')
@section('content')
@section('title','Death Notices')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/user/dashboard') }}" class="tip-bottom"><i class="icon-dashboard"></i> Dashboard</a>  <a href="">Death Notice</a> <a href="" class="current">Death Notices</a> </div>
    <h1>View All Death Notice</h1>
    
    @if(Session::has('flash_message_error'))
       
       <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_error') !!} </strong>
       </div>

    @endif

      @if(Session::has('flash_message_success'))

        <div class="alert alert-success alert-block">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
               <strong> {!! session('flash_message_success') !!} </strong>
        </div>
       @endif
       
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-list"></i></span>
            <h5>All Death Notices List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table" style="table-layout: fixed;">
              <thead>
                <tr>
                  <th>User Type</th>
                  <th>DP/RTA Name</th>
                  <th>Death Person Name</th>
                  <th>Father's Name</th>
                  <th style="max-width:100px; width:100px;word-wrap:break-word;">Grandfather's Name</th>
                  <th>Address</th>
                  <th>BO Account</th>
                  <th>File</th>
                  <th>Published Date</th>
                  <th>Ending Date</th>
                  <th>Created Date</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              @foreach($notices as $data)
                <tr class="gradeU">
                  <td style="max-width:100px; width:100px;word-wrap:break-word;text-transform: uppercase;"><?php if($data->user_type == 3) echo "DP"; else if($data->user_type == 4) echo "RTA"; else echo "";?></td>
      
                    <?php if($data->user_type == 3) 
                      $id = DB::table('death_notice')->leftJoin('dp', 'death_notice.user_id', '=', 'dp.dp_id')->where('user_id',$data->user_id)->get(); 
                      else if($data->user_type == 4)  
                        $id = DB::table('death_notice')->leftJoin('rta',
                       'death_notice.user_id', '=', 'rta.rta_id')->where('user_id',$data->user_id)->get(); 
                       else echo "";?>
                  @foreach($id as $user)
                  @endforeach
                  <td style="max-width:100px; width:100px;word-wrap:break-word;text-transform: uppercase;"> {{ $user->name.' ('.$data->user_id.')' }}  </td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->name }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->father_name }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->grandfather_name }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->address }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->bo_account }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;"> <a href="{{ asset('death_notice_files/' . $data->file) }}"  target="_blank" title="View File">{{ $data->file }}</a></td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->published_date }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->ending_date }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;">{{ $data->created_at }}</td>
                  <td style="max-width:100px; width:100px;word-wrap:break-word;"><?php if($data->status == 0) echo 'Pending'; if($data->status==1) echo 'Verified';  if($data->status==2) echo "Rejected";  if($data->status == 3) echo 'Deleted'; if($data->status==4) echo "Ended"; ?></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection