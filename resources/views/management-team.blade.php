@include('Home.layouts.header')
<title>Management Team :: CDSC</title>

<!-- Project Details Start -->
<section id="solution" class="p-t-30 p-b-30">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="solution_tabs">
                    <ul>
                                                        <li class="">
                                                        <a href="{{url('Home.About-Us.introduction')}}">Introduction</a>
                                </li>
                                                        <li class="active">
                                                        <a href="{{url('Home.About-Us.board-of-directory')}}">Board of Directors</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('Home.About-Us.management-team')}}">Management Team</a>
                                </li>
                                                        <li class="">
                                                        <a href="{{url('Home.About-Us.global-partner')}}">Global Partners</a>
                                            
                                </li>
                                            </ul>
                </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div id="board-director" class="our_team">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading">
                                <div class="heading_border bg_red"></div>
                                <h2 style="text-align:center;margin-bottom:40px;">Management Team</h2>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-md-offset-4">
                            <div class="item">
                                <div class="single-effect">
                                    <figure class="wpf-demo-gallery">
                                                                                <a href="#">
                                                                                            <img src="../../uploads/teams/dev_prakash_gupta.jpg" alt="img" style="height:220px;">
                                                                                    </a>
                                                                            </figure>
                                </div>
                                <div class="team_text">
                                    <h3>Mr. Dev Prakash Gupta</h3>
                                    <h5>Chief Executive Officer</h5>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <div class="item">
                                <div class="single-effect">
                                    <figure class="wpf-demo-gallery">
                                                                                <a href="#">
                                                                                            <img src="../../uploads/teams/default.jpg" alt="img" style="height:220px;">
                                                                                    </a>
                                                                            </figure>
                                </div>
                                <div class="team_text">
                                    <h3>Mr. Mr. Sunil Gurung</h3>
                                    <h5>Head, Finance and Accounts Department</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="item">
                                <div class="single-effect">
                                    <figure class="wpf-demo-gallery">
                                                                                <a href="#">
                                                                                            <img src="../../uploads/teams/sabina.jpg" alt="img" style="height:220px;">
                                                                                    </a>
                                                                            </figure>
                                </div>
                                <div class="team_text">
                                    <h3>Mrs. Sabina Pujari</h3>
                                    <h5>Head, Admin, Business Development</h5>
                                </div>
                            </div>
                        </div>
                    
                    </div>

                       <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <div class="item">
                                <div class="single-effect">
                                    <figure class="wpf-demo-gallery">
                                                                                <a href="#">
                                                                                            <img src="../../uploads/teams/default.jpg" alt="img" style="height:220px;">
                                                                                    </a>
                                                                            </figure>
                                </div>
                                <div class="team_text">
                                    <h3>Mr. Yoga Raj Joshi</h3>
                                    <h5>Head, Database Section IT Department</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="item">
                                <div class="single-effect">
                                    <figure class="wpf-demo-gallery">
                                                                                <a href="#">
                                                                                            <img src="../../uploads/teams/default.jpg" alt="img" style="height:220px;">
                                                                                    </a>
                                                                            </figure>
                                </div>
                                <div class="team_text">
                                    <h3>Mr. Suresh Neupane</h3>
                                    <h5>Head, System Section IT Department</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="item">
                                <div class="single-effect">
                                    <figure class="wpf-demo-gallery">
                                                                                <a href="#">
                                                                                            <img src="../../uploads/teams/default3.jpg" alt="img" style="height:220px;">
                                                                                    </a>
                                                                            </figure>
                                </div>
                                <div class="team_text">
                                    <h3>Mrs. Kanchan Sapkota</h3>
                                    <h5>Head, Network Section IT Department, Operations Department</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Project Details End -->
   <!-- Footer Start -->
  @include('Home.layouts.footer')