
$(document).ready(function(){
	
	
	
	$('input[type=checkbox],input[type=radio],input[type=file]').uniform();
	
	$('select').select2();
	
	// Form Validation
    $("#basic_validate").validate({
		rules:{
			required:{
				required:true
			},
			email:{
				required:true,
				email: true
			},
			date:{
				required:true,
				date: true
			},
			url:{
				required:true,
				url: true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	//add news validation
	$("#add_news_notice").validate({
		rules:{
			title:{
				required:true
			},
			description:{
				required:true
			},
			type:{
				required:true
			},
			file_name:{
				required:true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	//edit news validation
	$("#edit_news_notice").validate({
		rules:{
			news_title:{
				required:true
			},
			news_description:{
				required:true
			},
			type:{
				required:true
			},
			file_name:{
				required:true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	
	//add press release validation
	$("#add_press_release").validate({
		rules:{
			title:{
				required:true
			},
			description:{
				required:true
			},
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	//edit press release validation
	$("#edit_press_release").validate({
		rules:{
			title:{
				required:true
			},
			description:{
				required:true
			},
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	
	//add claering member validation
	$("#add_cm").validate({
		rules:{
			name:{
				required:true
			},
			cm_no:{
				required:true,
				digits:true
			},
			address:{
				required:true
			},
			pull_account:{
				required:true,
				digits:true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	//edit claering member validation
	$("#edit_cm").validate({
		rules:{
			name:{
				required:true
			},
			cm_no:{
				required:true,
				digits:true
			},
			address:{
				required:true
			},
			pull_account:{
				required:true,
				digits:true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	
	//add downloads validation
	$("#add_downloads").validate({
		rules:{
			title:{
				required:true
			},
			type:{
				required:true
			},
			file_name:{
				required:true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	//edit downloads validation
	$("#edit_downloads").validate({
		rules:{
			title:{
				required:true
			},
			type:{
				required:true
			},
			file_name:{
				required:true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});	

	//add page validation
	$("#add_page").validate({
		rules:{
			title:{
				required:true
			},
			description:{
				required:true
			},
			
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	//edit publication validation
	$("#edit_page").validate({
		rules:{
			title:{
				required:true
			},
			description:{
				required:true
			},
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});	

	//add RTA validation
	$("#add_rta").validate({
		rules:{
			rta_id:{
				required:true,
				digits:true
			},
			name:{
				required:true
			},
			address:{
				required:true
			},
			phone:{
				required:true,
			},
			email:{
				required:true,
				email:true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	//edit RTA validation
	$("#edit_rta").validate({
		rules:{
			name:{
				required:true,
				digits:true
			},
			address:{
				required:true
			},
			phone:{
				required:true,
			},
			email:{
				required:true,
				email:true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});	

	//add Issuer validation
	$("#add_issuer").validate({
		rules:{
			name:{
				required:true
			},
			address:{
				required:true
			},
			phone:{
				required:true,
			},
			email:{
				required:true,
				email:true
			},
			company_code:{
				required:true,
				digits:true
			},
			reg_no:{
				required:true,
				digits:true
			},
			pan_no:{
				required:true,
				digits:true
			},
			issued_capital:{
				required:true,
				digits:true
			},
			listed_capital:{
				required:true,
				digits:true
			},
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	//edit issuer validation
	$("#edit_issuer").validate({
		rules:{
			name:{
				required:true
			},
			address:{
				required:true
			},
			phone:{
				required:true,
				digits:true
			},
			email:{
				required:true,
				email:true
			},
			company_code:{
				required:true,
				digits:true
			},
			reg_no:{
				required:true,
				digits:true
			},
			pan_no:{
				required:true,
				digits:true
			},
			issued_capital:{
				required:true,
				digits:true
			},
			listed_capital:{
				required:true,
				digits:true
			},
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	//add ISIN validation
	$("#add_isin").validate({
		rules:{
			script:{
				required:true
			},
			isin_code:{
				required:true
			},
			type:{
				required:true
			},
			remarks:{
				required:true
			},
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	//edit ISIN validation
	$("edit_isin").validate({
		rules:{
			script:{
				required:true
			},
			isin_code:{
				required:true
			},
			type:{
				required:true
			},
			remarks:{
				required:true
			},
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});


	//add DP validation
	$("#add_dp").validate({
		rules:{
			dp_id:{
				required:true,
				digits:true
			},
			name:{
				required:true
			},
			address:{
				required:true
			},
			phone:{
				required:true,
			},
			email:{
				required:true,
				email:true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	//edit DP validation
	$("#edit_dp").validate({
		rules:{
			dp_id:{
				required:true
			},
			name:{
				required:true
			},
			address:{
				required:true
			},
			phone:{
				required:true,
				digits:true
			},
			email:{
				required:true,
				email:true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	//add CDSC Tariff validation
	$("#add_cdsc_tariff").validate({
		rules:{
			title:{
				required:true
			},
			amount:{
				required:true,
				digits:true
			},
			cdsc_amount:{
				required:true,
				digits:true
			},
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

		//edit CDSC Tariff validation
		$("#edit_cdsc_tariff").validate({
			rules:{
				title:{
					required:true
				},
				amount:{
					required:true,
					digits:true
				},
				cdsc_amount:{
					required:true,
					digits:true
				},
			},
			errorClass: "help-inline",
			errorElement: "span",
			highlight:function(element, errorClass, validClass) {
				$(element).parents('.control-group').addClass('error');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).parents('.control-group').removeClass('error');
				$(element).parents('.control-group').addClass('success');
			}
		});

		//add Settlement validation
		$("#add_settlement").validate({
			rules:{
				settlement_id:{
					required:true,
					digits:true
				},
				trade_date:{
					required:true,
					date:true
				},
			},
				errorClass: "help-inline",
				errorElement: "span",
				highlight:function(element, errorClass, validClass) {
					$(element).parents('.control-group').addClass('error');
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).parents('.control-group').removeClass('error');
					$(element).parents('.control-group').addClass('success');
				}
		});

		//edit Settlement validation
		$("#edit_settlement").validate({
			rules:{
				settlement_id:{
				    required:true
				},
				trade_date:{
					required:true,
					date:true
				},
			},
				errorClass: "help-inline",
				errorElement: "span",
				highlight:function(element, errorClass, validClass) {
					$(element).parents('.control-group').addClass('error');
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).parents('.control-group').removeClass('error');
					$(element).parents('.control-group').addClass('success');
				}
		});

		//add bbok closure validation
	$("#add_book_closure").validate({
		rules:{
			last_trade_date:{
				required:true,
				date:true
			},
			book_closure_start_date:{
				required:true,
				date:true
			},
			book_closure_end_date:{
				required:true,
				date:true
			},
			book_closure_purpose:{
				required:true
			},
			book_closure_fiscal_year:{
				required:true
			},
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

		//edit bbok closure validation
		$("#edit_book_closure").validate({
			rules:{
				last_trade_date:{
					required:true,
					date:true
				},
				book_closure_start_date:{
					required:true,
					date:true
				},
				book_closure_end_date:{
					required:true,
					date:true
				},
				book_closure_purpose:{
					required:true
				},
				book_closure_fiscal_year:{
					required:true
				},
			},
			errorClass: "help-inline",
			errorElement: "span",
			highlight:function(element, errorClass, validClass) {
				$(element).parents('.control-group').addClass('error');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).parents('.control-group').removeClass('error');
				$(element).parents('.control-group').addClass('success');
			}
		});

	//add menu validation
	$("#add_menu").validate({
		rules:{
			name:{
				required:true,
				regex:"^[a-zA-Z]+$",
			},
			order:{
				required:true,
				number:true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	//edit menu validation
	$("#edit_menu").validate({
		rules:{
			name:{
				required:true
			},
			order:{
				required:true,
				number:true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	//add submenu validation
	$("#add_submenu").validate({
		rules:{
			submenu_name:{
				required:true
			},
			order:{
				required:true,
				number:true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	//edit submenu validation
	$("#edit_submenu").validate({
		rules:{
			submenu_name:{
				required:true
			},
			order:{
				required:true,
				number:true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

			//add daily update validation
			$("#add_daily_update").validate({
				rules:{
					no_of_bo_account:{
						required:true,
						digits:true
					},
					no_of_demat_shares:{
						required:true,
						digits:true
					}
				},
				errorClass: "help-inline",
				errorElement: "span",
				highlight:function(element, errorClass, validClass) {
					$(element).parents('.control-group').addClass('error');
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).parents('.control-group').removeClass('error');
					$(element).parents('.control-group').addClass('success');
				}
			});
			
			//edit daily update validation
			$("#edit_daily_update").validate({
				rules:{
					no_of_bo_account:{
						required:true,
						number:true
					},
					no_of_demat_shares:{
						required:true,
						number:true
					}
				},
				errorClass: "help-inline",
				errorElement: "span",
				highlight:function(element, errorClass, validClass) {
					$(element).parents('.control-group').addClass('error');
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).parents('.control-group').removeClass('error');
					$(element).parents('.control-group').addClass('success');
				}
			});

			//add Image validation
			$("#add_image").validate({
				rules:{
					title:{
						required:true,
					}
				},
				errorClass: "help-inline",
				errorElement: "span",
				highlight:function(element, errorClass, validClass) {
					$(element).parents('.control-group').addClass('error');
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).parents('.control-group').removeClass('error');
					$(element).parents('.control-group').addClass('success');
				}
			});

			//edit Image validation
			$("#edit_image").validate({
				rules:{
					title:{
						required:true,
					}
				},
				errorClass: "help-inline",
				errorElement: "span",
				highlight:function(element, errorClass, validClass) {
					$(element).parents('.control-group').addClass('error');
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).parents('.control-group').removeClass('error');
					$(element).parents('.control-group').addClass('success');
				}
			});

			//add casba bank validation
			$("#add_casba_bank").validate({
				rules:{
					bank_id:{
						required:true,
						digits:true
					},
					bank_name:{
						required:true
					}
				},
				errorClass: "help-inline",
				errorElement: "span",
				highlight:function(element, errorClass, validClass) {
					$(element).parents('.control-group').addClass('error');
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).parents('.control-group').removeClass('error');
					$(element).parents('.control-group').addClass('success');
				}
			});

			//edit casba bank validation
			$("#edit_casba_bank").validate({
				rules:{
					bank_id:{
						required:true,
						number:true
					},
					bank_name:{
						required:true
					}
				},
				errorClass: "help-inline",
				errorElement: "span",
				highlight:function(element, errorClass, validClass) {
					$(element).parents('.control-group').addClass('error');
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).parents('.control-group').removeClass('error');
					$(element).parents('.control-group').addClass('success');
				}
			});

				//add user validation
			$("#add_user").validate({
				rules:{
					email:{
						required:true,
						email:true
					},
					password:{
						required:true
					}
				},
				errorClass: "help-inline",
				errorElement: "span",
				highlight:function(element, errorClass, validClass) {
					$(element).parents('.control-group').addClass('error');
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).parents('.control-group').removeClass('error');
					$(element).parents('.control-group').addClass('success');
				}
			});

				//edit user validation
			$("#edit_user").validate({
				rules:{
					email:{
						required:true,
						email:true
					},
					password:{
						required:true
					}
				},
				errorClass: "help-inline",
				errorElement: "span",
				highlight:function(element, errorClass, validClass) {
					$(element).parents('.control-group').addClass('error');
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).parents('.control-group').removeClass('error');
					$(element).parents('.control-group').addClass('success');
				}
			});
	
	$("#number_validate").validate({
		rules:{
			min:{
				required: true,
				min:10
			},
			max:{
				required:true,
				max:24
			},
			number:{
				required:true,
				number:true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});
	
	$("#password_validate").validate({
		rules:{
			current_pwd:{
				required: true,
				minlength:6,
				maxlength:200
			},
			new_pwd:{
				required: true,
				minlength:6,
				maxlength:200
			},
			confirm_pwd:{
				required: true,
				minlength:6,
				maxlength:200,
				equalTo:"#new_pwd"
			},
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	//delete news script
	/*$("#delNews").click(function(){
		if(confirm('Are You Sure Want to Delete?')){
			return true;
		}
		return false;
	});*/

	//delete ISIN
	$(".deleteISINRecord").click(function(){
		var id = $(this).attr('rel');
		var deleteFunction = $(this).attr('rel1');
		swal({
			title: 'Do you want to delete?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'
		},
		function(){
			window.location.href="/admin/"+deleteFunction+"/"+id;
		  });
		  
		});

	//delete news
	$(".deleteNewsandNoticeRecord").click(function(){
		var id = $(this).attr('rel');
		var deleteFunction = $(this).attr('rel1');
		swal({
			title: 'Do you want to delete?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'
		},
		function(){
			window.location.href="/admin/"+deleteFunction+"/"+id;
		  });
		  
		});

		//publish news and Notice
	$(".publishNewsandNoticeRecord").click(function(){
		var id = $(this).attr('rel');
		var deleteFunction = $(this).attr('rel1');
		swal({
			title: 'Do you want to publish?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, publish it!'
		},
		function(){
			window.location.href="admin/"+deleteFunction+"/"+id;
		  });
		  
		});

		//unpublish News and Notice
		$(".unpublishNewsandNoticeRecord").click(function(){
			var id = $(this).attr('rel');
			var deleteFunction = $(this).attr('rel1');
			swal({
				title: 'Do you want to unpublish?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, unpublish it!'
			},
			function(){
				window.location.href="/admin/"+deleteFunction+"/"+id;
			  });
			  
			});


				//delete Press Release
			$(".deletePressRecord").click(function(){
				var id = $(this).attr('rel');
				var deleteFunction = $(this).attr('rel1');
				swal({
					title: 'Do you want to delete?',
					text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, delete it!'
				},
				function(){
					window.location.href="/admin/"+deleteFunction+"/"+id;
				  });
				  
				});

		//publish Press Release
	$(".publishPressRecord").click(function(){
		var id = $(this).attr('rel');
		var deleteFunction = $(this).attr('rel1');
		swal({
			title: 'Do you want to publish?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, publish it!'
		},
		function(){
			window.location.href="/admin/"+deleteFunction+"/"+id;
		  });
		  
		});

		//unpublish Press Release
		$(".unpublishPressRecord").click(function(){
			var id = $(this).attr('rel');
			var deleteFunction = $(this).attr('rel1');
			swal({
				title: 'Do you want to unpublish?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, unpublish it!'
			},
			function(){
				window.location.href="/admin/"+deleteFunction+"/"+id;
			  });
			  
			});


	//delete clearing member
	$(".deleteCMRecord").click(function(){
		var id = $(this).attr('rel');
		var deleteFunction = $(this).attr('rel1');
		swal({
			title: 'Do you want to delete?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'
		},
		function(){
			window.location.href="/admin/"+deleteFunction+"/"+id;
		  });
		  
		});

		//activate Clearing Member
		$(".activateCMRecord").click(function(){
			var id = $(this).attr('rel');
			var deleteFunction = $(this).attr('rel1');
			swal({
				title: 'Do you want to activate?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, activate it!'
			},
			function(){
				window.location.href="/admin/"+deleteFunction+"/"+id;
			  });
			  
			});
			
			//deactivate Clearing Member
		$(".deactivateCMRecord").click(function(){
			var id = $(this).attr('rel');
			var deleteFunction = $(this).attr('rel1');
			swal({
				title: 'Do you want to deactivate?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, deactivate it!'
			},
			function(){
				window.location.href="/admin/"+deleteFunction+"/"+id;
			  });
			  
			});

		//delete downloads script
		$(".deleteDownloadsRecord").click(function(){
			var id = $(this).attr('rel');
			var deleteFunction = $(this).attr('rel1');
			swal({
				title: 'Do you want to delete?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			},
			function(){
				window.location.href="/admin/"+deleteFunction+"/"+id;
			  });
			  
			});

			//delete publication script
		$(".deletePageRecord").click(function(){
			var id = $(this).attr('rel');
			var deleteFunction = $(this).attr('rel1');
			swal({
				title: 'Do you want to delete?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			},
			function(){
				window.location.href="/admin/"+deleteFunction+"/"+id;
			  });
			  
			});

			//delete Issuer script
		$(".deleteIssuerRecord").click(function(){
			var id = $(this).attr('rel');
			var deleteFunction = $(this).attr('rel1');
			swal({
				title: 'Do you want to delete?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			},
			function(){
				window.location.href="/admin/"+deleteFunction+"/"+id;
			  });
			  
			});

			//activate Issuer
		$(".activateIssuerRecord").click(function(){
			var id = $(this).attr('rel');
			var deleteFunction = $(this).attr('rel1');
			swal({
				title: 'Do you want to activate?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, activate it!'
			},
			function(){
				window.location.href="/admin/"+deleteFunction+"/"+id;
			  });
			  
			});

				//deactivate Issuer
		$(".deactivateIssuerRecord").click(function(){
			var id = $(this).attr('rel');
			var deleteFunction = $(this).attr('rel1');
			swal({
				title: 'Do you want to deactivate?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, deactivate it!'
			},
			function(){
				window.location.href="/admin/"+deleteFunction+"/"+id;
			  });
			  
			});


		//delete rta script
		$(".deleteRTARecord").click(function(){
			var id = $(this).attr('rel');
			var deleteFunction = $(this).attr('rel1');
			swal({
				title: 'Do you want to delete?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			},
			function(){
				window.location.href="/admin/"+deleteFunction+"/"+id;
			  });
			  
			});

			//activate RTA
		$(".activateRTARecord").click(function(){
			var id = $(this).attr('rel');
			var deleteFunction = $(this).attr('rel1');
			swal({
				title: 'Do you want to activate?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, activate it!'
			},
			function(){
				window.location.href="/admin/"+deleteFunction+"/"+id;
			  });
			  
			});

			//deactivate RTA
		$(".deactivateRTARecord").click(function(){
			var id = $(this).attr('rel');
			var deleteFunction = $(this).attr('rel1');
			swal({
				title: 'Do you want to deactivate?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, deactivate it!'
			},
			function(){
				window.location.href="/admin/"+deleteFunction+"/"+id;
			  });
			  
			});


			//delete DP script
		$(".deleteDPRecord").click(function(){
			var id = $(this).attr('rel');
			var deleteFunction = $(this).attr('rel1');
			swal({
				title: 'Do you want to delete?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			},
			function(){
				window.location.href="/admin/"+deleteFunction+"/"+id;
			  });
			  
			});

			//activate DP
		$(".activateDPRecord").click(function(){
			var id = $(this).attr('rel');
			var deleteFunction = $(this).attr('rel1');
			swal({
				title: 'Do you want to activate?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, activate it!'
			},
			function(){
				window.location.href="/admin/"+deleteFunction+"/"+id;
			  });
			  
			});

			//deactivate DP
		$(".deactivateDPRecord").click(function(){
			var id = $(this).attr('rel');
			var deleteFunction = $(this).attr('rel1');
			swal({
				title: 'Do you want to deactivate?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, deactivate it!'
			},
			function(){
				window.location.href="/admin/"+deleteFunction+"/"+id;
			  });
			  
			});

			
			//delete cdsc tariff script
		$(".deleteCDSCTariffRecord").click(function(){
			var id = $(this).attr('rel');
			var deleteFunction = $(this).attr('rel1');
			swal({
				title: 'Do you want to delete?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			},
			function(){
				window.location.href="/admin/"+deleteFunction+"/"+id;
			  });
			  
			});

				//delete settlement
		$(".deleteSettlementRecord").click(function(){
			var id = $(this).attr('rel');
			var deleteFunction = $(this).attr('rel1');
			swal({
				title: 'Do you want to delete?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			},
			function(){
				window.location.href="/admin/"+deleteFunction+"/"+id;
			  });
			  
			});

				//delete book closure
		$(".deleteBookClosureRecord").click(function(){
			var id = $(this).attr('rel');
			var deleteFunction = $(this).attr('rel1');
			swal({
				title: 'Do you want to delete?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			},
			function(){
				window.location.href="/admin/"+deleteFunction+"/"+id;
			  });
			  
			});

			//slider image
		$(".deleteImgRecord").click(function(){
			var id = $(this).attr('rel');
			var deleteFunction = $(this).attr('rel1');
			swal({
				title: 'Do you want to delete?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			},
			function(){
				window.location.href="/admin/"+deleteFunction+"/"+id;
			  });
			  
			});

				//delete Menu
	$(".deleteMenuRecord").click(function(){
		var id = $(this).attr('rel');
		var deleteFunction = $(this).attr('rel1');
		swal({
			title: 'Do you want to delete?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'
		},
		function(){
			window.location.href="/admin/"+deleteFunction+"/"+id;
		  });
		  
		});

			//deactivate Menu
			$(".activateMenuRecord").click(function(){
				var id = $(this).attr('rel');
				var deleteFunction = $(this).attr('rel1');
				swal({
					title: 'Do you want to activate?',
					text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, deactivate it!'
				},
				function(){
					window.location.href="/admin/"+deleteFunction+"/"+id;
				  });
				  
				});

		//deactivate Menu
		$(".deactivateMenuRecord").click(function(){
			var id = $(this).attr('rel');
			var deleteFunction = $(this).attr('rel1');
			swal({
				title: 'Do you want to deactivate?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, deactivate it!'
			},
			function(){
				window.location.href="/admin/"+deleteFunction+"/"+id;
			  });
			  
			});

		//delete Submenu
	$(".deleteSubmenuRecord").click(function(){
		var id = $(this).attr('rel');
		var deleteFunction = $(this).attr('rel1');
		swal({
			title: 'Do you want to Delete?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'
		},
		function(){
			window.location.href="/admin/"+deleteFunction+"/"+id;
		  });
		  
		});

		//activate Submenu
		$(".activateSubmenuRecord").click(function(){
			var id = $(this).attr('rel');
			var deleteFunction = $(this).attr('rel1');
			swal({
				title: 'Do you want to activate?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, deactivate it!'
			},
			function(){
				window.location.href="/admin/"+deleteFunction+"/"+id;
			  });
			  
			});

			//deactivate Submenu
			$(".deactivateSubmenuRecord").click(function(){
				var id = $(this).attr('rel');
				var deleteFunction = $(this).attr('rel1');
				swal({
					title: 'Do you want to deactivate?',
					text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, deactivate it!'
				},
				function(){
					window.location.href="/admin/"+deleteFunction+"/"+id;
				  });
				  
				});

			//delete Submenu
	$(".deleteBankRecord").click(function(){
		var id = $(this).attr('rel');
		var deleteFunction = $(this).attr('rel1');
		swal({
			title: 'Do you want to Delete?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'
		},
		function(){
			window.location.href="/admin/"+deleteFunction+"/"+id;
		  });
		  
		});

					//activate bank in casba
			$(".activateBankRecord").click(function(){
				var id = $(this).attr('rel');
				var deleteFunction = $(this).attr('rel1');
				swal({
					title: 'Do you want to activate?',
					text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, deactivate it!'
				},
				function(){
					window.location.href="/admin/"+deleteFunction+"/"+id;
				  });
				  
				});

				//deactivate bank in casba
			$(".deactivateBankRecord").click(function(){
				var id = $(this).attr('rel');
				var deleteFunction = $(this).attr('rel1');
				swal({
					title: 'Do you want to deactivate?',
					text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, deactivate it!'
				},
				function(){
					window.location.href="/admin/"+deleteFunction+"/"+id;
				  });
				  
				});

					//activate Page
			$(".activatePageRecord").click(function(){
				var id = $(this).attr('rel');
				var deleteFunction = $(this).attr('rel1');
				swal({
					title: 'Do you want to activate?',
					text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, deactivate it!'
				},
				function(){
					window.location.href="/admin/"+deleteFunction+"/"+id;
				  });
				  
				});

						//deactivate Page
			$(".deactivatePageRecord").click(function(){
				var id = $(this).attr('rel');
				var deleteFunction = $(this).attr('rel1');
				swal({
					title: 'Do you want to deactivate?',
					text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, deactivate it!'
				},
				function(){
					window.location.href="/admin/"+deleteFunction+"/"+id;
				  });
				  
				});

				//activate ISIN
			$(".activateISINRecord").click(function(){
				var id = $(this).attr('rel');
				var deleteFunction = $(this).attr('rel1');
				swal({
					title: 'Do you want to activate?',
					text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, deactivate it!'
				},
				function(){
					window.location.href="/admin/"+deleteFunction+"/"+id;
				  });
				  
				});

					//deactivate ISIN
			$(".deactivateISINRecord").click(function(){
				var id = $(this).attr('rel');
				var deleteFunction = $(this).attr('rel1');
				swal({
					title: 'Do you want to deactivate?',
					text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, deactivate it!'
				},
				function(){
					window.location.href="/admin/"+deleteFunction+"/"+id;
				  });
				  
				});

				//activate Settlement
			$(".activateSettlementRecord").click(function(){
				var id = $(this).attr('rel');
				var deleteFunction = $(this).attr('rel1');
				swal({
					title: 'Do you want to activate?',
					text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, activate it!'
				},
				function(){
					window.location.href="/admin/"+deleteFunction+"/"+id;
				  });
				  
				});


			//deactivate Settlement
			$(".deactivateSettlementRecord").click(function(){
				var id = $(this).attr('rel');
				var deleteFunction = $(this).attr('rel1');
				swal({
					title: 'Do you want to deactivate?',
					text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, deactivate it!'
				},
				function(){
					window.location.href="/admin/"+deleteFunction+"/"+id;
				  });
				  
				});

				//deactivate CDSC Tariff
			$(".deactivateTariffRecord").click(function(){
				var id = $(this).attr('rel');
				var deleteFunction = $(this).attr('rel1');
				swal({
					title: 'Do you want to deactivate?',
					text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, deactivate it!'
				},
				function(){
					window.location.href="/admin/"+deleteFunction+"/"+id;
				  });
				  
				});

				//activate CDSC Tariff
			$(".activateTariffRecord").click(function(){
				var id = $(this).attr('rel');
				var deleteFunction = $(this).attr('rel1');
				swal({
					title: 'Do you want to activate?',
					text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, deactivate it!'
				},
				function(){
					window.location.href="/admin/"+deleteFunction+"/"+id;
				  });
				  
				});

				//activate downloads
			$(".activateDownloadsRecord").click(function(){
				var id = $(this).attr('rel');
				var deleteFunction = $(this).attr('rel1');
				swal({
					title: 'Do you want to activate?',
					text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, deactivate it!'
				},
				function(){
					window.location.href="/admin/"+deleteFunction+"/"+id;
				  });
				  
				});


				//deactivate downloads
			$(".deactivateDownloadsRecord").click(function(){
				var id = $(this).attr('rel');
				var deleteFunction = $(this).attr('rel1');
				swal({
					title: 'Do you want to deactivate?',
					text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, deactivate it!'
				},
				function(){
					window.location.href="/admin/"+deleteFunction+"/"+id;
				  });
				  
				});


				//delete user
		$(".deleteUserRecord").click(function(){
			var id = $(this).attr('rel');
			var deleteFunction = $(this).attr('rel1');
			swal({
				title: 'Do you want to delete?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			},
			function(){
				window.location.href="/admin/"+deleteFunction+"/"+id;
			  });
			  
			});

		$(".activateUserRecord").click(function(){
				var id = $(this).attr('rel');
				var deleteFunction = $(this).attr('rel1');
				swal({
					title: 'Do you want to activate?',
					text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, deactivate it!'
				},
				function(){
					window.location.href="/admin/"+deleteFunction+"/"+id;
				  });
				  
				});

		$(".deactivateUserRecord").click(function(){
				var id = $(this).attr('rel');
				var deleteFunction = $(this).attr('rel1');
				swal({
					title: 'Do you want to deactivate?',
					text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, deactivate it!'
				},
				function(){
					window.location.href="/admin/"+deleteFunction+"/"+id;
				  });
				  
				});

});
