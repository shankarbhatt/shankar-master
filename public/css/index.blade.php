@include('Home.layouts.header')
@include('Home.layouts.nav')  
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
<!-- jquery section -->
<STYLE>
body, input{
	font-family: Calibri, Arial;
	margin: 0px;
	padding: 0px;
}
a {
	color: #5d1e06
}
a:visited {
	color: #5d1e06
}
#header h2 {
	color: white;
	background-color: ##0254EB;
	margin:0px;
	padding: 5px;
}
.comment {
	background-color: #77302e00;
	margin: 10px;
   
}
a.morelink {
	text-decoration:none;
	outline: none;
}
.morecontent span {
	display: none;

}
</STYLE>

<SCRIPT>
$(document).ready(function() {
	var showChar = 100;
	var ellipsestext = "...";
	var moretext = "more";
	var lesstext = "less";
	$('.more').each(function() {
		var content = $(this).html();

		if(content.length > showChar) {

			var c = content.substr(0, showChar);
			var h = content.substr(showChar-1, content.length - showChar);

			var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="{{url('Home.Notices')}}" class="morelink">'+moretext+'</a></span>';

			$(this).html(html);
		}

	});

// 	$(".morelink").click(function(){
// 		if($(this).hasClass("less")) {
// 			$(this).removeClass("less");
// 			$(this).html(moretext);
// 		} else {
// 			$(this).addClass("less");
// 			$(this).html(lesstext);
// 		}
// 		$(this).parent().prev().toggle();
// 		$(this).prev().toggle();
// 		return false;
// 	});
});
</SCRIPT>

<!-- Slider section              -->
<section>
        <div id="carousel-example" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                                        <li data-target="#carousel-example" data-slide-to="0" class="active"></li>
                                        <li data-target="#carousel-example" data-slide-to="1" class="active"></li>
                            </ol>

             <!-- Wrapper for slides -->
             <div class="carousel-inner" role="listbox">
                                       <div class="item active">
                            <img src="{{asset('images/cdsc.jpg')}}" alt="">
                       </div>
                                           <div class="item ">
                            <img src="{{asset('images/cds1.jpg')}}" alt="">
                       </div>
                                </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="tp-leftarrow tparrows" aria-hidden=""></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="tp-rightarrow tparrows" aria-hidden=""></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>
    <!-- cdsc update -->
    <section style="background-color: #e8e8e845;" class="p-b-40">
        <div class="container">
            <div class="heading_border bg_red"></div>
            <h2 class=" text-left">CDSC UPDATES</h2>
            <div class="col1">
                <div class="grid_full">
                    <div class="tile royal_blue">
                        <div class="tile-content" style="top: 0px;">
                            <div class="content one">
                                <h1>
                                    <a href="{{url('Home.Investor.registered-company')}}">
                                        Registered Companies
                                    </a>
                                </h1>
                            </div>
                            <div class="content two ie7">
                                <p>
                                <a href="{{url('Home.Investor.registered-company')}}">
                                        262                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col2">
                <div class="grid_full">
                    <div class="tile royal_blue">
                        <div class="tile-content" style="top: 0px;">
                            <div class="content one">
                                <h1>
                                    <a href="{{url('Home.Clearing-Member.clearingmember')}}">
                                        Registered Clearing Members
                                    </a>
                                </h1>
                            </div>
                            <div class="content two ie7">
                                <p>
                                <a href="{{url('Home.Clearing-Member.clearingmember')}}">
                                        1                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="col3">
                <div class="grid_full">
                    <div class="tile royal_blue">
                        <div class="tile-content" style="top: 0px;">
                            <div class="content one">
                                <h1>
                                <a href="{{url('Home.Depository-Participants.numberofdp')}}">
                                        Licensed Depository Participants
                                    </a>
                                </h1>
                            </div>
                            <div class="content two ie7">
                                <p>
                                    <a href="{{url('Home.Depository-Participants.numberofdp')}}">
                                        61                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="col4">
                <div class="grid_full">
                    <div class="tile royal_blue">
                        <div class="tile-content" style="top: 0px;">
                            <div class="content one">
                                <h1>
                                    <a href="#">
                                        Beneficial Owners' Demat Account
                                    </a>
                                </h1>
                            </div>
                            <div class="content two ie7">
                                <p>
                                    <a href="#">
                                        5000                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="col5">
                <div class="grid_full">
                    <div class="tile royal_blue">
                        <div class="tile-content" style="top: 0px;">
                            <div class="content one">
                                <h1>
                                    <a href="#">
                                        No. of Demat Shares
                                    </a>
                                </h1>
                            </div>
                            <div class="content two ie7">
                                <p>
                                    <a href="#">
                                        2228726650                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col5">
                <div class="grid_full">
                    <div class="tile royal_blue">
                        <div class="tile-content" style="top: 0px;">
                            <div class="content one">
                                <h1>
                                <a href="{{url('Home.RTA.rtalist')}}">
                                        RTA<br><span>LIST</span>
                                    </a>
                                </h1>
                            </div>
                            <div class="content two ie7">
                                <p>
                                <a href="{{url('Home.RTA.rtalist')}}">
                                        59                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col5">
                <div class="grid_full">
                    <div class="tile royal_blue">
                        <div class="tile-content" style="top: 0px;">
                            <div class="content one">
                                <h1>
                                    <a href="{{url('Home.Investor.settlementid')}}">
                                        SETTLEMENT ID
                                    </a>
                                </h1>
                            </div>
                            <div class="content two ie7">
                                <p>
                                <a href="{{url('Home.Investor.settlementid')}}">
                                        59                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col4">
                <div class="grid_full">
                    <div class="tile royal_blue">
                        <div class="tile-content" style="top: 0px;">
                            <div class="content one">
                                <h1>
                                    <a href="{{url('Home.registerbankincasba')}}">
                                        Registered Banks in C-ASBA
                                    </a>
                                </h1>
                            </div>
                            <div class="content two ie7">
                                <p>
                                <a href="{{url('Home.registerbankincasba')}}">
                                        54                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col5">
                <div class="grid_full">
                    <div class="tile royal_blue">
                        <div class="tile-content" style="top: 0px;">
                            <div class="content one">
                                <h1>
                                    <a href="{{url('Home.registeredDPsinMEROSHARE')}}">
                                        Registered DPs in MEROSHARE
                                    </a>
                                </h1>
                            </div>
                            <div class="content two ie7">
                                <p>
                                <a href="{{url('Home.registeredDPsinMEROSHARE')}}">
                                        65                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
<!-- Latested Notices -->
    <section id="s_services" class="p-t-100 p-b-50">  
          <div class="container">
            <div class="row p-t-40 p-b-40">
                <div class="col-md-8">
                    <div class="heading">
                        <div class="heading_border bg_red"></div>
                        <h2>Latest Notices</h2>
                    </div>
                    <div id="services_slider" class="owl-carousel">
                                                <div class="item">
                            <div class="services ">         
                            <h3 class="text-uppercase">    <a href="{{url('Home.Notices')}}">Your Title</a></h3>
                                <p class="comment more ">Sed ut perspiciatis unde omnis istebhzxgfjzdgfhdjsgdfhjsa  yegrys yge uyyg yeg r7yu yegy gyg  gry natus error sit voluptatem accusantium d#8230;                                <br>
                               
                            </div>
                        </div>
                                            <div class="item">
                            <div class="services">         
                                <h3 class="text-uppercase"><a href="{{url('Home.Notices')}}">What is Lorem Ipsum?</a></h3>
                                <p class="comment more ">Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the&#8230;                                <br>
                                <a href="{{url('Home.Notices.7.html')}}" style="font-size:15px">[READ MORE]</a>
                            </div>
                        </div>
                                            <div class="item">
                            <div class="services">         
                                <h3 class="text-uppercase"><a href="{{url('Home.Notices')}}">What is Lorem Ipsum?</a></h3>
                                <p class="comment more ">Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the&#8230;                                <br>
                               
                            </div>
                        </div>
                                            <div class="item">
                            <div class="services">         
                                <h3 class="text-uppercase"><a href="{{url('Home.Notices')}}">Dummy Text Generator </a></h3>
                                <p class="comment more ">Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the&#8230;                                <br>
                               
                            </div>
                        </div>
                                        </div>
                                        <br>
                                        <a href="{{url('Home.Notices')}}" class="btn-light button-black pill-right">View All </a>
                                    </div>
                <div class="circulars col-md-4 col-sm-4 col-xs-12">
                    <div class="heading">
                        <div class="heading_border bg_red"></div>
                        <h2>Circulars</h2>
                    </div>
                     <div>
                        <div class="over_image"></div>
                        <div class="category_box have_qus m-b-0">
                            <ul class="pro-list">
                                                                    <li> <a href="{{url('uploads.downloads.notice_20740318_annual_fee.pdf')}}" download="notice_20740318_annual_fee.pdf" title="Download">By Laws Sample 2</a></li>
                                                                    <li> <a href="{{url('uploads.downloads.3ISSUER%2c_RTA_and_CDS_agreement.pdf')}}" download="3ISSUER,_RTA_and_CDS_agreement.pdf" title="Download">Sample 3</a></li>
                                                                    <li> <a href="{{url('uploads.downloads.3ISSUER%2c_RTA_and_CDS_agreement.pdf')}}" download="3ISSUER,_RTA_and_CDS_agreement.pdf" title="Download">Sample 2</a></li>
                                
                            </ul>
                            <a href="{{url('Home.Publication.circulars')}}" class="btn-light button-black">View All</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

  

    <!-- Latest News & update Start -->
    <section style="background-color: #e4e3bb8c;" id="latest_news" class="p-b-100 p-t-100 bg_gray">

            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="heading">
                            <div class="heading_border bg_red"></div>
                            <h2>Latest News</h2>
                        </div>
                        <div id="latest_news-slider" class="owl-carousel">
                                                            <div class="item">
                                    <div class="feature_box">
                                        <a href="{{url('Home.news')}}">
                                            <h3>Dummy News Five</h3><br>
                                            <p class="comment more ">CDS and Clearing Limited, a company established under the company act is a company promoted&nbsp;by&#8230;                                        </a>
                                    </div>
                                </div>
                                                                <div class="item">
                                    <div class="feature_box">
                                    <a href="{{url('Home.news')}}">
                                            <h3>Dummy News Three</h3><br>
                                            <p class="comment more ">CDS and Clearing Limited, a company established under the company act is a&#8230;                                        </a>
                                    </div>
                                </div>
                                                                <div class="item">
                                    <div class="feature_box">
                                    <a href="{{url('Home.news')}}">
                                            <h3>Dummy New Two</h3><br>
                                            <p class="comment more ">The main objective of the company is to act as a central depository for various&#8230;                                        </a>
                                    </div>
                                </div>
                                                                <div class="item">
                                    <div class="feature_box">
                                    <a href="{{url('Home.news')}}">
                                            <h3>Dummy News One</h3><br>
                                            <p class="comment more ">The main objective of the company is to act as a central depository for various&#8230;                                        </a>
                                    </div>
                                </div>
                                                                <div class="item">
                                    <div class="feature_box">
                                    <a href="{{url('Home.news')}}">
                                            <h3>Dummy News</h3><br>
                                            <p class="comment more ">The main objective of the company is to act as a central depository for various&#8230;</p>


<p style="text-align: justify;">The main objective&#8230;                                        </a>
                                    </div>
                                </div>
                                                            </div>
                                                        <a href="{{url('Home.news')}}" class="btn-light button-black m-t-40 pill-right">View All </a>
                                                    </div>
                  </div>
              </div>
          </div>
    </section>
    <!-- Latest News & update end -->

    
    <!-- Feature Start -->
    <section id="feature" class="bg_gray feature_slider p-b-40">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                <div class="heading_border bg_red"></div>
                            <h2>Latest Press Release</h2>
                        </div>
                    <div class="row">
                        <div id="latest_news-slider_1" class=" owl-carousel ">
                                                        <div class="item">
                                <div class="feature_box">
                                    <a href="{{('Home.pressrelease1')}} ">
                                        <h4>Some of the facts about the NEPSE</h4><br>
                                        <p class="comment more ">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem&#8230;                                        </p>
                                </a>
                            </div>
                        </div>
                                                    <div class="item">
                                <div class="feature_box">
                                    <a href="{{('Home.pressrelease1')}} ">
                                        <h4>Press Release</h4><br>
                                        <p class="comment more ">Press Release on 2073-09-24 (Please Click to download)                                        </p>
                                </a>
                            </div>
                        </div>
                                                    <div class="item">
                                <div class="feature_box">
                                <a href="{{('Home.pressrelease1')}} ">
                                        <h4>Press Release on 2074-03-16.</h4><br>
                                        <p class="comment more ">Press Release on 2074-03-16                                        </p>
                                </a>
                            </div>
                        </div>
                                                    <div class="item">
                                <div class="feature_box">
                                <a href="{{('Home.pressrelease1')}} ">
                                        <h4>Press Release on 2074-03-20  </h4><br>
                                        <p class="comment more ">Press Release on 2074-03-20 Regarding Multiple Clearing Bank along with Net Settlement                                        </p>
                                </a>
                            </div>
                        </div>
                                            </div>
                </div>
                                <a href="{{url('Home.pressrelease1')}}" class="btn-light button-black">View All </a>
                                
            </div>
        </div>
    </div>
</section>

    <script type="text/javascript">
        $(document).ready(function() {
         
        $('#carouselExampleIndicators').carousel({
          interval: 5000,
          pause: "hover"
        });


       });
   </script>  
   @include('Home.layouts.footer')