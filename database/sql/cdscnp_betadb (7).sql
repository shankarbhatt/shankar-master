-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 07, 2018 at 12:22 AM
-- Server version: 5.6.41
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cdscnp_betadb`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'CDSC Admin', 'admin@cdsc.com.np', '$2y$10$sOao6VMGcTiVGqCpjXMqZ.ZqpJAflbqo4Ne3M2eKCDsYnhXilFBzW', 'ILWxQphlOqhAatIol62FFrNyd7NQkKrWWGpfSyafkz4mPbMJglhFX5brPAHy', NULL, '2018-09-30 01:47:05');

-- --------------------------------------------------------

--
-- Table structure for table `casba_bank`
--

CREATE TABLE `casba_bank` (
  `id` int(11) NOT NULL,
  `bank_id` int(100) NOT NULL,
  `bank_name` varchar(200) CHARACTER SET latin1 NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `casba_bank`
--

INSERT INTO `casba_bank` (`id`, `bank_id`, `bank_name`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 9801, 'United', 0, NULL, '2018-09-20 11:43:51', '0000-00-00 00:00:00'),
(2, 9801, 'United Finance Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(3, 9001, 'Goodwill Finance Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(4, 401, 'Nabil Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(5, 201, 'Rastriya Banijya Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(6, 2601, 'Prabhu Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(7, 1901, 'Global IME Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(8, 2501, 'NMB Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(9, 1801, 'Siddhartha Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(10, 1101, 'Bank of Kathmandu Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(11, 7502, 'Excel Development Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(12, 2701, 'Janata Bank Nepal Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(13, 501, 'Nepal Investment Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(14, 101, 'Nepal Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(15, 4501, 'Sanima Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(16, 9915, 'Guheshwori Merchant Banking Finance Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(17, 1601, 'Kumari Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(18, 2201, 'Sunrise Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(19, 7401, 'Tinau Bikas Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(20, 901, 'Nepal Bangladesh Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(21, 1201, 'Nepal Credit & Commerce Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(22, 6601, 'Garima Bikas Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(23, 9931, 'Mahalaxmi Bikas Bank Limited.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(24, 1501, 'Machhapuchhre Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(25, 8201, 'Srijana Finance Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(26, 1701, 'Laxmi Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(27, 2301, 'NIC Asia Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(28, 9938, 'Jebils Finance Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(29, 3101, 'Century Commercial Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(30, 2101, 'Prime Commercial Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(31, 9912, 'Purnima Bikas Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(32, 2001, 'Citizens Bank International Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(33, 6401, 'Sindhu Bikash Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(34, 5001, 'Deva Bikas Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(35, 701, 'Himalayan Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(36, 9939, 'Reliance Finance Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(37, 6001, 'Jyoti Bikash Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(38, 6701, 'OM Development Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(39, 7201, 'Muktinath Bikas Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(40, 9919, 'ICFC Finance Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(41, 801, 'Nepal SBI Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(42, 7301, 'Shangri-la Development Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(43, 2801, 'Mega Bank Nepal Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(44, 8101, 'Shine Resunga Development Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(45, 301, 'Agriculture Development Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(46, 5401, 'Lumbini Bikas Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(47, 6301, 'Tourism Development Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(48, 8001, 'Kailash Bikas Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(49, 9935, 'Manjushree Financial Institution Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(50, 7515, 'Kankai Bikas Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(51, 8301, 'Gurkhas Finance Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(52, 6801, 'Kamana Sewa Bikas Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(53, 1001, 'Everest Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(54, 9906, 'Central Finance Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(55, 3001, 'CIVIL Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(56, 601, 'Standard Chartered Bank Nepal Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(57, 5901, 'Saptakoshi Development Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(58, 9945, 'Kanchan Development Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(59, 7602, 'Nepal Community Development Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(60, 7514, 'Hamro Bikas Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(61, 7001, 'Gandaki Bikas Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(62, 9905, 'Pokhara Finance Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(63, 9936, 'Mission Development Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(64, 9201, 'Shree Investment & Finance Co. Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00'),
(65, 8401, 'Bhargav Bikash Bank Ltd.', 1, NULL, '2018-09-20 11:52:09', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cdsc_tariff`
--

CREATE TABLE `cdsc_tariff` (
  `id` int(10) UNSIGNED NOT NULL,
  `category` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `cdsc_amount` double NOT NULL,
  `status` tinyint(10) NOT NULL DEFAULT '1' COMMENT '0-Inactive 1-Active 2-Deleted',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cdsc_tariff`
--

INSERT INTO `cdsc_tariff` (`id`, `category`, `title`, `amount`, `cdsc_amount`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, '1', 'Account Opening Fee', 50, 5, 1, NULL, '2018-11-04 14:46:33', '2018-11-04 14:51:10'),
(3, '1', 'Account Operation Annual Fee', 100, 10, 1, NULL, '2018-11-04 14:49:17', '2018-11-04 14:49:17'),
(4, '1', 'Renewal Fee', 50, 5, 1, NULL, '2018-11-04 14:50:41', '2018-11-04 14:50:53'),
(5, '1', 'Mortgage Charge Fee', 50, 5, 1, NULL, '2018-11-04 14:53:04', '2018-11-04 14:53:18'),
(6, '1', 'Account Blocked Fee', 25, 2, 1, NULL, '2018-11-04 14:54:45', '2018-11-04 15:01:50');

-- --------------------------------------------------------

--
-- Table structure for table `cdsc_tariff_category`
--

CREATE TABLE `cdsc_tariff_category` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cdsc_tariff_category`
--

INSERT INTO `cdsc_tariff_category` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'DP', '2018-09-16 13:54:55', '2018-09-16 08:09:55');

-- --------------------------------------------------------

--
-- Table structure for table `clearing_member`
--

CREATE TABLE `clearing_member` (
  `cm_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cm_no` int(50) NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pool_account` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pool_account_dp` int(100) DEFAULT NULL,
  `status` int(10) NOT NULL DEFAULT '1' COMMENT '0-Inactive 1-Active 2-Deleted',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clearing_member`
--

INSERT INTO `clearing_member` (`cm_id`, `name`, `cm_no`, `address`, `pool_account`, `pool_account_dp`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'ABC Securities Pvt.Ltd', 17, 'kathmandu-Indrachowk,Nepal', '1301320000002970', 13200, 1, NULL, NULL, NULL),
(2, 'ARUN SECURITIES  PVT. LTD.', 3, 'PUTALISADAK-32,NEPAL', '1301020000003339', 10200, 1, NULL, NULL, NULL),
(3, 'ARYATARA INVESTMENT AND SECURITIES PVT. LTD.  ARYATARA', 57, 'ANAMNAGAR-KATHMANDU-32,KATHMANDU,NEPAL', '1301190000006167', 11900, 1, NULL, NULL, NULL),
(4, 'ASIAN SECURITIES PRIVATE LIMITED  ASIAN', 26, 'KTM-PUTALISADAK-32,NEPAL', '1301470000050915', 14700, 1, NULL, NULL, NULL),
(5, 'Agrawal Securities Pvt. Ltd.  Securities', 6, 'Kathmandu-Radhemarh - 31, Dillibajar,Nepal', '1301230000001699', 12300, 1, NULL, NULL, NULL),
(6, 'Ashutosh Brokerage & Securities   PVT.LTD', 8, 'Bagmati-Khicha Pokhari,Nepal', '1301110000000859', 11100, 1, NULL, NULL, NULL),
(7, 'BHRIKUTI STOCK BROKING COMPANY  PVT. LTD.', 55, 'NEWROAD-22,NEPAL', '1301020000003172', 10200, 1, NULL, NULL, NULL),
(8, 'CREATIVE  SECURITIES PVT.LTD  .', 40, 'PUTALISADAK-PUTALISADAK,NEPAL', '1301330000048375', 13300, 1, NULL, NULL, NULL),
(9, 'Crystal Kanchanjungha Securities Pvt.Ltd  ', 50, 'Kathmandu-Newplaza-32,Nepal', '1301340000000475', 13400, 1, NULL, NULL, NULL),
(10, 'DAKSHINKALI INVESTMENT AND SECURITIES PVT. LTD.', 33, 'KAMALADI-32-APEX BUILDING,NEPAL', '1301200000002317', 12000, 1, NULL, NULL, NULL),
(11, 'DEEVYA SECURITIES AND STOCK HOUSE PVT. LTD.  DEEVYA', 59, 'KATHMANDU-PUTALISADAK, KATHMANDU,NEPAL', '1301450000000791', 14500, 1, NULL, NULL, NULL),
(12, 'DIPSHIKHA DHITOPATRA KAROBAR COMPANY  PVT. LTD', 38, 'ANAMNAGAR-KA.MA.NA.PA-32,NEPAL', '1301130000003581', 11300, 1, NULL, NULL, NULL),
(13, 'Dynamic Money Managers Security  Pvt. Ltd.', 44, 'Kathmandu-Kamalpokhari-31,Nepal', '1301090000001432', 10900, 1, NULL, NULL, NULL),
(14, 'IMPERIAL SECURITIES COMPANY PRIVATE  LIMITED', 45, 'BAGMATI-ANAMNAGAR-32,NEPAL', '1301310000005588', 13100, 1, NULL, NULL, NULL),
(15, 'Investment Management Nepal  pvt.ltd.', 53, 'Bagmati-Tripureshwor  Ward No.11,Nepal', '1301110000000922', 11100, 1, NULL, NULL, NULL),
(16, 'J. F.  SECURITIES COMPANY PVT.LTD  .', 7, 'PUTALISADAK-32, KAMALADI,NEPAL', '1301020000003552', 10200, 1, NULL, NULL, NULL),
(17, 'KOHINOOR INVESTMENT & SECURITIES PVT. LTD.  kohinoor Investment', 35, 'Kathmandu-Kamalpokhari-32 Kathmandu,Nepal', '1301430000003000', 14300, 1, NULL, NULL, NULL),
(18, 'KUMARI SECURITIES PVT.LTD  KUMARI SECURITIES PV', 1, 'KATHMANDU-DILLIBAZAR - 33,NEPAL', '1301550000070860', 15500, 1, NULL, NULL, NULL),
(19, 'Kalika Securities  Pvt Ltd.', 46, 'Kathmandu-Putalisadak,Nepal', '1301020000002563', 10200, 1, NULL, NULL, NULL),
(20, 'LINCH STOCK MARKET LTD.  LINCH', 41, 'KATHMANDU-NEW BANESHWOR -34,NEPAL', '1301380000001608', 13800, 1, NULL, NULL, NULL),
(21, 'MALLA & MALLA STOCK BROKING CO PVT. LTD  MALLA & MALLA STOCK', 11, 'KATHMANDU-DILLIBAZAR , 33,NEPAL', '1301550000040107', 15500, 1, NULL, NULL, NULL),
(22, 'MIDAS STOCK BROKING CO. PVT. LTD. ', 21, 'KATHMANDU-KAMALADI MOD, PUTALISADAK-31,NEPAL', '1301550000078796', 15500, 1, NULL, NULL, NULL),
(23, 'Market Securities  Exchange Co. (p) Ltd.  ltd.', 5, 'Bagmati-Khicha Pokhari -22,Nepal', '1301110000002837', 11100, 1, NULL, NULL, NULL),
(24, 'NAASA SECURITIES  COMPANY', 58, 'NAXAL-1,NEPAL', '1301020000003149', 10200, 1, NULL, NULL, NULL),
(25, 'NEEV SECURITIES PVT. LTD.  .', 47, 'KATHMANDU-KAMALADI,NEPAL', '1301550000082101', 15500, 1, NULL, NULL, NULL),
(26, 'NEPAL INVESTMENT & SECURITIES TRADING PVT. LTD.  NEPAL INVESTMENT', 19, 'OLD BANESHWOR-BHIMSENGOLA, 34,NEPAL', '1301550000042051', 15500, 1, NULL, NULL, NULL),
(27, 'NEPAL STOCK HOUSE PVT.LTD  NSHPL', 14, 'Kalikasthan, Kathmandu', '1301150000002088', 11500, 1, NULL, NULL, NULL),
(28, 'ONLINE SECURITIES PRIVATE LIMITED  ONLINE', 49, 'KATHMANDU 31-PUTALISADAK,NEPAL', '1301180000003585', 11800, 1, NULL, NULL, NULL),
(29, 'OXFORD SECURITIES  PVT. LTD', 51, 'KALIMATI-13,NEPAL', '1301020000003191', 10200, 1, NULL, NULL, NULL),
(30, 'PRAGYAN SECURITIES PVT.LTD  PRAGYAN SECURITIES P', 10, 'KATHMANDU-KAMLADI  31,NEPAL', '1301550000068678', 15500, 1, NULL, NULL, NULL),
(31, 'PREMIER SECURITIES CO.  LTD.', 32, 'KATHMANDU-PUTALI SADAK,NEPAL', '1301480000007982', 14800, 1, NULL, NULL, NULL),
(32, 'PRIMO SECURITIES  PVT.LTD', 16, 'PUTALISADAK-SHANKARDEVMARG,NEPAL', '1301280000006504', 12800, 1, NULL, NULL, NULL),
(33, 'SAGARMATHA SECURITIES  PVT. LTD.', 18, 'KALIKASTHAN-7,NEPAL', '1301020000021451', 10200, 1, NULL, NULL, NULL),
(34, 'SECURED SECURITIES LTD', 36, 'KATHMANDU-PUTALISADAK,NEPAL', '1301160000002526', 11600, 1, NULL, NULL, NULL),
(35, 'SHREE HARI SECURITIES PVT. LTD.  PVT. LTD.', 56, 'KATHMANDU-HATTISAR ROAD-31, KAMALADI,,NEPAL', '1301400000006441', 14000, 1, NULL, NULL, NULL),
(36, 'SIPLA SECURITIES  PVT. LTD.', 20, 'Kathmandu-Khichapokhari,Nepal', '1301090000002461', 10900, 1, NULL, NULL, NULL),
(37, 'SIPRABI SECURITIES  PVT. LTD.', 22, 'KAPAN-01,NEPAL', '1301020000003571', 10200, 1, NULL, NULL, NULL),
(38, 'SOUTH ASIAN BULLS  PVT. LTD', 43, 'KULESHWOR-WARD NO.14,NEPAL', '1301300000005652', 13000, 1, NULL, NULL, NULL),
(39, 'STOCK BROKER OPAL SECURITIES INVESTMENT  PRIVATE LIMITED', 4, 'KATHMANDU-121 DEVKUMARIMARGA,NEPAL', '1301020000001798', 10200, 1, NULL, NULL, NULL),
(40, 'SWARNALAXMI SECURITIES PVT. LTD.  SWARNALAXMI', 37, 'KATHMANDU-KAMALPOKHARI KATHMANDU-33,NEPAL', '1301080000000051', 10800, 1, NULL, NULL, NULL),
(41, 'SWETA SECURITIES PVT.LTD  SWETA SECURITIES PVT', 25, 'PUTALISADAK-RAMSAHA PATH - 31,NEPAL', '1301550000071450', 15500, 1, NULL, NULL, NULL),
(42, 'Sani Securities Company Limited  Sani', 42, 'Kathmandu-Nachghar,Jamal 30,Nepal', '1301440000001101', 14400, 1, NULL, NULL, NULL),
(43, 'Sewa Securities Pvt. Ltd.  Sewa Securities Pvt.', 54, 'Tripureshwor-Kathmandu-11,Nepal', '1301270000000843', 12700, 1, NULL, NULL, NULL),
(44, 'Shree Krishna Securities  Pvt. Ltd.', 28, 'Kathmandu-Dharmapath, Kathmandu,Nepal', '1301090000001578', 10900, 1, NULL, NULL, NULL),
(45, 'Sumeru Securities Pvt Ltd  sumeru', 39, 'kathmandu-Hatiisar-1,nepal', '1301420000009474', 14200, 1, NULL, NULL, NULL),
(46, 'Sundhara Securities Ltd.  ltd', 52, 'Bagmati-Bagdarbar Marg  Sundhara-11,Nepal', '1301110000002818', 11100, 1, NULL, NULL, NULL),
(47, 'THRIVE BROKERAGE HOUSE PVT. LTD.  THRIVE BROKERAGE', 13, 'KATHMANDU-BACCHA POKHARI, 01,NEPAL', '1301550000049872', 15500, 1, NULL, NULL, NULL),
(48, 'TRISHAKTI SECURITIES PUBLIC LTD.  PUBLIC LTD', 48, 'KATHMANDU-PUTALISADAK,NEPAL', '1301140000003496', 11400, 1, NULL, NULL, NULL),
(49, 'TRISHUL SECURITIES & INVESTMENT  LTD', 29, 'PUTALISADAK-32, NEW PLAZA,NEPAL', '1301020000003081', 10200, 1, NULL, NULL, NULL),
(50, 'VISION SECURITIES  PRIVATE LIMITED', 34, 'PUTALISADAK-KA.MA.NA.PA.-32,NEPAL', '1301350000000607', 13500, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `daily_update`
--

CREATE TABLE `daily_update` (
  `id` int(11) NOT NULL,
  `no_of_bo_account` bigint(200) NOT NULL,
  `no_of_demat_shares` bigint(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daily_update`
--

INSERT INTO `daily_update` (`id`, `no_of_bo_account`, `no_of_demat_shares`, `created_at`, `updated_at`) VALUES
(3, 1305340, 3780069015, '2018-09-13 21:21:01', '2018-09-13 21:21:01'),
(4, 1324102, 3876471705, '2018-09-21 12:24:25', '2018-09-21 12:24:25'),
(5, 1324593, 3876889972, '2018-09-23 15:33:01', '2018-09-23 15:33:01'),
(6, 1325884, 3878792197, '2018-09-27 15:30:34', '2018-09-27 15:30:34'),
(7, 1326401, 3880180960, '2018-09-30 16:02:05', '2018-09-30 16:02:05'),
(8, 1327180, 3882442647, '2018-10-02 17:39:58', '2018-10-02 17:39:58'),
(9, 1328245, 3893503035, '2018-10-05 14:45:11', '2018-10-05 14:45:11'),
(10, 1328638, 3895662088, '2018-10-07 16:33:54', '2018-10-07 16:33:54'),
(11, 1329115, 3897157858, '2018-10-08 15:43:32', '2018-10-08 15:43:32'),
(12, 1329550, 3897814468, '2018-10-09 15:34:32', '2018-10-09 15:34:32'),
(13, 1330224, 3898255138, '2018-10-10 16:23:10', '2018-10-10 16:23:10'),
(14, 1331006, 3899401663, '2018-10-12 14:48:43', '2018-10-12 14:48:43'),
(15, 1331411, 3899614050, '2018-10-14 15:26:19', '2018-10-14 15:26:19'),
(16, 1356543, 3912324894, '2018-11-04 15:44:07', '2018-11-04 15:44:07'),
(17, 1376968, 3947292766, '2018-11-20 17:41:47', '2018-11-20 17:41:47'),
(18, 1399119, 3969716918, '2018-11-30 20:12:20', '2018-11-30 20:12:20'),
(19, 1399851, 3970269175, '2018-12-02 16:03:36', '2018-12-02 16:03:36'),
(20, 1401069, 3971500234, '2018-12-03 16:20:15', '2018-12-03 16:20:15'),
(21, 1404512, 3974495884, '2018-12-05 16:13:01', '2018-12-05 16:13:01'),
(22, 1406192, 3975188471, '2018-12-06 18:03:22', '2018-12-06 18:03:22'),
(23, 1406192, 3975188471, '2018-12-06 18:04:18', '2018-12-06 18:04:18');

-- --------------------------------------------------------

--
-- Table structure for table `death_notice`
--

CREATE TABLE `death_notice` (
  `id` int(11) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `user_type` int(30) NOT NULL,
  `name` varchar(191) NOT NULL,
  `father_name` varchar(200) DEFAULT NULL,
  `grandfather_name` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `bo_account` varchar(100) DEFAULT NULL,
  `file_name` varchar(200) DEFAULT NULL,
  `file` varchar(200) NOT NULL,
  `published_date` date DEFAULT NULL,
  `ending_date` date DEFAULT NULL,
  `verified_by` varchar(100) DEFAULT NULL,
  `status` int(30) NOT NULL DEFAULT '0' COMMENT '0-Pending 1-Verified 2-Rejected 3-Deleted 4-Ended',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `death_notice`
--

INSERT INTO `death_notice` (`id`, `user_id`, `user_type`, `name`, `father_name`, `grandfather_name`, `address`, `bo_account`, `file_name`, `file`, `published_date`, `ending_date`, `verified_by`, `status`, `created_at`, `updated_at`) VALUES
(1, '12300', 3, 'RAI CHAND BAID', NULL, NULL, NULL, NULL, 'death tranfer notice -rai chand baid', 'death_notice_12300-2018-11-15_11-40-57.pdf', '2018-11-15', '2018-12-20', '1', 3, '2018-11-18 11:12:06', NULL),
(2, '12300', 3, 'SUNIL SHRESTHA', NULL, NULL, NULL, NULL, 'death tranfer notice of sunil shrestha', 'death_notice_12300-2018-11-21_07-47-55.pdf', '2018-11-21', '2018-12-27', '1', 3, '2018-11-22 08:32:23', '2018-11-22 19:17:23'),
(3, '10900', 3, 'Gobinda Raj Bista', NULL, NULL, NULL, NULL, 'Hakdabi notice for death transfer', 'death_notice_10900-2018-11-21_08-40-25.pdf', '2018-11-23', '2018-12-28', '6', 3, '2018-11-28 07:13:06', '2018-11-28 17:58:06'),
(4, '12300', 3, 'SUNIL SHRESTHA', 'SANUKAJI SHRESTHA', 'HIRADHAN SHRESTHA', 'BHAKTAPUR- 15', '1301230000013299', 'HAKDABI NOTICE OF SUNIL SHRESTHA', 'death_notice_12300-2018-11-22_05-56-09.pdf', '2018-11-23', '2018-12-28', '1', 3, '2018-11-22 10:26:48', '2018-11-22 21:11:48'),
(5, '10900', 3, 'Gobinda Raj Bista', 'Tara Raj Bista', 'Reshami Raj Bista', 'Kathmandu-26', '1301090000267232', 'Hakdabi notice for death transfer', 'death_notice_10900-2018-11-22_14-42-45.pdf', '2018-11-23', '2018-12-28', '6', 1, '2018-11-23 04:28:39', '2018-11-23 15:13:39'),
(6, '12300', 3, 'SUNIL SHRESTHA', 'SANUKAJI SHRESTHA', 'HIRADHAN SHRESTHA', 'BHAKTAPUR- 15', '1301230000013299', 'HAKDABI NOTICE OF SUNIL SHRESTHA', 'death_notice_12300-2018-11-22_14-48-14.pdf', NULL, NULL, '7', 3, '2018-12-02 08:06:35', '2018-12-02 18:51:35'),
(7, '12300', 3, 'SUNIL SHRESTHA', 'SANUKAJI SHRESTHA', 'HIRADHAN SHRESTHA', 'BHAKTAPUR- 15', '1301230000013299', 'HAKDABI NOTICE OF SUNIL SHRESTHA', 'death_notice_12300-2018-11-25_14-26-28.pdf', '2018-11-26', '2018-12-31', '6', 1, '2018-11-26 06:48:15', '2018-11-26 17:33:15'),
(8, '10900', 3, 'Manmaya Devi Gurung', 'Dil Bahadur Gurung', 'Dev Prasad Gurung', 'Lampatan-1, Kaski', '1301090000579838', 'Hakdabi notice for death transfer', 'death_notice_10900-2018-11-26_15-49-07.pdf', NULL, NULL, '6', 2, '2018-11-27 05:19:32', '2018-11-27 16:04:32'),
(9, '10900', 3, 'Manmaya Devi Gurung', 'Dil Bahadur Gurung', 'Dev Prasad Gurung', 'Lampatan-1, Kaski', '1301090000579838', 'Hakdabi notice for death transfer', 'death_notice_10900-2018-11-27_11-10-11.pdf', '2018-11-28', '2019-01-02', '6', 1, '2018-11-28 07:12:21', '2018-11-28 17:57:21'),
(10, '11700', 3, 'BHAWESH KHANAL', 'NARESH KHANAL', 'SHREEDHAR KHANAL', 'TRIPURESHWOR WN;11', '1301170000144136', 'hakdabi format bhawesh khanal-roji khanal', 'death_notice_11700-2018-11-27_14-17-54.pdf', NULL, NULL, '6', 2, '2018-11-27 08:35:22', '2018-11-27 19:20:22'),
(11, '11700', 3, 'BHAWESH KHANAL', 'NARESH KHANAL', 'SHREEDHAR KHANAL', 'TRIPURESHWOR WN;11', '1301170000144136', 'hakdabi notice bhawesh khanal-roji khanal', 'death_notice_11700-2018-11-27_14-29-17.pdf', '2018-11-30', '2019-01-04', '6', 1, '2018-11-30 05:04:28', '2018-11-30 15:49:28'),
(12, '120', 4, 'tesdfsd', 'fsdfsdf', 'sdfsdfs', 'sdfsdf', NULL, 'sdfsdsdf', 'death_notice_120-2018-11-28_13-23-16.pdf', NULL, NULL, '1', 3, '2018-11-28 07:53:53', '2018-11-28 18:38:53'),
(13, '140', 4, 'Tenzing Lama', 'Man Bahadur Lama', 'Gemba Dorje Lama', 'Biratnagar - 4, Morang', '1301100000885066', 'Notice Pubilcation of Death Transfer', 'death_notice_140-2018-11-28_13-51-55.pdf', NULL, NULL, '7', 3, '2018-12-02 04:48:23', '2018-12-02 15:33:23'),
(14, '140', 4, 'Jagat Bahadur Adhikari', 'Amar Bahadur Adhikari', 'Bir Bahadur Adhikari', 'Resunga Na.Pa. -10 ,Gulmi', '1301100000892067', 'Notice Pubilcation of Death Transfer', 'death_notice_140-2018-11-28_13-54-44.pdf', NULL, NULL, '7', 3, '2018-12-02 04:48:34', '2018-12-02 15:33:34'),
(15, '120', 4, 'Chakra Bahadur Bhujel', 'Santa Bahadur Bhujel', 'Hasta Bahadur Bhujel', 'Ward No.6, Hariwan, Sarlahi', NULL, 'Chakra Bdr Bhujel', 'death_notice_120-2018-11-28_14-28-29.pdf', '2018-11-30', '2019-01-04', '7', 1, '2018-11-30 04:51:05', '2018-11-30 15:36:05'),
(16, '10600', 3, 'RADIUM LAMICHHANE', 'GOPAL LAMICHHANE', 'CHUDAMANI LAMICHHANE', 'KALANKI,KATHMANDU', '1301060000119494', 'RADIUM DEATH TRF', 'death_notice_10600-2018-11-29_12-18-26.pdf', NULL, NULL, '7', 3, '2018-12-02 04:47:53', '2018-12-02 15:32:53'),
(17, '142', 4, 'Ashok Shrestha', 'Gopal Lal SHrestha', 'Janak Lal Shrestha', 'Hetauda 4', '1301090000054321', NULL, 'death_notice_142-2018-11-29_15-37-22.pdf', NULL, NULL, '6', 2, '2018-11-30 04:21:41', '2018-11-30 15:06:41'),
(18, '112', 4, 'Rajendra Prasad Ulak', 'Bel Prasad Ulak', 'Bhakta Lal Ulak', 'Khopasi 1, Kavre', '1301510000118329', NULL, 'death_notice_112-2018-11-29_15-44-40.pdf', NULL, NULL, '7', 2, '2018-12-02 06:39:20', '2018-12-02 17:24:20'),
(19, '101', 4, 'Santu Ghimire', 'Tikaram Adhikari', 'Netrapani Ghimire', '3/42 Bharatpokhari,Kaski', NULL, NULL, 'death_notice_101-19_2018-12-04_09-56-46.pdf', '2018-12-04', '2019-01-08', '7', 1, '2018-12-05 05:34:34', '2018-12-05 15:38:10'),
(20, '160', 4, 'BINOD NIDHI TIWARI', 'BATU KRISHNA TIWARI', 'TEJ NIDHI TIWARI', 'KTM-33,DILLIBAZAR', '49773', NULL, 'death_notice_160-2018-11-30_12-27-48.pdf', NULL, NULL, '7', 2, '2018-11-30 06:52:57', '2018-11-30 17:37:57'),
(21, '160', 4, 'CHAKRA BAHADUR BHUJEL', 'SANTA BAHADUR BHUJEL', 'HASTA BAHADUR BHUJEL', 'HARIBAN, SARLAHI', NULL, NULL, 'death_notice_160-2018-11-30_12-29-50.pdf', '2018-12-02', '2019-01-06', '7', 1, '2018-12-02 05:45:25', '2018-12-02 16:30:25'),
(22, '160', 4, 'BINOD NIDHI TIWARI', 'BATU KRISHNA TIWARI', 'TEJ NIDHI TIWARI', 'KTM-33,DILLIBAZAR', NULL, NULL, 'death_notice_160-2018-11-30_12-43-55.pdf', '2018-12-02', '2019-01-06', '7', 1, '2018-12-02 05:45:01', '2018-12-02 16:30:01'),
(23, '140', 4, 'Tenzing Lama', 'Man Bahadur Lama', 'Gemba Dorje Lama', 'Biratnagar -4 ,Morang', '1301100000885066', NULL, 'death_notice_140-2018-11-30_13-57-15.pdf', NULL, NULL, '7', 3, '2018-12-02 08:06:27', '2018-12-02 18:51:27'),
(24, '140', 4, 'Jagat Bahadur Adhikari', 'Tilak Ram Subedi', 'Nanda Ram Subedi', 'Resunga Na.Pa.- 10 .Gulmi', '1301100000892067', NULL, 'death_notice_140-2018-11-30_14-01-19.pdf', NULL, NULL, '7', 3, '2018-12-02 08:06:18', '2018-12-02 18:51:18'),
(25, '140', 4, 'Tenzing Lama', 'Man Bahadur Lama', 'Gemba Dorje Lama', 'Biratnagar -4 ,Morang', '1301100000885066', NULL, 'death_notice_140-2018-11-30_15-22-26.pdf', '2018-12-02', '2019-01-06', '7', 1, '2018-12-02 04:55:38', '2018-12-02 15:40:38'),
(26, '140', 4, 'Jagat Bahadur Adhikari', 'Amar Bahadur Adhikari', 'Bir Bahadur Adhikari', 'Resunga Na.Pa.- 10 .Gulmi', '1301100000892067', NULL, 'death_notice_140-2018-11-30_15-24-09.pdf', '2018-12-02', '2019-01-06', '7', 1, '2018-12-02 04:54:03', '2018-12-02 15:39:03'),
(27, '10900', 3, 'Kumar Silwal', 'Pitamber Silwal', 'Chetnath Silwal', 'Lalitpur 21', '1301090000057598', NULL, 'death_notice_10900-2018-12-02_10-30-16.pdf', '2018-12-04', '2019-01-08', '7', 1, '2018-12-04 07:25:01', '2018-12-04 18:10:01'),
(28, '120', 4, 'Bal Narayan Manandhar', 'Buddhi Narayan Manandhar', 'Laxmi das Manandhar', '13,kalimati kathmandu', NULL, NULL, 'death_notice_120-2018-12-02_11-02-54.pdf', NULL, NULL, '7', 2, '2018-12-02 05:42:39', '2018-12-02 16:27:39'),
(29, '120', 4, 'Shiba Prasad Agrawal', 'Baluram Agrawal', 'Soji Ram Agrawal', '2,Sunsari dharan', NULL, NULL, 'death_notice_120-2018-12-02_11-06-19.pdf', NULL, NULL, '7', 2, '2018-12-02 05:42:47', '2018-12-02 16:27:47'),
(30, '120', 4, 'Bal Narayan Manandhar', 'Buddhi Narayan Manandhar', 'Laxmi Das Manandhar', '13 Kalimati,Kathmandu', NULL, NULL, 'death_notice_120-2018-12-02_12-20-25.pdf', '2018-12-02', '2019-01-06', '7', 1, '2018-12-02 09:36:06', '2018-12-02 20:21:06'),
(31, '120', 4, 'Shiba Prasad Agrawal', 'Baluram Agrawal', 'Sojiram Agrawal', '2,Sunsari Dharan', NULL, NULL, 'death_notice_120-2018-12-02_12-23-26.pdf', '2018-12-02', '2019-01-06', '7', 1, '2018-12-02 09:52:39', '2018-12-02 20:37:39'),
(32, '112', 4, 'Rajendra Prasad Ulak', 'Bel Prasad Ulak', 'Bhakta Lal Ulak', 'Khopasi 1, Kavre', '1301510000118329', NULL, 'death_notice_112-2018-12-02_14-54-35.pdf', '2018-12-05', '2019-01-09', '7', 1, '2018-12-05 05:17:16', '2018-12-05 16:02:16'),
(33, '10900', 3, 'Phadindra Parajuli', 'Shiva Prasad Jamarkatel', 'Keshav Prasad Parajuli', 'Bhaktapur 3', '1301090000094497', NULL, 'death_notice_10900-2018-12-02_14-56-52.pdf', '2018-12-04', '2019-01-08', '7', 1, '2018-12-04 06:17:28', '2018-12-04 17:02:28'),
(34, '10900', 3, 'Keshav Prasad Parajuli', 'Ambika Prasad Parajuli', 'Bishnulal Parajuli', 'Bhaktapur 3', '1301090000094292', NULL, 'death_notice_10900-2018-12-02_14-59-53.pdf', '2018-12-04', '2019-01-08', '7', 1, '2018-12-04 06:17:43', '2018-12-04 17:02:43'),
(35, '10600', 3, 'RADIUM LAMICHHANE', 'GOPAL LAMICHHANE', 'CHUDAMANI LAMICHHANE', 'KALANKI,KATHMANDU', '1301060000119494', NULL, 'death_notice_10600-2018-12-02_15-43-59.pdf', NULL, NULL, '7', 2, '2018-12-06 10:30:25', '2018-12-06 21:15:25'),
(36, '10600', 3, 'RADIUM LAMICHHANE', 'GOPAL LAMICHHANE', 'CHUDAMANI LAMICHHANE', 'KALANKI,KATHMANDU', '1301060000119494', NULL, 'death_notice_10600-2018-12-02_15-44-01.pdf', NULL, NULL, NULL, 0, '2018-12-02 20:44:01', NULL),
(37, '10600', 3, 'KALPANA GYAWALI', 'BHAKTA BAHADUR ADHIKARI', 'SADAKHAR ADHIKARI', 'PRAGATINAGAR NAWALPARASI', '1301060000163166', NULL, 'death_notice_10600-2018-12-02_16-51-25.pdf', '2018-12-06', '2019-01-10', '7', 1, '2018-12-06 06:27:04', '2018-12-06 17:12:04'),
(38, '10600', 3, 'HARI HAR LAL SHRESTHA', 'JAGGA NATH SHRESTHA', 'HARI LAL SHRESTHA', 'DHARMAPATH-22 KATHMANDU', '1301060000330771', NULL, 'death_notice_10600-2018-12-02_17-01-07.pdf', '2018-12-06', '2019-01-10', '7', 1, '2018-12-06 05:55:03', '2018-12-06 16:40:03'),
(39, '10600', 3, 'AMBIKA HADA', 'BISHNU SHANKER PRADHAN', 'JIT MAN PRADHAN', 'GAHAWA-6 PARSA', '1301060000986311', NULL, 'death_notice_10600-2018-12-02_17-06-11.pdf', NULL, NULL, '7', 2, '2018-12-06 10:29:28', '2018-12-06 21:14:28'),
(40, '10200', 3, 'GAURAV SHRESTHA', 'SHANTA GOPAL SHRESTHA', 'BHOLAMAN SHRESTHA', 'RATOPUL-07, KATHMANDU', '1301020000018552', NULL, 'death_notice_10200-2018-12-02_17-14-50.pdf', '2018-12-06', '2019-01-10', '7', 1, '2018-12-06 06:03:02', '2018-12-06 16:48:02'),
(41, '10200', 3, 'KAILASH GURUNG', 'NARA BAHADUR GURUNG', 'SHER BAHADUR GURUNG', 'CHHINAMAKHU-04, BHOJPUR', '1301020000007576', NULL, 'death_notice_10200-2018-12-02_17-19-33.pdf', NULL, NULL, NULL, 0, '2018-12-02 22:19:33', NULL),
(42, '142', 4, 'Kirshna Prasad Regmi Sharma', 'Bijuli Prasad Regmi', 'Baburam Regmi', 'Kathmandu-3', NULL, NULL, 'death_notice_142-2018-12-03_11-52-43.pdf', '2018-12-05', '2019-01-09', '7', 1, '2018-12-05 06:07:03', '2018-12-05 16:52:03'),
(43, '10400', 3, 'Anupama Sharma', 'Govinda Prasad Acharya', 'Damodar Prasad Acharya', 'Maitidevi-30, Kathmandu', '1301040000030458', NULL, 'death_notice_10400-2018-12-03_14-11-18.pdf', NULL, NULL, '7', 2, '2018-12-05 09:36:11', '2018-12-05 20:21:11'),
(44, '15000', 3, 'Shear Bahadur khatri', 'Padam Bahadur khatri', 'Man Bahadur khatri', 'Suechatar-01,kathmandu', '1301500000024077', NULL, 'death_notice_15000-2018-12-03_14-14-04.pdf', NULL, NULL, '7', 2, '2018-12-06 10:49:41', '2018-12-06 21:34:41'),
(45, '123', 4, 'Chakra Bahadur Bhujel', 'Santa Bahadur Bhujel', 'Hasta Bahadur Bhujel', 'Hariwan, Sarlahi', 'NA', NULL, 'death_notice_123-2018-12-03_16-19-03.pdf', NULL, NULL, '7', 2, '2018-12-06 10:36:25', '2018-12-06 21:21:25'),
(46, '123', 4, 'Daman Man Singh Basnet', 'Lila Man Singh Basnet', 'Sher Man Singh Basnet', 'Dillibazar, Kathmandu', 'NA', NULL, 'death_notice_123-2018-12-03_16-41-52.pdf', NULL, NULL, '7', 2, '2018-12-06 10:36:32', '2018-12-06 21:21:32'),
(47, '14800', 3, 'BUDHI LAL SHAH', 'DEVI LAL SHAH TELI', 'BILAT SHAH TELI', 'SKHUWANANKARKTI MAUWAHI 1 SIRAHA', '1301480000075521', NULL, 'death_notice_14800-2018-12-03_16-46-36.pdf', NULL, NULL, '7', 3, '2018-12-04 06:28:31', '2018-12-04 17:13:31'),
(48, '101', 4, 'Kamala Debi Yadav', 'Ram Phal Yadav', 'Janak Lal Yadav', '2,Deuri Bharuwa, Saptari', NULL, NULL, 'death_notice_101-2018-12-04_09-33-59.pdf', '2018-12-05', '2019-01-09', '7', 1, '2018-12-05 04:43:03', '2018-12-05 15:28:03'),
(49, '101', 4, 'Paspati Debi Singh', 'Mahabir Singh', 'Ram Surat Singh', '13,Birjung,Parsa', NULL, NULL, 'death_notice_101-2018-12-04_09-37-11.pdf', '2018-12-05', '2019-01-09', '7', 1, '2018-12-05 06:36:23', '2018-12-05 16:32:29'),
(50, '101', 4, 'Budhani Shahu', 'Mukat Shahu Sudi', 'Bindu Lal Shahu Sudi', '2,Kanchanpur,saptari', NULL, NULL, 'death_notice_101-2018-12-04_09-41-48.pdf', '2018-12-05', '2019-01-09', '7', 1, '2018-12-05 06:35:31', '2018-12-05 16:27:31'),
(51, '101', 4, 'Yagyashwor Shah', 'Dirgha Bahadur Shah', 'Prithvi Bahadur Shah', '1,Nepalgunj', NULL, NULL, 'death_notice_101-2018-12-04_09-47-11.pdf', '2018-12-05', '2019-01-09', '7', 1, '2018-12-05 04:47:37', '2018-12-05 15:32:37'),
(52, '101', 4, 'Jit Bahadur Magar', 'Dhan Bahadur Magar', 'Shamser Bahadur Magar', '6,Nijgad,Bara', NULL, NULL, 'death_notice_101-2018-12-04_09-49-40.pdf', '2018-12-05', '2019-01-09', '7', 1, '2018-12-05 04:48:52', '2018-12-05 15:33:52'),
(53, '101', 4, 'Ram Prasad Shrestha', 'Ratna Bahadur Shrestha', 'Krishna Lal Shrestha', '1,Ilam', NULL, NULL, 'death_notice_101-2018-12-04_09-51-38.pdf', '2018-12-05', '2019-01-09', '7', 1, '2018-12-05 04:50:24', '2018-12-05 15:35:24'),
(54, '101', 4, 'Yek Dev Paudel', 'Modh Nath Paudel', 'Hari Prasad', '7,Sundar Bajar,Lamjung', NULL, NULL, 'death_notice_101-2018-12-04_09-54-25.pdf', '2018-12-05', '2019-01-09', '7', 1, '2018-12-05 04:51:32', '2018-12-05 15:36:32'),
(55, '101', 4, 'Hom Kumari Koirala', 'Bhim Prasad Dahal', 'Shri Belas Dahal', '5,Biratnagar Morang', NULL, NULL, 'death_notice_101-2018-12-04_10-00-37.pdf', '2018-12-05', '2019-01-09', '7', 1, '2018-12-05 04:54:39', '2018-12-05 15:39:39'),
(56, '101', 4, 'Narayan Krishna Shrestha', 'Nuchhe Lal Shrestha', 'Shiba Lal Shrestha', '3,Maharajgunj', NULL, NULL, 'death_notice_101-2018-12-04_10-05-39.pdf', '2018-12-05', '2019-01-09', '7', 1, '2018-12-05 04:55:57', '2018-12-05 15:40:57'),
(57, '14800', 3, 'BUDHI LAL SHAH', 'DEVI LAL SHAH TELI', 'BILAT SHAH TELI', 'SKHUWANANKARKTI -1 MAUWAHI SIRAHA', '1301480000075521', NULL, 'death_notice_14800-2018-12-04_10-52-02.pdf', NULL, NULL, NULL, 0, '2018-12-04 15:52:02', NULL),
(58, '153', 4, 'Daman Man Singh Basnet', 'Lila Man Singh Basnet', 'Sher Man Singh Basnet', 'Dillibazar,KTM Ga 2/48', NULL, NULL, 'death_notice_153-2018-12-04_12-30-59.pdf', '2018-12-05', '2019-01-09', '7', 1, '2018-12-05 06:31:35', '2018-12-05 17:16:35'),
(59, '14100', 3, 'Uttar Bahadur Singh Suwal', 'Krishna Bahadur Singh Suwal', 'Kancha Bahadur suwal', 'Kathmandu 17, Kathmandu', '1301480000009050', NULL, 'death_notice_14100-2018-12-04_17-30-45.pdf', NULL, NULL, '7', 2, '2018-12-05 04:37:45', '2018-12-05 15:22:45'),
(60, '10400', 3, 'Anupama Sharma', 'Govinda Prasad Acharya', 'Damodar Prasad Acharya', 'Maitidevi-30, Kathmandu', '1301040000030458', NULL, 'death_notice_10400-2018-12-05_16-09-45.pdf', NULL, NULL, '7', 2, '2018-12-06 05:13:05', '2018-12-06 15:58:05'),
(61, '101', 4, 'Maiya Shrestha', 'Hira Bahadur Shrestha', 'Ram Bahadur Shrestha', '10,Biratnagar,Morang', NULL, NULL, 'death_notice_101-2018-12-05_17-14-11.pdf', '2018-12-06', '2019-01-10', '7', 1, '2018-12-06 06:30:31', '2018-12-06 17:15:31'),
(62, '10400', 3, 'Anupama Sharma', 'Govinda Prasad Acharya', 'Damodar Prasad Acharya', 'Maitidevi-30, Kathmandu', '1301040000030458', NULL, 'death_notice_10400-2018-12-06_11-04-32.pdf', '2018-12-06', '2019-01-10', '7', 1, '2018-12-06 05:22:12', '2018-12-06 16:07:12'),
(63, '142', 4, 'Ishwari Prasad Timilsina', 'Lilanath Sharma', 'Hari Lal Timilsina', 'Bharatpur-10', NULL, NULL, 'death_notice_142-2018-12-06_11-39-03.pdf', '2018-12-06', '2019-01-10', '7', 1, '2018-12-06 08:06:48', '2018-12-06 18:51:48'),
(64, '142', 4, 'Rabi Prasad Timilsina', 'Ishwari Prasad TImilsina', 'Lilanath Timilsina', 'Bharatpur-10', NULL, NULL, 'death_notice_142-2018-12-06_11-51-46.pdf', '2018-12-06', '2019-01-10', '7', 1, '2018-12-06 08:05:30', '2018-12-06 18:50:30'),
(65, '142', 4, 'Poti Tamang', 'Singhbir Tamang', 'Bire Tamang', 'Manakamana-7', NULL, NULL, 'death_notice_142-2018-12-06_11-55-32.pdf', NULL, NULL, '7', 2, '2018-12-06 10:14:28', '2018-12-06 20:59:28'),
(66, '10900', 3, 'Ishwari Prasad Timilsina', 'Lilanath Sharma', 'Hari Lal Timilsina', 'Bharatpur-10', '1301090000453563', NULL, 'death_notice_10900-2018-12-06_13-37-39.pdf', '2018-12-06', '2019-01-10', '7', 1, '2018-12-06 10:23:29', '2018-12-06 21:08:29');

-- --------------------------------------------------------

--
-- Table structure for table `death_notice_log`
--

CREATE TABLE `death_notice_log` (
  `id` int(11) NOT NULL,
  `notice_id` int(50) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `user_type` int(30) NOT NULL,
  `name` varchar(200) NOT NULL,
  `father_name` varchar(200) DEFAULT NULL,
  `grandfather_name` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `bo_account` varchar(100) DEFAULT NULL,
  `file_name` varchar(200) DEFAULT NULL,
  `file` varchar(200) NOT NULL,
  `published_date` date DEFAULT NULL,
  `ending_date` date DEFAULT NULL,
  `verified_by` varchar(100) DEFAULT NULL,
  `request` int(10) DEFAULT NULL COMMENT '1-add 2-edit 3-delete',
  `status` int(10) NOT NULL DEFAULT '0' COMMENT '0-Pending 1-Verified 2-Rejected 3-Deleted 4-Ended',
  `isVerified` int(10) NOT NULL DEFAULT '0' COMMENT '0-Pending 1-Verified ',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `death_notice_log`
--

INSERT INTO `death_notice_log` (`id`, `notice_id`, `user_id`, `user_type`, `name`, `father_name`, `grandfather_name`, `address`, `bo_account`, `file_name`, `file`, `published_date`, `ending_date`, `verified_by`, `request`, `status`, `isVerified`, `created_at`, `updated_at`) VALUES
(1, 1, '12300', 3, 'RAI CHAND BAID', NULL, NULL, NULL, NULL, 'death tranfer notice -rai chand baid', 'death_notice_12300-2018-11-15_11-40-57.pdf', '2018-11-15', '2018-12-20', '1', 1, 2, 1, '2018-11-18 08:19:36', '2018-11-15 16:40:57'),
(2, 1, '12300', 3, 'RAI CHAND BAID', NULL, NULL, NULL, NULL, 'death tranfer notice -rai chand baid', 'death_notice_12300-2018-11-15_11-40-57.pdf', '2018-11-15', '2018-12-20', '1', 3, 3, 1, '2018-11-18 11:19:04', '2018-11-18 14:11:55'),
(3, 2, '12300', 3, 'SUNIL SHRESTHA', NULL, NULL, NULL, NULL, 'death tranfer notice of sunil shrestha', 'death_notice_12300-2018-11-21_07-47-55.pdf', '2018-11-21', '2018-12-27', '1', 1, 2, 1, '2018-11-21 11:31:46', '2018-11-21 12:47:55'),
(4, 3, '10900', 3, 'Gobinda Raj Bista', NULL, NULL, NULL, NULL, 'Hakdabi notice for death transfer', 'death_notice_10900-2018-11-21_08-40-25.pdf', '2018-11-23', '2018-12-28', '1', 1, 2, 1, '2018-11-22 09:00:49', '2018-11-22 19:45:49'),
(5, 2, '12300', 3, 'SUNIL SHRESTHA', NULL, NULL, NULL, NULL, 'death tranfer notice of sunil shrestha', 'death_notice_12300-2018-11-21_07-47-55.pdf', '2018-11-21', '2018-12-27', '1', 3, 3, 1, '2018-11-22 08:32:23', '2018-11-22 19:17:23'),
(6, 4, '12300', 3, 'SUNIL SHRESTHA', 'SANUKAJI SHRESTHA', 'HIRADHAN SHRESTHA', 'BHAKTAPUR- 15', '1301230000013299', 'HAKDABI NOTICE OF SUNIL SHRESTHA', 'death_notice_12300-2018-11-22_05-56-09.pdf', '2018-11-23', '2018-12-28', '1', 1, 2, 1, '2018-11-22 09:00:31', '2018-11-22 19:45:31'),
(7, 5, '10900', 3, 'Gobinda Raj Bista', 'Tara Raj Bista', 'Reshami Raj Bista', 'Kathmandu-26', '1301090000267232', 'Hakdabi notice for death transfer', 'death_notice_10900-2018-11-22_14-42-45.pdf', '2018-11-23', '2018-12-28', '6', 1, 1, 1, '2018-11-23 04:28:39', '2018-11-23 15:13:39'),
(8, 6, '12300', 3, 'SUNIL SHRESTHA', 'SANUKAJI SHRESTHA', 'HIRADHAN SHRESTHA', 'BHAKTAPUR- 15', '1301230000013299', 'HAKDABI NOTICE OF SUNIL SHRESTHA', 'death_notice_12300-2018-11-22_14-48-14.pdf', NULL, NULL, '1', 1, 2, 1, '2018-11-25 11:16:33', '2018-11-25 22:01:33'),
(9, 4, '12300', 3, 'SUNIL SHRESTHA', 'SANUKAJI SHRESTHA', 'HIRADHAN SHRESTHA', 'BHAKTAPUR- 15', '1301230000013299', 'HAKDABI NOTICE OF SUNIL SHRESTHA', 'death_notice_12300-2018-11-22_05-56-09.pdf', '2018-11-23', '2018-12-28', '1', 3, 3, 1, '2018-11-22 10:26:48', '2018-11-22 21:11:48'),
(10, 6, '12300', 3, 'SUNIL SHRESTHA', 'SANUKAJI SHRESTHA', 'HIRADHAN SHRESTHA', 'BHAKTAPUR- 15', '1301230000013299', 'HAKDABI NOTICE OF SUNIL SHRESTHA', 'death_notice_12300-2018-11-22_14-48-14.pdf', NULL, NULL, '7', 3, 3, 1, '2018-12-02 08:06:35', '2018-12-02 18:51:35'),
(11, 7, '12300', 3, 'SUNIL SHRESTHA', 'SANUKAJI SHRESTHA', 'HIRADHAN SHRESTHA', 'BHAKTAPUR- 15', '1301230000013299', 'HAKDABI NOTICE OF SUNIL SHRESTHA', 'death_notice_12300-2018-11-25_14-26-28.pdf', '2018-11-26', '2018-12-31', '6', 1, 1, 1, '2018-11-26 06:48:15', '2018-11-26 17:33:15'),
(12, 8, '10900', 3, 'Manmaya Devi Gurung', 'Dil Bahadur Gurung', 'Dev Prasad Gurung', 'Lampatan-1, Kaski', '1301090000579838', 'Hakdabi notice for death transfer', 'death_notice_10900-2018-11-26_15-49-07.pdf', NULL, NULL, '6', 1, 2, 1, '2018-11-27 05:19:32', '2018-11-27 16:04:32'),
(13, 3, '10900', 3, 'Gobinda Raj Bista', NULL, NULL, NULL, NULL, 'Hakdabi notice for death transfer', 'death_notice_10900-2018-11-21_08-40-25.pdf', '2018-11-23', '2018-12-28', '6', 3, 3, 1, '2018-11-28 07:13:06', '2018-11-28 17:58:06'),
(14, 9, '10900', 3, 'Manmaya Devi Gurung', 'Dil Bahadur Gurung', 'Dev Prasad Gurung', 'Lampatan-1, Kaski', '1301090000579838', 'Hakdabi notice for death transfer', 'death_notice_10900-2018-11-27_11-10-11.pdf', '2018-11-28', '2019-01-02', '6', 1, 1, 1, '2018-11-28 07:12:21', '2018-11-28 17:57:21'),
(15, 10, '11700', 3, 'BHAWESH KHANAL', 'NARESH KHANAL', 'SHREEDHAR KHANAL', 'TRIPURESHWOR WN;11', '1301170000144136', 'hakdabi format bhawesh khanal-roji khanal', 'death_notice_11700-2018-11-27_14-17-54.pdf', NULL, NULL, '6', 1, 2, 1, '2018-11-27 08:35:22', '2018-11-27 19:20:22'),
(16, 11, '11700', 3, 'BHAWESH KHANAL', 'NARESH KHANAL', 'SHREEDHAR KHANAL', 'TRIPURESHWOR WN;11', '1301170000144136', 'hakdabi notice bhawesh khanal-roji khanal', 'death_notice_11700-2018-11-27_14-29-17.pdf', '2018-11-30', '2019-01-04', '6', 1, 1, 1, '2018-11-30 05:04:28', '2018-11-30 15:49:28'),
(17, 12, '120', 4, 'tesdfsd', 'fsdfsdf', 'sdfsdfs', 'sdfsdf', NULL, 'sdfsdsdf', 'death_notice_120-2018-11-28_13-23-16.pdf', NULL, NULL, '1', 1, 2, 1, '2018-11-28 07:42:38', '2018-11-28 18:27:38'),
(18, 12, '120', 4, 'aaaaa', 'bbbbbb', 'ccccccccc', 'dddddddd', NULL, 'eeeeeeeeee', 'death_notice_120-12_2018-11-28_13-27-17.pdf', NULL, NULL, '1', 2, 2, 1, '2018-11-28 07:42:38', '2018-11-28 18:27:38'),
(19, 12, '120', 4, 'tesdfsd', 'fsdfsdf', 'sdfsdfs', 'sdfsdf', NULL, 'sdfsdsdf', 'death_notice_120-2018-11-28_13-23-16.pdf', NULL, NULL, '1', 3, 3, 1, '2018-11-28 07:53:53', '2018-11-28 18:38:53'),
(20, 13, '140', 4, 'Tenzing Lama', 'Man Bahadur Lama', 'Gemba Dorje Lama', 'Biratnagar - 4, Morang', '1301100000885066', 'Notice Pubilcation of Death Transfer', 'death_notice_140-2018-11-28_13-51-55.pdf', NULL, NULL, '7', 1, 2, 1, '2018-11-30 05:06:00', '2018-11-30 15:51:00'),
(21, 14, '140', 4, 'Jagat Bahadur Adhikari', 'Amar Bahadur Adhikari', 'Bir Bahadur Adhikari', 'Resunga Na.Pa. -10 ,Gulmi', '1301100000892067', 'Notice Pubilcation of Death Transfer', 'death_notice_140-2018-11-28_13-54-44.pdf', NULL, NULL, '7', 1, 2, 1, '2018-11-30 05:06:20', '2018-11-30 15:51:20'),
(22, 15, '120', 4, 'Chakra Bahadur Bhujel', 'Santa Bahadur Bhujel', 'Hasta Bahadur Bhujel', 'Ward No.6, Hariwan, Sarlahi', NULL, 'Chakra Bdr Bhujel', 'death_notice_120-2018-11-28_14-28-29.pdf', '2018-11-30', '2019-01-04', '7', 1, 1, 1, '2018-11-30 04:51:05', '2018-11-30 15:36:05'),
(23, 16, '10600', 3, 'RADIUM LAMICHHANE', 'GOPAL LAMICHHANE', 'CHUDAMANI LAMICHHANE', 'KALANKI,KATHMANDU', '1301060000119494', 'RADIUM DEATH TRF', 'death_notice_10600-2018-11-29_12-18-26.pdf', NULL, NULL, '6', 1, 2, 1, '2018-11-29 08:25:35', '2018-11-29 19:10:35'),
(24, 17, '142', 4, 'Ashok Shrestha', 'Gopal Lal SHrestha', 'Janak Lal Shrestha', 'Hetauda 4', '1301090000054321', NULL, 'death_notice_142-2018-11-29_15-37-22.pdf', NULL, NULL, '6', 1, 2, 1, '2018-11-30 04:21:41', '2018-11-30 15:06:41'),
(25, 18, '112', 4, 'Rajendra Prasad Ulak', 'Bel Prasad Ulak', 'Bhakta Lal Ulak', 'Khopasi 1, Kavre', '1301510000118329', NULL, 'death_notice_112-2018-11-29_15-44-40.pdf', NULL, NULL, '7', 1, 2, 1, '2018-12-02 06:39:20', '2018-12-02 17:24:20'),
(26, 16, '10600', 3, 'RADIUM LAMICHHANE', 'GOPAL LAMICHHANE', 'CHUDAMANI LAMICHHANE', 'KALANKI,KATHMANDU', '1301060000119494', NULL, 'death_notice_10600-2018-11-29_12-18-26.pdf', NULL, NULL, '7', 3, 3, 1, '2018-12-02 04:47:53', '2018-12-02 15:32:53'),
(27, 19, '101', 4, 'Santu Ghimire', 'Tikaram Adhikari', 'Netrapani Ghimire', '3/42 Bharatpokhari,Kaski', '1301090000677413', NULL, 'death_notice_101-2018-11-30_11-52-36.pdf', '2018-11-30', '2019-01-04', '7', 1, 1, 1, '2018-12-05 05:32:24', '2018-12-05 15:38:10'),
(28, 20, '160', 4, 'BINOD NIDHI TIWARI', 'BATU KRISHNA TIWARI', 'TEJ NIDHI TIWARI', 'KTM-33,DILLIBAZAR', '49773', NULL, 'death_notice_160-2018-11-30_12-27-48.pdf', NULL, NULL, '7', 1, 2, 1, '2018-12-02 06:07:21', '2018-12-02 16:52:21'),
(29, 21, '160', 4, 'CHAKRA BAHADUR BHUJEL', 'SANTA BAHADUR BHUJEL', 'HASTA BAHADUR BHUJEL', 'HARIBAN, SARLAHI', NULL, NULL, 'death_notice_160-2018-11-30_12-29-50.pdf', '2018-12-02', '2019-01-06', '7', 1, 1, 1, '2018-12-02 05:45:25', '2018-12-02 16:30:25'),
(30, 20, '160', 4, 'BINOD NIDHI TIWARI', 'BATU KRISHNA TIWARI', 'TEJ NIDHI TIWARI', 'KTM-33,DILLIBAZAR', NULL, NULL, 'death_notice_160-2018-11-30_12-27-48.pdf', NULL, NULL, '7', 2, 2, 1, '2018-12-02 06:07:21', '2018-12-02 16:52:21'),
(31, 22, '160', 4, 'BINOD NIDHI TIWARI', 'BATU KRISHNA TIWARI', 'TEJ NIDHI TIWARI', 'KTM-33,DILLIBAZAR', NULL, NULL, 'death_notice_160-2018-11-30_12-43-55.pdf', '2018-12-02', '2019-01-06', '7', 1, 1, 1, '2018-12-02 05:45:01', '2018-12-02 16:30:01'),
(32, 23, '140', 4, 'Tenzing Lama', 'Man Bahadur Lama', 'Gemba Dorje Lama', 'Biratnagar -4 ,Morang', '1301100000885066', NULL, 'death_notice_140-2018-11-30_13-57-15.pdf', NULL, NULL, '7', 1, 2, 1, '2018-12-02 04:56:03', '2018-12-02 15:41:03'),
(33, 24, '140', 4, 'Jagat Bahadur Adhikari', 'Tilak Ram Subedi', 'Nanda Ram Subedi', 'Resunga Na.Pa.- 10 .Gulmi', '1301100000892067', NULL, 'death_notice_140-2018-11-30_14-01-19.pdf', NULL, NULL, '7', 1, 2, 1, '2018-12-02 04:56:10', '2018-12-02 15:41:10'),
(34, 13, '140', 4, 'Tenzing Lama', 'Man Bahadur Lama', 'Gemba Dorje Lama', 'Biratnagar - 4, Morang', '1301100000885066', NULL, 'death_notice_140-2018-11-28_13-51-55.pdf', NULL, NULL, '7', 3, 3, 1, '2018-12-02 04:48:23', '2018-12-02 15:33:23'),
(35, 14, '140', 4, 'Jagat Bahadur Adhikari', 'Amar Bahadur Adhikari', 'Bir Bahadur Adhikari', 'Resunga Na.Pa. -10 ,Gulmi', '1301100000892067', NULL, 'death_notice_140-2018-11-28_13-54-44.pdf', NULL, NULL, '7', 3, 3, 1, '2018-12-02 04:48:34', '2018-12-02 15:33:34'),
(36, 25, '140', 4, 'Tenzing Lama', 'Man Bahadur Lama', 'Gemba Dorje Lama', 'Biratnagar -4 ,Morang', '1301100000885066', NULL, 'death_notice_140-2018-11-30_15-22-26.pdf', '2018-12-02', '2019-01-06', '7', 1, 1, 1, '2018-12-02 04:55:38', '2018-12-02 15:40:38'),
(37, 26, '140', 4, 'Jagat Bahadur Adhikari', 'Amar Bahadur Adhikari', 'Bir Bahadur Adhikari', 'Resunga Na.Pa.- 10 .Gulmi', '1301100000892067', NULL, 'death_notice_140-2018-11-30_15-24-09.pdf', '2018-12-02', '2019-01-06', '7', 1, 1, 1, '2018-12-02 04:54:03', '2018-12-02 15:39:03'),
(38, 27, '10900', 3, 'Kumar Silwal', 'Pitamber Silwal', 'Chetnath Silwal', 'Lalitpur 21', '1301090000057598', NULL, 'death_notice_10900-2018-12-02_10-30-16.pdf', '2018-12-04', '2019-01-08', '7', 1, 1, 1, '2018-12-04 07:25:01', '2018-12-04 18:10:01'),
(39, 28, '120', 4, 'Bal Narayan Manandhar', 'Buddhi Narayan Manandhar', 'Laxmi das Manandhar', '13,kalimati kathmandu', NULL, NULL, 'death_notice_120-2018-12-02_11-02-54.pdf', NULL, NULL, '7', 1, 2, 1, '2018-12-02 05:42:39', '2018-12-02 16:27:39'),
(40, 29, '120', 4, 'Shiba Prasad Agrawal', 'Baluram Agrawal', 'Soji Ram Agrawal', '2,Sunsari dharan', NULL, NULL, 'death_notice_120-2018-12-02_11-06-19.pdf', NULL, NULL, '7', 1, 2, 1, '2018-12-02 05:42:47', '2018-12-02 16:27:47'),
(41, 30, '120', 4, 'Bal Narayan Manandhar', 'Buddhi Narayan Manandhar', 'Laxmi Das Manandhar', '13 Kalimati,Kathmandu', NULL, NULL, 'death_notice_120-2018-12-02_12-20-25.pdf', '2018-12-02', '2019-01-06', '7', 1, 1, 1, '2018-12-02 09:36:06', '2018-12-02 20:21:06'),
(42, 31, '120', 4, 'Shiba Prasad Agrawal', 'Baluram Agrawal', 'Sojiram Agrawal', '2,Sunsari Dharan', NULL, NULL, 'death_notice_120-2018-12-02_12-23-26.pdf', '2018-12-02', '2019-01-06', '7', 1, 1, 1, '2018-12-02 09:52:39', '2018-12-02 20:37:39'),
(43, 6, '12300', 3, 'SUNIL SHRESTHA', 'SANUKAJI SHRESTHA', 'HIRADHAN SHRESTHA', 'BHAKTAPUR- 15', '1301230000013299', NULL, 'death_notice_12300-2018-11-22_14-48-14.pdf', NULL, NULL, '7', 3, 3, 1, '2018-12-02 08:06:35', '2018-12-02 18:51:35'),
(44, 24, '140', 4, 'Jagat Bahadur Adhikari', 'Tilak Ram Subedi', 'Nanda Ram Subedi', 'Resunga Na.Pa.- 10 .Gulmi', '1301100000892067', NULL, 'death_notice_140-2018-11-30_14-01-19.pdf', NULL, NULL, '7', 3, 3, 1, '2018-12-02 08:06:18', '2018-12-02 18:51:18'),
(45, 23, '140', 4, 'Tenzing Lama', 'Man Bahadur Lama', 'Gemba Dorje Lama', 'Biratnagar -4 ,Morang', '1301100000885066', NULL, 'death_notice_140-2018-11-30_13-57-15.pdf', NULL, NULL, '7', 3, 3, 1, '2018-12-02 08:06:27', '2018-12-02 18:51:27'),
(46, 32, '112', 4, 'Rajendra Prasad Ulak', 'Bel Prasad Ulak', 'Bhakta Lal Ulak', 'Khopasi 1, Kavre', '1301510000118329', NULL, 'death_notice_112-2018-12-02_14-54-35.pdf', '2018-12-05', '2019-01-09', '7', 1, 1, 1, '2018-12-05 05:17:16', '2018-12-05 16:02:16'),
(47, 33, '10900', 3, 'Phadindra Parajuli', 'Shiva Prasad Jamarkatel', 'Keshav Prasad Parajuli', 'Bhaktapur 3', '1301090000094497', NULL, 'death_notice_10900-2018-12-02_14-56-52.pdf', '2018-12-04', '2019-01-08', '7', 1, 1, 1, '2018-12-04 06:17:28', '2018-12-04 17:02:28'),
(48, 34, '10900', 3, 'Keshav Prasad Parajuli', 'Ambika Prasad Parajuli', 'Bishnulal Parajuli', 'Bhaktapur 3', '1301090000094292', NULL, 'death_notice_10900-2018-12-02_14-59-53.pdf', '2018-12-04', '2019-01-08', '7', 1, 1, 1, '2018-12-04 06:17:43', '2018-12-04 17:02:43'),
(49, 35, '10600', 3, 'RADIUM LAMICHHANE', 'GOPAL LAMICHHANE', 'CHUDAMANI LAMICHHANE', 'KALANKI,KATHMANDU', '1301060000119494', NULL, 'death_notice_10600-2018-12-02_15-43-59.pdf', NULL, NULL, '7', 1, 2, 1, '2018-12-06 10:30:25', '2018-12-06 21:15:25'),
(50, 36, '10600', 3, 'RADIUM LAMICHHANE', 'GOPAL LAMICHHANE', 'CHUDAMANI LAMICHHANE', 'KALANKI,KATHMANDU', '1301060000119494', NULL, 'death_notice_10600-2018-12-02_15-44-01.pdf', NULL, NULL, NULL, 1, 0, 0, '2018-12-02 20:44:01', '2018-12-02 20:44:01'),
(51, 37, '10600', 3, 'KALPANA GYAWALI', 'BHAKTA BAHADUR ADHIKARI', 'SADAKHAR ADHIKARI', 'PRAGATINAGAR NAWALPARASI', '1301060000163166', NULL, 'death_notice_10600-2018-12-02_16-51-25.pdf', '2018-12-06', '2019-01-10', '7', 1, 1, 1, '2018-12-06 06:27:04', '2018-12-06 17:12:04'),
(52, 38, '10600', 3, 'HARI HAR LAL SHRESTHA', 'JAGGA NATH SHRESTHA', 'HARI LAL SHRESTHA', 'DHARMAPATH-22 KATHMANDU', '1301060000330771', NULL, 'death_notice_10600-2018-12-02_17-01-07.pdf', '2018-12-06', '2019-01-10', '7', 1, 1, 1, '2018-12-06 05:55:03', '2018-12-06 16:40:03'),
(53, 39, '10600', 3, 'AMBIKA HADA', 'BISHNU SHANKER PRADHAN', 'JIT MAN PRADHAN', 'GAHAWA-6 PARSA', '1301060000986311', NULL, 'death_notice_10600-2018-12-02_17-06-11.pdf', NULL, NULL, '7', 1, 2, 1, '2018-12-06 10:29:28', '2018-12-06 21:14:28'),
(54, 40, '10200', 3, 'GAURAV SHRESTHA', 'SHANTA GOPAL SHRESTHA', 'BHOLAMAN SHRESTHA', 'RATOPUL-07, KATHMANDU', '1301020000018552', NULL, 'death_notice_10200-2018-12-02_17-14-50.pdf', '2018-12-06', '2019-01-10', '7', 1, 1, 1, '2018-12-06 06:03:02', '2018-12-06 16:48:02'),
(55, 41, '10200', 3, 'KAILASH GURUNG', 'NARA BAHADUR GURUNG', 'SHER BAHADUR GURUNG', 'CHHINAMAKHU-04, BHOJPUR', '1301020000007576', NULL, 'death_notice_10200-2018-12-02_17-19-33.pdf', NULL, NULL, NULL, 1, 0, 0, '2018-12-02 22:19:33', '2018-12-02 22:19:33'),
(56, 42, '142', 4, 'Kirshna Prasad Regmi Sharma', 'Bijuli Prasad Regmi', 'Baburam Regmi', 'Kathmandu-3', NULL, NULL, 'death_notice_142-2018-12-03_11-52-43.pdf', '2018-12-05', '2019-01-09', '7', 1, 1, 1, '2018-12-05 06:07:03', '2018-12-05 16:52:03'),
(57, 43, '10400', 3, 'Anupama Sharma', 'Govinda Prasad Acharya', 'Damodar Prasad Acharya', 'Maitidevi-30, Kathmandu', '1301040000030458', NULL, 'death_notice_10400-2018-12-03_14-11-18.pdf', NULL, NULL, '7', 1, 2, 1, '2018-12-05 09:36:11', '2018-12-05 20:21:11'),
(58, 44, '15000', 3, 'Shear Bahadur khatri', 'Padam Bahadur khatri', 'Man Bahadur khatri', 'Suechatar-01,kathmandu', '1301500000024077', NULL, 'death_notice_15000-2018-12-03_14-14-04.pdf', NULL, NULL, '7', 1, 2, 1, '2018-12-06 10:49:41', '2018-12-06 21:34:41'),
(59, 45, '123', 4, 'Chakra Bahadur Bhujel', 'Santa Bahadur Bhujel', 'Hasta Bahadur Bhujel', 'Hariwan, Sarlahi', 'NA', NULL, 'death_notice_123-2018-12-03_16-19-03.pdf', NULL, NULL, '7', 1, 2, 1, '2018-12-06 10:36:25', '2018-12-06 21:21:25'),
(60, 46, '123', 4, 'Daman Man Singh Basnet', 'Lila Man Singh Basnet', 'Sher Man Singh Basnet', 'Dillibazar, Kathmandu', 'NA', NULL, 'death_notice_123-2018-12-03_16-41-52.pdf', NULL, NULL, '7', 1, 2, 1, '2018-12-06 10:36:32', '2018-12-06 21:21:32'),
(61, 47, '14800', 3, 'BUDHI LAL SHAH', 'DEVI LAL SHAH TELI', 'BILAT SHAH TELI', 'SKHUWANANKARKTI MAUWAHI 1 SIRAHA', '1301480000075521', NULL, 'death_notice_14800-2018-12-03_16-46-36.pdf', NULL, NULL, '7', 1, 2, 1, '2018-12-04 04:51:20', '2018-12-04 15:36:20'),
(62, 48, '101', 4, 'Kamala Debi Yadav', 'Ram Phal Yadav', 'Janak Lal Yadav', '2,Deuri Bharuwa, Saptari', NULL, NULL, 'death_notice_101-2018-12-04_09-33-59.pdf', '2018-12-05', '2019-01-09', '7', 1, 1, 1, '2018-12-05 04:43:03', '2018-12-05 15:28:03'),
(63, 49, '101', 4, 'Paspati Debi Singh', 'Mahabir Singh', 'Ram Surat Singh', '13,Birjung,Parsa', NULL, NULL, 'death_notice_101-2018-12-04_09-37-11.pdf', NULL, NULL, '7', 1, 2, 1, '2018-12-05 06:33:20', '2018-12-05 16:32:29'),
(64, 50, '101', 4, 'Budhuni Shahu', 'Mukat Shahu Sudi', 'Bindu Lal Shahu Sudi', '2,Kanchanpur,saptari', NULL, NULL, 'death_notice_101-2018-12-04_09-41-48.pdf', NULL, NULL, '7', 1, 2, 1, '2018-12-05 06:34:39', '2018-12-05 16:27:31'),
(65, 51, '101', 4, 'Yagyashwor Shah', 'Dirgha Bahadur Shah', 'Prithvi Bahadur Shah', '1,Nepalgunj', NULL, NULL, 'death_notice_101-2018-12-04_09-47-11.pdf', '2018-12-05', '2019-01-09', '7', 1, 1, 1, '2018-12-05 04:47:37', '2018-12-05 15:32:37'),
(66, 52, '101', 4, 'Jit Bahadur Magar', 'Dhan Bahadur Magar', 'Shamser Bahadur Magar', '6,Nijgad,Bara', NULL, NULL, 'death_notice_101-2018-12-04_09-49-40.pdf', '2018-12-05', '2019-01-09', '7', 1, 1, 1, '2018-12-05 04:48:52', '2018-12-05 15:33:52'),
(67, 53, '101', 4, 'Ram Prasad Shrestha', 'Ratna Bahadur Shrestha', 'Krishna Lal Shrestha', '1,Ilam', NULL, NULL, 'death_notice_101-2018-12-04_09-51-38.pdf', '2018-12-05', '2019-01-09', '7', 1, 1, 1, '2018-12-05 04:50:24', '2018-12-05 15:35:24'),
(68, 54, '101', 4, 'Yek Dev Paudel', 'Modh Nath Paudel', 'Hari Prasad', '7,Sundar Bajar,Lamjung', NULL, NULL, 'death_notice_101-2018-12-04_09-54-25.pdf', '2018-12-05', '2019-01-09', '7', 1, 1, 1, '2018-12-05 04:51:32', '2018-12-05 15:36:32'),
(69, 19, '101', 4, 'Santu Ghimire', 'Tikaram Adhikari', 'Netrapani Ghimire', '3/42 Bharatpokhari,Kaski', NULL, NULL, 'death_notice_101-19_2018-12-04_09-56-46.pdf', '2018-12-04', '2019-01-08', '7', 2, 1, 1, '2018-12-05 05:34:11', '2018-12-05 15:38:10'),
(70, 55, '101', 4, 'Hom Kumari Koirala', 'Bhim Prasad Dahal', 'Shri Belas Dahal', '5,Biratnagar Morang', NULL, NULL, 'death_notice_101-2018-12-04_10-00-37.pdf', '2018-12-05', '2019-01-09', '7', 1, 1, 1, '2018-12-05 04:54:39', '2018-12-05 15:39:39'),
(71, 56, '101', 4, 'Narayan Krishna Shrestha', 'Nuchhe Lal Shrestha', 'Shiba Lal Shrestha', '3,Maharajgunj', NULL, NULL, 'death_notice_101-2018-12-04_10-05-39.pdf', '2018-12-05', '2019-01-09', '7', 1, 1, 1, '2018-12-05 04:55:57', '2018-12-05 15:40:57'),
(72, 57, '14800', 3, 'BUDHI LAL SHAH', 'DEVI LAL SHAH TELI', 'BILAT SHAH TELI', 'SKHUWANANKARKTI -1 MAUWAHI SIRAHA', '1301480000075521', NULL, 'death_notice_14800-2018-12-04_10-52-02.pdf', NULL, NULL, NULL, 1, 0, 0, '2018-12-04 15:52:02', '2018-12-04 15:52:02'),
(73, 47, '14800', 3, 'BUDHI LAL SHAH', 'DEVI LAL SHAH TELI', 'BILAT SHAH TELI', 'SKHUWANANKARKTI MAUWAHI 1 SIRAHA', '1301480000075521', NULL, 'death_notice_14800-2018-12-03_16-46-36.pdf', NULL, NULL, '7', 3, 3, 1, '2018-12-04 06:28:31', '2018-12-04 17:13:31'),
(74, 58, '153', 4, 'Daman Man Singh Basnet', 'Lila Man Singh Basnet', 'Sher Man Singh Basnet', 'Dillibazar,KTM Ga 2/48', NULL, NULL, 'death_notice_153-2018-12-04_12-30-59.pdf', '2018-12-05', '2019-01-09', '7', 1, 1, 1, '2018-12-05 06:31:35', '2018-12-05 17:16:35'),
(75, 59, '14100', 3, 'Uttar Bahadur Singh Suwal', 'Krishna Bahadur Singh Suwal', 'Kancha Bahadur suwal', 'Kathmandu 17, Kathmandu', '1301480000009050', NULL, 'death_notice_14100-2018-12-04_17-30-45.pdf', NULL, NULL, '7', 1, 2, 1, '2018-12-05 04:37:45', '2018-12-05 15:22:45'),
(76, 50, '101', 4, 'Budhani Shahu', 'Mukat Shahu Sudi', 'Bindu Lal Shahu Sudi', '2,Kanchanpur,saptari', NULL, NULL, 'death_notice_101-2018-12-04_09-41-48.pdf', '2018-12-05', '2019-01-09', '7', 2, 1, 1, '2018-12-05 06:34:53', '2018-12-05 16:27:31'),
(77, 49, '101', 4, 'Paspati Debi Singh', 'Mahabir Singh', 'Ram Surat Singh', '13,Birjung,Parsa', NULL, NULL, 'death_notice_101-2018-12-04_09-37-11.pdf', '2018-12-05', '2019-01-00', '7', 2, 1, 1, '2018-12-05 06:33:53', '2018-12-05 16:32:29'),
(78, 60, '10400', 3, 'Anupama Sharma', 'Govinda Prasad Acharya', 'Damodar Prasad Acharya', 'Maitidevi-30, Kathmandu', '1301040000030458', NULL, 'death_notice_10400-2018-12-05_16-09-45.pdf', NULL, NULL, '7', 1, 2, 1, '2018-12-06 05:13:05', '2018-12-06 15:58:05'),
(79, 61, '101', 4, 'Maiya Shrestha', 'Hira Bahadur Shrestha', 'Ram Bahadur Shrestha', '10,Biratnagar,Morang', NULL, NULL, 'death_notice_101-2018-12-05_17-14-11.pdf', '2018-12-06', '2019-01-10', '7', 1, 1, 1, '2018-12-06 06:30:31', '2018-12-06 17:15:31'),
(80, 62, '10400', 3, 'Anupama Sharma', 'Govinda Prasad Acharya', 'Damodar Prasad Acharya', 'Maitidevi-30, Kathmandu', '1301040000030458', NULL, 'death_notice_10400-2018-12-06_11-04-32.pdf', '2018-12-06', '2019-01-10', '7', 1, 1, 1, '2018-12-06 05:22:12', '2018-12-06 16:07:12'),
(81, 63, '142', 4, 'Ishwari Prasad Timilsina', 'Lilanath Sharma', 'Hari Lal Timilsina', 'Bharatpur-10', NULL, NULL, 'death_notice_142-2018-12-06_11-39-03.pdf', '2018-12-06', '2019-01-10', '7', 1, 1, 1, '2018-12-06 08:06:48', '2018-12-06 18:51:48'),
(82, 64, '142', 4, 'Rabi Prasad Timilsina', 'Ishwari Prasad TImilsina', 'Lilanath Timilsina', 'Bharatpur-10', NULL, NULL, 'death_notice_142-2018-12-06_11-51-46.pdf', '2018-12-06', '2019-01-10', '7', 1, 1, 1, '2018-12-06 08:05:30', '2018-12-06 18:50:30'),
(83, 65, '142', 4, 'Poti Tamang', 'Singhbir Tamang', 'Bire Tamang', 'Manakamana-7', NULL, NULL, 'death_notice_142-2018-12-06_11-55-32.pdf', NULL, NULL, '7', 1, 2, 1, '2018-12-06 10:14:28', '2018-12-06 20:59:28'),
(84, 66, '10900', 3, 'Ishwari Prasad Timilsina', 'Lilanath Sharma', 'Hari Lal Timilsina', 'Bharatpur-10', '1301090000453563', NULL, 'death_notice_10900-2018-12-06_13-37-39.pdf', '2018-12-06', '2019-01-10', '7', 1, 1, 1, '2018-12-06 10:23:29', '2018-12-06 21:08:29');

-- --------------------------------------------------------

--
-- Table structure for table `downloads`
--

CREATE TABLE `downloads` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(50) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0-Inactive 1-Active 2-Deleted',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `downloads`
--

INSERT INTO `downloads` (`id`, `title`, `file_name`, `file`, `type`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 'CASBA_Directive_2074', 'CASBA_Directive_2074', '2018_09_11_07_33_02_CASBA_Directive_2074.pdf', 1, 1, NULL, '2018-09-11 05:48:02', '2018-09-11 05:48:02'),
(3, 'CDS_Byelaws_2068_2nd_Revise_2073_(Revised_Byelaws)', 'CDS_Byelaws_2068_2nd_Revise_2073_(Revised_Byelaws)', '2018_09_11_07_33_47_CDS_Byelaws_2068_2nd_Revise_2073_(Revised_Byelaws).pdf', 1, 1, NULL, '2018-09-11 05:48:47', '2018-09-11 05:48:47'),
(4, 'CDSC_FAQ_(Nepali)', 'CDSC_FAQ_(Nepali)', '2018_09_11_07_34_05_CDSC_FAQ_(Nepali).pdf', 1, 1, NULL, '2018-09-11 05:49:05', '2018-09-11 05:49:05'),
(5, 'CDSC_FAQ_English', 'CDSC_FAQ_English', '2018_09_11_07_34_36_CDSC_FAQ_English.pdf', 1, 1, NULL, '2018-09-11 05:49:36', '2018-09-11 05:49:36'),
(6, 'CDSCAnnualReportFY07172Nepali', 'CDSCAnnualReportFY07172Nepali', '2018_09_11_07_35_25_CDSCAnnualReportFY07172Nepali.pdf', 1, 1, NULL, '2018-09-11 05:50:25', '2018-09-11 05:50:25'),
(7, 'CDSCAnnualReportFY07374Nepali', 'CDSCAnnualReportFY07374Nepali', '2018_09_11_07_35_55_CDSCAnnualReportFY07374Nepali.pdf', 1, 1, NULL, '2018-09-11 05:50:55', '2018-09-11 05:50:55'),
(8, 'CDSServiceRegulation_en', 'CDSServiceRegulation_en', '2018_09_11_07_36_13_CDSServiceRegulation_en.pdf', 1, 1, NULL, '2018-09-11 05:51:13', '2018-09-11 05:51:13'),
(9, 'CNS FAQ', 'CNS FAQ', '2018_09_11_07_36_35_CNS FAQ.pdf', 1, 1, NULL, '2018-09-11 05:51:35', '2018-09-11 05:51:35'),
(10, 'CNS_Byelaws_2069_3rd_Revise_2073(Revised_Byelaws)', 'CNS_Byelaws_2069_3rd_Revise_2073(Revised_Byelaws)', '2018_09_11_07_36_56_CNS_Byelaws_2069_3rd_Revise_2073(Revised_Byelaws).pdf', 1, 1, NULL, '2018-09-11 05:51:56', '2018-09-11 05:51:56'),
(11, 'meroshare_manual_2074_nepali_version', 'meroshare_manual_2074_nepali_version', '2018_09_11_07_37_28_meroshare_manual_2074_nepali_version.pdf', 1, 1, NULL, '2018-09-11 05:52:28', '2018-09-11 05:52:28'),
(12, 'mode_of_operation_for_demat', 'mode_of_operation_for_demat', '2018_09_11_07_37_59_mode_of_operation_for_demat.pdf', 1, 1, NULL, '2018-09-11 05:52:59', '2018-09-11 05:52:59'),
(13, 'mode_of_operations_related_to_physical_securities_settlement2071', 'mode_of_operations_related_to_physical_securities_settlement2071', '2018_09_11_07_38_31_mode_of_operations_related_to_physical_securities_settlement2071.pdf', 1, 1, NULL, '2018-09-11 05:53:31', '2018-09-11 05:53:31'),
(14, 'SecuritiesAct2063', 'SecuritiesAct2063', '2018_09_11_07_38_52_SecuritiesAct2063.pdf', 1, 1, NULL, '2018-09-11 05:53:52', '2018-09-11 05:53:52'),
(15, 'circular regarding Demat Request Number \r\n (DRN) (2071-10-15)', 'circular_20711015', '2018_09_11_07_46_37_circular_20711015.pdf', 2, 1, NULL, '2018-09-11 06:01:37', '2018-09-11 06:01:37'),
(16, 'circular regarding DRF Number (2071-10-26)', 'circular_20711027', '2018_09_11_07_46_57_circular_20711027.pdf', 2, 1, NULL, '2018-09-11 06:01:57', '2018-09-11 06:01:57'),
(17, 'circular regarding additional share dematerialisation (2071-12-09)', 'circular_20711209', '2018_09_11_07_47_37_circular_20711209.pdf', 2, 1, NULL, '2018-09-11 06:02:37', '2018-09-11 06:02:37'),
(18, 'circular regarding client wise pay out and balance enquiry module (2071-12-27)', 'circular_20711227', '2018_09_11_07_48_09_circular_20711227.pdf', 2, 1, NULL, '2018-09-11 06:03:09', '2018-09-11 06:03:09'),
(19, 'circular regarding dematerialisation (2072-09-20)', 'circular_2071092001', '2018_09_11_07_48_32_circular_2071092001.pdf', 2, 1, NULL, '2018-09-11 06:03:32', '2018-09-11 06:03:32'),
(20, 'circular regarding instruction slip (2071-09-20)', 'circular_2071092002', '2018_09_11_07_48_54_circular_2071092002.pdf', 2, 1, NULL, '2018-09-11 06:03:54', '2018-09-11 06:03:54'),
(21, 'circular regarding transaction fee (2072-01-02)', 'circular_20720102', '2018_09_11_07_49_15_circular_20720102.pdf', 2, 1, NULL, '2018-09-11 06:04:15', '2018-09-11 06:04:15'),
(22, 'circular regarding start of official work (2072-01-25)', 'circular_20720125', '2018_09_11_07_49_36_circular_20720125.pdf', 2, 1, NULL, '2018-09-11 06:04:36', '2018-09-11 06:04:36'),
(23, 'circular regarding Demat Marking (2072-02-02)', 'circular_20720222', '2018_09_11_07_49_54_circular_20720222.pdf', 2, 1, NULL, '2018-09-11 06:04:54', '2018-09-11 06:04:54'),
(24, 'circular_regarding clearing and settlement (2072-03-08)', 'circular_20720308', '2018_09_11_07_50_17_circular_20720308.pdf', 2, 1, NULL, '2018-09-11 06:05:17', '2018-09-11 06:05:17'),
(25, 'circular regarding transaction fee (2072-05-28)', 'circular_20720528', '2018_09_11_07_50_35_circular_20720528.pdf', 2, 1, NULL, '2018-09-11 06:05:35', '2018-09-11 06:05:35'),
(26, 'circular regarding charge for correction (2072-06-05)', 'circular_20720605', '2018_09_11_07_50_52_circular_20720605.pdf', 2, 1, NULL, '2018-09-11 06:05:52', '2018-09-11 06:05:52'),
(27, 'circular regarding valuation of demat shares (2072-06-06)', 'circular_20720606', '2018_09_11_07_51_09_circular_20720606.pdf', 2, 1, NULL, '2018-09-11 06:06:09', '2018-09-11 06:06:09'),
(28, 'circular regarding EOD Time (2073-07-19)', 'circular_207374_08', '2018_09_11_07_52_04_circular_207374_08.pdf', 2, 1, NULL, '2018-09-11 06:07:04', '2018-09-11 06:07:04'),
(29, 'circular_regarding_transaction_fee (2073-09-26) ', 'circular_207374_10_transaction_fee', '2018_09_11_07_52_32_circular_207374_10_transaction_fee.pdf', 2, 1, NULL, '2018-09-11 06:07:32', '2018-09-11 06:07:32'),
(30, 'circular_regarding_beneficial_owner_bank_account (2073-09-26)', 'circular_207374_11_bo_bank_account', '2018_09_11_07_52_53_circular_207374_11_bo_bank_account.pdf', 2, 1, NULL, '2018-09-11 06:07:53', '2018-09-11 06:07:53'),
(31, 'circular_regarding_corporate_action (2073-10-26)', 'circular_207374_12_corporate_action', '2018_09_11_07_53_07_circular_207374_12_corporate_action.pdf', 2, 1, NULL, '2018-09-11 06:08:07', '2018-09-11 06:08:07'),
(32, 'circular regarding fee and details(2074-02-31)', 'circular_207374_16', '2018_09_11_07_53_25_circular_207374_16.pdf', 2, 1, NULL, '2018-09-11 06:08:25', '2018-09-11 06:08:25'),
(33, 'circular_regarding_online_service_meroshare_work_procedure (2074-03-07)', 'circular_207374_17_online_service_meroshare_work_procedure', '2018_09_11_07_53_46_circular_207374_17_online_service_meroshare_work_procedure.pdf', 2, 1, NULL, '2018-09-11 06:08:46', '2018-09-11 06:08:46'),
(34, 'circular regarding instruction slip(2073-05-14)', 'circular_20730514', '2018_09_11_07_54_00_circular_20730514.pdf', 2, 1, NULL, '2018-09-11 06:09:00', '2018-09-11 06:09:00'),
(35, 'circular regarding clearing and settlement time (2073-05-24)', 'circular_20730524', '2018_09_11_07_54_18_circular_20730524.pdf', 2, 1, NULL, '2018-09-11 06:09:18', '2018-09-11 06:09:18'),
(36, 'circular regarding annual charge (2073-07-03)', 'circular_20730703', '2018_09_11_07_54_40_circular_20730703.pdf', 2, 1, NULL, '2018-09-11 06:09:40', '2018-09-11 06:09:40'),
(37, 'circular regarding BO-BO transfer (2073-06-06)', 'circular regarding BO-BO transfer (2073-06-06)', '2018_09_11_07_55_28_circular_2073060601.pdf', 2, 1, NULL, '2018-09-11 06:10:28', '2018-09-11 06:10:28'),
(38, 'circular regarding client mapping (2073-06-02)', 'circular_2073060602', '2018_09_11_07_55_45_circular_2073060602.pdf', 2, 1, NULL, '2018-09-11 06:10:45', '2018-09-11 06:10:45'),
(39, 'circular regarding bonus and right share (2075-02-20)', 'circular_20750220_clearing_member', '2018_09_11_07_56_21_circular_20750220_clearing_member.pdf', 2, 1, NULL, '2018-09-11 06:11:21', '2018-09-11 06:11:21'),
(40, 'circular regarding bonus and right share (2075-02-23)', 'circular_20750223_clearing_member', '2018_09_11_07_56_40_circular_20750223_clearing_member.pdf', 2, 1, NULL, '2018-09-11 06:11:40', '2018-09-11 06:11:40'),
(41, 'circular regarding capital windfall gain tax (2075-03-31)', 'circular_20750331_clearing_member_windfall_gain_tax', '2018_09_11_07_57_03_circular_20750331_clearing_member_windfall_gain_tax.pdf', 2, 1, NULL, '2018-09-11 06:12:03', '2018-09-11 06:12:03'),
(42, 'circular regarding capital gain tax (2075-03-32)', 'circular_20750332_clearing_members_capital_gain_tax', '2018_09_11_07_57_21_circular_20750332_clearing_members_capital_gain_tax.pdf', 2, 1, NULL, '2018-09-11 06:12:21', '2018-09-11 06:12:21'),
(43, 'Mode of operation for EDIS', 'Mode of operation for EDIS', '2018_10_04_08_33_07_mode_of_operation_for_edis_directive.pdf', 1, 1, NULL, '2018-10-04 06:48:07', '2018-10-04 06:48:07'),
(44, 'EDIS Manual for Client', 'EDIS Manual for Client', '2018_10_07_16_43_38_edis_manual_for_client.pdf', 1, 1, NULL, '2018-10-07 14:58:38', '2018-10-07 14:58:38'),
(45, 'EDIS Manual for DPs', 'EDIS Manual for DPs', '2018_10_10_06_50_32_EDIS Manual for DP.pdf', 4, 1, NULL, '2018-10-10 05:05:32', '2018-10-10 05:05:32'),
(46, 'circular_regarding clearing and settlement (2072-03-08)', 'circular_20720308', '2018_09_11_07_50_17_circular_20720308.pdf', 2, 1, NULL, '2018-09-11 06:05:17', '2018-09-11 06:05:17'),
(47, 'Application Letter', 'Application Letter', '2018_11_02_19_12_54_application_letter.pdf', 3, 1, NULL, '2018-11-02 23:12:54', '2018-11-02 23:12:54'),
(48, 'Required Documents', 'Required Documents', '2018_11_02_19_13_11_required_documents.pdf', 3, 1, NULL, '2018-11-02 23:13:11', '2018-11-02 23:13:11'),
(49, 'Master Creation Form', 'Master Creation Form', '2018_11_02_19_13_27_master_creation_form.pdf', 3, 1, NULL, '2018-11-02 23:13:27', '2018-11-02 23:13:27'),
(50, 'Registration Fees and Annual Fees', 'Registration Fees and Annual Fees', '2018_11_02_19_13_49_registration_fees_annual_fees.pdf', 3, 1, NULL, '2018-11-02 23:13:49', '2018-11-02 23:13:49'),
(51, 'Book Close Discloser Information Form', 'Book Close Discloser Information Form', '2018_11_02_19_14_22_book_close_discloser_information_form.pdf', 3, 1, NULL, '2018-11-02 23:14:22', '2018-11-02 23:14:22');

-- --------------------------------------------------------

--
-- Table structure for table `downloads_type`
--

CREATE TABLE `downloads_type` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `downloads_type`
--

INSERT INTO `downloads_type` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'By Laws', '2018-09-18 15:11:37', '0000-00-00 00:00:00'),
(2, 'Circular', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Demat Registration', '2018-09-27 07:19:31', '0000-00-00 00:00:00'),
(4, 'Publication', '2018-10-22 07:15:19', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `dp`
--

CREATE TABLE `dp` (
  `id` int(10) UNSIGNED NOT NULL,
  `dp_id` int(11) NOT NULL,
  `dp_type` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meroshare` int(11) NOT NULL DEFAULT '1',
  `setup_date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0-Inactive, 1-Active, 2-Deleted',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dp`
--

INSERT INTO `dp` (`id`, `dp_id`, `dp_type`, `name`, `address`, `phone`, `email`, `meroshare`, `setup_date`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 10100, 3, 'CIVIL CAPITAL MARKET LIMITED', '6th  FLOOR, CTC MALL,SUNDHARA,KATHMANDU,Kathmandu,NEPAL', '14262303 14261251 14248890', 'ccm@civilcapital.com.np', 1, '2041-10-05', 1, NULL, NULL, NULL),
(2, 10200, 3, 'NIBL ACE CAPITAL LIMITED', 'LAL COLONY MARG,LAL DURBAR,Kathmandu,NEPAL', '14426161 14426133', 'acecapital@ace.com.np', 1, '2041-10-05', 1, NULL, NULL, NULL),
(3, 10300, 3, 'STOCK MANAGEMENT & DP LIMITED', 'GHUMTI KUMARI MARGA,PURANO BANESWOR,KATHMANDU,NEPAL', '14464982 14464983', '', 0, '2041-10-05', 2, NULL, NULL, NULL),
(4, 10900, 3, 'SIDDHARTHA CAPITAL LIMITED', 'ka. ma. na. pa. ward no. 1,Narayan Chour, Naxal,Kathmandu,Nepal', '14420924 14420925 14420929', '', 1, '0000-00-00', 1, NULL, NULL, NULL),
(5, 10400, 3, 'NABIL INVESTMENT BANKING LTD.', 'NARAYANCHOUR,NAXAL,KATHMANDU,NEPAL', '14411604 14411733 14410554', 'nabilinvest@nabilbank.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(6, 10600, 3, 'NIBL ACE CAPITAL LIMITED', 'Lazimpat,Kathmandu,Nepal', '14005080 14005058 14005084', 'info@niblcapital.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(7, 10700, 3, 'LAXMI CAPITAL MARKET LTD.', 'NAYABANESHWOR,KATHMANDU,NEPAL', '14780222 14781582 14780583', 'info@laxmicapital.com.np', 1, '0000-00-00', 1, NULL, NULL, NULL),
(8, 10800, 3, 'EVEREST BANK LTD.', 'Lazimpat,Kathmandu,Nepal', '14443377 14443160', '', 1, '0000-00-00', 1, NULL, NULL, NULL),
(9, 11400, 3, 'TRISHAKTI SECURITIES PUBLIC LIMITED', 'PUTALISADAK,KATHMANDU,NEPAL', '14232132 14232133', 'trishantisecurities@gmail.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(10, 11500, 3, 'NEPAL STOCK HOUSE PRIVATE LIMITED', 'KALIKASTHAN,KATHMANDU,NEPAL', '01-4429621, 01-4429631', 'nshplktm@gmail.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(11, 11600, 3, 'SECURED SECURITIES LIMITED', 'KA. MA. NA. PA. 31,PRADARSHANI MARG,JOSHI COMPLEX, 1ST FLOOR,KATHMANDU,Kathmandu,NEPAL', '14262861 14224523 14224523', 'ss36nepal@gmail.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(12, 12200, 3, 'JANATA BANK NEPAL LIMITED', 'SHANKHAMUL MARG,NAYA BANESHWOR,KATHMANDU,Kathmandu,NEPAL', '14786100 14785900 14786300', 'info@janatabank.com.np', 1, '0000-00-00', 1, NULL, NULL, NULL),
(13, 13100, 3, 'IMPERIAL SECURITIES COMPANY PRIVATE LIMITED', 'K.M.C.-32, ANAMNAGAR,HANUMANSTHAN,KATHMANDU,Kathmandu,NEPAL', '14231004 14222187 14231344', '45.imperial@gmail.com', 1, '2042-00-08', 1, NULL, NULL, NULL),
(14, 13000, 3, 'SOUTH ASIAN BULLS PRIVATE LIMITED', 'TULSI KRISHNA PLAZA,2ND FLOOR,KULESWOR,KATHMANDU,Kathmandu,NEPAL', '14273400 14273400 14284785', 'sabullsp@gmail.com', 1, '2042-00-08', 1, NULL, NULL, NULL),
(15, 12700, 3, 'SEWA SECURITIES PRIVATE LIMITED', 'TRIPURESWOR,KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14256642 14256644 14256644', 'sewasecurities@gmail.com', 1, '2042-00-08', 1, NULL, NULL, NULL),
(16, 12800, 3, 'PRIMO SECURITIES PRIVATE LIMITED', 'SHANKER DEV MARG,PUTALISADAK,KATHMANDU,Kathmandu,NEPAL', '14168175 14168164 14168214', 'primo.securities@gmail.com', 1, '2042-00-08', 1, NULL, NULL, NULL),
(17, 11000, 3, 'NMB CAPITAL LIMITED', 'NAGPOKHARI,KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14437963 14437964 14251544', 'info@nmbcl.com.np', 1, '0000-00-00', 1, NULL, NULL, NULL),
(18, 11100, 3, 'BANK OF KATHMANDU LIMITED', 'ANAMNAGAR,KATHMANDU,KATHMANDU,NEPAL', '14770414 14770413', '', 1, '0000-00-00', 1, NULL, NULL, NULL),
(19, 11200, 3, 'GLOBAL IME CAPITAL LIMITED', 'JAMAL,KATHMANDU,Kathmandu,NEPAL', '14222460 14222534', 'info@globalimecapital.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(20, 12300, 3, 'AGRAWAL SECURITIES PRIVATE LIMITED', 'RADHE MARG,DILLIBAZAR,KATHMANDU,Kathmandu,NEPAL', '14424406 14431597 14424657', '', 1, '0000-00-00', 1, NULL, NULL, NULL),
(21, 12900, 3, 'SHREE KRISHNA SECURITIES LIMITED', '3RD FLOOR, ALLIANCE TOWER,CHARKHAL ROAD,DILLIBAZAR,KATHMANDU,Kathmandu,NEPAL', '14441225 14441226 14431592', 'mundara@wlink.com.np/skslkathmandu@gmail.com', 1, '2042-00-08', 1, NULL, NULL, NULL),
(22, 12600, 3, 'PRABHU CAPITAL LIMITED', 'KAMALADI,KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14221946 14221952 14222802', 'info@prabhucapital.com', 1, '2042-00-08', 1, NULL, NULL, NULL),
(23, 13300, 3, 'CREATIVE SECURITIES PRIVATE LIMITED', 'PUTALISADAK,KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14419582 14168203', 'creativesec2011@gmail.com', 1, '2042-04-07', 1, NULL, NULL, NULL),
(24, 13200, 3, 'ABC SECURITIES PRIVATE LIMITED', 'INDRACHOWK,KATHMANDU,NEPAL', '14230787 14226507', 'abcsecurities17@gmail.com', 1, '2042-04-07', 1, NULL, NULL, NULL),
(25, 11300, 3, 'DIPSHIKHA DHITOPATRA KAROBAR COMPANY (P.) LTD.', 'ANAMNAGAR - 32,KATHMANDU,Kathmandu,NEPAL', '14102532 14102534 14425640', 'info@ddkc.com.np', 1, '0000-00-00', 1, NULL, NULL, NULL),
(26, 11800, 3, 'ONLINE SECURITIES PRIVATE LIMITED', 'PUTALISADAK,KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14168308 14168302 14168298', 'onlinesecurities_49@yahoo.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(27, 11700, 3, 'CITIZENS BANK INTERNATIONAL LIMITED', 'NARAYANHITY PATH,KATHMANDU,Kathmandu,NEPAL', '4427842 4427843 4427044', 'info@ctznbank.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(28, 11900, 3, 'ARYATARA INVESTMENT AND SECURITIES PRIVATE LIMITED', 'ANAMNAGAR 32,KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14233596 14233597', 'aryatara57@yahoo.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(29, 12000, 3, 'DAKSHINKALI INVESTMENT AND SECURITIES PRIVATE LIMITED', 'KAMALADI,KATHMANDU,Kathmandu,NEPAL', '14168640 14168641 14168742', 'displtd33@gmail.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(30, 12500, 3, 'VIBOR CAPITAL LIMITED', 'TRADETOWER, THAPATHALI,KATHMANDU,Kathmandu,NEPAL', '15111172 15111159 15524554', 'info@viborcapital@gmail.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(31, 12400, 3, 'SUNRISE CAPITAL LIMITED', 'KAMALPOKHARI,KATHMANDU,Kathmandu,NEPAL', '14439676 14425343 14428688', 'info@sunrisecapital.com.np', 1, '0000-00-00', 1, NULL, NULL, NULL),
(32, 13800, 3, 'LINCH STOCK MARKET  LIMITED', 'NEW BANESHWOR,KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14469367 14469068 14469068', 'linchstockmarket@gmail.com', 1, '2042-09-03', 1, NULL, NULL, NULL),
(33, 14600, 3, 'SIPLA SECURITIES PRIVATE LIMITED', 'KHICHAPOKHARI,KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14255782 14255078 14255078', 'sipla20@gmail.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(34, 13400, 3, 'CRYSTAL KANCHANJUNGHA SECURITIES PVT. LTD', 'NEW PLAZA,KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14011176 14011072', 'crystalkanchanjungha@gmail.com', 1, '2042-06-05', 1, NULL, NULL, NULL),
(35, 14400, 3, 'SANI SECURITIES COMPANY LIMITED', 'NACHGHAR, JAMAL,KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14166005 14166006', 'sani.securities@gmail.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(36, 14500, 3, 'DEEVYAA  SECURITIES & STOCK HOUSE PRIVATE LIMITED', 'PUTALISADAK,KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14421488 14420987', 'deevya59@gmail.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(37, 15400, 3, 'KAILASH BIKAS BANK LIMITED', 'NEWPLAZA,KATHMANDU,Kathmandu,NEPAL', '14443034 14443034 14423779', 'info@kailashbank.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(38, 13700, 3, 'NIC ASIA BANK LIMITED', 'TRADE TOWER NEPAL,THAPATHALI,KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '15111179 15111178 15111180', 'info@nicasiabank.com', 1, '2042-07-08', 1, NULL, NULL, NULL),
(39, 15100, 3, 'NEPAL BANGLADESH BANK LIMITED', 'BIJULIBAZAR,KATHMANDU,Kathmandu,NEPAL', '14780770 14782767 14784326', 'nbbldp@nbbl.com.np', 1, '0000-00-00', 1, NULL, NULL, NULL),
(40, 15700, 3, 'NEPAL BANK LIMITED', 'DHARMAPATH,KATHMANDU,Kathmandu,NEPAL', '14221185 14221185 14222396', 'kbo@nbl.com.np', 1, '0000-00-00', 1, NULL, NULL, NULL),
(41, 16400, 3, 'NATIONAL MERCHANT BANKER LIMITED', 'GYNESHWOR -32,ANANDA BHAIRAV MARGA,KATHMANDU,Kathmandu,NEPAL', '14430746 14430746', 'info@nmbl.com.np', 1, '0000-00-00', 1, NULL, NULL, NULL),
(42, 16100, 3, 'MACHHAPUCHCHHRE BANK LIMITED', 'LAZIMPAT,KATHMANDU,Kathmandu,NEPAL', '14428556 14428556 14425356', 'machbank@mbl.com.np', 1, '0000-00-00', 1, NULL, NULL, NULL),
(43, 16500, 3, 'RBB MERCHANT BANKING LIMITED', 'TEKU, KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14243265 14245018', 'rbbmbl@rbbmbl.com.np', 1, '0000-00-00', 1, NULL, NULL, NULL),
(44, 15900, 3, 'NAASA SECURITIES COMPANY LTD', 'NAXAL,KATHMANDU,Kathmandu,NEPAL', '14440384 14440386 14440385', 'naasasecurities58@gmail.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(45, 16200, 3, 'GUHESWORI MERCHANT BANKING & FINANCE LIMITED', 'PULCHOK, LALITPUR,KATHMANDU,Kathmandu,NEPAL', '15550406 15537407 15536169', 'archana.pradhan@gmbf.com.np', 1, '0000-00-00', 1, NULL, NULL, NULL),
(46, 16300, 3, 'NEPAL CREDIT AND COMMERCE BANK LIMITED', 'BAGBAZAR,KATHMANDU,Kathmandu,NEPAL', '', 'nccdp@nccbank.com.np', 1, '0000-00-00', 1, NULL, NULL, NULL),
(47, 14000, 3, 'SRI HARI SECURITIES PVT. LTD.', 'HATTISAR ROAD,KAMALADI, KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14437562 14437465 14437465', 'info@srihari.com.np', 1, '0000-00-00', 1, NULL, NULL, NULL),
(48, 15000, 3, 'BHRIKUTI STOCK BROKING COMPANY PRIVATE LIMITED', 'NEWROAD,KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14223466 14224648', 'rajibnepal@gmail.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(49, 15200, 3, 'KUMARI BANK LIMITED', 'NAXAL,KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14415173 14414126', 'info@kumaribank.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(50, 16700, 3, 'MAHALAXMI BIKAS BANK LIMITED', 'DURBARMARG,  KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14268162 14268208 14230664', 'info@mahalaxmi.com.np', 1, '0000-00-00', 1, NULL, NULL, NULL),
(51, 13600, 3, 'MARKET SECURITIES EXCHANGE COMPANY PVT. LTD', 'OM DEV PLAZA COMPLEX,BLOCK B, 4TH FLOOR,KHICHAPOKHARI,KATHMANDU,Kathmandu,NEPAL', '14248973 14249558 14249558', 'mkt.securities@gmail.com', 1, '2042-07-08', 1, NULL, NULL, NULL),
(52, 14100, 3, 'KRITI CAPITAL & INVESTMENTS  LIMITED', 'BAGDURBAR, SUNDHARA,JDA COMPLEX, KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14266325 14249453 14281776', 'kriticapital@gmail.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(53, 14700, 3, 'ASIAN SECURITIES PRIVATE LIMITED', 'PUTALISADAK,KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14424351 14424485 14431395', 'asiansecurities26@gmail.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(54, 15800, 3, 'SANIMA BANK LTD', 'NAXAL,KATHMANDU,Kathmandu,NEPAL', '14439953 14428979 14428969', 'manishjoshi.sc@sanimabank.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(55, 16000, 3, 'CENTURY COMMERCIAL BANK LIMITED', 'PUTALISADAK,KATHMANDU,Kathmandu,NEPAL', '14445062 14412579 14441422', 'welcome@centurybank.com.np', 1, '0000-00-00', 1, NULL, NULL, NULL),
(56, 13500, 3, 'VISION SECURITIES PVT. LTD', 'ANAMNAGAR, 32,KATHMANDU,Kathmandu,NEPAL', '14770452 14770425', 'visionsecurities@hotmail.com', 1, '2042-06-05', 1, NULL, NULL, NULL),
(57, 13900, 3, 'PRABHU BANK LIMITED', 'BABARMAHAL,KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14788500 14780588', 'info@prabhubank.com', 1, '2042-09-03', 1, NULL, NULL, NULL),
(58, 14200, 3, 'SUMERU  SECURITIES  PRIVATE  LIMITED', 'HATTISAR,KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14444740 14424209', 'sumeru.securities@gmail.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(59, 14300, 3, 'KOHINOOR INVESTMENT & SECURITIES PRIVATE LIMITED', 'HATTISAR, KAMAL POKHARI,KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14442857 14442858 14442857', 'kohinoorinvest@gmail.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(60, 14800, 3, 'PREMIER SECURITIES COMPANY LIMITED', 'NEWPLAZA, PUTALISADAK,KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '4432832 4432704', 'premiersecurities@hotmail.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(61, 14900, 3, 'DYNAMIC MONEY MANAGERS SECURITIES PRIVATE LIMITED', 'KAMALPOKHARI-31,KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14414285 14414522', 'dynamicmoney2006@gmail.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(62, 15300, 3, 'NEPAL SBI BANK LIMITED', 'HATTISAR,KATHMANDU,Kathmandu,NEPAL', '12119911 14435671 14435612', 'info@nsbl.com.np', 1, '0000-00-00', 1, NULL, NULL, NULL),
(63, 15500, 3, 'NEPAL DP LIMITED', 'PUTALISADAK,KATHMANDU,Kathmandu,NEPAL', '14227086 14227086 14233591', 'nepaldpltd@gmail.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(64, 15600, 3, 'ASHUTOSH BROKERAGE AND SECURITIES PRIVATE LIMITED', 'KHICHAPOKHARI,KATHMANDU,Kathmandu,NEPAL', '14220276 14220276 14240162', 'broker8ash@gmail.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(65, 16600, 3, 'PROVIDENT MERCHANT BANKING LIMITED', 'KATHMANDU 35, BALKUMARI,BRIDGE,KATHAMNDU,Kathmandu,NEPAL', '5147097 5147192 5147097', 'provident mbl@gmail.com', 1, '0000-00-00', 1, NULL, NULL, NULL),
(66, 17100, 3, 'TRISUL SECURITIES & INVESTMENT LIMITED', 'NEWPLAZA, KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14440709 14440709', 'broker.trisul@gmail.com', 0, '0000-00-00', 1, NULL, NULL, NULL),
(67, 16800, 3, 'UNITED FINANCE LIMITED', 'DURBARMARG, KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '01-4241648 01-4241645', 'info@ufl.com.np', 1, '0000-00-00', 1, NULL, NULL, NULL),
(68, 17200, 3, 'AGRICULTURAL DEVELOPMENT BANK LIMITED', 'RAMSHAHPATH, KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14251537 14251537', 'asba@adbl.gov.np', 1, '0000-00-00', 1, NULL, NULL, NULL),
(69, 16900, 3, 'PRIME COMMERCIAL BANK LIMITED', 'KAMALPOKHARI, KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14423433 14423215 14423346', 'info@pcbl.com.np', 1, '0000-00-00', 1, NULL, NULL, NULL),
(70, 17000, 3, 'OXFORD SECURITIES PVT. LTD.', 'KALIMATI, KATHMANDU,KATHMANDU,Kathmandu,NEPAL', '14273850 14278113', 'broker_51@yahoo.com', 1, '0000-00-00', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `isin`
--

CREATE TABLE `isin` (
  `id` int(10) UNSIGNED NOT NULL,
  `issuer_id` int(11) NOT NULL,
  `script` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isin_code` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(10) NOT NULL DEFAULT '1' COMMENT '0-Inactive 1-Active 2-Deleted',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `isin`
--

INSERT INTO `isin` (`id`, `issuer_id`, `script`, `isin_code`, `type`, `remarks`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 10007, 'SBI', 'NPE007A00002', '1', 'Nepal SBI Bank Ltd- Ordinary Share', 1, NULL, NULL, NULL),
(2, 10003, 'BOKL', 'NPE003A00001', '1', 'BANK OF KATHMANDU LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(3, 10008, 'SIL', 'NPE008A00000', '1', 'Siddhartha Insurance Limited- Ordinary Share', 1, NULL, NULL, NULL),
(4, 10004, 'LBL', 'NPE004A00009', '1', 'LAXMI BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(5, 10011, 'CZBIL', 'NPE011A00004', '1', 'CITIZENS BANK INTERNATIONAL LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(6, 10006, 'SICL', 'NPE006A00004', '1', 'SHIKHAR INSURANCE COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(7, 10005, 'HGI', 'NPE005A00006', '1', 'HIMALAYAN GENERAL INSURANCE COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(8, 10010, 'PLIC', 'NPE010A00006', '1', 'PRIMELIFE INSURANCE COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(9, 10001, 'ADBL', 'NPE001A00005', '1', 'AGRICULTURAL DEVELOPMENT BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(10, 10239, 'SHPC', 'NPE234A00002', '1', 'SANIMA MAI HYDROPOWER LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(11, 10339, 'SMBPO', 'NPE308A40000', '2', 'SUPPORT MICROFINANCE BITTIYA SANSTHA LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(12, 10019, 'HBLPO', 'NPE019A40003', '2', 'HIMALAYAN BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(13, 10272, 'NBBL', 'NPE249A00000', '1', 'NAGBELI LAGHUBITTA BITTIYA SANSTHA LIMITED-ORDINARY SHARE', 1, NULL, NULL, NULL),
(14, 10112, 'DDBL', 'NPE110A00004', '1', 'DEPROSC LAGHUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(15, 10112, 'DDBLPO', 'NPE110A40000', '2', 'DEPROSC LAGHUBITTA BITTIYA SANSTHA LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(16, 10101, 'SMFDBP', 'NPE099A40005', '2', 'SUMMIT LAGHUBITTA BITTIYA SANSTHA LTD-PROMOTER SHARE', 1, NULL, NULL, '2018-10-10 04:37:11'),
(17, 10195, 'AHPC', 'NPE191A00004', '1', 'ARUN VALLEY HYDROPOWER DEVELOPMENT COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(18, 10301, 'MSMBS', 'NPE275A00005', '1', 'MAHILA SAHAYATRA MICROFINANCE BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(19, 10301, 'MSMBSP', 'NPE275A40001', '2', 'MAHILA SAHAYATRA MICROFINANCE BITTIYA SANSTHA LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(20, 10342, 'UMB', 'NPE311A00008', '1', 'UNNATI MICROFINANCE BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(21, 10012, 'CBLPO', 'NPE012A40008', '2', 'CIVIL BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(22, 10110, 'WDBLPO', 'NPE108A40004', '2', 'WESTERN DEVELOPMENT BANK LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(23, 10010, 'PLICPO', 'NPE010A40002', '2', 'PRIMELIFE INSURANCE COMPANY LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(24, 10291, 'NMBMFP', 'NPE266A40000', '2', 'NMB  MICROFINANCE BITTIYA SANSTHA LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(25, 10140, 'MFIL', 'NPE137A00007', '1', 'MANJUSHREE FINANCE LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(26, 10337, 'NHDL', 'NPE306A00008', '1', 'NEPAL HYDRO DEVELOPER LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(27, 10022, 'PRVU', 'NPE022A00001', '1', 'PRABHU  BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(28, 10229, 'RLFL', 'NPE224A00003', '1', 'RELIANCE FINANCE LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(29, 10229, 'RLFLPO', 'NPE224A40009', '2', 'RELIANCE FINANCE LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(30, 10314, 'NMFBSP', 'NPE284A40003', '2', 'NATIONAL MICROFINACE BITTIYA SANSTHA LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(31, 10215, 'PIC', 'NPE211A00000', '1', 'PREMIER INSURANCE COMPANY (NEPAL) LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(32, 10007, 'SBID2078', 'NPE007A11025', '7', 'Nepal SBI Bank Ltd. - Debenture - 2078', 1, NULL, NULL, NULL),
(33, 10007, 'SBID2079', 'NPE007A11017', '9', 'Nepal SBI Bank Ltd. - Debenture - 2079', 1, NULL, NULL, NULL),
(34, 10007, 'SBID2080', 'NPE007A11009', '10', 'Nepal SBI Bank Ltd. - Debenture - 2080', 1, NULL, NULL, NULL),
(35, 10032, 'PCBL', 'NPE031A00002', '1', 'PRIME COMMERCIAL BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(36, 10032, 'PCBLP', 'NPE031A40008', '2', 'PRIME COMMERCIAL BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(37, 10249, 'CCBLPO', 'NPE243A40009', '2', 'CENTURY COMMERCIAL BANK LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(38, 10045, 'GDBLPO', 'NPE044A40001', '2', 'GANDAKI BIKAS BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(39, 10127, 'BFCPO', 'NPE125A40008', '2', 'BEST FINANCE COMPANY LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(40, 10330, 'UMHL', 'NPE299A00005', '1', 'UNITED MODI HYDROPOWER LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(41, 10345, 'CMF1', 'NPE313A35000', '3', 'CITIZENS MUTUAL FUND 1- MUTUAL FUND', 1, NULL, NULL, NULL),
(42, 10084, 'NUBL', 'NPE082A00005', '1', 'NIRDHAN UTTHAN LAGHUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(43, 10289, 'NMBSF1', 'NPE264A35005', '3', 'NMB SULAV INVESTMENT FUND I - MUTUAL FUND', 1, NULL, NULL, NULL),
(44, 10191, 'SRS', 'NPE187A00002', '1', 'SRI RAM SUGAR MILLS LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(45, 10333, 'LEMF', 'NPE302A35003', '3', 'LAXMI EQUITY FUND - MUTUAL FUND', 1, NULL, NULL, NULL),
(46, 10024, 'MBLPO', 'NPE024A40003', '2', 'MACHHAPUCHCHHRE BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(47, 10236, 'RMDC', 'NPE231A00008', '1', 'RMDC LAGHUBITTA  BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(48, 10074, 'MDB', 'NPE072A00006', '1', 'MITERI DEVELOPMENT BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(49, 10110, 'WDBL', 'NPE108A00008', '1', 'WESTERN DEVELOPMENT BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(50, 10172, 'SHL', 'NPE168A00002', '1', 'SOALTEE HOTEL LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(51, 10203, 'PRIN', 'NPE199A00007', '1', 'PRABHU INSURANCE LIMITED- ORDINARY SHARE', 1, NULL, NULL, NULL),
(52, 10157, 'PROFL', 'NPE154A00002', '1', 'PROGRESSIVE FINANCE LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(53, 10157, 'PROFLP', 'NPE154A40008', '2', 'PROGRESSIVE FINANCE LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(54, 10291, 'NMBMF', 'NPE266A00004', '1', 'NMB MICROFINANCE BITTIYA SANSTHA LIMITED -ORDINARY SHARE', 1, NULL, NULL, NULL),
(55, 10309, 'MERO', 'NPE280A00005', '1', 'MERO MICROFINANCE BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(56, 10309, 'MEROPO', 'NPE280A40001', '2', 'MERO MICROFINANCE BITTIYA SANSTHA LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(57, 10332, 'SDESI', 'NPE301A00009', '1', 'SWADESHI LAHGUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(58, 10233, 'MIDBL', 'NPE228A00004', '1', 'MISSION DEVELOPMENT BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(59, 10004, 'LBLPO', 'NPE004A40005', '2', 'LAXMI BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(60, 10096, 'SBBLJ', 'NPE094A00000', '1', 'SAHAYOGI VIKAS BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(61, 10096, 'SBBLJP', 'NPE094A40006', '2', 'SAHAYOGI VIKAS BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(62, 10104, 'SWBBLP', 'NPE102A40007', '2', 'SWABALAMBAN LAGHUBITTA BITTIYA SANSTHA LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(63, 10118, 'SYFL', 'NPE116A00001', '1', 'SYNERGY FINANCE LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(64, 10118, 'SYFLPO', 'NPE116A40007', '2', 'SYNERGY FINANCE LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(65, 10241, 'NNLB', 'NPE236A00007', '1', 'NAYA NEPAL LAGHU BITTA BIKAS BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(66, 10217, 'RBCL', 'NPE213A00006', '1', 'RASTRIYA BEEMA COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(67, 10217, 'RBCLPO', 'NPE213A40002', '2', 'RASTRIYA BEEMA COMPANY LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(68, 10316, 'RSDC', 'NPE286A00002', '1', 'RSDC LAGHUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(69, 10316, 'RSDCP', 'NPE286A40008', '2', 'RSDC LAGHUBITTA BITTIYA SANSTHA LIMITED - PROMOTER SAHRE', 1, NULL, NULL, NULL),
(70, 10077, 'MNBBL', 'NPE075A00009', '1', 'MUKTINATH BIKAS BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(71, 10135, 'GUFLPO', 'NPE133A40002', '2', 'GURKHAS FINANCE LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(72, 10209, 'IGIPO', 'NPE205A40008', '2', 'IME GENERAL INSURANCE LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(73, 10025, 'NABIL', 'NPE025A00004', '1', 'NABIL BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(74, 10029, 'NICA', 'NPE029A00006', '1', 'NIC ASIA BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(75, 10021, 'KBL', 'NPE021A00003', '1', 'KUMARI BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(76, 10245, 'NCDB', 'NPE240A00009', '1', 'NEPAL COMMUNITY DEVELOPMENT BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(77, 10298, 'SEOS', 'NPE272A35008', '3', 'SIDDHARTHA EQUITY ORIENTED SCHEME- MUTUAL FUND', 1, NULL, NULL, NULL),
(78, 10332, 'SDESIP', 'NPE301A40005', '2', 'SWADESHI LAGHUBITTA BITTIYA SANSTHA LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(79, 10020, 'JBNL', 'NPE020A00005', '1', 'JANATA BANK NEPAL LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(80, 10024, 'MBL', 'NPE024A00007', '1', 'MACHHAPUCHCHHRE BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(81, 10025, 'NABILP', 'NPE025A40000', '2', 'NABIL BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(82, 10130, 'ICFCPO', 'NPE128A40002', '2', 'ICFC FINANCE LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(83, 10013, 'NBBPO', 'NPE013A40006', '2', 'NEPAL BANGLADESH BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(84, 10027, 'NCCBPO', 'NPE027A40006', '2', 'NEPAL CREDIT AND COMMERCE BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(85, 10016, 'EBLCP', 'NPE016A06000', '4', 'EVEREST BANK LIMITED - CONVERTIBLE PREFERENCE SHARE', 1, NULL, NULL, NULL),
(86, 10238, 'SKBBL', 'NPE233A00004', '1', 'SANA KISAN BIKAS LAGHUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(87, 10290, 'VLBS', 'NPE265A00006', '1', 'VIJAYA LAGHUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(88, 10290, 'VLBSPO', 'NPE265A40002', '2', 'VIJAYA LAGHUBITTA BITTIYA SANSTHA LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(89, 10126, 'GFCLPO', 'NPE124A40001', '2', 'GOODWILL FINANCE LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(90, 10213, 'NLICP', 'NPE209A40000', '2', 'NEPAL LIFE INSURANCE COMPANY LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(91, 10082, 'NIDC', 'NPE080A00009', '1', 'NIDC DEVELOPMENT BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(92, 10206, 'GLICL', 'NPE202A00009', '1', 'GURANS LIFE INSURANCE COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(93, 10074, 'MDBPO', 'NPE072A40002', '2', 'MITERI DEVELOPMENT BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(94, 10063, 'JBBL', 'NPE061A00009', '1', 'JYOTI BIKASH BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(95, 10203, 'PRINPO', 'NPE199A40003', '2', 'PRABHU INSURANCE LIMITED-PROMOTER SHARE', 1, NULL, NULL, NULL),
(96, 10052, 'CORBL', 'NPE050A00002', '1', 'CORPORATE DEVELOPMENT BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(97, 10052, 'CORBLP', 'NPE050A40008', '2', 'CORPORATE DEVELOPMENT BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(98, 10012, 'CBL', 'NPE012A00002', '1', 'CIVIL BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(99, 10013, 'NBB', 'NPE013A00000', '1', 'NEPAL BANGLADESH BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(100, 10029, 'NICAP', 'NPE029A40002', '2', 'NIC ASIA BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(101, 10016, 'EBL', 'NPE016A00003', '1', 'EVEREST BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(102, 10016, 'EBLPO', 'NPE016A40009', '3', 'EVEREST BANK LIMITED - PROMOTOR SHARE', 1, NULL, NULL, NULL),
(103, 10104, 'SWBBL', 'NPE102A00001', '1', 'SWABALAMBAN LAGHUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(104, 10168, 'UFL', 'NPE164A00001', '1', 'UNITED FINANCE LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(105, 10168, 'UFLPO', 'NPE164A40007', '2', 'UNITED FINANCE LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(106, 10238, 'SKBBLP', 'NPE233A40000', '2', 'SANA KISAN BIKAS LAGHUBITTA BITTIYA SANSTHA LIMITED-PROMOTER SHARE', 1, NULL, NULL, NULL),
(107, 10197, 'CHCL', 'NPE193A00000', '1', 'CHILIME HYDROPOWER COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(108, 10318, 'NMBHF1', 'NPE288A35004', '3', 'NMB HYBRID FUND L- 1 MUTUAL FUND', 1, NULL, NULL, NULL),
(109, 10320, 'SLBS', 'NPE290A00004', '1', 'SURYODAYA LAGHUBITTIA BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(110, 10320, 'SLBSP', 'NPE290A40000', '2', 'SURYODAYA LAGHUBITTA BITTIYA SANSTHA LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(111, 10009, 'SBL', 'NPE009A00008', '1', 'SIDDHARTHA BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(112, 10107, 'TNBL', 'NPE105A00004', '1', 'TINAU DEVELOPMENT BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(113, 10107, 'TNBLPO', 'NPE105A40000', '2', 'TINAU DEVELOPMENT BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(114, 10207, 'LGIL', 'NPE203A00007', '1', 'LUMBINI GENERAL INSURANCE COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(115, 10006, 'SICLPO', 'NPE006A40000', '2', 'SHIKHAR INSURANCE COMPANY LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(116, 10053, 'EDBLPO', 'NPE051A40006', '2', 'EXCEL DEVELOPMENT BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(117, 10080, 'DBBL', 'NPE078A00003', '1', 'DEVA BIKAS BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(118, 10080, 'DBBLPO', 'NPE078A40009', '2', 'DEVA BIKAS BANK LIMITED  - PROMOTER SHARE', 1, NULL, NULL, NULL),
(119, 10240, 'KMCDB', 'NPE235A00009', '1', 'KALIKA LAGHUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(120, 10128, 'GMFIL', 'NPE126A00000', '1', 'GUHESWORI MERCHANT BANKING AND FINANCE LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(121, 10133, 'JFLPO', 'NPE131A40006', '2', 'JANAKI FINANCE LIMITED-PROMOTER SHARE', 1, NULL, NULL, NULL),
(122, 10003, 'BOKD2079', 'NPE003A11016', '9', 'Bank Of Kathmandu Ltd.- Debenture- 2079', 1, NULL, NULL, NULL),
(123, 10187, 'NLO', 'NPE183A00001', '1', 'NEPAL LUBE OIL LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(124, 10030, 'NMB', 'NPE030A00004', '1', 'NMB BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(125, 10030, 'NMBPO', 'NPE030A40000', '2', 'NMB BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(126, 10033, 'SBBLPO', 'NPE032A40006', '2', 'SANIMA BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(127, 10173, 'TRH', 'NPE169A00000', '1', 'TARAGAON REGENCY HOTELS LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(128, 10101, 'SMFDB', 'NPE099A00009', '1', 'SUMMIT LAGHUBITTA BITTIYA SANSTHA LTD - ORDINARY SHARE', 1, NULL, NULL, '2018-10-10 04:37:33'),
(129, 10049, 'ODBL', 'NPE047A00008', '1', 'OM DEVELOPMENT BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(130, 10049, 'ODBLPO', 'NPE047A40004', '2', 'OM DEVELOPMENT BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(131, 10216, 'PICL', 'NPE212A00008', '1', 'PRUDENTIAL INSURANCE COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(132, 10212, 'NLGPO', 'NPE208A40002', '2', 'NLG INSURANCE COMPANY LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(133, 10142, 'MPFLPO', 'NPE139A40009', '2', 'MULTIPURPOSE FINANCE COMPANUY LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(134, 10327, 'FOWAD', 'NPE297A00009', '1', 'FORWARD COMMUNITY MICROFINANCE BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(135, 10327, 'FOWADP', 'NPE297A40005', '2', 'FORWARD COMMUNITY MICROFINANCE BITTIYA SANSTHA LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(136, 10014, 'SRBL', 'NPE014A00008', '1', 'SUNRISE BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(137, 10035, 'MEGA', 'NPE034A00006', '1', 'MEGA BANK NEPAL LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(138, 10028, 'NIB', 'NPE028A00008', '1', 'NEPAL INVESTMENT BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(139, 10021, 'KBLPO', 'NPE021A40009', '2', 'KUMARI BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(140, 10194, 'NTC', 'NPE190A00006', '1', 'NEPAL DOORSANCHAR COMPANY LIMITED (NTC) - ORDINARY SHARE', 1, NULL, NULL, NULL),
(141, 10042, 'BHBL', 'NPE041A00001', '1', 'BHARGAV BIKASH BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(142, 10042, 'BHBLPO', 'NPE041A40007', '2', 'BHARGAV BIKASH BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(143, 10308, 'GIMES1', 'NPE279A35003', '11', 'GLOBAL IME SAMMUNAT SCHEME -1', 1, NULL, NULL, NULL),
(144, 10351, 'NADEP', 'NPE316A00007', '1', 'NADEP LAGHUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(145, 10192, 'UNL', 'NPE188A00000', '1', 'UNILEVER NEPAL LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(146, 10269, 'LLBS', 'NPE247A00004', '1', 'LAXMI LAGHUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(147, 10269, 'LLBSPO', 'NPE247A40000', '2', 'LAXMI LAGHUBITTA BITTIYA SASNTHA LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(148, 10160, 'SFFIL', 'NPE157A00005', '1', 'SRIJANA FINANCE LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(149, 10285, 'JSLBB', 'NPE260A00007', '1', 'JANAUTTHAN SAMUDAYIC LAGHUBITTA BIKASH BANK LIMITED -ORDINARY SHARE', 1, NULL, NULL, NULL),
(150, 10285, 'JSLBBP', 'NPE260A40003', '2', 'JANAUTTHAN SAMUDAYIC LAGHUBITTA BIKASH BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(151, 10100, 'SLBBL', 'NPE098A00001', '1', 'SWAROJGAR LAGHUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(152, 10034, 'SCB', 'NPE033A00008', '1', 'STANDARD CHARTERED BANK NEPAL LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(153, 10014, 'SRBLPO', 'NPE014A40004', '2', 'SUNRISE BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(154, 10003, 'BOKLPO', 'NPE003A40007', '2', 'BANK OF KATHMANDU LIMITED - PROMOTOR SHARE', 1, NULL, NULL, NULL),
(155, 10055, 'GBBLPO', 'NPE053A40002', '2', 'GARIMA BIKAS BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(156, 10218, 'SIC', 'NPE214A00004', '1', 'SAGARMATHA INSURANCE COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(157, 10213, 'NLIC', 'NPE209A00004', '1', 'NEPAL LIFE INSURANCE COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(158, 10019, 'HBL', 'NPE019A00007', '1', 'HIMALAYAN BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(159, 10196, 'BPCL', 'NPE192A00002', '1', 'BUTWAL POWER COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(160, 10099, 'SHINE', 'NPE097A00003', '1', 'SHINE RESUNGA DEVELOPMENT BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(161, 10218, 'SICPO', 'NPE214A40000', '2', 'SAGARMATHA INSURANCE COMPANY LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(162, 10007, 'SBIPO', 'NPE007A40008', '2', 'NEPAL SBI BANK LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(163, 10215, 'PICPO', 'NPE211A40006', '2', 'PREMIER INSURANCE COMPANY (NEPAL) LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(164, 10064, 'KADBLP', 'NPE062A40003', '2', 'KANCHAN DEVELOPMENT BANK LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(165, 10053, 'EDBL', 'NPE051A00000', '1', 'EXCEL DEVELOPMENT BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(166, 10026, 'NBL', 'NPE026A00002', '1', 'NEPAL BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(167, 10055, 'GBBL', 'NPE053A00006', '1', 'GARIMA BIKAS BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(168, 10071, 'KNBL', 'NPE069A00002', '1', 'KANKAI BIKAS BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(169, 10114, 'MLBL', 'NPE112A00000', '1', 'MAHALAXMI BIKAS  BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(170, 10245, 'NCDBPO', 'NPE240A40005', '2', 'NEPAL COMMUNITY DEVELOPMENT BANK LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(171, 10210, 'NICL', 'NPE206A00000', '1', 'NEPAL INSURANCE COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(172, 10142, 'MPFL', 'NPE139A00003', '1', 'MULTIPURPOSE FINANCE COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(173, 10147, 'NFS', 'NPE144A00003', '1', 'NEPAL FINANCE LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(174, 10147, 'NFSPO', 'NPE144A40009', '2', 'NEPAL FINANCE LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(175, 10027, 'NCCB', 'NPE027A00000', '1', 'NEPAL CREDIT AND COMMERCE BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(176, 10033, 'SANIMA', 'NPE032A00000', '1', 'SANIMA BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(177, 10205, 'EIC', 'NPE201A00001', '1', 'EVEREST INSURANCE COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(178, 10227, 'JEFL', 'NPE222A00007', '1', 'JEBILS FINANCE LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(179, 10146, 'NEFLPO', 'NPE143A40001', '2', 'NEPAL EXPRESS FINANCE LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(180, 10199, 'BBC', 'NPE195A00005', '1', 'BISHAL BAZAR COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(181, 10130, 'ICFC', 'NPE128A00006', '1', 'ICFC FINANCE LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(182, 10206, 'GLICLP', 'NPE202A40005', '2', 'GURANS LIFE INSURANCE COMPANY LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(183, 10236, 'RMDCPO', 'NPE231A40004', '2', 'RMDC LAGHUBITTA BITTIYA SANSTHA  LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(184, 10287, 'WOMI', 'NPE262A00003', '1', 'WOMI MICROFINANCE BITTIYA SANSTHA LIMITED- ORDINARY SHARE', 1, NULL, NULL, NULL),
(185, 10287, 'WOMIPO', 'NPE262A40009', '2', 'WOMI MICROFINANCE BITTIYA SANSTHA LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(186, 10176, 'BNL', 'NPE172A00004', '1', 'BOTTLERS NEPAL LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(187, 10177, 'BNT', 'NPE173A00002', '1', 'BOTTLERS NEPAL (TERAI) LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(188, 10314, 'NMFBS', 'NPE284A00007', '1', 'NATIONAL MICROFINANCE BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(189, 10083, 'NLBBL', 'NPE081A00007', '1', 'NERUDE LAGHUBITA BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(190, 10095, 'SADBL', 'NPE093A00002', '1', 'SHANGRILA DEVELOPMENT BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(191, 10351, 'NADEPP', 'NPE316A40003', '2', 'NADEP LAGHUBITTA BITTIYA SANSTHA LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(192, 10065, 'KBBL', 'NPE063A00005', '1', 'KAILASH BIKAS BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(193, 10065, 'KBBLPO', 'NPE063A40001', '2', 'KAILASH BIKASH BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(194, 10063, 'JBBLPO', 'NPE061A40005', '2', 'JYOTI BIKASH BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(195, 10310, 'HIDCL', 'NPE281A00003', '1', 'JALAVIDHYUT LAGANI TATHA BIKAS COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(196, 10322, 'DHPL', 'NPE292A00000', '1', 'DIBYASHWORI HYDROPOWER LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(197, 10321, 'KKHC', 'NPE291A00002', '1', 'KHANI KHOLA HYDROPOWER COMPANY  LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(198, 10338, 'SEF', 'NPE307A35002', '3', 'SIDDHARTHA EQUITY FUND - MUTUAL FUND', 1, NULL, NULL, NULL),
(199, 10011, 'CZBILP', 'NPE011A40000', '2', 'CITIZENS BANK INTERNATIONAL LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(200, 10047, 'CBBL', 'NPE046A00000', '1', 'CHHIMEK LAGHUBITTA BIKAS BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(201, 10047, 'CBBLPO', 'NPE046A40006', '2', 'CHHIMEK LAGHUBITTA BIKAS BANK LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(202, 10300, 'KMFL', 'NPE274A00008', '1', 'KISAN MICRO FINANCE BITTIYA SANSTHA- ORDINARY SHARE', 1, NULL, NULL, NULL),
(203, 10300, 'KMFLPO', 'NPE274A40004', '2', 'KISAN MICRO FINANCE BITTIYA SANSTHA LIMITED- PROMOTER SAHRE', 1, NULL, NULL, NULL),
(204, 10162, 'SIFC', 'NPE159A00001', '1', 'SHREE INVESTMENT AND FINANCE COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(205, 10162, 'SIFCPO', 'NPE159A40007', '2', 'SHREE INVESTMENT AND  FINANCE COMPANY LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(206, 10064, 'KADBL', 'NPE062A00007', '1', 'KANCHAN DEVELOPMENT BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(207, 10045, 'GDBL', 'NPE044A00005', '1', 'GANDAKI BIKAS BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(208, 10127, 'BFC', 'NPE125A00002', '1', 'BEST FINANCE COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(209, 10122, 'CMB', 'NPE120A00003', '1', 'CAPITAL MERCHANT BANKING AND FINANCE LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(210, 10028, 'NIBPO', 'NPE028A40004', '2', 'NEPAL INVESTMENT BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(211, 10137, 'LFC', 'NPE135A00001', '1', 'LALITPUR FINANCE LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(212, 10137, 'LFCPO', 'NPE135A40007', '2', 'LALITPUR FINANCE LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(213, 10211, 'NILPO', 'NPE207A40004', '2', 'NECO INSURANCE COMPANY LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(214, 10276, 'MLBBL', 'NPE251A00006', '1', 'MITHILA LAGHUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(215, 10334, 'HPPL', 'NPE303A00005', '1', 'HIMALAYAN POWER PARTNER LIMITED - ORDIANRY SHARE', 1, NULL, NULL, NULL),
(216, 10204, 'ALICL', 'NPE200A00003', '1', 'ASIAN LIFE INSURANCE COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(217, 10204, 'ALICLP', 'NPE200A40009', '2', 'ASIAN LIFE INSURANCE COMPANY LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(218, 10202, 'STC', 'NPE198A00009', '1', 'SALT TRADING CORPORATION LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(219, 10219, 'SLICL', 'NPE215A00001', '1', 'SURYA LIFE INSURANCE COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(220, 10219, 'SLICLP', 'NPE215A40007', '2', 'SURYA LIFE INSURANCE COMPANY LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(221, 10208, 'LICN', 'NPE204A00005', '1', 'LIFE INSURANCE CORPORATION (NEPAL) LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(222, 10208, 'LICNPO', 'NPE204A40001', '2', 'LIFE INSURANCE CORPORATION (NEPAL) LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(223, 10231, 'HAMRO', 'NPE226A00008', '1', 'HAMRO BIKAS  BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(224, 10220, 'UIC', 'NPE216A00009', '1', 'UNITED INSURANCE COMPANY (NEPAL) LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(225, 10009, 'SBLPO', 'NPE009A40004', '2', 'SIDDHARTHA BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(226, 10082, 'NIDCPO', 'NPE080A40005', '2', 'NIDC DEVELOPMENT BANK LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(227, 10207, 'LGILPO', 'NPE203A40003', '2', 'LUMBINI GENERAL INSURANCE COMPANY LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(228, 10119, 'CFCL', 'NPE117A00009', '1', 'CENTRAL FINANCE   LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(229, 10119, 'CFCLPO', 'NPE117A40005', '2', 'CENTRAL FINANCE LIMITED -  PROMOTER SHARE', 1, NULL, NULL, NULL),
(230, 10299, 'SHBL', 'NPE273A00000', '1', 'SAHARA BIKASH BANK LIMITED- ORDINARY SHARE', 1, NULL, NULL, NULL),
(231, 10299, 'SHBLPO', 'NPE273A40006', '2', 'SAHARA BIKASH BANK LIMITED-PROMOTER SHARE', 1, NULL, NULL, NULL),
(232, 10237, 'SINDU', 'NPE232A00006', '1', 'SINDHU BIKASH BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(233, 10276, 'MLBBLP', 'NPE251A40002', '2', 'MITHILA LAGHUBITTABITTIYA SANSTHA LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(234, 10296, 'GILB', 'NPE270A00006', '1', 'GLOBAL IME LAGHUBITTA BITTIYA SANSTHA LIMITED- ORDINARY SHARE', 1, NULL, NULL, NULL),
(235, 10296, 'GILBPO', 'NPE270A40002', '2', 'GLOBAL IME LAGHUBITTA  BITTIYA SANSTHA LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(236, 10249, 'CCBL', 'NPE243A00003', '1', 'CENTURY COMMERCIAL BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(237, 10281, 'API', 'NPE256A00005', '1', 'API POWER COMPANY LIMITED- ORDINARY SHARE', 1, NULL, NULL, NULL),
(238, 10016, 'EBLD2079', 'NPE016A11000', '9', 'Everest Bank Ltd. - Debenture - 2079', 1, NULL, NULL, NULL),
(239, 10341, 'SAEF', 'NPE310A35006', '3', 'SANIMA EQUITY FUND - MUTUAL FUND', 1, NULL, NULL, NULL),
(240, 10029, 'NICGF', 'NPE029A35002', '3', 'NIC ASIA GROWTH FUND - MUTUAL FUND', 1, NULL, NULL, NULL),
(241, 10280, 'LVF1', 'NPE255A35003', '3', 'LAXMI VALUE FUND-1- MUTUAL FUND', 1, NULL, NULL, NULL),
(242, 10292, 'CLBSL', 'NPE267A00002', '1', 'CIVIL LAGHUBITTA BITTIYA SANSTHA LIMITED -ORDINARY SHARE', 1, NULL, NULL, NULL),
(243, 10292, 'CLBSLP', 'NPE267A40008', '2', 'CIVIL LAGHUBITTA BITTIYA SANSTHA LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(244, 10028, 'NIBD2077', 'NPE028A11005', '6', 'Nepal Investment Bank Ltd. - Debenture - 2077', 1, NULL, NULL, NULL),
(245, 10335, 'LBBLPO', 'NPE304A40009', '2', 'LUMBINI BIKAS BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(246, 10346, 'NSEWA', 'NPE314A00002', '1', 'NEPAL SEVA LAGHUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(247, 10346, 'NSEWAP', 'NPE314A40008', '2', 'NEPAL SEVA LAGHUBITTA BITTIYA SANSTHA LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(248, 10067, 'KEBL', 'NPE065A00000', '1', 'KABELI BIKAS BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(249, 10342, 'UMBPO', 'NPE311A40004', '2', 'UNNATI MICROFINANCE BITTIYA SANSTHA LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(250, 10114, 'MLBLPO', 'NPE112A40006', '2', 'MAHALAXMI BIKAS  BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(251, 10241, 'NNLBPO', 'NPE236A40003', '2', 'NAYA NEPAL LAGHU BITTA BIKAS BANK LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(252, 10279, 'BARUN', 'NPE254A00000', '1', 'BARUN HYDROPOWER COMPANY LIMITED- ORDINARY SHARE', 1, NULL, NULL, NULL),
(253, 10171, 'OHL', 'NPE167A00004', '1', 'ORIENTAL HOTEL LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(254, 10140, 'MFILPO', 'NPE137A40003', '2', 'MANJUSHREE FINANCE LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(255, 10170, 'WMBF', 'NPE166A00006', '1', 'WORLD MERCHANT BANKING AND FINANCE LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(256, 10170, 'WMBFPO', 'NPE166A40002', '2', 'WORLD MERCHANT BANKING AND FINANCE LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(257, 10336, 'MSLB', 'NPE305A00000', '1', 'MAHULI SAMUDAYIK LAGHUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(258, 10336, 'MSLBP', 'NPE305A40006', '2', 'MAHULI SAMUDAYIK LAGHUBITTIA BITTIYA SANSTHA LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(259, 10340, 'RADHI', 'NPE309A00002', '1', 'RADHI BIDYUT COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(260, 10121, 'CIT', 'NPE119A00005', '1', 'CITIZEN INVESTMENT TRUST -  ORDINARY SHARE', 1, NULL, NULL, NULL),
(261, 10231, 'HAMROP', 'NPE226A40004', '2', 'HAMRO BIKAS BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(262, 10003, 'BOKD2076', 'NPE003A11008', '8', 'Bank Of Kathmandu Ltd.- Debenture- 2076', 1, NULL, NULL, NULL),
(263, 10329, 'SMATAP', 'NPE298A40003', '2', 'SAMATA MICROFINANCE BITTIYA SANSTHA LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(264, 10212, 'NLG', 'NPE208A00006', '1', 'NLG INSURANCE COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(265, 10272, 'NBBLPO', 'NPE249A40006', '2', 'NAGBELI LAGHUBITTA BITTIYA SANSTHA LIMITED-PROMOTER SHARE', 1, NULL, NULL, NULL),
(266, 10091, 'PURBL', 'NPE089A00000', '1', 'PURNIMA BIKAS BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(267, 10028, 'NIBD2078', 'NPE028A11013', '7', 'Nepal Investment Bank Ltd. - Debenture - 2078', 1, NULL, NULL, NULL),
(268, 10237, 'SINDUP', 'NPE232A40002', '2', 'SINDHU BIKASH BANK LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(269, 10070, 'KSBBL', 'NPE068A00004', '1', 'KAMANA SEWA BIKAS BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(270, 10070, 'KSBBLP', 'NPE068A40000', '2', 'KAMANA SEWA BIKAS BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(271, 10344, 'AMFI', 'NPE312A00006', '1', 'AARAMBHA MICROFINANCE BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(272, 10344, 'AMFIPO', 'NPE312A40002', '2', 'AARAMBHA MICROFINANCE BITTIYA SANSTHA LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(273, 10319, 'NEF', 'NPE289A35002', '3', 'NABIL EQUITY FUND - MUTUAL FUND', 1, NULL, NULL, NULL),
(274, 10136, 'CEFL', 'NPE134A00004', '1', 'CITY EXPRESS FINANCE COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(275, 10136, 'CEFLPO', 'NPE134A40000', '2', 'CITY EXPRESS FINANCE COMPANY LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(276, 10034, 'SCBPO', 'NPE033A40004', '2', 'STANDARD CHARTERED BANK NEPAL LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(277, 10160, 'SFFILP', 'NPE157A40001', '2', 'SRIJANA FINANCE LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(278, 10133, 'JFL', 'NPE131A00000', '1', 'JANAKI FINANCE LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(279, 10323, 'AKPL', 'NPE293A00008', '1', 'ARUN KABELI POWER LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(280, 10347, 'RRHP', 'NPE315A00009', '1', 'RAIRANG HYDROPOWER DEVELOPMENT COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(281, 10198, 'NHPC', 'NPE194A00008', '1', 'NATIONAL HYDROPOWER COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(282, 10277, 'MMFDB', 'NPE252A00004', '1', 'MIRMIRE LAGHUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(283, 10277, 'MMFDBP', 'NPE252A40000', '2', 'MIRMIRE LAGHUBITTA BITTIYA SANSTHA LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(284, 10302, 'SKDBL', 'NPE276A00003', '1', 'SAPTAKOSHI DEVELOPMENT BANK LIMITED- ORDINARY SHARE', 1, NULL, NULL, NULL),
(285, 10302, 'SKDBLP', 'NPE276A40009', '2', 'SAPTAKOSH DEVELOPMENT BANK LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(286, 10211, 'NIL', 'NPE207A00008', '1', 'NECO INSURANCE LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(287, 10335, 'LBBL', 'NPE304A00003', '1', 'LUMBINI BIKAS BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(288, 10005, 'HGIPO', 'NPE005A40002', '2', 'HIMALAYAN GENERAL INSURANCE COMPANY LIMITED-PROMOTER SHARE', 1, NULL, NULL, NULL),
(289, 10214, 'NLICL', 'NPE210A00002', '1', 'NATIONAL LIFE INSURANCE COMPANY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(290, 10214, 'NLICLP', 'NPE210A40008', '2', 'NATIONAL LIFE INSURANCE COMPANY LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(291, 10312, 'NGPL', 'NPE282A00001', '1', 'NGADI GROUP POWER LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(292, 10022, 'PRVUPO', 'NPE022A40007', '2', 'PRABHU BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(293, 10091, 'PURBLP', 'NPE089A40006', '2', 'PURNIMA BIKAS BANK LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(294, 10029, 'NICAD2077', 'NPE029A11003', '6', 'NIC Asia Bank Ltd.- Debenture- 2077', 1, NULL, NULL, NULL),
(295, 10035, 'MEGAPO', 'NPE034A40002', '2', 'MEGA BANK NEPAL LIMITED-PROMOTER SHARE', 1, NULL, NULL, NULL),
(296, 10282, 'NIBSF1', 'NPE257A35009', '3', 'NIBL SAMRIDDHI FUND-1- MUTUAL FUND', 1, NULL, NULL, NULL),
(297, 10288, 'RHPC', 'NPE263A00001', '1', 'RIDI HYDROPOWER DEVELOPMENT COMPANY LIMITED- ORDINARY SHARE', 1, NULL, NULL, NULL),
(298, 10126, 'GFCL', 'NPE124A00005', '1', 'GOODWILL FINANCE LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(299, 10067, 'KEBLPO', 'NPE065A40006', '2', 'KABELI BIKAS BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(300, 10155, 'PFL', 'NPE152A00006', '1', 'POKHARA FINANCE LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(301, 10155, 'PFLPO', 'NPE152A40002', '2', 'POKHARA FINANCE LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(302, 10220, 'UICPO', 'NPE216A40005', '2', 'UNITED INSURANCE COMPANY (NEPAL) LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(303, 10324, 'NIBLPF', 'NPE294A35002', '3', 'NIBL PRAGATI FUND - MUTUAL FUND', 1, NULL, NULL, NULL),
(304, 10227, 'JEFLPO', 'NPE222A40003', '2', 'JEBILS FINANCE LIMITED-PROMOTER SHARE', 1, NULL, NULL, NULL),
(305, 10072, 'KRBL', 'NPE070A00000', '1', 'KARNALI DEVELOPMENT BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(306, 10072, 'KRBLPO', 'NPE070A40006', '2', 'KARNALI DEVLOPMENT BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(307, 10329, 'SMATA', 'NPE298A00007', '1', 'SAMATA MICROFINANCE BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(308, 10017, 'GBIME', 'NPE017A00001', '1', 'GLOBAL IME BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(309, 10017, 'GBIMEP', 'NPE017A40007', '2', 'GLOBAL IME BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(310, 10240, 'KMCDBP', 'NPE235A40005', '2', 'KALIKA LAGHUBITTA BITTIYA SANSTHA LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(311, 10233, 'MIDBLP', 'NPE228A40000', '2', 'MISSION DEVELOPMENT BANK LIMITED-PROMOTER SHARE', 1, NULL, NULL, NULL),
(312, 10084, 'NUBLPO', 'NPE082A40001', '2', 'NIRDHAN UTTHAN LAGHUBITTA BITTIYA SANSTHA LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(313, 10128, 'GMFILP', 'NPE126A40006', '2', 'GUHESWORI MERCHANT BANKING AND FINANCE LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(314, 10095, 'SADBLP', 'NPE093A40008', '2', 'SHANGRILA DEVELOPMENT BANK LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(315, 10100, 'SLBBLP', 'NPE098A40007', '2', 'SWAROJGAR LAGHUBITTA BITTIYA SANSTHA LIMITED- PROMOTER SAHRE', 1, NULL, NULL, NULL),
(316, 10183, 'HDL', 'NPE179A00009', '1', 'HIMALAYAN DISTILLERY LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(317, 10331, 'CHL', 'NPE300A00001', '1', 'CHHYANGDI HYDROPOWER LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(318, 10077, 'MNBBLP', 'NPE075A40005', '2', 'MUKTINATH BIKAS BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(319, 10071, 'KNBLPO', 'NPE069A40008', '2', 'KANKAI BIKAS BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(320, 10008, 'SILPO', 'NPE008A40006', '2', 'SIDDHARTHA INSURANCE LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(321, 10054, 'FMDBL', 'NPE052A00008', '1', 'FIRST MICROFINANCE LAGHU BITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(322, 10054, 'FMDBLP', 'NPE052A40004', '2', 'FIRST MICROFINANCE LAGHU BITTA BITTIYA SANSTHA LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(323, 10235, 'GBLBS', 'NPE230A00000', '1', 'GRAMEEN BIKAS LAGHUBITTA BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(324, 10235, 'GBLBSP', 'NPE230A40006', '2', 'GRAMEEN BIKAS LAGHUBITTA BITTIYA SANSTHA LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(325, 10116, 'HATH', 'NPE114A00006', '1', 'HATHWAY FINANCE COMPANY  LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(326, 10116, 'HATHPO', 'NPE114A40002', '2', 'HATHWAY  FINANCE COMPANY LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(327, 10029, 'NICAD8182', 'NPE029A11011', '5', 'NIC ASIA DEBENTURE 2081/82', 1, NULL, NULL, NULL),
(328, 10122, 'CMBFLP', 'NPE120A40009', '2', 'CAPITAL MERCHANT BANKING AND FINANCE LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(329, 10099, 'SHINEP', 'NPE097A40009', '2', 'SHINE RESUNGA DEVELOPMENT BANK LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(330, 10216, 'PICLPO', 'NPE212A40004', '2', 'PRUDENTIAL INSURANCE COMPANY LIMITED-PROMOTER SHARE', 1, NULL, NULL, NULL),
(331, 10210, 'NICLPO', 'NPE206A40006', '2', 'NEPAL INSURANCE COMPANY LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(332, 10209, 'IGI', 'NPE205A00002', '1', 'IME GENERAL INSURANCE  LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(333, 10326, 'SPDL', 'NPE296A00001', '1', 'SYNERGY POWER DEVELOPMENT LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(334, 10339, 'SMB', 'NPE308A00004', '1', 'SUPPORT MICROFINANCE BITTIYA SANSTHA LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(335, 10020, 'JBNLPO', 'NPE020A40001', '2', 'JANATA BANK NEPAL LIMITED- PROMOTER SHARE', 1, NULL, NULL, NULL),
(336, 10083, 'NLBBLP', 'NPE081A40003', '2', 'NERUDE LAGHUBITTA BITTIYA SANSTHA LIMITED- PORMOTER SHARE', 1, NULL, NULL, NULL),
(337, 10135, 'GUFL', 'NPE133A00006', '1', 'GURKHAS  FINANCE LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(338, 10315, 'GRDBL', 'NPE285A00004', '1', 'GREEN DEVELOPMENT BANK LIMITED - ORDINARY SHARE', 1, NULL, NULL, NULL),
(339, 10315, 'GRDBLP', 'NPE285A40000', '2', 'GREEN DEVELOPMENT BANK LIMITED - PROMOTER SHARE', 1, NULL, NULL, NULL),
(340, 10352, 'PMHPL', 'NEP317A00005', '1', 'PANCHAKANYA MAI HYDROPOWER LTD - ORDINARY SHARE', 1, NULL, '2018-10-14 07:00:10', '2018-10-14 07:00:10'),
(341, 10353, 'KPCL', 'NEP319A00001', '1', 'KALIKA POWER COMPANY LIMITED - ORDINARY SHARE', 1, NULL, '2018-10-14 07:02:35', '2018-10-15 06:45:18'),
(342, 10029, 'NICAD8283', 'NPE029A11029', '12', 'NIC ASIA Debenture 2082/083', 1, NULL, '2018-12-04 11:12:53', '2018-12-04 11:14:12');

-- --------------------------------------------------------

--
-- Table structure for table `isin_type`
--

CREATE TABLE `isin_type` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `isin_type`
--

INSERT INTO `isin_type` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Ordinary Share', '2018-09-23 09:03:07', '0000-00-00 00:00:00'),
(2, 'Promoter Share', '2018-10-22 07:14:37', '0000-00-00 00:00:00'),
(3, 'Mutual Fund', '2018-11-02 17:40:46', '0000-00-00 00:00:00'),
(4, 'Preference Share', '2018-11-02 17:43:25', '0000-00-00 00:00:00'),
(5, 'Debenture 2081/82', '2018-11-02 17:47:08', '0000-00-00 00:00:00'),
(6, 'Debenture 2077', '2018-11-02 17:48:55', '0000-00-00 00:00:00'),
(7, 'Debenture 2078', '2018-11-02 17:49:18', '0000-00-00 00:00:00'),
(8, 'Debenture 2076', '2018-11-02 17:49:30', '0000-00-00 00:00:00'),
(9, 'Debenture 2079', '2018-11-02 17:50:21', '0000-00-00 00:00:00'),
(10, 'Debenture 2080', '2018-11-02 17:52:37', '0000-00-00 00:00:00'),
(11, 'Sammunat Scheme', '2018-11-02 17:52:17', '0000-00-00 00:00:00'),
(12, 'Debenture 2082/83', '2018-12-04 06:10:12', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `issuer`
--

CREATE TABLE `issuer` (
  `issuer_id` int(10) UNSIGNED NOT NULL,
  `rta_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reg_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pan_no` int(11) DEFAULT NULL,
  `issued_capital` double NOT NULL,
  `listed_capital` double NOT NULL,
  `last_modification_date` date DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `issuer`
--

INSERT INTO `issuer` (`issuer_id`, `rta_id`, `name`, `address`, `phone`, `email`, `company_code`, `reg_no`, `pan_no`, `issued_capital`, `listed_capital`, `last_modification_date`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(10001, 101, 'AGRICULTURAL DEVELOPMENT BANK LIMITED', 'RAMSHAH PATH ,,KATHMANDU', '014264016,', '0', '001A', '928', 500049734, 8505216000, 8505216000, '0000-00-00', 1, NULL, NULL, NULL),
(10003, 116, 'BANK OF KATHMANDU LIMITED', 'HOUSE NO. 2/433, WARD NO. 1 ,KAMALPOKHARI,KATHMANDU', '014414541,', 'info@boklumbini.com.np', '003A', '103350', 500049255, 3060479000, 3060479000, '0000-00-00', 1, NULL, NULL, NULL),
(10004, 103, 'LAXMI BANK LIMITED', 'HATTISAR ,,KATHMANDU', '014444684,014444685', 'info@laxmibank.com', '004A', '802/058/59', 300277409, 2963308044, 2963308044, '0000-00-00', 1, NULL, NULL, NULL),
(10005, 109, 'HIMALAYAN GENERAL INSURANCE COMPANY LIMITED', 'BABARMAHAL ,,KATHMANDU', '014231788,01423014', 'ktm@hgi.com.np', '005A', '25/045', 500056246, 503328000, 503328000, '0000-00-00', 1, NULL, NULL, NULL),
(10006, 120, 'SHIKHAR INSURANCE COMPANY LTD.', 'SHIKHAR BIZ CENTRE ,THAPATHALI,KATHMANDU', '01-4246101,01-4246102', 'info@shikharinsurance.com', '006A', '878/060/061', 301715445, 422924200, 422924200, '0000-00-00', 1, NULL, NULL, NULL),
(10007, 164, 'NEPAL SBI BANK LTD.', 'HATTISAR ,,KATHMANDU', '014435516,', 'corporate@nsbl.com.np', '007A', '17/049-50', 300323003, 2413911528, 2413911528, '0000-00-00', 1, NULL, NULL, NULL),
(10008, 109, 'SIDDHARTHA INSURANCE LIMITED', 'BABARMAHAL ,,KATHMANDU', '014257766,014256190', 'sil@info.com.np', '008A', '787/057/58', 302148480, 314296300, 314296300, '0000-00-00', 1, NULL, NULL, NULL),
(10009, 142, 'SIDDHARTHA BANK LIMITED', 'KATHMANDU METROPOLITAN CITY ,WARD NO. 1, HATTISAR,KATHMANDU', '014442919,014442920', 'sbl@siddharthabank.com', '009A', '804/058/059', 300433504, 4150555030, 4150555030, '0000-00-00', 1, NULL, NULL, NULL),
(10010, 116, 'PRIMELIFE INSURANCE COMPANY LIMITED', 'HATTISAR ,,KATHMANDU', '014441414,', 'info@primelifenepal.com', '010A', '1003-063-064', 302889804, 535450500, 535450500, '0000-00-00', 1, NULL, NULL, NULL),
(10011, 160, 'CITIZENS BANK INTERNATIONAL LIMITED', 'NARAYANHITY PATH ,,KATHMANDU', '4427842,4427843', 'info@ctznbank.com', '011A', '979/063/064', 302406436, 3959013300, 3959013300, '0000-00-00', 1, NULL, NULL, NULL),
(10012, 110, 'CIVIL BANK LIMITED', 'KATHMANDU-31 ,KAMALADI,KATHMANDU', '014169040,014169030', 'info@civilbank.com.np', '012A', '1247/067/068', 304425824, 3702248285, 3702248285, '0000-00-00', 1, NULL, NULL, NULL),
(10013, 108, 'NEPAL BANGLADESH BANK LIMITED', 'BIJULI BAZAAR ,NEW BANESHWOR,KATHMANDU', '014783975,014783976', 'mdoffice@nbbl.com.np', '013A', '50/050/51', 500009105, 4175068800, 4175068800, '0000-00-00', 1, NULL, NULL, NULL),
(10014, 115, 'SUNRISE BANK LIMITED', 'GAIRIDHARA CROSSING ,KATHMANDU-2,KATHMANDU', '014004560,014004562', 'info@sunrisebank.com.np', '014A', '1018/064-065', 302669770, 3986599800, 3986599800, '0000-00-00', 1, NULL, NULL, NULL),
(10016, 126, 'EVEREST BANK LIMITED', 'EBL HOUSE ,LAZIMPAT,KATHMANDU', '014443377,', 'ebl@mos.com.np', '016A', '71/049/50', 500061903, 120000000, 120000000, '0000-00-00', 1, NULL, NULL, NULL),
(10017, 108, 'GLOBAL IME BANK LIMITED', 'PANIPOKHARI - 3 ,,KATHMANDU', '014002507,014002508', 'info@globalimebank.com.np', '017A', '962/062/63', 302122026, 4337957474, 4337957474, '0000-00-00', 1, NULL, NULL, NULL),
(10019, 112, 'HIMALAYAN BANK LIMITED', 'KAMALADI ,KATHMANDU,KATHMANDU', '014227749,014246219', 'himal@himalayanbank.com', '019A', '63/048/49', 500087158, 6897349990, 6897349990, '0000-00-00', 1, NULL, NULL, NULL),
(10020, 162, 'JANATA BANK NEPAL LIMITED', 'SHANKHAMUL MARG ,NEW BANESHWOR,KATHMANDU', '014786100,014785900', 'info@janatabank.com.np', '020A', '1204/066/067', 304010998, 3876140757, 3876140757, '0000-00-00', 1, NULL, NULL, NULL),
(10021, 123, 'KUMARI BANK LIMITED', 'DURBAR MARG ,,KATHMANDU', '014221311,014221314', 'info@kumaribank.com', '021A', '723/056', 500009099, 3510063510, 3510063510, '0000-00-00', 1, NULL, NULL, NULL),
(10022, 119, 'PRABHU BANK LIMITED', 'BABARMAHAL-11 ,KATHMANDU,KATHMANDU', '014788500,', 'info@prabhubank.com', '022A', '786/057/58', 301212515, 3185668600, 3185668600, '0000-00-00', 1, NULL, NULL, NULL),
(10024, 114, 'MACHHAPUCHCHHRE BANK LIMITED', 'LAZIMPAT ,,KATHMANDU', '014428556,', 'machbank@mbl.com.np', '024A', '678/054/55', 300225961, 4108403430, 4108403430, '0000-00-00', 1, NULL, NULL, NULL),
(10025, 116, 'NABIL BANK LIMITED', 'TINDHARA ,DURBAR MARG,KATHMANDU', '014227181,014221718', 'nabil@nabilbank.com', '025A', '1-2040/41', 500184567, 2413187500, 2413187500, '0000-00-00', 1, NULL, NULL, NULL),
(10026, 110, 'NEPAL BANK LIMITED', 'DHARMAPATH, KATHMANDU ,,KATHMANDU', '01-4222397,01-4222365', 'info@nepalbank.com.np', '026A', '958/062/063', 301119272, 8008185800, 8008185800, '0000-00-00', 1, NULL, NULL, NULL),
(10027, 115, 'NEPAL CREDIT AND COMMERCE BANK LIMITED', 'SIDDHARTHA NAGAR ,BHAIRAHWA,BHAIRAHWA', '071521952,014246991', 'corporate@nccbank.com.np', '027A', '603', 301269328, 2386319700, 2386319700, '0000-00-00', 1, NULL, NULL, NULL),
(10028, 120, 'NEPAL INVESTMENT BANK LIMITED', 'DURBARMARG ,,KATHMANDU', '014228229,014242530', 'info@nibl.com.np', '028A', '05/042', 500093638, 3301883700, 3301883700, '0000-00-00', 1, NULL, NULL, NULL),
(10029, 166, 'NIC ASIA BANK LIMITED', 'MAIN ROAD-3 ,BIRATNAGAR,BIRATNAGAR', '021521921,015111177', 'info@nicasiabank.com', '029A', '658', 300756090, 3935247329, 3935247329, '0000-00-00', 1, NULL, NULL, '2018-12-04 10:56:49'),
(10030, 109, 'NMB BANK LIMITED', 'NMB BHAWAN ,BABARMAHAL,KATHMANDU', '014246160,', 'call@nmb.com.np', '030A', '238/052/53', 500106495, 3725612440, 3725612440, '0000-00-00', 1, NULL, NULL, NULL),
(10032, 110, 'PRIME COMMERCIAL BANK LIMITED', 'KA.MA.NA.PA. WARD NO. 31 ,KAMALPOKHARI,KATHMANDU', '014423433,', 'info@pcbl.com.np', '031A', '1007', 302599853, 3936316464, 3936316464, '0000-00-00', 1, NULL, NULL, NULL),
(10033, 161, 'SANIMA BANK LIMITED', 'NARAYAN CHOUR ,NAXAL,KATHMANDU', '014428979,014428980', 'sanima@sanimabank.com', '032A', '884/060/61', 301638418, 4081612785, 4081612785, '0000-00-00', 1, NULL, NULL, NULL),
(10034, 121, 'STANDARD CHARTERED BANK NEPAL LIMITED', 'NAYA BANESHWOR ,KATHMANDU,KATHMANDU', '014782333,', 'ir.nepal@sc.com', '033A', '4/042', 500058631, 2386578633, 2386578633, '0000-00-00', 1, NULL, NULL, NULL),
(10035, 116, 'MEGA BANK NEPAL LIMITED', 'KAMALADI ,KATHMANDU,KATHMANDU', '014169217,014257711', 'company.secretary@megabank.com.np', '034A', '1229/066/67', 304301241, 5043405080, 5043405080, '0000-00-00', 1, NULL, NULL, NULL),
(10042, 109, 'BHARGAV BIKASH BANK LIMITED', 'SURKHET ROAD ,NEPALGUNJ,NEPALGUNJ', '081415027,081415037', 'info@bhargavbank.com.np', '041A', '983/063/064', 302522002, 190080000, 190080000, '0000-00-00', 1, NULL, NULL, NULL),
(10045, 119, 'GANDAKI BIKAS BANK LIMITED .', 'POKHARA-4, CHIPLEDHUNGA ,BP CHOWK, KASKI,KASKI', '061528001,061528002', 'info@gandakibank.com.np', '044A', '1029/064/65', 302719897, 1175502990.5, 1175502990.5, '0000-00-00', 1, NULL, NULL, NULL),
(10047, 109, 'CHHIMEK LAGHUBITTA BITTIYA SANSTHA LIMITED', 'NEW BANESHWOR ,KATHMANDU,KATHMANDU', '014490513,', 'cbbhtd@ntc.net.np', '046A', '800/058/59', 300283349, 490000000, 490000000, '0000-00-00', 1, NULL, NULL, NULL),
(10049, 120, 'OM DEVELOPMENT BANK LIMITED', 'POKHARA-4 ,CHIPLEDHUNGA,POKHARA', '061521505,061521912', 'mail@citybanknepal.com', '047A', '1013/064-65', 302647707, 1232464100, 1232464100, '0000-00-00', 1, NULL, NULL, NULL),
(10052, 110, 'CORPORATE DEVELOPMENT BANK LIMITED', 'ADARSHANAGAR WARD NO. 13 ,,BIRGUNJ', '051531031,', 'info@corporatebank.com.np', '050A', '1016/064/065', 302637212, 60000000, 60000000, '0000-00-00', 1, NULL, NULL, NULL),
(10053, 101, 'EXCEL DEVELOPMENT BANK LIMITED', 'ANARMANI - 3 ,BIRTAMODE,BIRTAMODE', '023543564,023543714', 'info@edb.com.np', '051A', '922/061/062', 301931043, 353263727, 353263727, '0000-00-00', 1, NULL, NULL, NULL),
(10054, 108, 'FIRST MICROFINANCE LAGHU BITTA BITTIYA SANSTHA LIMITED', 'CHARTER TOWER ,GYANESHOR-33,KATHMANDU', '014425358,014425361', 'info@fmdb.com.np', '052A', '1181/066/067', 303854726, 335352900, 335352900, '0000-00-00', 1, NULL, NULL, NULL),
(10055, 101, 'GARIMA BIKAS BANK LIMITED', 'MAHENDRAPOOL, WARD NO-9 ,POKHARA METROPOLITAN CITY,POKHARA', '061533694,061533695', 'info@garimabank.com.np', '053A', '1014/2064/64', 302648157, 1292788798, 1292788798, '0000-00-00', 1, NULL, NULL, NULL),
(10063, 120, 'JYOTI BIKASH BANK LIMITED', 'KMC WARD NO. 31 ,KAMALADI,KATHMANDU', '014411116,014427550', 'info@jbbl.com.np', '061A', '1065/064/065', 302961135, 1033740190, 1033740190, '0000-00-00', 1, NULL, NULL, NULL),
(10064, 108, 'KANCHAN DEVELOPMENT BANK LIMITED', 'BHIMDUTTANAGAR-4 ,KANCHANPUR,KANCHANPUR', '099520265,', 'info@kanchanbank.com.np', '062A', '1159/066/067', 303499033, 258060000, 258060000, '0000-00-00', 1, NULL, NULL, NULL),
(10065, 108, 'KAILASH BIKAS BANK LIMITED', 'NEW PLAZA MARGA ,PUTALISADAK,KATHMANDU', '014443034,014432139', 'info@kailashbank.com', '063A', '10-049/50', 300937990, 1235111600, 1235111600, '0000-00-00', 1, NULL, NULL, NULL),
(10067, 120, 'KABELI BIKAS BANK LIMITED', 'DHANKUTA -7 ,DHANKUTA,DHANKUTA', '026521435,', 'kbbremit@gmail.com', '065A', '1025/064/065', 302571956, 103188935, 103188935, '0000-00-00', 1, NULL, NULL, NULL),
(10070, 109, 'KAMANA SEWA BIKAS BANK LIMITED', 'POKHARA -8 ,SRIJANACHOWK, KASKI,POKHARA', '061539672,061539673', '0', '068A', '996/063/64', 302645891, 1226301204, 1226301204, '0000-00-00', 1, NULL, NULL, NULL),
(10071, 142, 'KANKAI BIKAS BANK LIMITED', 'DAMAK- 11 ,JHAPA,DAMAK', '023584749,023585449', 'kbbl@kankaibank.com.np', '069A', '973/063-64', 302411331, 245000000, 245000000, '0000-00-00', 1, NULL, NULL, NULL),
(10072, 119, 'KARNALI DEVELOPMENT BANK LIMITED', 'NEPALGUNJ ,BANKE,NEPALGUNJ', '081526014,081526015', 'kdbl@ntc.net.np', '070A', '834/059/60', 301288048, 246386700, 246386700, '0000-00-00', 1, NULL, NULL, NULL),
(10074, 119, 'MITERI DEVELOPMENT BANK LIMITED', 'MAHENDRA PATH ,DHARAN - 5,DHARAN', '025531317,', 'info@miteribank.com.np', '072A', '955/062/063', 302157596, 244999950.8, 244999950.8, '0000-00-00', 1, NULL, NULL, NULL),
(10077, 120, 'MUKTINATH BIKAS BANK LIMITED', 'POKHARA- 09 ,PRITHVI CHOWK,POKHARA', '061522786,061522787', 'info@muktinathbank.com.np', '075A', '956/062/063', 302349296, 1269964025.33, 1269964025.33, '0000-00-00', 1, NULL, NULL, NULL),
(10080, 120, 'DEVA BIKAS BANK LIMITED', 'LALDURBAR MARG ,KATHMANDU,KATHMANDU', '014256717,014256657', 'info@devabank.com.np', '078A', '951/062/063', 302208410, 1120431097, 1120431097, '0000-00-00', 1, NULL, NULL, NULL),
(10082, 117, 'NIDC DEVELOPMENT BANK LIMITED', 'NIDC BUILDING ,DURBARMARG,KATHMANDU', '014228322,014227428', 'nidc@nidc.org.np', '080A', '957/062/63', 302293692, 3091000, 3091000, '0000-00-00', 1, NULL, NULL, NULL),
(10083, 115, 'NERUDE LAGHUBITTA BITTIYA SANSTHA LIMITED', 'Biratnagar Ma.Na.Pa. Ward No.3, Bargachhi (Bhupi Ballav Marg)', '021-462321, 021-463032, 021-462101', 'nerude@wlink.com.np', '081A', '989/063/064', 302430749, 169432200, 169432200, '2018-10-07', 1, NULL, NULL, '2018-10-07 13:49:41'),
(10084, 120, 'NIRDHAN UTTHAN LAGHUBITTA BITTIYA SANSTHA LIMITED', 'NAXAL ,BHAGAWATIBAHAL,KATHMANDU', '014413711,014413794', 'info@nirdhan.com', '082A', '691/055/56', 300307176, 490000000, 490000000, '0000-00-00', 1, NULL, NULL, NULL),
(10091, 142, 'PURNIMA BIKAS BANK LIMITED', 'SNP-8, RUPANDEHI ,BHAIRAHAWA,RUPANDEHI', '071520856,071527319', 'info@purnimabank.com', '089A', '1059/064/065', 302829105, 257265393, 257265393, '0000-00-00', 1, NULL, NULL, NULL),
(10095, 120, 'SHANGRILA DEVELOPMENT BANK LIMITED', 'BALUWATAR ,KATHMANDU,KATHMANDU', '014421861,014421862', 'info@shangrilabank.com', '093A', '871/060/61', 301655248, 1198176100, 1198176100, '0000-00-00', 1, NULL, NULL, NULL),
(10096, 166, 'SAHAYOGI VIKAS BANK LIMITED', 'WARD NO. 4 ,VIDYAPATI CHOWK,JANAKPUR', '041525972,041527124', 'sahayogibank@yahoo.com', '094A', '843/059/060', 301312013, 249737400, 249737400, '0000-00-00', 1, NULL, NULL, NULL),
(10099, 120, 'SHINE RESUNGA DEVELOPMENT BANK LIMITED', 'SIDDHARTHA ROAD ,BUTWAL - 8,BUTWAL', '071551500,071551498', 'info@srdb.com.np', '097A', '1080/065/066', 303287340, 506995800, 506995800, '0000-00-00', 1, NULL, NULL, NULL),
(10100, 109, 'SWAROJGAR LAGHUBITTA BITTIYA SANSTHA LIMITED', 'BANEPA - 10 ,KAVRE,BANEPA', '011661060,', 'swarojgarbank@yahoo.com', '098A', '1157/066/067', 303863289, 60023902, 60023902, '0000-00-00', 1, NULL, NULL, NULL),
(10101, 110, 'Summit Laghubitta Bittiya Sanstha Ltd', 'BIRTAMODE-7 ,JHAPA,BIRTAMODE', '023541420,023544420', 'banksummit@yahoo.com', '099A', '98092/065/66', 303546360, 100656000, 100656000, '2018-10-10', 1, NULL, NULL, '2018-10-10 10:20:17'),
(10104, 120, 'SWABALAMBAN LAGHUBITTA BITTIYA SANSTHA LIMITED', 'BALUWATAR ,,KATHMANDU', '014434921,014434922', 'swbbl@ntc.net.np', '102A', '807/058/59', 300854280, 352177500, 352177500, '0000-00-00', 1, NULL, NULL, NULL),
(10107, 142, 'TINAU DEVELOPMENT BANK LIMITED', 'BUTWAL-8 ,HOSPITAL LINE,BUTWAL', '071545205,071545576', 'info@tinaubank.com', '105A', '952/062/063', 302138715, 248396596, 248396596, '0000-00-00', 1, NULL, NULL, NULL),
(10110, 119, 'WESTERN DEVELOPMENT BANK LIMITED', 'GHORAHI-10 ,DANG,GHORAHI', '082560732,082561579', 'wdbldang@yahoo.com', '108A', '906/061/062', 301981215, 236851430, 236851430, '0000-00-00', 1, NULL, NULL, NULL),
(10112, 142, 'DEPROSC LAGHUBITTA BITTIYA SANSTHA LIMITED', 'SHAHEEDPATH ,NARAYANGARH,CHITWAN', '056527900,014288650', 'ddbank@wlink.com.np', '110A', '767/057/58', 301235073, 344519142, 344519142, '0000-00-00', 1, NULL, NULL, NULL),
(10114, 142, 'MAHALAXMI BIKAS BANK LIMITED', 'KATHMANDU MUNICIPALITY ,WARD NO. 1, DURBARMARG,KATHMANDU', '014268719,014223735', '0', '112A', '105435/056/057', 500034363, 1211964229, 1211964229, '0000-00-00', 1, NULL, NULL, NULL),
(10116, 108, 'HATHWAY FINANCE COMPANY LIMITED', 'BHANU CHOWK 6 ,DHARAN,  SUNSARI,DHARAN', '025533930,025533931', 'arunfinanceltd@gmail.com', '114A', '145/051/052', 300827251, 120000000, 120000000, '0000-00-00', 1, NULL, NULL, NULL),
(10118, 119, 'SYNERGY FINANCE LIMITED', 'KAMALADI ,,KATHMANDU', '014442461,014442462', 'corporate@synergyfinance.com.np', '116A', '105298', 300666508, 179499500, 179499500, '0000-00-00', 1, NULL, NULL, NULL),
(10119, 109, 'CENTRAL FINANCE  LIMITED', 'WARD NO. 10, KUPONDOLE ,LALITPUR,LALITPUR', '015544517,', 'central@ntc.net.np', '117A', '514/052/53', 500061141, 369074002, 369074002, '0000-00-00', 1, NULL, NULL, NULL),
(10121, 118, 'CITIZEN INVESTMENT TRUST', 'NEW BANESHWOR ,,KATHMANDU', '014784945,014785321', 'info@nlk.org.np', '119A', 'CIT ACT 2047', 300981807, 903123145, 903123145, '0000-00-00', 1, NULL, NULL, NULL),
(10122, 170, 'CAPITAL MERCHANT BANKING AND FINANCE LIMITED', 'BATTISPUTALI ,,KATHMANDU', '014471458,', 'info@cmbfl.com.np', '120A', '791/057-58', 300434314, 458184100, 458184100, '0000-00-00', 1, NULL, NULL, NULL),
(10126, 120, 'GOODWILL FINANCE LIMITED', 'HATTISAR ,KAMALPOKHARI, KATHMANDU,KATHMANDU', '014444039,', 'goodwill@finance.wlink.com.np', '124A', '97/051/52', 500046719, 408000000, 408000000, '0000-00-00', 1, NULL, NULL, NULL),
(10127, 108, 'BEST FINANCE COMPANY LIMITED', 'CHABAHIL ,KATHMADNU,KATHMANDU', '014484753,014484754', '0', '125A', '177/051/52', 500066342, 178359000, 178359000, '0000-00-00', 1, NULL, NULL, NULL),
(10128, 148, 'GUHESWORI MERCHANT BANKING AND FINANCE LIMITED', 'HARIHAR BHAWAN ,PULCHOWK,LALITPUR', '015537407,015550406', 'gmbf@wlink.com.np', '126A', '776/057/058', 300801035, 392033755, 392033755, '0000-00-00', 1, NULL, NULL, NULL),
(10130, 108, 'ICFC FINANCE LIMITED', 'THIRBAM SADAK - 3 ,BHATBHATENI,KATHMANDU', '014425292,', 'info@icfcbank.com', '128A', '854/060/61', 301629373, 449806900, 449806900, '0000-00-00', 1, NULL, NULL, NULL),
(10133, 115, 'JANAKI FINANCE COMPANY LIMITED', 'JANAKPUR-2 ,,JANAKPUR', '041521586,041523339', 'info@jfcjanakpur.com; janakicompanyltd@gmail.com', '131A', '246/052/53', 301012519, 205731700, 205731700, '0000-00-00', 1, NULL, NULL, NULL),
(10135, 120, 'GURKHAS FINANCE LIMITED .', 'DILLI BAZAR, KATHMANDU ,,KATHMANDU', '014430527,014437401', 'info@gurkhasfinance.com.np', '133A', '79/051/52', 500060061, 499199500, 499199500, '0000-00-00', 1, NULL, NULL, NULL),
(10136, 165, 'CITY EXPRESS FINANCE COMPANY LIMITED', 'DURBARMARGA, GHANTAGHAR ,,KATHMANDU', '014244099,014244199', 'info@cityexpressfinance.com.np', '134A', '931/2062/063', 302099887, 67500000, 67500000, '0000-00-00', 1, NULL, NULL, NULL),
(10137, 127, 'LALITPUR FINANCE LIMITED', 'LAGANKHEL ,WARD NO. 5,LALITPUR', '015536598,015523850', 'lfcl@wlink.com.np', '135A', '147/051-052', 500056561, 84520400, 84520400, '0000-00-00', 1, NULL, NULL, NULL),
(10140, 101, 'MANJUSHREE FINANCE LIMITED', 'OMKAR BUILDING, NEW BANESHWOR ,KATHMANDU 10,KATHMANDU', '014782517,014784170', 'info@manjushreefinance.com.np', '137A', '1000/063-64', 302698792, 393989502.9, 393989502.9, '0000-00-00', 1, NULL, NULL, NULL),
(10142, 110, 'MULTIPURPOSE FINANCE COMPANY LIMITED', 'RAJBIRAJ -3, SAPTARI ,NEPAL,SAPTARI', '031521170,', 'MFCLfin43@gmail.com', '139A', '644/053/54', 300492033, 24884200, 24884200, '0000-00-00', 1, NULL, NULL, NULL),
(10146, 108, 'NEPAL EXPRESS FINANCE LIMITED', 'SUNDHARA ,KATHMANDU,KATHMANDU', '014268056,014268057', 'nepexfinance@wlink.com.np', '143A', '941/062/063', 302133514, 134665800, 134665800, '0000-00-00', 2, NULL, NULL, NULL),
(10147, 108, 'NEPAL FINANCE LIMITED', 'KAMALADI ,,KATHMANDU', '014220031,014247020', 'mail@nefinsco.com', '144A', '66/049/50', 500059409, 67809900, 67809900, '0000-00-00', 1, NULL, NULL, NULL),
(10155, 120, 'POKHARA FINANCE LIMITED', 'NEWROAD, POKHARA 9 ,KASKI,KASKI', '061531145,', 'info@pokharafinance.com.np', '152A', '314/052/53', 300441710, 369536815, 369536815, '0000-00-00', 1, NULL, NULL, NULL),
(10157, 120, 'PROGRESSIVE FINANCE LIMITED', 'PAKO ,NEW ROAD, KATHMANDU,KATHMANDU', '014246402,014241244', 'info@pfltd.com.np', '154A', '98/051/52', 500055304, 102900000, 102900000, '0000-00-00', 1, NULL, NULL, NULL),
(10160, 115, 'SRIJANA FINANCE LIMITED', 'BIRATNAGAR-8 ,MORANG,MORANG', '021528058,021527510', 'srijanafinancebrt@yahoo.com', '157A', '684/054/055', 300445815, 160272000, 160272000, '0000-00-00', 1, NULL, NULL, NULL),
(10162, 142, 'SHREE INVESTMENT AND FINANCE COMPANY LIMITED', 'DILLIBAZAR ,,KATHMANDU', '014422038,014426146', 'info@shreefinance.com.np', '159A', '75/050/51', 500060656, 295160842, 295160842, '0000-00-00', 1, NULL, NULL, NULL),
(10168, 139, 'UNITED FINANCE LIMITED', 'I.J. PLAZA ,DURBARMARG,KATHMANDU', '014241648,014222663', 'info@ufl.com.np', '164A', '6-049/50', 500058602, 392254342.1, 392254342.1, '0000-00-00', 1, NULL, NULL, NULL),
(10170, 167, 'WORLD MERCHANT BANKING AND FINANCE LIMITED', 'HETAUDA MUNICIPALITY-1 ,MAKAWANPUR,MAKAWANPUR', '057525100,057521479', 'wmbfl2015@gmail.com', '166A', '742/056/57', 300578784, 73945100, 73945100, '0000-00-00', 1, NULL, NULL, NULL),
(10171, 120, 'ORIENTAL HOTELS LIMITED', 'LAZIMPAT ,KATHMANDU-2,KATHMANDU', '014411818,014423888', 'legal@radkat.com.np', '167A', '1465/050/51', 300048719, 934236527, 934236527, '0000-00-00', 1, NULL, NULL, NULL),
(10172, 141, 'SOALTEE HOTEL LIMITED', 'TAHACHAL ,KATHMANDU,KATHMANDU', '014272567,014273999', 'legal@soaltee.com.np', '168A', '96/032', 300047697, 666063740, 666063740, '0000-00-00', 1, NULL, NULL, NULL),
(10173, 130, 'TARAGAON REGENCY HOTELS LIMITED', 'BOUDDHA ,,KATHMANDU', '014491234,014490035', 'info@taragaon.com', '169', '1805051', 300052637, 2500000000, 1886654000, '2018-10-04', 1, NULL, NULL, '2018-10-04 14:29:21'),
(10176, 116, 'BOTTLERS NEPAL LIMITED', 'BALAJU ,KATHMANDU,KATHMANDU', '014360602,014351871', 'prburma@coca-cola.com.np', '172A', '140/041', 300048362, 194888700, 194888700, '0000-00-00', 1, NULL, NULL, NULL),
(10177, 116, 'BOTTLERS NEPAL (TERAI) LIMITED', 'BHARATPUR ,CHITWAN,CHITWAN', '056520316,', 'prburma@coca-cola.com.np', '173A', '153/042', 300010295, 121000000, 121000000, '0000-00-00', 1, NULL, NULL, NULL),
(10183, 116, 'HIMALAYAN DISTILLERY LIMITED', 'LIPNI BIRTA, 7 PARSA ,,PARSA', '015522010,', 'hd@ecomail.com.np', '179A', '757.', 300033962, 385645500, 385645500, '0000-00-00', 1, NULL, NULL, NULL),
(10187, 161, 'NEPAL LUBE OIL LIMITED', 'AMLEKHGUNJ, BARA NEPAL ,,AMLEKHGUNJ', '015545891,015545892', 'nlol@cg.holdings', '183A', '137/041', 300020298, 26943900, 26943900, '0000-00-00', 1, NULL, NULL, NULL),
(10191, 142, 'SRI RAM SUGAR MILLS LIMITED', 'GOLCHHA HOUSE, GANABAHAL- 22 ,,KATHMANDU', '014250001,', 'shriramsugar@gmail.com', '187A', '99770', 300020346, 304599000, 304599000, '0000-00-00', 1, NULL, NULL, NULL),
(10192, 115, 'UNILEVER  NEPAL LIMITED', '4TH FLOOR, HERITAGE PLAZA-II ,KAMALADI,KATHMANDU', '014169151,057411047', 'deepikasharma@nll.com.np', '188A', '202-048/049', 300006007, 92070000, 92070000, '0000-00-00', 1, NULL, NULL, NULL),
(10194, 140, 'NEPAL DOORSANCHAR COMPANY LIMITED (NTC)', 'BHADRAKALI PLAZA ,KATHMANDU - 11,KATHMANDU', '014210288,', 'cs.nt@ntc.net.np', '190A', '869/060/061', 300044614, 15000000000, 15000000000, '0000-00-00', 1, NULL, NULL, NULL),
(10195, 127, 'ARUN VALLEY HYDROPOWER DEVELOPMENT COMPANY LIMITED', 'WARD NO.: 34, BANESHWOR ,KATHMANDU,KATHMANDU', '015111085,015111086', 'arunvalley2054@gmail.com', '191A', '934/062/63', 500103814, 933012300, 933012300, '0000-00-00', 1, NULL, NULL, NULL),
(10196, 109, 'BUTWAL POWER COMPANY LIMITED', 'GANGA DEVI MARGA - 313 ,BUDDHANAGAR,KATHMANDU', '014781776,014785295', 'info@bpc.com.np', '192A', '3/049/50', 500047963, 2218672000, 2218672000, '0000-00-00', 1, NULL, NULL, NULL),
(10197, 142, 'CHILIME HYDROPOWER COMPANY LIMITED', 'DHUMBARAHI ,KATHMANDU,KATHMANDU', '014370773,014370793', 'chpcl@wlink.com.np', '193A', '250/052/53', 500059898, 3965113152, 3965113152, '0000-00-00', 1, NULL, NULL, NULL),
(10198, 108, 'NATIONAL HYDROPOWER COMPANY LIMITED', 'BALUWATAR ,KATHMANDU,KATHMANDU', '014426142,', 'hydronational@yahoo.com', '194A', '340/052/53', 500163034, 1385186200, 1385186200, '0000-00-00', 1, NULL, NULL, NULL),
(10199, 157, 'BISHAL BAZAR COMPANY LIMITED', 'SUKRAPATH, KATHMANDU ,NEPAL,KATHMANDU', '014242185,', 'info@bishalbazar.com.np', '195A', '82', 500051775, 49140000, 49140000, '0000-00-00', 1, NULL, NULL, '2018-10-01 11:18:10'),
(10202, 120, 'SALT TRADING CORPORATION LIMITED', 'KALIMATI ,KATHMANDU,KATHMANDU', '014271208,014271014', 'admin@stcnepal.com', '198A', '72/2020', 300045710, 122833400, 122833400, '0000-00-00', 1, NULL, NULL, NULL),
(10203, 119, 'PRABHU INSURANCE LIMITED', 'TINKUNE ,KATHMANDU,KATHMANDU', '014499220,', 'info@prabhuinsurance.com', '199A', '183/051/052', 500105630, 337319973.6, 337319973.6, '0000-00-00', 1, NULL, NULL, NULL),
(10204, 120, 'ASIAN LIFE INSURANCE COMPANY LIMITED', 'ASIAN LIFE BHAWAN ,MAITIDEVI,KATHMANDU', '014430270,014410115', 'asianlife@asianlife.com.np', '200A', '1032/064/65', 302837205, 380143670, 380143670, '0000-00-00', 1, NULL, NULL, NULL),
(10205, 109, 'EVEREST INSURANCE COMPANY LIMITED', 'HATTISAR ,,KATHMANDU', '014445090,014445092', 'info@eic.com.np', '201A', '057/48/49', 500092677, 133650000, 133650000, '0000-00-00', 1, NULL, NULL, NULL),
(10206, 109, 'GURANS LIFE INSURANCE COMPANY LIMITED', 'SHREE RAJ BHAWAN 4TH FLOOR ,TINKUNE,KATHMANDU', '015199310,16600144400', 'info@guranslife.com', '202A', '1005/064/065', 302603835, 178200000, 178200000, '0000-00-00', 1, NULL, NULL, NULL),
(10207, 101, 'LUMBINI GENERAL INSURANCE COMPANY LIMITED', 'GYANESHWOR ,KATHMANDU,KATHMANDU', '014411707,014440818', 'lgic@mos.com.np', '203A', '106011', 301906010, 431200000, 431200000, '0000-00-00', 1, NULL, NULL, NULL),
(10208, 164, 'LIFE INSURANCE CORPORATION (NEPAL) LIMITED', 'STAR MALL 4 FLOOR, PUTALISADAK, KATHMANDU', '014443616,014443617', 'liccorporate@licnepal.com.np', '204A', '765/057/058', 500213148, 266935500, 266935500, '0000-00-00', 1, NULL, NULL, '2018-11-18 10:57:41'),
(10209, 108, 'IME GENERAL INSURANCE LIMITED', 'NARAYANCHAUR, NAXAL ,KATHMANDU,KATHMANDU', '014411510,', 'nbic@mos.com.np', '205A', '698/2055/10/12', 500190018, 432000000, 432000000, '0000-00-00', 1, NULL, NULL, NULL),
(10210, 120, 'NEPAL INSURANCE COMPANY LIMITED', 'Hama Iron & Steel Industries Building, Kamaladi, Ganeshthan, Kathmandu', '014221353,014228690', 'nic@wlink.com.np', '206A', '1088/065/066', 500056921, 303440938, 303440938, '0000-00-00', 1, NULL, NULL, NULL),
(10211, 133, 'NECO INSURANCE LIMITED', 'NAVADURGA BHAVAN ,ANAMNAGAR,KATHMANDU', '014770415,014770127', 'info@necoinsurance.com.np', '207A', '127/051/52', 500051106, 705767040, 705767040, '0000-00-00', 1, NULL, NULL, NULL),
(10212, 110, 'NLG INSURANCE COMPANY LIMITED', 'PANIPOKHARI ,LAZIMPAT,KATHMANDU', '014006625,014442646', 'nlgi@mail.com.np', '208A', '919-2061', 302248245, 320203100, 320203100, '0000-00-00', 1, NULL, NULL, NULL),
(10213, 111, 'NEPAL LIFE INSURANCE COMPANY LIMITED', 'HERITAGE PLAZA - 1 ,WARD NO. 31,KATHMANDU', '014169082,014169083', 'info@nepallife.com.np', '209A', '700/055/56', 500063433, 2638157135, 2638157135, '0000-00-00', 1, NULL, NULL, NULL),
(10214, 146, 'NATIONAL LIFE INSURANCE COMPANY LIMITED', 'LAZIMPAT, KATHMANDU ,,KATHMANDU', '014414799,', 'nlgilife@mail.com.np', '210A', '8/042', 500075236, 579627849.5, 579627849.5, '0000-00-00', 1, NULL, NULL, NULL),
(10215, 153, 'PREMIER INSURANCE COMPANY (NEPAL) LIMITED', 'NARAYAN CHAUR ,NAXAL,KATHMANDU', '014413543,014410648', 'premier@picl.com.np', '211A', '62/048/49', 500060418, 278523100, 278523100, '0000-00-00', 1, NULL, NULL, NULL),
(10216, 109, 'PRUDENTIAL INSURANCE COMPANY LIMITED', 'KAMALADI COMPLEX ,KAMALADI,KATHMANDU', '014212940,014212941', 'prudential@wlink.com.np', '212A', '763/057-58', 300723748, 128304000, 128304000, '0000-00-00', 1, NULL, NULL, NULL),
(10217, 120, 'RASTRIYA BEEMA COMPANY LIMITED', 'KATHMANDU METROPOLITAN CITY-11 ,RAMSHAHPATH, KATHMANDU,KATHMANDU', '014212412,', '0', '213A', '123480/70/071', 601885020, 32794122, 32794122, '0000-00-00', 1, NULL, NULL, NULL),
(10218, 108, 'SAGARMATHA INSURANCE COMPANY LIMITED', 'SURAKSHAN BHAWAN ,BHAGAWATI MARGA,KATHMANDU', '014412367,', 'info@sagarmathainsurance.com.np', '214A', '141/051/52', 500054420, 156104651, 156104651, '0000-00-00', 1, NULL, NULL, NULL),
(10219, 166, 'SURYA LIFE INSURANCE COMPANY LIMITED', 'SANO GAUCHARAN ,KATHMANDU,KATHMANDU', '014423743,', 'info@suryalife.com', '215A', '1006/063/064', 302837144, 303187500, 303187500, '0000-00-00', 1, NULL, NULL, NULL),
(10220, 108, 'UNITED INSURANCE COMPANY (NEPAL) LIMITED', 'TRADE TOWER ,THAPATHALI,KATHMANDU', '015111111,', 'uic@mail.com.np', '216A', '65A', 500058017, 122505000, 122505000, '0000-00-00', 1, NULL, NULL, NULL),
(10227, 115, 'JEBILS FINANCE LIMITED', 'NEW ROAD ,WARD NO. 24,KATHMANDU', '014220426,014220439', 'info@jebils.com', '222A', '1133/065/066', 303730442, 283177125, 283177125, '0000-00-00', 1, NULL, NULL, NULL),
(10229, 115, 'RELIANCE FINANCE LIMITED', 'B.J. BHAWAN ,PRADARSHANI MARG,KATHMANDU', '014261104,', 'info@reliancenepal.com.np', '224A', '1153/065/066', 303763691, 364182028, 364182028, '0000-00-00', 1, NULL, NULL, NULL),
(10231, 120, 'HAMRO BIKAS BANK LIMITED', 'BIDUR - 3, BATTAR ,NUWAKOT,NUWAKOT', '010561777,010561778', 'hamrobank@gmail.com', '226A', '1089/065/066', 303587040, 241197415, 241197415, '0000-00-00', 1, NULL, NULL, NULL),
(10233, 127, 'MISSION DEVELOPMENT BANK LIMITED', 'BUTWAL - 8 ,DURGAMANDIRLINE,BUTWAL', '071551586,071551587', 'info@missionbanknepal.com', '228A', '1182/066/067', 303787776, 155784060, 155784060, '0000-00-00', 1, NULL, NULL, NULL),
(10235, 119, 'GRAMEEN BIKAS LAGHUBITTA BITTIYA SANSTHA LIMITED', 'KALIKANAGAR,12 ,BUTWAL,BUTWAL', '071438951,071438952', 'info@grameenbanknepal.com', '230A', '110/051/52', 301348634, 197103100, 197103100, '0000-00-00', 1, NULL, NULL, NULL),
(10236, 115, 'RMDC LAGHUBITTA BITTIYA SANSTHA LIMITED', 'PUTALISADAK ,KATHMANDU,KATHMANDU', '014268019,014268020', 'rmdc@wlink.com.np', '231A', '692/055/56', 500044041, 218017800, 218017800, '0000-00-00', 1, NULL, NULL, NULL),
(10237, 120, 'SINDHU BIKASH BANK LIMITED', 'BANEPA ,KAVRE,KAVRE', '011662340,011662341', 'info@sindhubikashbank.com.np', '232A', '304006922', 0, 231253785, 231253785, '0000-00-00', 1, NULL, NULL, NULL),
(10238, 116, 'SANA KISAN BIKAS LAGHUBITTA BITTIYA SANSTHA LIMITED', 'SUBIDHANAGAR ,TINKUNE,KATHMANDU', '014111923,014111828', 'info@skbbl.com.np', '233A', '792/057/58', 301636483, 189093750, 189093750, '0000-00-00', 1, NULL, NULL, NULL),
(10239, 161, 'SANIMA MAI HYDROPOWER LIMITED', 'KATHMANDU-1, NAXAL ,NARAYANCHOUR,KATHMANDU', '014446442,014415314', 'sanimamai@sanimahydro.com', '234A', '1331/068/069', 303052414, 2110000000, 2110000000, '0000-00-00', 1, NULL, NULL, NULL),
(10240, 109, 'KALIKA LAGHUBITTA BITTIYA SANSTHA LIMITED', 'WALLING -3 ,,SYANGJA', '063440555,063440704', 'info@kalikabank.com.np', '235A', '1199/066/067', 303758079, 40000000, 40000000, '0000-00-00', 1, NULL, NULL, NULL),
(10241, 109, 'NAYA NEPAL LAGHUBITTA BITTIYA SANSTHA LIMITED', 'DHULIKHEL-6 ,KAVREPALANCHOWK,KAVREPALANCHOWK', '011490671,', 'nemfin@ntc.net.np', '236A', '1081/065/066', 303541844, 11760000, 11760000, '0000-00-00', 1, NULL, NULL, NULL),
(10245, 120, 'NEPAL COMMUNITY DEVELOPMENT BANK LIMITED', 'BUTWAL-8 ,RUPANDEHI,BUTWAL', '071541817,071541818', 'info@ncdbank.com', '240A', '1221/066/067', 303806846, 157918470, 157918470, '0000-00-00', 1, NULL, NULL, NULL),
(10249, 110, 'CENTURY COMMERCIAL BANK LIMITED', 'WARD NO. 31 ,PUTALISADAK,KATHMANDU', '014445062,014412579', 'welcome@centurybank.com.np', '243A', '2067/01/10', 304236570, 4057176384, 4057176384, '0000-00-00', 1, NULL, NULL, NULL),
(10269, 103, 'LAXMI LAGHUBITTA BITTIYA SANSTHA LIMITED', 'KATHMANDU-4 ,MAHARAGUNJ,KATHMANDU', '014016209,014016238', 'info@laxmilaghu.com.np', '247A', '1269/2067/068', 600279279, 72600000, 72600000, '0000-00-00', 1, NULL, NULL, NULL),
(10272, 109, 'NAGBELI LAGHUBITTA BITTIYA SANSTHA LIMITED', 'ANARMANI - 4 ,BIRTAMOD - 7,BIRTAMOD', '023540933,', 'nagbeli.nagbeli@gmail.com', '249A', '1146/065/066', 303937690, 22993895, 22993895, '0000-00-00', 1, NULL, NULL, NULL),
(10276, 162, 'MITHILA LAGHUBITTA BITTIYA SANSTHA LIMITED', 'DHALKEBAR ,DHANUSHA,DHANUSHA', '041560210,', 'mithilabank@yahoo.com', '251A', '1093/065/066', 303241922, 19793381, 19793381, '0000-00-00', 1, NULL, NULL, NULL),
(10277, 164, 'MIRMIRE LAGHUBITTA BITTIYA SANSTHA LIMITED', 'BANEPA - 8 ,KAVRE,KAVRE', '011662311,', 'info@mirmirebank.com.np', '252A', '1196/066/067', 304005424, 17658000, 17658000, '0000-00-00', 1, NULL, NULL, NULL),
(10279, 120, 'BARUN HYDROPOWER COMPANY LIMITED', 'ANAMNAGAR-32 ,KATHMANDU,KATHMANDU', '014102757,', 'barunhydro@gmail.com', '254A', '1154/065/066', 301617998, 255150000, 255150000, '0000-00-00', 1, NULL, NULL, NULL),
(10280, 103, 'LAXMI VALUE FUND-1', 'NEW BANESHWOR-10 ,KATHMANDU,KATHMANDU', '014780222,014781582', 'info@laxmicapital.com.np', '255A', '1140/065/066', 303692964, 500000000, 500000000, '0000-00-00', 1, NULL, NULL, NULL),
(10281, 127, 'API POWER COMPANY LIMITED', '5 FLOOR, TRADE TOWER ,THAPATHALI, KATHMANDU,KATHMANDU', '015111033,015111037', 'info@sae.com.np', '256A', '114473/59/060', 301442912, 1134000000, 1134000000, '0000-00-00', 1, NULL, NULL, NULL),
(10282, 120, 'NIBL SAMRIDDHI FUND -1', 'UTTAR DOKHA, LAZIMPAT ,KATHMANDU,KATHMANDU', '014005080,014005058', 'info@niblcapital.com', '257A', '1284/067/068', 305003418, 1000000000, 1000000000, '0000-00-00', 1, NULL, NULL, NULL),
(10285, 142, 'JANAUTTHAN SAMUDAYIK LAGHUBITTA BIKASH BANK LIMITED', 'BUTWAL-11 ,RUPANDEHI,RUPANDEHI', '071540063,071541939', 'info@jucbank.com.np', '260A', '1226/066/067', 304412594, 10800000, 10800000, '0000-00-00', 1, NULL, NULL, NULL),
(10287, 142, 'WOMI MICROFINANCE BITTIYA SANSTHA LIMITED', 'NAUBISE - 6, KHANIHKOLA ,DHADING,DHADING', '010401161,', 'womimf@gmail.com', '262A', '1296/067/068', 600109992, 49533120, 49533120, '0000-00-00', 1, NULL, NULL, NULL),
(10288, 155, 'RIDI HYDROPOWER DEVELOPMENT COMPANY LIMITED', 'THAPATHALI ,KATHMANDU,KATHMANDU', '015111015,', 'ridihydro@wlink.com.np', '263A', '1075/064/065', 300847884, 501055100, 501055100, '0000-00-00', 1, NULL, NULL, NULL),
(10289, 109, 'NMB SULAV INVESTMENT FUND - I', 'NAGPOKHARI, NAXAL ,KATHMANDU,KATHMANDU', '014437997,', 'info@nmbcapital.com.np', '264A', '1235/066/067', 304425567, 750000000, 750000000, '0000-00-00', 1, NULL, NULL, NULL),
(10290, 120, 'VIJAYA LAGHUBITTA BITTIYA SANSTHA LIMITED', 'GAINDAKOT-05 ,NAWALPARASI,NAWALPARASI', '078303122,', 'vlbs@vlbs.com.np', '265A', '15737/069/070', 600871778, 53130000, 53130000, '0000-00-00', 1, NULL, NULL, NULL),
(10291, 109, 'NMB  MICROFINANCE BITTIYA SANSTHA LIMITED', 'HEMJA-5, MILANCHOWK ,KASKI,KASKI', '061400427,', 'cleanvillagebank@gmail.com', '266A', '1343/068/069', 601108365, 98612500, 98612500, '0000-00-00', 1, NULL, NULL, NULL),
(10292, 110, 'CIVIL LAGHUBITTA BITTIYA SANSTHA LIMITED', 'CHABHIL-7 ,KATHMANDU,KATHMANDU', '014471438,014474202', 'ktmmicro@gmail.com', '267A', '1287/067/068', 305039624, 43890000, 43890000, '0000-00-00', 1, NULL, NULL, NULL),
(10296, 108, 'GLOBAL IME LAGHUBITTA BITTIYA SANSTHA LIMITED', 'BESISHAHAR 01 ,LAMJUNG,LAMJUNG', '066521177,', 'rmfi@reliablebank.com.np', '270A', '110455/069/070', 601104080, 28815000, 28815000, '0000-00-00', 1, NULL, NULL, NULL),
(10298, 142, 'SIDDHARTHA EQUITY ORIENTED SCHEME', 'SIDDHARTHA INSURANCE COMPLEX ,BABAR MAHAL, KATHMANDU,KATHMANDU', '014257767,014257768', 'scl@siddharthacapital.com', '272A', '1326/068/069-', 600380326, 1000000000, 1000000000, '0000-00-00', 1, NULL, NULL, NULL),
(10299, 108, 'SAHARA BIKASH BANK LIMITED', 'MALANGWA - 8 ,SARLAHI,SARLAHI', '046521471,', 'saharabb2010@hotmail.com', '273A', '1202/066/067', 304212035, 35767400, 35767400, '0000-00-00', 1, NULL, NULL, NULL),
(10300, 110, 'KISAN MICRO FINANCE BITTIYA SANSTHA LIMITED', 'LAMKICHUHA MUNICIPALITY ,WARD NO. 7,KAILALI', '091419053,', 'kmfachham@gmail.com', '274A', '1346/068/069', 600650982, 19220000, 19220000, '0000-00-00', 1, NULL, NULL, NULL),
(10301, 142, 'MAHILA SAHAYATRA MICROFINANCE BITTIYA SANSTHA LIMITED', 'CHITLANG -5 ,MAKAWANPUR,MAKAWANPUR', '057693941,', 'mahilasahayatra@gmail.com', '275A', '1334/068/069', 600618087, 36300000, 36300000, '0000-00-00', 1, NULL, NULL, NULL),
(10302, 109, 'SAPTAKOSHI DEVELOPMENT BANK LIMITED', 'TANKISINAWARI-2 ,MORANG,MORANG', '021420676,021420677', 'info@skdbl.com.np', '276A', '1338/068/069', 305600877, 203404000, 203404000, '0000-00-00', 1, NULL, NULL, NULL),
(10308, 108, 'GLOBAL IME SAMMUNAT SCHEME -1', 'JAMAL, KATHMANDU ,,KATHMANDU', '014222460,', '0', '279A', '1067/064/065', 302971646, 1000000000, 1000000000, '0000-00-00', 1, NULL, NULL, NULL),
(10309, 108, 'MERO MICROFINANCE BITTIYA SANSTHA LIMITED', 'BIDUR-4, BATTAR ,,NUWAKOT', '010561746,010561747', 'corporate@meromicrofinance.com', '280A', '107214/069/070', 601335464, 87516000, 87516000, '0000-00-00', 1, NULL, NULL, NULL),
(10310, 108, 'JALAVIDHYUT LAGANI TATHA BIKAS COMPANY LIMITED', 'HATTISAR, KATHMANDU ,,KATHMANDU', '014445013,014445014', 'info@hidcl.org.np', '281A', '1300/067/068', 600232009, 10000000000, 10000000000, '0000-00-00', 1, NULL, NULL, NULL),
(10312, 142, 'NGADI GROUP POWER LIMITED', 'ANAMNAGAR ,,KATHMANDU', '014238159,', 'info@siurihydro.com', '282A', '38467/062/063', 302087624, 535554800, 535554800, '0000-00-00', 1, NULL, NULL, NULL),
(10314, 120, 'NATIONAL MICROFINANCE BITTIYA SANSTHA LIMITED', 'NILKHANTHA 3, DHADING ,,DHADING', '010520876,', 'Rambdryadav@gmail.com', '284A', '117106/070/071', 601593581, 84249200, 84249200, '0000-00-00', 1, NULL, NULL, NULL),
(10315, 120, 'GREEN DEVELOPMENT BANK LIMITED', 'HARI SHANKER ROAD, BAGLUNG ,,BAGLUNG', '068522865,068522866', 'info@greenbank.com.np', '285A', '107206/069/070', 600750406, 225000000, 225000000, '0000-00-00', 1, NULL, NULL, NULL),
(10316, 120, 'RSDC LAGHUBITTA BITTIYA SANSTHA LIMITED', 'KALIKANAGAR12, BUTWAL ,RUPANDEHI,BUTWAL', '071438513,071419013', 'info@rsdcmf.com.np', '286A', '110814/069/070', 600947815, 143980000, 143980000, '0000-00-00', 1, NULL, NULL, NULL),
(10318, 109, 'NMB HYBRID FUND L - 1', 'NAGPOKHARI ,,KATHMANDU', '014437963,', 'info@nmbcl.com.np', '288A', '1235/066/067.', 304425567, 1000000000, 1000000000, '0000-00-00', 1, NULL, NULL, NULL),
(10319, 116, 'NABIL EQUITY FUND', 'CENTRAL PLAZA, NARAYANCHOUR ,NAXAL,KATHMANDU', '4411604,4411733', 'nabilinvest@nabilbank.com', '289A', '1211/066/067.', 304242201, 1250000000, 1250000000, '0000-00-00', 1, NULL, NULL, NULL),
(10320, 108, 'SURYODAYA LAGHUBITTA BITTIYA SANSTHA LIMITED', 'PUTALIBAZAR - 1 SYANGJA ,,SYANGJA', '063421187,', 'suryodayalaghubitta@gmail.com', '290A', '108117/069/070', 601101168, 22680000, 22680000, '0000-00-00', 1, NULL, NULL, NULL),
(10321, 162, 'KHANI KHOLA HYDROPOWER COMPANY LIMITED', 'SHREE GORAKHANATH MARGA ,DHUMBARAHI,KATHMANDU', '014429956,014436325', 'khanikhola.hpcegmail.com', '291A', '106404/069/070', 600844466, 465714300, 465714300, '0000-00-00', 1, NULL, NULL, NULL),
(10322, 101, 'DIBYASHWARI HYDROPOWER LIMITED', 'OLD BANESHWOR, KATHMANDU ,,KATHMANDU', '014116068,016201315', 'dibyashwarihydropower@gmail.com', '292A', '106951/063/064', 302580062, 264000000, 264000000, '0000-00-00', 1, NULL, NULL, NULL),
(10323, 127, 'ARUN KABELI POWER LIMITED', 'TRADE TOWER, THAPATHALI ,,KATHMANDU', '015111085,', 'info@arunkabeli.com', '293A', '1277/067/068', 304956159, 1500000000, 1500000000, '0000-00-00', 1, NULL, NULL, NULL),
(10324, 120, 'NIBL PRAGATI FUND', 'LAZIMPAT, KATHMANDU ,,KATHMANDU', '014005080,', 'info@niblcapital.com', '294A', '1284/067/068 .', 305003418, 750000000, 750000000, '0000-00-00', 1, NULL, NULL, NULL),
(10326, 162, 'SYNERGY POWER DEVELOPMENT LIMITED', 'BHAGAWATI BAHAL, NAXAL ,,KATHMANDU', '014440433,', 'info@synergyhydro.com', '296A', '43534/063/064', 302483354, 700000000, 700000000, '0000-00-00', 1, NULL, NULL, NULL),
(10327, 116, 'FORWARD COMMUNITY MICROFINANCE BITTIYA SANSTHA LIMITED', 'DUHABI BHALUWA -3, SUNSARI ,,SUNSARI', '025540018,', 'info@forwardmfbank.com.np', '297A', '97254/069/070', 601075869, 90049700, 90049700, '0000-00-00', 1, NULL, NULL, NULL),
(10329, 120, 'SAMATA MICROFINANCE BITTIYA SANSTHA LIMITED', 'SIMARA - 3, BARA ,,SIMARA', '053521921,', 'samata@samata.org.np', '298A', '111548/069/070', 601028249, 26544000, 26544000, '0000-00-00', 1, NULL, NULL, NULL),
(10330, 160, 'UNITED MODI HYDROPOWER LIMITED', 'HERITAGE PLAZA-11 ,KAMALADI, KATHMANDU,KATHMANDU', '014169114,', 'unitedmodi@ntc.net.np', '299A', '49185/064/065', 302750072, 1150000000, 1150000000, '0000-00-00', 1, NULL, NULL, NULL),
(10331, 108, 'CHHYANGDI HYDROPOWER LIMITED', 'GAIRIDHARA 2, KATHMANDU ,,KATHMANDU', '014426483,014424925', 'info@chpl.com.np', '300A', '47832/064/065', 303760113, 270000000, 270000000, '0000-00-00', 1, NULL, NULL, NULL),
(10332, 166, 'SWADESHI LAGHUBITTA BITTIYA SANSTHA LIMITED', 'ITAHARI , SUNSARI ,,ITAHARI', '025587620,025588302', 'info@swadeshibank.com', '301A', '127403/071/072', 602424998, 34500000, 34500000, '0000-00-00', 1, NULL, NULL, NULL),
(10333, 103, 'LAXMI EQUITY FUND', 'NEW BANESHWOR KATHMANDU - 10 ,,KATHMANDU', '014780222,', 'info@laxmicapital.com.np', '302A', '1140/065/066 .', 303692964, 1250000000, 1250000000, '0000-00-00', 1, NULL, NULL, NULL),
(10334, 108, 'HIMALAYAN POWER PARTNER LIMITED', 'MAHARAJGUNJ, PANIPOKHARI -3 ,KATHMANDU,KATHMAMDU', '014002801,', 'hpphydro.finance@gmail.com', '303A', '114858/070/071', 301660400, 1065417000, 1065417000, '0000-00-00', 1, NULL, NULL, NULL),
(10335, 127, 'LUMBINI BIKAS BANK LIMITED', 'KA.MA.NA.PA. WARD NO. 30 ,DILLIBAZAR, KATHMANDU,KATHMANDU', '014413232,014433234', 'info@lumbinibikasbank.com', '304A', '106260/063/064', 302598461, 1090894947, 1090894947, '0000-00-00', 1, NULL, NULL, NULL),
(10336, 115, 'MAHULI SAMUDAYIK LAGHUBITTA BITTIYA SANSTHA LIMITED', 'BAKDHUWA WARD NO. 9 ,MAHULI BAZAR,MAHULI BAZAR', '031411005,031411006', '0', '305A', '96867/069/070', 601043446, 18000000, 18000000, '0000-00-00', 1, NULL, NULL, NULL),
(10337, 109, 'NEPAL HYDRO DEVELOPER LIMITED', 'DILLIBAZAR, KATHMANDU ,,KATHAMANDU', '014441131,014441444', 'info@charnawatihydro.com', '306A', '124295/70/071', 302405851, 260000000, 260000000, '0000-00-00', 1, NULL, NULL, NULL),
(10338, 142, 'SIDDHARTHA EQUITY FUND', 'NARAYANCHAUR, NAXAL, ,KATHMANDU,KATHMANDU', '014420924,014420925', 'scl@siddharthacapital.com', '307A', '1326/068/069.', 600380326, 1500000000, 1500000000, '0000-00-00', 1, NULL, NULL, NULL),
(10339, 120, 'SUPPORT MICROFINANCE BITTIYA SANSTHA LIMITED', 'Itahari Sub Metropolitan City ,Ward No.1,Itahari', '025-588177,025-588178', 'smicrofinance7@gmail.com', '308A', '128367', 602434634, 42000000, 42000000, '0000-00-00', 1, NULL, NULL, NULL),
(10340, 168, 'RADHI BIDYUT COMPANY LIMITED', 'TRIPURESHWOR-11, KATHMANDU ,,KATHMANDU', '014232748,014232749', 'info@radhibidyut.com.np', '309A', '926/061/62', 302673302, 451004000, 451004000, '0000-00-00', 1, NULL, NULL, NULL),
(10341, 161, 'SANIMA CAPITAL LIMITED', 'NARAYANCHOUR , NAXAL KATHMANDU ,,KATHMANDU', '014428956,014428957', 'sanimacapital@sanimabank.com', '310A', '142428/072/073', 603564642, 1300000000, 1300000000, '0000-00-00', 1, NULL, NULL, NULL),
(10342, 142, 'UNNATI MICROFINANACE BITTIYA SANSTHA LIMITED', 'SIDDHARTHANAGAR - 12, ,RUPENDEHI,BUTWAL', '071521128,', 'info@unnatimfi.com.np', '311A', '118159/070/071', 601792674, 16500000, 16500000, '0000-00-00', 1, NULL, NULL, NULL),
(10344, 166, 'AARAMBHA MICROFINANCE BITTIYA SANSTHA LIMITED', 'BANEPA -10, KAVREPALANCHOK ,,BANEPA', '011664579,', 'aarambhamicrofinance@gmail.com', '312A', '121444/070/071', 601864478, 29400000, 29400000, '0000-00-00', 1, NULL, NULL, NULL),
(10345, 160, 'CBIL CAPITAL LIMITED', 'BATULEGHAR, DILLIBAZAR ,,KATHMANDU', '014418526,014417457', 'info@cbilcapital.com', '313A', '945/062/063', 302201811, 820000000, 820000000, '0000-00-00', 1, NULL, NULL, NULL),
(10346, 109, 'NEPAL SEVA LAGHUBITTA BITTIYA SANSTHA LIMITED', 'FATAKSHILA-08, SINDHUPALCHOWK ,,SINDHUPALCHOWK', '016204707,', 'nepalseva@gmail.com', '314A', '117446/070/071', 601621989, 18000000, 18000000, '0000-00-00', 1, NULL, NULL, NULL),
(10347, 127, 'RAIRANG HYDROPOWER DEVELOPMENT COMPANY LIMITED', 'PO.BOX NO. 21839 5TH FLOOR ,TRADETOWER,KATHMANDU', '015111015,015111016', 'hydrorairang@gmail.com', '315A', '18458/059/060', 300846984, 560000000, 560000000, '0000-00-00', 1, NULL, NULL, NULL),
(10351, 116, 'NADEP LAGHUBITTA BITTIYA SANSTHA LIMITED', 'GAJURI-1, DHADING, ,,GAJURI', '10402111,10402124', 'nadeplaghubitta@gmail.com', '316A', '119118/070/071', 601799215, 48000000, 48000000, '0000-00-00', 1, NULL, NULL, NULL),
(10352, 115, 'Panchakanya Mai Hydropower Ltd', 'Pulchowk-3, Lalitpur', '01-5010078,  01-5010079', 'mail.mvhpl@gmail.com', '', '147290', 301216559, 11000000, 11000000, '2018-10-07', 1, NULL, '2018-10-07 13:38:11', '2018-10-07 13:38:35'),
(10353, 108, 'Kalika Power Company Limited', 'Bharatpur Sub- Metropolitan-10 Chitwan', '056-520456, 056-522440', 'info@kalikagroup.com', '', '52159/064/065', 302808720, 6000000, 6000000, '2018-10-07', 1, NULL, '2018-10-07 13:44:18', '2018-10-07 13:44:33'),
(10354, 169, 'NIC Asia Debenture 2082/083', 'Thapathali, Kathmandu', '01-5111177', NULL, NULL, NULL, NULL, 1830000, 1830000, NULL, 1, NULL, '2018-12-04 11:07:50', '2018-12-04 11:49:49');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `status`, `name`, `created_at`, `updated_at`, `deleted_at`, `order`) VALUES
(1, 1, 'About Us', '2018-10-09 02:17:00', '2018-10-30 23:30:40', NULL, 1),
(2, 1, 'Investor', '2018-10-09 02:17:31', '2018-10-09 02:17:31', NULL, 2),
(3, 1, 'DP', '2018-10-09 02:17:45', '2018-10-09 02:17:45', NULL, 3),
(4, 1, 'RTA', '2018-10-09 02:17:59', '2018-10-09 02:17:59', NULL, 4),
(5, 1, 'Clearing Member', '2018-10-09 02:18:09', '2018-10-09 02:18:09', NULL, 5),
(6, 1, 'Issuer', '2018-10-09 02:18:34', '2018-10-09 02:18:34', NULL, 6),
(7, 1, 'Services', '2018-10-09 02:18:48', '2018-10-09 02:18:48', NULL, 7),
(8, 1, 'Downloads', '2018-10-15 02:41:24', '2018-10-15 02:41:24', NULL, 8);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `notice_duration` int(50) NOT NULL DEFAULT '35'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `notice_duration`) VALUES
(1, 35);

-- --------------------------------------------------------

--
-- Table structure for table `news_notice`
--

CREATE TABLE `news_notice` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0-Unpublished 1-Published 2-Deleted',
  `type` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `published_date` date DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news_notice`
--

INSERT INTO `news_notice` (`id`, `title`, `description`, `file_name`, `file`, `status`, `type`, `published_date`, `deleted_at`, `created_at`, `updated_at`) VALUES
(9, 'Notice regarding Full Demat (2072-09-30)', 'Full Demat Notice&nbsp; 2072_09_30', '', '2018_09_11_07_16_26_full_demat_notice_ 2072_09_30.pdf', 0, 'notice', '2018-09-11', NULL, '2018-09-11 11:16:26', '2018-09-14 13:22:16'),
(10, 'Notice regarding Interview (2073-06-06)', 'notice_2073_06_06_notice_for_interview', '', '2018_09_11_07_16_50_notice_2073_06_06_notice_for_interview.pdf', 1, 'notice', '2018-09-11', NULL, '2018-09-11 11:16:50', '2018-09-12 11:07:01'),
(11, ' Notice regarding Final Result of Bigyapan (2073-04-14)', 'notice_2073_06_15_notice_final_result_bigyapan_2073-74-02', '', '2018_09_11_07_18_16_notice_2073_06_15_notice_final_result_bigyapan_2073-74-02.pdf', 1, 'notice', '2018-09-11', NULL, '2018-09-11 11:18:16', '2018-09-12 11:08:00'),
(12, 'Notice regarding appointment of IT Officer for CDS and Clearing Limited  (2073-04-14)', 'notice_2073_07_19_notice_final_result_bigyapan_2073-74-1', '', '2018_09_11_07_18_38_notice_2073_07_19_notice_final_result_bigyapan_2073-74-1.pdf', 1, 'notice', '2018-09-11', NULL, '2018-09-11 11:18:38', '2018-09-12 11:08:49'),
(13, 'Notice regarding Ammendment of Law (2073-11-11)', 'notice_2073_11_14_notice_byelaw_ammendment', '', '2018_09_11_07_19_04_notice_2073_11_14_notice_byelaw_ammendment.pdf', 1, 'notice', '2018-09-11', NULL, '2018-09-11 11:19:04', '2018-09-12 11:09:47'),
(14, 'Notice Regarding Close Out (2074-01-07)', 'notice_2074_01_07_regarding_closeou', '', '2018_09_11_07_19_54_notice_2074_01_07_regarding_closeou.pdf', 1, 'notice', '2018-09-11', NULL, '2018-09-11 11:19:54', '2018-09-12 11:10:14'),
(15, 'Notice regarding  Vacancy of Driver (2074-02-30)', 'notice_2074_02_30_vacancy_driver', '', '2018_09_11_07_20_22_notice_2074_02_30_vacancy_driver.pdf', 1, 'notice', '2018-09-11', NULL, '2018-09-11 11:20:22', '2018-09-12 11:10:37'),
(17, 'Notice regarding DRN Verification (2074-09-09)', 'notice_2074_09-09_In_Person_Verification_Form_RTAs', '', '2018_09_11_07_21_07_notice_2074_09-09_In_Person_Verification_Form_RTAs.pdf', 1, 'notice', '2018-09-11', NULL, '2018-09-11 11:21:07', '2018-09-12 11:11:10'),
(18, 'Notice  regarding Standing List (2074-03-09 )', 'notice_20740309_standing_list', '', '2018_09_11_07_21_44_notice_20740309_standing_list.pdf', 1, 'notice', '2018-09-11', NULL, '2018-09-11 11:21:44', '2018-09-12 11:11:44'),
(19, 'Notice_regarding_Annual_Fee (2074-03-18)', 'Notice_regarding_annual_fee (2074-03-18)', '', '2018_09_11_07_22_12_notice_20740318_annual_fee.pdf', 1, 'notice', '2018-09-11', NULL, '2018-09-11 11:22:12', '2018-09-12 11:12:13'),
(20, 'Notice regarding Multiple Clearing Bank with Net Settlement (2074-03-21)', 'notice_20740321_net_settlement_and_securities_funds_payin', '', '2018_09_11_07_22_39_notice_20740321_net_settlement_and_securities_funds_payin.pdf', 1, 'notice', '2018-09-11', NULL, '2018-09-11 11:22:39', '2018-09-12 11:13:11'),
(21, 'Notice regarding Demat Account Annual Fee (2074-03-30)', 'notice_20740330_demat_account_annual_fee', '', '2018_09_11_07_23_01_notice_20740330_demat_account_annual_fee.pdf', 1, 'notice', '2018-09-11', NULL, '2018-09-11 11:23:01', '2018-09-12 11:15:00'),
(22, 'Notice regarding renew of Demat Account and KYC Update (2073-11-15)', 'notice_20740416_demat_account_annual_fee_and_kyc', '', '2018_09_11_07_23_32_notice_20740416_demat_account_annual_fee_and_kyc.pdf', 1, 'notice', '2018-09-11', NULL, '2018-09-11 11:23:32', '2018-09-12 11:15:53'),
(23, 'Notice_regarding Special AGM (2074-06-01)', 'notice_20740516_special_agm', '', '2018_09_11_07_24_01_notice_20740516_special_agm.pdf', 1, 'notice', '2018-09-11', NULL, '2018-09-11 11:24:01', '2018-09-12 11:16:37'),
(24, '	Notice regarding Special AGM (2074-06-01)', 'notice_20740526_special_agm_revised', '', '2018_09_11_07_24_32_notice_20740526_special_agm_revised.pdf', 1, 'notice', '2018-09-11', NULL, '2018-09-11 11:24:32', '2018-09-11 11:24:32'),
(25, 'Notice_regarding_opening of multiple_demat_account (2074-03-11)', 'notice_20740726_multiple_demat_account', '', '2018_09_11_07_24_58_notice_20740726_multiple_demat_account.pdf', 1, 'notice', '2018-09-11', NULL, '2018-09-11 11:24:58', '2018-09-11 11:24:58'),
(26, 'notice_regarding_7th AGM of CDS and Clearing Limited (2074-09-27)', 'notice_20740806_cds_7th_agm', '', '2018_09_11_07_25_35_notice_20740806_cds_7th_agm.pdf', 1, 'notice', '2018-09-11', NULL, '2018-09-11 11:25:35', '2018-09-11 11:25:35'),
(27, 'Notice_regarding C-ASBA (2074-09-07)', 'notice_20740912_casba', '', '2018_09_11_07_26_07_notice_20740912_casba.pdf', 1, 'notice', '2018-09-11', NULL, '2018-09-11 11:26:07', '2018-09-11 11:26:07'),
(28, 'Notice_regarding operation of C-ASBA (2074-10-09)', 'notice_20741009_casba_mandatory', '', '2018_09_11_07_26_34_notice_20741009_casba_mandatory.pdf', 1, 'notice', '2018-09-11', NULL, '2018-09-11 11:26:34', '2018-09-11 11:26:34'),
(29, 'Notice_regarding_mandatory of C-ASBA (2074-11-11)', 'notice_20741011_casba_mandatory', '', '2018_09_11_07_27_01_notice_20741011_casba_mandatory.pdf', 1, 'notice', '2018-09-11', NULL, '2018-09-11 11:27:01', '2018-09-11 11:27:01'),
(30, 'Notice_regarding_C-ASBA_work_procedure_for_banks (2074-12-13)', 'notice_20741213_casba_work_procedure_for_banks', '', '2018_09_11_07_27_37_notice_20741213_casba_work_procedure_for_banks.pdf', 1, 'notice', '2018-09-11', NULL, '2018-09-11 11:27:37', '2018-09-11 11:27:37'),
(31, 'Important Notice regarding C-ASBA work procedure to investors (2074-01-04)', 'notice for casba work procedure for investors', '', '2018_09_11_07_28_07_notice_20741213_casba_work_procedure_for_investors.pdf', 1, 'notice', '2018-09-12', NULL, '2018-09-11 11:28:07', '2018-09-11 11:28:07'),
(32, 'Invitation for bids for the supply, delivery, installation and commencement of Hardware & Software System (2075-01-04)', 'invitation for electronic bids for the supply, delivery, installation and commencement of', 'Bids', '2018_09_11_07_28_27_notice_20750104_invitation_for_bids.pdf', 1, 'notice', '2018-09-15', NULL, '2018-09-11 11:28:27', '2018-11-14 10:16:19'),
(33, 'Notice regarding interviews for different positions of CDS & Clearing Limited\r\n(2074-12-27)', 'notice for interview for different position ', '', '2018_09_11_07_28_56_notice_20750121_interview.pdf', 1, 'notice', '2018-09-12', NULL, '2018-09-11 11:28:56', '2018-09-11 11:28:56'),
(34, 'Notice regarding Candidate nomination list  for different position under the agreement (2074-12-27)', 'vacancy result publish notice ', '', '2018_09_11_07_29_23_notice_20750128_result.pdf', 1, 'notice', '2018-09-12', NULL, '2018-09-11 11:29:23', '2018-09-11 11:29:23'),
(35, 'Notice regarding registration of  Standing List (2075-03-08)', 'notice for standing_list', '', '2018_09_11_07_29_47_notice_20750308_standing_list.pdf', 1, 'notice', '2018-09-16', NULL, '2018-09-11 11:29:47', '2018-09-11 11:29:47'),
(36, 'Notice_regarding_refund of Bonus and Public Share(2073-09-12)', 'notice_regarding_bank_account_2073_09_12', '', '2018_09_11_07_30_19_notice_regarding_bank_account_2073_09_12.pdf', 1, 'notice', '2018-09-11', NULL, '2018-09-11 11:30:19', '2018-09-11 11:30:19'),
(37, 'Notice_regarding_selection_of_commercial banks for clearing_and_settlement banking services (20730-11-15)', 'notice_regarding_selection_of_clearing_and_settlement_banks', '', '2018_09_11_07_31_00_notice_regarding_selection_of_clearing_and_settlement_banks.pdf', 1, 'notice', '2018-09-11', NULL, '2018-09-11 11:31:00', '2018-09-11 11:31:00'),
(38, 'Notice regarding Supply, Delivery and Commissioning of SUV (2075-07-28)', 'Notice regarding Supply, Delivery and Commissioning of SUV (2075-07-28)', 'Notice regarding Supply, Delivery and Commissioning of SUV (2075-07-28)', '2018_11_14_04_24_38_CDC 3x15.pdf', 1, 'notice', '2018-11-14', NULL, '2018-11-14 09:24:38', '2018-11-14 09:35:30');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `page_id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(11) NOT NULL,
  `submenu_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`page_id`, `menu_id`, `submenu_id`, `title`, `description`, `status`, `deleted_at`, `created_at`, `updated_at`, `slug`) VALUES
(2, 1, 1, 'Introduction', 'CDS and Clearing Limited, a company established under the company act is a company promoted by Nepal Stock Exchange Limited (NEPSE) in 2010 to provide centralized depository, clearing and settlement services in Nepal. The company is inaugurated on 31st March 2011. The main objective of the company is to act as a central depository for various instruments (Equity, Bonds, and Warrants etc) especially to handle securities in dematerialized form. This organization is entrusted with the safekeeping, deposit, and withdrawal of securities certificates and transfer of ownership/rights of the said instruments. The depository functions will be performed by the company under the securities regulations of Securities Board of Nepal (SEBON). CDSC is a wholly owned subsidiary company of Nepal Stock Exchange Ltd. (NEPSE) which was established on 7th Poush 2067.', 1, NULL, '2018-10-09 02:42:20', '2018-11-29 11:57:46', 'cdscintroduction'),
(3, 1, 2, 'Board Of Director', 'b', 1, NULL, '2018-10-09 02:45:53', '2018-10-09 02:45:53', 'bod'),
(6, 1, 4, 'Global Partners', 'globalPartner', 1, NULL, '2018-10-11 03:32:02', '2018-10-11 03:32:02', 'globalpartner'),
(12, 1, 3, 'Management Team', 'managementTeam', 1, NULL, '2018-10-11 03:49:21', '2018-10-11 03:49:21', 'managementTeam'),
(13, 2, 5, 'FAQ', '...', 1, NULL, '2018-10-11 03:51:02', '2018-10-30 01:28:28', 'frequentlyaskquestion'),
(14, 2, 6, 'Settlement ID', 'settlementid', 1, NULL, '2018-10-11 03:58:08', '2018-10-11 03:58:08', 'settlementid'),
(17, 2, 7, 'Dos And Donts', 'do&dont', 1, NULL, '2018-10-11 04:09:45', '2018-10-11 04:09:45', 'do&dont'),
(19, 3, 9, 'Introduction', 'CDSC\'s demat services are extended through its agents called Depository Participants (DP). Investors are linked to the depository through the DPs. A DP processes all the instructions given by the investors. All the accounts and records of the investor are maintained by CDSC. A DP remains as the point of contact for the investors for carrying out their depository services. CDSC’s system is a centralized database architecture with connectivity to each DP through a secured VPN network.\r\n\r\nCDSC -DPs can also set up branches with direct electronic connectivity with CDSC.\r\n\r\nCDSC -DPs can also extend service to investors through their various service centers connected through their back office application Dpsecure.', 1, NULL, '2018-10-11 04:59:29', '2018-10-11 04:59:29', 'dpintroduction'),
(20, 3, 10, 'List Of DP', 'listOfDP', 1, NULL, '2018-10-11 05:10:00', '2018-10-30 06:31:19', 'listofdp'),
(21, 3, 11, 'Admission Procedure', '....', 1, NULL, '2018-10-11 05:15:36', '2018-10-30 06:28:09', 'admissionprocedure'),
(22, 3, 12, 'Hardware Software Required', 'Hardware Requirement\r\nServer PC - 1\r\nProcessor - Minimum 2.5 GHz or Higher. RAM - Minimum 1 GB or Higher. Hard Disk Drive - 100 GB or Higher. CD/DVD Drive. NIC Card. Operating System - Windows Server 2003 - 32 Bit. Antivirus Software. WorkStation PC - Required Nos Processor - Minimum 2.5 GHz or Higher. RAM - Minimum 1 GB or Higher. Hard Disk Drive - 100 GB or Higher. NIC Card. Operating System - Windows XP Professional - 32 Bit. Network Hardware Configurable Router/Firewall - 1. (Features: IPSEc, SSL for VPN). Switch - 24 Ports - 1. Network Cables - As Required. Electricity Backup Electricity Backup for Load Shedding. Front Office Data Backup facilities either on Tape or other Back Media. Printer & Scanner & Fax - Nos As required. Database Software/System Software/Application Software Microsoft SQL Server 2008 Express Edition. Microsoft .NET Framework 4.0.', 1, NULL, '2018-10-11 23:31:24', '2018-10-11 23:35:09', 'hardwaresoftwarerequired'),
(23, 4, 13, 'Introduction', 'The companies who wish to admit their shares and securities into the system should obtain electronic connectivity with CDSC to avail the services of a registrar and transfer agents (RTA). CDSC records and safe keeps the securities admitted by these companies. CDSC functions as record keeping office in respect of the securities admitted by these companies.CDSC has maintained a centralized database architecture with on-line connectivity with the RTA through VPN network. All the RTA&rsquo;s are connected to CDSC and are providing services to a number of companies across the country.', 1, NULL, '2018-10-11 23:46:58', '2018-10-11 23:48:45', 'rtaintroduction'),
(24, 4, 14, 'RTA List', 'a', 1, NULL, '2018-10-11 23:49:46', '2018-10-11 23:49:46', 'listofrta'),
(25, 4, 15, 'Admission Procedure', 'rtaadmissionprocedure', 1, NULL, '2018-10-15 00:11:16', '2018-10-15 00:11:16', 'rtaadmissionprocedure'),
(26, 4, 16, 'Documentation for Corporate Action', 'documentation', 1, NULL, '2018-10-15 00:16:47', '2018-10-15 00:16:47', 'documentation'),
(27, 5, 17, 'Introduction', '.', 1, NULL, '2018-10-15 00:59:34', '2018-10-15 00:59:34', 'cmintroduction'),
(28, 5, 18, 'List Of Clearing Member', 'listofclearingmember', 1, NULL, '2018-10-15 01:04:41', '2018-10-15 01:04:41', 'listofclearingmember'),
(29, 5, 19, 'Settlement Procedure', '.', 1, NULL, '2018-10-15 01:09:43', '2018-10-15 01:09:43', 'cmsettlementprocedure'),
(30, 6, 20, 'Introduction', '.', 1, NULL, '2018-10-15 01:14:03', '2018-10-15 01:14:03', 'issuer'),
(31, 6, 21, 'ISIN & Script', '.', 1, NULL, '2018-10-15 01:15:53', '2018-10-15 01:15:53', 'isinscript'),
(32, 6, 22, 'Registered Company', '.', 1, NULL, '2018-10-15 01:28:06', '2018-10-15 01:28:06', 'registeredcompany'),
(33, 6, 23, 'Benefits', '.', 1, NULL, '2018-10-15 01:30:41', '2018-10-15 01:30:41', 'benefits'),
(34, 6, 24, 'Admission Procedure', '.', 1, NULL, '2018-10-15 01:32:57', '2018-10-15 01:36:38', 'issueradmissionprocedure'),
(35, 6, 24, 'Admission Procedure', '.', 0, NULL, '2018-10-15 01:32:57', '2018-11-04 11:51:02', 'issueradmissionprocedure'),
(36, 7, 25, 'Account Opening', 'To utilise the services offered by a depository, any person having investment in any security or intending to invest in securities needs to have a demat account with a CDSCL-DP. The holder of such demat account is called as \"Beneficial Owner (BO)\". A BO can maintain a demat account with zero balance in such account. A BO can open more than one account with the same or multiple DPs, in the same name/s and order, if he/she desires so. The investor can approach any DP/s of his/her choice to open a demat account.', 1, NULL, '2018-10-15 01:59:09', '2018-10-15 02:19:20', 'accountopening'),
(37, 7, 26, 'Dematerialisation', 'Dematerialisation is a process by which physical certificates (of shares / debentures / other securities) are converted into electronic balances. A BO has to submit the request for dematerialisation by submitting the demat request form (DRF) duly completed along with the concerned physical certificates, to his/her DP.', 1, NULL, '2018-10-15 02:27:16', '2018-10-15 02:27:16', 'dematerialisation'),
(38, 7, 27, 'Transmission Of Securities', 'CDSC offers a facility for transmission of balances held in BO account/s (to other BO account/s).', 1, NULL, '2018-10-15 02:28:23', '2018-10-15 02:28:23', 'transmissionofsecurities'),
(39, 7, 28, 'Account Statement', 'Generally a DP sends to the BO, a statement of his account, if required by BO, if there is any transaction in the account. The balances and transactions can also be viewed by the BOs through CDSC web based facility &lsquo;Mero Share&rsquo;.', 1, NULL, '2018-10-15 02:29:41', '2018-10-15 02:29:41', 'accountstatement'),
(40, 7, 29, 'Rematerialisation', '\r\n\r\nRematerialisation is the process by which the electronic balances held in the demat account can be converted back into physical certificates.\r\n\r\n', 1, NULL, '2018-10-15 02:30:34', '2018-10-15 02:30:34', 'rematerialisation'),
(41, 7, 30, 'Pledging', '\r\n\r\nIf the BO decides to pledge any securities in his BO account, he can avail of the same by submitting the pledge creation form duly completed, to his DP.\r\n\r\n', 1, NULL, '2018-10-15 02:31:35', '2018-10-15 02:31:35', 'pledging'),
(42, 8, 31, 'Circulars', '.', 1, NULL, '2018-10-15 03:47:53', '2018-10-15 03:47:53', 'circulars'),
(43, 8, 32, 'By Laws', '.', 1, NULL, '2018-10-15 04:14:30', '2018-10-15 04:14:30', 'bylaws'),
(44, 8, 33, 'Publication', '.', 1, NULL, '2018-10-15 04:18:00', '2018-10-15 04:18:00', 'publication'),
(45, 8, 34, 'Demat Registration', '.', 1, NULL, '2018-10-15 04:18:44', '2018-10-15 04:18:44', 'demat'),
(46, 2, 8, 'Events', 'e', 1, NULL, '2018-10-21 00:27:31', '2018-10-21 00:27:31', 'events'),
(47, 3, 10, 'List Of DP', '.', 1, NULL, '2018-10-21 23:53:26', '2018-10-21 23:54:35', 'searchlistofdp'),
(48, 2, 6, 'Searching', '.', 1, NULL, '2018-10-22 00:40:41', '2018-10-22 00:40:41', 'settlementidsearch'),
(49, 2, 36, 'Death Notice', 'deathnotice', 1, NULL, '2018-10-22 00:50:12', '2018-10-22 00:50:12', 'deathnotice'),
(50, 4, 14, 'Search RTA', '.', 1, NULL, '2018-10-22 01:04:42', '2018-10-22 01:04:42', 'rtalistsearch'),
(51, 6, 22, 'Company', '.', 1, NULL, '2018-10-22 01:24:43', '2018-10-22 01:24:43', 'company'),
(52, 6, 22, 'Register Bank In Mero Share', '.', 1, NULL, '2018-10-22 02:11:45', '2018-10-22 02:11:45', 'registeredbankinmeroshare'),
(53, 6, 22, 'Bank IN C-ASBA', 'casbabank', 1, NULL, '2018-10-22 02:31:03', '2018-10-22 02:31:03', 'casbabank'),
(54, 2, 36, 'Death Notice Search', '...', 1, NULL, '2018-10-29 03:11:23', '2018-10-29 03:11:23', 'deathnoticesearch'),
(55, 5, 37, 'CDSC Tariff', 'cdsc-tariff', 1, NULL, '2018-11-02 22:40:59', '2018-11-02 22:40:59', 'cdsc-tariff');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `press_release`
--

CREATE TABLE `press_release` (
  `press_release_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published_date` date DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `press_release`
--

INSERT INTO `press_release` (`press_release_id`, `title`, `description`, `file_name`, `file`, `published_date`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(5, 'press_release regarding transaction of demat shares (2072-04-07)', 'press_release_2072_04_07', '', '2018_09_11_08_00_30_press_release_2072_04_07.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:00:30', '2018-09-11 12:16:34'),
(6, '	press release regarding transaction of demat shares (2072-04-12)', 'press_release_2072_04_12', '', '2018_09_11_08_00_56_press_release_2072_04_12.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:00:56', '2018-09-11 12:16:38'),
(7, '	press release regarding transaction of demat shares (2072-04-21)', 'press_release_2072_04_21', '', '2018_09_11_08_01_20_press_release_2072_04_21.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:01:20', '2018-09-11 12:16:43'),
(8, 'press release regarding registration of companies (2072-04-31)', 'press_release_2072_04_31', '', '2018_09_11_08_01_38_press_release_2072_04_31.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:01:38', '2018-09-11 12:01:38'),
(9, 'press_release regarding completing of 3rd and 4th AGM (2072-05-09)', 'press_release_2072_05_09', '', '2018_09_11_08_02_03_press_release_2072_05_09.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:02:03', '2018-09-11 12:02:03'),
(10, 'press_release_appointment of CEO of CDS and Clearing Limited (2072-06-29)', 'press_release_2072_06_29', '', '2018_09_11_08_02_16_press_release_2072_06_29.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:02:16', '2018-09-11 12:02:16'),
(11, '	press release regarding transaction of demat shares (2072-08-09)', 'press_release_2072_08_09', '', '2018_09_11_08_02_40_press_release_2072_08_09.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:02:40', '2018-09-11 12:02:40'),
(12, '	press release regarding transaction of demat shares (2072-08-28)', 'press_release_2072_08_28', '', '2018_09_11_08_02_54_press_release_2072_08_28.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:02:54', '2018-09-11 12:02:54'),
(13, 'press_release_regarding registration of company for dematerialisation (2072-09-03)', 'press_release_2072_09_03', '', '2018_09_11_08_03_12_press_release_2072_09_03.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:03:12', '2018-09-11 12:03:12'),
(14, 'press_release_ regarding completing 5th year and starting 6th year of CDS and Clearing Limited (2072-09-07)', 'press_release_2072_09_07', '', '2018_09_11_08_03_30_press_release_2072_09_07.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:03:30', '2018-09-11 12:03:30'),
(15, 'press_release_regarding share dematerialisation of Chilime Lagubhittya Company Limited  (2072-09-19)', 'press_release_2072_09_19_01', '', '2018_09_11_08_03_53_press_release_2072_09_19_01.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:03:53', '2018-09-11 12:03:53'),
(16, 'press release regarding share dematerialisation of Guheshowri Merchant and Finance Limited (2072-09-21)', 'press_release_2072_09_21', '', '2018_09_11_08_05_58_press_release_2072_09_21.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:05:58', '2018-09-11 12:05:58'),
(17, 'press release regarding share dematerialisation of API Power Company Limited (2072-09-19)', 'press_release_2072_09_24', '', '2018_09_11_08_06_17_press_release_2072_09_24.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:06:17', '2018-09-11 12:06:17'),
(18, 'press_release_regarding 5th AGM (2072-09-29)', 'press_release_2072_09_29', '', '2018_09_11_08_06_40_press_release_2072_09_29.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:06:40', '2018-09-11 12:06:40'),
(19, 'press release regarding registration of company for dematerialisation (2072-10-03)', 'press_release_2072_10_03', '', '2018_09_11_08_07_00_press_release_2072_10_03.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:07:00', '2018-09-11 12:07:00'),
(20, '	press release regarding share dematerialisation of Jalbidhuth Lagani and Bikas Company Limited (2073-03-07)', 'press_release_2073_03_07', '', '2018_09_11_08_07_30_press_release_2073_03_07.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:07:30', '2018-09-11 12:07:30'),
(21, 'press_release_regarding charge for Brokers (2073-04-07)', 'press_release_2073_04_07', '', '2018_09_11_08_08_00_press_release_2073_04_07.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:08:00', '2018-09-11 12:08:00'),
(22, 'press release regarding completing 6th year and starting 7th year of CDS and Clearing Limited (2073-09-07)', 'press_release_2073_09_07', '', '2018_09_11_08_08_31_press_release_2073_09_07.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:08:31', '2018-09-11 12:08:31'),
(23, 'press_release_regarding_6th_AGM (2073-09-24)', 'press_release_2073_09_24_6th_AGM', '', '2018_09_11_08_08_50_press_release_2073_09_24_6th_AGM.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:08:50', '2018-09-11 12:08:50'),
(24, 'press_release_regarding Multiple Clearing Bank with Net Settlement (2074-03-16)', 'press_release_2074_03_16', '', '2018_09_11_08_09_16_press_release_2074_03_16.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:09:16', '2018-09-11 12:09:16'),
(25, 'press_release_regarding Multiple Clearing Bank with Net Settlement (2074-03-20)', 'press_release_2074_03_20_multiple_clearing_bank_with_net_settlement', '', '2018_09_11_08_09_57_press_release_2074_03_20_multiple_clearing_bank_with_net_settlement.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:09:57', '2018-09-11 12:09:57'),
(26, 'press_release_regarding_multiple_demat_account (2074-07-30)', 'press_release_2074_07_30_regarding_multiple_demat_account', '', '2018_09_11_08_10_24_press_release_2074_07_30_regarding_multiple_demat_account.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:10:24', '2018-09-11 12:10:24'),
(27, 'press_release_regarding_investor_awareness_program (2074-08-26)', 'press_release_2074_08_26_regarding_investor_awarness_program', '', '2018_09_11_08_10_54_press_release_2074_08_26_regarding_investor_awarness_program.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:10:54', '2018-09-11 12:10:54'),
(28, 'press_release_regarding_7th_AGM (2074-09-27)', 'press_release_2074_09_27_regarding_7th_AGM', '', '2018_09_11_08_11_26_press_release_2074_09_27_regarding_7th_AGM.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:11:26', '2018-09-11 12:11:26'),
(29, 'press_release_regarding_investor_awareness_program (2074-10-24)', 'press_release_2074_10_24_regarding_investor_awarness_program', '', '2018_09_11_08_12_03_press_release_2074_10_24_regarding_investor_awarness_program.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:12:03', '2018-09-11 12:12:03'),
(30, 'press_release_regarding_casba (2074-10-26)', 'press_release_2074_10_26_regarding_casba', '', '2018_09_11_08_12_46_press_release_2074_10_26_regarding_casba.pdf', '2018-09-11', 1, NULL, '2018-09-11 12:12:46', '2018-09-11 12:12:46'),
(31, 'press_release regarding_implementation_of_casba_ipo_allotment_aarambha_microfinance (2074-11-23)', 'press_release regarding_successfully_implemented_casba_ipo_allotment_aarambha_microfinance(_2074_11_23_)', '', '2018_09_11_08_13_08_press_release_2074_11_23_regarding_successfully_implemented_casba_ipo_allotment_aarambha_microfinance.pdf', '2018-09-12', 1, NULL, '2018-09-11 12:13:08', '2018-09-11 12:13:08'),
(32, 'press_release_regarding_investor_awareness_program (2074-11-29)', 'press_release_2074_11_29_regarding_investor_awareness_program', '', '2018_09_11_08_13_34_press_release_2074_11_29_regarding_investor_awareness_program.pdf', '2018-09-13', 1, NULL, '2018-09-11 12:13:34', '2018-09-11 12:13:34'),
(33, 'press_release regarding_investor_awareness_program (2074-12-04)', 'press_release regarding_investor_awareness_program(_2074_12_04)', '', '2018_09_11_08_14_14_press_release_2074_12_04_regarding_investor_awareness_program.pdf', '2018-09-14', 1, NULL, '2018-09-11 12:14:14', '2018-09-11 12:14:14'),
(34, 'press_release_regarding_investor_awareness_program (2074-12-18)', 'press_release_regarding_investor_awareness_program(_2074_12_18)', '', '2018_09_11_08_14_38_press_release_2074_12_18_regarding_investor_awareness_program.pdf', '2018-09-15', 1, NULL, '2018-09-11 12:14:38', '2018-09-11 12:14:38'),
(35, 'press release regarding meroshare_application (2075-03-19)', 'press release regarding meroshare_app (2075_03_19)', '', '2018_09_11_08_15_11_press_release_2075_03_19_regarding_meroshare_app.pdf', '2018-09-16', 1, NULL, '2018-09-11 12:15:11', '2018-09-11 12:15:11'),
(37, 'press release regarding EDIS (2075-06-18)', 'press release regarding EDIS (2075-06-18)', 'press release regarding EDIS (2075-06-18)', '2018_10_04_12_39_37_press_release_regarding_edis.pdf', '2018-10-11', 1, NULL, '2018-10-04 12:30:37', '2018-11-11 14:27:19');

-- --------------------------------------------------------

--
-- Table structure for table `rta`
--

CREATE TABLE `rta` (
  `id` int(11) NOT NULL,
  `rta_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dp_type` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `setup_date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rta`
--

INSERT INTO `rta` (`id`, `rta_id`, `dp_type`, `name`, `address`, `phone`, `email`, `setup_date`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '101', 4, 'NIBL ACE CAPITAL LIMITED - 2', 'LALDURBAR ,KATHMANDU,', ',014426161', 'ram@niblcapital.com', '2041-10-05', 1, NULL, NULL, NULL),
(2, '105', 4, 'SHIKHAR INSURANCE COMPANY LTD', 'SHIKHAR BIZ CENTER ,THAPATHALI,', '014246101,014246102', 'Shikharins@mos.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(3, '109', 4, 'NMB CAPITAL LIMITED', 'NMB BHAWAN ,BABARMAHAL,', '014253096,', 'info@nmbcl.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(4, '102', 4, 'BANK OF KATHMANDU LTD.', 'KAMALPOKHARI ,,', '014414541,', 'info@bok.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(5, '104', 4, 'HIMALAYAN GENERAL INSURANCE CO. LTD.', 'Babarmahal ,,', '014231788,014213014', 'ktm@hgi.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(6, '106', 4, 'NEPAL SBI BANK LTD', 'HATTISAR ,,', '014435516,014435613', 'nsblco@nsbl.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(7, '107', 4, 'PRIMELIFE INSURANCE COMPANY LIMITED', 'HATTISAR ,,', '014441414,', 'info@primelifenepal.com', '0000-00-00', 1, NULL, NULL, NULL),
(8, '108', 4, 'GLOBAL IME CAPITAL LIMITED', 'Jamal, Rastriya Naach Ghar ,3rd Floor,', '014222460,9851128189', 'info@elitecapital.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(9, '103', 4, 'LAXMI CAPITAL MARKET LIMITED', 'PULCHOWK ,,', '015551463,015551363', '', '0000-00-00', 1, NULL, NULL, NULL),
(10, '110', 4, 'CIVIL CAPITAL MARKET LIMITED', 'KAMALADI ,,', '4168654,', 'ccm@civilcapital.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(11, '126', 4, 'EVEREST BANK LIMITED', 'LAZIMPAT ,,', '014443377,9841270357', 'ebl@mos.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(12, '130', 4, 'TARAGAON REGENCY HOTELS LIMITED', 'BOUDDHA ,,', '014491234,014490035', 'info@taragaon.com', '0000-00-00', 1, NULL, NULL, NULL),
(13, '128', 4, 'JEBILS FINANCE LIMITED', 'NEW ROAD ,,', '014220426,014220439', 'info@jebils.com', '0000-00-00', 1, NULL, NULL, NULL),
(14, '119', 4, 'PRABHU CAPITAL LIMITED', 'KAMALADI ,KATHMANDU,', '014221952,014221946', 'info@prabhucapital.com', '0000-00-00', 1, NULL, NULL, NULL),
(15, '131', 4, 'EVEREST INSURANCE COMPANY LIMITED', 'HATTISAR ,,', '014445090,014445092', 'info@eic.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(16, '134', 4, 'LALITPUR FINANCE LIMITED', 'LAGANKHEL-5 ,LALITPUR,', '015536598,015523850', 'lfcl@wlink.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(17, '111', 4, 'NEPAL LIFE INSURANCE COMPANY LIMITED', 'HERITAGE PLAZA - 1 ,KAMALADI,', '014169082,014169083', 'info@nepallife.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(18, '112', 4, 'HIMALAYAN BANK LIMITED', 'KAMALADI ,KATHMANDU,', '014227749,014246219', 'himal@himalayanbank.com', '0000-00-00', 1, NULL, NULL, NULL),
(19, '114', 4, 'MACHHAPUCHCHHRE BANK LIMITED', 'LAZIMPAT ,,', '014428556,', 'machbank@mbl.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(20, '120', 4, 'NIBL ACE CAPITAL  LIMITED - 1', 'UTTARDHOKA ,LAZIMPAT,', '014005080,', 'info@niblcapital.com', '0000-00-00', 1, NULL, NULL, NULL),
(21, '123', 4, 'KUMARI BANK LIMITED', 'DURBAR MARG ,KATHMANDU,', '014221311,014221314', 'info@kumaribank.com', '0000-00-00', 1, NULL, NULL, NULL),
(22, '127', 4, 'VIBOR CAPITAL LIMITED', 'TRADE TOWER, THAPATHALI ,KATHMANDU,', '015111172,015111159', 'info.viborcapital@gmail.com', '0000-00-00', 1, NULL, NULL, NULL),
(23, '136', 4, 'UNION FINANCE LIMITED', 'C AND D BLOCK, 1ST FLOOR ,FOUR SQUARE COMPLEX,NARAYAN CHOUR, NAXAL', '014427091,014429337', 'ufl@union.com.np', '2042-00-04', 1, NULL, NULL, NULL),
(24, '113', 4, 'JANATA BANK NEPAL LIMITED', 'SANKHAMUL MARG - 10 ,NEW BANESHWOR,', '014786100,', 'info@janatabank.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(25, '115', 4, 'SUNRISE CAPITAL LIMITED', 'KAMAL POKHARI ,KATHMANDU,', '014439676,014425343', 'info@ncm.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(26, '116', 4, 'NABIL INVESTMENT BANKING LIMITED', 'NARAYANCHAUR, NAXAL ,KATHMANDU-1,', '014411604,014411733', 'nabilinvest@nabilbank.com', '0000-00-00', 1, NULL, NULL, NULL),
(27, '117', 4, 'NIDC DEVELOPMENT BANK LIMITED', 'NIDC BUILDING ,DURBARMARG,KATHMANDU', '014227221,', 'ibparajuli@yahoo.com', '0000-00-00', 1, NULL, NULL, NULL),
(28, '118', 4, 'CITIZEN INVESTMENT TRUST', 'NEW BANESHWOR ,KATHMANDU,', '014784522,014785321', 'cit.cmkt@gmail.com', '0000-00-00', 1, NULL, NULL, NULL),
(29, '121', 4, 'STANDARD CHARTERED BANK NEPAL LIMITED', 'NAYA BANESHWOR ,KATHMANDU,', '014782333,', 'singh.bimal@sc.com', '0000-00-00', 1, NULL, NULL, NULL),
(30, '132', 4, 'SIDDHARTHA FINANCE LIMITED', 'THAPATHALI ,KATHMANDU,', '014101518,014101521', 'sfl.bhw@gmail.com', '0000-00-00', 1, NULL, NULL, NULL),
(31, '133', 4, 'NECO INSURANCE LIMITED', 'NAVADURGA BHAVAN ,ANAMNAGAR,', '014770415,014770127', 'necoinsurance.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(32, '155', 4, 'RIDI HYDROPOWER DEVELOPMENT COMPANY LIMITED', 'THAPATHALI ,KATHMANDU,', '015111015,', 'ridihydro@wlink.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(33, '158', 4, 'SHREE INVESTMENT AND FINANCE COMPANY LIMITED', 'DILLIBAZAR, KATHMANDU ,,', '014422038,014426146', 'info@shreefinance.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(34, '159', 4, 'NATIONAL MICROFINANCE BITTIYA SANSTHA LIMITED', 'NILKHANTHA 3, DHADING ,,', '010520876,010520761', 'Rambdryadav@gmail.com', '0000-00-00', 1, NULL, NULL, NULL),
(35, '162', 4, 'KATHMANDU CAPITAL MARKET LIMITED', 'KAMALADI, ,,', '014168634,', 'kathmanducapitalmarket@kcml.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(36, '165', 4, 'KUBER MERTCHANT FINANCE LIMITED', 'GYANESHWOR - 33 ,,', '014416854,014416855', 'info@kubermerchant.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(37, '168', 4, 'RADHI BIDYUT COMPANY LIMITED', 'TRIPURESHWOR, KATHMANDU ,,', '014232749,014232750', 'nfo@radhibidyut.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(38, '139', 4, 'UNITED FINANCE LIMITED', 'I.J. PLAZA ,DURBARMARG,', '014241648,014222663', 'info@ufl.com.np', '2042-09-00', 1, NULL, NULL, NULL),
(39, '142', 4, 'SIDDHARTHA CAPITAL LIMITED', 'Ka. Ma. Na. Pa. ward no. 1 ,Narayanchour, Naxal,', '014420924,014420925', 'scl@siddharthacapital.com', '0000-00-00', 1, NULL, NULL, NULL),
(40, '156', 4, 'CENTRAL FINANCE LIMITED', 'WARD NO. 10, KUPONDOLE ,LALITPUR,', '015544517,', 'central@ntc.net.np', '0000-00-00', 1, NULL, NULL, NULL),
(41, '166', 4, 'NIC ASIA CAPITAL LIMITED', 'BABARMAHAL, KATHMANDU ,,', '01-4221994,01-4240664', 'info@nicasiacapital.com', '0000-00-00', 1, NULL, NULL, NULL),
(42, '152', 4, 'PASHCHIMANCHAL FINANCE COMPANY LIMITED', 'BUTWAL-10, SUKRAPATH ,RUPANDEHI,', '071544693,071544956', 'paficol@gmail.com', '0000-00-00', 1, NULL, NULL, NULL),
(43, '160', 4, 'CBIL CAPITAL LIMITED', 'DILLIBAZAR, KATHMANDU ,,', '014418667,014417723', 'info@cbilcapital.com', '0000-00-00', 1, NULL, NULL, NULL),
(44, '164', 4, 'NEPAL SBI MERCHANT BANKING LIMITED', 'HATTISAR, KATHMANDU ,,', '014412743,014412743', 'sharedpt@nsbl.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(45, '169', 4, 'NIC ASIA BANK LIMITED', 'THAPATHALI TTN, KATHMANDU ,,', '015111178,015111180', 'jagadishowr.devkota@nicasiabank.com', '0000-00-00', 1, NULL, NULL, NULL),
(46, '148', 4, 'GUHESWORI MERCHANT BANKING AND FINANCE LIMITED', 'HARIHAR BHAWAN ,PULCHOWK,LALITPUR', '015537407,015550406', 'gmbf@wlink.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(47, '150', 4, 'API POWER COMPANY LIMITED', '5 FLOOR, TRADE TOWER ,THAPATHALI, KATHMANDU,', '015111035,015111037', 'info@sae.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(48, '170', 4, 'CAPITAL MERCHANT BANKING AND FINANCE LIMITED', 'BATTISPUTALI ,,', '014471458,', 'info@cmbfl.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(49, '143', 4, 'LIFE INSURANCE CORPORATION (NEPAL) LIMITED', 'BHAGWATI BAHAL ,NAXAL, KATHMANDU,', '014443616,014443617', 'liccorporate@licnepal.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(50, '151', 4, 'PROGRESSIVE FINANCE LIMITED', 'PAKO ,NEW ROAD, KATHMANDU,', '014241244,', 'info@pfltd.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(51, '157', 4, 'BISHAL BAZAR COMPANY LIMITED', 'SUKRAPATH, KATHMANDU ,NEPAL,', '014242185,', 'info@bishalbazar.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(52, '146', 4, 'NATIONAL LIFE INSURANCE COMPANY LIMITED', 'LAZIMPAT ,KATHMANDU,', '014414799,', 'nlgilife@mail.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(53, '147', 4, 'ARUN VALLEY HYDROPOWER DEVELOPMENT COMPANY LIMITED', '2 FLOOR, TRADE TOWER NEPAL ,THAPATHALI,KATHMANDU', '015111086,015111085', 'arunvalley2054@gmail.com', '0000-00-00', 1, NULL, NULL, NULL),
(54, '140', 4, 'NEPAL DOORSANCHAR COMPANY LIMITED (NTC)', 'BHADRAKALI PLAZA ,KATHMANDU - 11,', '014210288,', 'cs.nt@ntc.net.np', '2042-10-04', 1, NULL, NULL, NULL),
(55, '141', 4, 'SOALTEE HOTEL LIMITED', 'KMC 13, TAHACHAL ,KATHMANDU,', '014272567,014273999', 'legal@soaltee.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(56, '145', 4, 'PRABHU INSURANCE LIMITED', 'TINKUNE ,KATHMANDU,', '014499220,', 'info@prabhuinsurance.com', '0000-00-00', 1, NULL, NULL, NULL),
(57, '149', 4, 'SAGARMATHA FINANCE LIMITED', 'MANBHAWAN ,LALITPUR,', '015547841,015535158', 'info@smb.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(58, '153', 4, 'PREMIER INSURANCE COMPANY (NEPAL) LIMITED', 'NARAYAN CHAUR ,NAXAL,', '014410648,014413543', 'premier@picl.com.np', '0000-00-00', 1, NULL, NULL, NULL),
(59, '161', 4, 'SANIMA CAPITAL LIMITED', 'NAXAL, KATHMANDU ,,', '014428956,014428957', 'sanimacapital@sanimabank.com', '0000-00-00', 1, NULL, NULL, NULL),
(60, '167', 4, 'WORLD MERCHANT BANKING AND FINANCE LIMITED', 'PUTALISADAK, KATHMANDU ,,', '014212099,', 'wmbfl2015@gmail.com', '0000-00-00', 1, NULL, NULL, NULL),
(61, '138', 4, 'PRABHU BANK LIMITED', 'BABARMAHAL ,KATHMANDU,', '014788500,', 'info@prabhubank.com', '2042-06-05', 1, NULL, NULL, NULL),
(62, '163', 4, 'ARUN KABELI POWER LIMITED', 'TRADE TOWER, THAPATHALI ,,', '015111085,', 'info@arunkabeli.com', '0000-00-00', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settlement`
--

CREATE TABLE `settlement` (
  `id` int(100) UNSIGNED NOT NULL,
  `settlement_id` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trade_date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settlement`
--

INSERT INTO `settlement` (`id`, `settlement_id`, `trade_date`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '1211002013008', '2013-04-02', 1, NULL, NULL, NULL),
(2, '1211002013013', '2013-04-08', 1, NULL, NULL, NULL),
(3, '1211002013014', '2013-04-09', 1, NULL, NULL, NULL),
(4, '1211002013020', '2013-04-21', 1, NULL, NULL, NULL),
(5, '1211002013022', '2013-04-23', 1, NULL, NULL, NULL),
(6, '1211002013006', '2013-03-31', 1, NULL, NULL, NULL),
(7, '1211002013024', '2013-04-28', 1, NULL, NULL, NULL),
(8, '1211002013026', '2013-04-30', 1, NULL, NULL, NULL),
(9, '1211002013028', '2013-05-05', 1, NULL, NULL, NULL),
(10, '1211002013037', '2013-05-15', 1, NULL, NULL, NULL),
(11, '1211002013055', '2013-06-10', 1, NULL, NULL, NULL),
(12, '1211002013071', '2013-06-30', 1, NULL, NULL, NULL),
(13, '1211002013075', '2013-07-04', 1, NULL, NULL, NULL),
(14, '1211002013076', '2013-07-05', 1, NULL, NULL, NULL),
(15, '1211002013078', '2013-07-08', 1, NULL, NULL, NULL),
(16, '1211002013018', '2013-04-17', 1, NULL, NULL, NULL),
(17, '1211002013019', '2013-04-18', 1, NULL, NULL, NULL),
(18, '1211002013027', '2013-05-02', 1, NULL, NULL, NULL),
(19, '1211002013011', '2013-04-05', 1, NULL, NULL, NULL),
(20, '1211002013016', '2013-04-15', 1, NULL, NULL, NULL),
(21, '1211002013034', '2013-05-12', 1, NULL, NULL, NULL),
(22, '1211002013038', '2013-05-16', 1, NULL, NULL, NULL),
(23, '1211002013040', '2013-05-20', 1, NULL, NULL, NULL),
(24, '1211002013045', '2013-05-26', 1, NULL, NULL, NULL),
(25, '1211002013062', '2013-06-18', 1, NULL, NULL, NULL),
(26, '1211002013064', '2013-06-20', 1, NULL, NULL, NULL),
(27, '1211002013065', '2013-06-21', 1, NULL, NULL, NULL),
(28, '1211002013066', '2013-06-23', 1, NULL, NULL, NULL),
(29, '1211002013074', '2013-07-03', 1, NULL, NULL, NULL),
(30, '1211002013002', '2013-03-25', 1, NULL, NULL, NULL),
(31, '1211002013003', '2013-03-27', 1, NULL, NULL, NULL),
(32, '1211002013001', '2013-03-24', 1, NULL, NULL, NULL),
(33, '1211002013007', '2013-04-01', 1, NULL, NULL, NULL),
(34, '1211002013010', '2013-04-04', 1, NULL, NULL, NULL),
(35, '1211002013015', '2013-04-11', 1, NULL, NULL, NULL),
(36, '1211002013025', '2013-04-29', 1, NULL, NULL, NULL),
(37, '1211002013029', '2013-05-06', 1, NULL, NULL, NULL),
(38, '1211002013031', '2013-05-08', 1, NULL, NULL, NULL),
(39, '1211002013032', '2013-05-09', 1, NULL, NULL, NULL),
(40, '1211002013035', '2013-05-13', 1, NULL, NULL, NULL),
(41, '1211002013039', '2013-05-19', 1, NULL, NULL, NULL),
(42, '1211002013046', '2013-05-27', 1, NULL, NULL, NULL),
(43, '1211002013048', '2013-05-30', 1, NULL, NULL, NULL),
(44, '1211002013052', '2013-06-05', 1, NULL, NULL, NULL),
(45, '1211002013054', '2013-06-09', 1, NULL, NULL, NULL),
(46, '1211002013056', '2013-06-11', 1, NULL, NULL, NULL),
(47, '1211002013057', '2013-06-12', 1, NULL, NULL, NULL),
(48, '1211002013060', '2013-06-16', 1, NULL, NULL, NULL),
(49, '1211002013067', '2013-06-24', 1, NULL, NULL, NULL),
(50, '1211002013070', '2013-06-27', 1, NULL, NULL, NULL),
(51, '1211002013079', '2013-07-09', 1, NULL, NULL, NULL),
(52, '1211002013080', '2013-07-10', 1, NULL, NULL, NULL),
(53, '1211002013009', '2013-04-03', 1, NULL, NULL, NULL),
(54, '1211002013012', '2013-04-07', 1, NULL, NULL, NULL),
(55, '1211002013004', '2013-03-28', 1, NULL, NULL, NULL),
(56, '1211002013033', '2013-05-10', 1, NULL, NULL, NULL),
(57, '1211002013036', '2013-05-14', 1, NULL, NULL, NULL),
(58, '1211002013042', '2013-05-22', 1, NULL, NULL, NULL),
(59, '1211002013043', '2013-05-23', 1, NULL, NULL, NULL),
(60, '1211002013044', '2013-05-24', 1, NULL, NULL, NULL),
(61, '1211002013047', '2013-05-28', 1, NULL, NULL, NULL),
(62, '1211002013050', '2013-06-03', 1, NULL, NULL, NULL),
(63, '1211002013053', '2013-06-06', 1, NULL, NULL, NULL),
(64, '1211002013058', '2013-06-13', 1, NULL, NULL, NULL),
(65, '1211002013061', '2013-06-17', 1, NULL, NULL, NULL),
(66, '1211002013063', '2013-06-19', 1, NULL, NULL, NULL),
(67, '1211002013069', '2013-06-26', 1, NULL, NULL, NULL),
(68, '1211002013073', '2013-07-02', 1, NULL, NULL, NULL),
(69, '1211002013081', '2013-07-11', 1, NULL, NULL, NULL),
(70, '1211002013017', '2013-04-16', 1, NULL, NULL, NULL),
(71, '1211002013021', '2013-04-22', 1, NULL, NULL, NULL),
(72, '1211002013023', '2013-04-25', 1, NULL, NULL, NULL),
(73, '1211002013030', '2013-05-07', 1, NULL, NULL, NULL),
(74, '1211002013005', '2013-03-29', 1, NULL, NULL, NULL),
(75, '1211002013041', '2013-05-21', 1, NULL, NULL, NULL),
(76, '1211002013049', '2013-06-02', 1, NULL, NULL, NULL),
(77, '1211002013051', '2013-06-04', 1, NULL, NULL, NULL),
(78, '1211002013059', '2013-06-14', 1, NULL, NULL, NULL),
(79, '1211002013068', '2013-06-25', 1, NULL, NULL, NULL),
(80, '1211002013072', '2013-07-01', 1, NULL, NULL, NULL),
(81, '1211002013077', '2013-07-07', 1, NULL, NULL, NULL),
(82, '1211002013082', '2013-07-14', 1, NULL, NULL, NULL),
(83, '1211002013083', '2013-07-15', 1, NULL, NULL, NULL),
(84, '1211002013088', '2013-07-22', 1, NULL, NULL, NULL),
(85, '1211002013089', '2013-07-23', 1, NULL, NULL, NULL),
(86, '1211002013094', '2013-07-29', 1, NULL, NULL, NULL),
(87, '1211002013095', '2013-07-30', 1, NULL, NULL, NULL),
(88, '1211002013098', '2013-08-02', 1, NULL, NULL, NULL),
(89, '1211002013102', '2013-08-07', 1, NULL, NULL, NULL),
(90, '1211002013106', '2013-08-13', 1, NULL, NULL, NULL),
(91, '1211002013112', '2013-08-20', 1, NULL, NULL, NULL),
(92, '1211002013113', '2013-08-23', 1, NULL, NULL, NULL),
(93, '1211002013114', '2013-08-25', 1, NULL, NULL, NULL),
(94, '1211002013116', '2013-08-27', 1, NULL, NULL, NULL),
(95, '1211002013119', '2013-09-01', 1, NULL, NULL, NULL),
(96, '1211002013120', '2013-09-02', 1, NULL, NULL, NULL),
(97, '1211002013122', '2013-09-04', 1, NULL, NULL, NULL),
(98, '1211002013123', '2013-09-05', 1, NULL, NULL, NULL),
(99, '1211002013126', '2013-09-09', 1, NULL, NULL, NULL),
(100, '1211002013127', '2013-09-10', 1, NULL, NULL, NULL),
(101, '1211002013128', '2013-09-11', 1, NULL, NULL, NULL),
(102, '1211002013129', '2013-09-12', 1, NULL, NULL, NULL),
(103, '1211002013138', '2013-09-24', 1, NULL, NULL, NULL),
(104, '1211002013144', '2013-10-01', 1, NULL, NULL, NULL),
(105, '1211002013146', '2013-10-03', 1, NULL, NULL, NULL),
(106, '1211002013151', '2013-10-09', 1, NULL, NULL, NULL),
(107, '1211002013153', '2013-10-17', 1, NULL, NULL, NULL),
(108, '1211002013156', '2013-10-22', 1, NULL, NULL, NULL),
(109, '1211002013160', '2013-10-27', 1, NULL, NULL, NULL),
(110, '1211002013165', '2013-11-01', 1, NULL, NULL, NULL),
(111, '1211002013166', '2013-11-06', 1, NULL, NULL, NULL),
(112, '1211002013171', '2013-11-13', 1, NULL, NULL, NULL),
(113, '1211002013172', '2013-11-14', 1, NULL, NULL, NULL),
(114, '1211002013174', '2013-11-21', 1, NULL, NULL, NULL),
(115, '1211002013179', '2013-11-28', 1, NULL, NULL, NULL),
(116, '1211002013183', '2013-12-03', 1, NULL, NULL, NULL),
(117, '1211002013187', '2013-12-08', 1, NULL, NULL, NULL),
(118, '1211002013197', '2013-12-20', 1, NULL, NULL, NULL),
(119, '1211002013200', '2013-12-24', 1, NULL, NULL, NULL),
(120, '1211002013204', '2013-12-31', 1, NULL, NULL, NULL),
(121, '1211002013205', '2014-01-01', 1, NULL, NULL, NULL),
(122, '1211002013208', '2014-01-05', 1, NULL, NULL, NULL),
(123, '1211002013212', '2014-01-09', 1, NULL, NULL, NULL),
(124, '1211002013216', '2014-01-14', 1, NULL, NULL, NULL),
(125, '1211002014003', '2014-01-24', 1, NULL, NULL, NULL),
(126, '1211002014005', '2014-01-27', 1, NULL, NULL, NULL),
(127, '1211002014006', '2014-01-28', 1, NULL, NULL, NULL),
(128, '1211002014007', '2014-01-29', 1, NULL, NULL, NULL),
(129, '1211002014008', '2014-02-02', 1, NULL, NULL, NULL),
(130, '1211002014010', '2014-02-04', 1, NULL, NULL, NULL),
(131, '1211002014011', '2014-02-05', 1, NULL, NULL, NULL),
(132, '1211002014013', '2014-02-07', 1, NULL, NULL, NULL),
(133, '1211002013085', '2013-07-17', 1, NULL, NULL, NULL),
(134, '1211002013086', '2013-07-18', 1, NULL, NULL, NULL),
(135, '1211002013087', '2013-07-21', 1, NULL, NULL, NULL),
(136, '1211002013090', '2013-07-24', 1, NULL, NULL, NULL),
(137, '1211002013091', '2013-07-25', 1, NULL, NULL, NULL),
(138, '1211002013099', '2013-08-04', 1, NULL, NULL, NULL),
(139, '1211002013101', '2013-08-06', 1, NULL, NULL, NULL),
(140, '1211002013103', '2013-08-08', 1, NULL, NULL, NULL),
(141, '1211002013104', '2013-08-11', 1, NULL, NULL, NULL),
(142, '1211002013107', '2013-08-14', 1, NULL, NULL, NULL),
(143, '1211002013110', '2013-08-18', 1, NULL, NULL, NULL),
(144, '1211002013111', '2013-08-19', 1, NULL, NULL, NULL),
(145, '1211002013115', '2013-08-26', 1, NULL, NULL, NULL),
(146, '1211002013121', '2013-09-03', 1, NULL, NULL, NULL),
(147, '1211002013124', '2013-09-06', 1, NULL, NULL, NULL),
(148, '1211002013133', '2013-09-17', 1, NULL, NULL, NULL),
(149, '1211002013134', '2013-09-19', 1, NULL, NULL, NULL),
(150, '1211002013135', '2013-09-20', 1, NULL, NULL, NULL),
(151, '1211002013137', '2013-09-23', 1, NULL, NULL, NULL),
(152, '1211002013140', '2013-09-26', 1, NULL, NULL, NULL),
(153, '1211002013141', '2013-09-27', 1, NULL, NULL, NULL),
(154, '1211002013143', '2013-09-30', 1, NULL, NULL, NULL),
(155, '1211002013145', '2013-10-02', 1, NULL, NULL, NULL),
(156, '1211002013147', '2013-10-04', 1, NULL, NULL, NULL),
(157, '1211002013149', '2013-10-07', 1, NULL, NULL, NULL),
(158, '1211002013152', '2013-10-10', 1, NULL, NULL, NULL),
(159, '1211002013155', '2013-10-21', 1, NULL, NULL, NULL),
(160, '1211002013158', '2013-10-24', 1, NULL, NULL, NULL),
(161, '1211002013159', '2013-10-25', 1, NULL, NULL, NULL),
(162, '1211002013164', '2013-10-31', 1, NULL, NULL, NULL),
(163, '1211002013167', '2013-11-07', 1, NULL, NULL, NULL),
(164, '1211002013169', '2013-11-11', 1, NULL, NULL, NULL),
(165, '1211002013176', '2013-11-25', 1, NULL, NULL, NULL),
(166, '1211002013178', '2013-11-27', 1, NULL, NULL, NULL),
(167, '1211002013181', '2013-12-01', 1, NULL, NULL, NULL),
(168, '1211002013184', '2013-12-04', 1, NULL, NULL, NULL),
(169, '1211002013186', '2013-12-06', 1, NULL, NULL, NULL),
(170, '1211002013189', '2013-12-10', 1, NULL, NULL, NULL),
(171, '1211002013191', '2013-12-12', 1, NULL, NULL, NULL),
(172, '1211002013192', '2013-12-13', 1, NULL, NULL, NULL),
(173, '1211002013195', '2013-12-18', 1, NULL, NULL, NULL),
(174, '1211002013196', '2013-12-19', 1, NULL, NULL, NULL),
(175, '1211002013198', '2013-12-22', 1, NULL, NULL, NULL),
(176, '1211002013201', '2013-12-26', 1, NULL, NULL, NULL),
(177, '1211002013203', '2013-12-29', 1, NULL, NULL, NULL),
(178, '1211002013206', '2014-01-02', 1, NULL, NULL, NULL),
(179, '1211002013211', '2014-01-08', 1, NULL, NULL, NULL),
(180, '1211002014002', '2014-01-23', 1, NULL, NULL, NULL),
(181, '1211002014004', '2014-01-26', 1, NULL, NULL, NULL),
(182, '1211002014009', '2014-02-03', 1, NULL, NULL, NULL),
(183, '1211002014012', '2014-02-06', 1, NULL, NULL, NULL),
(184, '1211002013084', '2013-07-16', 1, NULL, NULL, NULL),
(185, '1211002013092', '2013-07-26', 1, NULL, NULL, NULL),
(186, '1211002013093', '2013-07-28', 1, NULL, NULL, NULL),
(187, '1211002013096', '2013-07-31', 1, NULL, NULL, NULL),
(188, '1211002013097', '2013-08-01', 1, NULL, NULL, NULL),
(189, '1211002013100', '2013-08-05', 1, NULL, NULL, NULL),
(190, '1211002013105', '2013-08-12', 1, NULL, NULL, NULL),
(191, '1211002013108', '2013-08-15', 1, NULL, NULL, NULL),
(192, '1211002013109', '2013-08-16', 1, NULL, NULL, NULL),
(193, '1211002013117', '2013-08-29', 1, NULL, NULL, NULL),
(194, '1211002013118', '2013-08-30', 1, NULL, NULL, NULL),
(195, '1211002013125', '2013-09-08', 1, NULL, NULL, NULL),
(196, '1211002013130', '2013-09-13', 1, NULL, NULL, NULL),
(197, '1211002013131', '2013-09-15', 1, NULL, NULL, NULL),
(198, '1211002013132', '2013-09-16', 1, NULL, NULL, NULL),
(199, '1211002013136', '2013-09-22', 1, NULL, NULL, NULL),
(200, '1211002013139', '2013-09-25', 1, NULL, NULL, NULL),
(201, '1211002013142', '2013-09-29', 1, NULL, NULL, NULL),
(202, '1211002013148', '2013-10-06', 1, NULL, NULL, NULL),
(203, '1211002013150', '2013-10-08', 1, NULL, NULL, NULL),
(204, '1211002013154', '2013-10-20', 1, NULL, NULL, NULL),
(205, '1211002013157', '2013-10-23', 1, NULL, NULL, NULL),
(206, '1211002013161', '2013-10-28', 1, NULL, NULL, NULL),
(207, '1211002013162', '2013-10-29', 1, NULL, NULL, NULL),
(208, '1211002013163', '2013-10-30', 1, NULL, NULL, NULL),
(209, '1211002013168', '2013-11-10', 1, NULL, NULL, NULL),
(210, '1211002013170', '2013-11-12', 1, NULL, NULL, NULL),
(211, '1211002013173', '2013-11-15', 1, NULL, NULL, NULL),
(212, '1211002013175', '2013-11-24', 1, NULL, NULL, NULL),
(213, '1211002013177', '2013-11-26', 1, NULL, NULL, NULL),
(214, '1211002013180', '2013-11-29', 1, NULL, NULL, NULL),
(215, '1211002013182', '2013-12-02', 1, NULL, NULL, NULL),
(216, '1211002013185', '2013-12-05', 1, NULL, NULL, NULL),
(217, '1211002013188', '2013-12-09', 1, NULL, NULL, NULL),
(218, '1211002013190', '2013-12-11', 1, NULL, NULL, NULL),
(219, '1211002013193', '2013-12-15', 1, NULL, NULL, NULL),
(220, '1211002013194', '2013-12-16', 1, NULL, NULL, NULL),
(221, '1211002013199', '2013-12-23', 1, NULL, NULL, NULL),
(222, '1211002013202', '2013-12-27', 1, NULL, NULL, NULL),
(223, '1211002013207', '2014-01-03', 1, NULL, NULL, NULL),
(224, '1211002013209', '2014-01-06', 1, NULL, NULL, NULL),
(225, '1211002013210', '2014-01-07', 1, NULL, NULL, NULL),
(226, '1211002013213', '2014-01-10', 1, NULL, NULL, NULL),
(227, '1211002013214', '2014-01-12', 1, NULL, NULL, NULL),
(228, '1211002013215', '2014-01-13', 1, NULL, NULL, NULL),
(229, '1211002013217', '2014-01-16', 1, NULL, NULL, NULL),
(230, '1211002013218', '2014-01-17', 1, NULL, NULL, NULL),
(231, '1211002013219', '2014-01-19', 1, NULL, NULL, NULL),
(232, '1211002013220', '2014-01-20', 1, NULL, NULL, NULL),
(233, '1211002013221', '2014-01-21', 1, NULL, NULL, NULL),
(234, '1211002014001', '2014-01-22', 1, NULL, NULL, NULL),
(235, '1211002014014', '2014-02-09', 1, NULL, NULL, NULL),
(236, '1211002014026', '2014-02-24', 1, NULL, NULL, NULL),
(237, '1211002014058', '2014-04-07', 1, NULL, NULL, NULL),
(238, '1211002014081', '2014-05-08', 1, NULL, NULL, NULL),
(239, '1211002014122', '2014-06-29', 1, NULL, NULL, NULL),
(240, '1211002014134', '2014-07-13', 1, NULL, NULL, NULL),
(241, '1211002014140', '2014-07-20', 1, NULL, NULL, NULL),
(242, '1211002014151', '2014-08-03', 1, NULL, NULL, NULL),
(243, '1211002014159', '2014-08-14', 1, NULL, NULL, NULL),
(244, '1211002014164', '2014-08-21', 1, NULL, NULL, NULL),
(245, '1211002014174', '2014-09-03', 1, NULL, NULL, NULL),
(246, '1211002014180', '2014-09-11', 1, NULL, NULL, NULL),
(247, '1211002014189', '2014-09-22', 1, NULL, NULL, NULL),
(248, '1211002014191', '2014-09-24', 1, NULL, NULL, NULL),
(249, '1211002014211', '2014-10-28', 1, NULL, NULL, NULL),
(250, '1211002014213', '2014-10-31', 1, NULL, NULL, NULL),
(251, '1211002014245', '2014-12-10', 1, NULL, NULL, NULL),
(252, '1211002014247', '2014-12-12', 1, NULL, NULL, NULL),
(253, '1211002014255', '2014-12-22', 1, NULL, NULL, NULL),
(254, '1211002014259', '2014-12-28', 1, NULL, NULL, NULL),
(255, '1211002014261', '2014-12-31', 1, NULL, NULL, NULL),
(256, '1211002014263', '2015-01-02', 1, NULL, NULL, NULL),
(257, '1211002015004', '2015-01-08', 1, NULL, NULL, NULL),
(258, '1211002015037', '2015-02-22', 1, NULL, NULL, NULL),
(259, '1211002015044', '2015-03-02', 1, NULL, NULL, NULL),
(260, '1211002015054', '2015-03-16', 1, NULL, NULL, NULL),
(261, '1211002015064', '2015-03-29', 1, NULL, NULL, NULL),
(262, '1211002015070', '2015-04-05', 1, NULL, NULL, NULL),
(263, '1211002015076', '2015-04-12', 1, NULL, NULL, NULL),
(264, '1211002015078', '2015-04-15', 1, NULL, NULL, NULL),
(265, '1211002015079', '2015-04-16', 1, NULL, NULL, NULL),
(266, '1211002015111', '2015-06-24', 1, NULL, NULL, NULL),
(267, '1211002015113', '2015-06-26', 1, NULL, NULL, NULL),
(268, '1211002015123', '2015-07-08', 1, NULL, NULL, NULL),
(269, '1211002015124', '2015-07-09', 1, NULL, NULL, NULL),
(270, '1211002015125', '2015-07-10', 1, NULL, NULL, NULL),
(271, '1211002015127', '2015-07-13', 1, NULL, NULL, NULL),
(272, '1211002015128', '2015-07-14', 1, NULL, NULL, NULL),
(273, '1211002015129', '2015-07-15', 1, NULL, NULL, NULL),
(274, '1211002015131', '2015-07-17', 1, NULL, NULL, NULL),
(275, '1211002015132', '2015-07-22', 1, NULL, NULL, NULL),
(276, '1211002015133', '2015-07-23', 1, NULL, NULL, NULL),
(277, '1211002015134', '2015-07-24', 1, NULL, NULL, NULL),
(278, '1211002015135', '2015-07-26', 1, NULL, NULL, NULL),
(279, '1211002015137', '2015-07-28', 1, NULL, NULL, NULL),
(280, '1211002015138', '2015-07-29', 1, NULL, NULL, NULL),
(281, '1211002015139', '2015-07-30', 1, NULL, NULL, NULL),
(282, '1211002015140', '2015-07-31', 1, NULL, NULL, NULL),
(283, '1211002015141', '2015-08-02', 1, NULL, NULL, NULL),
(284, '1211002015143', '2015-08-04', 1, NULL, NULL, NULL),
(285, '1211002015144', '2015-08-05', 1, NULL, NULL, NULL),
(286, '1211002014017', '2014-02-12', 1, NULL, NULL, NULL),
(287, '1211002014019', '2014-02-14', 1, NULL, NULL, NULL),
(288, '1211002014027', '2014-02-25', 1, NULL, NULL, NULL),
(289, '1211002014039', '2014-03-13', 1, NULL, NULL, NULL),
(290, '1211002014042', '2014-03-18', 1, NULL, NULL, NULL),
(291, '1211002014047', '2014-03-24', 1, NULL, NULL, NULL),
(292, '1211002014063', '2014-04-15', 1, NULL, NULL, NULL),
(293, '1211002014071', '2014-04-25', 1, NULL, NULL, NULL),
(294, '1211002014101', '2014-06-03', 1, NULL, NULL, NULL),
(295, '1211002014115', '2014-06-19', 1, NULL, NULL, NULL),
(296, '1211002014119', '2014-06-25', 1, NULL, NULL, NULL),
(297, '1211002014127', '2014-07-04', 1, NULL, NULL, NULL),
(298, '1211002014141', '2014-07-21', 1, NULL, NULL, NULL),
(299, '1211002014147', '2014-07-28', 1, NULL, NULL, NULL),
(300, '1211002014155', '2014-08-07', 1, NULL, NULL, NULL),
(301, '1211002014171', '2014-08-29', 1, NULL, NULL, NULL),
(302, '1211002014198', '2014-10-10', 1, NULL, NULL, NULL),
(303, '1211002014202', '2014-10-15', 1, NULL, NULL, NULL),
(304, '1211002014204', '2014-10-17', 1, NULL, NULL, NULL),
(305, '1211002014209', '2014-10-26', 1, NULL, NULL, NULL),
(306, '1211002014217', '2014-11-05', 1, NULL, NULL, NULL),
(307, '1211002014249', '2014-12-15', 1, NULL, NULL, NULL),
(308, '1211002014267', '2015-01-07', 1, NULL, NULL, NULL),
(309, '1211002015007', '2015-01-12', 1, NULL, NULL, NULL),
(310, '1211002015023', '2015-02-03', 1, NULL, NULL, NULL),
(311, '1211002015024', '2015-02-04', 1, NULL, NULL, NULL),
(312, '1211002015025', '2015-02-05', 1, NULL, NULL, NULL),
(313, '1211002015040', '2015-02-25', 1, NULL, NULL, NULL),
(314, '1211002015051', '2015-03-12', 1, NULL, NULL, NULL),
(315, '1211002015058', '2015-03-22', 1, NULL, NULL, NULL),
(316, '1211002015072', '2015-04-07', 1, NULL, NULL, NULL),
(317, '1211002015074', '2015-04-09', 1, NULL, NULL, NULL),
(318, '1211002015082', '2015-04-21', 1, NULL, NULL, NULL),
(319, '1211002015084', '2015-04-23', 1, NULL, NULL, NULL),
(320, '1211002015086', '2015-05-25', 1, NULL, NULL, NULL),
(321, '1211002015088', '2015-05-27', 1, NULL, NULL, NULL),
(322, '1211002015090', '2015-05-31', 1, NULL, NULL, NULL),
(323, '1211002015092', '2015-06-02', 1, NULL, NULL, NULL),
(324, '1211002015094', '2015-06-04', 1, NULL, NULL, NULL),
(325, '1211002015096', '2015-06-07', 1, NULL, NULL, NULL),
(326, '1211002015098', '2015-06-09', 1, NULL, NULL, NULL),
(327, '1211002015103', '2015-06-15', 1, NULL, NULL, NULL),
(328, '1211002015142', '2015-08-03', 1, NULL, NULL, NULL),
(329, '1211002015145', '2015-08-06', 1, NULL, NULL, NULL),
(330, '1211002015146', '2015-08-07', 1, NULL, NULL, NULL),
(331, '1211002015147', '2015-08-09', 1, NULL, NULL, NULL),
(332, '1211002015148', '2015-08-10', 1, NULL, NULL, NULL),
(333, '1211002015149', '2015-08-11', 1, NULL, NULL, NULL),
(334, '1211002015150', '2015-08-12', 1, NULL, NULL, NULL),
(335, '1211002015151', '2015-08-13', 1, NULL, NULL, NULL),
(336, '1211002015152', '2015-08-14', 1, NULL, NULL, NULL),
(337, '1211002014018', '2014-02-13', 1, NULL, NULL, NULL),
(338, '1211002014020', '2014-02-16', 1, NULL, NULL, NULL),
(339, '1211002014031', '2014-03-04', 1, NULL, NULL, NULL),
(340, '1211002014032', '2014-03-05', 1, NULL, NULL, NULL),
(341, '1211002014054', '2014-04-02', 1, NULL, NULL, NULL),
(342, '1211002014056', '2014-04-04', 1, NULL, NULL, NULL),
(343, '1211002014059', '2014-04-09', 1, NULL, NULL, NULL),
(344, '1211002014061', '2014-04-11', 1, NULL, NULL, NULL),
(345, '1211002014069', '2014-04-22', 1, NULL, NULL, NULL),
(346, '1211002014075', '2014-04-30', 1, NULL, NULL, NULL),
(347, '1211002014106', '2014-06-09', 1, NULL, NULL, NULL),
(348, '1211002014116', '2014-06-20', 1, NULL, NULL, NULL),
(349, '1211002014132', '2014-07-10', 1, NULL, NULL, NULL),
(350, '1211002014137', '2014-07-16', 1, NULL, NULL, NULL),
(351, '1211002014139', '2014-07-18', 1, NULL, NULL, NULL),
(352, '1211002014162', '2014-08-19', 1, NULL, NULL, NULL),
(353, '1211002014166', '2014-08-24', 1, NULL, NULL, NULL),
(354, '1211002014168', '2014-08-26', 1, NULL, NULL, NULL),
(355, '1211002014170', '2014-08-28', 1, NULL, NULL, NULL),
(356, '1211002014173', '2014-09-01', 1, NULL, NULL, NULL),
(357, '1211002014175', '2014-09-04', 1, NULL, NULL, NULL),
(358, '1211002014178', '2014-09-09', 1, NULL, NULL, NULL),
(359, '1211002014179', '2014-09-10', 1, NULL, NULL, NULL),
(360, '1211002014184', '2014-09-16', 1, NULL, NULL, NULL),
(361, '1211002014186', '2014-09-18', 1, NULL, NULL, NULL),
(362, '1211002014192', '2014-09-26', 1, NULL, NULL, NULL),
(363, '1211002014193', '2014-09-28', 1, NULL, NULL, NULL),
(364, '1211002014194', '2014-09-29', 1, NULL, NULL, NULL),
(365, '1211002014205', '2014-10-19', 1, NULL, NULL, NULL),
(366, '1211002014208', '2014-10-22', 1, NULL, NULL, NULL),
(367, '1211002014228', '2014-11-18', 1, NULL, NULL, NULL),
(368, '1211002014230', '2014-11-20', 1, NULL, NULL, NULL),
(369, '1211002014237', '2014-12-01', 1, NULL, NULL, NULL),
(370, '1211002014239', '2014-12-03', 1, NULL, NULL, NULL),
(371, '1211002014240', '2014-12-04', 1, NULL, NULL, NULL),
(372, '1211002014241', '2014-12-05', 1, NULL, NULL, NULL),
(373, '1211002014243', '2014-12-08', 1, NULL, NULL, NULL),
(374, '1211002015001', '2015-01-05', 1, NULL, NULL, NULL),
(375, '1211002015011', '2015-01-18', 1, NULL, NULL, NULL),
(376, '1211002015018', '2015-01-27', 1, NULL, NULL, NULL),
(377, '1211002015046', '2015-03-04', 1, NULL, NULL, NULL),
(378, '1211002015069', '2015-04-03', 1, NULL, NULL, NULL),
(379, '1211002015105', '2015-06-17', 1, NULL, NULL, NULL),
(380, '1211002015107', '2015-06-19', 1, NULL, NULL, NULL),
(381, '1211002015108', '2015-06-21', 1, NULL, NULL, NULL),
(382, '1211002015110', '2015-06-23', 1, NULL, NULL, NULL),
(383, '1211002015112', '2015-06-25', 1, NULL, NULL, NULL),
(384, '1211002015115', '2015-06-29', 1, NULL, NULL, NULL),
(385, '1211002015126', '2015-07-12', 1, NULL, NULL, NULL),
(386, '1211002015130', '2015-07-16', 1, NULL, NULL, NULL),
(387, '1211002015136', '2015-07-27', 1, NULL, NULL, NULL),
(388, '1211002014028', '2014-02-26', 1, NULL, NULL, NULL),
(389, '1211002014029', '2014-02-28', 1, NULL, NULL, NULL),
(390, '1211002014030', '2014-03-03', 1, NULL, NULL, NULL),
(391, '1211002014036', '2014-03-10', 1, NULL, NULL, NULL),
(392, '1211002014040', '2014-03-14', 1, NULL, NULL, NULL),
(393, '1211002014043', '2014-03-19', 1, NULL, NULL, NULL),
(394, '1211002014050', '2014-03-27', 1, NULL, NULL, NULL),
(395, '1211002014051', '2014-03-28', 1, NULL, NULL, NULL),
(396, '1211002014060', '2014-04-10', 1, NULL, NULL, NULL),
(397, '1211002014064', '2014-04-16', 1, NULL, NULL, NULL),
(398, '1211002014079', '2014-05-06', 1, NULL, NULL, NULL),
(399, '1211002014091', '2014-05-21', 1, NULL, NULL, NULL),
(400, '1211002014096', '2014-05-27', 1, NULL, NULL, NULL),
(401, '1211002014097', '2014-05-28', 1, NULL, NULL, NULL),
(402, '1211002014098', '2014-05-30', 1, NULL, NULL, NULL),
(403, '1211002014107', '2014-06-10', 1, NULL, NULL, NULL),
(404, '1211002014113', '2014-06-17', 1, NULL, NULL, NULL),
(405, '1211002014124', '2014-07-01', 1, NULL, NULL, NULL),
(406, '1211002014130', '2014-07-08', 1, NULL, NULL, NULL),
(407, '1211002014131', '2014-07-09', 1, NULL, NULL, NULL),
(408, '1211002014146', '2014-07-27', 1, NULL, NULL, NULL),
(409, '1211002014157', '2014-08-12', 1, NULL, NULL, NULL),
(410, '1211002014158', '2014-08-13', 1, NULL, NULL, NULL),
(411, '1211002014181', '2014-09-12', 1, NULL, NULL, NULL),
(412, '1211002014188', '2014-09-21', 1, NULL, NULL, NULL),
(413, '1211002014190', '2014-09-23', 1, NULL, NULL, NULL),
(414, '1211002014196', '2014-10-08', 1, NULL, NULL, NULL),
(415, '1211002014200', '2014-10-13', 1, NULL, NULL, NULL),
(416, '1211002014203', '2014-10-16', 1, NULL, NULL, NULL),
(417, '1211002014212', '2014-10-30', 1, NULL, NULL, NULL),
(418, '1211002014215', '2014-11-03', 1, NULL, NULL, NULL),
(419, '1211002014221', '2014-11-10', 1, NULL, NULL, NULL),
(420, '1211002014223', '2014-11-12', 1, NULL, NULL, NULL),
(421, '1211002014226', '2014-11-16', 1, NULL, NULL, NULL),
(422, '1211002014235', '2014-11-28', 1, NULL, NULL, NULL),
(423, '1211002014248', '2014-12-14', 1, NULL, NULL, NULL),
(424, '1211002014260', '2014-12-29', 1, NULL, NULL, NULL),
(425, '1211002014264', '2015-01-04', 1, NULL, NULL, NULL),
(426, '1211002014266', '2015-01-06', 1, NULL, NULL, NULL),
(427, '1211002014268', '2015-01-08', 1, NULL, NULL, NULL),
(428, '1211002015006', '2015-01-11', 1, NULL, NULL, NULL),
(429, '1211002015008', '2015-01-13', 1, NULL, NULL, NULL),
(430, '1211002015012', '2015-01-19', 1, NULL, NULL, NULL),
(431, '1211002015014', '2015-01-22', 1, NULL, NULL, NULL),
(432, '1211002015015', '2015-01-23', 1, NULL, NULL, NULL),
(433, '1211002015027', '2015-02-08', 1, NULL, NULL, NULL),
(434, '1211002015029', '2015-02-10', 1, NULL, NULL, NULL),
(435, '1211002015031', '2015-02-12', 1, NULL, NULL, NULL),
(436, '1211002015045', '2015-03-03', 1, NULL, NULL, NULL),
(437, '1211002015047', '2015-03-06', 1, NULL, NULL, NULL),
(438, '1211002015055', '2015-03-17', 1, NULL, NULL, NULL),
(439, '1211002014021', '2014-02-17', 1, NULL, NULL, NULL),
(440, '1211002014023', '2014-02-20', 1, NULL, NULL, NULL),
(441, '1211002014038', '2014-03-12', 1, NULL, NULL, NULL),
(442, '1211002014045', '2014-03-21', 1, NULL, NULL, NULL),
(443, '1211002014048', '2014-03-25', 1, NULL, NULL, NULL),
(444, '1211002014049', '2014-03-26', 1, NULL, NULL, NULL),
(445, '1211002014052', '2014-03-31', 1, NULL, NULL, NULL),
(446, '1211002014062', '2014-04-13', 1, NULL, NULL, NULL),
(447, '1211002014066', '2014-04-18', 1, NULL, NULL, NULL),
(448, '1211002014070', '2014-04-23', 1, NULL, NULL, NULL),
(449, '1211002014073', '2014-04-28', 1, NULL, NULL, NULL),
(450, '1211002014076', '2014-05-02', 1, NULL, NULL, NULL),
(451, '1211002014078', '2014-05-05', 1, NULL, NULL, NULL),
(452, '1211002014080', '2014-05-07', 1, NULL, NULL, NULL),
(453, '1211002014085', '2014-05-13', 1, NULL, NULL, NULL),
(454, '1211002014090', '2014-05-20', 1, NULL, NULL, NULL),
(455, '1211002014092', '2014-05-22', 1, NULL, NULL, NULL),
(456, '1211002014104', '2014-06-06', 1, NULL, NULL, NULL),
(457, '1211002014109', '2014-06-12', 1, NULL, NULL, NULL),
(458, '1211002014114', '2014-06-18', 1, NULL, NULL, NULL),
(459, '1211002014117', '2014-06-23', 1, NULL, NULL, NULL),
(460, '1211002014118', '2014-06-24', 1, NULL, NULL, NULL),
(461, '1211002014125', '2014-07-02', 1, NULL, NULL, NULL),
(462, '1211002014133', '2014-07-11', 1, NULL, NULL, NULL),
(463, '1211002014135', '2014-07-14', 1, NULL, NULL, NULL),
(464, '1211002014136', '2014-07-15', 1, NULL, NULL, NULL),
(465, '1211002014142', '2014-07-22', 1, NULL, NULL, NULL),
(466, '1211002014143', '2014-07-23', 1, NULL, NULL, NULL),
(467, '1211002014144', '2014-07-24', 1, NULL, NULL, NULL),
(468, '1211002014145', '2014-07-25', 1, NULL, NULL, NULL),
(469, '1211002014152', '2014-08-04', 1, NULL, NULL, NULL),
(470, '1211002014154', '2014-08-06', 1, NULL, NULL, NULL),
(471, '1211002014172', '2014-08-31', 1, NULL, NULL, NULL),
(472, '1211002014182', '2014-09-14', 1, NULL, NULL, NULL),
(473, '1211002014187', '2014-09-19', 1, NULL, NULL, NULL),
(474, '1211002014197', '2014-10-09', 1, NULL, NULL, NULL),
(475, '1211002014206', '2014-10-20', 1, NULL, NULL, NULL),
(476, '1211002014250', '2014-12-16', 1, NULL, NULL, NULL),
(477, '1211002014252', '2014-12-18', 1, NULL, NULL, NULL),
(478, '1211002014253', '2014-12-19', 1, NULL, NULL, NULL),
(479, '1211002015020', '2015-01-29', 1, NULL, NULL, NULL),
(480, '1211002015028', '2015-02-09', 1, NULL, NULL, NULL),
(481, '1211002015030', '2015-02-11', 1, NULL, NULL, NULL),
(482, '1211002015035', '2015-02-18', 1, NULL, NULL, NULL),
(483, '1211002015042', '2015-02-27', 1, NULL, NULL, NULL),
(484, '1211002015048', '2015-03-09', 1, NULL, NULL, NULL),
(485, '1211002015057', '2015-03-19', 1, NULL, NULL, NULL),
(486, '1211002015073', '2015-04-08', 1, NULL, NULL, NULL),
(487, '1211002015077', '2015-04-13', 1, NULL, NULL, NULL),
(488, '1211002015080', '2015-04-19', 1, NULL, NULL, NULL),
(489, '1211002015091', '2015-06-01', 1, NULL, NULL, NULL),
(490, '1211002014016', '2014-02-11', 1, NULL, NULL, NULL),
(491, '1211002014024', '2014-02-21', 1, NULL, NULL, NULL),
(492, '1211002014033', '2014-03-06', 1, NULL, NULL, NULL),
(493, '1211002014034', '2014-03-07', 1, NULL, NULL, NULL),
(494, '1211002014053', '2014-04-01', 1, NULL, NULL, NULL),
(495, '1211002014067', '2014-04-20', 1, NULL, NULL, NULL),
(496, '1211002014084', '2014-05-12', 1, NULL, NULL, NULL),
(497, '1211002014086', '2014-05-15', 1, NULL, NULL, NULL),
(498, '1211002014093', '2014-05-23', 1, NULL, NULL, NULL),
(499, '1211002014094', '2014-05-25', 1, NULL, NULL, NULL),
(500, '1211002014095', '2014-05-26', 1, NULL, NULL, NULL),
(501, '1211002014100', '2014-06-02', 1, NULL, NULL, NULL),
(502, '1211002014103', '2014-06-05', 1, NULL, NULL, NULL),
(503, '1211002014108', '2014-06-11', 1, NULL, NULL, NULL),
(504, '1211002014111', '2014-06-15', 1, NULL, NULL, NULL),
(505, '1211002014112', '2014-06-16', 1, NULL, NULL, NULL),
(506, '1211002014120', '2014-06-26', 1, NULL, NULL, NULL),
(507, '1211002014126', '2014-07-03', 1, NULL, NULL, NULL),
(508, '1211002014128', '2014-07-06', 1, NULL, NULL, NULL),
(509, '1211002014129', '2014-07-07', 1, NULL, NULL, NULL),
(510, '1211002014138', '2014-07-17', 1, NULL, NULL, NULL),
(511, '1211002014148', '2014-07-30', 1, NULL, NULL, NULL),
(512, '1211002014150', '2014-08-01', 1, NULL, NULL, NULL),
(513, '1211002014160', '2014-08-15', 1, NULL, NULL, NULL),
(514, '1211002014161', '2014-08-18', 1, NULL, NULL, NULL),
(515, '1211002014163', '2014-08-20', 1, NULL, NULL, NULL),
(516, '1211002014169', '2014-08-27', 1, NULL, NULL, NULL),
(517, '1211002014195', '2014-09-30', 1, NULL, NULL, NULL),
(518, '1211002014207', '2014-10-21', 1, NULL, NULL, NULL),
(519, '1211002014210', '2014-10-27', 1, NULL, NULL, NULL),
(520, '1211002014225', '2014-11-14', 1, NULL, NULL, NULL),
(521, '1211002014229', '2014-11-19', 1, NULL, NULL, NULL),
(522, '1211002014232', '2014-11-23', 1, NULL, NULL, NULL),
(523, '1211002014234', '2014-11-25', 1, NULL, NULL, NULL),
(524, '1211002014236', '2014-11-30', 1, NULL, NULL, NULL),
(525, '1211002014242', '2014-12-07', 1, NULL, NULL, NULL),
(526, '1211002014246', '2014-12-11', 1, NULL, NULL, NULL),
(527, '1211002014254', '2014-12-21', 1, NULL, NULL, NULL),
(528, '1211002014256', '2014-12-23', 1, NULL, NULL, NULL),
(529, '1211002014257', '2014-12-24', 1, NULL, NULL, NULL),
(530, '1211002014258', '2014-12-26', 1, NULL, NULL, NULL),
(531, '1211002015003', '2015-01-07', 1, NULL, NULL, NULL),
(532, '1211002015005', '2015-01-09', 1, NULL, NULL, NULL),
(533, '1211002015016', '2015-01-25', 1, NULL, NULL, NULL),
(534, '1211002015026', '2015-02-06', 1, NULL, NULL, NULL),
(535, '1211002015034', '2015-02-16', 1, NULL, NULL, NULL),
(536, '1211002015036', '2015-02-20', 1, NULL, NULL, NULL),
(537, '1211002015043', '2015-03-01', 1, NULL, NULL, NULL),
(538, '1211002015053', '2015-03-15', 1, NULL, NULL, NULL),
(539, '1211002015063', '2015-03-27', 1, NULL, NULL, NULL),
(540, '1211002015065', '2015-03-30', 1, NULL, NULL, NULL),
(541, '1211002014015', '2014-02-10', 1, NULL, NULL, NULL),
(542, '1211002014022', '2014-02-18', 1, NULL, NULL, NULL),
(543, '1211002014035', '2014-03-09', 1, NULL, NULL, NULL),
(544, '1211002014037', '2014-03-11', 1, NULL, NULL, NULL),
(545, '1211002014041', '2014-03-17', 1, NULL, NULL, NULL),
(546, '1211002014055', '2014-04-03', 1, NULL, NULL, NULL),
(547, '1211002014065', '2014-04-17', 1, NULL, NULL, NULL),
(548, '1211002014072', '2014-04-27', 1, NULL, NULL, NULL),
(549, '1211002014077', '2014-05-04', 1, NULL, NULL, NULL),
(550, '1211002014082', '2014-05-09', 1, NULL, NULL, NULL),
(551, '1211002014083', '2014-05-11', 1, NULL, NULL, NULL),
(552, '1211002014087', '2014-05-16', 1, NULL, NULL, NULL),
(553, '1211002014088', '2014-05-18', 1, NULL, NULL, NULL),
(554, '1211002014099', '2014-06-01', 1, NULL, NULL, NULL),
(555, '1211002014105', '2014-06-08', 1, NULL, NULL, NULL),
(556, '1211002014110', '2014-06-13', 1, NULL, NULL, NULL),
(557, '1211002014165', '2014-08-22', 1, NULL, NULL, NULL),
(558, '1211002014185', '2014-09-17', 1, NULL, NULL, NULL),
(559, '1211002014199', '2014-10-12', 1, NULL, NULL, NULL),
(560, '1211002014201', '2014-10-14', 1, NULL, NULL, NULL),
(561, '1211002014216', '2014-11-04', 1, NULL, NULL, NULL),
(562, '1211002014218', '2014-11-06', 1, NULL, NULL, NULL),
(563, '1211002014219', '2014-11-07', 1, NULL, NULL, NULL),
(564, '1211002014224', '2014-11-13', 1, NULL, NULL, NULL),
(565, '1211002014265', '2015-01-05', 1, NULL, NULL, NULL),
(566, '1211002014269', '2015-01-09', 1, NULL, NULL, NULL),
(567, '1211002015002', '2015-01-06', 1, NULL, NULL, NULL),
(568, '1211002015021', '2015-02-01', 1, NULL, NULL, NULL),
(569, '1211002015022', '2015-02-02', 1, NULL, NULL, NULL),
(570, '1211002015039', '2015-02-24', 1, NULL, NULL, NULL),
(571, '1211002015041', '2015-02-26', 1, NULL, NULL, NULL),
(572, '1211002015049', '2015-03-10', 1, NULL, NULL, NULL),
(573, '1211002015050', '2015-03-11', 1, NULL, NULL, NULL),
(574, '1211002015052', '2015-03-13', 1, NULL, NULL, NULL),
(575, '1211002015056', '2015-03-18', 1, NULL, NULL, NULL),
(576, '1211002015059', '2015-03-23', 1, NULL, NULL, NULL),
(577, '1211002015061', '2015-03-25', 1, NULL, NULL, NULL),
(578, '1211002015067', '2015-04-01', 1, NULL, NULL, NULL),
(579, '1211002015071', '2015-04-06', 1, NULL, NULL, NULL),
(580, '1211002015075', '2015-04-10', 1, NULL, NULL, NULL),
(581, '1211002015083', '2015-04-22', 1, NULL, NULL, NULL),
(582, '1211002015085', '2015-05-24', 1, NULL, NULL, NULL),
(583, '1211002015087', '2015-05-26', 1, NULL, NULL, NULL),
(584, '1211002015089', '2015-05-28', 1, NULL, NULL, NULL),
(585, '1211002015095', '2015-06-05', 1, NULL, NULL, NULL),
(586, '1211002015097', '2015-06-08', 1, NULL, NULL, NULL),
(587, '1211002015099', '2015-06-10', 1, NULL, NULL, NULL),
(588, '1211002015100', '2015-06-11', 1, NULL, NULL, NULL),
(589, '1211002015101', '2015-06-12', 1, NULL, NULL, NULL),
(590, '1211002015102', '2015-06-14', 1, NULL, NULL, NULL),
(591, '1211002015104', '2015-06-16', 1, NULL, NULL, NULL),
(592, '1211002014025', '2014-02-23', 1, NULL, NULL, NULL),
(593, '1211002014044', '2014-03-20', 1, NULL, NULL, NULL),
(594, '1211002014046', '2014-03-23', 1, NULL, NULL, NULL),
(595, '1211002014057', '2014-04-06', 1, NULL, NULL, NULL),
(596, '1211002014068', '2014-04-21', 1, NULL, NULL, NULL),
(597, '1211002014074', '2014-04-29', 1, NULL, NULL, NULL),
(598, '1211002014089', '2014-05-19', 1, NULL, NULL, NULL),
(599, '1211002014102', '2014-06-04', 1, NULL, NULL, NULL),
(600, '1211002014121', '2014-06-27', 1, NULL, NULL, NULL),
(601, '1211002014123', '2014-06-30', 1, NULL, NULL, NULL),
(602, '1211002014149', '2014-07-31', 1, NULL, NULL, NULL),
(603, '1211002014153', '2014-08-05', 1, NULL, NULL, NULL),
(604, '1211002014156', '2014-08-08', 1, NULL, NULL, NULL),
(605, '1211002014167', '2014-08-25', 1, NULL, NULL, NULL),
(606, '1211002014176', '2014-09-05', 1, NULL, NULL, NULL),
(607, '1211002014177', '2014-09-07', 1, NULL, NULL, NULL),
(608, '1211002014183', '2014-09-15', 1, NULL, NULL, NULL),
(609, '1211002014214', '2014-11-02', 1, NULL, NULL, NULL),
(610, '1211002014220', '2014-11-09', 1, NULL, NULL, NULL),
(611, '1211002014222', '2014-11-11', 1, NULL, NULL, NULL),
(612, '1211002014227', '2014-11-17', 1, NULL, NULL, NULL),
(613, '1211002014231', '2014-11-21', 1, NULL, NULL, NULL),
(614, '1211002014233', '2014-11-24', 1, NULL, NULL, NULL),
(615, '1211002014238', '2014-12-02', 1, NULL, NULL, NULL),
(616, '1211002014244', '2014-12-09', 1, NULL, NULL, NULL),
(617, '1211002014251', '2014-12-17', 1, NULL, NULL, NULL),
(618, '1211002014262', '2015-01-01', 1, NULL, NULL, NULL),
(619, '1211002015009', '2015-01-14', 1, NULL, NULL, NULL),
(620, '1211002015010', '2015-01-16', 1, NULL, NULL, NULL),
(621, '1211002015013', '2015-01-20', 1, NULL, NULL, NULL),
(622, '1211002015017', '2015-01-26', 1, NULL, NULL, NULL),
(623, '1211002015019', '2015-01-28', 1, NULL, NULL, NULL),
(624, '1211002015032', '2015-02-13', 1, NULL, NULL, NULL),
(625, '1211002015033', '2015-02-15', 1, NULL, NULL, NULL),
(626, '1211002015038', '2015-02-23', 1, NULL, NULL, NULL),
(627, '1211002015060', '2015-03-24', 1, NULL, NULL, NULL),
(628, '1211002015062', '2015-03-26', 1, NULL, NULL, NULL),
(629, '1211002015066', '2015-03-31', 1, NULL, NULL, NULL),
(630, '1211002015068', '2015-04-02', 1, NULL, NULL, NULL),
(631, '1211002015081', '2015-04-20', 1, NULL, NULL, NULL),
(632, '1211002015093', '2015-06-03', 1, NULL, NULL, NULL),
(633, '1211002015106', '2015-06-18', 1, NULL, NULL, NULL),
(634, '1211002015109', '2015-06-22', 1, NULL, NULL, NULL),
(635, '1211002015114', '2015-06-28', 1, NULL, NULL, NULL),
(636, '1211002015116', '2015-06-30', 1, NULL, NULL, NULL),
(637, '1211002015117', '2015-07-01', 1, NULL, NULL, NULL),
(638, '1211002015118', '2015-07-02', 1, NULL, NULL, NULL),
(639, '1211002015119', '2015-07-03', 1, NULL, NULL, NULL),
(640, '1211002015120', '2015-07-05', 1, NULL, NULL, NULL),
(641, '1211002015121', '2015-07-06', 1, NULL, NULL, NULL),
(642, '1211002015122', '2015-07-07', 1, NULL, NULL, NULL),
(643, '1211002015166', '2015-09-01', 1, NULL, NULL, NULL),
(644, '1211002015168', '2015-09-03', 1, NULL, NULL, NULL),
(645, '1211002015202', '2015-10-25', 1, NULL, NULL, NULL),
(646, '1211002015206', '2015-10-30', 1, NULL, NULL, NULL),
(647, '1211002015210', '2015-11-04', 1, NULL, NULL, NULL),
(648, '1211002015214', '2015-11-09', 1, NULL, NULL, NULL),
(649, '1211002015219', '2015-11-19', 1, NULL, NULL, NULL),
(650, '1211002015221', '2015-11-22', 1, NULL, NULL, NULL),
(651, '1211002015224', '2015-11-25', 1, NULL, NULL, NULL),
(652, '1211002015236', '2015-12-09', 1, NULL, NULL, NULL),
(653, '1211002015238', '2015-12-11', 1, NULL, NULL, NULL),
(654, '1211002015239', '2015-12-13', 1, NULL, NULL, NULL),
(655, '1211002015247', '2015-12-22', 1, NULL, NULL, NULL),
(656, '1211002015249', '2015-12-24', 1, NULL, NULL, NULL),
(657, '1211002016016', '2016-01-20', 1, NULL, NULL, NULL),
(658, '1211002016020', '2016-01-25', 1, NULL, NULL, NULL),
(659, '1211002016021', '2016-01-26', 1, NULL, NULL, NULL),
(660, '1211002016023', '2016-01-28', 1, NULL, NULL, NULL),
(661, '1211002016026', '2016-02-01', 1, NULL, NULL, NULL),
(662, '1211002016028', '2016-02-03', 1, NULL, NULL, NULL),
(663, '1211002016032', '2016-02-08', 1, NULL, NULL, NULL),
(664, '1211002016033', '2016-02-11', 1, NULL, NULL, NULL),
(665, '1211002016034', '2016-02-12', 1, NULL, NULL, NULL),
(666, '1211002016035', '2016-02-14', 1, NULL, NULL, NULL),
(667, '1211002016039', '2016-02-18', 1, NULL, NULL, NULL),
(668, '1211002016068', '2016-03-29', 1, NULL, NULL, NULL),
(669, '1211002016070', '2016-03-31', 1, NULL, NULL, NULL),
(670, '1211002016093', '2016-05-02', 1, NULL, NULL, NULL),
(671, '1211002016106', '2016-05-17', 1, NULL, NULL, NULL),
(672, '1211002016107', '2016-05-18', 1, NULL, NULL, NULL),
(673, '1211002016108', '2016-05-19', 1, NULL, NULL, NULL),
(674, '1211002016109', '2016-05-20', 1, NULL, NULL, NULL),
(675, '1211002016121', '2016-06-03', 1, NULL, NULL, NULL),
(676, '1211002016129', '2016-06-13', 1, NULL, NULL, NULL),
(677, '1211002016133', '2016-06-17', 1, NULL, NULL, NULL),
(678, '1211002016136', '2016-06-21', 1, NULL, NULL, NULL),
(679, '1211002016138', '2016-06-23', 1, NULL, NULL, NULL),
(680, '1211002016143', '2016-06-29', 1, NULL, NULL, NULL),
(681, '1211002016150', '2016-07-08', 1, NULL, NULL, NULL),
(682, '1211002016177', '2016-08-10', 1, NULL, NULL, NULL),
(683, '1211002016186', '2016-08-23', 1, NULL, NULL, NULL),
(684, '1211002016187', '2016-08-24', 1, NULL, NULL, NULL),
(685, '1211002016188', '2016-08-26', 1, NULL, NULL, NULL),
(686, '1211002016196', '2016-09-05', 1, NULL, NULL, NULL),
(687, '1211002016227', '2016-10-20', 1, NULL, NULL, NULL),
(688, '1211002016230', '2016-10-24', 1, NULL, NULL, NULL),
(689, '1211002016236', '2016-11-04', 1, NULL, NULL, NULL),
(690, '1211002016237', '2016-11-07', 1, NULL, NULL, NULL),
(691, '1211002016238', '2016-11-08', 1, NULL, NULL, NULL),
(692, '1211002016240', '2016-11-10', 1, NULL, NULL, NULL),
(693, '1211002016241', '2016-11-11', 1, NULL, NULL, NULL),
(694, '1211002015153', '2015-08-16', 1, NULL, NULL, NULL),
(695, '1211002015164', '2015-08-28', 1, NULL, NULL, NULL),
(696, '1211002015170', '2015-09-06', 1, NULL, NULL, NULL),
(697, '1211002015172', '2015-09-08', 1, NULL, NULL, NULL),
(698, '1211002015178', '2015-09-15', 1, NULL, NULL, NULL),
(699, '1211002015180', '2015-09-17', 1, NULL, NULL, NULL),
(700, '1211002015181', '2015-09-18', 1, NULL, NULL, NULL),
(701, '1211002015182', '2015-09-22', 1, NULL, NULL, NULL),
(702, '1211002015185', '2015-09-28', 1, NULL, NULL, NULL),
(703, '1211002015187', '2015-09-30', 1, NULL, NULL, NULL),
(704, '1211002015197', '2015-10-14', 1, NULL, NULL, NULL),
(705, '1211002015198', '2015-10-15', 1, NULL, NULL, NULL),
(706, '1211002015201', '2015-10-19', 1, NULL, NULL, NULL),
(707, '1211002015203', '2015-10-27', 1, NULL, NULL, NULL),
(708, '1211002015205', '2015-10-29', 1, NULL, NULL, NULL),
(709, '1211002015211', '2015-11-05', 1, NULL, NULL, NULL),
(710, '1211002016003', '2016-01-04', 1, NULL, NULL, NULL),
(711, '1211002016005', '2016-01-06', 1, NULL, NULL, NULL),
(712, '1211002016012', '2016-01-14', 1, NULL, NULL, NULL),
(713, '1211002016013', '2016-01-17', 1, NULL, NULL, NULL),
(714, '1211002016018', '2016-01-22', 1, NULL, NULL, NULL),
(715, '1211002016045', '2016-02-26', 1, NULL, NULL, NULL),
(716, '1211002016047', '2016-02-29', 1, NULL, NULL, NULL),
(717, '1211002016064', '2016-03-24', 1, NULL, NULL, NULL),
(718, '1211002016066', '2016-03-27', 1, NULL, NULL, NULL),
(719, '1211002016084', '2016-04-20', 1, NULL, NULL, NULL),
(720, '1211002016100', '2016-05-10', 1, NULL, NULL, NULL),
(721, '1211002016102', '2016-05-12', 1, NULL, NULL, NULL),
(722, '1211002016104', '2016-05-15', 1, NULL, NULL, NULL),
(723, '1211002016122', '2016-06-05', 1, NULL, NULL, NULL),
(724, '1211002016123', '2016-06-06', 1, NULL, NULL, NULL),
(725, '1211002016137', '2016-06-22', 1, NULL, NULL, NULL),
(726, '1211002016140', '2016-06-26', 1, NULL, NULL, NULL),
(727, '1211002016145', '2016-07-01', 1, NULL, NULL, NULL),
(728, '1211002016153', '2016-07-12', 1, NULL, NULL, NULL),
(729, '1211002016154', '2016-07-13', 1, NULL, NULL, NULL),
(730, '1211002016155', '2016-07-14', 1, NULL, NULL, NULL),
(731, '1211002016158', '2016-07-18', 1, NULL, NULL, NULL),
(732, '1211002016169', '2016-08-01', 1, NULL, NULL, NULL),
(733, '1211002016195', '2016-09-04', 1, NULL, NULL, NULL),
(734, '1211002016201', '2016-09-11', 1, NULL, NULL, NULL),
(735, '1211002016202', '2016-09-12', 1, NULL, NULL, NULL),
(736, '1211002016203', '2016-09-14', 1, NULL, NULL, NULL),
(737, '1211002016218', '2016-10-04', 1, NULL, NULL, NULL),
(738, '1211002016220', '2016-10-06', 1, NULL, NULL, NULL),
(739, '1211002016225', '2016-10-18', 1, NULL, NULL, NULL),
(740, '1211002016235', '2016-11-03', 1, NULL, NULL, NULL),
(741, '1211002016242', '2016-11-13', 1, NULL, NULL, NULL),
(742, '1211002016243', '2016-11-14', 1, NULL, NULL, NULL),
(743, '1211002016244', '2016-11-15', 1, NULL, NULL, NULL),
(744, '1211002016245', '2016-11-16', 1, NULL, NULL, NULL),
(745, '1211002015165', '2015-08-31', 1, NULL, NULL, NULL),
(746, '1211002015171', '2015-09-07', 1, NULL, NULL, NULL),
(747, '1211002015173', '2015-09-09', 1, NULL, NULL, NULL),
(748, '1211002015174', '2015-09-10', 1, NULL, NULL, NULL),
(749, '1211002015177', '2015-09-14', 1, NULL, NULL, NULL),
(750, '1211002015184', '2015-09-24', 1, NULL, NULL, NULL),
(751, '1211002015193', '2015-10-07', 1, NULL, NULL, NULL),
(752, '1211002015208', '2015-11-02', 1, NULL, NULL, NULL),
(753, '1211002015222', '2015-11-23', 1, NULL, NULL, NULL),
(754, '1211002015244', '2015-12-18', 1, NULL, NULL, NULL),
(755, '1211002015245', '2015-12-20', 1, NULL, NULL, NULL),
(756, '1211002015248', '2015-12-23', 1, NULL, NULL, NULL),
(757, '1211002016001', '2016-01-01', 1, NULL, NULL, NULL),
(758, '1211002016006', '2016-01-07', 1, NULL, NULL, NULL),
(759, '1211002016010', '2016-01-12', 1, NULL, NULL, NULL),
(760, '1211002016015', '2016-01-19', 1, NULL, NULL, NULL),
(761, '1211002016017', '2016-01-21', 1, NULL, NULL, NULL),
(762, '1211002016041', '2016-02-22', 1, NULL, NULL, NULL),
(763, '1211002016043', '2016-02-24', 1, NULL, NULL, NULL),
(764, '1211002016060', '2016-03-18', 1, NULL, NULL, NULL),
(765, '1211002016067', '2016-03-28', 1, NULL, NULL, NULL),
(766, '1211002016074', '2016-04-05', 1, NULL, NULL, NULL),
(767, '1211002016079', '2016-04-12', 1, NULL, NULL, NULL),
(768, '1211002016080', '2016-04-14', 1, NULL, NULL, NULL),
(769, '1211002016091', '2016-04-28', 1, NULL, NULL, NULL),
(770, '1211002016119', '2016-06-01', 1, NULL, NULL, NULL),
(771, '1211002016128', '2016-06-12', 1, NULL, NULL, NULL),
(772, '1211002016131', '2016-06-15', 1, NULL, NULL, NULL),
(773, '1211002016141', '2016-06-27', 1, NULL, NULL, NULL),
(774, '1211002016147', '2016-07-04', 1, NULL, NULL, NULL),
(775, '1211002016152', '2016-07-11', 1, NULL, NULL, NULL),
(776, '1211002016165', '2016-07-26', 1, NULL, NULL, NULL),
(777, '1211002016168', '2016-07-31', 1, NULL, NULL, NULL),
(778, '1211002016174', '2016-08-07', 1, NULL, NULL, NULL),
(779, '1211002016183', '2016-08-17', 1, NULL, NULL, NULL),
(780, '1211002016200', '2016-09-09', 1, NULL, NULL, NULL),
(781, '1211002016211', '2016-09-26', 1, NULL, NULL, NULL),
(782, '1211002016215', '2016-09-30', 1, NULL, NULL, NULL),
(783, '1211002016231', '2016-10-25', 1, NULL, NULL, NULL),
(784, '1211002016233', '2016-10-27', 1, NULL, NULL, NULL),
(785, '1211002016239', '2016-11-09', 1, NULL, NULL, NULL),
(786, '1211002016246', '2016-11-17', 1, NULL, NULL, NULL),
(787, '1211002016247', '2016-11-18', 1, NULL, NULL, NULL),
(788, '1211002016248', '2016-11-20', 1, NULL, NULL, NULL),
(789, '1211002016249', '2016-11-21', 1, NULL, NULL, NULL),
(790, '1211002016250', '2016-11-22', 1, NULL, NULL, NULL),
(791, '1211002016251', '2016-11-23', 1, NULL, NULL, NULL),
(792, '1211002016252', '2016-11-24', 1, NULL, NULL, NULL),
(793, '1211002016253', '2016-11-25', 1, NULL, NULL, NULL),
(794, '1211002016254', '2016-11-27', 1, NULL, NULL, NULL),
(795, '1211002016255', '2016-11-28', 1, NULL, NULL, NULL),
(796, '1211002015155', '2015-08-18', 1, NULL, NULL, NULL),
(797, '1211002015156', '2015-08-19', 1, NULL, NULL, NULL),
(798, '1211002015157', '2015-08-20', 1, NULL, NULL, NULL),
(799, '1211002015158', '2015-08-21', 1, NULL, NULL, NULL),
(800, '1211002015159', '2015-08-23', 1, NULL, NULL, NULL),
(801, '1211002015160', '2015-08-24', 1, NULL, NULL, NULL),
(802, '1211002015161', '2015-08-25', 1, NULL, NULL, NULL),
(803, '1211002015169', '2015-09-04', 1, NULL, NULL, NULL),
(804, '1211002015186', '2015-09-29', 1, NULL, NULL, NULL),
(805, '1211002015188', '2015-10-01', 1, NULL, NULL, NULL),
(806, '1211002015189', '2015-10-02', 1, NULL, NULL, NULL),
(807, '1211002015190', '2015-10-04', 1, NULL, NULL, NULL),
(808, '1211002015192', '2015-10-06', 1, NULL, NULL, NULL),
(809, '1211002015195', '2015-10-09', 1, NULL, NULL, NULL),
(810, '1211002015212', '2015-11-06', 1, NULL, NULL, NULL),
(811, '1211002015213', '2015-11-08', 1, NULL, NULL, NULL),
(812, '1211002015227', '2015-11-29', 1, NULL, NULL, NULL),
(813, '1211002015232', '2015-12-04', 1, NULL, NULL, NULL),
(814, '1211002015234', '2015-12-07', 1, NULL, NULL, NULL),
(815, '1211002016022', '2016-01-27', 1, NULL, NULL, NULL),
(816, '1211002016027', '2016-02-02', 1, NULL, NULL, NULL),
(817, '1211002016029', '2016-02-04', 1, NULL, NULL, NULL),
(818, '1211002016030', '2016-02-05', 1, NULL, NULL, NULL),
(819, '1211002016037', '2016-02-16', 1, NULL, NULL, NULL),
(820, '1211002016044', '2016-02-25', 1, NULL, NULL, NULL),
(821, '1211002016046', '2016-02-28', 1, NULL, NULL, NULL),
(822, '1211002016049', '2016-03-02', 1, NULL, NULL, NULL),
(823, '1211002016056', '2016-03-14', 1, NULL, NULL, NULL),
(824, '1211002016058', '2016-03-16', 1, NULL, NULL, NULL),
(825, '1211002016069', '2016-03-30', 1, NULL, NULL, NULL),
(826, '1211002016073', '2016-04-04', 1, NULL, NULL, NULL),
(827, '1211002016075', '2016-04-06', 1, NULL, NULL, NULL),
(828, '1211002016077', '2016-04-10', 1, NULL, NULL, NULL),
(829, '1211002016083', '2016-04-19', 1, NULL, NULL, NULL),
(830, '1211002016088', '2016-04-25', 1, NULL, NULL, NULL),
(831, '1211002016090', '2016-04-27', 1, NULL, NULL, NULL),
(832, '1211002016092', '2016-04-29', 1, NULL, NULL, NULL),
(833, '1211002016094', '2016-05-03', 1, NULL, NULL, NULL),
(834, '1211002016097', '2016-05-06', 1, NULL, NULL, NULL),
(835, '1211002016099', '2016-05-09', 1, NULL, NULL, NULL),
(836, '1211002016101', '2016-05-11', 1, NULL, NULL, NULL),
(837, '1211002016105', '2016-05-16', 1, NULL, NULL, NULL),
(838, '1211002016127', '2016-06-10', 1, NULL, NULL, NULL),
(839, '1211002016135', '2016-06-20', 1, NULL, NULL, NULL),
(840, '1211002016146', '2016-07-03', 1, NULL, NULL, NULL),
(841, '1211002016148', '2016-07-05', 1, NULL, NULL, NULL),
(842, '1211002016156', '2016-07-15', 1, NULL, NULL, NULL),
(843, '1211002016157', '2016-07-17', 1, NULL, NULL, NULL),
(844, '1211002016160', '2016-07-20', 1, NULL, NULL, NULL),
(845, '1211002016164', '2016-07-25', 1, NULL, NULL, NULL),
(846, '1211002016166', '2016-07-27', 1, NULL, NULL, NULL),
(847, '1211002015162', '2015-08-26', 1, NULL, NULL, NULL),
(848, '1211002015167', '2015-09-02', 1, NULL, NULL, NULL),
(849, '1211002015176', '2015-09-13', 1, NULL, NULL, NULL),
(850, '1211002015183', '2015-09-23', 1, NULL, NULL, NULL),
(851, '1211002015191', '2015-10-05', 1, NULL, NULL, NULL),
(852, '1211002015200', '2015-10-18', 1, NULL, NULL, NULL),
(853, '1211002015217', '2015-11-16', 1, NULL, NULL, NULL),
(854, '1211002015218', '2015-11-18', 1, NULL, NULL, NULL),
(855, '1211002015220', '2015-11-20', 1, NULL, NULL, NULL),
(856, '1211002015223', '2015-11-24', 1, NULL, NULL, NULL),
(857, '1211002015225', '2015-11-26', 1, NULL, NULL, NULL),
(858, '1211002015226', '2015-11-27', 1, NULL, NULL, NULL),
(859, '1211002015230', '2015-12-02', 1, NULL, NULL, NULL),
(860, '1211002015240', '2015-12-14', 1, NULL, NULL, NULL),
(861, '1211002015242', '2015-12-16', 1, NULL, NULL, NULL),
(862, '1211002015250', '2015-12-27', 1, NULL, NULL, NULL),
(863, '1211002015251', '2015-12-28', 1, NULL, NULL, NULL),
(864, '1211002015252', '2015-12-29', 1, NULL, NULL, NULL),
(865, '1211002015254', '2016-01-01', 1, NULL, NULL, NULL),
(866, '1211002016008', '2016-01-10', 1, NULL, NULL, NULL),
(867, '1211002016014', '2016-01-18', 1, NULL, NULL, NULL),
(868, '1211002016024', '2016-01-29', 1, NULL, NULL, NULL),
(869, '1211002016025', '2016-01-31', 1, NULL, NULL, NULL),
(870, '1211002016031', '2016-02-07', 1, NULL, NULL, NULL),
(871, '1211002016051', '2016-03-04', 1, NULL, NULL, NULL),
(872, '1211002016057', '2016-03-15', 1, NULL, NULL, NULL),
(873, '1211002016059', '2016-03-17', 1, NULL, NULL, NULL),
(874, '1211002016062', '2016-03-21', 1, NULL, NULL, NULL),
(875, '1211002016071', '2016-04-01', 1, NULL, NULL, NULL),
(876, '1211002016078', '2016-04-11', 1, NULL, NULL, NULL);
INSERT INTO `settlement` (`id`, `settlement_id`, `trade_date`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(877, '1211002016082', '2016-04-18', 1, NULL, NULL, NULL),
(878, '1211002016087', '2016-04-24', 1, NULL, NULL, NULL),
(879, '1211002016096', '2016-05-05', 1, NULL, NULL, NULL),
(880, '1211002016098', '2016-05-08', 1, NULL, NULL, NULL),
(881, '1211002016117', '2016-05-30', 1, NULL, NULL, NULL),
(882, '1211002016120', '2016-06-02', 1, NULL, NULL, NULL),
(883, '1211002016124', '2016-06-07', 1, NULL, NULL, NULL),
(884, '1211002016125', '2016-06-08', 1, NULL, NULL, NULL),
(885, '1211002016126', '2016-06-09', 1, NULL, NULL, NULL),
(886, '1211002016159', '2016-07-19', 1, NULL, NULL, NULL),
(887, '1211002016161', '2016-07-21', 1, NULL, NULL, NULL),
(888, '1211002016167', '2016-07-28', 1, NULL, NULL, NULL),
(889, '1211002016172', '2016-08-04', 1, NULL, NULL, NULL),
(890, '1211002016175', '2016-08-08', 1, NULL, NULL, NULL),
(891, '1211002016176', '2016-08-09', 1, NULL, NULL, NULL),
(892, '1211002016178', '2016-08-11', 1, NULL, NULL, NULL),
(893, '1211002016179', '2016-08-12', 1, NULL, NULL, NULL),
(894, '1211002016184', '2016-08-21', 1, NULL, NULL, NULL),
(895, '1211002016190', '2016-08-29', 1, NULL, NULL, NULL),
(896, '1211002016192', '2016-08-31', 1, NULL, NULL, NULL),
(897, '1211002016194', '2016-09-02', 1, NULL, NULL, NULL),
(898, '1211002015175', '2015-09-11', 1, NULL, NULL, NULL),
(899, '1211002015179', '2015-09-16', 1, NULL, NULL, NULL),
(900, '1211002015196', '2015-10-11', 1, NULL, NULL, NULL),
(901, '1211002015199', '2015-10-16', 1, NULL, NULL, NULL),
(902, '1211002015204', '2015-10-28', 1, NULL, NULL, NULL),
(903, '1211002015207', '2015-11-01', 1, NULL, NULL, NULL),
(904, '1211002015209', '2015-11-03', 1, NULL, NULL, NULL),
(905, '1211002015215', '2015-11-10', 1, NULL, NULL, NULL),
(906, '1211002015229', '2015-12-01', 1, NULL, NULL, NULL),
(907, '1211002015231', '2015-12-03', 1, NULL, NULL, NULL),
(908, '1211002015241', '2015-12-15', 1, NULL, NULL, NULL),
(909, '1211002015243', '2015-12-17', 1, NULL, NULL, NULL),
(910, '1211002016004', '2016-01-05', 1, NULL, NULL, NULL),
(911, '1211002016007', '2016-01-08', 1, NULL, NULL, NULL),
(912, '1211002016009', '2016-01-11', 1, NULL, NULL, NULL),
(913, '1211002016038', '2016-02-17', 1, NULL, NULL, NULL),
(914, '1211002016040', '2016-02-21', 1, NULL, NULL, NULL),
(915, '1211002016042', '2016-02-23', 1, NULL, NULL, NULL),
(916, '1211002016052', '2016-03-06', 1, NULL, NULL, NULL),
(917, '1211002016054', '2016-03-11', 1, NULL, NULL, NULL),
(918, '1211002016072', '2016-04-03', 1, NULL, NULL, NULL),
(919, '1211002016076', '2016-04-08', 1, NULL, NULL, NULL),
(920, '1211002016089', '2016-04-26', 1, NULL, NULL, NULL),
(921, '1211002016103', '2016-05-13', 1, NULL, NULL, NULL),
(922, '1211002016112', '2016-05-24', 1, NULL, NULL, NULL),
(923, '1211002016114', '2016-05-26', 1, NULL, NULL, NULL),
(924, '1211002016115', '2016-05-27', 1, NULL, NULL, NULL),
(925, '1211002016130', '2016-06-14', 1, NULL, NULL, NULL),
(926, '1211002016132', '2016-06-16', 1, NULL, NULL, NULL),
(927, '1211002016134', '2016-06-19', 1, NULL, NULL, NULL),
(928, '1211002016139', '2016-06-24', 1, NULL, NULL, NULL),
(929, '1211002016142', '2016-06-28', 1, NULL, NULL, NULL),
(930, '1211002016144', '2016-06-30', 1, NULL, NULL, NULL),
(931, '1211002016149', '2016-07-06', 1, NULL, NULL, NULL),
(932, '1211002016162', '2016-07-22', 1, NULL, NULL, NULL),
(933, '1211002016180', '2016-08-14', 1, NULL, NULL, NULL),
(934, '1211002016185', '2016-08-22', 1, NULL, NULL, NULL),
(935, '1211002016189', '2016-08-28', 1, NULL, NULL, NULL),
(936, '1211002016193', '2016-09-01', 1, NULL, NULL, NULL),
(937, '1211002016197', '2016-09-06', 1, NULL, NULL, NULL),
(938, '1211002016198', '2016-09-07', 1, NULL, NULL, NULL),
(939, '1211002016199', '2016-09-08', 1, NULL, NULL, NULL),
(940, '1211002016204', '2016-09-16', 1, NULL, NULL, NULL),
(941, '1211002016205', '2016-09-18', 1, NULL, NULL, NULL),
(942, '1211002016206', '2016-09-20', 1, NULL, NULL, NULL),
(943, '1211002016207', '2016-09-21', 1, NULL, NULL, NULL),
(944, '1211002016209', '2016-09-23', 1, NULL, NULL, NULL),
(945, '1211002016210', '2016-09-25', 1, NULL, NULL, NULL),
(946, '1211002016212', '2016-09-27', 1, NULL, NULL, NULL),
(947, '1211002016214', '2016-09-29', 1, NULL, NULL, NULL),
(948, '1211002016217', '2016-10-03', 1, NULL, NULL, NULL),
(949, '1211002015154', '2015-08-17', 1, NULL, NULL, NULL),
(950, '1211002015163', '2015-08-27', 1, NULL, NULL, NULL),
(951, '1211002015194', '2015-10-08', 1, NULL, NULL, NULL),
(952, '1211002015216', '2015-11-15', 1, NULL, NULL, NULL),
(953, '1211002015228', '2015-11-30', 1, NULL, NULL, NULL),
(954, '1211002015233', '2015-12-06', 1, NULL, NULL, NULL),
(955, '1211002015235', '2015-12-08', 1, NULL, NULL, NULL),
(956, '1211002015237', '2015-12-10', 1, NULL, NULL, NULL),
(957, '1211002015246', '2015-12-21', 1, NULL, NULL, NULL),
(958, '1211002015253', '2015-12-31', 1, NULL, NULL, NULL),
(959, '1211002016002', '2016-01-03', 1, NULL, NULL, NULL),
(960, '1211002016011', '2016-01-13', 1, NULL, NULL, NULL),
(961, '1211002016019', '2016-01-24', 1, NULL, NULL, NULL),
(962, '1211002016036', '2016-02-15', 1, NULL, NULL, NULL),
(963, '1211002016048', '2016-03-01', 1, NULL, NULL, NULL),
(964, '1211002016050', '2016-03-03', 1, NULL, NULL, NULL),
(965, '1211002016053', '2016-03-10', 1, NULL, NULL, NULL),
(966, '1211002016055', '2016-03-13', 1, NULL, NULL, NULL),
(967, '1211002016061', '2016-03-20', 1, NULL, NULL, NULL),
(968, '1211002016063', '2016-03-23', 1, NULL, NULL, NULL),
(969, '1211002016065', '2016-03-25', 1, NULL, NULL, NULL),
(970, '1211002016081', '2016-04-17', 1, NULL, NULL, NULL),
(971, '1211002016085', '2016-04-21', 1, NULL, NULL, NULL),
(972, '1211002016086', '2016-04-22', 1, NULL, NULL, NULL),
(973, '1211002016095', '2016-05-04', 1, NULL, NULL, NULL),
(974, '1211002016110', '2016-05-22', 1, NULL, NULL, NULL),
(975, '1211002016111', '2016-05-23', 1, NULL, NULL, NULL),
(976, '1211002016113', '2016-05-25', 1, NULL, NULL, NULL),
(977, '1211002016116', '2016-05-29', 1, NULL, NULL, NULL),
(978, '1211002016118', '2016-05-31', 1, NULL, NULL, NULL),
(979, '1211002016151', '2016-07-10', 1, NULL, NULL, NULL),
(980, '1211002016163', '2016-07-24', 1, NULL, NULL, NULL),
(981, '1211002016170', '2016-08-02', 1, NULL, NULL, NULL),
(982, '1211002016171', '2016-08-03', 1, NULL, NULL, NULL),
(983, '1211002016173', '2016-08-05', 1, NULL, NULL, NULL),
(984, '1211002016181', '2016-08-15', 1, NULL, NULL, NULL),
(985, '1211002016182', '2016-08-16', 1, NULL, NULL, NULL),
(986, '1211002016191', '2016-08-30', 1, NULL, NULL, NULL),
(987, '1211002016208', '2016-09-22', 1, NULL, NULL, NULL),
(988, '1211002016213', '2016-09-28', 1, NULL, NULL, NULL),
(989, '1211002016216', '2016-10-02', 1, NULL, NULL, NULL),
(990, '1211002016219', '2016-10-05', 1, NULL, NULL, NULL),
(991, '1211002016221', '2016-10-07', 1, NULL, NULL, NULL),
(992, '1211002016222', '2016-10-14', 1, NULL, NULL, NULL),
(993, '1211002016223', '2016-10-16', 1, NULL, NULL, NULL),
(994, '1211002016224', '2016-10-17', 1, NULL, NULL, NULL),
(995, '1211002016226', '2016-10-19', 1, NULL, NULL, NULL),
(996, '1211002016228', '2016-10-21', 1, NULL, NULL, NULL),
(997, '1211002016229', '2016-10-23', 1, NULL, NULL, NULL),
(998, '1211002016232', '2016-10-26', 1, NULL, NULL, NULL),
(999, '1211002016234', '2016-10-28', 1, NULL, NULL, NULL),
(1000, '1211002016269', '2016-12-15', 1, NULL, NULL, NULL),
(1001, '1211002017003', '2017-01-03', 1, NULL, NULL, NULL),
(1002, '1211002017005', '2017-01-05', 1, NULL, NULL, NULL),
(1003, '1211002017031', '2017-02-06', 1, NULL, NULL, NULL),
(1004, '1211002017034', '2017-02-09', 1, NULL, NULL, NULL),
(1005, '1211002017044', '2017-02-21', 1, NULL, NULL, NULL),
(1006, '1211002017046', '2017-02-23', 1, NULL, NULL, NULL),
(1007, '1211002017094', '2017-04-30', 1, NULL, NULL, NULL),
(1008, '1211002017098', '2017-05-05', 1, NULL, NULL, NULL),
(1009, '1211002017100', '2017-05-08', 1, NULL, NULL, NULL),
(1010, '1211002017101', '2017-05-09', 1, NULL, NULL, NULL),
(1011, '1211002017111', '2017-05-23', 1, NULL, NULL, NULL),
(1012, '1211002017113', '2017-05-26', 1, NULL, NULL, NULL),
(1013, '1211002017125', '2017-06-11', 1, NULL, NULL, NULL),
(1014, '1211002017126', '2017-06-12', 1, NULL, NULL, NULL),
(1015, '1211002017130', '2017-06-16', 1, NULL, NULL, NULL),
(1016, '1211002017131', '2017-06-18', 1, NULL, NULL, NULL),
(1017, '1211002017136', '2017-06-23', 1, NULL, NULL, NULL),
(1018, '1211002017137', '2017-06-25', 1, NULL, NULL, NULL),
(1019, '1211002017141', '2017-06-30', 1, NULL, NULL, NULL),
(1020, '1211002017142', '2017-07-02', 1, NULL, NULL, NULL),
(1021, '1211002017146', '2017-07-06', 1, NULL, NULL, NULL),
(1022, '1211002017148', '2017-07-09', 1, NULL, NULL, NULL),
(1023, '1211002017152', '2017-07-13', 1, NULL, NULL, NULL),
(1024, '1211002017154', '2017-07-16', 1, NULL, NULL, NULL),
(1025, '1211002017199', '2017-09-13', 1, NULL, NULL, NULL),
(1026, '1211002017219', '2017-10-16', 1, NULL, NULL, NULL),
(1027, '1211002017245', '2017-11-20', 1, NULL, NULL, NULL),
(1028, '1211002017256', '2017-12-05', 1, NULL, NULL, NULL),
(1029, '1211002017261', '2017-12-12', 1, NULL, NULL, NULL),
(1030, '1211002017264', '2017-12-15', 1, NULL, NULL, NULL),
(1031, '1211002017265', '2017-12-17', 1, NULL, NULL, NULL),
(1032, '1211002017267', '2017-12-19', 1, NULL, NULL, NULL),
(1033, '1211002017269', '2017-12-21', 1, NULL, NULL, NULL),
(1034, '1211002017270', '2017-12-22', 1, NULL, NULL, NULL),
(1035, '1211002018015', '2018-01-21', 1, NULL, NULL, NULL),
(1036, '1211002018035', '2018-02-15', 1, NULL, NULL, NULL),
(1037, '1211002018042', '2018-02-26', 1, NULL, NULL, NULL),
(1038, '1211002018045', '2018-03-02', 1, NULL, NULL, NULL),
(1039, '1211002018052', '2018-03-12', 1, NULL, NULL, NULL),
(1040, '1211002018081', '2018-04-16', 1, NULL, NULL, NULL),
(1041, '1211002018083', '2018-04-18', 1, NULL, NULL, NULL),
(1042, '1211002018085', '2018-04-20', 1, NULL, NULL, NULL),
(1043, '1211002018086', '2018-04-22', 1, NULL, NULL, NULL),
(1044, '1211002018088', '2018-04-24', 1, NULL, NULL, NULL),
(1045, '1211002018090', '2018-04-26', 1, NULL, NULL, NULL),
(1046, '1211002018094', '2018-05-03', 1, NULL, NULL, NULL),
(1047, '1211002018095', '2018-05-04', 1, NULL, NULL, NULL),
(1048, '1211002018096', '2018-05-06', 1, NULL, NULL, NULL),
(1049, '1211002018098', '2018-05-08', 1, NULL, NULL, NULL),
(1050, '1211002018099', '2018-05-09', 1, NULL, NULL, NULL),
(1051, '1211002016256', '2016-11-29', 1, NULL, NULL, NULL),
(1052, '1211002016258', '2016-12-01', 1, NULL, NULL, NULL),
(1053, '1211002016259', '2016-12-02', 1, NULL, NULL, NULL),
(1054, '1211002016268', '2016-12-14', 1, NULL, NULL, NULL),
(1055, '1211002016271', '2016-12-18', 1, NULL, NULL, NULL),
(1056, '1211002016273', '2016-12-20', 1, NULL, NULL, NULL),
(1057, '1211002016275', '2016-12-22', 1, NULL, NULL, NULL),
(1058, '1211002016278', '2016-12-27', 1, NULL, NULL, NULL),
(1059, '1211002016280', '2016-12-29', 1, NULL, NULL, NULL),
(1060, '1211002017007', '2017-01-08', 1, NULL, NULL, NULL),
(1061, '1211002017025', '2017-01-30', 1, NULL, NULL, NULL),
(1062, '1211002017038', '2017-02-14', 1, NULL, NULL, NULL),
(1063, '1211002017043', '2017-02-20', 1, NULL, NULL, NULL),
(1064, '1211002017049', '2017-03-01', 1, NULL, NULL, NULL),
(1065, '1211002017071', '2017-03-30', 1, NULL, NULL, NULL),
(1066, '1211002017078', '2017-04-09', 1, NULL, NULL, NULL),
(1067, '1211002017080', '2017-04-11', 1, NULL, NULL, NULL),
(1068, '1211002017081', '2017-04-12', 1, NULL, NULL, NULL),
(1069, '1211002017089', '2017-04-24', 1, NULL, NULL, NULL),
(1070, '1211002017103', '2017-05-12', 1, NULL, NULL, NULL),
(1071, '1211002017109', '2017-05-21', 1, NULL, NULL, NULL),
(1072, '1211002017114', '2017-05-28', 1, NULL, NULL, NULL),
(1073, '1211002017127', '2017-06-13', 1, NULL, NULL, NULL),
(1074, '1211002017129', '2017-06-15', 1, NULL, NULL, NULL),
(1075, '1211002017138', '2017-06-27', 1, NULL, NULL, NULL),
(1076, '1211002017144', '2017-07-04', 1, NULL, NULL, NULL),
(1077, '1211002017155', '2017-07-17', 1, NULL, NULL, NULL),
(1078, '1211002017159', '2017-07-21', 1, NULL, NULL, NULL),
(1079, '1211002017162', '2017-07-25', 1, NULL, NULL, NULL),
(1080, '1211002017166', '2017-07-31', 1, NULL, NULL, NULL),
(1081, '1211002017167', '2017-08-01', 1, NULL, NULL, NULL),
(1082, '1211002017168', '2017-08-02', 1, NULL, NULL, NULL),
(1083, '1211002017170', '2017-08-04', 1, NULL, NULL, NULL),
(1084, '1211002017198', '2017-09-12', 1, NULL, NULL, NULL),
(1085, '1211002017208', '2017-09-26', 1, NULL, NULL, NULL),
(1086, '1211002017218', '2017-10-15', 1, NULL, NULL, NULL),
(1087, '1211002017220', '2017-10-17', 1, NULL, NULL, NULL),
(1088, '1211002017225', '2017-10-25', 1, NULL, NULL, NULL),
(1089, '1211002017246', '2017-11-21', 1, NULL, NULL, NULL),
(1090, '1211002017249', '2017-11-24', 1, NULL, NULL, NULL),
(1091, '1211002017252', '2017-11-28', 1, NULL, NULL, NULL),
(1092, '1211002018003', '2018-01-03', 1, NULL, NULL, NULL),
(1093, '1211002018005', '2018-01-05', 1, NULL, NULL, NULL),
(1094, '1211002018009', '2018-01-10', 1, NULL, NULL, NULL),
(1095, '1211002018011', '2018-01-14', 1, NULL, NULL, NULL),
(1096, '1211002018013', '2018-01-17', 1, NULL, NULL, NULL),
(1097, '1211002018029', '2018-02-07', 1, NULL, NULL, NULL),
(1098, '1211002018041', '2018-02-25', 1, NULL, NULL, NULL),
(1099, '1211002018058', '2018-03-19', 1, NULL, NULL, NULL),
(1100, '1211002018062', '2018-03-23', 1, NULL, NULL, NULL),
(1101, '1211002018100', '2018-05-10', 1, NULL, NULL, NULL),
(1102, '1211002016261', '2016-12-05', 1, NULL, NULL, NULL),
(1103, '1211002016264', '2016-12-08', 1, NULL, NULL, NULL),
(1104, '1211002017001', '2017-01-01', 1, NULL, NULL, NULL),
(1105, '1211002017013', '2017-01-15', 1, NULL, NULL, NULL),
(1106, '1211002017017', '2017-01-19', 1, NULL, NULL, NULL),
(1107, '1211002017026', '2017-01-31', 1, NULL, NULL, NULL),
(1108, '1211002017036', '2017-02-12', 1, NULL, NULL, NULL),
(1109, '1211002017041', '2017-02-17', 1, NULL, NULL, NULL),
(1110, '1211002017047', '2017-02-26', 1, NULL, NULL, NULL),
(1111, '1211002017051', '2017-03-03', 1, NULL, NULL, NULL),
(1112, '1211002017052', '2017-03-05', 1, NULL, NULL, NULL),
(1113, '1211002017053', '2017-03-06', 1, NULL, NULL, NULL),
(1114, '1211002017054', '2017-03-07', 1, NULL, NULL, NULL),
(1115, '1211002017084', '2017-04-18', 1, NULL, NULL, NULL),
(1116, '1211002017085', '2017-04-19', 1, NULL, NULL, NULL),
(1117, '1211002017088', '2017-04-23', 1, NULL, NULL, NULL),
(1118, '1211002017096', '2017-05-03', 1, NULL, NULL, NULL),
(1119, '1211002017112', '2017-05-24', 1, NULL, NULL, NULL),
(1120, '1211002017133', '2017-06-20', 1, NULL, NULL, NULL),
(1121, '1211002017145', '2017-07-05', 1, NULL, NULL, NULL),
(1122, '1211002017156', '2017-07-18', 1, NULL, NULL, NULL),
(1123, '1211002017158', '2017-07-20', 1, NULL, NULL, NULL),
(1124, '1211002017176', '2017-08-13', 1, NULL, NULL, NULL),
(1125, '1211002017178', '2017-08-16', 1, NULL, NULL, NULL),
(1126, '1211002017180', '2017-08-18', 1, NULL, NULL, NULL),
(1127, '1211002017190', '2017-08-31', 1, NULL, NULL, NULL),
(1128, '1211002017194', '2017-09-07', 1, NULL, NULL, NULL),
(1129, '1211002017196', '2017-09-10', 1, NULL, NULL, NULL),
(1130, '1211002017201', '2017-09-15', 1, NULL, NULL, NULL),
(1131, '1211002017203', '2017-09-18', 1, NULL, NULL, NULL),
(1132, '1211002017205', '2017-09-22', 1, NULL, NULL, NULL),
(1133, '1211002017206', '2017-09-24', 1, NULL, NULL, NULL),
(1134, '1211002017259', '2017-12-10', 1, NULL, NULL, NULL),
(1135, '1211002017263', '2017-12-14', 1, NULL, NULL, NULL),
(1136, '1211002017272', '2017-12-26', 1, NULL, NULL, NULL),
(1137, '1211002018004', '2018-01-04', 1, NULL, NULL, NULL),
(1138, '1211002018006', '2018-01-07', 1, NULL, NULL, NULL),
(1139, '1211002018033', '2018-02-12', 1, NULL, NULL, NULL),
(1140, '1211002018047', '2018-03-05', 1, NULL, NULL, NULL),
(1141, '1211002018057', '2018-03-18', 1, NULL, NULL, NULL),
(1142, '1211002018065', '2018-03-28', 1, NULL, NULL, NULL),
(1143, '1211002018066', '2018-03-29', 1, NULL, NULL, NULL),
(1144, '1211002018071', '2018-04-04', 1, NULL, NULL, NULL),
(1145, '1211002018089', '2018-04-25', 1, NULL, NULL, NULL),
(1146, '1211002018097', '2018-05-07', 1, NULL, NULL, NULL),
(1147, '1211002018101', '2018-05-11', 1, NULL, NULL, NULL),
(1148, '1211002018102', '2018-05-14', 1, NULL, NULL, NULL),
(1149, '1211002018103', '2018-05-15', 1, NULL, NULL, NULL),
(1150, '1211002018104', '2018-05-16', 1, NULL, NULL, NULL),
(1151, '1211002018105', '2018-05-17', 1, NULL, NULL, NULL),
(1152, '1211002018106', '2018-05-18', 1, NULL, NULL, NULL),
(1153, '1211002017011', '2017-01-12', 1, NULL, NULL, NULL),
(1154, '1211002017014', '2017-01-16', 1, NULL, NULL, NULL),
(1155, '1211002017037', '2017-02-13', 1, NULL, NULL, NULL),
(1156, '1211002017040', '2017-02-16', 1, NULL, NULL, NULL),
(1157, '1211002017042', '2017-02-19', 1, NULL, NULL, NULL),
(1158, '1211002017045', '2017-02-22', 1, NULL, NULL, NULL),
(1159, '1211002017055', '2017-03-09', 1, NULL, NULL, NULL),
(1160, '1211002017063', '2017-03-20', 1, NULL, NULL, NULL),
(1161, '1211002017065', '2017-03-22', 1, NULL, NULL, NULL),
(1162, '1211002017068', '2017-03-26', 1, NULL, NULL, NULL),
(1163, '1211002017070', '2017-03-29', 1, NULL, NULL, NULL),
(1164, '1211002017072', '2017-03-31', 1, NULL, NULL, NULL),
(1165, '1211002017074', '2017-04-03', 1, NULL, NULL, NULL),
(1166, '1211002017075', '2017-04-04', 1, NULL, NULL, NULL),
(1167, '1211002017077', '2017-04-07', 1, NULL, NULL, NULL),
(1168, '1211002017079', '2017-04-10', 1, NULL, NULL, NULL),
(1169, '1211002017082', '2017-04-13', 1, NULL, NULL, NULL),
(1170, '1211002017104', '2017-05-15', 1, NULL, NULL, NULL),
(1171, '1211002017118', '2017-06-02', 1, NULL, NULL, NULL),
(1172, '1211002017121', '2017-06-06', 1, NULL, NULL, NULL),
(1173, '1211002017128', '2017-06-14', 1, NULL, NULL, NULL),
(1174, '1211002017132', '2017-06-19', 1, NULL, NULL, NULL),
(1175, '1211002017135', '2017-06-22', 1, NULL, NULL, NULL),
(1176, '1211002017139', '2017-06-28', 1, NULL, NULL, NULL),
(1177, '1211002017161', '2017-07-24', 1, NULL, NULL, NULL),
(1178, '1211002017164', '2017-07-27', 1, NULL, NULL, NULL),
(1179, '1211002017171', '2017-08-06', 1, NULL, NULL, NULL),
(1180, '1211002017172', '2017-08-07', 1, NULL, NULL, NULL),
(1181, '1211002017174', '2017-08-10', 1, NULL, NULL, NULL),
(1182, '1211002017175', '2017-08-11', 1, NULL, NULL, NULL),
(1183, '1211002017207', '2017-09-25', 1, NULL, NULL, NULL),
(1184, '1211002017209', '2017-10-03', 1, NULL, NULL, NULL),
(1185, '1211002017210', '2017-10-04', 1, NULL, NULL, NULL),
(1186, '1211002017221', '2017-10-18', 1, NULL, NULL, NULL),
(1187, '1211002017228', '2017-10-31', 1, NULL, NULL, NULL),
(1188, '1211002017232', '2017-11-05', 1, NULL, NULL, NULL),
(1189, '1211002017234', '2017-11-07', 1, NULL, NULL, NULL),
(1190, '1211002017235', '2017-11-08', 1, NULL, NULL, NULL),
(1191, '1211002017236', '2017-11-09', 1, NULL, NULL, NULL),
(1192, '1211002017238', '2017-11-12', 1, NULL, NULL, NULL),
(1193, '1211002017243', '2017-11-17', 1, NULL, NULL, NULL),
(1194, '1211002017250', '2017-11-26', 1, NULL, NULL, NULL),
(1195, '1211002017255', '2017-12-04', 1, NULL, NULL, NULL),
(1196, '1211002018022', '2018-01-29', 1, NULL, NULL, NULL),
(1197, '1211002018025', '2018-02-02', 1, NULL, NULL, NULL),
(1198, '1211002018043', '2018-02-27', 1, NULL, NULL, NULL),
(1199, '1211002018068', '2018-04-01', 1, NULL, NULL, NULL),
(1200, '1211002018070', '2018-04-03', 1, NULL, NULL, NULL),
(1201, '1211002018072', '2018-04-05', 1, NULL, NULL, NULL),
(1202, '1211002018076', '2018-04-10', 1, NULL, NULL, NULL),
(1203, '1211002018091', '2018-04-27', 1, NULL, NULL, NULL),
(1204, '1211002016267', '2016-12-12', 1, NULL, NULL, NULL),
(1205, '1211002016270', '2016-12-16', 1, NULL, NULL, NULL),
(1206, '1211002016274', '2016-12-21', 1, NULL, NULL, NULL),
(1207, '1211002017016', '2017-01-18', 1, NULL, NULL, NULL),
(1208, '1211002017018', '2017-01-20', 1, NULL, NULL, NULL),
(1209, '1211002017019', '2017-01-22', 1, NULL, NULL, NULL),
(1210, '1211002017023', '2017-01-26', 1, NULL, NULL, NULL),
(1211, '1211002017030', '2017-02-05', 1, NULL, NULL, NULL),
(1212, '1211002017033', '2017-02-08', 1, NULL, NULL, NULL),
(1213, '1211002017035', '2017-02-10', 1, NULL, NULL, NULL),
(1214, '1211002017059', '2017-03-15', 1, NULL, NULL, NULL),
(1215, '1211002017069', '2017-03-28', 1, NULL, NULL, NULL),
(1216, '1211002017076', '2017-04-06', 1, NULL, NULL, NULL),
(1217, '1211002017083', '2017-04-16', 1, NULL, NULL, NULL),
(1218, '1211002017087', '2017-04-21', 1, NULL, NULL, NULL),
(1219, '1211002017102', '2017-05-11', 1, NULL, NULL, NULL),
(1220, '1211002017105', '2017-05-16', 1, NULL, NULL, NULL),
(1221, '1211002017116', '2017-05-31', 1, NULL, NULL, NULL),
(1222, '1211002017117', '2017-06-01', 1, NULL, NULL, NULL),
(1223, '1211002017120', '2017-06-05', 1, NULL, NULL, NULL),
(1224, '1211002017122', '2017-06-07', 1, NULL, NULL, NULL),
(1225, '1211002017123', '2017-06-08', 1, NULL, NULL, NULL),
(1226, '1211002017150', '2017-07-11', 1, NULL, NULL, NULL),
(1227, '1211002017153', '2017-07-14', 1, NULL, NULL, NULL),
(1228, '1211002017173', '2017-08-09', 1, NULL, NULL, NULL),
(1229, '1211002017181', '2017-08-20', 1, NULL, NULL, NULL),
(1230, '1211002017186', '2017-08-25', 1, NULL, NULL, NULL),
(1231, '1211002017202', '2017-09-17', 1, NULL, NULL, NULL),
(1232, '1211002017212', '2017-10-08', 1, NULL, NULL, NULL),
(1233, '1211002017217', '2017-10-13', 1, NULL, NULL, NULL),
(1234, '1211002017241', '2017-11-15', 1, NULL, NULL, NULL),
(1235, '1211002017251', '2017-11-27', 1, NULL, NULL, NULL),
(1236, '1211002017254', '2017-11-30', 1, NULL, NULL, NULL),
(1237, '1211002017274', '2017-12-28', 1, NULL, NULL, NULL),
(1238, '1211002018016', '2018-01-22', 1, NULL, NULL, NULL),
(1239, '1211002018017', '2018-01-23', 1, NULL, NULL, NULL),
(1240, '1211002018020', '2018-01-26', 1, NULL, NULL, NULL),
(1241, '1211002018021', '2018-01-28', 1, NULL, NULL, NULL),
(1242, '1211002018023', '2018-01-31', 1, NULL, NULL, NULL),
(1243, '1211002018026', '2018-02-04', 1, NULL, NULL, NULL),
(1244, '1211002018031', '2018-02-09', 1, NULL, NULL, NULL),
(1245, '1211002018032', '2018-02-11', 1, NULL, NULL, NULL),
(1246, '1211002018036', '2018-02-18', 1, NULL, NULL, NULL),
(1247, '1211002018048', '2018-03-06', 1, NULL, NULL, NULL),
(1248, '1211002018049', '2018-03-07', 1, NULL, NULL, NULL),
(1249, '1211002018051', '2018-03-11', 1, NULL, NULL, NULL),
(1250, '1211002018053', '2018-03-13', 1, NULL, NULL, NULL),
(1251, '1211002018055', '2018-03-15', 1, NULL, NULL, NULL),
(1252, '1211002018056', '2018-03-16', 1, NULL, NULL, NULL),
(1253, '1211002018069', '2018-04-02', 1, NULL, NULL, NULL),
(1254, '1211002018074', '2018-04-08', 1, NULL, NULL, NULL),
(1255, '1211002016260', '2016-12-04', 1, NULL, NULL, NULL),
(1256, '1211002016265', '2016-12-09', 1, NULL, NULL, NULL),
(1257, '1211002017008', '2017-01-09', 1, NULL, NULL, NULL),
(1258, '1211002017009', '2017-01-10', 1, NULL, NULL, NULL),
(1259, '1211002017020', '2017-01-23', 1, NULL, NULL, NULL),
(1260, '1211002017022', '2017-01-25', 1, NULL, NULL, NULL),
(1261, '1211002017024', '2017-01-27', 1, NULL, NULL, NULL),
(1262, '1211002017028', '2017-02-02', 1, NULL, NULL, NULL),
(1263, '1211002017032', '2017-02-07', 1, NULL, NULL, NULL),
(1264, '1211002017050', '2017-03-02', 1, NULL, NULL, NULL),
(1265, '1211002017056', '2017-03-10', 1, NULL, NULL, NULL),
(1266, '1211002017066', '2017-03-23', 1, NULL, NULL, NULL),
(1267, '1211002017091', '2017-04-26', 1, NULL, NULL, NULL),
(1268, '1211002017093', '2017-04-28', 1, NULL, NULL, NULL),
(1269, '1211002017107', '2017-05-18', 1, NULL, NULL, NULL),
(1270, '1211002017119', '2017-06-04', 1, NULL, NULL, NULL),
(1271, '1211002017124', '2017-06-09', 1, NULL, NULL, NULL),
(1272, '1211002017143', '2017-07-03', 1, NULL, NULL, NULL),
(1273, '1211002017147', '2017-07-07', 1, NULL, NULL, NULL),
(1274, '1211002017165', '2017-07-30', 1, NULL, NULL, NULL),
(1275, '1211002017169', '2017-08-03', 1, NULL, NULL, NULL),
(1276, '1211002017182', '2017-08-21', 1, NULL, NULL, NULL),
(1277, '1211002017187', '2017-08-27', 1, NULL, NULL, NULL),
(1278, '1211002017188', '2017-08-28', 1, NULL, NULL, NULL),
(1279, '1211002017189', '2017-08-30', 1, NULL, NULL, NULL),
(1280, '1211002017191', '2017-09-01', 1, NULL, NULL, NULL),
(1281, '1211002017193', '2017-09-06', 1, NULL, NULL, NULL),
(1282, '1211002017204', '2017-09-20', 1, NULL, NULL, NULL),
(1283, '1211002017213', '2017-10-09', 1, NULL, NULL, NULL),
(1284, '1211002017214', '2017-10-10', 1, NULL, NULL, NULL),
(1285, '1211002017215', '2017-10-11', 1, NULL, NULL, NULL),
(1286, '1211002017216', '2017-10-12', 1, NULL, NULL, NULL),
(1287, '1211002017224', '2017-10-24', 1, NULL, NULL, NULL),
(1288, '1211002017226', '2017-10-29', 1, NULL, NULL, NULL),
(1289, '1211002017240', '2017-11-14', 1, NULL, NULL, NULL),
(1290, '1211002017242', '2017-11-16', 1, NULL, NULL, NULL),
(1291, '1211002017257', '2017-12-06', 1, NULL, NULL, NULL),
(1292, '1211002017258', '2017-12-08', 1, NULL, NULL, NULL),
(1293, '1211002017266', '2017-12-18', 1, NULL, NULL, NULL),
(1294, '1211002017268', '2017-12-20', 1, NULL, NULL, NULL),
(1295, '1211002017273', '2017-12-27', 1, NULL, NULL, NULL),
(1296, '1211002017276', '2017-12-31', 1, NULL, NULL, NULL),
(1297, '1211002018027', '2018-02-05', 1, NULL, NULL, NULL),
(1298, '1211002018030', '2018-02-08', 1, NULL, NULL, NULL),
(1299, '1211002018038', '2018-02-21', 1, NULL, NULL, NULL),
(1300, '1211002018044', '2018-02-28', 1, NULL, NULL, NULL),
(1301, '1211002018046', '2018-03-04', 1, NULL, NULL, NULL),
(1302, '1211002018050', '2018-03-09', 1, NULL, NULL, NULL),
(1303, '1211002018054', '2018-03-14', 1, NULL, NULL, NULL),
(1304, '1211002018073', '2018-04-06', 1, NULL, NULL, NULL),
(1305, '1211002018075', '2018-04-09', 1, NULL, NULL, NULL),
(1306, '1211002016262', '2016-12-06', 1, NULL, NULL, NULL),
(1307, '1211002016263', '2016-12-07', 1, NULL, NULL, NULL),
(1308, '1211002016276', '2016-12-23', 1, NULL, NULL, NULL),
(1309, '1211002017002', '2017-01-02', 1, NULL, NULL, NULL),
(1310, '1211002017004', '2017-01-04', 1, NULL, NULL, NULL),
(1311, '1211002017006', '2017-01-06', 1, NULL, NULL, NULL),
(1312, '1211002017012', '2017-01-13', 1, NULL, NULL, NULL),
(1313, '1211002017015', '2017-01-17', 1, NULL, NULL, NULL),
(1314, '1211002017021', '2017-01-24', 1, NULL, NULL, NULL),
(1315, '1211002017027', '2017-02-01', 1, NULL, NULL, NULL),
(1316, '1211002017029', '2017-02-03', 1, NULL, NULL, NULL),
(1317, '1211002017039', '2017-02-15', 1, NULL, NULL, NULL),
(1318, '1211002017057', '2017-03-13', 1, NULL, NULL, NULL),
(1319, '1211002017060', '2017-03-16', 1, NULL, NULL, NULL),
(1320, '1211002017062', '2017-03-19', 1, NULL, NULL, NULL),
(1321, '1211002017064', '2017-03-21', 1, NULL, NULL, NULL),
(1322, '1211002017067', '2017-03-24', 1, NULL, NULL, NULL),
(1323, '1211002017073', '2017-04-02', 1, NULL, NULL, NULL),
(1324, '1211002017086', '2017-04-20', 1, NULL, NULL, NULL),
(1325, '1211002017099', '2017-05-07', 1, NULL, NULL, NULL),
(1326, '1211002017115', '2017-05-30', 1, NULL, NULL, NULL),
(1327, '1211002017134', '2017-06-21', 1, NULL, NULL, NULL),
(1328, '1211002017140', '2017-06-29', 1, NULL, NULL, NULL),
(1329, '1211002017157', '2017-07-19', 1, NULL, NULL, NULL),
(1330, '1211002017177', '2017-08-15', 1, NULL, NULL, NULL),
(1331, '1211002017179', '2017-08-17', 1, NULL, NULL, NULL),
(1332, '1211002017183', '2017-08-22', 1, NULL, NULL, NULL),
(1333, '1211002017185', '2017-08-24', 1, NULL, NULL, NULL),
(1334, '1211002017192', '2017-09-04', 1, NULL, NULL, NULL),
(1335, '1211002017211', '2017-10-06', 1, NULL, NULL, NULL),
(1336, '1211002017222', '2017-10-22', 1, NULL, NULL, NULL),
(1337, '1211002017227', '2017-10-30', 1, NULL, NULL, NULL),
(1338, '1211002017230', '2017-11-02', 1, NULL, NULL, NULL),
(1339, '1211002017231', '2017-11-03', 1, NULL, NULL, NULL),
(1340, '1211002017239', '2017-11-13', 1, NULL, NULL, NULL),
(1341, '1211002017244', '2017-11-19', 1, NULL, NULL, NULL),
(1342, '1211002017247', '2017-11-22', 1, NULL, NULL, NULL),
(1343, '1211002017248', '2017-11-23', 1, NULL, NULL, NULL),
(1344, '1211002017253', '2017-11-29', 1, NULL, NULL, NULL),
(1345, '1211002017260', '2017-12-11', 1, NULL, NULL, NULL),
(1346, '1211002017262', '2017-12-13', 1, NULL, NULL, NULL),
(1347, '1211002017271', '2017-12-24', 1, NULL, NULL, NULL),
(1348, '1211002017275', '2017-12-29', 1, NULL, NULL, NULL),
(1349, '1211002018001', '2018-01-01', 1, NULL, NULL, NULL),
(1350, '1211002018002', '2018-01-02', 1, NULL, NULL, NULL),
(1351, '1211002018007', '2018-01-08', 1, NULL, NULL, NULL),
(1352, '1211002018008', '2018-01-09', 1, NULL, NULL, NULL),
(1353, '1211002018010', '2018-01-12', 1, NULL, NULL, NULL),
(1354, '1211002018024', '2018-02-01', 1, NULL, NULL, NULL),
(1355, '1211002018034', '2018-02-14', 1, NULL, NULL, NULL),
(1356, '1211002018037', '2018-02-20', 1, NULL, NULL, NULL),
(1357, '1211002016257', '2016-11-30', 1, NULL, NULL, NULL),
(1358, '1211002016266', '2016-12-11', 1, NULL, NULL, NULL),
(1359, '1211002016272', '2016-12-19', 1, NULL, NULL, NULL),
(1360, '1211002016277', '2016-12-26', 1, NULL, NULL, NULL),
(1361, '1211002016279', '2016-12-28', 1, NULL, NULL, NULL),
(1362, '1211002016281', '2016-12-29', 1, NULL, NULL, NULL),
(1363, '1211002017010', '2017-01-11', 1, NULL, NULL, NULL),
(1364, '1211002017048', '2017-02-28', 1, NULL, NULL, NULL),
(1365, '1211002017058', '2017-03-14', 1, NULL, NULL, NULL),
(1366, '1211002017061', '2017-03-17', 1, NULL, NULL, NULL),
(1367, '1211002017090', '2017-04-25', 1, NULL, NULL, NULL),
(1368, '1211002017092', '2017-04-27', 1, NULL, NULL, NULL),
(1369, '1211002017095', '2017-05-02', 1, NULL, NULL, NULL),
(1370, '1211002017097', '2017-05-04', 1, NULL, NULL, NULL),
(1371, '1211002017106', '2017-05-17', 1, NULL, NULL, NULL),
(1372, '1211002017108', '2017-05-19', 1, NULL, NULL, NULL),
(1373, '1211002017110', '2017-05-22', 1, NULL, NULL, NULL),
(1374, '1211002017149', '2017-07-10', 1, NULL, NULL, NULL),
(1375, '1211002017151', '2017-07-12', 1, NULL, NULL, NULL),
(1376, '1211002017160', '2017-07-23', 1, NULL, NULL, NULL),
(1377, '1211002017163', '2017-07-26', 1, NULL, NULL, NULL),
(1378, '1211002017184', '2017-08-23', 1, NULL, NULL, NULL),
(1379, '1211002017195', '2017-09-08', 1, NULL, NULL, NULL),
(1380, '1211002017197', '2017-09-11', 1, NULL, NULL, NULL),
(1381, '1211002017200', '2017-09-14', 1, NULL, NULL, NULL),
(1382, '1211002017223', '2017-10-23', 1, NULL, NULL, NULL),
(1383, '1211002017229', '2017-11-01', 1, NULL, NULL, NULL),
(1384, '1211002017233', '2017-11-06', 1, NULL, NULL, NULL),
(1385, '1211002017237', '2017-11-10', 1, NULL, NULL, NULL),
(1386, '1211002018012', '2018-01-16', 1, NULL, NULL, NULL),
(1387, '1211002018014', '2018-01-19', 1, NULL, NULL, NULL),
(1388, '1211002018018', '2018-01-24', 1, NULL, NULL, NULL),
(1389, '1211002018019', '2018-01-25', 1, NULL, NULL, NULL),
(1390, '1211002018028', '2018-02-06', 1, NULL, NULL, NULL),
(1391, '1211002018039', '2018-02-22', 1, NULL, NULL, NULL),
(1392, '1211002018040', '2018-02-23', 1, NULL, NULL, NULL),
(1393, '1211002018059', '2018-03-20', 1, NULL, NULL, NULL),
(1394, '1211002018060', '2018-03-21', 1, NULL, NULL, NULL),
(1395, '1211002018061', '2018-03-22', 1, NULL, NULL, NULL),
(1396, '1211002018063', '2018-03-26', 1, NULL, NULL, NULL),
(1397, '1211002018064', '2018-03-27', 1, NULL, NULL, NULL),
(1398, '1211002018067', '2018-03-30', 1, NULL, NULL, NULL),
(1399, '1211002018077', '2018-04-11', 1, NULL, NULL, NULL),
(1400, '1211002018078', '2018-04-12', 1, NULL, NULL, NULL),
(1401, '1211002018079', '2018-04-13', 1, NULL, NULL, NULL),
(1402, '1211002018080', '2018-04-15', 1, NULL, NULL, NULL),
(1403, '1211002018082', '2018-04-17', 1, NULL, NULL, NULL),
(1404, '1211002018084', '2018-04-19', 1, NULL, NULL, NULL),
(1405, '1211002018087', '2018-04-23', 1, NULL, NULL, NULL),
(1406, '1211002018092', '2018-04-29', 1, NULL, NULL, NULL),
(1407, '1211002018093', '2018-05-02', 1, NULL, NULL, NULL),
(1408, '1211002018114', '2018-05-28', 1, NULL, NULL, NULL),
(1409, '1211002018116', '2018-05-30', 1, NULL, NULL, NULL),
(1410, '1211002018117', '2018-05-31', 1, NULL, NULL, NULL),
(1411, '1211002018119', '2018-06-03', 1, NULL, NULL, NULL),
(1412, '1211002018124', '2018-06-08', 1, NULL, NULL, NULL),
(1413, '1211002018126', '2018-06-11', 1, NULL, NULL, NULL),
(1414, '1211002018127', '2018-06-12', 1, NULL, NULL, NULL),
(1415, '1211002018129', '2018-06-14', 1, NULL, NULL, NULL),
(1416, '1211002018130', '2018-06-15', 1, NULL, NULL, NULL),
(1417, '1211002018146', '2018-07-04', 1, NULL, NULL, NULL),
(1418, '1211002018149', '2018-07-08', 1, NULL, NULL, NULL),
(1419, '1211002018152', '2018-07-11', 1, NULL, NULL, NULL),
(1420, '1211002018154', '2018-07-13', 1, NULL, NULL, NULL),
(1421, '1211002018157', '2018-07-17', 1, NULL, NULL, NULL),
(1422, '1211002018159', '2018-07-19', 1, NULL, NULL, NULL),
(1423, '1211002018167', '2018-07-29', 1, NULL, NULL, NULL),
(1424, '1211002018171', '2018-08-02', 1, NULL, NULL, NULL),
(1425, '1211002018174', '2018-08-06', 1, NULL, NULL, NULL),
(1426, '1211002018175', '2018-08-07', 1, NULL, NULL, NULL),
(1427, '1211002018178', '2018-08-10', 1, NULL, NULL, NULL),
(1428, '1211002018179', '2018-08-12', 1, NULL, NULL, NULL),
(1429, '1211002018184', '2018-08-17', 1, NULL, NULL, NULL),
(1430, '1211002018203', '2018-09-10', 1, NULL, NULL, NULL),
(1431, '1211002018204', '2018-09-11', 1, NULL, NULL, NULL),
(1432, '1211002018206', '2018-09-13', 1, NULL, NULL, NULL),
(1433, '1211002018207', '2018-09-14', 1, NULL, NULL, NULL),
(1434, '1211002018121', '2018-06-05', 1, NULL, NULL, NULL),
(1435, '1211002018128', '2018-06-13', 1, NULL, NULL, NULL),
(1436, '1211002018138', '2018-06-25', 1, NULL, NULL, NULL),
(1437, '1211002018141', '2018-06-28', 1, NULL, NULL, NULL),
(1438, '1211002018155', '2018-07-15', 1, NULL, NULL, NULL),
(1439, '1211002018170', '2018-08-01', 1, NULL, NULL, NULL),
(1440, '1211002018173', '2018-08-05', 1, NULL, NULL, NULL),
(1441, '1211002018177', '2018-08-09', 1, NULL, NULL, NULL),
(1442, '1211002018193', '2018-08-29', 1, NULL, NULL, NULL),
(1443, '1211002018197', '2018-09-03', 1, NULL, NULL, NULL),
(1444, '1211002018200', '2018-09-06', 1, NULL, NULL, NULL),
(1445, '1211002018107', '2018-05-20', 1, NULL, NULL, NULL),
(1446, '1211002018108', '2018-05-21', 1, NULL, NULL, NULL),
(1447, '1211002018109', '2018-05-22', 1, NULL, NULL, NULL),
(1448, '1211002018111', '2018-05-24', 1, NULL, NULL, NULL),
(1449, '1211002018112', '2018-05-25', 1, NULL, NULL, NULL),
(1450, '1211002018122', '2018-06-06', 1, NULL, NULL, NULL),
(1451, '1211002018125', '2018-06-10', 1, NULL, NULL, NULL),
(1452, '1211002018140', '2018-06-27', 1, NULL, NULL, NULL),
(1453, '1211002018151', '2018-07-10', 1, NULL, NULL, NULL),
(1454, '1211002018153', '2018-07-12', 1, NULL, NULL, NULL),
(1455, '1211002018156', '2018-07-16', 1, NULL, NULL, NULL),
(1456, '1211002018158', '2018-07-18', 1, NULL, NULL, NULL),
(1457, '1211002018160', '2018-07-20', 1, NULL, NULL, NULL),
(1458, '1211002018169', '2018-07-31', 1, NULL, NULL, NULL),
(1459, '1211002018176', '2018-08-08', 1, NULL, NULL, NULL),
(1460, '1211002018181', '2018-08-14', 1, NULL, NULL, NULL),
(1461, '1211002018182', '2018-08-15', 1, NULL, NULL, NULL),
(1462, '1211002018186', '2018-08-20', 1, NULL, NULL, NULL),
(1463, '1211002018187', '2018-08-21', 1, NULL, NULL, NULL),
(1464, '1211002018189', '2018-08-23', 1, NULL, NULL, NULL),
(1465, '1211002018194', '2018-08-30', 1, NULL, NULL, NULL),
(1466, '1211002018196', '2018-09-02', 1, NULL, NULL, NULL),
(1467, '1211002018198', '2018-09-04', 1, NULL, NULL, NULL),
(1468, '1211002018201', '2018-09-07', 1, NULL, NULL, NULL),
(1469, '1211002018110', '2018-05-23', 1, NULL, NULL, NULL),
(1470, '1211002018115', '2018-05-29', 1, NULL, NULL, NULL),
(1471, '1211002018131', '2018-06-17', 1, NULL, NULL, NULL),
(1472, '1211002018132', '2018-06-18', 1, NULL, NULL, NULL),
(1473, '1211002018135', '2018-06-21', 1, NULL, NULL, NULL),
(1474, '1211002018136', '2018-06-22', 1, NULL, NULL, NULL),
(1475, '1211002018139', '2018-06-26', 1, NULL, NULL, NULL),
(1476, '1211002018148', '2018-07-06', 1, NULL, NULL, NULL),
(1477, '1211002018162', '2018-07-23', 1, NULL, NULL, NULL),
(1478, '1211002018165', '2018-07-26', 1, NULL, NULL, NULL),
(1479, '1211002018180', '2018-08-13', 1, NULL, NULL, NULL),
(1480, '1211002018183', '2018-08-16', 1, NULL, NULL, NULL),
(1481, '1211002018188', '2018-08-22', 1, NULL, NULL, NULL),
(1482, '1211002018205', '2018-09-12', 1, NULL, NULL, NULL),
(1483, '1211002018120', '2018-06-04', 1, NULL, NULL, NULL),
(1484, '1211002018123', '2018-06-07', 1, NULL, NULL, NULL),
(1485, '1211002018133', '2018-06-19', 1, NULL, NULL, NULL),
(1486, '1211002018150', '2018-07-09', 1, NULL, NULL, NULL),
(1487, '1211002018192', '2018-08-28', 1, NULL, NULL, NULL),
(1488, '1211002018195', '2018-08-31', 1, NULL, NULL, NULL),
(1489, '1211002018202', '2018-09-09', 1, NULL, NULL, NULL),
(1490, '1211002018113', '2018-05-27', 1, NULL, NULL, NULL),
(1491, '1211002018118', '2018-06-01', 1, NULL, NULL, NULL),
(1492, '1211002018185', '2018-08-19', 1, NULL, NULL, NULL),
(1493, '1211002018190', '2018-08-24', 1, NULL, NULL, NULL),
(1494, '1211002018199', '2018-09-05', 1, NULL, NULL, NULL),
(1495, '1211002018134', '2018-06-20', 1, NULL, NULL, NULL),
(1496, '1211002018137', '2018-06-24', 1, NULL, NULL, NULL),
(1497, '1211002018142', '2018-06-29', 1, NULL, NULL, NULL),
(1498, '1211002018143', '2018-07-01', 1, NULL, NULL, NULL),
(1499, '1211002018144', '2018-07-02', 1, NULL, NULL, NULL),
(1500, '1211002018145', '2018-07-03', 1, NULL, NULL, NULL),
(1501, '1211002018147', '2018-07-05', 1, NULL, NULL, NULL),
(1502, '1211002018161', '2018-07-22', 1, NULL, NULL, NULL),
(1503, '1211002018163', '2018-07-24', 1, NULL, NULL, NULL),
(1504, '1211002018164', '2018-07-25', 1, NULL, NULL, NULL),
(1505, '1211002018166', '2018-07-27', 1, NULL, NULL, NULL),
(1506, '1211002018168', '2018-07-30', 1, NULL, NULL, NULL),
(1507, '1211002018172', '2018-08-03', 1, NULL, NULL, NULL),
(1508, '1211002018191', '2018-08-26', 1, NULL, NULL, NULL),
(1509, '1211002018208', '2018-09-16', 1, NULL, NULL, NULL),
(1510, '1211002018209', '2018-09-17', 1, NULL, NULL, NULL),
(1511, '1211002018210', '2018-09-18', 1, NULL, NULL, NULL),
(1512, '1211002018211', '2018-09-20', 1, NULL, NULL, NULL),
(1513, '1211002018212', '2018-09-21', 1, NULL, NULL, NULL),
(1514, '1211002018213', '2018-09-23', 1, NULL, '2018-09-25 11:55:05', '2018-09-25 11:55:05'),
(1515, '1211002018214', '2018-09-25', 1, NULL, '2018-09-25 11:55:29', '2018-09-25 11:55:29'),
(1516, '1211002018215', '2018-09-26', 1, NULL, '2018-09-25 11:56:40', '2018-09-25 11:56:40'),
(1517, '1211002018216', '2018-09-27', 1, NULL, '2018-09-25 11:57:03', '2018-09-25 11:57:03'),
(1518, '1211002018217', '2018-09-28', 1, NULL, '2018-09-25 11:57:18', '2018-09-25 11:57:18'),
(1519, '1211002018218', '2018-09-30', 1, NULL, '2018-09-30 08:23:20', '2018-09-30 08:23:20'),
(1520, '1211002018219', '2018-10-01', 1, NULL, '2018-09-30 08:24:27', '2018-09-30 08:24:27'),
(1521, '1211002018220', '2018-10-02', 1, NULL, '2018-09-30 08:24:56', '2018-09-30 08:24:56'),
(1522, '121100201821', '2018-10-03', 1, '2018-09-30 08:34:20', '2018-09-30 08:26:01', '2018-09-30 08:34:20'),
(1523, '1211002018222', '2018-10-04', 1, '2018-09-30 08:34:47', '2018-09-30 08:27:13', '2018-09-30 08:34:47'),
(1524, '1211002018221', '2018-10-03', 1, NULL, '2018-09-30 10:24:15', '2018-10-01 11:39:56'),
(1525, '1111111111112', '2018-10-01', 2, '2018-10-01 11:15:02', '2018-10-01 11:06:43', '2018-10-01 11:15:02'),
(1526, '1211002018222', '2018-10-04', 1, NULL, '2018-10-01 12:06:07', '2018-10-01 12:06:07'),
(1527, '1211002018223', '2018-10-05', 1, NULL, '2018-10-01 12:06:55', '2018-10-01 12:07:11'),
(1528, '1211002018224', '2018-10-07', 1, NULL, '2018-10-05 10:14:57', '2018-10-05 10:14:57'),
(1529, '1211002018225', '2018-10-08', 1, NULL, '2018-10-05 10:15:52', '2018-10-05 10:15:52'),
(1530, '1211002018226', '2018-10-09', 1, NULL, '2018-10-05 10:16:36', '2018-10-05 10:16:36'),
(1531, '1211002018227', '2018-10-10', 1, NULL, '2018-10-05 10:19:27', '2018-10-05 10:19:27'),
(1532, '1211002018228', '2018-10-11', 1, NULL, '2018-10-05 10:20:13', '2018-10-05 10:20:13'),
(1533, '1211002018229', '2018-10-12', 1, NULL, '2018-10-05 10:22:33', '2018-10-05 10:22:33'),
(1534, '1211002018230', '2018-10-14', 1, NULL, '2018-10-14 08:35:45', '2018-10-14 08:35:45'),
(1535, '1211002018231', '2018-10-15', 1, NULL, '2018-10-14 08:36:09', '2018-10-14 08:36:09'),
(1536, '1211002018232', '2018-10-21', 1, NULL, '2018-10-21 10:35:20', '2018-10-21 10:35:20'),
(1537, '1211002018233', '2018-10-22', 1, NULL, '2018-10-21 10:35:58', '2018-10-21 10:35:58'),
(1538, '1211002018234', '2018-10-23', 1, NULL, '2018-10-21 10:36:39', '2018-10-21 10:37:52'),
(1539, '1211002018235', '2018-10-24', 1, NULL, '2018-10-21 10:38:14', '2018-10-21 10:38:14'),
(1540, '1211002018236', '2018-10-25', 1, NULL, '2018-10-21 10:39:21', '2018-10-21 10:39:21'),
(1541, '1211002018237', '2018-10-26', 1, NULL, '2018-10-21 10:39:56', '2018-10-21 10:39:56'),
(1542, '1211002018238', '2018-10-28', 1, NULL, '2018-10-28 09:25:22', '2018-10-28 09:25:22'),
(1543, '1211002018239', '2018-10-29', 1, NULL, '2018-10-28 09:25:40', '2018-10-28 09:25:40'),
(1544, '1211002018240', '2018-10-30', 1, NULL, '2018-10-28 09:26:05', '2018-10-28 09:26:05'),
(1545, '1211002018241', '2018-10-31', 1, NULL, '2018-10-28 09:26:52', '2018-10-28 09:26:52'),
(1546, '1211002018242', '2018-11-01', 1, NULL, '2018-10-28 09:27:14', '2018-10-28 09:27:14'),
(1547, '1211002018243', '2018-11-02', 1, NULL, '2018-10-28 09:27:55', '2018-10-28 09:27:55'),
(1548, '1211002018244', '2018-11-04', 1, NULL, '2018-11-02 09:30:25', '2018-11-02 09:30:25'),
(1549, '1211002018245', '2018-11-05', 1, NULL, '2018-11-02 09:30:42', '2018-11-02 09:30:42'),
(1550, '1211002018246', '2018-11-06', 1, NULL, '2018-11-02 09:31:18', '2018-11-02 09:31:18'),
(1551, '1211002018247', '2018-11-11', 1, NULL, '2018-11-12 11:08:43', '2018-11-12 11:08:43'),
(1552, '1211002018248', '2018-11-12', 1, NULL, '2018-11-12 11:09:08', '2018-11-12 11:09:08'),
(1553, '1211002018249', '2018-11-13', 1, NULL, '2018-11-13 11:15:56', '2018-11-13 11:15:56'),
(1554, '1211002018250', '2018-11-14', 1, NULL, '2018-11-13 11:16:24', '2018-11-13 11:16:24'),
(1555, '1211002018251', '2018-11-15', 1, NULL, '2018-11-13 11:17:27', '2018-11-13 11:17:27'),
(1556, '1211002018252', '2018-11-16', 1, NULL, '2018-11-13 11:18:18', '2018-11-13 11:18:18'),
(1557, '1211002018253', '2018-11-18', 1, NULL, '2018-11-18 11:28:31', '2018-11-18 11:28:31'),
(1558, '1211002018254', '2018-11-19', 1, NULL, '2018-11-18 11:28:46', '2018-11-18 11:28:46'),
(1559, '1211002018255', '2018-11-20', 1, NULL, '2018-11-18 11:29:11', '2018-11-18 11:29:11'),
(1560, '1211002018256', '2018-11-21', 1, NULL, '2018-11-18 11:29:37', '2018-11-18 11:29:37'),
(1561, '1211002018257', '2018-11-22', 1, NULL, '2018-11-18 11:30:07', '2018-11-18 11:30:07'),
(1562, '1211002018258', '2018-11-23', 1, NULL, '2018-11-18 11:30:43', '2018-11-18 11:30:43'),
(1563, '1211002018259', '2018-11-25', 1, NULL, '2018-11-25 11:48:54', '2018-11-25 11:48:54'),
(1564, '1211002018260', '2018-11-26', 1, NULL, '2018-11-25 11:49:33', '2018-11-25 11:49:33'),
(1565, '1211002018261', '2018-11-27', 1, NULL, '2018-11-25 11:49:56', '2018-11-25 11:49:56'),
(1566, '1211002018262', '2018-11-28', 1, NULL, '2018-11-25 11:51:14', '2018-11-25 11:51:14'),
(1567, '1211002018263', '2018-11-29', 1, NULL, '2018-11-25 11:52:21', '2018-11-25 11:52:21'),
(1568, '1211002018264', '2018-11-30', 1, NULL, '2018-11-25 11:54:53', '2018-11-25 11:54:53'),
(1569, '1211002018265', '2018-12-02', 1, NULL, '2018-11-30 10:53:33', '2018-11-30 10:53:33'),
(1570, '1211002018266', '2018-12-03', 1, NULL, '2018-11-30 10:53:53', '2018-11-30 10:53:53'),
(1571, '1211002018267', '2018-12-04', 1, NULL, '2018-11-30 10:54:13', '2018-11-30 10:54:13'),
(1572, '1211002018268', '2018-12-05', 1, NULL, '2018-11-30 10:54:53', '2018-11-30 10:54:53'),
(1573, '1211002018269', '2018-12-06', 1, NULL, '2018-11-30 10:55:43', '2018-11-30 10:55:43'),
(1574, '1211002018270', '2018-12-07', 1, NULL, '2018-11-30 10:56:13', '2018-11-30 10:56:13');

-- --------------------------------------------------------

--
-- Table structure for table `slider_image`
--

CREATE TABLE `slider_image` (
  `image_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider_image`
--

INSERT INTO `slider_image` (`image_id`, `title`, `description`, `file`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'cdsc1', 'cdsc image', '2018_10_31_08_13_47_cdscslide1.jpg', NULL, '2018-10-31 02:28:47', '2018-10-31 02:34:28'),
(2, 'cdsc2', 'cdsc2', '2018_10_31_08_15_18_cdscslide2.jpg', NULL, '2018-10-31 02:30:18', '2018-10-31 02:34:32'),
(3, 'cdsc3', 'cdsc3', '2018_10_31_08_15_36_cdscslide3.jpg', NULL, '2018-10-31 02:30:36', '2018-10-31 02:34:35'),
(4, 'cdsc4', 'cdsc4', '2018_10_31_08_15_52_cdscslide4.jpg', NULL, '2018-10-31 02:30:52', '2018-10-31 02:34:40');

-- --------------------------------------------------------

--
-- Table structure for table `submenu`
--

CREATE TABLE `submenu` (
  `submenu_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `menu_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `submenu`
--

INSERT INTO `submenu` (`submenu_id`, `status`, `menu_id`, `name`, `created_at`, `updated_at`, `deleted_at`, `order`) VALUES
(1, 1, 1, 'Introduction', '2018-10-09 02:19:16', '2018-10-09 02:19:16', NULL, 1),
(2, 1, 1, 'Board Of Directors', '2018-10-09 02:19:28', '2018-11-21 10:11:11', NULL, 2),
(3, 1, 1, 'Management Team', '2018-10-09 02:19:39', '2018-10-09 02:19:39', NULL, 3),
(4, 1, 1, 'Global Partners', '2018-10-09 02:19:51', '2018-10-09 02:19:51', NULL, 4),
(5, 1, 2, 'FAQ', '2018-10-09 02:20:20', '2018-10-09 02:20:20', NULL, 1),
(6, 1, 2, 'Settlement ID', '2018-10-09 02:20:38', '2018-10-09 02:20:38', NULL, 2),
(7, 1, 2, 'Dos And Donts', '2018-10-09 02:21:14', '2018-10-09 02:21:14', NULL, 3),
(8, 1, 2, 'Event', '2018-10-09 02:21:30', '2018-10-09 02:21:30', NULL, 4),
(9, 1, 3, 'Introduction', '2018-10-09 02:21:48', '2018-10-09 02:21:48', NULL, 1),
(10, 1, 3, 'List Of DP', '2018-10-09 02:22:31', '2018-10-09 02:22:31', NULL, 2),
(11, 1, 3, 'Admission Procedure', '2018-10-09 02:23:48', '2018-10-09 02:23:48', NULL, 3),
(12, 1, 3, 'Hardware Software Required', '2018-10-09 02:24:15', '2018-10-09 02:24:15', NULL, 4),
(13, 1, 4, 'Introduction', '2018-10-09 02:24:29', '2018-10-09 02:24:29', NULL, 1),
(14, 1, 4, 'RTA List', '2018-10-09 02:24:47', '2018-10-09 02:24:47', NULL, 2),
(15, 1, 4, 'Admission Procedure', '2018-10-09 02:25:12', '2018-10-09 02:25:12', NULL, 3),
(16, 1, 4, 'Documentation for Corporate Action', '2018-10-09 02:25:40', '2018-10-09 02:25:40', NULL, 4),
(17, 1, 5, 'Introduction', '2018-10-09 02:25:57', '2018-10-09 02:25:57', NULL, 1),
(18, 1, 5, 'List Of Clearing Member', '2018-10-09 02:26:21', '2018-10-09 02:26:21', NULL, 2),
(19, 1, 5, 'Settlement Procedure', '2018-10-09 02:26:59', '2018-10-09 02:26:59', NULL, 3),
(20, 1, 6, 'Introduction', '2018-10-09 02:27:18', '2018-10-09 02:27:18', NULL, 1),
(21, 1, 6, 'ISIN & Script', '2018-10-09 02:27:38', '2018-10-09 02:27:38', NULL, 2),
(22, 1, 6, 'Registered Company', '2018-10-09 02:28:18', '2018-10-09 02:28:18', NULL, 3),
(23, 1, 6, 'Benefits', '2018-10-09 02:29:15', '2018-10-09 02:29:15', NULL, 4),
(24, 1, 6, 'Admission Procedure', '2018-10-09 02:29:25', '2018-10-09 02:29:25', NULL, 5),
(25, 1, 7, 'Account Opening', '2018-10-09 02:29:53', '2018-10-09 02:29:53', NULL, 1),
(26, 1, 7, 'Dematerialisation', '2018-10-09 02:30:28', '2018-10-09 02:30:28', NULL, 2),
(27, 1, 7, 'Transmission of securities', '2018-10-09 02:30:58', '2018-10-09 02:30:58', NULL, 3),
(28, 1, 7, 'Account Statement', '2018-10-09 02:31:22', '2018-10-09 02:31:22', NULL, 4),
(29, 1, 7, 'Rematerialisation', '2018-10-09 02:31:55', '2018-10-09 02:31:55', NULL, 5),
(30, 1, 7, 'Pledging', '2018-10-09 02:32:23', '2018-10-09 02:32:23', NULL, 6),
(31, 1, 8, 'Circulars', '2018-10-15 02:42:01', '2018-10-15 02:42:01', NULL, 1),
(32, 1, 8, 'By Laws', '2018-10-15 02:43:18', '2018-10-15 02:43:18', NULL, 2),
(33, 1, 8, 'Publications', '2018-10-15 03:34:30', '2018-10-15 03:34:30', NULL, 3),
(34, 1, 8, 'Demat Registration', '2018-10-15 03:35:03', '2018-10-15 03:35:03', NULL, 4),
(35, 1, 2, 'Investor', '2018-10-21 00:26:38', '2018-10-21 00:26:38', NULL, 5),
(36, 1, 2, 'Death Notice', '2018-10-22 00:44:57', '2018-10-22 00:44:57', NULL, 6),
(37, 1, 3, 'CDSC Traiff', '2018-11-02 22:40:29', '2018-11-02 22:44:50', NULL, 5);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(50) NOT NULL DEFAULT '0',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL COMMENT '1-CDSC 3-DP 4-RTA',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actual_password` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(10) NOT NULL DEFAULT '1' COMMENT '0-Inactive 1-Active 2-Deleted',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_id`, `email`, `type`, `password`, `actual_password`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'operations@cdsc.com.np', 1, '$2y$10$rCvp8Zm1ZVSgYh72iQJUlOQOHMi5sir5q6L1h7cMRX1XAES/dY1Au', NULL, 1, NULL, '2018-11-15 12:49:25', '2018-11-20 11:22:35'),
(2, 12300, 'agrawalsecurities1@gmail.com', 3, '$2y$10$RUvtkQ71e0Q.yr0KkOdFyuPd1sYEKqNEz2MHTdSPhZQebEX0a7Zwa', NULL, 1, NULL, '2018-11-15 12:52:38', '2018-11-15 12:52:38'),
(3, 12000, 'dpdispltd@gmail.com', 3, '$2y$10$kATqQLjrtszEP6b0TbXDA.942e.6I.8MyHKCqAq.zZxlC0RLtyiI2', NULL, 1, NULL, '2018-11-18 11:02:09', '2018-11-18 11:02:09'),
(4, 10900, 'pallavi@siddharthacapital.com', 3, '$2y$10$baeqXW8CBDzNphGMTMfuyOF1tavOKbc1StLOsNcBtF4X3Yh7wGBzi', NULL, 1, NULL, '2018-11-19 09:44:37', '2018-11-20 14:01:20'),
(6, 1, 'jagat.subedi@cdscnp.com', 1, '$2y$10$nfl6l9jeryQ2zSTKHaEqeOP0lXmcFny2kqk3JS0bip7T7VC7FHddu', NULL, 1, NULL, '2018-11-22 14:59:44', '2018-11-23 11:59:52'),
(7, 1, 'dev.rijal@cdsc.com.np', 1, '$2y$10$mkotgPvhojH1mDDgwQCHBOIXlsT3QPtXh.E5EBARs3SuqSx3gEwwW', NULL, 1, NULL, '2018-11-22 15:00:13', '2018-11-23 11:03:46'),
(8, 153, 'premier@picl.com.np', 4, '$2y$10$Sldx1ukXpuapAujGWhWu/uwXcmxVSQxIGtciA0ed9nVYVvhfVMXuq', NULL, 1, NULL, '2018-11-25 11:01:06', '2018-11-25 14:37:37'),
(9, 160, 'shanti.pudasaini@cbilcapital.com', 4, '$2y$10$yy.1Y.CFLzzStYfFZ2z/S.7dh.Dg6kz1PqVtW8n/Ing314DJryvZG', NULL, 1, NULL, '2018-11-25 11:06:29', '2018-11-25 11:55:11'),
(10, 114, 'sunita.dahal@mbl.com.np', 4, '$2y$10$pemWt8cNeT.pnRrig6bq8eRxX1PK8LrLx8.Bob9xGKPkLvoAEQ7gW', '4cq7tR6K', 1, NULL, '2018-11-25 11:14:11', '2018-11-25 11:14:11'),
(11, 16100, 'sujata.bajracharya@mbl.com.np', 3, '$2y$10$30KITHrshd35G2XsFoQiRePLnusRpzlQmt8D6dg1qfAAQMnYbdOyW', NULL, 1, NULL, '2018-11-25 11:14:35', '2018-11-25 11:29:14'),
(12, 11700, 'anjana.sapkota@ctznbank.com', 3, '$2y$10$0VBwBJr3xuMtuhN6GSw4ZuGiWRzjFA3ix82w01uh.Mi3VBbDEOlvO', NULL, 1, NULL, '2018-11-25 13:20:27', '2018-11-25 13:42:59'),
(13, 140, 'cs.nt@ntc.net.np', 4, '$2y$10$T8AFwQ3Ouk.0Yh.z5MVhF.XRhCH2/NnY2VY3puA0PmOurDndiBlJi', NULL, 1, NULL, '2018-11-25 14:17:48', '2018-11-25 14:39:19'),
(14, 10200, 'amita@niblcapital.com', 3, '$2y$10$Talq0RDOzlLYLpWIwpcPgePwQ9VOb5kaLtsYW.EHtsPmqXRJ5fv62', NULL, 1, NULL, '2018-11-26 12:53:22', '2018-11-26 13:19:46'),
(15, 10600, 'amita@niblcapital.com', 3, '$2y$10$0BHH5xw2y/3MNo.dWxj37uuwQldPML4bspp.eA2bRMHD14UNWuE/S', NULL, 1, NULL, '2018-11-26 12:55:20', '2018-11-26 13:18:43'),
(16, 101, 'pratibha@niblcapital.com', 4, '$2y$10$a9eBo/HLiwjr9eLWTLQqj.cg7jXXT6FE7zX4nA5.rs2Sl6F0AfWQm', NULL, 1, NULL, '2018-11-26 14:38:57', '2018-11-26 14:50:01'),
(17, 120, 'pratibha@niblcapital.com', 4, '$2y$10$sKu1CgvZnEnSjvIezXWUh.AxrGnw4r8AvvX7Tc4K1MRpYzZrBOzZG', NULL, 1, NULL, '2018-11-26 14:39:11', '2018-11-26 14:57:17'),
(18, 14100, 'rajesh.kriticapital@gmail.com', 3, '$2y$10$2q.sAOojnwik5FiLo4ERwustPcFu8s1SJMXy2wa0QTkEaAGgfPvn2', NULL, 1, NULL, '2018-11-27 14:39:56', '2018-11-27 15:39:20'),
(19, 142, 'pallavi@siddharthacapital.com', 4, '$2y$10$b3hgEeS.CJy.vv3GKNSW6OX5FRCsCsSrr4m3VkNCPegzDv2/v/Ydi', NULL, 1, NULL, '2018-11-27 14:41:55', '2018-11-27 15:50:05'),
(20, 112, 'legal@himalayanbank.com', 4, '$2y$10$FBDQb5mRRIaIb5bNM9UpbObcO7hnaAc9Al16il7V9HXk3Blgq.tDS', NULL, 1, NULL, '2018-11-29 10:27:57', '2018-11-29 14:35:33'),
(21, 15000, 'dpbhrikuti55@gmail.com', 3, '$2y$10$IhfWtlyvREnFzQpWl3xguuSQpBAX8/b.XbuibsIadaVJPqVqrEDLK', NULL, 1, NULL, '2018-11-29 12:28:51', '2018-11-29 13:09:32'),
(22, 116, 'sujan.maharjan@nabilinvest.com.np', 4, '$2y$10$YhqjfP8D2foAd2RSjk5Jne0v7BSE4pvmL3nR5o5ews5.xkjjvCTg6', NULL, 1, NULL, '2018-11-30 11:16:51', '2018-12-05 15:31:47'),
(23, 10400, 'bibek.paneru@nabilinvest.com.np', 3, '$2y$10$miUEXhosGxRUGuqqWgkvE./QetejzhAE8MPemF83hAJdqbXiD6VsW', NULL, 1, NULL, '2018-11-30 11:17:18', '2018-11-30 11:46:03'),
(24, 14800, 'premiersecurities@hotmail.com', 3, '$2y$10$t5soea2D.rf7MqI9Fk0sjufaIfngnfT3ZmFCBpdLxIqnp6tAOeDqG', NULL, 1, NULL, '2018-11-30 11:18:06', '2018-11-30 15:25:24'),
(25, 119, 'dipa.bhandari@prabhucapital.com', 4, '$2y$10$8F19ZM8OIfciAkzuF.dTEOPAgxYSs5DfrbvoNfd/u8uWBtka2.rHu', NULL, 1, NULL, '2018-12-02 14:12:42', '2018-12-02 15:04:35'),
(26, 12600, 'dipa.bhandari@prabhucapital.com', 3, '$2y$10$1/.VD8layCc.EFPkBQND5u2pQqo/FiqCjgK2XjQWElLBoI3M5A8My', NULL, 1, NULL, '2018-12-02 14:13:05', '2018-12-02 15:02:01'),
(27, 123, 'sanam.khadgi@kumaribank.com', 4, '$2y$10$tYgDrpCgGJHgjfV9eNj0peFbPkERiEgFOJotlyt4rJwI3dC9T7VCa', NULL, 1, NULL, '2018-12-03 15:09:42', '2018-12-03 15:22:15'),
(28, 109, 'prabal.pant@nmbcl.com.np', 4, '$2y$10$9S9ofH7xnt9HvCBVwIi2lOYhH6vpzaR3I7Jiy3AH.KdHLwIrxTvWS', NULL, 1, NULL, '2018-12-05 14:07:10', '2018-12-05 15:29:58'),
(29, 11000, 'sovana.baral@nmbcl.com.np', 3, '$2y$10$57Dx4kB.uJiGqEA5uts5luyS/tlq3lupn9.5/j0KlpZYOr9DIZX9C', NULL, 1, NULL, '2018-12-05 14:07:37', '2018-12-05 14:29:57'),
(30, 161, 'rashmirijal.sc@sanimabank.com', 4, '$2y$10$zgdKTRNA3U.ZGLD4TBipVe4E1YtL9FoG4QZheUzWJfHvTW2Sn7pjW', NULL, 1, NULL, '2018-12-05 14:11:05', '2018-12-06 09:25:19'),
(31, 15800, 'rashmirijal.sc@sanimabank.com', 3, '$2y$10$fLxigAS8nkEYb/6lm0Sx9eztlD41bUhty8PN6gZfaJn11sDX7194a', NULL, 1, NULL, '2018-12-05 14:11:33', '2018-12-06 09:29:40');

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE `user_type` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'CDSC', '2018-09-26 05:40:25', '0000-00-00 00:00:00'),
(3, 'DP', '2018-09-26 05:40:25', '0000-00-00 00:00:00'),
(4, 'RTA', '2018-09-26 05:40:32', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `tokenIndx` (`remember_token`);

--
-- Indexes for table `casba_bank`
--
ALTER TABLE `casba_bank`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bankIdIndx` (`bank_id`),
  ADD KEY `statusIndx` (`status`),
  ADD KEY `deletedAtIndx` (`deleted_at`),
  ADD KEY `bankNameIndx` (`bank_name`);

--
-- Indexes for table `cdsc_tariff`
--
ALTER TABLE `cdsc_tariff`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoryIndx` (`category`),
  ADD KEY `statusIndx` (`status`);

--
-- Indexes for table `cdsc_tariff_category`
--
ALTER TABLE `cdsc_tariff_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clearing_member`
--
ALTER TABLE `clearing_member`
  ADD PRIMARY KEY (`cm_id`),
  ADD KEY `cmNoIndx` (`cm_no`),
  ADD KEY `poolAccountDPIndx` (`pool_account_dp`),
  ADD KEY `statusIndx` (`status`),
  ADD KEY `deletedAtIndx` (`deleted_at`),
  ADD KEY `nameIndx` (`name`);

--
-- Indexes for table `daily_update`
--
ALTER TABLE `daily_update`
  ADD PRIMARY KEY (`id`),
  ADD KEY `createdAtIndx` (`created_at`),
  ADD KEY `updatedAtIndx` (`updated_at`);

--
-- Indexes for table `death_notice`
--
ALTER TABLE `death_notice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userIdIndx` (`user_id`),
  ADD KEY `userTypeIndx` (`user_type`),
  ADD KEY `publishedDateIndx` (`published_date`),
  ADD KEY `endingDateIndx` (`ending_date`),
  ADD KEY `verifiedByIndx` (`verified_by`),
  ADD KEY `statusIndx` (`status`),
  ADD KEY `nameIndx` (`name`),
  ADD KEY `createdAtIndx` (`created_at`),
  ADD KEY `updatedAtIndx` (`updated_at`);

--
-- Indexes for table `death_notice_log`
--
ALTER TABLE `death_notice_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `noticeIdIndx` (`notice_id`),
  ADD KEY `userIdIndx` (`user_id`),
  ADD KEY `userTypeIndx` (`user_type`),
  ADD KEY `publishedDateIndx` (`published_date`),
  ADD KEY `endingDateIndx` (`ending_date`),
  ADD KEY `verifiedByIndx` (`verified_by`),
  ADD KEY `requestIndx` (`request`),
  ADD KEY `statusIndx` (`status`),
  ADD KEY `isVerifiedIndx` (`isVerified`),
  ADD KEY `nameIndx` (`name`),
  ADD KEY `createdAtIndx` (`created_at`),
  ADD KEY `updatedAtIndx` (`updated_at`);

--
-- Indexes for table `downloads`
--
ALTER TABLE `downloads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `typeIndx` (`type`),
  ADD KEY `statusIndx` (`status`),
  ADD KEY `deletedAtIndx` (`deleted_at`),
  ADD KEY `createdAtIndx` (`created_at`),
  ADD KEY `updatedAtIndx` (`updated_at`),
  ADD KEY `titleIndx` (`title`);

--
-- Indexes for table `downloads_type`
--
ALTER TABLE `downloads_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `createdAtIndx` (`created_at`),
  ADD KEY `updatedAtIndx` (`updated_at`);

--
-- Indexes for table `dp`
--
ALTER TABLE `dp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dpIdIndx` (`dp_id`),
  ADD KEY `dpTypeIndx` (`dp_type`),
  ADD KEY `nameIndx` (`name`),
  ADD KEY `meroshareIndx` (`meroshare`),
  ADD KEY `statusIndx` (`status`),
  ADD KEY `setupDateIndx` (`setup_date`),
  ADD KEY `deletedAtIndx` (`deleted_at`),
  ADD KEY `createdAtIndx` (`created_at`),
  ADD KEY `updatedAtIndx` (`updated_at`);

--
-- Indexes for table `isin`
--
ALTER TABLE `isin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `issuerIdIndx` (`issuer_id`),
  ADD KEY `scriptIndx` (`script`),
  ADD KEY `isinCodeIndx` (`isin_code`),
  ADD KEY `typeIndx` (`type`),
  ADD KEY `statusIndx` (`status`),
  ADD KEY `deletedAtIndx` (`deleted_at`),
  ADD KEY `createdAtIndx` (`created_at`),
  ADD KEY `updatedAtIndx` (`updated_at`);

--
-- Indexes for table `isin_type`
--
ALTER TABLE `isin_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `createdAtIndx` (`created_at`),
  ADD KEY `updatedAtIndx` (`updated_at`);

--
-- Indexes for table `issuer`
--
ALTER TABLE `issuer`
  ADD PRIMARY KEY (`issuer_id`),
  ADD KEY `rtaIdIndx` (`rta_id`),
  ADD KEY `nameIndx` (`name`),
  ADD KEY `statusIndx` (`status`),
  ADD KEY `deletedAtIndx` (`deleted_at`),
  ADD KEY `createdAtIndx` (`created_at`),
  ADD KEY `updatedAtIndx` (`updated_at`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`),
  ADD KEY `statusIndx` (`status`),
  ADD KEY `createdAtIndx` (`created_at`),
  ADD KEY `updatedAtIndx` (`updated_at`),
  ADD KEY `deletedAtIndx` (`deleted_at`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_notice`
--
ALTER TABLE `news_notice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `statusIndx` (`status`),
  ADD KEY `typeIndx` (`type`),
  ADD KEY `publishedDateAtIndx` (`published_date`),
  ADD KEY `createdAtIndx` (`created_at`),
  ADD KEY `updatedAtIndx` (`updated_at`),
  ADD KEY `deletedAtIndx` (`deleted_at`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`page_id`),
  ADD KEY `menuIdIndx` (`menu_id`),
  ADD KEY `subMenuIdIndx` (`submenu_id`),
  ADD KEY `statusIndx` (`status`),
  ADD KEY `createdAtIndx` (`created_at`),
  ADD KEY `updatedAtIndx` (`updated_at`),
  ADD KEY `deletedAtIndx` (`deleted_at`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `tokenIndx` (`token`);

--
-- Indexes for table `press_release`
--
ALTER TABLE `press_release`
  ADD PRIMARY KEY (`press_release_id`),
  ADD KEY `titleIndx` (`title`),
  ADD KEY `statusIndx` (`status`),
  ADD KEY `publishedDateAtIndx` (`published_date`),
  ADD KEY `createdAtIndx` (`created_at`),
  ADD KEY `updatedAtIndx` (`updated_at`),
  ADD KEY `deletedAtIndx` (`deleted_at`);

--
-- Indexes for table `rta`
--
ALTER TABLE `rta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rtaIdIndx` (`rta_id`),
  ADD KEY `dpTypeIndx` (`dp_type`),
  ADD KEY `nameIndx` (`name`),
  ADD KEY `statusIndx` (`status`),
  ADD KEY `setupDateIndx` (`setup_date`),
  ADD KEY `createdAtIndx` (`created_at`),
  ADD KEY `updatedAtIndx` (`updated_at`),
  ADD KEY `deletedAtIndx` (`deleted_at`);

--
-- Indexes for table `settlement`
--
ALTER TABLE `settlement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `settlementIdIndx` (`settlement_id`),
  ADD KEY `statusIndx` (`status`),
  ADD KEY `tradeDateIndx` (`trade_date`),
  ADD KEY `createdAtIndx` (`created_at`),
  ADD KEY `updatedAtIndx` (`updated_at`),
  ADD KEY `deletedAtIndx` (`deleted_at`);

--
-- Indexes for table `slider_image`
--
ALTER TABLE `slider_image`
  ADD PRIMARY KEY (`image_id`),
  ADD KEY `createdAtIndx` (`created_at`),
  ADD KEY `updatedAtIndx` (`updated_at`),
  ADD KEY `deletedAtIndx` (`deleted_at`);

--
-- Indexes for table `submenu`
--
ALTER TABLE `submenu`
  ADD PRIMARY KEY (`submenu_id`),
  ADD KEY `menuIdIndx` (`menu_id`),
  ADD KEY `createdAtIndx` (`created_at`),
  ADD KEY `updatedAtIndx` (`updated_at`),
  ADD KEY `deletedAtIndx` (`deleted_at`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userIdIndx` (`user_id`),
  ADD KEY `emailIndx` (`email`),
  ADD KEY `typeIndx` (`type`),
  ADD KEY `statusIndx` (`status`),
  ADD KEY `createdAtIndx` (`created_at`),
  ADD KEY `updatedAtIndx` (`updated_at`),
  ADD KEY `deletedAtIndx` (`deleted_at`);

--
-- Indexes for table `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `titleIndx` (`title`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `casba_bank`
--
ALTER TABLE `casba_bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `cdsc_tariff`
--
ALTER TABLE `cdsc_tariff`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cdsc_tariff_category`
--
ALTER TABLE `cdsc_tariff_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `clearing_member`
--
ALTER TABLE `clearing_member`
  MODIFY `cm_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `daily_update`
--
ALTER TABLE `daily_update`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `death_notice`
--
ALTER TABLE `death_notice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `death_notice_log`
--
ALTER TABLE `death_notice_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `downloads`
--
ALTER TABLE `downloads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `downloads_type`
--
ALTER TABLE `downloads_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `dp`
--
ALTER TABLE `dp`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `isin`
--
ALTER TABLE `isin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=343;

--
-- AUTO_INCREMENT for table `isin_type`
--
ALTER TABLE `isin_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `issuer`
--
ALTER TABLE `issuer`
  MODIFY `issuer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10355;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `news_notice`
--
ALTER TABLE `news_notice`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `page_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `press_release`
--
ALTER TABLE `press_release`
  MODIFY `press_release_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `rta`
--
ALTER TABLE `rta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `settlement`
--
ALTER TABLE `settlement`
  MODIFY `id` int(100) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1575;

--
-- AUTO_INCREMENT for table `slider_image`
--
ALTER TABLE `slider_image`
  MODIFY `image_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `submenu`
--
ALTER TABLE `submenu`
  MODIFY `submenu_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `user_type`
--
ALTER TABLE `user_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
