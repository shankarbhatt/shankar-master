<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookClosureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_closure', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('issuer_id');
            $table->date('last_trade_date');
            $table->date('book_closure_start_date');
            $table->date('book_closure_end_date');
            $table->string('book_closure_purpose');
            $table->string('book_closure_fiscal_year');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_closure');
    }
}
