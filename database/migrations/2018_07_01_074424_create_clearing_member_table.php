<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClearingMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clearing_member', function (Blueprint $table) {
            $table->increments('cm_id');
            $table->string('name');
            $table->integer('cm_no');
            $table->string('address');
            $table->integer('pool_account');
            $table->integer('pool_account_dp');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clearing_member');
    }
}
